#!/bin/bash
# Purpose: Take the documentation in the Repository and copy it to
#          the appropriate places in the directory tree.
# Run this script from the top level folder.

root_path=`pwd`

# Locate the documentation.
src_dir="$root_path/Repository"
# The target folders.
docCmdFolder="MakeDocumentation"

if [ ! -d "$src_dir" ]
then
  echo "The directory: $src_dir does not exist."
  exit
fi

# Lists of files to copy.
sub_dirs="C++ Java Python"
top_level_files="CHANGELOG.md DevelopersPleaseReadThisFirst.md LICENSE README.md TaggingForRelease.md"
cpp_files="LICENSE README.md"
java_files="LICENSE README.html README.md"
python_files="LICENSE README.md"

dest_dir="$root_path"
for f in $top_level_files
do
#    echo "$src_dir/$f -> $dest_dir"
    cp -u $src_dir/$f $dest_dir
done

for dir in $sub_dirs
do
    dest_dir="$root_path/$dir"
    if [ ! -d "$dest_dir" ]
    then
        echo "The directory: $dest_dir does not exist."
        exit
    fi
done

dest_dir="$root_path/C++"
for f in $cpp_files
do
    cp -u $src_dir/$f $dest_dir
done
dest_dir="$root_path/Java"
for f in $java_files
do
    cp -u $src_dir/$f $dest_dir
done
dest_dir="$root_path/Python"
for f in $python_files
do
    cp -u $src_dir/$f $dest_dir
done
