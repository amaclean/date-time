# Tasks

1. If you intend to commit code to the repository, copy any scripts in `/hooks` into `.git/hooks` before committing any code.
2. For Python code set the indentation to **4** spaces, no tabs.
3. For Java code set the indentation to **2** spaces, no tabs.
4. For C++ code set the indentation to **2** spaces, no tabs.
5. Follow the documentation/formatting as in the original code.
6. Update the text files in the `Repository` folder.
<br>When this is done, go to the top-level directory and run:
    - `CopyDocumentation.sh` or `CopyDocumentation.cmd` to copy the files in the **Repository** folder to their appropriate locations.
    - `MakeAllTheDocumentation.sh` or `MakeAllTheDocumentation.cmd` to update the documentation in the **doc** folders for each language. Note that these two scripts also call their respective `CopyDocumentation` scripts.
