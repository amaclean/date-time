echo off
rem Purpose: make the documentation for C++, Java and Python.
rem Date:       2015-01-05

rem doxygen, javadoc and sphinx need to be installed.
rem Ensure that these programs exist in your path.
rem Locate the scripts and command files.
set docCmdFolder="MakeDocumentation"
set root_path=%cd%

if exist "%~dp0\CopyDocumentation.cmd" (
echo Copying the documentation into the appropriate folders.
@call CopyDocumentation.cmd
) else (
echo Error: CopyDocumentation.cmd not found.
goto :eof

)
if not exist %root_path%\C++\doc md  %root_path%\C++\doc
if not exist %root_path%\Java\DateTime\doc md  %root_path%\Java\DateTime\doc
if not exist %root_path%\Python\doc md  %root_path%\Python\doc

cd %root_path%\C++\%docCmdFolder%
doxygen
cd %root_path%
cd %root_path%\Java\DateTime\src\%docCmdFolder%
doxygen
cmd /c MakeJavaDoc.cmd
cd %root_path%
cd %root_path%\Python\%docCmdFolder%
doxygen
cd %root_path%\Python\%docCmdFolder%
cmd /c make html
cd %root_path%

echo Done.
