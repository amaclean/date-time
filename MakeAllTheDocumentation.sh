#!/bin/bash
# Purpose: make the documentation for C++, Java and Python.
# Date:       2015-01-05

# doxygen, javadoc and sphinx need to be installed.
# Ensure that these programs exist in your path.
#
# For the Macintosh:
# If you installed Doxygen as an app the command line version is most
# likely in:
# /Applications/Doxygen.app/Contents/Resources/
# So if /usr/local/bin is in your PATH then add a symbolic link:
# sudo ln -s /Applications/Doxygen.app/Contents/Resources/doxygen /usr/local/bin
#
# Locate the scripts and command files.
docCmdFolder="MakeDocumentation"
docPath=( /C++:doxygen /Java/DateTime/src:doxygen /Java/DateTime/src:'bash ./MakeJavaDoc.sh' /Python:doxygen /Python:'make html' ) # /C++:doxygen /Java/src:doxygen /Java/src:"bash ./MakeJavaDoc.sh")
root_path=$(pwd)

# Make the documentation folders.
subDir=("C++" "Java/DateTime" "Python")
for i in "${subDir[@]}"
do
	if [ ! -d "$root_path/$i/doc" ]
	then
		 mkdir "$root_path/$i/doc"
	fi
done

# Copy the files (if needed)
./CopyDocumentation.sh

# Generate the documentation.
for dir in "${docPath[@]}"
do
	cd $root_path${dir%%:*}/$docCmdFolder
	${dir##*:}
	cd $root_path
done
