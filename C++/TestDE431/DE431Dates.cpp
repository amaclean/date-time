
#include "DE431Dates.h"

#include <cmath>
#include <fstream>
#include <sstream>
#include <string>

DE431Dates::TestResults DE431Dates::ReadFile(std::string const& fileName,
                                             double const& minJD,
                                             double const& maxJD)
{
  TestResults res;
  res.result.first = false;
  res.passed = 0;
  res.failed = 0;
  std::ifstream dates(fileName.c_str(), std::ios::binary);
  if (dates.is_open())
  {
    std::string line;
    int linesProcessed = 0;
    DT::YMD ymd;
    double jd = 0;
    std::pair<bool, std::string> p1;
    res.result.first = true;
    double jdOld = 0;
    bool gotStartDate = false;
    while (std::getline(dates, line))
    {
      if (line[0] == '#')
      {
        continue;
      }
      std::istringstream iss;
      iss.str(line);
      iss >> ymd.year >> ymd.month >> ymd.day >> jd;
      // Assume the dates in the file are ordered.
      if (jd > maxJD)
      {
        break;
      }
      if (jd < minJD)
      {
        continue;
      }
      else
      {
        if (!gotStartDate)
        {
          res.startDate = line;
          gotStartDate = true;
          jdOld = jd;
        }
        res.endDate = line;
        p1 = TestJulianGregorian(jd, ymd);
        std::ostringstream os;
        os << std::fixed << std::setw(4) << std::setprecision(1) << jd - jdOld;
        p1.second = line + " deltaDays: " + os.str() + "\n" + p1.second;
        res.result.second.push_back(p1);
        res.result.first = res.result.first && p1.first;
        if (p1.first)
        {
          ++res.passed;
        }
        else
        {
          ++res.failed;
        }
        jdOld = jd;
      }
      linesProcessed++;
      if (linesProcessed % 10000 == 0)
      {
        std::cout << "Processed " << std::setw(6) << linesProcessed << " dates."
                  << std::endl;
      }
    }
    dates.close();
  }
  else
  {
    std::cerr << "Failed to open: " << fileName << " for reading." << std::endl;
  }

  return res;
}

std::pair<bool, std::string> DE431Dates::TestCD2JDJG(double const& jd,
                                                     DT::YMD const& ymd)
{
  std::pair<bool, std::string> res;
  auto tjd = this->pDateConversions->cd2jdJG(ymd);
  auto ref = static_cast<int>(std::ceil(jd));
  if (tjd != ref)
  {
    std::ostringstream os;
    os << "Fail (cd2jdJG) got: " << std::fixed << std::setw(10)
       << std::setprecision(1) << tjd - 0.5 << " expected: " << ref - 0.5
       << std::endl;
    res.first = false;
    res.second = os.str();
  }
  else
  {
    res.first = true;
    res.second = "Passed (cd2jdJG)\n";
  }
  return res;
}

std::pair<bool, std::string> DE431Dates::TestJD2CDJG(double const& jd,
                                                     DT::YMD const& ymd)
{
  std::pair<bool, std::string> res;
  auto ymd1 = this->pDateConversions->jd2cdJG(static_cast<int>(std::ceil(jd)));
  if (ymd1 != ymd)
  {
    std::ostringstream os;
    os << "Fail (jd2cdJG) got: " << ymd1 << " expected: " << ymd << std::endl;
    res.first = false;
    res.second = os.str();
  }
  else
  {
    res.first = true;
    res.second = "Passed (jd2cdJG)\n";
  }
  return res;
}

std::pair<bool, std::string> DE431Dates::TestJulianGregorian(double const& jd,
                                                             DT::YMD const& ymd)
{
  auto p1 = TestCD2JDJG(jd, ymd);
  auto p2 = TestJD2CDJG(jd, ymd);
  return std::pair<bool, std::string>(p1.first && p2.first,
                                      p1.second + p2.second);
}
