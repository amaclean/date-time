/*=========================================================================

  Program:   Date Time Library
  File   :   DateConversionsTests.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef DTTESTS_TESTDE431_DE431DATES_H
#define DTTESTS_TESTDE431_DE431DATES_H

#pragma once

#include "DateConversions.h"

#include <memory>
#include <vector>

class DE431Dates
{
public:
  DE431Dates()
  {
    this->pDateConversions.reset(new DT::DateConversions());
  }

  virtual ~DE431Dates() = default;

public:
  struct TestResults
  {
    int passed = 0; // The number of passing dates.
    int failed = 0; // The number of failing dates.
    std::string startDate;
    std::string endDate;
    std::pair<bool, std::vector<std::pair<bool, std::string>>> result;
  };

  TestResults ReadFile(std::string const& fileName, double const& minJD,
                       double const& maxJD);

  /** Test the conversion of calendar date to julian day.
        The Gregorian or Julian Calendar is used as appropriate.

        @param jd the expected julian day for the given year, month and day.
        @param ymd the date.

        @return A pair consisting of true (if the test passed)
         or false and a message.
    */
  std::pair<bool, std::string> TestCD2JDJG(double const& jd,
                                           DT::YMD const& ymd);

  /** Test the conversion of julian day to calendar date.
      The Gregorian or Julian Calendar is used as appropriate.

      @param jd the julian day.
      @param ymd the date.

      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  std::pair<bool, std::string> TestJD2CDJG(double const& jd,
                                           DT::YMD const& ymd);

  /** Test the conversion of calendar date to julian day and vice versa.
      The Gregorian or Julian Calendar is used as appropriate.

      @param jd the julian day for the given year, month and day.
      @param ymd the date.

      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  std::pair<bool, std::string> TestJulianGregorian(double const& jd,
                                                   DT::YMD const& ymd);

private:
  std::unique_ptr<DT::DateConversions> pDateConversions;
};

#endif // DTTESTS_TESTDE431_DE431DATES_H
