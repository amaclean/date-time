#include "DE431Dates.h"

#include <chrono>
#include <ctime>
#include <fstream>
#include <sstream>
#include <string>

namespace {
/**
 * Show the command lime parameters.
 *
 * @param fn: The program name.
 */
std::string ShowUsage(std::string fn);

} // namespace

int main(int argc, char** argv)
{
  if (argc != 3)
  {
    std::cout << ShowUsage(argv[0]) << std::endl;
    return EXIT_FAILURE;
  }

  auto timeStr = [](const time_t& t) {
    tm tm_t{};
#if defined(WIN32)
    errno_t ern;
    ern = localtime_s(&tm_t, &t);
#else
    tm tm_info{};
    tm_info = *localtime_r(&t, &tm_t);
#endif
    // tm_t = *localtime(&t);
    std::ostringstream os;
    os << std::put_time(&tm_t, "%F %T%zZ");
    return os.str();
  };

  DE431Dates test;
  std::chrono::time_point<std::chrono::system_clock> start, end;
  start = std::chrono::system_clock::now();

  auto res = test.ReadFile(argv[1], -3100015.50, 8000016.50);
  // auto res = test.ReadFile(argv[1], -3099146.5, -3098690.5);
  // auto res = test.ReadFile(argv[1], -3100015.50, 0.50);
  // auto res = test.ReadFile(argv[1], -0.5, 365.5);
  // auto res = test.ReadFile(argv[1], -487.5, -1.5);

  std::ostringstream os;
  os << res.passed + res.failed << " total, " << res.passed << " passed, "
     << res.failed << " failed.";
  std::string stats = os.str();
  os.str("");
  std::ofstream result;
  result.open(argv[2], std::ios::out | std::ios::binary);
  result << "Tested\nFrom: " << res.startDate << "\nTo:   " << res.endDate
         << std::endl;
  if (!res.result.first)
  {
    result << "These tests failed." << std::endl;
    for (const auto& p : res.result.second)
    {
      if (!p.first)
      {
        result << p.second << std::endl;
      }
    }
    result << stats << std::endl;
    std::cout << stats << std::endl;
    result.close();
    return EXIT_FAILURE;
  }

  result << stats << std::endl;
  std::cout << stats << std::endl;
  result.close();

  end = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end - start;
  std::time_t end_time = std::chrono::system_clock::to_time_t(end);
  std::cout << "Finished computation at " << timeStr(end_time)
            << "Elapsed time: " << elapsed_seconds.count() << "s\n";

  return EXIT_SUCCESS;
}

namespace {

std::string ShowUsage(std::string fn)
{
  // Remove the folder (if present) then remove the extension in this order
  // since the folder name may contain periods.
  auto last_slash_idx = fn.find_last_of("\\/");
  if (std::string::npos != last_slash_idx)
  {
    fn.erase(0, last_slash_idx + 1);
  }
  auto period_idx = fn.rfind('.');
  if (std::string::npos != period_idx)
  {
    fn.erase(period_idx);
  }
  std::ostringstream os;
  os << "\nusage: " << fn << " src_fn tgt_fn\n\n"
     << "Test DE431 Dates.\n\n"
     << "positional arguments:\n"
     << "  src_fn      The file of DE431 dates, e.g. DE431Dates.txt.\n"
     << "  tgt_fn      The file of test results, e.g. DE431Results.txt.\n"
     << "\n"
     << std::endl;
  return os.str();
}

} // namespace
