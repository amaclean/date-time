/*=========================================================================

  Program:   Date Time Library
  File   :   HMSTests.cpp

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "HMSTests.h"

using namespace DT_Tests;

CommonTests::TResult HMSTests::TestAll()
{
  std::vector<TResult> messages{{this->TestConstructors()},
                                {this->TestSet()},
                                {this->TestComparisonOperators()},
                                {this->TestAssignment()},
                                {this->TestStringOutput()}};

  return this->AggregateResults("HMS Tests.", messages);
}

CommonTests::TResult HMSTests::TestConstructors()
{
  TResult p = this->MakeHeader("Testing constructors.", 2);

  p.second += "* HMS() ";
  DT::HMS h;
  auto p1 = this->PassFail(h.hour == 0 && h.minute == 0 && h.second == 0.0);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* HMS(h,m,s) ";
  DT::HMS h1(this->hour, this->minute, this->second);
  p1 = this->PassFail(h1.hour == this->hour && h1.minute == this->minute &&
                      h1.second == this->second);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* HMS(hms) ";
  DT::HMS h2(h1);
  p1 = this->PassFail(h1.hour == h2.hour && h1.minute == h2.minute &&
                      h1.second == h2.second);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult HMSTests::TestSet()
{
  TResult p = this->MakeHeader("Testing setting members.", 2);

  p.second += "* SetHMS(h,m,s) ";

  DT::HMS h1(this->hour, this->minute, this->second);
  DT::HMS h2;
  h2.SetHMS(this->hour, this->minute, this->second);
  auto p1 = this->PassFail(h1.hour == h2.hour && h1.minute == h2.minute &&
                           h1.second == h2.second);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult HMSTests::TestComparisonOperators()
{
  TResult p = this->MakeHeader("Testing comparison operators.", 2);

  p.second += "* == ";

  DT::HMS h1(this->hour, this->minute, this->second);
  auto p1 = this->PassFail(h1 == *this->hms);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* &asymp; ";
  DT::HMS h2(this->hour, this->minute, this->second - 0.001);
  p1 = this->PassFail(this->hms->IsClose(h2, 0.0001));
  p.first &= p1.first;
  p.second += "\n<br>" + p1.second;

  // Expect a fail here
  p1 = this->PassFail(!this->hms->IsClose(h2, 0.00001));
  p.first &= p1.first;
  p.second += "<br>" + p1.second;

  p.second += "* != ";
  h1.second -= 1;
  p1 = this->PassFail(h1 != *this->hms);
  p.first &= p1.first;
  p.second += p1.second;

  DT::HMS a(13, 7, 2);
  DT::HMS b(14, 7, 2);
  DT::HMS c(13, 8, 2);
  DT::HMS d(13, 7, 3);

  p.second += "* &lt; and &le; ";
  p1 = this->PassFail(a < b && a < c && a < d && a <= b && a <= c && a <= d &&
                      a <= a && !(a > b) && !(a >= b));
  p.first &= p1.first;
  p.second += p1.second;

  DT::HMS e(13, 7, 2);
  DT::HMS f(12, 7, 2);
  DT::HMS g(13, 6, 2);
  DT::HMS h(13, 7, 1);

  p.second += "* &gt; and &ge; ";
  p1 = this->PassFail(e > f && e > g && e > h && e >= f && e >= g && e >= h &&
                      e >= e && !(e < f) && !(e <= f));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult HMSTests::TestAssignment()
{
  TResult p = this->MakeHeader("Testing assignment.", 2);

  p.second += "* = ";

  auto h1 = *this->hms;
  auto p1 = this->PassFail(h1.hour == this->hms->hour &&
                           h1.minute == this->hms->minute &&
                           h1.second == this->hms->second);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult HMSTests::TestStringOutput()
{
  TResult p = this->MakeHeader("Testing <<.", 2);

  std::ostringstream os;
  os << *this->hms;
  auto p1 = this->Compare(os.str(), this->valueStr, "* ");
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  DT::HMS h1(-23, 59, 59.01);
  std::string expected = "-23:59:59.010000000000";
  os << h1;
  p1 = this->Compare(os.str(), expected, "* ");
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  h1.SetHMS(0, -59, 59.01);
  os << h1;
  expected = "-00:59:59.010000000000";
  p1 = this->Compare(os.str(), expected, "* ");
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  h1.SetHMS(0, 0, -59.01);
  expected = "-00:00:59.010000000000";
  os << h1;
  p1 = this->Compare(os.str(), expected, "* ");
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}
