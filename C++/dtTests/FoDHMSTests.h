/*=========================================================================

  Program:   Date Time Library
  File   :   FoDHMSTests.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef DTTESTS_FODHMSTESTS_H
#define DTTESTS_FODHMSTESTS_H

#pragma once

#include "CommonTests.h"
#include "DateConversions.h"
#include "DateTimeBase.h"
#include "TimeConversions.h"
#include "YMDHMS.h"

#include <memory>
#include <vector>

namespace DT_Tests {

/**
  A test harness to test fraction of the day to HMS conversions.

  @author Andrew J. P. Maclean
*/
class FoDHMSTests : public CommonTests, DT::DateTimeBase
{
public:
  FoDHMSTests()
  {
    this->dc.reset(new DT::DateConversions());
    this->tc.reset(new DT::TimeConversions());
    Init();
  }

  virtual ~FoDHMSTests() = default;

public:
  /**
   * Run all the tests.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   *         string of failed tests.
   */
  TResult TestAll();

  /** Testing Calendar Date -> (jdn, fod) -> Julian Date.
   *
   * @return A pair consisting of true (if the test passed)
   * or false and a message.
   */
  TResult TestCalendarDateToJulianDate();

  /** Testing Julian Date -> (jdn, fod) -> Calendar Date.
   *
   * @return A pair consisting of true (if the test passed)
   * or false and a message.
   */
  TResult TestJulianDateToCalendarDate();

  /** Testing jd -> jdn, hms -> calendar date -> jd.
   *
   * @return A pair consisting of true (if the test passed)
   * or false and a message.
   */
  TResult TestFodHMSJD();

  /** Testing jd -> jdn, hms -> calendar date -> jd.
   *
   * @return A pair consisting of true (if the test passed)
   * or false and a message.
   */
  TResult TestJD();

  /** Testing jd -> julian date time -> jd.
   *
   * @return A pair consisting of true (if the test passed)
   * or false and a message.
   */
  TResult TestJD1();

  /** Testing jd -> calendar -> jd.
   *
   * @return A pair consisting of true (if the test passed)
   * or false and a message.
   */
  TResult TestJD2();

  /** Testing calendar -> jd -> calendar.
   *
   * @return A pair consisting of true (if the test passed)
   * or false and a message.
   */
  TResult TestCalendar();

private:
  std::unique_ptr<DT::DateConversions> dc;
  std::unique_ptr<DT::TimeConversions> tc;

  // This vector is filled with test times consisting of pairs of
  // Julian date fractions of the day (they run from 12 Noon)
  // and hours minutes and seconds expressed as a fraction
  // of the day (they run from midnight).
  typedef std::pair<double, double> TDD;
  std::vector<TDD> testFoD;

  // This vector is filled with test dates consisting of pairs of
  // ymdhms and Julian Date.
  typedef std::pair<DT::YMDHMS, double> TDTD;
  std::vector<TDTD> testDates;

  std::string valueStr;

  void Init();
};

} // End namespace DT_Tests.

#endif // DTTESTS_FODHMSTESTS_H
