/*=========================================================================

  Program:   Date Time Library
  File   :   TimeIntervalTests.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef DTTESTS_TIMEINTERVALTESTS_H
#define DTTESTS_TIMEINTERVALTESTS_H

#pragma once

#include "CommonTests.h"
#include "DateTimeBase.h"
#include "TimeInterval.h"

#include <memory>

namespace DT_Tests {

/**
  A test harness to test the TimeInterval class.

  @author Andrew J. P. Maclean
*/
class TimeIntervalTests : public CommonTests, DT::DateTimeBase
{
public:
  TimeIntervalTests()
    : deltaDay(15), deltaFoD(0.459), valueStr(" 15.459000000000000")
  {
    this->ti.reset(new DT::TimeInterval(deltaDay + 1, deltaFoD - 1.0));
  }

  virtual ~TimeIntervalTests() = default;

public:
  /** Run all the tests.
      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestAll();

  /** Run the tests for the constructors.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestConstructors();

  /** Run the tests for the increment and addition operators.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestIncrementAndAddition();

  /** Run the tests for the decrement and subtraction operators.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestDecrementAndSubtraction();

  /** Run the tests for setting the members.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestSetGet();

  /** Run the tests for the comparison operators.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestComparisonOperators();

  /** Run the test for assignment.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestAssignment();

  /** Run the test for string output.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestStringOutput();

private:
  std::unique_ptr<DT::TimeInterval> ti;
  int deltaDay;
  double deltaFoD;
  std::string valueStr;
};

} // End namespace DT_Tests.

#endif // DTTESTS_TIMEINTERVALTESTS_H
