/*=========================================================================

  Program:   Date Time Library
  File   :   DateTimeParserTests.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef DTTESTS_DATETIMEPARSERTESTS_H
#define DTTESTS_DATETIMEPARSERTESTS_H

#pragma once

#include "CommonTests.h"
#include "DateTimeBase.h"
#include "DateTimeParser.h"

#include <memory>

namespace DT_Tests {

/**
  A test harness to test the DateTimeParser classes.

  @author Andrew J. P. Maclean
*/
class DateTimeParserTests : public CommonTests, DT::DateTimeBase
{
public:
  DateTimeParserTests();

  virtual ~DateTimeParserTests() = default;

public:
  /** Run all the tests.
      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestAll();

  /**
   * Run the test for parsing a decimal string.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   *         message.
   */
  TResult TestParseDecNum();

  /** Run the test for parsing a date time.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestParseDateTime();

  /** Run the test for parsing a date time.
   *
   * Here we are testing for expected failures in parsing.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestParseDateTimeFail();

  /** Run the test for parsing a date time.
   *
   * Here we are testing for expected failures in parsing.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestParseDateFail();

  /** Run the test for parsing a time.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestParseTime();

  /** Run the test for parsing a time.
   *
   * Here we are testing for expected failures in parsing.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestParseTimeFail();

  /** Run the test for parsing a time interval.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestParseTimeInterval();

  /** Run the test for parsing a time interval.
   *
   * Here we are testing for expected failures in parsing.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestParseTimeIntervalFail();

private:
  DT::DateTimeFormat fmt;

  std::unique_ptr<DT::DateTimeParser> parser;
};

} // End namespace DT_Tests.

#endif // DTTESTS_DATETIMEPARSERTESTS_H
