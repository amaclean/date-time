/*=========================================================================

  Program:   Date Time Library
  File   :   DayNamesTests.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef DTTESTS_DAYNAMESTESTS_H
#define DTTESTS_DAYNAMESTESTS_H

#pragma once

#include "CommonTests.h"
#include "DayNames.h"

#include <memory>

namespace DT_Tests {

/**
  A test harness to test the DayNames class.

  @author Andrew J. P. Maclean
*/
class DayNamesTests : public CommonTests
{
public:
  DayNamesTests()
  {
    this->dn.reset(new DT::DayNames());
  }

  virtual ~DayNamesTests() = default;

public:
  /** Run all the tests.
      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestAll();

  /** Run the test for constructors.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestConstructors();

  /** Run the test for assignment.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestAssignment();

  /** Run the tests for the comparison operators.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestComparisonOperators();

  /** Run the tests for testing the day names.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestDayNames();

private:
  std::unique_ptr<DT::DayNames> dn;
};

} // End namespace DT_Tests.

#endif // DTTESTS_DAYNAMESTESTS_H
