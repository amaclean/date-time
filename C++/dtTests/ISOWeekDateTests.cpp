/*=========================================================================

  Program:   Date Time Library
  File   :   ISOWeekDateTests.cpp

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "ISOWeekDateTests.h"

using namespace DT_Tests;

CommonTests::TResult ISOWeekDateTests::TestAll()
{
  std::vector<TResult> messages{{this->TestConstructors()},
                                {this->TestSet()},
                                {this->TestComparisonOperators()},
                                {this->TestAssignment()},
                                {this->TestStringOutput()}};

  return this->AggregateResults("ISOWeekDate Tests.", messages);
}

CommonTests::TResult ISOWeekDateTests::TestConstructors()
{
  TResult p = this->MakeHeader("Testing constructors.", 2);

  p.second += "* ISOWeekDate() ";
  DT::ISOWeekDate iwd1;
  auto p1 = this->PassFail(iwd1.year == 0 && iwd1.weekNumber == 0 &&
                           iwd1.weekDay == 0);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* ISOWeekDate(y,w,d) ";
  iwd1 = DT::ISOWeekDate(this->year, this->weekNumber, this->weekDay);
  p1 = this->PassFail(iwd1.year == this->year &&
                      iwd1.weekNumber == this->weekNumber &&
                      iwd1.weekDay == this->weekDay);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* ISOWeekDate(iwd) ";
  DT::ISOWeekDate iwd2(iwd1);
  p1 = this->PassFail(iwd1.year == iwd2.year &&
                      iwd1.weekNumber == iwd2.weekNumber &&
                      iwd1.weekDay == iwd2.weekDay);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult ISOWeekDateTests::TestSet()
{
  TResult p = this->MakeHeader("Testing setting members.", 2);

  p.second += "* SetISOWeekDate(y,w,d) ";
  DT::ISOWeekDate iwd1(this->year, this->weekNumber, this->weekDay);
  DT::ISOWeekDate iwd2;
  iwd2.SetISOWeekDate(this->year, this->weekNumber, this->weekDay);
  auto p1 = this->PassFail(iwd1.year == iwd2.year &&
                           iwd1.weekNumber == iwd2.weekNumber &&
                           iwd1.weekDay == iwd2.weekDay);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult ISOWeekDateTests::TestComparisonOperators()
{
  TResult p = this->MakeHeader("Testing comparison operators.", 2);

  p.second += "* == ";
  DT::ISOWeekDate iwd1(this->year, this->weekNumber, this->weekDay);
  auto p1 = this->PassFail(iwd1 == *this->iwd);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* != ";
  iwd1.weekDay -= 1;
  p1 = this->PassFail(iwd1 != *this->iwd);
  p.first &= p1.first;
  p.second += p1.second;

  DT::ISOWeekDate a(2013, 7, 2);
  DT::ISOWeekDate b(2014, 7, 2);
  DT::ISOWeekDate c(2013, 8, 2);
  DT::ISOWeekDate d(2013, 7, 3);

  p.second += "* &lt; and &le; ";
  p1 = this->PassFail(a < b && a < c && a < d && a <= b && a <= c && a <= d &&
                      a <= a && !(a > b) && !(a >= b));
  p.first &= p1.first;
  p.second += p1.second;

  DT::ISOWeekDate e(2013, 7, 2);
  DT::ISOWeekDate f(2012, 7, 2);
  DT::ISOWeekDate g(2013, 6, 2);
  DT::ISOWeekDate h(2013, 7, 1);

  p.second += "* &gt; and &ge; ";
  p1 = this->PassFail(e > f && e > g && e > h && e >= f && e >= g && e >= h &&
                      e >= e && !(e < f) && !(e <= f));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult ISOWeekDateTests::TestAssignment()
{
  TResult p = this->MakeHeader("Testing assignment.", 2);

  p.second += "* = ";
  DT::ISOWeekDate iwd1;
  iwd1 = *this->iwd;
  auto p1 = this->PassFail(iwd1.year == this->iwd->year &&
                           iwd1.weekNumber == this->iwd->weekNumber &&
                           iwd1.weekDay == this->iwd->weekDay);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult ISOWeekDateTests::TestStringOutput()
{
  TResult p = this->MakeHeader("Testing <<.", 2);

  std::ostringstream os;
  os << *this->iwd;
  auto p1 = this->Compare(os.str(), this->valueStr, "* ");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}
