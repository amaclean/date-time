/*=========================================================================

  Program:   Date Time Library
  File   :   YMDTests.cpp

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "YMDTests.h"

using namespace DT_Tests;

CommonTests::TResult YMDTests::TestAll()
{
  std::vector<TResult> messages{{this->TestConstructors()},
                                {this->TestSet()},
                                {this->TestComparisonOperators()},
                                {this->TestAssignment()},
                                {this->TestStringOutput()}};

  return this->AggregateResults("YMD Tests.", messages);
}

CommonTests::TResult YMDTests::TestConstructors()
{
  TResult p = this->MakeHeader("Testing constructors.", 2);

  p.second += "* YMD() ";
  DT::YMD y;
  auto p1 = this->PassFail(y.year == -4712 && y.month == 1 && y.day == 1);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* YMD(y,m,d) ";
  DT::YMD y1(this->year, this->month, this->day);
  p1 = this->PassFail(y1.year == this->year && y1.month == this->month &&
                      y1.day == this->day);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* YMD(ymd) ";
  DT::YMD y2(y1);
  p1 = this->PassFail(y1.year == y2.year && y1.month == y2.month &&
                      y1.day == y2.day);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult YMDTests::TestSet()
{
  TResult p = this->MakeHeader("Testing setting members.", 2);

  p.second += "* SetYMD(y,m,d) ";

  DT::YMD y1(this->year, this->month, this->day);
  DT::YMD y2;
  y2.SetYMD(this->year, this->month, this->day);
  auto p1 = this->PassFail(y1.year == y2.year && y1.month == y2.month &&
                           y1.day == y2.day);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult YMDTests::TestComparisonOperators()
{
  TResult p = this->MakeHeader("Testing comparison operators.", 2);

  p.second += "* == ";

  DT::YMD y1(this->year, this->month, this->day);
  auto p1 = this->PassFail(y1 == *this->ymd);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* != ";
  y1.day -= 1;
  p1 = this->PassFail(y1 != *this->ymd);
  p.first &= p1.first;
  p.second += p1.second;

  DT::YMD a(1992, 7, 2);
  DT::YMD b(1993, 7, 2);
  DT::YMD c(1992, 8, 2);
  DT::YMD d(1992, 7, 3);

  p.second += "* &lt; and &le; ";
  p1 = this->PassFail(a < b && a < c && a < d && a <= b && a <= c && a <= d &&
                      a <= a && !(a > b) && !(a >= b));
  p.first &= p1.first;
  p.second += p1.second;

  DT::YMD e(1992, 7, 2);
  DT::YMD f(1991, 7, 2);
  DT::YMD g(1992, 6, 2);
  DT::YMD h(1992, 7, 1);

  p.second += "* &gt; and &ge; ";
  p1 = this->PassFail(e > f && e > g && e > h && e >= f && e >= g && e >= h &&
                      e >= e && !(e < g) && !(e <= f));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult YMDTests::TestAssignment()
{
  TResult p = this->MakeHeader("Testing assignment.", 2);

  p.second += "* = ";

  auto y1 = *this->ymd;
  auto p1 =
      this->PassFail(y1.year == this->ymd->year &&
                     y1.month == this->ymd->month && y1.day == this->ymd->day);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult YMDTests::TestStringOutput()
{
  TResult p = this->MakeHeader("Testing <<.", 2);

  std::ostringstream os;

  os << *this->ymd;
  auto p1 = Compare(os.str(), this->valueStr, "* ");
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}
