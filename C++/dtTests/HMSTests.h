/*=========================================================================

  Program:   Date Time Library
  File   :   HMSTests.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef DTTESTS_HMSTESTS_H
#define DTTESTS_HMSTESTS_H

#pragma once

#include "CommonTests.h"
#include "HMS.h"

#include <memory>

namespace DT_Tests {

/**
  A test harness to test the HMS class.

  @author Andrew J. P. Maclean
*/
class HMSTests : public CommonTests
{
public:
  HMSTests()
    : hour(23), minute(59), second(59.999), valueStr(" 23:59:59.999000000000")
  {
    this->hms.reset(new DT::HMS(this->hour, this->minute, this->second));
  }

  virtual ~HMSTests() = default;

public:
  /** Run all the tests.
      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestAll();

  /** Run the tests for the constructors.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestConstructors();

  /** Run the tests for setting the members.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestSet();

  /** Run the tests for the comparison operators.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestComparisonOperators();

  /** Run the test for assignment.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestAssignment();

  /** Run the test for string output.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestStringOutput();

private:
  std::unique_ptr<DT::HMS> hms;

  int hour;
  int minute;
  double second;
  std::string valueStr;
};

} // End namespace DT_Tests.

#endif // DTTESTS_HMSTESTS_H
