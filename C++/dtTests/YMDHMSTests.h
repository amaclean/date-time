/*=========================================================================

  Program:   Date Time Library
  File   :   YMDHMSTests.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef DTTESTS_YMDHMSTESTS_H
#define DTTESTS_YMDHMSTESTS_H

#pragma once

#include "CommonTests.h"
#include "YMDHMS.h"

#include <memory>

namespace DT_Tests {

/**
  A test harness to test the YMDHMS class.

  @author Andrew J. P. Maclean
*/
class YMDHMSTests : public CommonTests
{
public:
  YMDHMSTests()
    : year(2013),
      month(1),
      day(1),
      hour(23),
      minute(59),
      second(30),
      valueStr("2013-01-01  23:59:30.000000000000")
  {
    this->ymdhms.reset(new DT::YMDHMS(this->year, this->month, this->day,
                                      this->hour, this->minute, this->second));
    this->ymd.reset(new DT::YMD(this->year, this->month, this->day));
    this->hms.reset(new DT::HMS(this->hour, this->minute, this->second));
    this->hm.reset(new DT::HM(this->hour, this->minute + this->second / 60.0));
  }

  virtual ~YMDHMSTests() = default;

public:
  /** Run all the tests.
      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestAll();

  /** Run the tests for the constructors.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestConstructors();

  /** Run the tests for setting and getting the members.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestSetGet();

  /** Run the tests for the comparison operators.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestComparisonOperators();

  /** Run the test for assignment.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestAssignment();

  /** Run the test for string output.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestStringOutput();

private:
  std::unique_ptr<DT::YMDHMS> ymdhms;
  std::unique_ptr<DT::YMD> ymd;
  std::unique_ptr<DT::HMS> hms;
  std::unique_ptr<DT::HM> hm;
  int year;
  int month;
  int day;
  int hour;
  int minute;
  double second;
  std::string valueStr;
};

} // End namespace DT_Tests.

#endif // DTTESTS_YMDHMSTESTS_H
