/*=========================================================================

  Program:   Date Time Library
  File   :   TimeIntervalTests.cpp

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "TimeIntervalTests.h"

using namespace DT_Tests;

CommonTests::TResult TimeIntervalTests::TestAll()
{
  std::vector<TResult> messages{{this->TestConstructors()},
                                {this->TestIncrementAndAddition()},
                                {this->TestDecrementAndSubtraction()},
                                {this->TestSetGet()},
                                {this->TestComparisonOperators()},
                                {this->TestAssignment()},
                                {this->TestStringOutput()}};

  return this->AggregateResults("TimeInterval Tests.", messages);
}

CommonTests::TResult TimeIntervalTests::TestConstructors()
{
  TResult p = this->MakeHeader("Testing constructors.", 2);

  p.second += "* TimeInterval() ";
  DT::TimeInterval t;
  auto p1 = this->PassFail(t.GetDeltaDay() == 0 && t.GetDeltaFoD() == 0.0);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* TimeInterval(deltaDay,deltaFoD) ";
  DT::TimeInterval t1(this->deltaDay, this->deltaFoD);
  p1 = this->PassFail(
      t1.GetDeltaDay() == this->deltaDay &&
      this->isclose(t1.GetDeltaFoD(), this->deltaFoD, this->rel_fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* TimeInterval(ti) ";
  DT::TimeInterval t2(t1);
  p1 = this->PassFail(
      t1.GetDeltaDay() == t2.GetDeltaDay() &&
      this->isclose(t1.GetDeltaFoD(), t2.GetDeltaFoD(), this->rel_fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult TimeIntervalTests::TestSetGet()
{
  TResult p = this->MakeHeader("Testing setting and getting members.", 2);

  p.second += "* SetTimeInterval(deltaDay,deltaFoD) ";
  DT::TimeInterval t1(this->deltaDay, this->deltaFoD);
  DT::TimeInterval t2;
  t2.SetTimeInterval(this->deltaDay, this->deltaFoD);
  auto p1 = this->PassFail(t1.GetDeltaDay() == t2.GetDeltaDay() &&
                           t1.GetDeltaFoD() == t2.GetDeltaFoD());
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* SetDeltaDay() ";
  auto dDay = t1.GetDeltaDay();
  t2.SetTimeInterval(0, this->deltaFoD);
  t2.SetDeltaDay(dDay);
  p1 = this->PassFail(t1.GetDeltaDay() == t2.GetDeltaDay() &&
                      t1.GetDeltaFoD() == t2.GetDeltaFoD());
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* GetDeltaFoD() ";
  auto dFoD = t1.GetDeltaFoD();
  t2.SetTimeInterval(this->deltaDay, 0);
  t2.SetDeltaFoD(dFoD);
  p1 = this->PassFail(t1.GetDeltaDay() == t2.GetDeltaDay() &&
                      t1.GetDeltaFoD() == t2.GetDeltaFoD());
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* GetDeltaFoD() ";
  t2.SetTimeInterval(0, 0);
  t2.SetDeltaFoD(this->deltaDay + dFoD);
  p1 = this->PassFail(t1.IsClose(t2, this->rel_fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult TimeIntervalTests::TestComparisonOperators()
{
  TResult p = this->MakeHeader("Testing comparison operators.", 2);

  p.second += "* == ";
  DT::TimeInterval t1(this->deltaDay, this->deltaFoD);
  auto p1 = this->PassFail(t1 != *this->ti);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* &asymp; ";
  t1.SetDeltaFoD(t1.GetDeltaFoD() - 0.01);
  p1 = this->PassFail(this->ti->IsClose(t1, 0.1));
  p.first &= p1.first;
  p.second += "\n<br>" + p1.second;

  // Expect a fail here
  p1 = this->PassFail(!this->ti->IsClose(t1, 0.01));
  p.first &= p1.first;
  p.second += "<br>" + p1.second;

  p.second += "* != ";
  p1 = this->PassFail(t1 != *this->ti);
  p.first &= p1.first;
  p.second += p1.second;

  DT::TimeInterval a(2456553, 0.53308796);
  DT::TimeInterval b(2456554, 0.53308796);
  DT::TimeInterval c(2456553, 0.53308797);

  p.second += "* &lt; and &le; ";
  p1 = this->PassFail(a < b && a < c && a <= b && a <= c && a <= a &&
                      !(a > b) && !(a >= b));
  p.first &= p1.first;
  p.second += p1.second;

  DT::TimeInterval e(2456553, 0.53308796);
  DT::TimeInterval f(2456552, 0.53308796);
  DT::TimeInterval g(2456553, 0.53308795);

  p.second += "* &gt; and &ge; ";
  p1 = this->PassFail(e > f && e > g && e >= f && e >= g && e >= e &&
                      !(e < f) && !(e <= f));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult TimeIntervalTests::TestAssignment()
{
  TResult p = this->MakeHeader("Testing assignment.", 2);

  p.second += "* = ";
  DT::TimeInterval t1;
  t1 = *this->ti;
  auto p1 = this->PassFail(t1.GetDeltaDay() == this->ti->GetDeltaDay() &&
                           t1.GetDeltaFoD() == this->ti->GetDeltaFoD());
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult TimeIntervalTests::TestIncrementAndAddition()
{
  TResult p = this->MakeHeader("Testing increment and addition.", 2);

  p.second += "* ti1 += ti2 ";
  DT::TimeInterval ti1(this->deltaDay, this->deltaFoD);
  DT::TimeInterval ti2;
  auto fd = 15 / 86400.0;
  ti2.SetTimeInterval(1, fd); // One day and 15s
  DT::TimeInterval ti3(ti1);
  ti3 += ti2;
  DT::TimeInterval ti4(this->deltaDay + 1, this->deltaFoD + fd);
  auto p1 = this->PassFail(ti3.IsClose(ti4, this->rel_fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* ti3 += days ";
  double days = 1 + fd;
  ti3 = ti1;
  ti3 += days;
  p1 = this->PassFail(ti3.IsClose(ti4, this->rel_fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* ti3 = t1 + days ";
  days = 1 + fd;
  ti3 = ti1 + days;
  p1 = this->PassFail(ti3.IsClose(ti4, this->rel_fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* ti1 + ti2 ";
  ti3 = ti1 + ti2;
  p1 = this->PassFail(ti3.IsClose(ti4, this->rel_fod));

  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult TimeIntervalTests::TestDecrementAndSubtraction()
{
  TResult p = this->MakeHeader("Testing decrement and subtraction.", 2);

  p.second += "* ti1 -= ti2 ";
  DT::TimeInterval ti1(this->deltaDay, this->deltaFoD);
  DT::TimeInterval ti2;
  double fd = 15 / 86400.0;
  ti2.SetTimeInterval(1, fd); // One day and 15s
  DT::TimeInterval ti3(ti1);
  ti3 -= ti2;
  DT::TimeInterval ti4(this->deltaDay - 1, this->deltaFoD - fd);
  auto p1 = this->PassFail(ti3.IsClose(ti4, this->rel_fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* ti3 -= days ";
  double days = 1 + fd;
  ti3 = ti1;
  ti3 -= days;
  p1 = this->PassFail(ti3.IsClose(ti4, this->rel_fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* ti3 = t1 - days ";
  days = 1 + fd;
  ti3 = ti1 - days;
  p1 = this->PassFail(ti3.IsClose(ti4, this->rel_fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* ti1 - ti2 ";
  ti3 = ti1 - ti2;
  p1 = this->PassFail(ti3.IsClose(ti4, this->rel_fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult TimeIntervalTests::TestStringOutput()
{
  TResult p = this->MakeHeader("Testing <<.", 2);

  std::ostringstream os;

  os << *this->ti;
  auto p1 = this->Compare(os.str(), this->valueStr, "* ");
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  DT::TimeInterval t1(-1, 0.5);
  std::string expected = "-0.500000000000000";
  os << t1;
  p1 = this->Compare(os.str(), expected, "* ");
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  t1.SetTimeInterval(0, -1.5);
  os << t1;
  expected = "-1.500000000000000";
  p1 = this->Compare(os.str(), expected, "* ");
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}
