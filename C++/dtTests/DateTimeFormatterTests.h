/*=========================================================================

  Program:   Date Time Library
  File   :   DateTimeFormatterTests.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef DTTESTS_DATETIMEFORMATTERTESTS_H
#define DTTESTS_DATETIMEFORMATTERTESTS_H

#pragma once

#include "CommonTests.h"
#include "DateTimeFormatter.h"

#include <memory>

namespace DT_Tests {

/**
  A test harness to test the DateTimeFormatter class.

  @author Andrew J. P. Maclean
*/
class DateTimeFormatterTests : public CommonTests
{
public:
  DateTimeFormatterTests()
  {
    this->formatter.reset(new DT::DateTimeFormatter());
    this->maxPrec = this->formatter->maximumPrecision;
  }

  virtual ~DateTimeFormatterTests() = default;

public:
  /** Run all the tests.
      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestAll();

  /**
   * Run the test for the FormattedDates.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   *         message.
   */
  TResult TestFormattedDates();

  /** Run the test for formatting as YMDHMS.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestFormatYMDHMS();

  /** Run the test for formatting as JDLong.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestFormatJDLong();

  /** Run the test for formatting a time interval.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestFormatTimeInterval();

  /** Run the test for formatting a time interval.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestFormatTimeIntervalDHMS();

  /** Run the test for formatting the day of the year.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestFormatDOY();

  /** Run the test for formatting an ISO WeekDate.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestFormatISOWeek();

  /**
   * Run the test for all the other formatting options.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   *         message.
   */
  TResult TestOtherFormatting();

private:
  std::unique_ptr<DT::DateTimeFormatter> formatter;

  unsigned int maxPrec;
};

} // End namespace DT_Tests.

#endif // DTTESTS_DATETIMEFORMATTERTESTS_H
