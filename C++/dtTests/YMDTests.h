/*=========================================================================

  Program:   Date Time Library
  File   :   YMDTests.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef DTTESTS_YMDTESTS_H
#define DTTESTS_YMDTESTS_H

#pragma once

#include "CommonTests.h"
#include "YMD.h"

#include <memory>

namespace DT_Tests {

/**
  A test harness to test the YMD class.

  @author Andrew J. P. Maclean
*/
class YMDTests : public CommonTests
{
public:
  YMDTests() : year(2013), month(1), day(1), valueStr("2013-01-01")
  {
    this->ymd.reset(new DT::YMD(this->year, this->month, this->day));
  }

  virtual ~YMDTests() = default;

public:
  /** Run all the tests.
      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestAll();

  /** Run the tests for the constructors.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestConstructors();

  /** Run the tests for setting the members.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestSet();

  /** Run the tests for the comparison operators.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestComparisonOperators();

  /** Run the test for assignment.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestAssignment();

  /** Run the test for string output.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestStringOutput();

private:
  std::unique_ptr<DT::YMD> ymd;
  int year;
  int month;
  int day;
  std::string valueStr;
};

} // End namespace DT_Tests.

#endif // DTTESTS_YMDTESTS_H
