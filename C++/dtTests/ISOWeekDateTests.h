/*=========================================================================

  Program:   Date Time Library
  File   :   ISOWeekDateTests.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef DTTESTS_ISOWEEKDATETESTS_H
#define DTTESTS_ISOWEEKDATETESTS_H

#pragma once

#include "CommonTests.h"
#include "ISOWeekDate.h"

#include <memory>

namespace DT_Tests {

/**
  A test harness to test the ISOWeekDate class.

  @author Andrew J. P. Maclean
*/
class ISOWeekDateTests : public CommonTests
{
public:
  ISOWeekDateTests()
    : year(2009), weekNumber(53), weekDay(4), valueStr("2009-W53-4")
  {
    this->iwd.reset(
        new DT::ISOWeekDate(this->year, this->weekNumber, this->weekDay));
  }

  virtual ~ISOWeekDateTests() = default;

public:
  /** Run all the tests.
      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestAll();

  /** Run the tests for the constructors.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestConstructors();

  /** Run the tests for setting the members.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestSet();

  /** Run the tests for the comparison operators.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestComparisonOperators();

  /** Run the test for assignment.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestAssignment();

  /** Run the test for string output.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestStringOutput();

private:
  std::unique_ptr<DT::ISOWeekDate> iwd;
  int year;
  int weekNumber;
  int weekDay;
  std::string valueStr;
};

} // End namespace DT_Tests.

#endif // DTTESTS_ISOWEEKDATETESTS_H
