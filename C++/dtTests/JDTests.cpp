/*=========================================================================

  Program:   Date Time Library
  File   :   JDTests.cpp

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "JDTests.h"

using namespace DT_Tests;

CommonTests::TResult JDTests::TestAll()
{
  std::vector<TResult> messages{
      {this->TestConstructors()},         {this->TestSet()},
      {this->TestComparisonOperators()},  {this->TestAssignment()},
      {this->TestIncrementAndAddition()}, {this->TestDecrementAndSubtraction()},
      {this->TestNormalising()},          {this->TestStringOutput()}};

  return this->AggregateResults("JD Tests.", messages);
}

CommonTests::TResult JDTests::TestConstructors()
{
  TResult p = this->MakeHeader("Testing constructors.", 2);

  p.second += "* JD() ";
  DT::JD jdl;
  auto p1 = this->PassFail(jdl.GetJDN() == 0 && jdl.GetFoD() == 0);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* JD(jdn,fod) ";
  DT::JD jd1(this->jdn, this->fod);
  p1 = this->PassFail((jd1.GetJDN() == this->jdn && jd1.GetFoD() == this->fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* JD(jd) ";
  DT::JD jd2(jd1);
  p1 = this->PassFail(jd1.GetJDN() == jd2.GetJDN() &&
                      jd1.GetFoD() == jd2.GetFoD());
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult JDTests::TestSet()
{
  TResult p = this->MakeHeader("Testing setting members.", 2);

  p.second += "* SetJD(jdn,fod) ";
  DT::JD jd1(this->jdn, this->fod);
  DT::JD jd2;
  jd2.SetJD(this->jdn, this->fod);
  auto p1 = this->PassFail(jd1 == jd2);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult JDTests::TestComparisonOperators()
{
  TResult p = this->MakeHeader("Testing comparison operators.", 2);

  p.second += "* == ";
  DT::JD jd1(this->jdn, this->fod);
  auto p1 = this->PassFail(jd1 == *this->jd);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* &asymp; ";
  p1 = this->PassFail(this->jd->IsClose(jd1));
  p.first &= p1.first;
  p.second += "\n<br>" + p1.second;

  DT::JD jd2(this->jdn, this->fod - 0.001);
  p1 = this->PassFail(this->jd->IsClose(jd2, 0.01));
  p.first &= p1.first;
  p.second += "<br>" + p1.second;

  // Expect a fail here
  p1 = this->PassFail(!this->jd->IsClose(jd2, 0.0001));
  p.first &= p1.first;
  p.second += "<br>" + p1.second;

  p.second += "* != ";
  jd1.SetFoD(jd1.GetFoD() - 0.2);
  p1 = this->PassFail(jd1 != *this->jd);
  p.first &= p1.first;
  p.second += p1.second;

  DT::JD a(2456553, 0.53308796);
  DT::JD b(2456554, 0.53308796);
  DT::JD c(2456553, 0.53308797);

  p.second += "* &lt; and &le; ";
  p1 = this->PassFail(a < b && a < c && a <= b && a <= c && a <= a &&
                      !(a > b) && !(a >= b));
  p.first &= p1.first;
  p.second += p1.second;

  DT::JD e(2456553, 0.53308796);
  DT::JD f(2456552, 0.53308796);
  DT::JD g(2456553, 0.53308795);

  p.second += "* &gt; and &ge; ";
  p1 = this->PassFail(e > f && e > g && e >= f && e >= g && e >= e &&
                      !(e < f) && !(e <= f));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult JDTests::TestAssignment()
{
  TResult p = this->MakeHeader("Testing assignment.", 2);

  p.second += "* = ";
  auto jd1 = *this->jd;
  auto p1 = this->PassFail(jd1 == *jd);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult JDTests::TestIncrementAndAddition()
{

  TResult p = this->MakeHeader("Testing increment and addition.", 2);

  p.second += "* jd1 += jd2 ";
  DT::JD jd1(this->jdn, this->fod);
  DT::JD jd2;
  auto fd = 15 / 86400.0;
  jd2.SetJD(1, fd); // One day and 15s
  DT::JD jd3(jd1);
  jd3 += jd2;
  DT::JD jd4(this->jdn + 1, this->fod + fd);
  auto p1 = this->PassFail(jd3.IsClose(jd4, this->rel_fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* jd3 += days ";
  double days = 1 + fd;
  jd3 = jd1;
  jd3 += days;
  p1 = this->PassFail(jd3.IsClose(jd4, this->rel_fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* jd1 + jd2 ";
  jd3 = jd1 + jd2;
  p1 = this->PassFail(jd3.IsClose(jd4, this->rel_fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult JDTests::TestDecrementAndSubtraction()
{

  TResult p = this->MakeHeader("Testing decrement and subtraction.", 2);

  p.second += "* jd1 -= jd2 ";
  DT::JD jd1(this->jdn, this->fod);
  DT::JD jd2;
  double fd = 15 / 86400.0;
  jd2.SetJD(1, fd); // One day and 15s
  DT::JD jd3(jd1);
  jd3 -= jd2;
  DT::JD jd4(this->jdn - 1, this->fod - fd);
  auto p1 = this->PassFail(jd3.IsClose(jd4, this->rel_fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* jd3 -= days ";
  double days = 1 + fd;
  jd3 = jd1;
  jd3 -= days;
  p1 = this->PassFail(jd3.IsClose(jd4, this->rel_fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* jd1 - jd2 ";
  jd3 = jd1 - jd2;
  p1 = this->PassFail(jd3.IsClose(jd4, this->rel_fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult JDTests::TestNormalising()
{
  TResult p = this->MakeHeader("Testing normalising.", 2);

  p.second += "* JD(0,-0.5) stored as jdn = -1, fod = 0.5 ";
  auto jdl = DT::JD(0, -0.5);
  auto p1 = this->PassFail(jdl.GetJDN() == -1 && jdl.GetFoD() == 0.5);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* JD(-1,0.5) stored as jdn = -1, fod = 0.5 ";
  jdl = DT::JD(-1, 0.5);
  p1 = this->PassFail(jdl.GetJDN() == -1 && jdl.GetFoD() == 0.5);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* JD(0,-1.5) stored as jdn = -2, fod = 0.5 ";
  jdl = DT::JD(0, -1.5);
  p1 = this->PassFail(jdl.GetJDN() == -2 && jdl.GetFoD() == 0.5);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult JDTests::TestStringOutput()
{
  TResult p = this->MakeHeader("Testing <<.", 2);

  std::ostringstream os;
  os << *this->jd;
  auto p1 = this->Compare(os.str(), this->valueStr, "* ");
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  // Here we have -1 and +0.5 = -1.5
  DT::JD j1(-1, 0.5);
  std::string expected = "-1.500000000000000";
  os << j1;
  p1 = this->Compare(os.str(), expected, "* ");
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  // Here we have -1 and -0.5 = -2.5
  j1.SetJD(0, -1.5);
  os << j1;
  expected = "-2.500000000000000";
  p1 = this->Compare(os.str(), expected, "* ");
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}
