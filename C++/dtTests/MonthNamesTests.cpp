/*=========================================================================

  Program:   Date Time Library
  File   :   MonthNamesTests.cpp

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "MonthNamesTests.h"

using namespace DT_Tests;

CommonTests::TResult MonthNamesTests::TestAll()
{
  std::vector<TResult> messages{{this->TestConstructors()},
                                {this->TestAssignment()},
                                {this->TestComparisonOperators()},
                                {this->TestMonthNames()}};

  return this->AggregateResults("MonthNames Tests.", messages);
}

CommonTests::TResult MonthNamesTests::TestConstructors()
{
  TResult p = this->MakeHeader("Testing constructors.", 2);

  p.second += "* MonthNames() ";
  auto m1(*this->mn);
  auto p1 = this->PassFail(m1 == *this->mn);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult MonthNamesTests::TestAssignment()
{
  TResult p = this->MakeHeader("Testing assignment.", 2);

  p.second += "* = ";
  auto m1 = *this->mn;
  auto p1 = this->PassFail(m1 == *this->mn);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult MonthNamesTests::TestComparisonOperators()
{
  TResult p = this->MakeHeader("Testing comparison operators.", 2);

  p.second += "* == ";
  auto mn1(*this->mn);
  auto p1 = this->PassFail(mn1 == *this->mn);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* != ";
  mn1.SetMonthName(1, "FIIK");
  p1 = this->PassFail(mn1 != *this->mn);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult MonthNamesTests::TestMonthNames()
{
  TResult p = this->MakeHeader("Testing month names.", 2);

  TResult p1(true, "");
  std::string fail = "";
  auto ok = true;

  p.second += "* Month names and abbreviations.\n";
  auto abbrevLength = 3;
  for (int i = 1; i < 13; ++i)
  {
    auto name = this->mn->GetMonthName(i);
    auto abbrev = this->mn->GetAbbreviatedMonthName(i, abbrevLength);
    if (name.substr(0, abbrevLength) != abbrev)
    {
      ok = false;
      fail += "  - (" + name + ", " + abbrev + ")\n";
    }
  }
  std::ostringstream os;
  if (ok)
  {
    os << "<br>Passed." << std::endl;
    p1.first = true;
    p1.second = os.str();
  }
  else
  {
    os << "<br>Failed month names:\n" << fail << std::endl;
    p1.first = false;
    p1.second = os.str();
  }
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  ok = true;
  fail.clear();
  p.second += "* Month names and unabbreviated names.\n";
  for (int i = 1; i < 13; ++i)
  {
    auto name = this->mn->GetMonthName(i);
    auto abbrev = this->mn->GetAbbreviatedMonthName(i, name.size());
    if (name != abbrev)
    {
      ok = false;
      fail += "  - (" + name + ", " + abbrev + ")\n";
    }
  }
  if (ok)
  {
    os << "<br>Passed." << std::endl;
    p1.first = true;
    p1.second = os.str();
  }
  else
  {
    os << "<br>Failed month names:\n" << fail << std::endl;
    p1.first = false;
    p1.second = os.str();
  }
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* Month Name range error.\n";
  p1 = this->PassFail(this->mn->GetMonthName(0).empty() &&
                      this->mn->GetMonthName(13).empty());
  p.first &= p1.first;
  p.second += "<br>" + p1.second;

  p.second += "* Abbreviated Month Name range error.\n";
  p1 = this->PassFail(
      this->mn->GetAbbreviatedMonthName(0, abbrevLength).empty() &&
      this->mn->GetAbbreviatedMonthName(13, abbrevLength).empty());
  p.first &= p1.first;
  p.second += "<br>" + p1.second;

  p.second += "* Set Month Name.\n";
  auto MonthName = this->mn->GetMonthName(5);
  auto newName = "Month 5";
  this->mn->SetMonthName(5, newName);
  p1 = this->PassFail(this->mn->GetMonthName(5) == newName);
  this->mn->SetMonthName(5, MonthName);
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}
