/*=========================================================================

  Program:   Date Time Library
  File   :   JDTests.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef DTTESTS_JDTESTS_H
#define DTTESTS_JDTESTS_H

#pragma once

#include "CommonTests.h"
#include "DateTimeBase.h"
#include "JD.h"

#include <memory>

namespace DT_Tests {

/**
  A test harness to test the JD class.

  @author Andrew J. P. Maclean
*/
class JDTests : public CommonTests, DT::DateTimeBase
{
public:
  JDTests() : jdn(2525008), fod(0.5), valueStr(" 2525008.500000000000000")
  {
    this->jd.reset(new DT::JD(jdn, fod));
  }

  virtual ~JDTests() = default;

public:
  /** Run all the tests.
      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestAll();

  /** Run the tests for the constructors.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestConstructors();

  /** Run the tests for setting the members.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestSet();

  /** Run the tests for the comparison operators.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestComparisonOperators();

  /** Run the test for assignment.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestAssignment();

  /** Run the test for normalisation.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  /** Run the tests for the increment and addition operators.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestIncrementAndAddition();

  /** Run the tests for the decrement and subtraction operators.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestDecrementAndSubtraction();

  /** Run the tests for normalisation.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestNormalising();

  /** Run the test for string output.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestStringOutput();

private:
  std::unique_ptr<DT::JD> jd;
  int jdn;
  double fod;

  std::string valueStr;
};

} // End namespace DT_Tests.

#endif // DTTESTS_JDTESTS_H
