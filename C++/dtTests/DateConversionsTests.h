/*=========================================================================

  Program:   Date Time Library
  File   :   DateConversionsTests.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef DTTESTS_DATECONVERSIONSTESTS_H
#define DTTESTS_DATECONVERSIONSTESTS_H

#pragma once

#include "CommonTests.h"
#include "DateConversions.h"

#include <memory>

namespace DT_Tests {

/**
  A test harness to test the DateConversions class.

  @author Andrew J. P. Maclean
*/
class DateConversionsTests : public CommonTests
{
public:
  DateConversionsTests()
  {
    this->dc.reset(new DT::DateConversions());
  }

  virtual ~DateConversionsTests() = default;

public:
  /** Run all the tests.
      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestAll();

  /** Run the proleptic Julian calendar tests for some specified dates.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestProlepticJulianCalendar();

  /** Run the proleptic Gregorian calendar tests for some specified dates.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestProlepticGregorianCalendar();

  /** Run the Julian/Gregorian tests for some specified dates.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestJulianGregorianCalendar();

  /** Test the day of the week for some dates.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestDayOfTheWeek();

  /** Run the tests for testing the day of the year.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestDayOfTheYear();

  /** Run the tests for leap years.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestLeapYear();

  /** Run the tests for the week of the year conversions.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestISOWeekDate();

private:
  /** Test the conversion of calendar date to julian day.
      The Julian Proleptic Calendar is used.

      @param jd the expected julian day for the given year, month and day.
      @param date the date.

      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestCD2JDJ(int const& jd, DT::YMD const& ymd);

  /** Test the conversion of julian day to calendar date.
      The Julian Proleptic Calendar is used.

      @param jd the julian day.
      @param ymd the date.

      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestJD2CDJ(int const& jd, DT::YMD const& ymd);

  /** Test the conversion of calendar date to julian day.
      The Gregorian Proleptic Calendar is used.

      @param jd the expected julian day for the given year, month and day.
      @param ymd the date.

      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestCD2JDG(int const& jd, DT::YMD const& ymd);

  /** Test the conversion of julian day to calendar date.
      The Gregorian Proleptic Calendar is used.

      @param jd the julian day.
      @param ymd the date.

      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestJD2CDG(int const& jd, DT::YMD const& ymd);

  /** Test the conversion of calendar date to julian day.
      The Gregorian or Julian Calendar is used as appropriate.

      @param jd the expected julian day for the given year, month and day.
      @param ymd the date.

      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestCD2JDJG(int const& jd, DT::YMD const& ymd);

  /** Test the conversion of julian day to calendar date.
      The Gregorian or Julian Calendar is used as appropriate.

      @param jd the julian day.
      @param ymd the date.

      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestJD2CDJG(int const& jd, DT::YMD const& ymd);

  /** Test the conversion of calendar date to ISO week of the year.
   *
   * The Gregorian Calendar is used.
   *
   * @param ymd The year, month and day.
   * @param iwd The ISO Week Date.
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult Testcd2woy(DT::YMD const& ymd, DT::ISOWeekDate const& iwd);

private:
  std::unique_ptr<DT::DateConversions> dc;
};

} // End namespace DT_Tests.

#endif // DTTESTS_DATECONVERSIONSTESTS_H
