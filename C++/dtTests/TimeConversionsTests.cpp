/*=========================================================================

  Program:   Time Time Library
  File   :   TimeConversionsTests.cpp

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "TimeConversionsTests.h"

using namespace DT_Tests;
using namespace std::placeholders; // for _1, _2, _3...

CommonTests::TResult TimeConversionsTests::TestAll()
{
  std::vector<TResult> messages = {this->TestTime()};

  return this->AggregateResults("TimeConversions Tests.", messages);
}

CommonTests::TResult TimeConversionsTests::Testhms2hr(double const& hr,
                                                      DT::HMS const& hms)
{
  TResult res;
  auto h = this->tc->hms2h(hms);
  if (!this->isclose(hr, h, this->rel_fod))
  {
    std::ostringstream os;
    os << "<br>Fail (hms2h), got: " << std::fixed << std::setprecision(12) << h
       << " expected: " << hr << std::endl;
    res.first = false;
    res.second = os.str();
    return res;
  }
  res.first = true;
  res.second = "<br>Passed (hms2h)\n";
  return res;
}

CommonTests::TResult TimeConversionsTests::Testhr2hms(double const& hr,
                                                      DT::HMS const& hms)
{
  TResult res;
  auto thms = this->tc->h2hms(hr);
  if (!thms.IsClose(hms, this->rel_seconds))
  {
    std::ostringstream os;
    os << "<br>Fail (h2hms), got: " << thms << " expected: " << hms
       << std::endl;
    res.first = false;
    res.second = os.str();
    return res;
  }
  res.first = true;
  res.second = "<br>Passed (h2hms)\n";
  return res;
}

CommonTests::TResult TimeConversionsTests::Testhm2hr(double const& hr,
                                                     DT::HM const& hm)
{
  TResult res;
  auto h = this->tc->hm2h(hm);
  if (!this->isclose(hr, h, this->rel_fod))
  {
    std::ostringstream os;
    os << "<br>Fail (hm2h), got: " << std::fixed << std::setprecision(12) << h
       << " expected: " << hr << std::endl;
    res.first = false;
    res.second = os.str();
    return res;
  }
  res.first = true;
  res.second = "<br>Passed (hm2h)\n";
  return res;
}

CommonTests::TResult TimeConversionsTests::Testhr2hm(double const& hr,
                                                     DT::HM const& hm)
{
  TResult res;
  DT::TimeConversions t;
  auto thm = this->tc->h2hm(hr);
  if (!thm.IsClose(hm, this->rel_minutes))
  {
    std::ostringstream os;
    os << "<br>Fail (h2hm), got: " << thm << " expected: " << hm << std::endl;
    res.first = false;
    res.second = os.str();
    return res;
  }
  res.first = true;
  res.second = "<br>Passed (h2hm)\n";
  return res;
}

CommonTests::TResult TimeConversionsTests::TestTime()
{
  TResult p = this->MakeHeader("Testing times.", 2);

  auto f1 = std::bind(&TimeConversionsTests::Testhms2hr, this, _1, _2);
  auto f2 = std::bind(&TimeConversionsTests::Testhr2hms, this, _1, _2);
  auto f3 = std::bind(&TimeConversionsTests::Testhm2hr, this, _1, _2);
  auto f4 = std::bind(&TimeConversionsTests::Testhr2hm, this, _1, _2);

  p.second += "* h=23.7625, 23:45:45\n";
  DT::HMS hms(23, 45, 45);
  auto p1 = this->TestF1F2<double, DT::HMS>(23.7625, hms, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* h=-23.7625, -23:45:45\n";
  hms.hour = -hms.hour;
  p1 = this->TestF1F2<double, DT::HMS>(-23.7625, hms, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* h=-0.01, -00:00:36\n";
  hms.SetHMS(0, 0, -36);
  p1 = this->TestF1F2<double, DT::HMS>(-0.01, hms, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  // Testing precision and minutes/seconds rollover.
  p.second += "* h=23.7625, 22:104:105.0 == 23:45:45\n";
  hms.SetHMS(22, 104, 105);
  hms = this->tc->h2hms(this->tc->hms2h(hms));
  p1 = this->TestF1F2<double, DT::HMS>(23.7625, hms, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* h=23.7625, 23:44:105.0 == 23:45:45\n";
  hms.SetHMS(23, 44, 105);
  hms = this->tc->h2hms(this->tc->hms2h(hms));
  p1 = this->TestF1F2<double, DT::HMS>(23.7625, hms, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;
  p.second += "* h=23.7625, 22:105:45.0 == 23:45:45\n";
  hms.SetHMS(22, 105, 45);
  hms = this->tc->h2hms(this->tc->hms2h(hms));
  p1 = this->TestF1F2<double, DT::HMS>(23.7625, hms, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;
  p.second += "* h=-23.7625, -22:104:105.0 == -23:45:45\n";
  hms.SetHMS(-22, 104, 105);
  hms = this->tc->h2hms(this->tc->hms2h(hms));
  p1 = this->TestF1F2<double, DT::HMS>(-23.7625, hms, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;
  p.second += "* h=-23.7625, -23:44:105.0 == -23:45:45\n";
  hms.SetHMS(-23, 44, 105);
  hms = this->tc->h2hms(this->tc->hms2h(hms));
  p1 = this->TestF1F2<double, DT::HMS>(-23.7625, hms, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;
  p.second += "* h=-23.7625, -22:105:45.0 == -23:45:45\n";
  hms.SetHMS(-22, 105, 45);
  hms = this->tc->h2hms(this->tc->hms2h(hms));
  p1 = this->TestF1F2<double, DT::HMS>(-23.7625, hms, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  // Testing HM
  p.second += "* h=23.7625, 23:45.75\n";
  DT::HM hm(23, 45.75);
  p1 = this->TestF1F2<double, DT::HM>(23.7625, hm, f3, f4);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* h=-23.7625, -23:45.75\n";
  hm.hour = -hm.hour;
  p1 = this->TestF1F2<double, DT::HM>(-23.7625, hm, f3, f4);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* h=00.7625, 00:45.75\n";
  hm.hour = 0;
  hm.minute = 45.75;
  p1 = this->TestF1F2<double, DT::HM>(0.7625, hm, f3, f4);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* h=-00.7625, -00:45.75\n";
  hm.minute = -hm.minute;
  p1 = this->TestF1F2<double, DT::HM>(-0.7625, hm, f3, f4);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  // Testing precision and minutes rollover.
  p.second += "* h=1.0125, 00:60.75 == 01:00.75\n";
  hm.SetHM(0, 60.75);
  hm = this->tc->h2hm(this->tc->hm2h(hm));
  p1 = this->TestF1F2<double, DT::HM>(1.0125, hm, f3, f4);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* h=1.016665, 00:60.9999 == 01:00.9999\n";
  hm.SetHM(0, 60.9999);
  hm = this->tc->h2hm(this->tc->hm2h(hm));
  p1 = this->TestF1F2<double, DT::HM>(1.016665, hm, f3, f4);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* h=-1.0125, -00:60.75 == -01:00.75\n";
  hm.SetHM(0, -60.75);
  hm = this->tc->h2hm(this->tc->hm2h(hm));
  p1 = this->TestF1F2<double, DT::HM>(-1.0125, hm, f3, f4);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "\n";
  return p;
}
