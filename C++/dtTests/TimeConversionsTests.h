/*=========================================================================

  Program:   Time Time Library
  File   :   TimeConversionsTests.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef DTTESTS_TIMECONVERSIONSTESTS_H
#define DTTESTS_TIMECONVERSIONSTESTS_H

#pragma once

#include "CommonTests.h"
#include "DateTimeBase.h"
#include "TimeConversions.h"

#include <memory>

namespace DT_Tests {

/**
  A test harness to test the TimeConversions class.

  @author Andrew J. P. Maclean
*/
class TimeConversionsTests : public CommonTests, DT::DateTimeBase
{
public:
  TimeConversionsTests()
  {
    this->tc.reset(new DT::TimeConversions());
  }

  virtual ~TimeConversionsTests() = default;

public:
  /** Run all the tests.
      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestAll();

  /** Run the tests for the time conversions.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestTime();

private:
  /** Test the conversion of hours minutes and seconds to hours.
      @param hr the expected hour of the day.
      @param time the time.

      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult Testhms2hr(double const& hr, DT::HMS const& hms);

  /** Test the conversion of hours to hours minutes and seconds.
      @param hr the hour of the day.
      @param time the expected time.

      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult Testhr2hms(double const& hr, DT::HMS const& hms);

  /** Test the conversion of hours and minutes to hours.
      @param hr the expected hour of the day.
      @param time the time.

      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult Testhm2hr(double const& hr, DT::HM const& hm);

  /** Test the conversion of hours of the day to hours and minutes.
      @param hr the hour of the day.
      @param time the expected time.

      @return A pair consisting of true (if the test passed)
       or false and a message.
  */

  TResult Testhr2hm(double const& hr, DT::HM const& hm);

private:
  std::unique_ptr<DT::TimeConversions> tc;
};

} // End namespace DT_Tests.

#endif // DTTESTS_TIMECONVERSIONSTESTS_H
