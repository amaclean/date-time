/*=========================================================================

  Program:   Date Time Library
  File   :   NonCopyableTests.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef DTTESTS_NONCOPYABLETEST_H
#define DTTESTS_NONCOPYABLETEST_H

#pragma once

namespace DT_Tests {
/**
  The test classes inherit from this class.

  Essentially it just ensures that classes derived from it are non-copyable.

  @author Andrew J. P. Maclean
*/
class NonCopyableTest
{
public:
  NonCopyableTest() = default;
  virtual ~NonCopyableTest() = default;

private:
  // Disable copy constructor and assignment.
  NonCopyableTest(NonCopyableTest const&) = delete;
  NonCopyableTest& operator=(NonCopyableTest const& x) = delete;
};
} // End namespace DT_Tests.
#endif // DTTESTS_NONCOPYABLETEST_H
