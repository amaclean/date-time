/*=========================================================================

  Program:   Date Time Library
  File   :   TimeZonesTests.cpp

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "TimeZonesTests.h"

using namespace DT_Tests;

CommonTests::TResult TimeZonesTests::TestAll()
{
  std::vector<TResult> messages{{this->TestConstructors()},
                                {this->TestAssignment()},
                                {this->TestGet()},
                                {this->TestComparisonOperators()},
                                {this->TestTimeZones()}};

  return this->AggregateResults("TimeZones Tests.", messages);
}

CommonTests::TResult TimeZonesTests::TestConstructors()
{
  TResult p = this->MakeHeader("Testing constructors.", 2);

  p.second += "* tz() ";
  auto tz1(*this->tz);
  auto p1 = this->PassFail(tz1 == *this->tz);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult TimeZonesTests::TestAssignment()
{
  TResult p = this->MakeHeader("Testing assignment.", 2);

  p.second += "* = ";
  auto tz1 = *this->tz;
  auto p1 = this->PassFail(tz1 == *this->tz);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult TimeZonesTests::TestGet()
{
  TResult p = this->MakeHeader("Testing getting members.", 2);

  p.second += "* GetTimeZoneName() ";
  std::string name;
  auto p1 = this->PassFail(this->tz->GetTimeZoneName(0, name));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* GetTimeZoneName() ";
  std::string name1;
  p1 = this->PassFail(!this->tz->GetTimeZoneName(12345, name1));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* GetTimeZoneValue() ";
  int value = -1;
  p1 = this->PassFail(this->tz->GetTimeZoneValue(name, value));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* GetTimeZoneValue() ";
  value = -1;
  p1 = this->PassFail(!this->tz->GetTimeZoneValue("FIIK", value));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult TimeZonesTests::TestComparisonOperators()
{
  TResult p = this->MakeHeader("Testing comparison operators.", 2);

  p.second += "* == ";
  auto tz1(*this->tz);
  auto p1 = this->PassFail(tz1 == *this->tz);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* != ";
  p1 = this->PassFail(!(tz1 != *this->tz));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult TimeZonesTests::TestTimeZones()
{
  TResult p = this->MakeHeader("Testing time zones.", 2);

  TResult p1(true, "");

  p.second += "* Size of the time zone maps.\n";
  auto TZMap = *this->tz->GetTimeZonesByName();
  auto invTZMap = *this->tz->GetTimeZonesByValue();
  std::ostringstream os;
  if (!TZMap.empty() && (TZMap.size() == invTZMap.size()))
  {
    os << "<br>Passed." << std::endl;
    p1.first = true;
    p1.second = os.str();
  }
  else
  {
    os << "<br>Failed: The maps returned by GetTimeZonesByName() and "
       << "GetTimeZonesByValue() should be the same size and > 0."
       << "\n<br>The sizes are: " << TZMap.size() << " and: " << invTZMap.size()
       << " respectively." << std::endl;
    p1.first = false;
    p1.second = os.str();
  }
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  std::string fail = "";
  if (!TZMap.empty() && TZMap.size() == invTZMap.size())
  {
    auto ok = true;
    p.second += "* Indexing of the time zone maps.\n";
    for (const auto& q : TZMap)
    {
      if (invTZMap[TZMap[q.first]] != q.first)
      {
        ok = false;
        fail += "  - (" + q.first + ", " + invTZMap[TZMap[q.first]] + ")\n";
      }
    }
    if (ok)
    {
      os << "<br>Passed." << std::endl;
      p1.first = true;
      p1.second = os.str();
    }
    else
    {
      os << "<br>Failed, here is a list of (Expected Index, Actual Index): \n"
         << fail << std::endl;
      p1.first = false;
      p1.second = os.str();
    }

    os.str("");
    p.first &= p1.first;
    p.second += p1.second;
  }

  p.second += "\n";
  return p;
}
