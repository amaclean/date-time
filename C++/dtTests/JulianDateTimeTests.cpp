/*=========================================================================

  Program:   Date Time Library
  File   :   JulianDateTimeTests.cpp

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "JulianDateTimeTests.h"
#include "DayNames.h"
#include "TimeZones.h"

using namespace DT_Tests;
using namespace std::placeholders; // for _1, _2, _3...

CommonTests::TResult JulianDateTimeTests::TestAll()
{
  std::vector<TResult> messages{
      {this->TestConstructors()},
      {this->TestSet()},
      {this->TestComparisonOperators()},
      {this->TestAssignment()},
      {this->TestIncrementAndAddition()},
      {this->TestDecrementAndSubtraction()},

      {this->TestEphemerisDates()},
      {this->TestDoublePrecision(DT::CALENDAR_TYPE::JULIAN)},
      {this->TestDoublePrecision(DT::CALENDAR_TYPE::GREGORIAN)},
      {this->TestDoublePrecision(DT::CALENDAR_TYPE::JULIAN_GREGORIAN)},

      {this->TestDayOfTheYear()},
      {this->TestDayOfTheWeek()},
      {this->TestISOWeekDate()},

      {this->TestTime()},

      {this->TestTimeZone()},

      {this->TestJulianDates()}};

  return this->AggregateResults("JulianDateTime Tests.", messages);
}

CommonTests::TResult
JulianDateTimeTests::TestCD2JDJ(double const& jd,
                                DT::JulianDateTime const& date)
{
  DT::YMD ymd = date.GetDate(DT::CALENDAR_TYPE::JULIAN);
  // Note: We need to round up because the calendrical calculation returns the
  // JD at midday.
  auto ref = static_cast<int>(std::ceil(jd));
  auto tjd = date.cd2jdJ(ymd);
  return this->Compare(tjd, ref, "<br>cd2jdJ");
}

CommonTests::TResult
JulianDateTimeTests::TestJD2CDJ(double const& jd,
                                DT::JulianDateTime const& date)
{
  // Note: We need to round up because the calendrical calculation returns the
  // JD at midday.
  auto ymd1 = date.jd2cdJ(static_cast<int>(std::ceil(jd)));
  auto ymd2 = date.GetDate(DT::CALENDAR_TYPE::JULIAN);
  return this->Compare(ymd1, ymd2, "<br>jd2cdJ");
}

CommonTests::TResult
JulianDateTimeTests::TestCD2JDG(double const& jd,
                                DT::JulianDateTime const& date)
{
  auto ymd = date.GetDate(DT::CALENDAR_TYPE::GREGORIAN);
  // Note: We need to round up because the calendrical calculation returns the
  // JD at midday.
  auto ref = static_cast<int>(std::ceil(jd));
  auto tjd = date.cd2jdG(ymd);
  return this->Compare(tjd, ref, "<br>cd2jdG");
}

CommonTests::TResult
JulianDateTimeTests::TestJD2CDG(double const& jd,
                                DT::JulianDateTime const& date)
{
  // Note: We need to round up because the calendrical calculation returns the
  // JD at midday.
  auto ymd1 = date.jd2cdG(static_cast<int>(std::ceil(jd)));
  auto ymd2 = date.GetDate(DT::CALENDAR_TYPE::GREGORIAN);
  return this->Compare(ymd1, ymd2, "<br>jd2cdG");
}

CommonTests::TResult
JulianDateTimeTests::TestGetDateJD(double const& jd,
                                   DT::JulianDateTime const& date)
{
  TResult res;
  auto jdpf = date.GetJDFullPrecision();
  if (this->isclose(jdpf.GetJDDouble(), jd, this->rel_fod))
  {
    res.first = true;
    res.second = "<br>Passed (GetJDFullPrecision())\n";
  }
  else
  {
    std::ostringstream os;
    os << "<br>Fail (GetJDFullPrecision()) got: " << std::fixed
       << jdpf.GetJDDouble() << ", expected: " << jd << std::endl;
    res.first = false;
    res.second = os.str();
  }
  return res;
}

CommonTests::TResult
JulianDateTimeTests::TestGetDateCT(double const& jd,
                                   DT::JulianDateTime const& date,
                                   DT::CALENDAR_TYPE const& CALENDAR_TYPE)
{
  TResult res;
  auto ymdhms1 = date.GetDateTime(CALENDAR_TYPE);
  DT::JulianDateTime d;
  d.SetDateTime(jd);
  auto ymdhms2 = d.GetDateTime(CALENDAR_TYPE);
  if (ymdhms1.IsClose(ymdhms2, this->rel_seconds))
  {
    res.first = true;
    res.second = "<br>Passed (GetDateTime())\n";
  }
  else
  {
    std::ostringstream os;
    os << "<br>Fail (GetDateTime()) got: " << ymdhms1
       << ", expected: " << ymdhms2 << std::endl;
    res.first = false;
    res.second = os.str();
  }
  return res;
}

CommonTests::TResult
JulianDateTimeTests::Testhms2hr(double const& hr,
                                DT::JulianDateTime const& time)
{
  TResult res;
  auto hms = time.GetTimeHMS();
  auto h = time.hms2h(hms);
  if (this->isclose(hr, h, this->rel_seconds))
  {
    res.first = true;
    res.second = "<br>Passed (hms2h)\n";
  }
  else
  {
    std::ostringstream os;
    os << "<br>Fail (hms2h) got: " << std::fixed << std::setprecision(12) << h
       << ", expected: " << hr << std::endl;
    res.first = false;
    res.second = os.str();
  }
  return res;
}

CommonTests::TResult
JulianDateTimeTests::Testhr2hms(double const& hr,
                                DT::JulianDateTime const& time)
{
  TResult res;
  DT::JulianDateTime t;
  auto thms = t.h2hms(hr);
  auto hms = time.GetTimeHMS();
  if (hms.IsClose(thms, this->rel_seconds))
  {
    res.first = true;
    res.second = "<br>Passed (h2hms)\n";
  }
  else
  {
    std::ostringstream os;
    os << "<br>Fail (h2hms) got: " << thms << ", expected: " << hms
       << std::endl;
    res.first = false;
    res.second = os.str();
  }
  return res;
}

CommonTests::TResult
JulianDateTimeTests::Testhm2hr(double const& hr, DT::JulianDateTime const& time)
{
  TResult res;
  auto hm = time.GetTimeHM();
  auto h = time.hm2h(hm);
  if (this->isclose(hr, h, this->rel_seconds))
  {
    res.first = true;
    res.second = "<br>Passed (hm2h)\n";
  }
  else
  {
    std::ostringstream os;
    os << "<br>Fail (hm2h) got: " << std::fixed << std::setprecision(12) << h
       << ", expected: " << hr << std::endl;
    res.first = false;
    res.second = os.str();
  }
  return res;
}

CommonTests::TResult
JulianDateTimeTests::Testhr2hm(double const& hr, DT::JulianDateTime const& time)
{
  TResult res;
  DT::JulianDateTime t;
  auto thm = t.h2hm(hr);
  auto hm = time.GetTimeHM();
  if (hm.IsClose(thm, this->rel_seconds))
  {
    res.first = true;
    res.second = "<br>Passed (h2hm)\n";
  }
  else
  {
    std::ostringstream os;
    os << "<br>Fail (h2hm) got: " << thm << ", expected: " << hm << std::endl;
    res.first = false;
    res.second = os.str();
  }
  return res;
}

CommonTests::TResult JulianDateTimeTests::Testcd2woy(DT::YMD const& ymd,
                                                     DT::ISOWeekDate const& iwd)
{
  TResult res;
  DT::JulianDateTime dt;
  dt.SetDate(ymd, DT::CALENDAR_TYPE::JULIAN_GREGORIAN);
  auto iwd1 = dt.GetISOWeek();
  if (iwd1 == iwd)
  {
    res.first = true;
    res.second = "<br>Passed\n";
  }
  else
  {
    std::ostringstream os;
    os << "<br>Fail (cd2woy) got: " << iwd1 << ", expected: " << iwd
       << std::endl;
    res.first = false;
    res.second = os.str();
  }
  return res;
}

CommonTests::TResult JulianDateTimeTests::TestConstructors()
{
  TResult p = this->MakeHeader("Testing constructors.", 2);

  DT::CALENDAR_TYPE calendar = DT::CALENDAR_TYPE::JULIAN_GREGORIAN;

  p.second += "* DateTime(dt) ";
  DT::YMDHMS ymdhms(2012, 1, 1, 23, 45, 45);
  DT::JulianDateTime d1;
  d1.SetDateTime(ymdhms, calendar);
  DT::JulianDateTime d2(d1);
  auto p1 = this->PassFail(d1 == d2);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* DateTime(jd) ";
  auto jdpf = d1.GetJDFullPrecision();
  DT::JulianDateTime d3(jdpf);
  p1 = this->PassFail(d1 == d3);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* DateTime(jdn, fod) ";
  jdpf = d1.GetJDFullPrecision();
  DT::JulianDateTime d4(jdpf.GetJDN(), jdpf.GetFoD());
  p1 = this->PassFail(d1 == d4);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* DateTime(y, m, d, h, min, s) ";
  DT::JulianDateTime d5(2012, 1, 1, 23, 45, 45, calendar);
  p1 = this->PassFail(d1 == d5);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* DateTime(ymdhms) ";
  DT::JulianDateTime d6(ymdhms, calendar);
  p1 = this->PassFail(d1 == d6);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult JulianDateTimeTests::TestSet()
{
  TResult p = this->MakeHeader("Testing SetDateTime()/SetDate().", 2);

  DT::CALENDAR_TYPE calendar = DT::CALENDAR_TYPE::JULIAN_GREGORIAN;

  p.second += "* SetDateTime(ymdhms) ";
  DT::JulianDateTime d1(2012, 1, 1, 23, 45, 45, calendar);
  DT::YMDHMS ymdhms(2012, 1, 1, 23, 45, 45);
  DT::JulianDateTime d2;
  d2.SetDateTime(ymdhms, calendar);
  auto p1 = this->PassFail(d1 == d2);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* SetDateTime(y, m, d, h, min, s) ";
  DT::JulianDateTime d3;
  d3.SetDateTime(2012, 1, 1, 23, 45, 45, calendar);
  p1 = this->PassFail(d1 == d3);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* SetDateTime(jn, fd) ";
  DT::JulianDateTime d4;
  auto jdpf = d1.GetJDFullPrecision();
  d4.SetDateTime(jdpf.GetJDN(), jdpf.GetFoD());
  p1 = this->PassFail(d1 == d4);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* SetDate(ymd) ";
  DT::JulianDateTime d5(2012, 1, 1, 0, 0, 0, calendar);
  DT::YMD ymd(2012, 1, 1);
  DT::JulianDateTime d6;
  d6.SetDate(ymd, calendar);
  p1 = this->PassFail(d5 == d6);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult JulianDateTimeTests::TestComparisonOperators()
{
  TResult p = this->MakeHeader("Testing comparison operators.", 2);

  DT::CALENDAR_TYPE calendar = DT::CALENDAR_TYPE::JULIAN_GREGORIAN;

  p.second += "* == ";
  DT::YMDHMS ymdhms(2012, 1, 1, 23, 45, 45);
  DT::JulianDateTime d1;
  d1.SetDateTime(ymdhms, calendar);
  DT::JulianDateTime d2;
  d2.SetDateTime(ymdhms, calendar);
  auto p1 = this->PassFail(d1 == d2);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* &asymp; ";
  DT::JulianDateTime d3;
  d3.SetDateTime(DT::YMDHMS(2012, 1, 1, 23, 45, 45.1), calendar);
  p1 = this->PassFail(d1.IsClose(d3, 1.0e-5));
  p.first &= p1.first;
  p.second += "\n<br>" + p1.second;

  p1 = this->PassFail(!d1.IsClose(d3, 1.0e-6));
  p.first &= p1.first;
  p.second += "<br>" + p1.second;

  p.second += "* != ";
  ymdhms.second = 45.001;
  d2.SetDateTime(ymdhms, calendar);
  p1 = this->PassFail(d1 != d2);
  p.first &= p1.first;
  p.second += p1.second;

  DT::JulianDateTime a(2456553, 0.53308796);
  DT::JulianDateTime b(2456554, 0.53308796);
  DT::JulianDateTime c(2456553, 0.53308797);

  p.second += "* &lt; and &le; ";
  p1 = this->PassFail(a < b && a < c && a <= b && a <= c && a <= a &&
                      !(a > b) && !(a >= b));
  p.first &= p1.first;
  p.second += p1.second;

  DT::JulianDateTime e(2456553, 0.53308796);
  DT::JulianDateTime f(2456552, 0.53308796);
  DT::JulianDateTime g(2456553, 0.53308795);

  p.second += "* &gt; and &ge; ";
  p1 = this->PassFail(e > f && e > g && e >= f && e >= g && e >= e &&
                      !(e < f) && !(e <= f));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult JulianDateTimeTests::TestAssignment()
{
  TResult p = this->MakeHeader("Testing assignment.", 2);

  DT::CALENDAR_TYPE calendar = DT::CALENDAR_TYPE::JULIAN_GREGORIAN;

  p.second += "* = ";
  DT::YMDHMS ymdhms(2012, 1, 1, 23, 45, 45);
  DT::JulianDateTime d1;
  d1.SetDateTime(ymdhms, calendar);
  auto d2 = d1;
  auto p1 = this->PassFail(d1 == d2);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult JulianDateTimeTests::TestIncrementAndAddition()
{
  TResult p = this->MakeHeader("Testing increment and addition.", 2);

  DT::CALENDAR_TYPE calendar = DT::CALENDAR_TYPE::JULIAN_GREGORIAN;

  p.second += "* dt1 += dt2 ";
  DT::YMDHMS ymdhms(2012, 1, 1, 23, 45, 45);
  DT::JulianDateTime d1;
  d1.SetDateTime(ymdhms, calendar);
  DT::JulianDateTime d2;
  double fod = 15 / 86400.0;
  d2.SetDeltaT(DT::TimeInterval(1, fod)); // One day and 15s
  DT::JulianDateTime d3(d1);
  d3 += d2;
  DT::JulianDateTime d4;
  DT::YMDHMS ymdhms1(2012, 1, 2, 23, 46, 0);
  d4.SetDateTime(ymdhms1, calendar);
  auto p1 = this->PassFail(d3.IsClose(d4, this->rel_fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* dt1 += days ";
  double days = 1 + fod;
  d3 = d1;
  d3 += days;
  p1 = this->PassFail(d3.IsClose(d4, this->rel_fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* dt1 + dt2 ";
  d3 = DT::JulianDateTime(d1 + d2);
  p1 = this->PassFail(d3.IsClose(d4, this->rel_fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult JulianDateTimeTests::TestDecrementAndSubtraction()
{
  TResult p = this->MakeHeader("Testing decrement and subtraction.", 2);

  DT::CALENDAR_TYPE calendar = DT::CALENDAR_TYPE::JULIAN_GREGORIAN;

  p.second += "* dt1 -= dt2 ";
  DT::YMDHMS ymdhms1(2012, 1, 2, 23, 46, 0);
  DT::JulianDateTime d1;
  d1.SetDateTime(ymdhms1, calendar);
  DT::JulianDateTime d2;
  auto fod = 15 / 86400.0;
  d2.SetDeltaT(DT::TimeInterval(1, fod)); // One day and 15s
  DT::JulianDateTime d3(d1);
  d3 -= d2;
  DT::YMDHMS ymdhms(2012, 1, 1, 23, 45, 45);
  DT::JulianDateTime d4;
  d4.SetDateTime(ymdhms, calendar);
  auto p1 = this->PassFail(d3.IsClose(d4, this->rel_fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* dt1 -= days ";
  double days = 1 + fod;
  d3 = d1;
  d3 -= days;
  p1 = this->PassFail(d3.IsClose(d4, this->rel_fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* dt1 - dt2 ";
  d3 = DT::JulianDateTime(d1 - d2);
  p1 = this->PassFail(d3.IsClose(d4, this->rel_fod));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult JulianDateTimeTests::TestEphemerisDates()
{
  TResult p = this->MakeHeader("Testing dates of ephemerides.", 2);

  DT::JulianDateTime date;
  DT::CALENDAR_TYPE calendar = DT::CALENDAR_TYPE::GREGORIAN;

  auto f1 = std::bind(&JulianDateTimeTests::TestCD2JDJ, this, _1, _2);
  auto f2 = std::bind(&JulianDateTimeTests::TestJD2CDJ, this, _1, _2);
  auto f3 = std::bind(&JulianDateTimeTests::TestCD2JDG, this, _1, _2);
  auto f4 = std::bind(&JulianDateTimeTests::TestJD2CDG, this, _1, _2);

  p.second += "* DE413 Start Jd=-3100015.50, -13200-AUG-15 00:00:00\n";
  DT::YMDHMS ymdhms(-13200, 8, 15, 0, 0, 0);
  date.SetDateTime(ymdhms, DT::CALENDAR_TYPE::JULIAN);
  auto p1 =
      this->TestF1F2<double, DT::JulianDateTime>(-3100015.50, date, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* DE413 End Jd=8000002.5, 17191-MAR-01 00:00:00\n";
  ymdhms = DT::YMDHMS(17191, 3, 1, 0, 0, 0);
  date.SetDateTime(ymdhms, DT::CALENDAR_TYPE::GREGORIAN);
  p1 = this->TestF1F2<double, DT::JulianDateTime>(8000002.5, date, f3, f4);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* DE406 Start Jd=625360.5, -3000 FEB 23 00:00:00\n";
  ymdhms = DT::YMDHMS(-3000, 2, 23, 0, 0, 0);
  date.SetDateTime(ymdhms, DT::CALENDAR_TYPE::JULIAN);
  p1 = this->TestF1F2<double, DT::JulianDateTime>(625360.5, date, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* DE406 end. Jd=2816912.5, 3000 MAY 06 00:00:00\n";
  ymdhms = DT::YMDHMS(3000, 5, 6, 0, 0, 0);
  date.SetDateTime(ymdhms, calendar);
  p1 = this->TestF1F2<double, DT::JulianDateTime>(2816912.5, date, f3, f4);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* DE406 end (UNIX.406). Jd=2816976.5, 3000 JUL 09 00:00:00\n";
  ymdhms = DT::YMDHMS(3000, 7, 9, 0, 0, 0);
  date.SetDate(ymdhms, calendar);
  p1 = this->TestF1F2<double, DT::JulianDateTime>(2816976.5, date, f3, f4);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* DE405 Start Jd=2305424.5, 1599 DEC 09 00:00:00\n";
  ymdhms = DT::YMDHMS(1599, 12, 9, 0, 0, 0);
  date.SetDate(ymdhms, calendar);
  p1 = this->TestF1F2<double, DT::JulianDateTime>(2305424.5, date, f3, f4);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* DE405 end. Jd=2525008.5, 2201 FEB 20 00:00:00\n";
  ymdhms = DT::YMDHMS(2201, 2, 20, 0, 0, 0);
  date.SetDate(ymdhms, calendar);
  p1 = this->TestF1F2<double, DT::JulianDateTime>(2525008.5, date, f3, f4);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* DE200 Start Jd=2305424.5, 1599 DEC 09 00:00:00\n";
  ymdhms = DT::YMDHMS(1599, 12, 9, 0, 0, 0);
  date.SetDate(ymdhms, calendar);
  p1 = this->TestF1F2<double, DT::JulianDateTime>(2305424.5, date, f3, f4);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* DE200 end. Jd=2513392.5, 2169 MAY 02 00:00:00\n";
  ymdhms = DT::YMDHMS(2169, 5, 2, 0, 0, 0);
  date.SetDate(ymdhms, calendar);
  p1 = this->TestF1F2<double, DT::JulianDateTime>(2513392.5, date, f3, f4);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult
JulianDateTimeTests::TestDoublePrecision(DT::CALENDAR_TYPE calendar)
{
  TResult p = this->MakeHeader(
      "Testing dates of ephemerides using double precision.", 2);

  switch (calendar)
  {
  case DT::CALENDAR_TYPE::JULIAN:
    p.second += this->MakeHeader("Julian Calendar.", 3).second;
    break;
  case DT::CALENDAR_TYPE::GREGORIAN:
    p.second += this->MakeHeader("Gregorian Calendar.", 3).second;
    break;
  case DT::CALENDAR_TYPE::JULIAN_GREGORIAN:
    p.second += this->MakeHeader("Julian/Gregorian Calendar.", 3).second;
    break;
  default:
    p.first = false;
    p.second += this->MakeHeader("Unknown calendar type.", 3).second;
  }

  auto f1 = std::bind(&JulianDateTimeTests::TestGetDateJD, this, _1, _2);
  auto f2 = std::bind(&JulianDateTimeTests::TestGetDateCT, this, _1, _2, _3);

  DT::JulianDateTime date;
  TResult p1;

  if (calendar == DT::CALENDAR_TYPE::JULIAN ||
      calendar == DT::CALENDAR_TYPE::JULIAN_GREGORIAN)
  {
    p.second += "* DE406 Start Jd=625360.5, -3000 FEB 23 00:00:00\n";
    date.SetDateTime(-3000, 2, 23, 0, 0, 0, calendar);
    p1 = this->TestF1F2<double, DT::JulianDateTime, DT::CALENDAR_TYPE>(
        625360.5, date, calendar, f1, f2);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;
  }

  if (calendar == DT::CALENDAR_TYPE::GREGORIAN ||
      calendar == DT::CALENDAR_TYPE::JULIAN_GREGORIAN)
  {
    p.second += "* DE406 end. Jd=2816912.5, 3000 MAY 06 00:00:00\n";
    date.SetDateTime(3000, 5, 6, 0, 0, 0, calendar);
    p1 = this->TestF1F2<double, DT::JulianDateTime, DT::CALENDAR_TYPE>(
        2816912.5, date, calendar, f1, f2);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* DE406 end (UNIX.406). Jd=2816976.5, 3000 JUL 09 00:00:00\n";
    date.SetDateTime(3000, 7, 9, 0, 0, 0, calendar);
    p1 = this->TestF1F2<double, DT::JulianDateTime, DT::CALENDAR_TYPE>(
        2816976.5, date, calendar, f1, f2);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* DE405 Start Jd=2305424.5, 1599 DEC 09 00:00:00\n";
    date.SetDateTime(1599, 12, 9, 0, 0, 0, calendar);
    p1 = this->TestF1F2<double, DT::JulianDateTime, DT::CALENDAR_TYPE>(
        2305424.5, date, calendar, f1, f2);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* DE405 end. Jd=2525008.5, 2201 FEB 20 00:00:00\n";
    date.SetDateTime(2201, 2, 20, 0, 0, 0, calendar);
    p1 = this->TestF1F2<double, DT::JulianDateTime, DT::CALENDAR_TYPE>(
        2525008.5, date, calendar, f1, f2);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* DE200 Start Jd=2305424.5, 1599 DEC 09 00:00:00\n";
    date.SetDateTime(1599, 12, 9, 0, 0, 0, calendar);
    p1 = this->TestF1F2<double, DT::JulianDateTime, DT::CALENDAR_TYPE>(
        2305424.5, date, calendar, f1, f2);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* DE200 end. Jd=2513392.5, 2169 MAY 02 00:00:00\n";
    date.SetDateTime(2169, 5, 2, 0, 0, 0, calendar);
    p1 = this->TestF1F2<double, DT::JulianDateTime, DT::CALENDAR_TYPE>(
        2513392.5, date, calendar, f1, f2);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;
  }

  p.second += "\n";
  return p;
}

CommonTests::TResult JulianDateTimeTests::TestDayOfTheYear()
{
  TResult p = this->MakeHeader("Testing Day of the Year.", 2);

  DT::YMD ymd(1900, 12, 31);
  DT::JulianDateTime jd(DT::JulianDateTime::DateConversions().cd2jdJG(ymd));
  // auto doyJ = jd.doyJ(ymd);
  auto doyG = jd.doyG(ymd);
  auto doyJG = jd.doyJG(ymd);

  std::ostringstream os;
  TResult p1;

  p.second += "* 1900.\n";
  // If we are using the Julian Calendar
  // Then this Julian Day corresponds to
  // this day of the year.
  auto doyJ = 353;
  if (doyJ == jd.GetDoY(DT::CALENDAR_TYPE::JULIAN) &&
      doyG == jd.GetDoY(DT::CALENDAR_TYPE::GREGORIAN) &&
      doyJG == jd.GetDoY(DT::CALENDAR_TYPE::JULIAN_GREGORIAN))
  {
    os << "<br>Passed." << std::endl;
    p1.first = true;
    p1.second = os.str();
  }
  else
  {
    os << "<br>Failed." << std::endl;
    p1.first = false;
    p1.second = os.str();
  }
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult JulianDateTimeTests::TestDayOfTheWeek()
{
  TResult p = this->MakeHeader("Testing the day of the week.", 2);

  DT::JulianDateTime date;
  DT::DayNames dn;
  int idx;
  std::string name;

  p.second += "* -4712 01 01 Monday (Julian)\n";
  DT::YMD ymd; // year = -4712, month = 1, day = 1.
  date.SetDate(ymd, DT::CALENDAR_TYPE::JULIAN);
  idx = date.GetDoW(DT::CALENDAR_TYPE::JULIAN, DT::FIRST_DOW::SUNDAY);
  name = dn.GetDayName(idx);
  if (idx != 2 || name != "Monday")
  {
    std::ostringstream os;
    os << std::fixed << "<br>Fail got: " << idx << " = " << name
       << ", expected: 2 = Monday." << std::endl;
    p.first &= false;
    p.second += os.str();
  }
  else
  {
    p.second += "<br>Passed.\n";
  }

  p.second += "* -4713 11 24 Friday (Gregorian)\n";
  ymd = DT::YMD(-4713, 11, 24);
  date.SetDate(ymd, DT::CALENDAR_TYPE::GREGORIAN);
  idx = date.GetDoW(DT::CALENDAR_TYPE::GREGORIAN, DT::FIRST_DOW::SUNDAY);
  name = dn.GetDayName(idx);
  if (idx != 6 || name != "Friday")
  {
    std::ostringstream os;
    os << std::fixed << "<br>Fail got: " << idx << " = " << name
       << ", expected: 6 = Friday." << std::endl;
    p.first &= false;
    p.second += os.str();
  }
  else
  {
    p.second += "<br>Passed.\n";
  }

  p.second += "* 1582 10 04 Thursday (Julian)\n";
  ymd = DT::YMD(1582, 10, 4);
  date.SetDate(ymd, DT::CALENDAR_TYPE::JULIAN_GREGORIAN);
  idx = date.GetDoW(DT::CALENDAR_TYPE::JULIAN_GREGORIAN, DT::FIRST_DOW::SUNDAY);
  name = dn.GetDayName(idx);
  if (idx != 5 || name != "Thursday")
  {
    std::ostringstream os;
    os << std::fixed << "<br>Fail got: " << idx << " = " << name
       << ", expected: 5 = Thursday." << std::endl;
    p.first &= false;
    p.second += os.str();
  }
  else
  {
    p.second += "<br>Passed.\n";
  }

  p.second += "* 1582 10 15 Friday (Gregorian)\n";
  ymd = DT::YMD(1582, 10, 15);
  date.SetDate(ymd, DT::CALENDAR_TYPE::GREGORIAN);
  idx = date.GetDoW(DT::CALENDAR_TYPE::GREGORIAN, DT::FIRST_DOW::SUNDAY);
  name = dn.GetDayName(idx);
  if (idx != 6)
  {
    std::ostringstream os;
    os << std::fixed << "<br>Fail got: " << idx << " = " << name
       << " expected: 6 = Friday." << std::endl;
    p.first = false;
    p.second += os.str();
  }
  else
  {
    p.second += "<br>Passed.\n";
  }

  p.second += "* 1582 10 15 Friday (Julian)\n";
  ymd = DT::YMD(1582, 10, 15);
  date.SetDate(ymd, DT::CALENDAR_TYPE::JULIAN);
  idx = date.GetDoW(DT::CALENDAR_TYPE::JULIAN, DT::FIRST_DOW::SUNDAY);
  name = dn.GetDayName(idx);
  if (idx != 6 || name != "Friday")
  {
    std::ostringstream os;
    os << std::fixed << "<br>Fail got: " << idx << " = " << name
       << ", expected: 6 = Friday." << std::endl;
    p.first &= false;
    p.second += os.str();
  }
  else
  {
    p.second += "<br>Passed.\n";
  }

  p.second += "\n";
  return p;
}

CommonTests::TResult JulianDateTimeTests::TestISOWeekDate()
{
  TResult p = this->MakeHeader("Testing week of the year.", 2);

  p.second += "* *  2005-01-01 is 2004-W53-6\n";
  DT::YMD ymd(2005, 1, 1);
  DT::ISOWeekDate iwd(2004, 53, 6);
  auto p1 = this->Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* *  2005-01-02 is 2004-W53-7\n";
  ymd = DT::YMD(2005, 1, 2);
  iwd = DT::ISOWeekDate(2004, 53, 7);
  p1 = this->Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* *  2005-12-31 is 2005-W52-6\n";
  ymd = DT::YMD(2005, 12, 31);
  iwd = DT::ISOWeekDate(2005, 52, 6);
  p1 = this->Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* *  2007-01-01 is 2007-W01-1 (both years 2007 start with the "
              "same day)\n";
  ymd = DT::YMD(2007, 1, 1);
  iwd = DT::ISOWeekDate(2007, 1, 1);
  p1 = this->Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* *  2007-12-30 is 2007-W52-7\n";
  ymd = DT::YMD(2007, 12, 30);
  iwd = DT::ISOWeekDate(2007, 52, 7);
  p1 = this->Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* *  2007-12-31 is 2008-W01-1\n";
  ymd = DT::YMD(2007, 12, 31);
  iwd = DT::ISOWeekDate(2008, 1, 1);
  p1 = this->Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* *  2008-01-01 is 2008-W01-2 (Gregorian year 2008 is a leap "
              "year,\n ISO year 2008 is 2 days shorter: 1 day longer at the "
              "start,\n 3 days shorter at the end)\n";
  ymd = DT::YMD(2008, 1, 1);
  iwd = DT::ISOWeekDate(2008, 1, 2);
  p1 = this->Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* *  2008-12-28 is 2008-W52-7\n";
  ymd = DT::YMD(2008, 12, 28);
  iwd = DT::ISOWeekDate(2008, 52, 7);
  p1 = this->Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* *  2008-12-29 is 2009-W01-1\n";
  ymd = DT::YMD(2008, 12, 29);
  iwd = DT::ISOWeekDate(2009, 1, 1);
  p1 = this->Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* *  2008-12-30 is 2009-W01-2\n";
  ymd = DT::YMD(2008, 12, 30);
  iwd = DT::ISOWeekDate(2009, 1, 2);
  p1 = this->Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* *  2008-12-31 is 2009-W01-3\n";
  ymd = DT::YMD(2008, 12, 31);
  iwd = DT::ISOWeekDate(2009, 1, 3);
  p1 = this->Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* *  2009-01-01 is 2009-W01-4\n";
  ymd = DT::YMD(2009, 1, 1);
  iwd = DT::ISOWeekDate(2009, 1, 4);
  p1 = this->Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* *  2009-12-31 is 2009-W53-4 (ISO year 2009 has 53 weeks,\n "
              "extending the Gregorian year 2009,\n which starts and ends with "
              "Thursday,\n at both ends with three days)\n";
  ymd = DT::YMD(2009, 12, 31);
  iwd = DT::ISOWeekDate(2009, 53, 4);
  p1 = this->Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* *  2010-01-01 is 2009-W53-5\n";
  ymd = DT::YMD(2010, 1, 1);
  iwd = DT::ISOWeekDate(2009, 53, 5);
  p1 = this->Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* *  2010-01-02 is 2009-W53-6\n";
  ymd = DT::YMD(2010, 1, 2);
  iwd = DT::ISOWeekDate(2009, 53, 6);
  p1 = this->Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* *  2010-01-03 is 2009-W53-7\n";
  ymd = DT::YMD(2010, 1, 3);
  iwd = DT::ISOWeekDate(2009, 53, 7);
  p1 = this->Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* *  2010-01-04 is 2010-W01-1\n";
  ymd = DT::YMD(2010, 1, 4);
  iwd = DT::ISOWeekDate(2010, 1, 1);
  p1 = this->Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* *  2008-09-26 is 2008-W39-5\n";
  ymd = DT::YMD(2008, 9, 26);
  iwd = DT::ISOWeekDate(2008, 39, 5);
  p1 = this->Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* *  2012-05-24 is 2012-W21-4\n";
  ymd = DT::YMD(2012, 5, 24);
  iwd = DT::ISOWeekDate(2012, 21, 4);
  p1 = this->Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult JulianDateTimeTests::TestTime()
{
  // Negative times are mod 24 since the fraction of the day
  // in the class JulianDateTime is always positive. The
  // Julian Day Number will be adjusted in this class so
  // that the sum of the jdn and fod will always be equivalent
  // to the time expressed as the fraction of the day.
  TResult p = this->MakeHeader("Testing times.", 2);

  auto f1 = std::bind(&JulianDateTimeTests::Testhms2hr, this, _1, _2);
  auto f2 = std::bind(&JulianDateTimeTests::Testhr2hms, this, _1, _2);
  auto f3 = std::bind(&JulianDateTimeTests::Testhm2hr, this, _1, _2);
  auto f4 = std::bind(&JulianDateTimeTests::Testhr2hm, this, _1, _2);

  p.second += "* h=23.7625, 23:45:45\n";
  DT::HMS hms(23, 45, 45);
  DT::JulianDateTime time;
  time.SetTime(hms);
  auto p1 = this->TestF1F2<double, DT::JulianDateTime>(23.7625, time, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* h=-23.7625, -23:45:45\n";
  hms.hour = -hms.hour;
  time.SetTime(hms);
  p1 = this->TestF1F2<double, DT::JulianDateTime>(24 - 23.7625, time, f1,
                                                  f2); // Working mod 24.
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* h=23.7625, 23:45.75\n";
  DT::HM hm(23, 45.75);
  DT::JulianDateTime time_hm;
  time_hm.SetTime(hm);
  p1 = this->TestF1F2<double, DT::JulianDateTime>(23.7625, time_hm, f3, f4);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* h=-23.7625, -23:45.75\n";
  hm.hour = -hm.hour;
  time_hm.SetTime(hm);
  p1 =
      this->TestF1F2<double, DT::JulianDateTime>(24 - 23.7625, time_hm, f3, f4);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* h=00.7625, 00:45.75\n";
  hm.hour = 0;
  hm.minute = 45.75;
  time_hm.SetTime(hm);
  p1 = this->TestF1F2<double, DT::JulianDateTime>(0.7625, time_hm, f3, f4);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* h=-00.7625, -00:45.75\n";
  hm.minute = -hm.minute;
  time_hm.SetTime(hm);
  p1 = this->TestF1F2<double, DT::JulianDateTime>(24.0 - 0.7625, time_hm, f3,
                                                  f4);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult JulianDateTimeTests::TestTimeZone()
{
  TResult p = this->MakeHeader("Testing time zones.", 2);

  // Time zone name, difference from UT 0 in seconds.
  std::pair<std::string, double> TZ;
  // Alternate time zone name, difference from UT 0 in seconds.
  std::pair<std::string, double> TZAlt;

  // Set up the timezone and the alternate one.
  int TZValue;
  std::string TZName = "UT";

  std::ostringstream os;
  TResult p1;
  DT::TimeZones timeZones;

  p.second += "* UT0, UT+10\n";
  if (timeZones.GetTimeZoneValue(TZName, TZValue))
  {
    TZ.first = TZName;
    TZ.second = TZValue;
  }
  else
  {
    os << "<br>Fail unable to get a time zone offset for " << TZName
       << std::endl;
    p1.first = false;
    p1.second = os.str();
  }
  TZName = "UT+10";
  if (timeZones.GetTimeZoneValue(TZName, TZValue))
  {
    TZAlt.first = TZName;
    TZAlt.second = TZValue;
  }
  else
  {
    os << "<br>Fail unable to get a time zone offset for " << TZName
       << std::endl;
    p1.first = false;
    p1.second = os.str();
  }

  const double seconds_in_day = 86400.0;
  DT::CALENDAR_TYPE calendar = DT::CALENDAR_TYPE::JULIAN_GREGORIAN;

  // 0.1ns is the best we can expect.
  DT::YMDHMS utc(2012, 1, 2, 23, 46, 0.0000000001);
  DT::JulianDateTime utcjd;
  utcjd.SetDateTime(utc, calendar);
  DT::JulianDateTime localjd(utcjd);
  localjd += TZAlt.second / seconds_in_day;
  DT::YMDHMS lt(localjd.GetDateTime(calendar));
  DT::YMDHMS expected_lt(2012, 1, 3, 9, 46, 0.0000000001);
  if (expected_lt.IsClose(lt, this->rel_seconds, this->abs_seconds))
  {
    os << "<br>Passed." << std::endl;
    p1.first = true;
    p1.second = os.str();
  }
  else
  {
    os << "<br>Fail got " << lt << ", expected: " << expected_lt << std::endl;
    p1.first = false;
    p1.second = os.str();
  }
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* UT0, UT-09:30\n";
  TZName = "UT";
  if (timeZones.GetTimeZoneValue(TZName, TZValue))
  {
    TZ.first = TZName;
    TZ.second = TZValue;
  }
  else
  {
    os << "<br>Fail unable to get a time zone offset for " << TZName
       << std::endl;
    p1.first = false;
    p1.second = os.str();
  }

  TZName = "UT-09:30";
  if (timeZones.GetTimeZoneValue(TZName, TZValue))
  {
    TZAlt.first = TZName;
    TZAlt.second = TZValue;
  }
  else
  {
    os << "<br>Fail unable to get a time zone offset for " << TZName
       << std::endl;
    p1.first = false;
    p1.second = os.str();
  }

  utc = DT::YMDHMS(2012, 1, 2, 23, 46, 0.0000000001);
  utcjd.SetDateTime(utc, calendar);
  localjd = utcjd;
  localjd += TZAlt.second / seconds_in_day;
  lt = DT::YMDHMS(localjd.GetDateTime(calendar));
  expected_lt = DT::YMDHMS(2012, 1, 2, 14, 16, 0.0000000001);
  if (expected_lt.IsClose(lt, this->rel_seconds, this->abs_seconds))
  {
    os << "<br>Passed." << std::endl;
    p1.first = true;
    p1.second = os.str();
  }
  else
  {
    os << "<br>Fail got " << lt << ", expected " << expected_lt << std::endl;
    p1.first = false;
    p1.second = os.str();
  }
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult JulianDateTimeTests::TestJulianDates()
{
  TResult p = this->MakeHeader("Testing Julian Date Set/Get.", 2);

  DT::JD ref(2400000, 0.5);
  DT::JulianDateTime jd(2400000, 0.5);
  auto jdfp = jd.GetJDFullPrecision();
  auto jdshort = jd.GetJDN() + jd.GetFoD();
  std::ostringstream os;
  TResult p1;

  p.second += "* Modified JD (2400000.5).\n";
  if (jdfp == ref && jdshort == 2400000.5)
  {
    os << "<br>Passed." << std::endl;
    p1.first = true;
    p1.second = os.str();
  }
  else
  {
    os << "<br>Failed expected 2400000.5, got GetJDFullPrecision() (" << jdfp
       << ") and GetJD() " << std::fixed << std::setprecision(8) << jdshort
       << std::endl;
    p1.first = false;
    p1.second = os.str();
  }
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* Truncated JD (2440000.5).\n";
  ref = DT::JD(2440000, 0.5);
  jd = DT::JulianDateTime(2440000, 0.5);
  jdfp = jd.GetJDFullPrecision();
  jdshort = jd.GetJDN() + jd.GetFoD();
  if (jdfp == ref && jdshort == 2440000.5)
  {
    os << "<br>Passed." << std::endl;
    p1.first = true;
    p1.second = os.str();
  }
  else
  {
    os << "<br>Failed expected 2440000.5, got GetJDFullPrecision() (" << jdfp
       << ") and GetJD() " << std::fixed << std::setprecision(8) << jdshort
       << std::endl;
    p1.first = false;
    p1.second = os.str();
  }
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* Dublin JD (2415020).\n";
  ref = DT::JD(2415020, 0);
  jd = DT::JulianDateTime(2415020, 0);
  jdfp = jd.GetJDFullPrecision();
  jdshort = jd.GetJDN() + jd.GetFoD();
  if (jdfp == ref && jdshort == 2415020)
  {
    os << "<br>Passed." << std::endl;
    p1.first = true;
    p1.second = os.str();
  }
  else
  {
    os << "<br>Failed expected 2415020, got GetJDFullPrecision() (" << jdfp
       << ") and GetJD() " << std::fixed << std::setprecision(8) << jdshort
       << std::endl;
    p1.first = false;
    p1.second = os.str();
  }
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}
