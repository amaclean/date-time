/*=========================================================================

  Program:   Date Time Library
  File   :   DateTimeFormatterTests.cpp

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "DateTimeFormatterTests.h"
#include "JulianDateTime.h"
#include "TimeZones.h"

#include <numeric>

/// @cond
namespace TestData {
std::vector<std::map<std::string, std::string>> formattedDatesData{
    {{"MonthNameAbbrev", "Dec"},
     {"JD", "2415398.13346207175926"},
     {"NamedDate", "Monday, 31 December 1900 15:12:11.123"},
     {"MonthName", "December"},
     {"DoW", "2"},
     {"DayName", "Monday"},
     {"JDShort", "2415398.13346207"},
     {"LeapYear", "Yes"},
     {"DoY", "366.63346207"},
     {"Calendar", "Julian"},
     {"Date", "1900-December-31 15:12:11.123"},
     {"IWD", ""},
     {"DayNameAbbrev", "Mon"}},

    {{"MonthNameAbbrev", "Jan"},
     {"JD", "2415398.13346207175926"},
     {"NamedDate", "Sunday, 13 January 1901 15:12:11.123"},
     {"MonthName", "January"},
     {"DoW", "1"},
     {"DayName", "Sunday"},
     {"JDShort", "2415398.13346207"},
     {"LeapYear", "No"},
     {"DoY", "13.63346207"},
     {"Calendar", "Gregorian"},
     {"Date", "1901-January-13 15:12:11.123"},
     {"IWD", "1901-02-7"},
     {"DayNameAbbrev", "Sun"}},

    {{"MonthNameAbbrev", "Jan"},
     {"JD", "2415398.13346207175926"},
     {"NamedDate", "Sunday, 13 January 1901 15:12:11.123"},
     {"MonthName", "January"},
     {"DoW", "1"},
     {"DayName", "Sunday"},
     {"JDShort", "2415398.13346207"},
     {"LeapYear", "No"},
     {"DoY", "13.63346207"},
     {"Calendar", "Julian/Gregorian"},
     {"Date", "1901-January-13 15:12:11.123"},
     {"IWD", "1901-02-7"},
     {"DayNameAbbrev", "Sun"}},

    {{"MonthNameAbbrev", "Jan"},
     {"JD", "2415398.13346207175926"},
     {"NamedDate", "Sunday, 13 Jan 1901 15:12:11.123"},
     {"MonthName", "January"},
     {"DoW", "1"},
     {"DayName", "Sunday"},
     {"JDShort", "2415398.13346207"},
     {"LeapYear", "No"},
     {"DoY", "13.63346207"},
     {"Calendar", "Julian/Gregorian"},
     {"Date", "1901-Jan-13 15:12:11.123"},
     {"IWD", "1901-02-7"},
     {"DayNameAbbrev", "Sun"}},

    {{"MonthNameAbbrev", "Jan"},
     {"JD", "2415398.13346207175926"},
     {"NamedDate", "Sunday, 13 Jan 1901 15:12:11.123"},
     {"MonthName", "January"},
     {"DoW", "1"},
     {"DayName", "Sunday"},
     {"JDShort", "2415398.13346207"},
     {"LeapYear", "No"},
     {"DoY", "13.63346207"},
     {"Calendar", "Julian/Gregorian"},
     {"Date", "1901-01-13 15:12:11.123"},
     {"IWD", "1901-02-7"},
     {"DayNameAbbrev", "Sun"}},

    {{"MonthNameAbbrev", "Jan"},
     {"JD", "2415398.13346207175926"},
     {"NamedDate", "Sun, 13 Jan 1901 15:12:11.123"},
     {"MonthName", "January"},
     {"DoW", "1"},
     {"DayName", "Sunday"},
     {"JDShort", "2415398.13346207"},
     {"LeapYear", "No"},
     {"DoY", "13.63346207"},
     {"Calendar", "Julian/Gregorian"},
     {"Date", "1901-01-13 15:12:11.123"},
     {"IWD", "1901-02-7"},
     {"DayNameAbbrev", "Sun"}},

    {{"MonthNameAbbrev", "Jan"},
     {"JD", "2415398.13346207175926"},
     {"NamedDate", "1901-01-13 15:12:11.123"},
     {"MonthName", "January"},
     {"DoW", "1"},
     {"DayName", "Sunday"},
     {"JDShort", "2415398.13346207"},
     {"LeapYear", "No"},
     {"DoY", "13.63346207"},
     {"Calendar", "Julian/Gregorian"},
     {"Date", "1901-01-13 15:12:11.123"},
     {"IWD", "1901-02-7"},
     {"DayNameAbbrev", "Sun"}},

    {{"MonthNameAbbrev", "Jan"},
     {"JD", "2415398.13346207175926"},
     {"NamedDate", "1901-01-13 15:12:11.123"},
     {"MonthName", "January"},
     {"DoW", "7"},
     {"DayName", "Sunday"},
     {"JDShort", "2415398.13346207"},
     {"LeapYear", "No"},
     {"DoY", "13.63346207"},
     {"Calendar", "Julian/Gregorian"},
     {"Date", "1901-01-13 15:12:11.123"},
     {"IWD", "1901-02-7"},
     {"DayNameAbbrev", "Sun"}},

    {{"MonthNameAbbrev", "Dec"},
     {"JD", "2269298.13346207175926"},
     {"NamedDate", "Thursday, 31 December 1500 15:12:11.123"},
     {"MonthName", "December"},
     {"DoW", "5"},
     {"DayName", "Thursday"},
     {"JDShort", "2269298.13346207"},
     {"LeapYear", "Yes"},
     {"DoY", "366.63346207"},
     {"Calendar", "Julian/Gregorian"},
     {"Date", "1500-December-31 15:12:11.123"},
     {"IWD", ""},
     {"DayNameAbbrev", "Thu"}},
};

/** For testing formatting into YMDHMS, JDLong*/
struct DT_TEST_DATA
{
  DT::JulianDateTime jdt;
  std::string strDate;
  std::string jdLongDate;
  std::string jdLongDateAlt;
  std::string doyStr;
};

std::vector<DT_TEST_DATA> dtTestData{
    {DT::JulianDateTime(DT::YMDHMS(-13200, 9, 1, 23, 59, 59.99996),
                        DT::CALENDAR_TYPE::JULIAN_GREGORIAN),
     "-13200-09-02 00:00:00.0000", "-3099997.5000000", "-3099997.5000000",
     "246.00000000"},
    {DT::JulianDateTime(DT::YMDHMS(-4712, 1, 1, 23, 59, 59.99996),
                        DT::CALENDAR_TYPE::JULIAN_GREGORIAN),
     "-4712-01-02 00:00:00.0000", "0.5000000", "0.5000000", "2.00000000"},
    {DT::JulianDateTime(DT::YMDHMS(2013, 9, 6, 23, 59, 59.99996),
                        DT::CALENDAR_TYPE::JULIAN_GREGORIAN),
     "2013-09-07 00:00:00.0000", "2456542.5000000", "2456542.5000000",
     "250.00000000"},
    {DT::JulianDateTime(DT::YMDHMS(2013, 9, 6, 23, 59, 59.999499999),
                        DT::CALENDAR_TYPE::JULIAN_GREGORIAN),
     "2013-09-06 23:59:59.9995", "2456542.5000000", "2456542.5000000",
     "249.99999999"},
    {DT::JulianDateTime(DT::YMDHMS(-13200, 9, 1, 23, 59, 59.99995),
                        DT::CALENDAR_TYPE::JULIAN_GREGORIAN),
     "-13200-09-02 00:00:00", "-3099997.5000000", "-3099998", "246"},
    {DT::JulianDateTime(DT::YMDHMS(-4712, 1, 1, 23, 59, 59.99995),
                        DT::CALENDAR_TYPE::JULIAN_GREGORIAN),
     "-4712-01-02 00:00:00", "0.5000000", "1", "2"},
    {DT::JulianDateTime(DT::YMDHMS(2013, 9, 6, 0, 0, 0),
                        DT::CALENDAR_TYPE::JULIAN_GREGORIAN),
     "2013-09-06 00:00:00", "2456542.0000000", "2456542", "249"},
    {DT::JulianDateTime(DT::YMDHMS(2013, 9, 6, 23, 59, 59.99995),
                        DT::CALENDAR_TYPE::JULIAN_GREGORIAN),
     "2013-09-07 00:00:00", "2456542.5000000", "2456543", "250"},
    {DT::JulianDateTime(DT::YMDHMS(2013, 9, 6, 23, 59, 59.999499999),
                        DT::CALENDAR_TYPE::JULIAN_GREGORIAN),
     "2013-09-07 00:00:00", "2456542.5000000", "2456543", "250"},
    {DT::JulianDateTime(DT::YMDHMS(0, 1, 1, 23, 59, 59.999499999),
                        DT::CALENDAR_TYPE::JULIAN_GREGORIAN),
     "0-01-02 00:00:00", "1721058.5000000", "1721059", "2"},
    {DT::JulianDateTime(DT::YMDHMS(-1, 1, 1, 23, 59, 59.999499999),
                        DT::CALENDAR_TYPE::JULIAN_GREGORIAN),
     "-1-01-02 00:00:00", "1720693.5000000", "1720694", "2"},
    {DT::JulianDateTime(DT::YMDHMS(-1, 1, 1, 12, 0, 0),
                        DT::CALENDAR_TYPE::JULIAN_GREGORIAN),
     "-1-01-01 12:00:00", "1720693.0000000", "1720693", "2"}};
// Some negative dates and times.
std::vector<DT_TEST_DATA> dtTestDataNegative{
    {DT::JulianDateTime(DT::YMDHMS(-13190, 8, 1, 0, 0, 0),
                        DT::CALENDAR_TYPE::JULIAN_GREGORIAN),
     "-13190-08-01 00:00:00", "-3096377.5000000", "-3096377.5000000",
     "213.00000000"},
    {DT::JulianDateTime(DT::YMDHMS(-13190, 8, 1, 6, 0, 0),
                        DT::CALENDAR_TYPE::JULIAN_GREGORIAN),
     "-13190-08-01 06:00:00", "-3096377.2500000", "-3096377.2500000",
     "213.25000000"},
    {DT::JulianDateTime(DT::YMDHMS(-13190, 8, 1, 12, 0, 0),
                        DT::CALENDAR_TYPE::JULIAN_GREGORIAN),
     "-13190-08-01 12:00:00", "-3096377.0000000", "-3096377.0000000",
     "213.50000000"},
    {DT::JulianDateTime(DT::YMDHMS(-13190, 8, 1, 18, 0, 0),
                        DT::CALENDAR_TYPE::JULIAN_GREGORIAN),
     "-13190-08-01 18:00:00", "-3096376.7500000", "-3096376.7500000",
     "213.75000000"},
    {DT::JulianDateTime(DT::YMDHMS(-4712, 1, 1, 0, 0, 0),
                        DT::CALENDAR_TYPE::JULIAN_GREGORIAN),
     "-4712-01-01 00:00:00", "-0.5000000", "-0.5000000", "1.00000000"},
    {DT::JulianDateTime(DT::YMDHMS(-4712, 1, 1, 6, 0, 0),
                        DT::CALENDAR_TYPE::JULIAN_GREGORIAN),
     "-4712-01-01 06:00:00", "-0.2500000", "-0.2500000", "1.25000000"},
    {DT::JulianDateTime(DT::YMDHMS(-4712, 1, 1, 12, 0, 0),
                        DT::CALENDAR_TYPE::JULIAN_GREGORIAN),
     "-4712-01-01 12:00:00", "0.0000000", "0.0000000", "1.50000000"},
    {DT::JulianDateTime(DT::YMDHMS(-4712, 1, 1, 18, 0, 0),
                        DT::CALENDAR_TYPE::JULIAN_GREGORIAN),
     "-4712-01-01 18:00:00", "0.2500000", "0.2500000", "1.75000000"},
};

/** Time intervals.*/
struct TI_TEST_DATA
{
  DT::TimeInterval ti;
  std::string tidStr;
  std::string tiDHMS;
};
std::vector<TI_TEST_DATA> ti{
    {DT::TimeInterval(0, 0), "0.0000000", "0d  00:00:00.0000"},
    {DT::TimeInterval(-1, -0.5), "-1.5000000", "-1d -12:00:00.0000"},
    {DT::TimeInterval(-1, 0.5), "-0.5000000", "-0d -12:00:00.0000"},
    {DT::TimeInterval(1, -0.5), "0.5000000", "0d  12:00:00.0000"},
    {DT::TimeInterval(11, -0.5), "10.5000000", "10d  12:00:00.0000"},
    {DT::TimeInterval(10, 0.5), "10.5000000", "10d  12:00:00.0000"},
    {DT::TimeInterval(1, -11.5), "-10.5000000", "-10d -12:00:00.0000"},
    {DT::TimeInterval(-10, 0.5), "-9.5000000", "-9d -12:00:00.0000"},
    {DT::TimeInterval(10, 0.5), "10.5000000", "10d  12:00:00.0000"},
    {DT::TimeInterval(1, -11.5), "-10.5000000", "-10d -12:00:00.0000"},
    {DT::TimeInterval(10, 0.5), "11", "10d  12:00:00"},
    {DT::TimeInterval(1, -11.5), "-11", "-10d -12:00:00"}};

} // End namespace TestData.
/// @endcond

using namespace DT_Tests;
using namespace TestData;
// using namespace std::placeholders;  // for _1, _2, _3...

CommonTests::TResult DateTimeFormatterTests::TestAll()
{
  std::vector<TResult> messages{
      {this->TestFormattedDates()},         {this->TestFormatYMDHMS()},
      {this->TestFormatJDLong()},           {this->TestFormatTimeInterval()},
      {this->TestFormatTimeIntervalDHMS()}, {this->TestFormatDOY()},
      {this->TestFormatISOWeek()},          {this->TestOtherFormatting()}};

  return this->AggregateResults("DateTimeFormatter Tests.", messages);
}

CommonTests::TResult DateTimeFormatterTests::TestFormattedDates()
{
  auto MakeHeader = [](int testNum) {
    std::ostringstream os;
    os << "   Test " << testNum << " ";
    return os.str();
  };

  // List any different values in the maps,
  // the maps must be the same size and have the same keys.
  auto WhereFailed = [this](std::map<std::string, std::string> a,
                            std::map<std::string, std::string> b) {
    auto FailureMessage = [&b](std::string s,
                               std::pair<std::string, std::string> const& kv) {
      if (kv.second != b[kv.first])
      {
        s += "    - For: " + kv.first + ", expected " + b[kv.first] + ", got " +
            kv.second + "\n";
      }
      return s;
    };

    std::string s;
    if (!this->MapKeyCompare(a, b))
    {
      s = " - The keys do not match or the maps are not the same size.\n";
    }
    else
    {
      // Find the non-equivalent values.
      s = std::accumulate(a.begin(), a.end(), std::string{}, FailureMessage);
    }
    return s;
  };

  auto DoCheck = [this, MakeHeader, WhereFailed](
                     int testNum, std::map<std::string, std::string> a,
                     TResult& p) {
    p.second += MakeHeader(testNum);
    TResult p1;
    if (this->MapKeyValueCompare(a, formattedDatesData[testNum]))
    {
      p1.first = true;
      p1.second = "passed.\n";
    }
    else
    {
      p1.first = false;
      p1.second = "failed: \n";
      p1.second += WhereFailed(a, formattedDatesData[testNum]);
    }
    p.first &= p1.first;
    p.second += p1.second;
  };


  TResult p = this->MakeHeader("Testing GetFormattedDates.", 2);

  DT::DateTimeFormat fmt;
  fmt.dayName = DT::DAY_NAME::UNABBREVIATED;
  fmt.monthName = DT::MONTH_NAME::UNABBREVIATED;

  fmt.calendar = DT::CALENDAR_TYPE::JULIAN;
  auto jdt = DT::JulianDateTime(1900, 12, 31, 15, 12, 11.123, fmt.calendar);
  auto dates = this->formatter->GetFormattedDates(jdt, fmt);
  int testNum = 0;
  DoCheck(testNum, dates, p);

  fmt.calendar = DT::CALENDAR_TYPE::GREGORIAN;
  dates = this->formatter->GetFormattedDates(jdt, fmt);
  testNum += 1;
  DoCheck(testNum, dates, p);

  fmt.calendar = DT::CALENDAR_TYPE::JULIAN_GREGORIAN;
  dates = this->formatter->GetFormattedDates(jdt, fmt);
  testNum += 1;
  DoCheck(testNum, dates, p);

  fmt.monthName = DT::MONTH_NAME::ABBREVIATED;
  dates = this->formatter->GetFormattedDates(jdt, fmt);
  testNum += 1;
  DoCheck(testNum, dates, p);

  fmt.monthName = DT::MONTH_NAME::NONE;
  dates = this->formatter->GetFormattedDates(jdt, fmt);
  testNum += 1;
  DoCheck(testNum, dates, p);

  fmt.dayName = DT::DAY_NAME::ABBREVIATED;
  dates = this->formatter->GetFormattedDates(jdt, fmt);
  testNum += 1;
  DoCheck(testNum, dates, p);

  fmt.dayName = DT::DAY_NAME::NONE;
  dates = this->formatter->GetFormattedDates(jdt, fmt);
  testNum += 1;
  DoCheck(testNum, dates, p);

  fmt.firstDoW = DT::FIRST_DOW::MONDAY;
  dates = this->formatter->GetFormattedDates(jdt, fmt);
  testNum += 1;
  DoCheck(testNum, dates, p);

  fmt.dayName = DT::DAY_NAME::UNABBREVIATED;
  fmt.monthName = DT::MONTH_NAME::UNABBREVIATED;
  fmt.firstDoW = DT::FIRST_DOW::SUNDAY;

  jdt = DT::JulianDateTime(1500, 12, 31, 15, 12, 11.123, fmt.calendar);
  dates = this->formatter->GetFormattedDates(jdt, fmt);
  testNum += 1;
  DoCheck(testNum, dates, p);

  p.second += "\n";
  return p;
}

CommonTests::TResult DateTimeFormatterTests::TestFormatYMDHMS()
{
  TResult p = this->MakeHeader("Testing FormatYMDHMS().", 2);

  unsigned int idx = 0;
  std::string str;
  TResult p1(true, "");

  DT::DateTimeFormat fmt;
  fmt.calendar = DT::CALENDAR_TYPE::JULIAN_GREGORIAN;
  fmt.precisionSeconds = 9;

  for (auto q : dtTestData)
  {
    if (idx < 4)
    {
      this->formatter->maximumPrecision = 4;
      str = this->formatter->FormatAsYMDHMS(q.jdt, fmt);
    }
    else
    {
      this->formatter->maximumPrecision = 0;
      str = this->formatter->FormatAsYMDHMS(q.jdt, fmt);
    }
    p1 = this->Compare(str, q.strDate, "   ");
    p.first &= p1.first;
    p.second += p1.second;
    ++idx;
  }
  idx = 0;
  this->formatter->maximumPrecision = this->maxPrec;
  for (auto q : dtTestData)
  {
    if (idx < 4)
    {
      fmt.precisionSeconds = 4;
      str = this->formatter->FormatAsYMDHMS(q.jdt, fmt);
    }
    else
    {
      fmt.precisionSeconds = 0;
      str = this->formatter->FormatAsYMDHMS(q.jdt, fmt);
    }
    p1 = this->Compare(str, q.strDate, "   ");
    p.first &= p1.first;
    p.second += p1.second;
    ++idx;
  }

  std::string res1 = "2013-Sep-07 00:00:00.0000";
  std::string res2 = "2013-September-07 00:00:00.0000";

  this->formatter->maximumPrecision = 4;
  fmt.precisionSeconds = 9;
  fmt.monthName = DT::MONTH_NAME::ABBREVIATED;
  str = this->formatter->FormatAsYMDHMS(dtTestData[2].jdt, fmt);
  p1 = this->Compare(str, res1, "   ");
  p.first &= p1.first;
  p.second += p1.second;

  fmt.monthName = DT::MONTH_NAME::UNABBREVIATED;
  str = this->formatter->FormatAsYMDHMS(dtTestData[2].jdt, fmt);
  p1 = this->Compare(str, res2, "   ");
  p.first &= p1.first;
  p.second += p1.second;

  fmt.monthName = DT::MONTH_NAME::NONE;
  this->formatter->maximumPrecision = this->maxPrec;

  p.second += "\n";
  return p;
}

CommonTests::TResult DateTimeFormatterTests::TestFormatJDLong()
{
  TResult p = this->MakeHeader("Testing FormatJDLong().", 2);

  TResult p1(true, "");
  unsigned int idx = 0;
  std::string str;

  DT::DateTimeFormat fmt;
  fmt.precisionFoD = 12;

  for (const auto& q : dtTestData)
  {
    if (idx < 4)
    {
      this->formatter->maximumPrecision = 7;
      str = this->formatter->FormatJDLong(q.jdt.GetJDFullPrecision(), fmt);
    }
    else
    {
      this->formatter->maximumPrecision = 0;
      str = this->formatter->FormatJDLong(q.jdt.GetJDFullPrecision(), fmt);
    }
    p1 = this->Compare(str, q.jdLongDateAlt, "   ");
    p.first &= p1.first;
    p.second += p1.second;
    ++idx;
  }
  this->formatter->maximumPrecision = this->maxPrec;
  idx = 0;
  for (const auto& q : dtTestData)
  {
    if (idx < 4)
    {
      fmt.precisionFoD = 7;
      str = this->formatter->FormatJDLong(q.jdt.GetJDFullPrecision(), fmt);
    }
    else
    {
      fmt.precisionFoD = 0;
      str = this->formatter->FormatJDLong(q.jdt.GetJDFullPrecision(), fmt);
    }
    p1 = this->Compare(str, q.jdLongDateAlt, "   ");
    p.first &= p1.first;
    p.second += p1.second;
    ++idx;
  }

  this->formatter->maximumPrecision = 7;
  fmt.precisionFoD = 12;
  // Look at negative dates in more detail.
  for (const auto& q : dtTestDataNegative)
  {
    str = this->formatter->FormatJDLong(q.jdt.GetJDFullPrecision(), fmt);
    p1 = this->Compare(str, q.jdLongDateAlt, "   ");
    p.first &= p1.first;
    p.second += p1.second;
  }
  this->formatter->maximumPrecision = this->maxPrec;

  p.second += "\n";
  return p;
}

CommonTests::TResult DateTimeFormatterTests::TestFormatTimeInterval()
{
  TResult p = this->MakeHeader("Testing FormatTimeInterval().", 2);

  TResult p1(true, "");
  unsigned int idx = 0;
  std::string str;

  DT::DateTimeFormat fmt;

  for (auto q : ti)
  {
    this->formatter->maximumPrecision = 7;
    if (idx < 10)
    {
      fmt.precisionFoD = 12;
      str = this->formatter->FormatTimeInterval(q.ti, fmt);
    }
    else
    {
      fmt.precisionFoD = 0;
      str = this->formatter->FormatTimeInterval(q.ti, fmt);
    }
    p1 = this->Compare(str, q.tidStr, "   ");
    p.first &= p1.first;
    p.second += p1.second;
    ++idx;
  }
  this->formatter->maximumPrecision = this->maxPrec;
  idx = 0;
  for (auto q : ti)
  {
    if (idx < 10)
    {
      fmt.precisionFoD = 7;
      str = this->formatter->FormatTimeInterval(q.ti, fmt);
    }
    else
    {
      fmt.precisionFoD = 0;
      str = this->formatter->FormatTimeInterval(q.ti, fmt);
    }
    p1 = this->Compare(str, q.tidStr, "   ");
    p.first &= p1.first;
    p.second += p1.second;
    ++idx;
  }
  this->formatter->maximumPrecision = this->maxPrec;

  p.second += "\n";
  return p;
}

CommonTests::TResult DateTimeFormatterTests::TestFormatTimeIntervalDHMS()
{
  TResult p = this->MakeHeader("Testing FormatTimeIntervalDHMS().", 2);

  TResult p1(true, "");
  unsigned int idx = 0;
  std::string str;

  DT::DateTimeFormat fmt;

  for (auto q : ti)
  {
    if (idx < 10)
    {
      this->formatter->maximumPrecision = 4;
      fmt.precisionSeconds = 12;
      str = this->formatter->FormatTimeIntervalDHMS(q.ti, fmt);
    }
    else
    {
      this->formatter->maximumPrecision = 12;
      fmt.precisionSeconds = 0;
      str = this->formatter->FormatTimeIntervalDHMS(q.ti, fmt);
    }
    p1 = this->Compare(str, q.tiDHMS, "   ");
    p.first &= p1.first;
    p.second += p1.second;
    ++idx;
  }
  this->formatter->maximumPrecision = this->maxPrec;
  idx = 0;
  for (auto q : ti)
  {
    if (idx < 10)
    {
      fmt.precisionSeconds = 4;
      str = this->formatter->FormatTimeIntervalDHMS(q.ti, fmt);
    }
    else
    {
      fmt.precisionSeconds = 0;
      str = this->formatter->FormatTimeIntervalDHMS(q.ti, fmt);
    }
    p1 = this->Compare(str, q.tiDHMS, "   ");
    p.first &= p1.first;
    p.second += p1.second;
    ++idx;
  }
  this->formatter->maximumPrecision = this->maxPrec;

  p.second += "\n";
  return p;
}

CommonTests::TResult DateTimeFormatterTests::TestFormatDOY()
{
  TResult p = this->MakeHeader("Testing FormatDOY().", 2);

  TResult p1(true, "");
  unsigned int idx = 0;
  std::string str;

  DT::DateTimeFormat fmt;
  fmt.calendar = DT::CALENDAR_TYPE::JULIAN_GREGORIAN;

  this->formatter->maximumPrecision = 8;
  for (auto q : dtTestData)
  {
    if (idx < 4)
    {
      fmt.precisionFoD = 12;
      str = this->formatter->FormatDOY(q.jdt, fmt);
    }
    else
    {
      fmt.precisionFoD = 0;
      str = this->formatter->FormatDOY(q.jdt, fmt);
    }
    p1 = this->Compare(str, q.doyStr, "   ");
    p.first &= p1.first;
    p.second += p1.second;
    ++idx;
  }
  this->formatter->maximumPrecision = this->maxPrec;
  idx = 0;
  for (auto q : dtTestData)
  {
    if (idx < 4)
    {
      fmt.precisionFoD = 8;
      str = this->formatter->FormatDOY(q.jdt, fmt);
    }
    else
    {
      fmt.precisionFoD = 0;
      str = this->formatter->FormatDOY(q.jdt, fmt);
    }
    p1 = this->Compare(str, q.doyStr, "   ");
    p.first &= p1.first;
    p.second += p1.second;
    ++idx;
  }

  this->formatter->maximumPrecision = 8;
  fmt.precisionFoD = 12;
  // Look at negative dates in more detail.
  for (auto q : dtTestDataNegative)
  {
    str = this->formatter->FormatDOY(q.jdt, fmt);
    p1 = this->Compare(str, q.doyStr, "   ");
    p.first &= p1.first;
    p.second += p1.second;
  }
  this->formatter->maximumPrecision = this->maxPrec;

  p.second += "\n";
  return p;
}

CommonTests::TResult DateTimeFormatterTests::TestFormatISOWeek()
{
  TResult p = this->MakeHeader("Testing FormatISOWeek().", 2);

  DT::JulianDateTime jd(2008, 1, 1, 0, 0, 0,
                        DT::CALENDAR_TYPE::JULIAN_GREGORIAN);
  std::string expected = "2008-01-2"; // Year-week-day of the week.
  auto str = this->formatter->FormatISOWeek(jd);
  auto p1 = this->Compare(str, expected, "   ");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult DateTimeFormatterTests::TestOtherFormatting()
{
  TResult p = this->MakeHeader("Testing other formatting functions.", 2);

  DT::DateTimeFormat fmt;
  fmt.calendar = DT::CALENDAR_TYPE::JULIAN_GREGORIAN;

  p.second += "* FormatAsYMDHMS()";
  DT::JulianDateTime jd(2012, 1, 3, 9, 46, 12.3215, fmt.calendar);
  this->formatter->maximumPrecision = 14;
  auto str = this->formatter->FormatAsYMDHMS(jd, fmt);
  this->formatter->maximumPrecision = this->maxPrec;
  std::string expected = "2012-01-03 09:46:12.322";
  auto p1 = this->Compare(str, expected, "");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* FormatDateWithNames()";
  fmt.dayName = DT::DAY_NAME::UNABBREVIATED;
  fmt.monthName = DT::MONTH_NAME::UNABBREVIATED;
  str = this->formatter->FormatDateWithNames(jd, fmt);
  expected = "Tuesday, 03 January 2012 09:46:12.322";
  p1 = this->Compare(str, expected, "");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* FormatDateWithNames()";
  fmt.precisionSeconds = 0;
  str = this->formatter->FormatDateWithNames(jd, fmt);
  expected = "Tuesday, 03 January 2012 09:46:12";
  p1 = this->Compare(str, expected, "");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* FormatDateWithNames()";
  fmt.dayName = DT::DAY_NAME::ABBREVIATED;
  fmt.monthName = DT::MONTH_NAME::ABBREVIATED;
  fmt.precisionSeconds = 0;
  str = this->formatter->FormatDateWithNames(jd, fmt);
  expected = "Tue, 03 Jan 2012 09:46:12";
  p1 = this->Compare(str, expected, "");
  p.first &= p1.first;
  p.second += p1.second;

  fmt.dayName = DT::DAY_NAME::NONE;
  fmt.monthName = DT::MONTH_NAME::NONE;
  fmt.precisionSeconds = 3;

  p.second += "* FormatAsYMD()";
  str = this->formatter->FormatAsYMD(jd, fmt);
  expected = "2012-01-03";
  p1 = this->Compare(str, expected, "");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* FormatAsHMS()";
  str = this->formatter->FormatAsHMS(jd, fmt);
  expected = "09:46:12.322";
  p1 = this->Compare(str, expected, "");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* FormatJDShort()";
  fmt.precisionFoD = 9;
  str = this->formatter->FormatJDShort(jd, fmt);
  expected = "2455929.907087055";
  p1 = this->Compare(str, expected, "");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* FormatTZAsHM()";
  DT::TimeZones tz;
  int tzDiff;
  if (tz.GetTimeZoneValue("UT-09:30", tzDiff))
  {
    DT::HM tzHM((int)(tzDiff / 3600.0),
                (tzDiff - (int)(tzDiff / 3600.0) * 3600.0) / 60.0);
    str = this->formatter->FormatTZAsHM(tzHM);
    expected = "-0930";
    p1 = this->Compare(str, expected, "");
    p.first &= p1.first;
    p.second += p1.second;
  }
  else
  {
    p1.first = false;
    p1.second = "The time zone UT-09:30 was not found.\n";
    p.first &= p1.first;
    p.second += p1.second;
  }

  p.second += "* FormatTZAsHM()";
  if (tz.GetTimeZoneValue("UT+12:45", tzDiff))
  {
    DT::HM tzHM = DT::HM((int)(tzDiff / 3600.0),
                         (tzDiff - (int)(tzDiff / 3600.0) * 3600.0) / 60.0);
    str = this->formatter->FormatTZAsHM(tzHM);
    expected = "+1245";
    p1 = this->Compare(str, expected, "");
    p.first &= p1.first;
    p.second += p1.second;
  }
  else
  {
    p1.first = false;
    p1.second = "The time zone UT+12:45 was not found.\n";
    p.first &= p1.first;
    p.second += p1.second;
  }

  p.second += "\n";
  return p;
}
