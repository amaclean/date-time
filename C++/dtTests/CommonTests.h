/*=========================================================================

Program:   Date Time Library
File   :   CommonTests.h

Copyright (c) Andrew J. P. Maclean
All rights reserved.
See Copyright.txt or the documentation for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef DTTESTS_COMMONTESTS_H
#define DTTESTS_COMMONTESTS_H

#pragma once

#include "NonCopyableTest.h"

#include <functional>
#include <sstream>
#include <string>
#include <vector>

//! Date and time tests.
/**
 *
 * This namespace contains all the classes that test the date time library.
 */
namespace DT_Tests {
/**
  Common routines for the tests.

  @author Andrew J. P. Maclean
*/
class CommonTests : public NonCopyableTest
{
public:
  /**
   * The type for a result.
   * A pair consisting of either true or false and a message.
   *
   */
  typedef std::pair<bool, std::string> TResult;

  CommonTests() = default;

  virtual ~CommonTests() = default;

public:
  /**
   * Make a markdown compatible header.
   *
   * @param header The header.
   * @param level The level of the header (1 ... 6).
   *
   * @return A pair consisting of true and the header string.
   */
  TResult MakeHeader(std::string const& header, unsigned int const& level) const
  {
    std::ostringstream os;
    // Clamp level to the range 1 .. 6 inclusive.
    auto l = level < 1 ? 1 : level > 6 ? 6 : level;
    // std::string hl(l, '#');
    std::string hl(l - 1, ' ');
    os << hl << " " << header << std::endl;
    return TResult(true, os.str());
  }

  /**
   * Concatenate the results into a markdown compatible format.
   *
   * @param header The header.
   * @param messages The messages to be aggregated.
   *
   * @return A pair consisting of either true or false and the concatenated
   * message.
   */
  TResult AggregateResults(std::string const& header,
                           std::vector<TResult> messages) const
  {
    TResult p = MakeHeader(header, 1);
    for (const auto& q : messages)
    {
      if (!q.first)
      {
        p.first &= q.first;
        p.second += q.second;
      }
    }
    return p;
  }

  /**
   * Combine the results of testing two functions of the form f(p1,p2).
   *
   * @param a The first parameter.
   * @param b The second parameter.
   * @param f1 The first function.
   * @param f2 The second function.
   *
   * @return A pair consisting of either true or false and a message.
   */
  template <typename P1, typename P2>
  auto TestF1F2(P1 const& a, P2 const& b,
                std::function<TResult(P1, P2)> const f1,
                std::function<TResult(P1, P2)> const f2) -> TResult
  {
    auto p1 = f1(a, b);
    auto p2 = f2(a, b);
    return TResult(p1.first && p2.first, p1.second + p2.second);
  }

  /**
   * Combine the results of testing two functions of the form f(p1,p2)
   * and g(p1,p2,p3).
   *
   * @param a The first parameter.
   * @param b The second parameter.
   * @param c The second parameter.
   * @param f1 The first function.
   * @param f2 The second function.
   *
   * @return A pair consisting of either true or false and a message.
   */
  template <typename P1, typename P2, typename P3>
  auto TestF1F2(P1 const& a, P2 const& b, P3 const& c,
                std::function<TResult(P1, P2)> f1,
                std::function<TResult(P1, P2, P3)> f2) -> TResult
  {
    auto p1 = f1(a, b);
    auto p2 = f2(a, b, c);
    return TResult(p1.first && p2.first, p1.second + p2.second);
  }

  /**
   * Compare two objects.
   * The type T must support the operation (operator==).
   *
   * @param observed The observed result.
   * @param expected The expected result.
   * @param msg The first part of the pass/fail message.
   *
   * @return A pair consisting of either true or false and a message.
   */
  template <typename T>
  auto Compare(T observed, T expected, std::string const& msg) -> TResult
  {
    std::ostringstream os;
    os << msg;
    if (observed == expected)
    {
      os << " passed." << std::endl;
      return TResult(true, os.str());
    }
    else
    {
      os << " failed, observed: " << observed << " expected: " << expected
         << std::endl;
      return TResult(false, os.str());
    }
  }

  /**
   * Generate a pass/fail message.
   *
   * @param result The result of a test, either true or false.
   *
   * @return A Pair consisting of either true or false and a message.
   */
  TResult PassFail(bool const& result)
  {
    if (result)
    {
      return TResult(true, std::string("Passed.\n"));
    }
    return TResult(false, std::string("Failed.\n"));
  }

  /**
   * Compare two maps for equality (keys and values must be equal).
   *
   * See:
   * http://stackoverflow.com/questions/8473009/how-to-efficiently-compare-two-maps-of-strings-in-c
   *
   * @param lhs - the first map.
   * @param rhs - the second map.
   *
   * @return true if the keys and values in lhs are the same as those in rhs.
   *
   */
  template <typename T>
  static bool MapKeyValueCompare(T const& lhs, T const& rhs)
  {
    // No predicate needed because operator== is defined for pairs.
    return lhs.size() == rhs.size() &&
        std::equal(lhs.begin(), lhs.end(), rhs.begin());
  }

  /**
   * Compare two maps for key equality (only the keys must be equal).
   *
   * See:
   * http://stackoverflow.com/questions/8473009/how-to-efficiently-compare-two-maps-of-strings-in-c
   *
   * @param lhs - the first map.
   * @param rhs - the second map.
   *
   * @return true if the keys in lhs are the same as those in rhs.
   *
   */
  template <typename T> static bool MapKeyCompare(T const& lhs, T const& rhs)
  {

    auto pred = [](decltype(*lhs.begin()) a, decltype(a) b) {
      return a.first == b.first;
    };

    return lhs.size() == rhs.size() &&
        std::equal(lhs.begin(), lhs.end(), rhs.begin(), pred);
  }

public:
};
} // End namespace DT_Tests.
#endif // DTTESTS_COMMONTESTS_H
