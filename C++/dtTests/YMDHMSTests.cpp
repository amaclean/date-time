/*=========================================================================

  Program:   Date Time Library
  File   :   YMDHMSTests.cpp

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "YMDHMSTests.h"

using namespace DT_Tests;

CommonTests::TResult YMDHMSTests::TestAll()
{
  std::vector<TResult> messages{{this->TestConstructors()},
                                {this->TestSetGet()},
                                {this->TestComparisonOperators()},
                                {this->TestAssignment()},
                                {this->TestStringOutput()}};

  return this->AggregateResults("YMDHMS Tests.", messages);
}

CommonTests::TResult YMDHMSTests::TestConstructors()
{
  TResult p = this->MakeHeader("Testing constructors.", 2);

  p.second += "* YMDHMS() ";
  DT::YMDHMS y;
  auto p1 = this->PassFail(y.year == -4712 && y.month == 1 && y.day == 1 &&
                           y.hour == 0 && y.minute == 0 && y.second == 0);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* YMDHMS(y, mon, d, h, min, s) ";
  DT::YMDHMS y1(this->year, this->month, this->day, this->hour, this->minute,
                this->second);
  p1 = this->PassFail(y1.year == this->year && y1.month == this->month &&
                      y1.day == this->day && y1.hour == this->hour &&
                      y1.minute == this->minute && y1.second == this->second);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* YMDHMS(ymdhms) ";
  DT::YMDHMS y2(y1);
  p1 = this->PassFail(y1.year == this->year && y1.month == this->month &&
                      y1.day == this->day && y1.hour == this->hour &&
                      y1.minute == this->minute && y1.second == this->second);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* YMDHMS(ymd, hms) ";
  DT::YMDHMS y3(*this->ymd, *this->hms);
  p1 = this->PassFail(y1.year == this->year && y1.month == this->month &&
                      y1.day == this->day && y1.hour == this->hour &&
                      y1.minute == this->minute && y1.second == this->second);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult YMDHMSTests::TestSetGet()
{
  TResult p = this->MakeHeader("Testing setting and getting members.", 2);

  p.second += "* SetYMDHMS(y ,m, d, h, min, s) ";
  DT::YMDHMS y1(this->year, this->month, this->day, this->hour, this->minute,
                this->second);
  DT::YMDHMS y2;
  y2.SetYMDHMS(this->year, this->month, this->day, this->hour, this->minute,
               this->second);
  auto p1 = this->PassFail(y1.year == y2.year && y1.month == y2.month &&
                           y1.day == y2.day && y1.hour == y2.hour &&
                           y1.minute == y2.minute && y1.second == y2.second);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* SetYMDHMS(ymd,hms) ";
  y2.SetYMDHMS(*this->ymd, *this->hms);
  p1 = this->PassFail(y1.year == y2.year && y1.month == y2.month &&
                      y1.day == y2.day && y1.hour == y2.hour &&
                      y1.minute == y2.minute && y1.second == y2.second);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* GetYMD() ";
  auto ymd1 = this->ymdhms->GetYMD();
  p1 = this->PassFail(ymd1.year == this->ymdhms->year &&
                      ymd1.month == this->ymdhms->month &&
                      ymd1.day == this->ymdhms->day);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* GetHMS() ";
  auto hms1 = this->ymdhms->GetHMS();
  p1 = this->PassFail(hms1.hour == this->ymdhms->hour &&
                      hms1.minute == this->ymdhms->minute &&
                      hms1.second == this->ymdhms->second);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* GetHM() ";
  auto hm1 = this->ymdhms->GetHM();
  p1 = this->PassFail(hm1.hour == this->ymdhms->hour &&
                      hm1.minute ==
                          this->ymdhms->minute + this->ymdhms->second / 60.0);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult YMDHMSTests::TestComparisonOperators()
{
  TResult p = this->MakeHeader("Testing comparison operators.", 2);

  p.second += "* == ";
  DT::YMDHMS y1(this->year, this->month, this->day, this->hour, this->minute,
                this->second);
  auto p1 = this->PassFail(y1 == *this->ymdhms);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* &asymp; ";
  DT::YMDHMS y2(this->year, this->month, this->day, this->hour, this->minute,
                this->second + 0.01);
  p1 = this->PassFail(this->ymdhms->IsClose(y2, 0.001));
  p.first &= p1.first;
  p.second += "\n<br>" + p1.second;

  // Expect a fail here
  p1 = this->PassFail(!this->ymdhms->IsClose(y2, 0.0001));
  p.first &= p1.first;
  p.second += "<br>" + p1.second;

  p.second += "* != ";
  y1.day -= 1;
  p1 = this->PassFail(y1 != *this->ymdhms);
  p.first &= p1.first;
  p.second += p1.second;

  DT::YMDHMS a(1992, 7, 2, 12, 58, 59.8);
  DT::YMDHMS b(1993, 7, 2, 12, 58, 59.8);
  DT::YMDHMS c(1992, 8, 2, 12, 58, 59.8);
  DT::YMDHMS d(1992, 7, 3, 12, 58, 59.8);
  DT::YMDHMS e(1992, 7, 2, 13, 58, 59.8);
  DT::YMDHMS f(1992, 7, 2, 12, 59, 59.8);
  DT::YMDHMS g(1992, 7, 2, 12, 58, 59.9);

  p.second += "* &lt; and &le; ";
  p1 = this->PassFail(a < b && a < c && a < d && a < e && a < f && a < g &&
                      a <= b && a <= c && a <= d && a <= e && a <= f &&
                      a <= g && a <= a && !(a > b) && !(a >= b));
  p.first &= p1.first;
  p.second += p1.second;

  DT::YMDHMS h(1992, 7, 2, 12, 59, 59.9);
  DT::YMDHMS i(1991, 7, 2, 12, 59, 59.9);
  DT::YMDHMS j(1992, 6, 2, 12, 59, 59.9);
  DT::YMDHMS k(1992, 7, 1, 12, 59, 59.9);
  DT::YMDHMS l(1992, 7, 2, 11, 59, 59.9);
  DT::YMDHMS m(1992, 7, 2, 12, 58, 59.9);
  DT::YMDHMS n(1992, 7, 2, 12, 58, 59.8);

  p.second += "* &gt; and &ge; ";
  p1 = this->PassFail(h > i && h > j && h > k && h > l && h > m && h > n &&
                      h >= i && h >= j && h >= k && h >= l && h >= m &&
                      h >= n && h >= h && !(h < i) && !(h <= i));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult YMDHMSTests::TestAssignment()
{
  TResult p = this->MakeHeader("Testing assignment.", 2);

  p.second += "* = ";
  auto y1 = *this->ymdhms;
  auto p1 = this->PassFail(
      y1.year == this->ymdhms->year && y1.month == this->ymdhms->month &&
      y1.day == this->ymdhms->day && y1.hour == this->ymdhms->hour &&
      y1.minute == this->ymdhms->minute && y1.second == this->ymdhms->second);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult YMDHMSTests::TestStringOutput()
{
  TResult p = this->MakeHeader("Testing <<.", 2);

  std::ostringstream os;

  os << *this->ymdhms;
  auto p1 = this->Compare(os.str(), this->valueStr, "* ");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}
