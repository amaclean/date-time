/*=========================================================================

  Program:   Date Time Library
  File   :   DateConversionsTests.cpp

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "DateConversionsTests.h"
#include "JG_CHANGEOVER.h"

using namespace DT_Tests;
using namespace std::placeholders; // for _1, _2, _3...

CommonTests::TResult DateConversionsTests::TestAll()
{
  std::vector<TResult> messages = {{this->TestProlepticJulianCalendar()},
                                   {this->TestProlepticGregorianCalendar()},
                                   {this->TestJulianGregorianCalendar()},

                                   {this->TestLeapYear()},
                                   {this->TestDayOfTheYear()},
                                   {this->TestDayOfTheWeek()},

                                   {this->TestISOWeekDate()}};

  return this->AggregateResults("DateConversions Tests.", messages);
}

CommonTests::TResult DateConversionsTests::TestCD2JDJ(int const& jd,
                                                      DT::YMD const& ymd)
{
  auto tjd = this->dc->cd2jdJ(ymd);
  return this->Compare(tjd, jd, "<br>cd2jdJ");
}

CommonTests::TResult DateConversionsTests::TestJD2CDJ(int const& jd,
                                                      DT::YMD const& ymd)
{
  auto ymd1 = this->dc->jd2cdJ(jd);
  return this->Compare(ymd1, ymd, "<br>jd2cdJ");
}

CommonTests::TResult DateConversionsTests::TestCD2JDG(int const& jd,
                                                      DT::YMD const& ymd)
{
  auto tjd = this->dc->cd2jdG(ymd);
  return this->Compare(tjd, jd, "<br>cd2jdG");
}

CommonTests::TResult DateConversionsTests::TestJD2CDG(int const& jd,
                                                      DT::YMD const& ymd)
{
  auto ymd1 = this->dc->jd2cdG(jd);
  return this->Compare(ymd1, ymd, "<br>jd2cdG");
}

CommonTests::TResult DateConversionsTests::TestCD2JDJG(int const& jd,
                                                       DT::YMD const& ymd)
{
  auto tjd = this->dc->cd2jdJG(ymd);
  return this->Compare(tjd, jd, "<br>cd2jdJG");
}

CommonTests::TResult DateConversionsTests::TestJD2CDJG(int const& jd,
                                                       DT::YMD const& ymd)
{
  auto ymd1 = this->dc->jd2cdJG(jd);
  return this->Compare(ymd1, ymd, "<br>jd2cdJG");
}

CommonTests::TResult DateConversionsTests::TestProlepticJulianCalendar()
{
  TResult p = this->MakeHeader("Testing Proleptic Julian, Julian calendar.", 2);

  auto f1 = std::bind(&DateConversionsTests::TestCD2JDJ, this, _1, _2);
  auto f2 = std::bind(&DateConversionsTests::TestJD2CDJ, this, _1, _2);

  p.second += "* Jd=0, -4712 01 01\n";
  DT::YMD ymd(-4712, 1, 1);
  auto p1 = this->TestF1F2<int, DT::YMD>(0, ymd, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* Jd=1721058, Jd=0, 0 01 01 (The first day of 1 B.C.)\n";
  ymd = DT::YMD(0, 1, 1);
  p1 = this->TestF1F2<int, DT::YMD>(1721058, ymd, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* Julian/Gregorian date changeover (last Julian day). "
              "Jd=2299160, 1582 10 04.\n";
  ymd = DT::YMD(1582, 10, 4);
  p1 = this->TestF1F2<int, DT::YMD>(2299160, ymd, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* Julian/Gregorian date changeover (first Gregorian day). Jd= "
              "2299171, 1582 10 15.\n";
  ymd = DT::YMD(1582, 10, 15);
  p1 = this->TestF1F2<int, DT::YMD>(2299171, ymd, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* Modified Julian Day 0.0. Jd= 2400013, 1858 11 17.\n";
  ymd = DT::YMD(1858, 11, 17);
  p1 = this->TestF1F2<int, DT::YMD>(2400013, ymd, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* Jd=2451545, 1999 12 19.\n";
  ymd = DT::YMD(1999, 12, 19);
  p1 = this->TestF1F2<int, DT::YMD>(2451545, ymd, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult DateConversionsTests::TestProlepticGregorianCalendar()
{
  TResult p =
      this->MakeHeader("Testing Proleptic Gregorian, Gregorian calendar.", 2);

  auto f1 = std::bind(&DateConversionsTests::TestCD2JDG, this, _1, _2);
  auto f2 = std::bind(&DateConversionsTests::TestJD2CDG, this, _1, _2);

  p.second += "* Jd=0, -4713 11 24\n";
  DT::YMD ymd(-4713, 11, 24);
  auto p1 = this->TestF1F2<int, DT::YMD>(0, ymd, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* Jd=1721060, 0 01 01 (The first day of 1 B.C.)\n";
  ymd = DT::YMD(0, 1, 1);
  p1 = this->TestF1F2<int, DT::YMD>(1721060, ymd, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* Julian/Gregorian date changeover (last Julian day). "
              "Jd=2299150, 1582 10 04.\n";
  ymd = DT::YMD(1582, 10, 4);
  p1 = this->TestF1F2<int, DT::YMD>(
      static_cast<int>(DT::JG_CHANGEOVER::GREGORIAN_PREVIOUS_DN), ymd, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* Julian/Gregorian date changeover (first Gregorian day). "
              "Jd=2299161, 1582 10 15.\n";
  ymd = DT::YMD(1582, 10, 15);
  p1 = this->TestF1F2<int, DT::YMD>(
      static_cast<int>(DT::JG_CHANGEOVER::GREGORIAN_START_DN), ymd, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* Modified Julian Day 0.0. Jd= 2400001, 1858 11 17.\n";
  ymd = DT::YMD(1858, 11, 17);
  p1 = this->TestF1F2<int, DT::YMD>(2400001, ymd, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* Jd=2451545, 2000 01 01.\n";
  ymd = DT::YMD(2000, 1, 1);
  p1 = this->TestF1F2<int, DT::YMD>(2451545, ymd, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult DateConversionsTests::TestJulianGregorianCalendar()
{
  TResult p = this->MakeHeader(
      "Testing Proleptic Julian, Julian, Gregorian calendar.", 2);

  auto f1 = std::bind(&DateConversionsTests::TestCD2JDJG, this, _1, _2);
  auto f2 = std::bind(&DateConversionsTests::TestJD2CDJG, this, _1, _2);

  p.second += "* Jd=0, -4712 01 01\n";
  DT::YMD ymd(-4712, 1, 1);
  auto p1 = this->TestF1F2<int, DT::YMD>(0, ymd, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* Jd=1721058, 0, 0 01 01 (The first day of 1 B.C.)\n";
  ymd = DT::YMD(0, 1, 1);
  p1 = this->TestF1F2<int, DT::YMD>(1721058, ymd, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* Julian/Gregorian date changeover (last Julian day). "
              "Jd=2299160, 1582 10 04.\n";
  ymd = DT::YMD(1582, 10, 4);
  p1 = this->TestF1F2<int, DT::YMD>(
      static_cast<int>(DT::JG_CHANGEOVER::JULIAN_PREVIOUS_DN), ymd, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* Julian/Gregorian date changeover (first Gregorian day). "
              "Jd=2299161, 1582 10 15.\n";
  ymd = DT::YMD(1582, 10, 15);
  p1 = this->TestF1F2<int, DT::YMD>(
      static_cast<int>(DT::JG_CHANGEOVER::GREGORIAN_START_DN), ymd, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* Modified Julian Day 0.0. Jd= 2400001, 1858 11 17.\n";
  ymd = DT::YMD(1858, 11, 17);
  p1 = this->TestF1F2<int, DT::YMD>(2400001, ymd, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* Jd=2451545, 2000 01 01.\n";
  ymd = DT::YMD(2000, 1, 1);
  p1 = this->TestF1F2<int, DT::YMD>(2451545, ymd, f1, f2);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult DateConversionsTests::TestLeapYear()
{
  TResult p = this->MakeHeader("Testing leap year.", 2);

  TResult p1(true, "");

  p.second += "* 1500\n";
  int year = 1500;
  std::ostringstream os;
  if (this->dc->isLeapYear(year, DT::CALENDAR_TYPE::JULIAN) == true &&
      this->dc->isLeapYear(year, DT::CALENDAR_TYPE::GREGORIAN) == false &&
      this->dc->isLeapYear(year, DT::CALENDAR_TYPE::JULIAN_GREGORIAN) == true)
  {
    os << "<br>Passed." << std::endl;
    p1.first = true;
    p1.second = os.str();
  }
  else
  {
    os << "<br>Fail " << year
       << " is a only a leap year for the Julian calendar." << std::endl;
    p1.first = false;
    p1.second = os.str();
  }
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* 1900\n";
  year = 1900;
  if (this->dc->isLeapYear(year, DT::CALENDAR_TYPE::JULIAN) == true &&
      this->dc->isLeapYear(year, DT::CALENDAR_TYPE::GREGORIAN) == false &&
      this->dc->isLeapYear(year, DT::CALENDAR_TYPE::JULIAN_GREGORIAN) == false)
  {
    os << "<br>Passed." << std::endl;
    p1.first = true;
    p1.second = os.str();
  }
  else
  {
    os << "<br>Fail " << year
       << " is a only a leap year for the Julian calendar." << std::endl;
    p1.first = false;
    p1.second = os.str();
  }
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* 2000\n";
  year = 2000;
  if (this->dc->isLeapYear(year, DT::CALENDAR_TYPE::JULIAN) == true &&
      this->dc->isLeapYear(year, DT::CALENDAR_TYPE::GREGORIAN) == true &&
      this->dc->isLeapYear(year, DT::CALENDAR_TYPE::JULIAN_GREGORIAN) == true)
  {
    os << "<br>Passed." << std::endl;
    p1.first = true;
    p1.second = os.str();
  }
  else
  {
    os << "<br>Fail " << year
       << " is a leap year for the Julian and Gregorian calendars."
       << std::endl;
    p1.first = false;
    p1.second = os.str();
  }
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* 2001\n";
  year = 2001;
  if (this->dc->isLeapYear(year, DT::CALENDAR_TYPE::JULIAN) == false &&
      this->dc->isLeapYear(year, DT::CALENDAR_TYPE::GREGORIAN) == false &&
      this->dc->isLeapYear(year, DT::CALENDAR_TYPE::JULIAN_GREGORIAN) == false)
  {
    os << "<br>Passed." << std::endl;
    p1.first = true;
    p1.second = os.str();
  }
  else
  {
    os << "<br>Fail " << year << " is not a leap year." << std::endl;
    p1.first = false;
    p1.second = os.str();
  }
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += p.second += "\n";
  return p;
}

CommonTests::TResult DateConversionsTests::TestDayOfTheYear()
{
  TResult p = this->MakeHeader("Testing Day of the Year.", 2);

  p.second += "* 1900.\n";
  std::ostringstream os;

  DT::YMD ymd(1900, 12, 31);
  auto doyJ = this->dc->doyJ(ymd);
  auto p1 = this->Compare(doyJ, 366, "<br>doyJ()");
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  // Not a leap year for the Gregorian Calendar.
  auto doyG = this->dc->doyG(ymd);
  p1 = this->Compare(doyG, 365, "<br>doyG()");
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  auto doyJG = this->dc->doyJG(ymd);
  p1 = this->Compare(doyJG, 365, "<br>doyJG()");
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult DateConversionsTests::TestDayOfTheWeek()
{
  // Note: For dow calculations we use the Julian Calendar for
  //       dates less that 1582-10-15 and the Gregorian Calendar
  //       for days on or after that date.
  TResult p = this->MakeHeader("Testing the day of the week.", 2);

  p.second += "* -4712 01 01 Monday (Julian)\n";
  DT::YMD ymd; // year = -4712, month = 1, day = 1.
  auto dow1 =
      this->dc->dow(ymd, DT::CALENDAR_TYPE::JULIAN, DT::FIRST_DOW::SUNDAY);
  auto dow2 =
      this->dc->dow(ymd, DT::CALENDAR_TYPE::JULIAN, DT::FIRST_DOW::MONDAY);
  if (dow1 != 2 && dow2 != 1)
  {
    std::ostringstream os;
    os << std::fixed << "<br>Fail got: " << dow1 << " and " << dow2
       << " expected: 2 and 1 (Monday)." << std::endl;
    p.first &= false;
    p.second += os.str();
  }
  else
  {
    p.second += "<br>Passed.\n";
  }

  p.second += "* -4713 11 24 Friday (Gregorian)\n";
  ymd = DT::YMD(-4713, 11, 24);
  dow1 = this->dc->dow(ymd, DT::CALENDAR_TYPE::JULIAN, DT::FIRST_DOW::SUNDAY);
  dow2 = this->dc->dow(ymd, DT::CALENDAR_TYPE::JULIAN, DT::FIRST_DOW::MONDAY);
  if (dow1 != 6 && dow2 != 5)
  {
    std::ostringstream os;
    os << std::fixed << "<br>Fail got: " << dow1 << " and " << dow2
       << " expected: 6 and 5 (Friday)." << std::endl;
    p.first &= false;
    p.second += os.str();
  }
  else
  {
    p.second += "<br>Passed.\n";
  }

  p.second += "* 1582 10 04 Thursday (Julian)\n";
  ymd = DT::YMD(1582, 10, 4);
  dow1 = this->dc->dow(ymd, DT::CALENDAR_TYPE::JULIAN_GREGORIAN,
                       DT::FIRST_DOW::SUNDAY);
  dow2 = this->dc->dow(ymd, DT::CALENDAR_TYPE::JULIAN_GREGORIAN,
                       DT::FIRST_DOW::MONDAY);
  if (dow1 != 5 && dow2 != 4)
  {
    std::ostringstream os;
    os << std::fixed << "<br>Fail got: " << dow1 << " and " << dow2
       << " expected: 5 and 4 (Thursday)." << std::endl;
    p.first &= false;
    p.second += os.str();
  }
  else
  {
    p.second += "<br>Passed.\n";
  }

  p.second += "* 1582 10 15 Friday (Gregorian)\n";
  ymd = DT::YMD(1582, 10, 15);
  dow1 =
      this->dc->dow(ymd, DT::CALENDAR_TYPE::GREGORIAN, DT::FIRST_DOW::SUNDAY);
  dow2 =
      this->dc->dow(ymd, DT::CALENDAR_TYPE::GREGORIAN, DT::FIRST_DOW::MONDAY);
  if (dow1 != 6 && dow2 != 5)
  {
    std::ostringstream os;
    os << std::fixed << "<br>Fail got: " << dow1 << " and " << dow2
       << " expected: 6 and 5 (Friday)." << std::endl;
    p.first &= false;
    p.second += os.str();
  }
  else
  {
    p.second += "<br>Passed.\n";
  }

  p.second += "* 1582 10 17 Sunday (Gregorian)\n";
  ymd = DT::YMD(1582, 10, 17);
  dow1 =
      this->dc->dow(ymd, DT::CALENDAR_TYPE::GREGORIAN, DT::FIRST_DOW::SUNDAY);
  dow2 =
      this->dc->dow(ymd, DT::CALENDAR_TYPE::GREGORIAN, DT::FIRST_DOW::MONDAY);
  if (dow1 != 1 && dow2 != 7)
  {
    std::ostringstream os;
    os << std::fixed << "<br>Fail got: " << dow1 << " and " << dow2
       << " expected: 1 and 7 (Sunday)." << std::endl;
    p.first &= false;
    p.second += os.str();
  }
  else
  {
    p.second += "<br>Passed.\n";
  }

  p.second += "\n";
  return p;
}

CommonTests::TResult
DateConversionsTests::Testcd2woy(DT::YMD const& ymd, DT::ISOWeekDate const& iwd)
{
  auto iwd1 = this->dc->cd2woy(ymd);
  return this->Compare(iwd1, iwd, "<br>cd2woy()");
}

CommonTests::TResult DateConversionsTests::TestISOWeekDate()
{
  TResult p = this->MakeHeader("Testing week of the year.", 2);

  p.second += "* 2005-01-01 is 2004-W53-6\n";
  DT::YMD ymd(2005, 1, 1);
  DT::ISOWeekDate iwd(2004, 53, 6);
  auto p1 = Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* 2005-01-02 is 2004-W53-7\n";
  ymd = DT::YMD(2005, 1, 2);
  iwd = DT::ISOWeekDate(2004, 53, 7);
  p1 = Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* 2005-12-31 is 2005-W52-6\n";
  ymd = DT::YMD(2005, 12, 31);
  iwd = DT::ISOWeekDate(2005, 52, 6);
  p1 = Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second +=
      "* 2007-01-01 is 2007-W01-1 (both years 2007 start with the same day)\n";
  ymd = DT::YMD(2007, 1, 1);
  iwd = DT::ISOWeekDate(2007, 1, 1);
  p1 = Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* 2007-12-30 is 2007-W52-7\n";
  ymd = DT::YMD(2007, 12, 30);
  iwd = DT::ISOWeekDate(2007, 52, 7);
  p1 = Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* 2007-12-31 is 2008-W01-1\n";
  ymd = DT::YMD(2007, 12, 31);
  iwd = DT::ISOWeekDate(2008, 1, 1);
  p1 = Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* 2008-01-01 is 2008-W01-2 (Gregorian year 2008 is a leap year, "
              "ISO year 2008 is 2 days shorter: 1 day longer at the start, 3 "
              "days shorter at the end)\n";
  ymd = DT::YMD(2008, 1, 1);
  iwd = DT::ISOWeekDate(2008, 1, 2);
  p1 = Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* 2008-12-28 is 2008-W52-7\n";
  ymd = DT::YMD(2008, 12, 28);
  iwd = DT::ISOWeekDate(2008, 52, 7);
  p1 = Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* 2008-12-29 is 2009-W01-1\n";
  ymd = DT::YMD(2008, 12, 29);
  iwd = DT::ISOWeekDate(2009, 1, 1);
  p1 = Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* 2008-12-30 is 2009-W01-2\n";
  ymd = DT::YMD(2008, 12, 30);
  iwd = DT::ISOWeekDate(2009, 1, 2);
  p1 = Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* 2008-12-31 is 2009-W01-3\n";
  ymd = DT::YMD(2008, 12, 31);
  iwd = DT::ISOWeekDate(2009, 1, 3);
  p1 = Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* 2009-01-01 is 2009-W01-4\n";
  ymd = DT::YMD(2009, 1, 1);
  iwd = DT::ISOWeekDate(2009, 1, 4);
  p1 = Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* 2009-12-31 is 2009-W53-4 (ISO year 2009 has 53 weeks, "
              "extending the Gregorian year 2009, which starts and ends with "
              "Thursday, at both ends with three days)\n";
  ymd = DT::YMD(2009, 12, 31);
  iwd = DT::ISOWeekDate(2009, 53, 4);
  p1 = Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* 2010-01-01 is 2009-W53-5\n";
  ymd = DT::YMD(2010, 1, 1);
  iwd = DT::ISOWeekDate(2009, 53, 5);
  p1 = Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* 2010-01-02 is 2009-W53-6\n";
  ymd = DT::YMD(2010, 1, 2);
  iwd = DT::ISOWeekDate(2009, 53, 6);
  p1 = Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* 2010-01-03 is 2009-W53-7\n";
  ymd = DT::YMD(2010, 1, 3);
  iwd = DT::ISOWeekDate(2009, 53, 7);
  p1 = Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* 2010-01-04 is 2010-W01-1\n";
  ymd = DT::YMD(2010, 1, 4);
  iwd = DT::ISOWeekDate(2010, 1, 1);
  p1 = Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* 2008-09-26 is 2008-W39-5\n";
  ymd = DT::YMD(2008, 9, 26);
  iwd = DT::ISOWeekDate(2008, 39, 5);
  p1 = Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "* 2012-05-24 is 2012-W21-4\n";
  ymd = DT::YMD(2012, 5, 24);
  iwd = DT::ISOWeekDate(2012, 21, 4);
  p1 = Testcd2woy(ymd, iwd);
  p.first = p.first & p1.first;
  p.second = p.second + p1.second;

  p.second += "\n";
  return p;
}
