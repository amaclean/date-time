/*=========================================================================

  Program:   Date Time Library
  File   :   JulianDateTimeTests.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef DTTESTS_JULIANDATETIMETESTS_H
#define DTTESTS_JULIANDATETIMETESTS_H

#pragma once

#include "CommonTests.h"
#include "DateTimeBase.h"
#include "JulianDateTime.h"

#include <memory>

namespace DT_Tests {

/**
  A test harness to test the date and time classes.

  @author Andrew J. P. Maclean
*/
class JulianDateTimeTests : public CommonTests, DT::DateTimeBase
{
public:
  JulianDateTimeTests() = default;

  virtual ~JulianDateTimeTests() = default;

public:
  /** Run all the tests.
      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestAll();

  /** Run the tests for the begin/end dates of some ephemerides.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestEphemerisDates();

  /** Run the tests for the date and time of specified dates and
   *  a specified calendar.
   *
   * @param calendar The calendar to use.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a string of failed tests.
   */
  TResult TestDoublePrecision(DT::CALENDAR_TYPE calendar);

  /** Test the day of the week for some dates.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestDayOfTheWeek();

  /** Run the tests for the week of the year conversions.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestISOWeekDate();

  /** Run the tests for the time conversions.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestTime();

  /** Run the tests for the comparison operators.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestComparisonOperators();

  /** Run the test for assignment.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestAssignment();

  /** Run the tests for the constructors.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestConstructors();

  /** Run the tests for setting the members.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestSet();

  /** Run the tests for the increment and addition operators.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestIncrementAndAddition();

  /** Run the tests for the decrement and subtraction operators.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestDecrementAndSubtraction();

  /** Run the tests for testing the time zone.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestTimeZone();

  /** Run the tests for testing the Julian date.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestJulianDates();

  /** Run the tests for testing the day of the year.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestDayOfTheYear();

private:
  /** Test the conversion of calendar date to julian day.
      The Julian Proleptic Calendar is used.

      @param jd the expected julian day for the given year, month and day.
      @param date the date.

      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestCD2JDJ(double const& jd, DT::JulianDateTime const& date);

  /** Test the conversion of julian day to calendar date.
      The Julian Proleptic Calendar is used.

      @param jd the julian day.
      @param date the date.

      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestJD2CDJ(double const& jd, DT::JulianDateTime const& date);

  /** Test the conversion of calendar date to julian day.
      The Gregorian Proleptic Calendar is used.

      @param jd the expected julian day for the given year, month and day.
      @param date the date.

      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestCD2JDG(double const& jd, DT::JulianDateTime const& date);

  /** Test the conversion of julian day to calendar date.
      The Gregorian Proleptic Calendar is used.

      @param jd the julian day.
      @param date the date.

      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestJD2CDG(double const& jd, DT::JulianDateTime const& date);

  /** Test the conversion of calendar date to julian day
   *  for a given hour of the day.

      @param jd the expected julian day for the given year, month and day.
      @param date the date.

      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestGetDateJD(double const& jd, DT::JulianDateTime const& date);

  /** Test the conversion of julian day to calendar date.

      @param jd the julian day.
      @param date the date.

      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestGetDateCT(double const& jd, DT::JulianDateTime const& date,
                        DT::CALENDAR_TYPE const& CALENDAR_TYPE =
                            DT::CALENDAR_TYPE::JULIAN_GREGORIAN);

  /** Test the conversion of hours minutes and seconds to hours.
      @param hr the expected hour of the day.
      @param time the time.

      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult Testhms2hr(double const& hr, DT::JulianDateTime const& time);

  /** Test the conversion of hours to hours minutes and seconds.
      @param hr the hour of the day.
      @param time the expected time.

      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult Testhr2hms(double const& hr, DT::JulianDateTime const& time);

  /** Test the conversion of hours and minutes to hours.
      @param hr the expected hour of the day.
      @param time the time.

      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult Testhm2hr(double const& hr, DT::JulianDateTime const& time);

  /** Test the conversion of hours of the day to hours and minutes.
      @param hr the hour of the day.
      @param time the expected time.

      @return A pair consisting of true (if the test passed)
       or false and a message.
  */

  /** Test the conversion of calendar date to ISO week of the year.
   *
   * The Gregorian Calendar is used.
   *
   * @param ymd The year, month and day.
   * @param iwd The ISO Week Date.
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult Testcd2woy(DT::YMD const& ymd, DT::ISOWeekDate const& iwd);

  TResult Testhr2hm(double const& hr, DT::JulianDateTime const& time);

private:
  std::string valueStr;

private:
};

} // End namespace DT_Tests.

#endif // DTTESTS_JULIANDATETIMETESTS_H
