/*=========================================================================

  Program:   Date Time Library
  File   :   DateTimeParserTests.cpp

  Copyright(c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "DateTimeParserTests.h"


namespace {
/**
 * @param out The output stream
 * @param  data The pair to display in the output stream.
 * @return The data as an output stream.
 *
 */
std::ostream& operator<<(std::ostream& out, const std::pair<int, double>& data)
{
  out << data.first << ", " << data.second;
  return out;
}
} // End anonymous namespace.

namespace TestData {

typedef std::pair<std::string, DT::YMDHMS> TStrDT;
std::vector<TStrDT> strDT{
    {"2013-09-06", DT::YMDHMS(2013, 9, 6, 0, 0, 0)},
    {"2013-09-06 23", DT::YMDHMS(2013, 9, 6, 23, 0, 0)},
    {"2013-09-06 23:59", DT::YMDHMS(2013, 9, 6, 23, 59, 0)},
    {"2013-09-06 23:59:59", DT::YMDHMS(2013, 9, 6, 23, 59, 59)},
    {"2013-09-06 23:59:59.999", DT::YMDHMS(2013, 9, 6, 23, 59, 59.999)},
    {"2013-Sep-06 23:59:59.9995", DT::YMDHMS(2013, 9, 6, 23, 59, 59.9995)},
    {"2013-September-06 23:59:59.9995",
     DT::YMDHMS(2013, 9, 6, 23, 59, 59.9995)},
    {"-2013-09-06", DT::YMDHMS(-2013, 9, 6, 0, 0, 0)},
    {"-2013-09-06 23", DT::YMDHMS(-2013, 9, 6, 23, 0, 0)},
    {"-2013-09-06 23:59", DT::YMDHMS(-2013, 9, 6, 23, 59, 0)},
    {"-2013-09-06 23:59:59", DT::YMDHMS(-2013, 9, 6, 23, 59, 59)},
    {"-2013-09-06 23:59:59.999", DT::YMDHMS(-2013, 9, 6, 23, 59, 59.999)},
    {"-2013-09-06 23:59:59.9995", DT::YMDHMS(-2013, 9, 6, 23, 59, 59.9995)}};

std::vector<std::pair<std::string, int>> parseStrDTFail{
    {std::pair<std::string, int>("", 2)},
    {std::pair<std::string, int>("2013", 3)},
    {std::pair<std::string, int>("2013-09-06 12:51:05:05", 5)},
    {std::pair<std::string, int>("2013-09-06 12.2:51:05.05", 5)},
    {std::pair<std::string, int>("2013-Fik-09", 4)}};

std::vector<std::pair<std::string, int>> parseStrDFail = {
    {std::pair<std::string, int>("", 2)},
    {std::pair<std::string, int>("2013", 3)},
    {std::pair<std::string, int>("2013-09-06 12:51:05:05", 3)},
    {std::pair<std::string, int>("2013-09-06 12.2:51:05.05", 3)},
    {std::pair<std::string, int>("2013-Fik-09", 4)}};

typedef std::pair<std::string, DT::HMS> TStrT;
std::vector<TStrT> strT{{TStrT("23:59:59.5", DT::HMS(23, 59, 59.5))},
                        {TStrT("23:59", DT::HMS(23, 59, 0))},
                        {TStrT("23", DT::HMS(23, 0, 0))},
                        {TStrT("00:59:59.5", DT::HMS(0, 59, 59.5))},
                        {TStrT("00:00:59.5", DT::HMS(0, 0, 59.5))},
                        {TStrT("-23:59:59.5", DT::HMS(-23, 59, 59.5))},
                        {TStrT("-23:59", DT::HMS(-23, 59, 0))},
                        {TStrT("-23", DT::HMS(-23, 0, 0))},
                        {TStrT("-00:59:59.5", DT::HMS(0, -59, 59.5))},
                        {TStrT("-00:00:59.5", DT::HMS(0, 0, -59.5))}};

std::vector<std::pair<std::string, int>> parseStrTFail{
    {std::pair<std::string, int>("", 2)},
    {std::pair<std::string, int>("12:5.7:.3", 5)},
    {std::pair<std::string, int>("12:5:.3", 5)},
    {std::pair<std::string, int>("12:5.7", 5)},
    {std::pair<std::string, int>("5.7", 5)},
    {std::pair<std::string, int>("23:Fik:09", 5)},
    {std::pair<std::string, int>("23:59:59:5", 5)}};

std::vector<std::pair<std::string, int>> parseStrTIFail{
    {std::pair<std::string, int>("", 2)},
    {std::pair<std::string, int>("00:00:00", 6)},
    {std::pair<std::string, int>(" 0d  00:00:.5", 5)},
    {std::pair<std::string, int>(" d  00:00:.5", 6)},
    {std::pair<std::string, int>(" x  00:00:00", 6)},
    {std::pair<std::string, int>(" x", 6)}};

typedef std::pair<std::string, std::pair<int, double>> TStrIntDbl;
std::vector<TStrIntDbl> decimalStringParseData{
    // Should pass:
    {TStrIntDbl("-3100015", std::make_pair(-3100015, 0))},
    {TStrIntDbl("-3100015.25", std::make_pair(-3100015, -0.25))},
    {TStrIntDbl("-3100015.50", std::make_pair(-3100015, -0.5))},
    {TStrIntDbl("-3100015.75", std::make_pair(-3100015, -0.75))},
    {TStrIntDbl("2456751", std::make_pair(2456751, 0))},
    {TStrIntDbl("2456751.25", std::make_pair(2456751, 0.25))},
    {TStrIntDbl("2456751.50", std::make_pair(2456751, 0.5))},
    {TStrIntDbl("2456751.75", std::make_pair(2456751, 0.75))},
    // Should fail:
    {TStrIntDbl("", std::make_pair(0, 0.0))},
    {TStrIntDbl(" ", std::make_pair(0, 0.0))},
    {TStrIntDbl("ABC", std::make_pair(0, 0.0))},
    {TStrIntDbl(".5", std::make_pair(0, -0.5))},
    {TStrIntDbl("-.5", std::make_pair(0, -0.5))},
    // Should pass:
    {TStrIntDbl("0.5", std::make_pair(0, 0.5))},
    {TStrIntDbl("-0.5", std::make_pair(0, -0.5))},
};

typedef std::pair<DT::TimeInterval, std::string> TTIStr;
std::vector<TTIStr> tidt{
    {TTIStr(DT::TimeInterval(0, 0), " 0d")},
    {TTIStr(DT::TimeInterval(0, 0), " 0d  00:00:0")},
    {TTIStr(DT::TimeInterval(-1, -0.25), "-1d -06:00:00.0")},
    {TTIStr(DT::TimeInterval(-1, 0.25), "-1d  06:00:00.0")},
    {TTIStr(DT::TimeInterval(1, -0.25), " 1d -06:00:00.0")},
    {TTIStr(DT::TimeInterval(1, 0.25), " 1d  06:00:00.0")}};

} // End namespace TestData.

using namespace DT_Tests;
using namespace TestData;

DateTimeParserTests::DateTimeParserTests()
{
  this->parser.reset(new DT::DateTimeParser());
}

CommonTests::TResult DateTimeParserTests::TestAll()
{
  // Test Scale (we expect 2.68).
  // std::cout << 2.675 << " " << this->this->parser->Scale(2.675, 2) <<
  // std::endl;

  std::vector<TResult> messages{
      {this->TestParseDecNum()},       {this->TestParseDateTime()},
      {this->TestParseDateTimeFail()}, {this->TestParseDateFail()},
      {this->TestParseTime()},         {this->TestParseTimeFail()},
      {this->TestParseTimeInterval()}, {this->TestParseTimeIntervalFail()}};

  return this->AggregateResults("DateTimeParser Tests.", messages);
}

CommonTests::TResult DateTimeParserTests::TestParseDecNum()
{

  TResult p = this->MakeHeader("Testing parsing a decimal string.", 2);

  // Scale a number by rounding it to a specified number of decimal places.
  auto Scale = [this](double x, unsigned int precision) {
    unsigned int maximumPrecision = 15;
    auto prec = (precision > maximumPrecision) ? maximumPrecision : precision;
    double scale = 1;
    for (unsigned int i = 0; i < prec; ++i)
    {
      scale *= 10.0;
    }
    x = x * scale;
    x = std::round(x);
    return x /= scale;
  };

  auto x = 3100015.75;
  auto y = Scale(x, 1);
  auto z = 3100015.8;
  if (this->isclose(y, z, this->rel_fod))
  {
    std::ostringstream os;
    os << "* Passed testing rounding." << std::endl;
    p.first &= true;
    p.second += os.str();
  }
  else
  {
    std::ostringstream os;
    os << "* Failed, testing rounding: " << std::fixed << x
       << " expected: " << z << " got " << y << std::endl;
    p.first &= false;
    p.second += os.str();
  }
  x = -x;
  y = Scale(x, 1);
  z = -z;
  if (this->isclose(y, z, this->rel_fod))
  {
    std::ostringstream os;
    os << "* Passed testing rounding." << std::endl;
    p.first &= true;
    p.second += os.str();
  }
  else
  {
    std::ostringstream os;
    os << "* Failed, testing rounding: " << std::fixed << x
       << " expected: " << z << " got " << y << std::endl;
    p.first &= false;
    p.second += os.str();
  }

  for (const auto& q : decimalStringParseData)
  {
    auto res = this->parser->ParseDecNum(q.first);
    if (res.first == 1)
    {
      if (res.second != q.second)
      {
        std::ostringstream os;
        os << "* Failed, parsing: " << q.first << " expected: (" << q.second
           << "), got: (" << res.second << " )" << std::endl;
        p.first &= false;
        p.second += os.str();
      }
      else
      {
        std::ostringstream os;
        os << "* Passed: " << q.first << std::endl;
        p.first &= true;
        p.second += os.str();
      }
    }
    else
    {
      if (q.first.empty())
      {
        std::ostringstream os;
        os << "* Fail is expected for parsing empty string." << std::endl;
        p.first &= true;
        p.second += os.str();
        continue;
      }
      if (q.first == "ABC")
      {
        std::ostringstream os;
        os << "* Fail is expected for parsing non-decimal string." << std::endl;
        p.first &= true;
        p.second += os.str();
        continue;
      }
      if (q.first == ".5" || q.first == "-.5" || q.first == " ")
      {
        std::ostringstream os;
        os << "* Fail is expected for parsing: " << q.first << std::endl;
        p.first &= true;
        p.second += os.str();
      }
      else
      {
        std::ostringstream os;
        os << "* Failed, parsing: " << q.first << " expected: " << q.second
           << ", got:" << res.second << std::endl;
        p.first &= false;
        p.second += os.str();
      }
    }
  }

  p.second += "\n";
  return p;
}

CommonTests::TResult DateTimeParserTests::TestParseDateTime()
{

  TResult p = this->MakeHeader("Testing expected ParseDateTime().", 2);

  TResult p1(true, "");
  std::ostringstream os;

  for (const auto& q : strDT)
  {
    auto res = this->parser->ParseDateTime(q.first, fmt);
    if (res.first == 1)
    {
      if (q.second.IsClose(res.second, this->rel_seconds))
      {
        os << "* Passed." << std::endl;
        p1.first = true;
        p1.second = os.str();
      }
      else
      {
        os << "* Failed, parsing: " << q.first << " expected: " << q.second
           << " got: " << res.second << std::endl;
        p1.first = false;
        p1.second = os.str();
      }
    }
    else
    {
      os << "* Parse failure parsing: " << q.first
         << ", expected 1, got: " << res.first << " = "
         << this->parser->parserMessages[res.first] << std::endl;
      p1.first = false;
      p1.second = os.str();
    }
    p.first &= p1.first;
    p.second += p1.second;
    os.str("");
  }

  p.second += "\n";
  return p;
}

CommonTests::TResult DateTimeParserTests::TestParseDateTimeFail()
{
  TResult p = this->MakeHeader("Testing expected ParseDateTime() failures.", 2);

  TResult p1(true, "");
  std::ostringstream os;

  for (const auto& q : parseStrDTFail)
  {
    auto res = this->parser->ParseDateTime(q.first, this->fmt);
    if (res.first != q.second)
    {
      os << "* Failed, expected " << q.second << ", got: " << res.first << " - "
         << this->parser->parserMessages[res.first]
         << " The string being parsed is: " << q.first << std::endl;
      p1.first &= false;
      p1.second = os.str();
    }
    else
    {
      os << "* Expected to fail, the string being parsed is: " << q.first
         << std::endl;
      p1.first = true;
      p1.second = os.str();
    }
    p.first &= p1.first;
    p.second += p1.second;
    os.str("");
  }

  p.second += "\n";
  return p;
}

CommonTests::TResult DateTimeParserTests::TestParseDateFail()
{
  TResult p = this->MakeHeader("Testing expected ParseDate() failures.", 2);

  TResult p1(true, "");
  std::ostringstream os;

  for (const auto& q : parseStrDFail)
  {
    auto res = this->parser->ParseDate(q.first, this->fmt);
    if (res.first != q.second)
    {
      os << "* Failed, expected " << q.second << ", got: " << res.first << " - "
         << this->parser->parserMessages[res.first]
         << " The string being parsed is: " << q.first << std::endl;
      p1.first &= false;
      p1.second = os.str();
    }
    else
    {
      os << "* Expected to fail, the string being parsed is: " << q.first
         << std::endl;
      p1.first = true;
      p1.second = os.str();
    }
    p.first &= p1.first;
    p.second += p1.second;
    os.str("");
  }

  p.second += "\n";
  return p;
}

CommonTests::TResult DateTimeParserTests::TestParseTime()
{

  TResult p = this->MakeHeader("Testing expected ParseTime().", 2);

  TResult p1(true, "");
  std::ostringstream os;

  for (const auto& q : strT)
  {
    auto res = this->parser->ParseTime(q.first, this->fmt);
    if (res.first == 1)
    {
      if (q.second.IsClose(res.second, this->rel_seconds))
      {
        os << "* Passed." << std::endl;
        p1.first = true;
        p1.second = os.str();
      }
      else
      {
        os << "* Failed, parsing: " << q.first << " expected: " << q.second
           << " got: " << res.second << std::endl;
        p1.first = false;
        p1.second = os.str();
      }
    }
    else
    {
      os << "* Parse failure parsing: " << q.first
         << ", expected 1, got: " << res.first << " = "
         << this->parser->parserMessages[res.first] << std::endl;
      p1.first = false;
      p1.second = os.str();
    }
    p.first &= p1.first;
    p.second += p1.second;
    os.str("");
  }

  p.second += "\n";
  return p;
}

CommonTests::TResult DateTimeParserTests::TestParseTimeFail()
{
  TResult p = this->MakeHeader("Testing expected ParseTime() failures.", 2);

  TResult p1(true, "");
  std::ostringstream os;

  for (const auto& q : parseStrTFail)
  {
    auto res = this->parser->ParseTime(q.first, this->fmt);
    if (res.first != q.second)
    {
      os << "* Failed, expected " << q.second << ", got: " << res.first << " - "
         << this->parser->parserMessages[res.first]
         << " The string being parsed is: " << q.first << std::endl;
      p1.first &= false;
      p1.second = os.str();
    }
    else
    {
      os << "* Expected to fail, the string being parsed is: " << q.first
         << std::endl;
      p1.first = true;
      p1.second = os.str();
    }
    p.first &= p1.first;
    p.second += p1.second;
    os.str("");
  }

  p.second += "\n";
  return p;
}

CommonTests::TResult DateTimeParserTests::TestParseTimeInterval()
{
  TResult p = this->MakeHeader("Testing ParseTimeInterval().", 2);

  TResult p1(true, "");
  std::ostringstream os;

  for (const auto& q : tidt)
  {
    auto res = this->parser->ParseTimeInterval(q.second, this->fmt);
    if (res.first == 1)
    {
      if (res.second.IsClose(q.first, this->rel_fod))
      {
        os << "* Passed." << std::endl;
        p1.first = true;
        p1.second = os.str();
      }
      else
      {
        os << "* Failed, parsing: " << q.second << " expected: " << q.first
           << " got: " << res.second << std::endl;
        p1.first = false;
        p1.second = os.str();
      }
    }
    else
    {
      os << "* Parse failure parsing : " << q.first
         << ", expected 1, got: " << res.first << " = "
         << this->parser->parserMessages[res.first] << std::endl;
      p1.first = false;
      p1.second = os.str();
    }
    p.first &= p1.first;
    p.second += p1.second;
    os.str("");
  }

  p.second += "\n";
  return p;
}

CommonTests::TResult DateTimeParserTests::TestParseTimeIntervalFail()
{
  TResult p =
      this->MakeHeader("Testing expected ParseTimeInterval() failures.", 2);

  TResult p1(true, "");
  std::ostringstream os;

  for (const auto& q : parseStrTIFail)
  {
    auto res = this->parser->ParseTimeInterval(q.first, this->fmt);
    if (res.first != q.second)
    {
      os << "* Failed, expected " << q.second << ", got: " << res.first << " - "
         << this->parser->parserMessages[res.first]
         << " The string being parsed is: " << q.first << std::endl;
      p1.first &= false;
      p1.second = os.str();
    }
    else
    {
      os << "* Expected to fail, the string being parsed is: " << q.first
         << std::endl;
      p1.first = true;
      p1.second = os.str();
    }
    p.first &= p1.first;
    p.second += p1.second;
    os.str("");
  }

  p.second += "\n";
  return p;
}
