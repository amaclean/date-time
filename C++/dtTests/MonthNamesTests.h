/*=========================================================================

  Program:   Date Time Library
  File   :   MonthNamesTests.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef DTTESTS_MONTHNAMESTESTS_H
#define DTTESTS_MONTHNAMESTESTS_H

#pragma once

#include "CommonTests.h"
#include "MonthNames.h"

#include <memory>

namespace DT_Tests {

/**
  A test harness to test the MonthNames class.

  @author Andrew J. P. Maclean
*/
class MonthNamesTests : public CommonTests
{
public:
  MonthNamesTests()
  {
    mn.reset(new DT::MonthNames());
  }

  virtual ~MonthNamesTests() = default;

public:
  /** Run all the tests.
      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestAll();

  /** Run the test for constructors.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestConstructors();

  /** Run the test for assignment.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestAssignment();

  /** Run the tests for the comparison operators.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestComparisonOperators();

  /** Run the tests for testing the month names.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestMonthNames();

private:
  std::unique_ptr<DT::MonthNames> mn;
};

} // End namespace DT_Tests.

#endif // DTTESTS_MONTHNAMESTESTS_H
