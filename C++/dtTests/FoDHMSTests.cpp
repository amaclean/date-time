/*=========================================================================

  Program:   Date Time Library
  File   :   FoDHMSTests.cpp

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "FoDHMSTests.h"
#include "JulianDateTime.h"

using namespace DT_Tests;

CommonTests::TResult FoDHMSTests::TestAll()
{
  std::vector<TResult> messages{{this->TestCalendarDateToJulianDate()},
                                {this->TestJulianDateToCalendarDate()},
                                {this->TestFodHMSJD()},
                                {this->TestJD()},
                                {this->TestJD1()},
                                {this->TestJD2()},
                                {this->TestCalendar()}};

  return this->AggregateResults("FoDHMS Tests.", messages);
}

void FoDHMSTests::Init()
{
  int steps = 7;
  for (int i = 0; i < steps; ++i)
  {
    double jdFoD = i / static_cast<double>(steps);
    double hmsFoD = (jdFoD >= 0.5) ? jdFoD - 0.5 : jdFoD + 0.5;
    this->testFoD.push_back(TDD(jdFoD, hmsFoD));
  }

  // We manually fill this vectors with known values taken from DE431.
  std::vector<std::string> DE431Dates = {
      {"-13200 08 15 -3100015.5"}, {"-13100 01 01 -3063717.5"},
      {"-13100 01 02 -3063716.5"}, {"-4713 12 31       -1.5"},
      {"-4712 01 01       -0.5"},  {"-4712 01 02        0.5"},
      {"-4712 01 03        1.5"},  {" 8000 01 01  4642999.5"},
      {" 8000 01 02  4643000.5"},  {"17191 03 01  8000002.5"},
      {"17191 03 15  8000016.5"}};

  DT::YMDHMS ymdhms;
  double jd = 0;
  std::istringstream iss;
  for (const auto& p : DE431Dates)
  {
    iss.str(p);
    iss >> ymdhms.year >> ymdhms.month >> ymdhms.day >> jd;
    this->testDates.push_back(TDTD(ymdhms, jd));
    iss.clear();
  }
}

CommonTests::TResult FoDHMSTests::TestCalendarDateToJulianDate()
{
  auto calendar = DT::CALENDAR_TYPE::JULIAN_GREGORIAN;
  TResult p = this->MakeHeader(
      "Testing Calendar Date -> (jdn, fod) -> Julian Date.", 2);

  DT::JulianDateTime jdp;
  auto pass = true;
  for (const auto& q : this->testDates)
  {
    jdp.SetDateTime(q.first, calendar);

    if (jdp.GetJDDouble() != q.second)
    {
      pass = false;
      std::ostringstream os;
      os << "* Failed, ";
      os << "for: " << q.first << ", expected: " << std::fixed
         << std::setprecision(1) << std::setw(10) << q.second
         << " got: " << jdp.GetJDDouble() << ", difference: " << std::setw(15)
         << std::setprecision(12) << jdp.GetJDDouble() - q.second << std::endl;
      p.first &= false;
      p.second += os.str();
    }
  }
  if (pass)
  {
    std::ostringstream os;
    os << "* Passed." << std::endl;
    p.first &= true;
    p.second += os.str();
  }

  p.second += "\n";
  return p;
}

CommonTests::TResult FoDHMSTests::TestJulianDateToCalendarDate()
{
  auto calendar = DT::CALENDAR_TYPE::JULIAN_GREGORIAN;
  TResult p = this->MakeHeader(
      "Testing Julian Date -> (jdn, fod) -> Calendar Date.", 2);

  auto pass = true;
  for (const auto& q : this->testDates)
  {
    DT::JulianDateTime jdp(0, q.second);
    auto ymdhms = jdp.GetDateTime(calendar);

    if (!ymdhms.IsClose(q.first, this->rel_seconds))
    {
      pass = false;
      std::ostringstream os;
      os << "* Failed, ";
      os << " for: " << std::fixed << std::setprecision(1) << std::setw(10)
         << q.second << ", expected: " << q.first << " got: " << ymdhms
         << std::endl;
      p.first &= false;
      p.second += os.str();
    }
  }
  if (pass)
  {
    std::ostringstream os;
    os << "* Passed." << std::endl;
    p.first &= true;
    p.second += os.str();
  }

  p.second += "\n";
  return p;
}

CommonTests::TResult FoDHMSTests::TestFodHMSJD()
{
  auto calendar = DT::CALENDAR_TYPE::JULIAN_GREGORIAN;
  TResult p = this->MakeHeader(
      "Testing (jdn, fod) -> (jdn, hms) -> calendar date -> (jdn, fod).", 2);

  auto pass = true;
  for (const auto& q : this->testDates)
  {
    DT::JulianDateTime jdp(0, q.second);
    for (auto r : this->testFoD)
    {
      jdp.SetFoD(r.first);

      int jd = 0;
      int days = 0;
      double fod = 0;
      double hms = 0;
      jdp.FoDToHMS(jdp.GetJDN(), jdp.GetFoD(), days, hms);
      jdp.HMSToFoD(days, hms, jd, fod);
      if (fod >= 0.5)
      {
        // The next day since we started at 12h of the current day.
        jd += 1;
      }
      DT::YMDHMS ymdhms(dc->jd2cdJG(jd), tc->h2hms(r.second * 24.0));
      DT::JulianDateTime jdt1;
      jdt1.SetDateTime(ymdhms, calendar);

      if (!jdp.IsClose(jdt1, this->rel_fod))
      {
        pass = false;
        std::ostringstream os;
        os << "* Failed, ";
        os << "jd: (" << std::setw(8) << jdp.GetJDN() << " " << std::fixed
           << std::setprecision(2) << std::setw(5) << jdp.GetFoD() << ") "
           << " hms: " << r.second << " ";
        os << "days (as hms): (" << std::setw(8) << days << " " << std::fixed
           << std::setprecision(2) << std::setw(5) << hms << ") ";
        os << ymdhms << " " //<< jdt1
           << "jd: (" << std::setw(8) << jdt1.GetJDN() << " " << std::fixed
           << std::setprecision(2) << std::setw(5) << jdt1.GetFoD() << ") "
           << std::endl;
        p.first &= false;
        p.second += os.str();
      }
    }
  }
  if (pass)
  {
    std::ostringstream os;
    os << "* Passed." << std::endl;
    p.first &= true;
    p.second += os.str();
  }

  p.second += "\n";
  return p;
}

CommonTests::TResult FoDHMSTests::TestJD()
{
  auto calendar = DT::CALENDAR_TYPE::JULIAN_GREGORIAN;
  TResult p =
      this->MakeHeader("Testing jd -> jdn, hms -> calendar date -> jd.", 2);

  auto pass = true;
  for (const auto& q : this->testDates)
  {
    for (auto r : this->testFoD)
    {
      DT::YMDHMS ymdhms(dc->jd2cdJG(static_cast<int>(q.second)),
                        tc->h2hms(r.second * 24.0));
      DT::JulianDateTime jdt1;
      jdt1.SetDateTime(ymdhms, calendar);
      auto jdt2 = jdt1.GetJDDouble();

      if (!this->isclose(jdt2, jdt1.GetJDN() + jdt1.GetFoD(), this->rel_fod))
      {
        pass = false;
        std::ostringstream os;
        os << "* Failed, ";
        os << "jd: (" << std::setw(8) << jdt1.GetJDN() << " " << std::fixed
           << std::setprecision(2) << std::setw(5) << jdt1.GetFoD()
           << ") = " << std::fixed << std::setprecision(2) << std::setw(12)
           << jdt1.GetJDN() + jdt1.GetFoD() << " from GetJDDouble(): " << jdt2
           << ", difference: " << std::setw(15) << std::setprecision(12)
           << jdt2 - (jdt1.GetJDN() + jdt1.GetFoD()) << std::endl;
        p.first &= false;
        p.second += os.str();
      }
    }
  }
  if (pass)
  {
    std::ostringstream os;
    os << "* Passed." << std::endl;
    p.first &= true;
    p.second += os.str();
  }

  p.second += "\n";
  return p;
}

CommonTests::TResult FoDHMSTests::TestJD1()
{
  TResult p = this->MakeHeader("Testing jd -> julian date time -> jd.", 2);

  auto pass = true;
  for (const auto& q : this->testDates)
  {
    for (auto r : this->testFoD)
    {
      auto jd = q.second + r.first;
      DT::JulianDateTime jdt(0, jd);
      auto jd1 = jdt.GetJDDouble();

      if (!this->isclose(jd, jd1, this->rel_fod))
      {
        pass = false;
        std::ostringstream os;
        os << "* Failed,";
        os << "jd: (" << std::fixed << std::setprecision(2) << std::setw(12)
           << q.second << " " << std::setw(5) << r.first << ") = " << std::fixed
           << std::setprecision(2) << std::setw(12) << jd << " got: " << jd1
           << ", difference: " << std::setw(15) << std::setprecision(12)
           << jd1 - jd << std::endl;
        p.first &= false;
        p.second += os.str();
      }
    }
  }
  if (pass)
  {
    std::ostringstream os;
    os << "* Passed." << std::endl;
    p.first &= true;
    p.second += os.str();
  }

  p.second += "\n";
  return p;
}

CommonTests::TResult FoDHMSTests::TestJD2()
{
  auto calendar = DT::CALENDAR_TYPE::JULIAN_GREGORIAN;
  TResult p = this->MakeHeader("Testing jd -> calendar -> jd.", 2);

  auto pass = true;
  for (const auto& q : this->testDates)
  {
    for (auto r : this->testFoD)
    {
      auto jd = q.second + r.first;
      DT::JulianDateTime jdt(0, jd);
      auto ymdhms = jdt.GetDateTime(calendar);
      DT::JulianDateTime jdt1;
      jdt1.SetDateTime(ymdhms, calendar);
      auto jd1 = jdt.GetJDDouble();

      if (!this->isclose(jd, jd1, this->rel_fod))
      {
        pass = false;
        std::ostringstream os;
        os << "* Failed, ";
        os << " jd: (" << std::fixed << std::setprecision(2) << std::setw(12)
           << q.second << " " << std::setw(5) << r.first << ") = " << std::fixed
           << std::setprecision(2) << std::setw(12) << jd << " got: " << jd1
           << ", difference: " << std::setw(15) << std::setprecision(12)
           << jd1 - jd << std::endl;
        p.first &= false;
        p.second += os.str();
      }
    }
  }
  if (pass)
  {
    std::ostringstream os;
    os << "* Passed." << std::endl;
    p.first &= true;
    p.second += os.str();
  }

  p.second += "\n";
  return p;
}

CommonTests::TResult FoDHMSTests::TestCalendar()
{
  auto calendar = DT::CALENDAR_TYPE::JULIAN_GREGORIAN;
  TResult p = this->MakeHeader("Testing calendar -> jd -> calendar.", 2);

  auto pass = true;
  for (const auto& q : this->testDates)
  {
    for (auto r : this->testFoD)
    {
      auto ymdhms(q.first);
      // Using fod_jd[i] as the hms times.
      // This makes times run from 0h to < 24h.
      auto hms = tc->h2hms(r.first * 24.0);
      ymdhms.SetHMS(hms.hour, hms.minute, hms.second);

      DT::JulianDateTime jdt1;
      jdt1.SetDateTime(ymdhms, calendar);
      auto ymdhms0 = jdt1.GetDateTime(calendar);

      if (!ymdhms.IsClose(ymdhms0, this->rel_seconds))
      {
        pass = false;
        std::ostringstream os;
        os << "* Failed, expected: " << ymdhms << " got: " << ymdhms0
           << std::endl;
        p.first &= false;
        p.second += os.str();
      }
    }
  }
  if (pass)
  {
    std::ostringstream os;
    os << "* Passed." << std::endl;
    p.first &= true;
    p.second += os.str();
  }

  p.second += "\n";
  return p;
}
