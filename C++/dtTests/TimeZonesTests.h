/*=========================================================================

  Program:   Date Time Library
  File   :   TimeZonesTests.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef DTTESTS_TIMEZONESTESTS_H
#define DTTESTS_TIMEZONESTESTS_H

#pragma once

#include "CommonTests.h"
#include "TimeZones.h"

#include <memory>

namespace DT_Tests {

/**
  A test harness to test the TimeZones class.

  @author Andrew J. P. Maclean
*/
class TimeZonesTests : public CommonTests
{
public:
  TimeZonesTests()
  {
    this->tz.reset(new DT::TimeZones());
  }

  virtual ~TimeZonesTests() = default;

public:
  /** Run all the tests.
      @return A pair consisting of true (if the test passed)
       or false and a message.
  */
  TResult TestAll();

  /** Run the test for constructors.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestConstructors();

  /** Run the test for assignment.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestAssignment();

  /** Run the tests for setting the members.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestGet();

  /** Run the tests for the comparison operators.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestComparisonOperators();

  /** Run the tests for testing the time zones.
   *
   * @return A pair consisting of true (if the test passed)
   *  or false and a message.
   */
  TResult TestTimeZones();

private:
  std::unique_ptr<DT::TimeZones> tz;
};

} // End namespace DT_Tests.

#endif // DTTESTS_TIMEZONESTESTS_H
