/*=========================================================================

  Program:   Date Time Library
  File   :   DayNamesTests.cpp

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "DayNamesTests.h"

using namespace DT_Tests;

CommonTests::TResult DayNamesTests::TestAll()
{
  std::vector<TResult> messages{{this->TestConstructors()},
                                {this->TestAssignment()},
                                {this->TestComparisonOperators()},
                                {this->TestDayNames()}};

  return this->AggregateResults("DayNames Tests.", messages);
}

CommonTests::TResult DayNamesTests::TestConstructors()
{
  TResult p = this->MakeHeader("Testing constructors.", 2);

  p.second += "* DayNames() ";
  auto m1(*this->dn);
  auto p1 = this->PassFail(m1 == *this->dn);
  p.first &= p1.first;
  p.second += "<br>" + p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult DayNamesTests::TestAssignment()
{
  TResult p = this->MakeHeader("Testing assignment.", 2);

  p.second += "* = ";
  auto m1 = *this->dn;
  auto p1 = this->PassFail(m1 == *this->dn);
  p.first &= p1.first;
  p.second += "<br>" + p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult DayNamesTests::TestComparisonOperators()
{
  TResult p = this->MakeHeader("Testing comparison operators.", 2);

  p.second += "* == ";
  auto mn1(*this->dn);
  auto p1 = this->PassFail(mn1 == *this->dn);
  p.first &= p1.first;
  p.second += "<br>" + p1.second;

  p.second += "* != ";
  mn1.SetDayName(1, "FIIK");
  p1 = this->PassFail(mn1 != *this->dn);
  p.first &= p1.first;
  p.second += "<br>" + p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult DayNamesTests::TestDayNames()
{
  TResult p = this->MakeHeader("Testing day names.", 2);

  TResult p1;

  p.second += "* Day names and abbreviations.\n";
  p1.first = true;
  std::string fail = "";
  auto ok = true;

  auto abbrevLength = 3;
  for (int i = 1; i < 8; ++i)
  {
    auto name = this->dn->GetDayName(i);
    auto abbrev = this->dn->GetAbbreviatedDayName(i, abbrevLength);
    if (name.substr(0, abbrevLength) != abbrev)
    {
      ok = false;
      fail += "  - (" + name + ", " + abbrev + ")\n";
    }
  }
  std::ostringstream os;
  if (ok)
  {
    os << "<br>Passed." << std::endl;
    p1.first = true;
    p1.second = os.str();
  }
  else
  {
    os << "<br>Failed day names:\n" << fail << std::endl;
    p1.first = false;
    p1.second = os.str();
  }
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  ok = true;
  fail.clear();
  p.second += "* Day names and unabbreviated names.\n";
  p1.first = true;
  for (int i = 1; i < 8; ++i)
  {
    auto name = this->dn->GetDayName(i);
    auto abbrev = this->dn->GetAbbreviatedDayName(i, name.size());
    if (name != abbrev)
    {
      ok = false;
      fail += "  - (" + name + ", " + abbrev + ")\n";
    }
  }
  if (ok)
  {
    os << "<br>Passed." << std::endl;
    p1.first = true;
    p1.second = os.str();
  }
  else
  {
    os << "<br>Failed day names:\n" << fail << std::endl;
    p1.first = false;
    p1.second = os.str();
  }
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* Day Name range error.\n";
  p1 = this->PassFail(this->dn->GetDayName(0).empty() &&
                      this->dn->GetDayName(8).empty());
  p.first &= p1.first;
  p.second += "<br>" + p1.second;

  p.second += "* Abbreviated Day Name range error.\n";
  p1 =
      this->PassFail(this->dn->GetAbbreviatedDayName(0, abbrevLength).empty() &&
                     this->dn->GetAbbreviatedDayName(8, abbrevLength).empty());
  p.first &= p1.first;
  p.second += "<br>" + p1.second;

  p.second += "* Set Day Name.\n";
  auto dayName = this->dn->GetDayName(5);
  auto newName = "Day 5";
  this->dn->SetDayName(5, newName);
  p1 = this->PassFail(this->dn->GetDayName(5) == newName);
  this->dn->SetDayName(5, dayName);
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}
