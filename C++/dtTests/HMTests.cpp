/*=========================================================================

  Program:   Date Time Library
  File   :   HMTests.cpp

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "HMTests.h"

using namespace DT_Tests;

CommonTests::TResult HMTests::TestAll()
{
  std::vector<TResult> messages{{this->TestConstructors()},
                                {this->TestSet()},
                                {this->TestComparisonOperators()},
                                {this->TestAssignment()},
                                {this->TestStringOutput()}};

  return this->AggregateResults("HM Tests.", messages);
}

CommonTests::TResult HMTests::TestConstructors()
{
  TResult p = this->MakeHeader("Testing constructors.", 2);

  p.second += "* HM() ";
  DT::HM h;
  auto p1 = this->PassFail(h.hour == 0 && h.minute == 0.0);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* HM(h,m) ";
  DT::HM h1(this->hour, this->minute);
  p1 = this->PassFail(h1.hour == this->hour && h1.minute == this->minute);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* HM(hm) ";
  DT::HM h2(h1);
  p1 = this->PassFail(h1.hour == h2.hour && h1.minute == h2.minute);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult HMTests::TestSet()
{
  TResult p = this->MakeHeader("Testing setting members.", 2);

  p.second += "* SetHM(h,m) ";
  DT::HM h1(this->hour, this->minute);
  DT::HM h2;
  h2.SetHM(this->hour, this->minute);
  auto p1 = this->PassFail(h1.hour == h2.hour && h1.minute == h2.minute);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult HMTests::TestComparisonOperators()
{
  TResult p = this->MakeHeader("Testing comparison operators.", 2);

  p.second += "* == ";
  DT::HM h1(this->hour, this->minute);
  auto p1 = this->PassFail(h1 == *this->hm);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "* &asymp; ";
  DT::HM h2(this->hour, this->minute - 0.001);
  p1 = this->PassFail(this->hm->IsClose(h2, 0.0001));
  p.first &= p1.first;
  p.second += "\n<br>" + p1.second;

  // Expect a fail here
  p1 = this->PassFail(!this->hm->IsClose(h2, 0.00001));
  p.first &= p1.first;
  p.second += "<br>" + p1.second;

  p.second += "* != ";
  h1.minute -= 1;
  p1 = this->PassFail(h1 != *this->hm);
  p.first &= p1.first;

  p.second += p1.second;
  DT::HM a(13, 7.2);
  DT::HM b(14, 7.2);
  DT::HM c(13, 8.2);

  p.second += "* &lt; and &le; ";
  p1 = this->PassFail(a < b && a < c && a <= b && a <= c && a <= a &&
                      !(a > b) && !(a >= b));
  p.first &= p1.first;
  p.second += p1.second;

  DT::HM e(13, 7.2);
  DT::HM f(12, 7.2);
  DT::HM g(13, 6.2);

  p.second += "* &gt; and &ge; ";
  p1 = this->PassFail(e > f && e > g && e >= f && e >= g && e >= e &&
                      !(e < f) && !(e <= f));
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult HMTests::TestAssignment()
{
  TResult p = this->MakeHeader("Testing assignment.", 2);

  p.second += "* = ";
  auto h1 = *this->hm;
  auto p1 = this->PassFail(h1.hour == this->hm->hour &&
                           h1.minute == this->hm->minute);
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}

CommonTests::TResult HMTests::TestStringOutput()
{
  TResult p = this->MakeHeader("Testing <<.", 2);

  std::ostringstream os;
  os << *this->hm;
  auto p1 = this->Compare(os.str(), this->valueStr, "* ");
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  DT::HM h1(-23, 59.01);
  std::string expected = "-23:59.010000000000";
  os << h1;
  p1 = this->Compare(os.str(), expected, "* ");
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  h1.SetHM(0, -59.01);
  os << h1;
  expected = "-00:59.010000000000";
  p1 = this->Compare(os.str(), expected, "* ");
  os.str("");
  p.first &= p1.first;
  p.second += p1.second;

  p.second += "\n";
  return p;
}
