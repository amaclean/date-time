
#include "DateConversionsTests.h"
#include "DayNamesTests.h"
#include "FoDHMSTests.h"
#include "HMSTests.h"
#include "HMTests.h"
#include "ISOWeekDateTests.h"
#include "JDTests.h"
#include "MonthNamesTests.h"
#include "TimeConversionsTests.h"
#include "TimeIntervalTests.h"
#include "TimeZonesTests.h"
#include "YMDHMSTests.h"
#include "YMDTests.h"

#include "DateTimeFormatterTests.h"
#include "DateTimeParserTests.h"
#include "JulianDateTimeTests.h"

#include <chrono>
#include <cstdlib>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <limits>
#include <string>
#include <vector>

typedef std::numeric_limits<double> dblLim;

using namespace DT_Tests;

int main(void)
{
  std::chrono::time_point<std::chrono::system_clock> start, end;
  start = std::chrono::system_clock::now();

  DateConversionsTests dc;
  DayNamesTests dn;
  FoDHMSTests fodhms;
  HMTests hm;
  HMSTests hms;
  ISOWeekDateTests iwd;
  JDTests jd;
  MonthNamesTests mn;
  TimeConversionsTests tc;
  TimeIntervalTests ti;
  TimeZonesTests tz;
  YMDTests ymd;
  YMDHMSTests ymdhms;

  JulianDateTimeTests jdt;
  DateTimeFormatterTests dtf;
  DateTimeParserTests dtp;

  std::vector<std::pair<bool, std::string>> messages = {
      {dc.TestAll()},     {dn.TestAll()},  {fodhms.TestAll()}, {hm.TestAll()},
      {hms.TestAll()},    {iwd.TestAll()}, {jd.TestAll()},     {mn.TestAll()},
      {tc.TestAll()},     {ti.TestAll()},  {tz.TestAll()},     {ymd.TestAll()},
      {ymdhms.TestAll()},

      {jdt.TestAll()},    {dtf.TestAll()}, {dtp.TestAll()}};

  end = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end - start;
  std::time_t start_time = std::chrono::system_clock::to_time_t(start);
  std::time_t end_time = std::chrono::system_clock::to_time_t(end);

  bool ok = true;
  for (const auto& q : messages)
  {
    ok &= q.first;
    if (!ok)
    {
      break;
    }
  }

  auto timeStr = [](const time_t& t) {
    tm tm_t{};
#if defined(WIN32)
    errno_t ern;
    ern = localtime_s(&tm_t, &t);
#else
    tm tm_info{};
    tm_info = *localtime_r(&t, &tm_t);
#endif
    // tm_t = *localtime(&t);
    std::ostringstream os;
    os << std::put_time(&tm_t, "%F %T%zZ");
    return os.str();
  };

  if (!ok)
  {
    //std::cout << "# Some tests failed.\n" << std::endl;
    std::cout << "Some tests failed.\n" << std::endl;
    for (const auto& q : messages)
    {
      if (!q.first)
      {
        std::cout << q.second << std::endl;
      }
    }
    std::cout << "\nStarted  computation at " << timeStr(start_time)
              << "\nFinished computation at " << timeStr(end_time)
              << "\nElapsed time: " << elapsed_seconds.count() << "s\n";
    return EXIT_FAILURE;
  }
  std::cout << "All tests passed." << std::endl;
  std::cout << "\nStarted  computation at " << timeStr(start_time)
            << "\nFinished computation at " << timeStr(end_time)
            << "\nElapsed time: " << elapsed_seconds.count() << "s\n";
  return EXIT_SUCCESS;
}
