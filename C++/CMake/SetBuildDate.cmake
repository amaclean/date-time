#-----------------------------------------------------------------------------
# Determine a build date.
# Get the build date
try_run(
  TEST_DID_RUN
  TEST_DID_COMPILE
  "${CMAKE_BINARY_DIR}"
  "${CMAKE_FILE_DIR}/date_time.cpp"
  OUTPUT_VARIABLE TEST_BUILD_DATE
)
if(TEST_DID_COMPILE AND NOT TEST_DID_RUN)
  string(REGEX MATCH "[0-9][0-9][0-9][0-9]/[0-9][0-9]/[0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9] [A-Z][A-Z][A-Z]" CMAKE_${PROJECT_NAME}_BUILD_DATE "${TEST_BUILD_DATE}")
else()
  set(CMAKE_${PROJECT_NAME}_BUILD_DATE "")
endif()
#message("CMAKE_${PROJECT_NAME}_BUILD_DATE: ${CMAKE_${PROJECT_NAME}_BUILD_DATE}")
