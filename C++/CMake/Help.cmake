#-----------------------------------------------------------------------------
# Help
# Expects HELP_EN_FILES and HELP_ES_FILES to contain lists of files to be copied.
# Define HELP_EN_FILES and HELP_ES_FILES before including this script.
#
#

set(HELP_DEST_DIR ${EXECUTABLE_OUTPUT_PATH}/help )
if(APPLE)
  # ${CONFIGURATION} - an XCode variable that identifies the build configuration
  #  (for example, Debug or Release) the target uses to generate the product.
  # Note: ninja does not recognise ${CONFIGURATION} whereas make treats it as
  #       being empty.
  if(CMAKE_MAKE_PROGRAM MATCHES "ninja$")
    set(HELP_DEST_DIR ${EXECUTABLE_OUTPUT_PATH}/${KIT}.app/Contents/MacOS/help )
  else()
    set(HELP_DEST_DIR ${EXECUTABLE_OUTPUT_PATH}/\${CONFIGURATION}/${KIT}.app/Contents/MacOS/help )
  endif()

  add_custom_target(CopyENHelp
                    ALL
                    echo "Copying english help files ..."
                    DEPENDS ${KIT})
  foreach(file ${HELP_EN_FILES})
    get_filename_component(fn ${file} NAME)
    set(tgt ${HELP_DEST_DIR}/en/${fn})
    set(src ${file})
    add_custom_command(
      TARGET CopyENHelp
      COMMAND ${CMAKE_COMMAND}
      ARGS -E copy_if_different ${src} ${tgt}
      COMMENT "source copy english help"
      POST_BUILD
    )
    endforeach(file)

  add_custom_target(CopyESHelp
                    ALL
                    echo "Copia de archivos de ayuda español ..."
                    DEPENDS ${KIT})
  foreach(file ${HELP_ES_FILES})
    get_filename_component(fn ${file} NAME)
    set(tgt ${HELP_DEST_DIR}/es/${fn})
    set(src ${file})
    add_custom_command(
      TARGET CopyESHelp
      COMMAND ${CMAKE_COMMAND}
      ARGS -E copy_if_different ${src} ${tgt}
      COMMENT "source copy spanish help"
      POST_BUILD
    )
    endforeach(file)

else(APPLE)

  add_custom_target(CopyENHelp
                    ALL
                    echo "Copying english help files ...")
  foreach(file ${HELP_EN_FILES})
    get_filename_component(fn ${file} NAME)
    set(tgt ${HELP_DEST_DIR}/en/${fn})
    set(src ${file})
    add_custom_command(
      TARGET CopyENHelp
      COMMAND ${CMAKE_COMMAND}
      ARGS -E copy_if_different ${src} ${tgt}
      COMMENT "source copy english help"
      POST_BUILD
    )
    endforeach(file)

  add_custom_target(CopyESHelp
                    ALL
                    echo "Copia de archivos de ayuda español ...")
  foreach(file ${HELP_ES_FILES})
    get_filename_component(fn ${file} NAME)
    set(tgt ${HELP_DEST_DIR}/es/${fn})
    set(src ${file})
    add_custom_command(
      TARGET CopyESHelp
      COMMAND ${CMAKE_COMMAND}
      ARGS -E copy_if_different ${src} ${tgt}
      COMMENT "source copy spanish help"
      POST_BUILD
    )
    endforeach(file)

  # Install help.
  install(FILES ${HELP_EN_FILES}
    DESTINATION bin/help/en
    CONFIGURATIONS Debug Release
    COMPONENT Runtime
  )

  install(FILES ${HELP_ES_FILES}
    DESTINATION bin/help/es
    CONFIGURATIONS Debug Release
    COMPONENT Runtime
  )

endif(APPLE)
