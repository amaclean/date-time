option(BUILD_TESTING
  "Build the tests."
  OFF)

option(BUILD_NON_QT_EXAMPLES
  "Build the examples not using QT."
  OFF)

option(BUILD_QT_EXAMPLES
  "Build the examples using QT."
  OFF)
