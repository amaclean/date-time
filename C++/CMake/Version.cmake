# Version number components.
set(SRC_MAJOR_VERSION 4)
set(SRC_MINOR_VERSION 0)
set(SRC_BUILD_VERSION 20250131)

if (NOT SRC_MINOR_VERSION LESS 100)
  message(FATAL_ERROR
    "The minor version number cannot exceed 100 without changing "
    "`VER_VERSION_CHECK`.")
endif ()

if (NOT SRC_BUILD_VERSION LESS 100000000)
  message(FATAL_ERROR
    "The build version number cannot exceed 100000000 without changing "
    "`VER_VERSION_CHECK`.")
endif ()

