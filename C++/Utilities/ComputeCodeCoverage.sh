#!/bin/bash

#==========================================================================
#
#   Modified from VTK/Utilities/Maintenance/computeCodeCoverageLocally.sh
#
#==========================================================================*/

if [[ $1 == "--help" ]]
then
echo "      "
echo "  How to use this script      "
echo "      "
echo "   0)  Use Linux or Mac, "
echo "        - install lcov"
echo "        - use the gcc compiler"
echo "      "
echo "   1)  Add the CMake flags:      "
echo "      "
echo "      CMAKE_CXX_FLAGS:STRING=-g -O0  -fprofile-arcs -ftest-coverage      "
echo"       CMAKE_CXX_FLAGS_DEBUG:STRING=-g -O0  -fprofile-arcs -ftest-coverage"
echo "      CMAKE_C_FLAGS:STRING= -g -O0  -fprofile-arcs -ftest-coverage       "
echo"       CMAKE_C_FLAGS_DEBUG:STRING= -g -O0  -fprofile-arcs -ftest-coverage "
echo "      "
echo "   2)  Compile the Date Time Library for Debug      "
echo "      "
echo "                     CMAKE_BUILD_TYPE  Debug      "
echo "      "
echo "   3)  From the TOP of the binary directory type the path to this script "
echo "       in the Date Time source tree.  This will run all tests in Date Time "
echo "       and generate code coverage for the entire library."
echo "       The code coverage report will be generated in HTML"
echo "        and can be opened with your favorite browser.     "
echo "    "
echo "       For example, "
echo "    "
echo "          In Linux, you can do        firefox  ./index.html     "
echo "          In Mac,   you can do        open     ./index.html     "
echo "    "

else

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $SCRIPT_DIR
DESTINATION_DIR="$(pwd)"
echo $DESTINATION_DIR
#==========================================================================
echo -n "Please wait while lcov deletes all .da files in . and subdirectories..."
lcov --quiet --directory . --zerocounters
ctest
echo "Done"
echo -n "Please wait while lcov captures the coverage data..."
#
# Some compilers (e.g. clang) place the .gcda files in the wrong directory
#
${SCRIPT_DIR}/fixcoverage.py
cd $DESTINATION_DIR
lcov --quiet --directory . --capture --output-file app.info
echo "Done"
echo -n "Please wait while lcov removes coverage for some files..."
lcov --quiet --remove app.info '*dt_test*' '*Examples*' '*Testing*'  '*Utilities*' '/usr/*' --output-file  app.info2
echo "Done"
echo -n "Please wait while genhtml generates an HTML view from lcov coverage data files..."
genhtml --quiet app.info2 >/dev/null 2>&1
echo "Done"
echo "To view results on Linux, type firefox ./index.html"
echo "To view results on Mac, type open ./index.html"

fi
