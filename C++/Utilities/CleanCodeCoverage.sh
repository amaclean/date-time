#!/bin/bash

# Run this from the TOP of the binary directory
# before running make.

find . -type f -name '*.html' -print0 | xargs -0 /bin/rm -f
find . -type f -name '*.gcno' -print0 | xargs -0 /bin/rm -f
find . -type f -name '*.gcda' -print0 | xargs -0 /bin/rm -f
find . -type f -name 'app.info*' -print0 | xargs -0 /bin/rm -f
