/**
 * Purpose: A simple example demonstrating the usage of the date and time
 * library.
 *
 * We parse a date string and output everything that we know about the date.
 *
 * @author Andrew J. P. Maclean
 *
 */
#include "DateTimeFormatter.h"
#include "DateTimeParser.h"

#include <chrono>
#include <format>
#include <iostream>

using namespace DT;

DT::DateTimeFormatter formatter;
DT::DateTimeParser parser;

/**
 * Display some information about a date.
 */
void DateDetails(std::string const& dateToParse)
{
  // Use a lambda function to build our date string.
  auto BuildDateString =
      [](std::map<std::string, std::string>& dates) -> std::string {
    auto res =
        std::format("Details for:         {:s}\n"
                    "Calendar             {:s}\n"
                    "JD (full precision): {:s}\n"
                    "JD (short form):     {:s}\n"
                    "Date:                {:21s}\n"
                    "Day of the year:     {:s}\n"
                    "Day of the week:     {:s} = {:s} or {:s}\n",
                    dates["NamedDate"], dates["Calendar"], dates["JD"],
                    dates["JDShort"], dates["Date"], dates["DoY"], dates["DoW"],
                    dates["DayName"], dates["DayNameAbbrev"]);
    if (!dates["IWD"].empty())
    {
      res += std::format("ISO Day of the week: {:s}\n", dates["IWD"]);
    }
    res += std::format("Leap Year:           {:s}", dates["LeapYear"]);

    return res;
  };

  // Define the formatting parameters and set the calendar.
  // Note: fmt.calendar is JULIAN_GREGORIAN by default.
  DT::DateTimeFormat fmt;
  fmt.dayName = DAY_NAME::UNABBREVIATED;
  fmt.monthName = MONTH_NAME::UNABBREVIATED;
  fmt.precisionSeconds = 3;
  fmt.precisionFoD = 8;

  // Here we just parse the date and output everything about the date,
  // (if the parse was successful).
  auto parsedDate = parser.ParseDateTime(dateToParse, fmt);
  if (parsedDate.first == 1)
  {
    auto jdt = JulianDateTime(parsedDate.second, fmt.calendar);
    auto dates = formatter.GetFormattedDates(jdt, fmt);
    // Output the date and other information about the date.
    std::cout << BuildDateString(dates) << "\n" << std::endl;
  }
  else
  {
    // Why did it fail?
    std::cout << "Parse failed: " << parser.parserMessages[parsedDate.first]
              << std::endl;
  }
}

int main(void)
{
  std::cout << "--------------------------------------------" << std::endl;
  std::cout << "Demonstrating parsing and outputting a date." << std::endl;
  std::cout << "--------------------------------------------" << std::endl;
  // auto dateToParse = "2016-12-31 23:59:59.99997";
  // auto dateToParse = "2016-12-31 23:01:02.99997";
  // auto dateToParse = "2016-12-31 00:01:01.99997";
  auto dateToParse = "1900-December-31 15:12:11.123";
  DateDetails(dateToParse);
  std::cout << "--------------------------------------------" << std::endl;
  auto time_zone = "Australia/Sydney";
  std::cout << std::format("Using the system time and a time zone ({:s}):\n",
                           time_zone);
  auto tz = std::chrono::locate_zone(time_zone);
  auto now_utc = std::chrono::system_clock::now();
  auto now_syd = std::chrono::zoned_time{tz, now_utc};
  auto dateTime = std::format("{:%F %T%Oz}", now_syd);
  std::cout << std::format("ISO Date to parse:   {:s}\n", dateTime)
            << std::endl;
  dateTime = std::format("{:%F %T}", now_syd);
  DateDetails(dateTime);
  std::cout << "--------------------------------------------" << std::endl;

  return EXIT_SUCCESS;
}
