/**
 * Purpose: Demonstrate the usage of the date and time library.
 *
 * The initialisation of the JulianDateTime class along with its usage
 * is demonstrated. How to nicely format the date is also demonstrated
 * using a class called DateTimeFormatter and lambda functions.
 * This example also demonstrates the importance of selecting
 * the correct calendar to use.
 *
 * @author Andrew J. P. Maclean
 *
 */
#include "DateTimeFormatter.h"

#include <chrono>
#include <ctime>
#include <format>
#include <vector>

using namespace DT;

DT::DateTimeFormatter formatter;

/**
 * Display some information about a date.
 */
void DateDetails()
{
  // Use a lambda function to build our date string.
  auto BuildDateString =
      [](std::map<std::string, std::string>& dates) -> std::string {
    auto res =
        std::format("Details for:         {:s}\n"
                    "Calendar             {:s}\n"
                    "JD (full precision): {:s}\n"
                    "JD (short form):     {:s}\n"
                    "Date:                {:21s}\n"
                    "Day of the year:     {:s}\n"
                    "Day of the week:     {:s} = {:s} or {:s}\n",
                    dates["NamedDate"], dates["Calendar"], dates["JD"],
                    dates["JDShort"], dates["Date"], dates["DoY"], dates["DoW"],
                    dates["DayName"], dates["DayNameAbbrev"]);
    if (!dates["IWD"].empty())
    {
      res += std::format("ISO Day of the week: {:s}\n", dates["IWD"]);
    }
    res += std::format("Leap Year:           {:s}", dates["LeapYear"]);

    return res;
  };

  // Define the formatting parameters and set the calendar.
  // Note: fmt.calendar is JULIAN_GREGORIAN by default.
  DT::DateTimeFormat fmt;
  fmt.dayName = DAY_NAME::UNABBREVIATED;
  fmt.monthName = MONTH_NAME::UNABBREVIATED;

  // We instantiate a YMDHMS class to hold the date and time.
  // You can:
  // 1) Individually set the time and date elements in this class
  //    as the members are public.
  // 2) Set either the date or time
  // 3) Set the date and time.
  YMDHMS ymdhms(2012, 12, 31, 15, 12, 11.123);
  // Alternatively construct a JulianDateTime class.
  JulianDateTime jdt(ymdhms, fmt.calendar);
  // Alternatively instantiate the class and set the date and time:
  // JulianDateTime jdt;
  // jdt.SetDateTime(2012,12,31,15,12,11.123, calendar);

  auto dates = formatter.GetFormattedDates(jdt, fmt);

  // Output the date and other information about the date.
  std::cout << BuildDateString(dates) << "\n" << std::endl;

  // Try some other dates.
  fmt.calendar = CALENDAR_TYPE::JULIAN;
  // Use a copy constructor.
  jdt = JulianDateTime(1900, 12, 31, 15, 12, 11.123, fmt.calendar);
  dates = formatter.GetFormattedDates(jdt, fmt);
  std::cout << BuildDateString(dates) << "\n" << std::endl;
  fmt.calendar = CALENDAR_TYPE::GREGORIAN;
  jdt = JulianDateTime(1900, 12, 31, 15, 12, 11.123, fmt.calendar);
  dates = formatter.GetFormattedDates(jdt, fmt);
  std::cout << BuildDateString(dates) << "\n" << std::endl;
  fmt.calendar = CALENDAR_TYPE::JULIAN_GREGORIAN;
  jdt = JulianDateTime(1900, 12, 31, 15, 12, 11.123, fmt.calendar);
  dates = formatter.GetFormattedDates(jdt, fmt);
  std::cout << BuildDateString(dates) << std::endl;
}

/**
 * Illustrate the differences between calendars.
 */
void CalenderDifferences()
{

  // Use a lambda function to build our date string.
  auto BuildDateString =
      [&](std::map<std::string, std::string> dates) -> std::string {
    std::ostringstream os;
    os << "For JD: " << dates["JDShort"] << " we have:\n"
       << std::setw(16) << std::setiosflags(std::ios_base::left)
       << dates["Calendar"] << ": " << dates["NamedDate"] << "\n"
       << std::setw(18) << " " << dates["Date"];

    return os.str();
  };

  auto GetDates = [&](JulianDateTime jdt, std::vector<DateTimeFormat> fmt)
      -> std::map<std::string, std::string> {
    std::map<std::string, std::string> res;
    int i = 0;
    for (const auto& p : fmt)
    {
      auto dates = formatter.GetFormattedDates(jdt, p);
      // Pick what we want to put in the result.
      switch (i)
      {
      case 0: {
        res["Calendar"] = dates["Calendar"];
        res["JDShort"] = dates["JDShort"];
        res["Date"] = dates["Date"];
        break;
      }
      case 1:
      default: {
        res["NamedDate"] = dates["NamedDate"];
      }
      }
      ++i;
    }
    return res;
  };

  YMDHMS ymdhms(-4712, 1, 1, 0, 0, 1 / 3.0);
  // Instantiate a JulianDateTime class.
  JulianDateTime jdt;
  // Set the  date and time.
  jdt.SetDateTime(ymdhms, CALENDAR_TYPE::JULIAN_GREGORIAN);

  // Define the formatting parameters.
  // We will use two different formats so fill a vector
  // with the formats and modify as appropriate.
  std::vector<DateTimeFormat> fmt(2, DateTimeFormat());
  fmt[1].dayName = DAY_NAME::UNABBREVIATED;
  fmt[1].monthName = MONTH_NAME::UNABBREVIATED;

  std::cout << "It is important to specify the calendar since the dates"
            << "\n may be different for different calendars:" << std::endl;
  // Output the date in several formats for different calendars.
  auto dates = GetDates(jdt, fmt);
  std::cout << BuildDateString(dates) << "\n" << std::endl;
  // See how the date has changed here.
  fmt[1].calendar = fmt[0].calendar = CALENDAR_TYPE::GREGORIAN;
  dates = GetDates(jdt, fmt);
  std::cout << BuildDateString(dates) << "\n" << std::endl;
  // It is easy to set a different date and time, here we just change the year.
  ymdhms.year = 2012;
  jdt.SetDateTime(ymdhms, CALENDAR_TYPE::JULIAN_GREGORIAN);
  // Use the Julian Calendar then the Gregorian Calendar.
  fmt[1].calendar = fmt[0].calendar = CALENDAR_TYPE::GREGORIAN;
  dates = GetDates(jdt, fmt);
  std::cout << BuildDateString(dates) << "\n" << std::endl;
  fmt[1].calendar = fmt[0].calendar = CALENDAR_TYPE::JULIAN;
  dates = GetDates(jdt, fmt);
  std::cout << BuildDateString(dates) << std::endl;
}

int main(void)
{
  auto timeStr = [](const time_t& t) {
    tm tm_t{};
#if defined(WIN32)
    errno_t ern;
    ern = localtime_s(&tm_t, &t);
#else
    tm tm_info{};
    tm_info = *localtime_r(&t, &tm_t);
#endif
    // tm_t = *localtime(&t);
    std::ostringstream os;
    os << std::put_time(&tm_t, "%F %T%zZ");
    return os.str();
  };

  std::chrono::time_point<std::chrono::system_clock> start, end;
  start = std::chrono::system_clock::now();
  std::cout << "--------------------------------------------" << std::endl;
  std::cout << "Demonstrating details of dates." << std::endl;
  std::cout << "--------------------------------------------" << std::endl;
  DateDetails();
  std::cout << "--------------------------------------------" << std::endl;
  std::cout << "Demonstrating differences between calendars." << std::endl;
  std::cout << "--------------------------------------------" << std::endl;
  CalenderDifferences();
  std::cout << "--------------------------------------------" << std::endl;
  end = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end - start;
  std::time_t start_time = std::chrono::system_clock::to_time_t(start);
  std::time_t end_time = std::chrono::system_clock::to_time_t(end);
  std::cout << "\nStarted  computation at " << timeStr(start_time)
            << "\nFinished computation at " << timeStr(end_time)
            << "\nElapsed time: " << elapsed_seconds.count() << "s\n";

  return EXIT_SUCCESS;
}
