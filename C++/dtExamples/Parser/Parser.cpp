/**
 * Purpose: Demonstrate the usage of the date and time helper parsers.
 *
 * @author Andrew J. P. Maclean
 *
 */

#include "DateTimeParser.h"

#include <chrono>
#include <ctime>
#include <memory>
#include <vector>

namespace TestData {
std::vector<std::string> decStrs{{"456.123"}, {"123"}, {"-456.123"}, {"+123"},
                                 {".123"},    {"x"},   {""}};
std::vector<std::string> dateStrs{{"-12-Jul-20"}, {"2015-01-01"}, {"2015-x-01"},
                                  {"2015-1-1"},   {""},           {"1234"},
                                  {"2015 01 01"}};
std::vector<std::string> timeStrs{
    {"12"},       {"-12:12"}, {"-12:15:23.456"}, {"-12-15-23.456"},
    {"12:23.1x"}, {""}};
std::vector<std::string> dateTimeStrs{{"2015-01-01 12"},
                                      {"2015-01-01 -12:12"},
                                      {"2015-01-01 -12:15:23.456"},
                                      {"-12-01-23.456"},
                                      {"-4712-01-01 -12-01-23.456"},
                                      {"2015-01-01 -12:15:23.456x"},
                                      {"2015-01-01"},
                                      {""}};
std::vector<std::string> timeIntervalStrs{{" 0d  00:00:0"},
                                          {"-1d -06:00:00.0"},
                                          {"-1d  06:00:00.0"},
                                          {" 1d -06:00:00.0"},
                                          {" 1d  06:00:00.0"},
                                          {" 12  "},
                                          {"-12 2.3 "},
                                          {"-12 2:3:4x "},
                                          {"-12 6:0:10.3 "},
                                          {"6x -12 6:0:10.3 "},
                                          {""}};
} // End namespace TestData.

using namespace DT;
using namespace TestData;

// This class contains the parsing functions.
std::unique_ptr<DateTimeParser> parser(new DateTimeParser());

DateTimeFormat fmt;

void ParseDecStr()
{
  std::cout << "Parsing decimals." << std::endl;
  for (const auto& p : decStrs)
  {
    auto res = parser->ParseDecNum(p);
    if (res.first == 1)
    {
      std::cout << p << " = (" << res.second.first << "," << res.second.second
                << ")" << std::endl;
    }
    else
    {
      std::cout << p << ": " << parser->parserMessages[res.first] << std::endl;
    }
  }
  std::cout << std::endl;
}

void ParseDateStr()
{
  std::cout << "Parsing dates." << std::endl;
  for (const auto& p : dateStrs)
  {
    auto res = parser->ParseDate(p, fmt);
    if (res.first == 1)
    {
      std::cout << p << " = " << res.second << std::endl;
    }
    else
    {
      std::cout << p << ": " << parser->parserMessages[res.first] << std::endl;
    }
  }
  std::cout << std::endl;
}

void ParseTimeStr()
{
  std::cout << "Parsing times." << std::endl;
  for (const auto& p : timeStrs)
  {
    auto res = parser->ParseTime(p, fmt);
    if (res.first == 1)
    {
      std::cout << p << " = " << res.second << std::endl;
    }
    else
    {
      std::cout << p << ": " << parser->parserMessages[res.first] << std::endl;
    }
  }
  std::cout << std::endl;
}

void ParseDateTimeStr()
{
  std::cout << "Parsing date and times." << std::endl;
  for (const auto& p : dateTimeStrs)
  {
    auto res = parser->ParseDateTime(p, fmt);
    if (res.first == 1)
    {
      std::cout << p << " = " << res.second << std::endl;
    }
    else
    {
      std::cout << p << ": " << parser->parserMessages[res.first] << std::endl;
    }
  }
  std::cout << std::endl;
}

void ParseDateTimeIntervalStr()
{
  std::cout << "Parsing time intervals." << std::endl;
  for (const auto& p : timeIntervalStrs)
  {
    auto res = parser->ParseTimeInterval(p, fmt);
    if (res.first == 1)
    {
      std::cout << p << " = " << res.second << std::endl;
    }
    else
    {
      std::cout << p << ": " << parser->parserMessages[res.first] << std::endl;
    }
  }
}

int main(void)
{
  auto timeStr = [](const time_t& t) {
    tm tm_t{};
#if defined(WIN32)
    errno_t ern;
    ern = localtime_s(&tm_t, &t);
#else
    tm tm_info{};
    tm_info = *localtime_r(&t, &tm_t);
#endif
    // tm_t = *localtime(&t);
    std::ostringstream os;
    os << std::put_time(&tm_t, "%F %T%zZ");
    return os.str();
  };

  std::chrono::time_point<std::chrono::system_clock> start, end;
  start = std::chrono::system_clock::now();
  // If you need to change the parser messages, e.g change the language,
  //  here's how to do it:
  // The last test result will show this message instead of the default
  //  since it is empty.
  parser->parserMessages[2] = "No hay datos.";
  std::cout << "--------------------------------------------" << std::endl;
  std::cout << "Demonstrating the parsers." << std::endl;
  std::cout << "--------------------------------------------" << std::endl;
  ParseDecStr();
  ParseDateStr();
  ParseTimeStr();
  ParseDateTimeStr();
  ParseDateTimeIntervalStr();
  std::cout << "--------------------------------------------" << std::endl;
  end = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end - start;
  std::time_t end_time = std::chrono::system_clock::to_time_t(end);
  std::cout << "Finished computation at " << timeStr(end_time)
            << "\nElapsed time: " << elapsed_seconds.count() << "s\n";

  return EXIT_SUCCESS;
}
