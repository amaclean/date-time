
#include "MainWindow.h"

#include <QString>

int main(int argc, char* argv[])
{
  QApplication app(argc, argv);
  // QString appName = app.objectName();

  MainWindow win;
  win.show();

  return app.exec();
}
