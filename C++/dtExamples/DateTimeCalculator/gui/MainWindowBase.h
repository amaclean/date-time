#ifndef DTEXAMPLES_DATETIMECALCULATOR_GUI_MAINWINDOWBASE_H
#define DTEXAMPLES_DATETIMECALCULATOR_GUI_MAINWINDOWBASE_H

#pragma once

#include "DateTimeFormat.h"
#include "JulianDateTime.h"
#include "TimeZones.h"

#include <QDateTime>

/*
A base class for the Main Window.
*/
class MainWindowBase
{

public:
  MainWindowBase()
    : jdPrecisionMax(15) // nanosecond precision
      ,
      secPrecisionMax(9) // nanosecond precision
      ,
      seconds_in_day(86400.0),
      timeZones(DT::TimeZones())
  {
    this->Initialise();
  }

  virtual ~MainWindowBase() = default;

  MainWindowBase(MainWindowBase const&) = delete;

  MainWindowBase& operator=(MainWindowBase const&) = delete;

public:
  /**
   * This function returns the UTC offset for any timezone.
   * For negative UTC offsets you must override this function and check
   * the boolean "isNegative".
   * I use this for sending requests to a server, if I want to check that
   * its not the day the clock moves forwards or backwards I just call the
   * function twice, once with today's date and then with tomorrow's date.
   * If they both return the same then we know that the clock isn't
   * switching in the next 24 hrs for Daylight Savings time.
   *
   * See: http://www.qtcentre.org/threads/22422-Timezone-offset
   *
   * @param midnightDateTime - The date and time at midnight.
   * @param isNegative - True if the UTC offset is negative.
   * @return The UTC Offset.
   *
   */
  static QTime GetTimeZoneDiff(QDateTime midnightDateTime, bool& isNegative);

  /**
   * @return The current system time as a Julian/Gredorian time.
   */
  DT::JulianDateTime CurrentUTC();

  DT::DateTimeFormat fmt;

  unsigned int jdPrecisionMax;
  unsigned int secPrecisionMax;
  const double seconds_in_day; // 86400.0

  // Time zones.
  DT::TimeZones timeZones;
  std::pair<std::string, double> TZ;
  std::pair<std::string, double> TZAlt;
  // Used in the date difference calculations.
  std::pair<std::string, double> TZA;
  std::pair<std::string, double> TZB;
  // Used in the date sum calculations.
  std::pair<std::string, double> TZS;

  // Date and time
  DT::JulianDateTime utc;
  DT::JD jd;
  // Date difference calculations.
  DT::JulianDateTime utcA;
  DT::JD jdA;
  DT::JulianDateTime utcB;
  DT::JD jdB;
  // Date sum calculations.
  DT::JulianDateTime utcAS;
  DT::TimeInterval tiDTBS;
  DT::JD jdAS;
  DT::TimeInterval tiJDBS;

private:
  void Initialise();
};

#endif // DTEXAMPLES_DATETIMECALCULATOR_GUI_MAINWINDOWBASE_H
