#include "MainWindow.h"

#include "AboutDlg.h"
#include "CalendarDlg.h"
#include "PrecisionDlg.h"

#include "DateTimeParser.h"
#include "JulianDateTime.h"

#include <sstream>

#include <QCloseEvent>
#include <QDateTime>
#include <QDesktopServices>
#include <QDir>
#include <QFile>
#include <QMessageBox>
#include <QPalette>
#include <QStatusBar>
#include <QString>
#include <QStringList>


MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent)
{
  setupUi(this);

  this->formatter.reset(new DT::DateTimeFormatter());
  this->parser.reset(new DT::DateTimeParser());

  this->parser->parserMessages[1] = tr("Parsed Ok.").toStdString();
  this->parser->parserMessages[2] = tr("No data.").toStdString();
  this->parser->parserMessages[3] = tr("Invalid date.").toStdString();
  this->parser->parserMessages[4] = tr("Bad month.").toStdString();
  this->parser->parserMessages[5] = tr("Invalid time.").toStdString();
  this->parser->parserMessages[6] =
      tr("Invalid integer or decimal number.").toStdString();

  this->PopulateTZ();

  this->SetDT();
  this->SetJD();

  this->SetDTAD();
  this->SetDTBD();
  this->SetJDAD();
  this->SetJDBD();
  this->DifferenceDTD();
  this->DifferenceJDD();

  this->SetDTAS();
  this->SetTIDTBS();
  this->SetJDAS();
  this->SetTIJDBS();
  this->SumDTS();
  this->SumJDS();

  permTxt.setText(tr("I'm sorry Dave, I'm afraid I can't do that ..."));
  sb->addPermanentWidget(&permTxt);
  QPalette palette;
  // Midnight Blue.
  palette.setColor(QPalette::WindowText, QColor(25, 25, 112, 255));
  permTxt.setPalette(palette);
  permTxt.setText(tr("Ready."));
  sb->showMessage(tr("Press Help|Instructions."), 10000);

  this->SetUpConnections();

  this->AltTZcomboBox->hide();

  this->tabWidget->setCurrentIndex(0);

  this->UpdatePermTxt();
}

MainWindow::~MainWindow()
{
}

void MainWindow::closeEvent(QCloseEvent* /*event*/)
{
}

void MainWindow::SetUpConnections()
{

  connect(this->actionInstructions, SIGNAL(triggered()), this,
          SLOT(Instructions()));

  connect(actionAbout, SIGNAL(triggered()), this, SLOT(About()));

  connect(this->actionSelectCalendar, SIGNAL(triggered()), this,
          SLOT(ChangeCalendar()));

  connect(this->actionSetDecimalPrecision, SIGNAL(triggered()), this,
          SLOT(ChangePrecision()));

  // ******************** Date Tab *****************************

  connect(TZcomboBox, SIGNAL(currentTextChanged(QString)), this,
          SLOT(GetTZ(QString)));

  connect(AltTZcomboBox, SIGNAL(currentTextChanged(QString)), this,
          SLOT(GetTZAlt(QString)));

  connect(altTZCBox, SIGNAL(stateChanged(int)), this,
          SLOT(AltTZCBoxStateChanged(int)));

  connect(dateTimelineEdit, SIGNAL(editingFinished()), this,
          SLOT(dateTimeLineEditEditingFinished()));

  connect(JDlineEdit, SIGNAL(editingFinished()), this,
          SLOT(JDlineEditEditingFinished()));

  connect(currTimeBtn, SIGNAL(clicked()), this, SLOT(SetCurrentTime()));

  // ******************** Difference Tab ******************************

  connect(ATZDcomboBox, SIGNAL(currentTextChanged(QString)), this,
          SLOT(GetTZAD(QString)));

  connect(BTZDcomboBox, SIGNAL(currentTextChanged(QString)), this,
          SLOT(GetTZBD(QString)));

  connect(dateTimeADlineEdit, SIGNAL(editingFinished()), this,
          SLOT(dateTimeADlineEditEditingFinished()));

  connect(JDADlineEdit, SIGNAL(editingFinished()), this,
          SLOT(JDADlineEditEditingFinished()));

  connect(dateTimeBDlineEdit, SIGNAL(editingFinished()), this,
          SLOT(dateTimeBDlineEditEditingFinished()));

  connect(JDBDlineEdit, SIGNAL(editingFinished()), this,
          SLOT(JDBDlineEditEditingFinished()));

  connect(nowADBtn, SIGNAL(clicked()), this, SLOT(SetCurrentTimeAD()));

  connect(nowBDBtn, SIGNAL(clicked()), this, SLOT(SetCurrentTimeBD()));

  // ******************** Sum Tab *************************************

  connect(TZAScomboBox, SIGNAL(currentTextChanged(QString)), this,
          SLOT(GetTZAS(QString)));

  connect(dateTimeASlineEdit, SIGNAL(editingFinished()), this,
          SLOT(dateTimeASlineEditEditingFinished()));

  connect(JDASlineEdit, SIGNAL(editingFinished()), this,
          SLOT(JDASlineEditEditingFinished()));

  connect(tiDTBSlineEdit, SIGNAL(editingFinished()), this,
          SLOT(tiBSlineEditEditingFinished()));

  connect(tiJDBSlineEdit, SIGNAL(editingFinished()), this,
          SLOT(JDBSlineEditEditingFinished()));

  connect(nowSBtn, SIGNAL(clicked()), this, SLOT(SetCurrentTimeAS()));
}

void MainWindow::PopulateTZ()
{
  QStringList items;
  for (auto p = this->timeZones.GetTimeZonesByValue()->begin();
       p != this->timeZones.GetTimeZonesByValue()->end(); ++p)
  {
    items.append(QString::fromStdString(p->second));
  }

  TZcomboBox->setEditable(false);
  TZcomboBox->addItems(items);
  TZcomboBox->setCurrentIndex(
      TZcomboBox->findText(QString::fromStdString(this->TZ.first)));

  AltTZcomboBox->setEditable(false);
  AltTZcomboBox->addItems(items);
  AltTZcomboBox->setCurrentIndex(
      AltTZcomboBox->findText(QString::fromStdString(this->TZAlt.first)));

  ATZDcomboBox->setEditable(false);
  ATZDcomboBox->addItems(items);
  ATZDcomboBox->setCurrentIndex(
      ATZDcomboBox->findText(QString::fromStdString(this->TZA.first)));

  BTZDcomboBox->setEditable(false);
  BTZDcomboBox->addItems(items);
  BTZDcomboBox->setCurrentIndex(
      BTZDcomboBox->findText(QString::fromStdString(this->TZB.first)));

  TZAScomboBox->setEditable(false);
  TZAScomboBox->addItems(items);
  TZAScomboBox->setCurrentIndex(
      TZAScomboBox->findText(QString::fromStdString(this->TZS.first)));
}

void MainWindow::UpdatePermTxt()
{
  switch (this->fmt.calendar)
  {
  case DT::CALENDAR_TYPE::JULIAN:
    permTxt.setText(tr("Julian"));
    break;
  case DT::CALENDAR_TYPE::GREGORIAN:
    permTxt.setText(tr("Gregorian"));
    break;
  case DT::CALENDAR_TYPE::JULIAN_GREGORIAN:
  default:
    permTxt.setText(tr("Julian/Gregorian"));
    break;
  }
}

void MainWindow::Instructions()
{
  QMessageBox msgBox;

  QDir helpDir = QDir(qApp->applicationDirPath());
  // QString searchDir = helpDir.absoluteFilePath("help");

  // Search a maximum of three levels up from the executable.
  int level = 0;
  while (!helpDir.exists("help") && level < 3)
  {
    helpDir.cdUp();
    ++level;
  }
  if (!helpDir.exists("help"))
  {
    msgBox.setText(tr("Unable to find the help folder."));
    msgBox.exec();
    return;
  }
  // Search for the specific translation.
  QString locale = QLocale::system().name();
  QStringList loc = locale.split("_");
  QStringList hlp;
  hlp << "help" << loc[0];
  if (!helpDir.cd(hlp.join("/")))
  {
    msgBox.setText(tr("Unable to find the path:\n") + helpDir.absolutePath() +
                   "/" + hlp.join("/"));
    msgBox.exec();
    return;
  }

  QString fn = helpDir.absoluteFilePath("Instructions.html");
  QFile ifn(fn);
  if (!ifn.open(QIODevice::ReadOnly | QIODevice::Text))
  {
    msgBox.setText(tr("The file\n") + fn + tr("\ndoes not exist."));
    msgBox.exec();
    return;
  }

  QUrl url(QUrl::fromLocalFile(fn));
  QDesktopServices::openUrl(url);
}

void MainWindow::About()
{
  AboutDlg dialog(this);
  if (dialog.exec())
  {
  }
}

void MainWindow::ChangeCalendar()
{
  CalendarDlg dialog(this);

  switch (this->fmt.calendar)
  {
  case DT::CALENDAR_TYPE::JULIAN: {
    dialog.julianBtn->setChecked(true);
    break;
  }
  case DT::CALENDAR_TYPE::GREGORIAN: {
    dialog.gregorianBtn->setChecked(true);
    break;
  }
  case DT::CALENDAR_TYPE::JULIAN_GREGORIAN:
  default: {
    dialog.julianGregorianBtn->setChecked(true);
    break;
  }
  }
  if (dialog.exec())
  {
    if (dialog.julianBtn->isChecked())
    {
      this->fmt.calendar = DT::CALENDAR_TYPE::JULIAN;
    }
    if (dialog.gregorianBtn->isChecked())
    {
      this->fmt.calendar = DT::CALENDAR_TYPE::GREGORIAN;
    }
    if (dialog.julianGregorianBtn->isChecked())
    {
      this->fmt.calendar = DT::CALENDAR_TYPE::JULIAN_GREGORIAN;
    }

    this->UpdateDisplay();

    this->SetDTAD();
    this->SetDTBD();
    this->SetJDAD();
    this->SetJDBD();
    this->DifferenceDTD();
    this->DifferenceJDD();

    this->SetDTAS();
    this->SetJDAS();
    this->SetTIDTBS();
    this->SetTIJDBS();
    this->SumDTS();
    this->SumJDS();
  }
  this->UpdatePermTxt();
}

void MainWindow::ChangePrecision()
{
  PrecisionDlg dialog(this);
  this->fmt.precisionSeconds =
      (this->fmt.precisionSeconds > this->secPrecisionMax)
      ? this->secPrecisionMax
      : this->fmt.precisionSeconds;
  this->fmt.precisionFoD = (this->fmt.precisionFoD > this->jdPrecisionMax)
      ? this->jdPrecisionMax
      : this->fmt.precisionFoD;
  dialog.SecComboBox->setCurrentIndex(this->fmt.precisionSeconds);
  dialog.JDComboBox->setCurrentIndex(this->fmt.precisionFoD);
  if (dialog.exec())
  {
    this->fmt.precisionSeconds = dialog.SecComboBox->currentIndex();
    this->fmt.precisionFoD = dialog.JDComboBox->currentIndex();

    this->UpdateDisplay();

    this->SetDTAD();
    this->SetDTBD();
    this->SetJDAD();
    this->SetJDBD();
    this->DifferenceDTD();
    this->DifferenceJDD();

    this->SetDTAS();
    this->SetJDAS();
    this->SetTIDTBS();
    this->SetTIJDBS();
    this->SumDTS();
    this->SumJDS();
  }
}

// ******************** Date Tab *****************************

void MainWindow::GetTimeZonesC()
{
  int offset;
  if (this->timeZones.GetTimeZoneValue(TZcomboBox->currentText().toStdString(),
                                       offset))
  {
    this->TZ.first = TZcomboBox->currentText().toStdString();
    this->TZ.second = offset;
  }
  if (this->timeZones.GetTimeZoneValue(
          AltTZcomboBox->currentText().toStdString(), offset))
  {
    this->TZAlt.first = AltTZcomboBox->currentText().toStdString();
    this->TZAlt.second = offset;
  }
}

void MainWindow::GetTZ(const QString& text)
{
  int offset;
  if (this->timeZones.GetTimeZoneValue(text.toStdString(), offset))
  {
    this->TZ.first = text.toStdString();
    this->TZ.second = offset;
    this->GetDT();
    this->UpdateDisplay();
  }
}

void MainWindow::GetTZAlt(const QString& text)
{
  int offset;
  if (this->timeZones.GetTimeZoneValue(text.toStdString(), offset))
  {
    this->TZAlt.first = text.toStdString();
    this->TZAlt.second = offset;
    this->UpdateDisplay();
  }
}

void MainWindow::AltTZCBoxStateChanged(int)
{
  if (this->altTZCBox->isChecked())
  {
    this->AltTZcomboBox->show();
  }
  else
  {
    this->AltTZcomboBox->hide();
  }
  this->UpdateDisplay();
}

void MainWindow::SetDT()
{
  // Local time.
  DT::JulianDateTime lt = this->utc;
  lt += this->TZ.second / this->seconds_in_day;

  this->formatter->maximumPrecision = this->secPrecisionMax;
  std::string str = this->formatter->FormatAsYMDHMS(lt, this->fmt);
  dateTimelineEdit->setText(QString::fromStdString(str));
  this->ShowDate();
}

void MainWindow::SetJD()
{
  this->formatter->maximumPrecision = this->jdPrecisionMax;
  std::string str = this->formatter->FormatJDLong(this->jd, this->fmt);
  JDlineEdit->setText(QString::fromStdString(str));
  this->ShowDate();
}

void MainWindow::GetDT()
{
  std::pair<int, DT::YMDHMS> res = this->parser->ParseDateTime(
      dateTimelineEdit->text().toStdString(), this->fmt);
  if (res.first != 1)
  {
    ParserErrorMsg(res.first, dateTimelineEdit->text());
    return;
  }
  this->utc.SetDateTime(res.second, this->fmt.calendar);
  this->utc -= this->TZ.second / this->seconds_in_day;
  this->jd = this->utc.GetJDFullPrecision();
}

void MainWindow::GetJD()
{
  std::pair<int, std::pair<int, double>> res =
      this->parser->ParseDecNum(JDlineEdit->text().toStdString());
  if (res.first != 1)
  {
    ParserErrorMsg(res.first, JDlineEdit->text());
    return;
  }
  this->jd = DT::JD(res.second.first, res.second.second);
  this->utc.SetDateTime(this->jd.GetJDN(), this->jd.GetFoD());
}

void MainWindow::dateTimeLineEditEditingFinished()
{
  this->GetDT();
  this->UpdateDisplay();
}

void MainWindow::JDlineEditEditingFinished()
{
  this->GetJD();
  this->UpdateDisplay();
}

void MainWindow::ShowDate()
{
  auto BuildDateString =
      [](std::string zone,
         std::map<std::string, std::string>& dates) -> std::string {
    std::ostringstream os;
    os << " <b>" << dates["Calendar"] << "</b>\n";
    os << " <br><b>" << zone << "</b>\n"
       << " <br>" << dates["DayName"] << " " << dates["Date"] << "\n"
       << " <br><i>DoY:</i>&nbsp;" << dates["DoY"] << "\n";
    os << " <br><i>Leap Year:</i>&nbsp;" << dates["LeapYear"] << "\n";
    if (!dates["IWD"].empty())
    {
      os << " <br><i>ISO Day of the week:</i>&nbsp;" << dates["IWD"] << "\n";
    }
    os << " <br><i>JD:</i>&nbsp;" << dates["JD"] << "\n";
    os << " <br><br>\n";
    return os.str();
  };

  DT::JulianDateTime lt(this->utc); // Local time.
  lt += this->TZ.second / this->seconds_in_day;

  std::map<std::string, std::string> dates =
      this->formatter->GetFormattedDates(lt, this->fmt);

  std::ostringstream os;
  os << BuildDateString(this->TZ.first, dates);
  if (altTZCBox->isChecked())
  {
    // Alternate local time.
    DT::JulianDateTime altlt(this->utc);
    altlt += this->TZAlt.second / this->seconds_in_day;

    DT::DateTimeFormat f = this->fmt;
    f.monthName = DT::MONTH_NAME::UNABBREVIATED;
    dates = this->formatter->GetFormattedDates(altlt, f);
    os << BuildDateString(this->TZAlt.first, dates);
  }

  DT::JulianDateTime utcl(this->jd.GetJDN(), this->jd.GetFoD());
  dates = this->formatter->GetFormattedDates(utcl, this->fmt);
  os << BuildDateString("UT", dates);

  dateTimetextBrowser->setText(QString::fromStdString(os.str()));
}

void MainWindow::SetCurrentTime()
{
  this->utc = this->CurrentUTC();
  this->jd = this->utc.GetJDFullPrecision();

  this->UpdateDisplay();
}

void MainWindow::UpdateDisplay()
{
  this->SetDT();
  this->SetJD();
  this->ShowDate();
}

// ******************** Difference Tab ******************************

void MainWindow::GetTimeZonesD()
{
  int offset;
  if (this->timeZones.GetTimeZoneValue(
          ATZDcomboBox->currentText().toStdString(), offset))
  {
    this->TZA.first = ATZDcomboBox->currentText().toStdString();
    this->TZA.second = offset;
  }
  if (this->timeZones.GetTimeZoneValue(
          BTZDcomboBox->currentText().toStdString(), offset))
  {
    this->TZB.first = BTZDcomboBox->currentText().toStdString();
    this->TZB.second = offset;
  }
}

void MainWindow::GetTZAD(const QString& text)
{
  int offset;
  if (this->timeZones.GetTimeZoneValue(text.toStdString(), offset))
  {
    this->TZA.first = text.toStdString();
    this->TZA.second = offset;
    this->GetDTAD();
    this->SetDTAD();
    this->SetJDAD();
    this->DifferenceDTD();
    this->DifferenceJDD();
  }
}

void MainWindow::GetTZBD(const QString& text)
{
  int offset;
  if (this->timeZones.GetTimeZoneValue(text.toStdString(), offset))
  {
    this->TZB.first = text.toStdString();
    this->TZB.second = offset;
    this->GetDTBD();
    this->SetDTBD();
    this->SetJDBD();
    this->DifferenceDTD();
    this->DifferenceJDD();
  }
}

void MainWindow::SetDTAD()
{
  // Local time.
  DT::JulianDateTime lt = this->utcA;
  lt += this->TZA.second / this->seconds_in_day;

  this->formatter->maximumPrecision = this->secPrecisionMax;
  std::string str = this->formatter->FormatAsYMDHMS(lt, this->fmt);
  dateTimeADlineEdit->setText(QString::fromStdString(str));
}

void MainWindow::SetDTBD()
{
  // Local time.
  DT::JulianDateTime lt = this->utcB;
  lt += this->TZB.second / this->seconds_in_day;

  this->formatter->maximumPrecision = this->secPrecisionMax;
  std::string str = this->formatter->FormatAsYMDHMS(lt, this->fmt);
  dateTimeBDlineEdit->setText(QString::fromStdString(str));
}

void MainWindow::SetJDAD()
{
  this->formatter->maximumPrecision = this->jdPrecisionMax;
  std::string str = this->formatter->FormatJDLong(this->jdA, this->fmt);
  JDADlineEdit->setText(QString::fromStdString(str));
}

void MainWindow::SetJDBD()
{
  this->formatter->maximumPrecision = this->jdPrecisionMax;
  std::string str = this->formatter->FormatJDLong(this->jdB, this->fmt);
  JDBDlineEdit->setText(QString::fromStdString(str));
}

void MainWindow::GetDTAD()
{
  std::pair<int, DT::YMDHMS> res = this->parser->ParseDateTime(
      dateTimeADlineEdit->text().toStdString(), this->fmt);
  if (res.first != 1)
  {
    ParserErrorMsg(res.first, dateTimeADlineEdit->text());
    return;
  }
  this->utcA.SetDateTime(res.second, this->fmt.calendar);
  this->utcA -= this->TZA.second / this->seconds_in_day;
  this->jdA = this->utcA.GetJDFullPrecision();
}

void MainWindow::GetDTBD()
{
  std::pair<int, DT::YMDHMS> res = this->parser->ParseDateTime(
      dateTimeBDlineEdit->text().toStdString(), this->fmt);
  if (res.first != 1)
  {
    ParserErrorMsg(res.first, dateTimeBDlineEdit->text());
    return;
  }
  this->utcB.SetDateTime(res.second, this->fmt.calendar);
  this->utcB -= this->TZB.second / this->seconds_in_day;
  this->jdB = this->utcB.GetJDFullPrecision();
}

void MainWindow::GetJDAD()
{
  std::pair<int, std::pair<int, double>> res =
      this->parser->ParseDecNum(JDADlineEdit->text().toStdString());
  if (res.first != 1)
  {
    ParserErrorMsg(res.first, JDADlineEdit->text());
    return;
  }
  this->jdA = DT::JD(res.second.first, res.second.second);
  this->utcA.SetDateTime(this->jdA.GetJDN(), this->jdA.GetFoD());
}

void MainWindow::GetJDBD()
{
  std::pair<int, std::pair<int, double>> res =
      this->parser->ParseDecNum(JDBDlineEdit->text().toStdString());
  if (res.first != 1)
  {
    ParserErrorMsg(res.first, JDBDlineEdit->text());
    return;
  }
  this->jdB = DT::JD(res.second.first, res.second.second);
  this->utcB.SetDateTime(this->jdB.GetJDN(), this->jdB.GetFoD());
}

void MainWindow::dateTimeADlineEditEditingFinished()
{
  this->GetDTAD();
  this->SetDTAD();
  this->SetJDAD();

  this->DifferenceDTD();
  this->DifferenceJDD();
}

void MainWindow::JDADlineEditEditingFinished()
{
  this->GetJDAD();
  this->SetJDAD();
  this->SetDTAD();

  this->DifferenceJDD();
  this->DifferenceDTD();
}

void MainWindow::dateTimeBDlineEditEditingFinished()
{
  this->GetDTBD();
  this->SetDTBD();
  this->SetJDBD();

  this->DifferenceDTD();
  this->DifferenceJDD();
}

void MainWindow::JDBDlineEditEditingFinished()
{
  this->GetJDBD();
  this->SetJDBD();
  this->SetDTBD();

  this->DifferenceJDD();
  this->DifferenceDTD();
}

void MainWindow::DifferenceDTD()
{
  // Difference
  DT::JulianDateTime utcAB = this->utcA - this->utcB;
  // Calculate the difference in days.
  DT::TimeInterval ti = utcAB.GetDeltaT();
  std::string str = this->formatter->FormatTimeIntervalDHMS(ti, this->fmt);
  dateTimeABDlineEdit->setText(QString::fromStdString(str));
}

void MainWindow::DifferenceJDD()
{
  // Calculate the difference in days.
  DT::TimeInterval ti(this->jdA.GetJDN() - this->jdB.GetJDN(),
                      this->jdA.GetFoD() - this->jdB.GetFoD());
  std::string str = this->formatter->FormatTimeInterval(ti, this->fmt);
  JDABDlineEdit->setText(QString::fromStdString(str));
}

void MainWindow::SetCurrentTimeAD()
{
  this->utcA = this->CurrentUTC();
  this->jdA = this->utcA.GetJDFullPrecision();

  this->SetDTAD();
  this->SetJDAD();

  this->DifferenceDTD();
  this->DifferenceJDD();
}

void MainWindow::SetCurrentTimeBD()
{
  this->utcB = this->CurrentUTC();
  this->jdB = this->utcB.GetJDFullPrecision();

  this->SetDTBD();
  this->SetJDBD();

  this->DifferenceDTD();
  this->DifferenceJDD();
}

// ******************** Sum Tab *************************************
void MainWindow::GetTimeZonesS()
{
  int offset;
  if (this->timeZones.GetTimeZoneValue(
          TZAScomboBox->currentText().toStdString(), offset))
  {
    this->TZS.first = TZAScomboBox->currentText().toStdString();
    this->TZS.second = offset;
  }
}

void MainWindow::GetTZAS(const QString& text)
{
  int offset;
  if (this->timeZones.GetTimeZoneValue(text.toStdString(), offset))
  {
    this->TZS.first = text.toStdString();
    this->TZS.second = offset;
    this->GetDTAS();
    this->SetDTAS();
    this->SetJDAS();
  }
}

void MainWindow::SetDTAS()
{
  // Local time.
  DT::JulianDateTime lt = this->utcAS;
  lt += this->TZS.second / this->seconds_in_day;

  std::string str = this->formatter->FormatAsYMDHMS(lt, this->fmt);
  dateTimeASlineEdit->setText(QString::fromStdString(str));
}

void MainWindow::SetTIDTBS()
{
  std::string str =
      this->formatter->FormatTimeIntervalDHMS(this->tiDTBS, this->fmt);
  tiDTBSlineEdit->setText(QString::fromStdString(str));
}

void MainWindow::SetJDAS()
{
  this->formatter->maximumPrecision = this->jdPrecisionMax;
  std::string str = this->formatter->FormatJDLong(this->jdAS, this->fmt);
  JDASlineEdit->setText(QString::fromStdString(str));
}

void MainWindow::SetTIJDBS()
{
  this->formatter->maximumPrecision = this->jdPrecisionMax;
  std::string str =
      this->formatter->FormatTimeInterval(this->tiJDBS, this->fmt);
  tiJDBSlineEdit->setText(QString::fromStdString(str));
}

void MainWindow::GetDTAS()
{
  std::pair<int, DT::YMDHMS> res = this->parser->ParseDateTime(
      dateTimeASlineEdit->text().toStdString(), this->fmt);
  if (res.first != 1)
  {
    ParserErrorMsg(res.first, dateTimeASlineEdit->text());
    return;
  }
  this->utcAS.SetDateTime(res.second, this->fmt.calendar);
  this->utcAS -= this->TZS.second / this->seconds_in_day;
  this->jdAS = this->utcAS.GetJDFullPrecision();
}

void MainWindow::GetTIDTBS()
{
  std::pair<int, DT::TimeInterval> res = this->parser->ParseTimeInterval(
      tiDTBSlineEdit->text().toStdString(), this->fmt);
  if (res.first != 1)
  {
    ParserErrorMsg(res.first, tiDTBSlineEdit->text());
    return;
  }
  this->tiDTBS = res.second;
  this->tiJDBS.SetTimeInterval(this->tiDTBS.GetDeltaDay(),
                               this->tiDTBS.GetDeltaFoD());
}

void MainWindow::GetJDAS()
{
  std::pair<int, std::pair<int, double>> res =
      this->parser->ParseDecNum(JDASlineEdit->text().toStdString());
  if (res.first != 1)
  {
    ParserErrorMsg(res.first, JDASlineEdit->text());
    return;
  }
  this->jdAS = DT::JD(res.second.first, res.second.second);
  this->utcAS.SetDateTime(this->jdAS.GetJDN(), this->jdAS.GetFoD());
}

void MainWindow::GetTIJDBS()
{
  std::pair<int, std::pair<int, double>> res =
      this->parser->ParseDecNum(tiJDBSlineEdit->text().toStdString());
  if (res.first != 1)
  {
    ParserErrorMsg(res.first, tiJDBSlineEdit->text());
    return;
  }
  this->tiJDBS = DT::TimeInterval(res.second.first, res.second.second);
  this->tiDTBS.SetTimeInterval(this->tiJDBS.GetDeltaDay(),
                               this->tiJDBS.GetDeltaFoD());
}

void MainWindow::dateTimeASlineEditEditingFinished()
{
  this->GetDTAS();
  this->SetDTAS();
  this->SetJDAS();

  this->SumDTS();
  this->SumJDS();
}

void MainWindow::tiBSlineEditEditingFinished()
{
  this->GetTIDTBS();
  this->SetTIDTBS();
  this->SetTIJDBS();

  this->SumDTS();
  this->SumJDS();
}

void MainWindow::JDASlineEditEditingFinished()
{
  this->GetJDAS();
  this->SetJDAS();
  this->SetDTAS();

  this->SumDTS();
  this->SumJDS();
}

void MainWindow::JDBSlineEditEditingFinished()
{
  this->GetTIJDBS();
  this->SetTIJDBS();
  this->SetTIDTBS();

  this->SumDTS();
  this->SumJDS();
}

void MainWindow::SumDTS()
{
  // Sum
  DT::JD jdl = this->utcAS.GetJDFullPrecision();
  auto jdn = jdl.GetJDN() + this->tiDTBS.GetDeltaDay();
  auto fod = jdl.GetFoD() + this->tiDTBS.GetDeltaFoD();
  jdl.SetJD(jdn, fod);
  DT::JulianDateTime lt(jdn, fod);
  lt += this->TZS.second / this->seconds_in_day;

  std::string str = this->formatter->FormatAsYMDHMS(lt, this->fmt);
  dateTimeABSlineEdit->setText(QString::fromStdString(str));
}

void MainWindow::SumJDS()
{
  // Calculate the sum in days.
  int jdn = this->jdAS.GetJDN() + this->tiJDBS.GetDeltaDay();
  double fod = this->jdAS.GetFoD() + this->tiJDBS.GetDeltaFoD();
  DT::JD jdl(jdn, fod);
  std::string str = this->formatter->FormatJDLong(jdl, this->fmt);
  JDABSlineEdit->setText(QString::fromStdString(str));
}

void MainWindow::SetCurrentTimeAS()
{
  this->utcAS = this->CurrentUTC();
  this->jdAS = this->utcAS.GetJDFullPrecision();

  this->SetDTAS();
  this->SetJDAS();

  this->SumDTS();
  this->SumJDS();
}

void MainWindow::ParserErrorMsg(int ErrNum, QString theError)
{
  QString errMsg =
      QString().fromStdString(this->parser->parserMessages[ErrNum]);
  errMsg += "\n" + theError;
  QMessageBox msgBox;
  msgBox.setWindowTitle(tr("Parser Error"));
  msgBox.setText(errMsg);
  msgBox.exec();
}
