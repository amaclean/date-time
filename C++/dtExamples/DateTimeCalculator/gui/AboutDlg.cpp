#include "AboutDlg.h"
#include "version.h"

#include <QString>

AboutDlg::AboutDlg(QWidget* parent) : QDialog(parent)
{
  setupUi(this);

  QString help_str = tr("Project Name:\t  ") + PROJECT_NAME + "\n" +
      tr("Program Name: ") + tr("DateTimeCalculator") + "\n" +
      tr("Version:\t ") + VERSION + "\n" + tr("Build Date:\t ") + BUILD_DATE +
      "\n";
  this->textBrowser->append(help_str);
}
