#include "DTInterface.h"
#include "Common.h"

DTInterface* DTInterface::pInstance_ = 0;
bool DTInterface::destroyed_ = false;

QTime DTInterface::GetTimeZoneDiff(QDateTime midnightDateTime, bool& isNegative)
{
  midnightDateTime.setTime(QTime(0, 0));
  QDateTime utc = midnightDateTime.toUTC();
  QDateTime local = midnightDateTime;
  local.setTimeSpec(Qt::LocalTime);
  QDateTime offset = local.toUTC();
  QTime properTimeOffset = QTime(offset.time().hour(), offset.time().minute());
  offset.setTimeSpec(Qt::LocalTime);
  utc.setTimeSpec(Qt::UTC);

  if (offset.secsTo(utc) < 0)
  {
    isNegative = true;
  }
  else
  {
    isNegative = false;
    properTimeOffset.setHMS(
        24 - properTimeOffset.hour() - (properTimeOffset.minute() / 60.0) -
            (properTimeOffset.second() / 3600.0),
        properTimeOffset.minute() - (properTimeOffset.second() / 60.0),
        properTimeOffset.second());
    if (!properTimeOffset.isValid())
    {
      // Midnight case
      properTimeOffset.setHMS(0, 0, 0);
    }
  }
  return properTimeOffset;
}

DT::JulianDateTime DTInterface::CurrentUTC()
{
  QDateTime currentUTC = QDateTime::currentDateTimeUtc();
  int year, month, day, hour, minute;
  double second;
  currentUTC.date().getDate(&year, &month, &day);
  hour = currentUTC.time().hour();
  minute = currentUTC.time().minute();
  second = currentUTC.time().second() + currentUTC.time().msec() / 1000.0;
  DT::JulianDateTime utc;
  utc.SetDateTime(DT::YMDHMS(year, month, day, hour, minute, second),
                  Common::Instance()->calendarType);
  return utc;
}
