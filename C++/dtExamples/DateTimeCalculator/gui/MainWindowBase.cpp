#include "MainWindowBase.h"

QTime MainWindowBase::GetTimeZoneDiff(QDateTime midnightDateTime,
                                      bool& isNegative)
{
  midnightDateTime.setTime(QTime(0, 0));
  QDateTime utcTime = midnightDateTime.toUTC();
  QDateTime localTime = midnightDateTime;
  // localTime.setTimeSpec(Qt::LocalTime);
  QDateTime offset = localTime.toUTC();
  QTime properTimeOffset = QTime(offset.time().hour(), offset.time().minute());
  // offset.setTimeSpec(Qt::LocalTime);
  // utcTime.setTimeSpec(Qt::UTC);

  if (offset.secsTo(utcTime) < 0)
  {
    isNegative = true;
  }
  else
  {
    isNegative = false;
    properTimeOffset.setHMS(
        24 - properTimeOffset.hour() - (properTimeOffset.minute() / 60.0) -
            (properTimeOffset.second() / 3600.0),
        properTimeOffset.minute() - (properTimeOffset.second() / 60.0),
        properTimeOffset.second());
    if (!properTimeOffset.isValid())
    {
      // Midnight case
      properTimeOffset.setHMS(0, 0, 0);
    }
  }
  return properTimeOffset;
}

DT::JulianDateTime MainWindowBase::CurrentUTC()
{
  QDateTime currentUTC = QDateTime::currentDateTimeUtc();
  int year, month, day, hour, minute;
  double second;
  currentUTC.date().getDate(&year, &month, &day);
  hour = currentUTC.time().hour();
  minute = currentUTC.time().minute();
  second = currentUTC.time().second() + currentUTC.time().msec() / 1000.0;
  DT::JulianDateTime utcTime;
  utcTime.SetDateTime(DT::YMDHMS(year, month, day, hour, minute, second),
                      DT::CALENDAR_TYPE::JULIAN_GREGORIAN);
  return utcTime;
}

void MainWindowBase::Initialise()
{
  this->fmt.precisionSeconds = 3;
  this->fmt.precisionFoD = 8;
  this->fmt.calendar = DT::CALENDAR_TYPE::JULIAN_GREGORIAN;

  // Set the time zones.
  this->TZ.first = "UT";
  this->TZ.second = 0;
  this->TZS = this->TZB = this->TZA = this->TZAlt = this->TZ;

  bool negativeOffset = true;
  QDateTime midnightDateTime = QDateTime::currentDateTime();
  midnightDateTime.setTime(QTime(0, 0));

  QTime localTZDiff = this->GetTimeZoneDiff(midnightDateTime, negativeOffset);

  double dt = ((localTZDiff.hour() * 60.0 + localTZDiff.minute()) * 60) +
      localTZDiff.second() + localTZDiff.msec() / 1000.0;
  dt = negativeOffset ? -dt : dt;

  std::string TZName;
  // UTC is used for for altTZ and TZB
  if (this->timeZones.GetTimeZoneName(int(dt), TZName))
  {
    this->TZ.first = TZName;
    this->TZ.second = dt;
    this->TZA = this->TZ;
    this->TZS = this->TZ;
  }

  // Get the current system time.
  QDateTime currentUTC = QDateTime::currentDateTimeUtc();
  int year, month, day, hour, minute;
  double second;
  currentUTC.date().getDate(&year, &month, &day);
  hour = currentUTC.time().hour();
  minute = currentUTC.time().minute();
  second = currentUTC.time().second() + currentUTC.time().msec() / 1000.0;
  this->utc.SetDateTime(DT::YMDHMS(year, month, day, hour, minute, second),
                        this->fmt.calendar);
  this->jd = this->utc.GetJDFullPrecision();
  this->utcAS = this->utcB = this->utcA = this->utc;
  this->jdAS = this->jdB = this->jdA = this->jd;
  this->tiDTBS = DT::TimeInterval(0, 0);
  this->tiJDBS = DT::TimeInterval(0, 0);
}
