#include "Common.h"
#include "DTInterface.h"

Common* Common::pInstance_ = 0;
bool Common::destroyed_ = false;

const double Common::seconds_in_day = 86400.0;

void Common::Initialise()
{
  this->calendarType = DT::CalendarType::JULIAN_GREGORIAN;
  this->timeZones = DT::TimeZones();

  // Set the time zones.
  this->TZ.first = "UT";
  this->TZ.second = 0;
  this->TZS = this->TZB = this->TZA = this->TZAlt = this->TZ;

  bool negativeOffset = true;
  QDateTime midnightDateTime = QDateTime::currentDateTime();
  midnightDateTime.setTime(QTime(0, 0));

  QTime localTZDiff = DTInterface::Instance()->GetTimeZoneDiff(midnightDateTime,
                                                               negativeOffset);

  double dt = ((localTZDiff.hour() * 60.0 + localTZDiff.minute()) * 60) +
      localTZDiff.second() + localTZDiff.msec();
  dt = negativeOffset ? -dt : dt;

  std::string TZName;
  // UTC is used for for altTZ and TZB
  if (timeZones.GetTimeZoneName(int(dt), TZName))
  {
    this->TZ.first = TZName;
    this->TZ.second = dt;
    this->TZA = this->TZ;
    this->TZS = this->TZ;
  }

  // Get the current system time.
  QDateTime currentUTC = QDateTime::currentDateTimeUtc();
  int year, month, day, hour, minute;
  double second;
  currentUTC.date().getDate(&year, &month, &day);
  hour = currentUTC.time().hour();
  minute = currentUTC.time().minute();
  second = currentUTC.time().second() + currentUTC.time().msec() / 1000.0;
  this->utc.SetDateTime(DT::YMDHMS(year, month, day, hour, minute, second),
                        this->calendarType);
  this->jd = this->utc.GetJDFullPrecision();
  utcAS = utcB = utcA = utc;
  jdAS = jdB = jdA = jd;
  tiDTBS = DT::TimeInterval(0, 0);
  tiJDBS = DT::TimeInterval(0, 0);
}
