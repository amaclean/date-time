#ifndef DTEXAMPLES_DATETIMECALCULATOR_GUI_MAINWINDOW_H
#define DTEXAMPLES_DATETIMECALCULATOR_GUI_MAINWINDOW_H

#pragma once

/*
We use a parent-child mechanism.
This class inherits from MainWindow.ui.
This class will be used to implement additional functionality
as required.

Qt's parent-child mechanism is implemented in QObject. When we create an
object (a widget, validator, or any other kind) with a parent, the parent adds
the object to the list of its children. When the parent is deleted, it walks
through its list of children and deletes each child. The children themselves
then delete all of their children, and so on recursively until none remain.
The parent-child mechanism greatly simplifies memory management,
reducing the risk of memory leaks. The only objects we must delete explicitly
are the objects we create with new and that have no parent. And if we delete
a child object before its parent, Qt will automatically remove that object from
the parent's list of children.
*/

#include "ui_MainWindow.h"

#include "DateTimeFormatter.h"
#include "DateTimeParser.h"
#include "MainWindowBase.h"

#include <QLabel>
#include <QString>

class MainWindow : public QMainWindow,
                   public Ui_MainWindow,
                   public MainWindowBase
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget* parent = 0);
  virtual ~MainWindow();

protected:
  void closeEvent(QCloseEvent* event);

private:
  // Display a permanent status message.
  QLabel permTxt;

private:
  std::unique_ptr<DT::DateTimeFormatter> formatter;
  std::unique_ptr<DT::DateTimeParser> parser;

  void SetUpConnections();

  void PopulateTZ();

  void ParserErrorMsg(int ErrNum, QString theError);

  void UpdatePermTxt();

  // ******************** Date Tab *****************************
  void GetTimeZonesC();
  void SetDT();
  void SetJD();
  void GetDT();
  void GetJD();
  void ShowDate();
  void UpdateDisplay();

  // ******************** Difference Tab ******************************
  void GetTimeZonesD();
  void SetDTAD();
  void SetDTBD();
  void SetJDAD();
  void SetJDBD();
  void GetDTAD();
  void GetDTBD();
  void GetJDAD();
  void GetJDBD();

  // ******************** Sum Tab *************************************
  void GetTimeZonesS();
  void SetDTAS();
  void SetTIDTBS();
  void SetJDAS();
  void SetTIJDBS();
  void GetDTAS();
  void GetTIDTBS();
  void GetJDAS();
  void GetTIJDBS();

private slots:
  void Instructions();
  void About();
  void ChangeCalendar();
  void ChangePrecision();

  // ******************** Date Tab *****************************
  void GetTZ(QString const& text);
  void GetTZAlt(QString const& text);
  void AltTZCBoxStateChanged(int);
  void dateTimeLineEditEditingFinished();
  void JDlineEditEditingFinished();
  void SetCurrentTime();

  // ******************** Difference Tab ******************************
  void GetTZAD(QString const& text);
  void GetTZBD(QString const& text);
  void dateTimeADlineEditEditingFinished();
  void JDADlineEditEditingFinished();
  void dateTimeBDlineEditEditingFinished();
  void JDBDlineEditEditingFinished();
  void DifferenceDTD();
  void DifferenceJDD();
  void SetCurrentTimeAD();
  void SetCurrentTimeBD();

  // ******************** Sum Tab *************************************
  void GetTZAS(QString const& text);
  void dateTimeASlineEditEditingFinished();
  void tiBSlineEditEditingFinished();
  void JDASlineEditEditingFinished();
  void JDBSlineEditEditingFinished();
  void SumDTS();
  void SumJDS();
  void SetCurrentTimeAS();
};

#endif // DTEXAMPLES_DATETIMECALCULATOR_GUI_MAINWINDOW_H
