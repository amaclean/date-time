#ifndef DTEXAMPLES_DATETIMECALCULATOR_GUI_PRECISIONDLG_H
#define DTEXAMPLES_DATETIMECALCULATOR_GUI_PRECISIONDLG_H

#pragma once

/*
We use a parent-child mechanism.
This class inherits both QDialog and Ui::Dialog where Ui::Dialog is the class
generated from PrecisionDlg.ui.
This class will be used to implement additional functionality
as required.

Qt's parent-child mechanism is implemented in QObject. When we create an
object (a widget, validator, or any other kind) with a parent, the parent adds
the object to the list of its children. When the parent is deleted, it walks
through its list of children and deletes each child. The children themselves
then delete all of their children, and so on recursively until none remain.
The parent-child mechanism greatly simplifies memory management,
reducing the risk of memory leaks. The only objects we must delete explicitly
are the objects we create with new and that have no parent. And if we delete
a child object before its parent, Qt will automatically remove that object from
the parent's list of children.
*/

#include "ui_PrecisionDlg.h"

class PrecisionDlg : public QDialog, public Ui::PrecisionDlg
{
  Q_OBJECT

public:
  explicit PrecisionDlg(QWidget* parent = 0);

private:
private slots:
};
#endif // DTEXAMPLES_DATETIMECALCULATOR_GUI_PRECISIONDLG_H
