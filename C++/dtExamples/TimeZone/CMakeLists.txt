set(KIT ${EG}TimeZone)


# Source files
set( Srcs
  TimeZone.cpp
)

# Include files
set( Incs
)

add_executable( ${KIT} ${Srcs} ${Incs} )
#-----------------------------------------------------------------------------
# Set compiler and linker flags for a single target with
#  target_compile_options and target_link_options.
if (MSVC)
  # Warning level 4, add /WX for all warnings as errors.
  target_compile_options(${KIT}  PRIVATE /W4 /std:c++latest)
else()
  # Lots of warnings, add -werror for all warnings as errors.
  target_compile_options(${KIT}  PRIVATE -Wall -Wextra -pedantic)
endif()

target_link_libraries( ${KIT}
  dt
)

install(TARGETS ${KIT}
  BUNDLE DESTINATION . COMPONENT Runtime
  RUNTIME DESTINATION bin COMPONENT Runtime
#  LIBRARY DESTINATION lib
#  ARCHIVE DESTINATION lib
)

