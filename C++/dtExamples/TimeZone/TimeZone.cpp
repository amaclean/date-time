/**
 Purpose: Demonstrate how to do time zone calculations.

 Demonstrates:
 1) How to convert between time zones.
 2) Date interval arithmetic.
 3) Format the date for output.
 4) Format the time zone for output.
 5) Decrementing (-=) and incrementing (+=) a date.

 */

#include "DateTimeFormatter.h"
#include "TimeZones.h"

#include <chrono>
#include <ctime>
#include <memory>

using namespace DT;

// This class contains useful formatting functions.
std::unique_ptr<DateTimeFormatter> formatter(new DateTimeFormatter());

/**
 * Demonstrate how to do time zone calculations..
 */
void TimeZoneCalculation()
{
  const double seconds_in_day = 86400.0;

  TimeZones timeZones;

  // Set up the timezone and the alternate one.
  HM hm;
  int TZValue;
  std::string TZName = "UT-09:30";
  if (timeZones.GetTimeZoneValue(TZName, TZValue))
  {
    hm.hour = (int)(TZValue / 3600.0),
    hm.minute = (TZValue - (int)(TZValue / 3600.0) * 3600.0) / 60.0;
  }
  else
  {
    std::cout << "Unable to get a time zone offset for: " << TZName
              << std::endl;
    return;
  }
  HM hmAlt;
  TZName = "UT+10";
  int TZAltValue;
  if (timeZones.GetTimeZoneValue(TZName, TZAltValue))
  {
    hmAlt.hour = (int)(TZAltValue / 3600.0),
    hmAlt.minute = (TZAltValue - (int)(TZAltValue / 3600.0) * 3600.0) / 60.0;
  }
  else
  {
    std::cout << "Unable to get a time zone offset for: " << TZName
              << std::endl;
    return;
  }

  // Define the formatting parameters.
  DateTimeFormat fmt;

  // Get the time for the time zone.
  // Note: fmt.calendar is JULIAN_GREGORIAN by default.
  JulianDateTime jdlt(2012, 1, 3, 9, 46, 12, fmt.calendar);

  // Instantiate a class to hold UTC.
  JulianDateTime jdut(jdlt);
  // Subtract the time zone difference in days to get UTC.
  jdut -= TZValue / seconds_in_day;

  // Now convert to the new time zone.
  // Instantiate a class to hold the alternate time.
  JulianDateTime jdaltlt(jdut);
  // Add the time zone difference in days to get local time.
  jdaltlt += TZAltValue / seconds_in_day;

  // Nicely format the output.
  // We generally display the time zone at the end of the time string.
  std::string localTime =
      formatter->FormatAsYMDHMS(jdlt, fmt) + " " + formatter->FormatTZAsHM(hm);
  // No need for the time zone (00:00).
  std::string utcTime = formatter->FormatAsYMDHMS(jdut, fmt);
  std::string alternateLocalTime = formatter->FormatAsYMDHMS(jdaltlt, fmt) +
      " " + formatter->FormatTZAsHM(hmAlt);

  std::cout << "Local Time:           " << localTime << std::endl;
  std::cout << "UT:                   " << utcTime << std::endl;
  std::cout << "Alternate Local Time: " << alternateLocalTime << std::endl;
  std::cout << "Note:" << std::endl;
  std::cout << "1) That the alternate local time is for the following day "
               "relative to UT."
            << std::endl;
  std::cout
      << "2) These three times represent the same time, so they are equivalent."
      << std::endl;
}

int main(void)
{
  auto timeStr = [](const time_t& t) {
    tm tm_t{};
#if defined(WIN32)
    errno_t ern;
    ern = localtime_s(&tm_t, &t);
#else
    tm tm_info{};
    tm_info = *localtime_r(&t, &tm_t);
#endif
    // tm_t = *localtime(&t);
    std::ostringstream os;
    os << std::put_time(&tm_t, "%F %T%zZ");
    return os.str();
  };

  std::chrono::time_point<std::chrono::system_clock> start, end;
  start = std::chrono::system_clock::now();
  std::cout << "--------------------------------------------" << std::endl;
  std::cout << "Demonstrating time zone calculations." << std::endl;
  std::cout << "--------------------------------------------" << std::endl;
  TimeZoneCalculation();
  std::cout << "--------------------------------------------" << std::endl;
  end = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end - start;
  std::time_t end_time = std::chrono::system_clock::to_time_t(end);
  std::cout << "Finished computation at " << timeStr(end_time)
            << "\nElapsed time: " << elapsed_seconds.count() << "s\n";

  return EXIT_SUCCESS;
}
