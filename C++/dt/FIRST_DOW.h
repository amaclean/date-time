/*=========================================================================

Program:   Date Time Library
File   :   FIRST_DOW.h

Copyright (c) Andrew J. P. Maclean
All rights reserved.
See Copyright.txt or the documentation for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#if !defined(FIRST_DOW_H_6AE304C9_4303_43B2_9CD5_3652E353C57D)
#define FIRST_DOW_H_6AE304C9_4303_43B2_9CD5_3652E353C57D

#pragma once

#include <cstdint>

namespace DT {
/**
Enumerate whether to use Sunday or Monday as the first day of the week.

*/
enum class FIRST_DOW : std::int8_t
{
  SUNDAY = 0,
  MONDAY
};

} // End namespace DT.
#endif // FIRST_DOW_H_6AE304C9_4303_43B2_9CD5_3652E353C57D
