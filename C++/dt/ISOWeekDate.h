/*=========================================================================

  Program:   Date Time Library
  File   :   ISOWeekDate.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#if !defined(ISOWEEKDATE_H_EFFD8674_5472_4AD4_911A_F1B8FCB6521C)
#define ISOWEEKDATE_H_EFFD8674_5472_4AD4_911A_F1B8FCB6521C

#pragma once

#include <iomanip>
#include <iostream>

namespace DT {

/**
  This class holds the ISO Date as year, week number and week day.

  @author Andrew J. P. Maclean
 */
class ISOWeekDate
{
public:
  ISOWeekDate() : year(0), weekNumber(0), weekDay(0)
  {
  }

  /**
   * @param year Year
   * @param weekNumber The week number.
   * @param weekDay The day of the week.
   *
   */
  ISOWeekDate(int const& year, int const& weekNumber, int const& weekDay)
  {
    this->year = year;
    this->weekNumber = weekNumber;
    this->weekDay = weekDay;
  }

  /**
   * Default copy constructor.
   *
   * @param wd The ISO week date.
   */
  ISOWeekDate(ISOWeekDate const& wd) = default;
  /**
   * Default assignment operator.
   *
   * @param wd The ISO week date.
   */
  ISOWeekDate& operator=(ISOWeekDate const& wd) = default;

  virtual ~ISOWeekDate() = default;

  /**
   * Set the ISO Week Date.
   * @param y Year
   * @param wn The week number.
   * @param wd The day of the week.
   *
   */
  void SetISOWeekDate(int const& y, int const& wn, int const& wd)
  {
    this->year = y;
    this->weekNumber = wn;
    this->weekDay = wd;
  }

  /**
   * @param rhs The ISO week date.
   *
   */
  bool operator==(ISOWeekDate const& rhs) const
  {
    return this->year == rhs.year && this->weekNumber == rhs.weekNumber &&
        this->weekDay == rhs.weekDay;
  }

  /**
   * @param rhs The ISO week date.
   *
   */
  bool operator!=(ISOWeekDate const& rhs) const
  {
    return !(*this == rhs);
  }

  /**
   * @param rhs The ISO Week Date.
   *
   */
  bool operator<(ISOWeekDate const& rhs) const
  {
    if (this->year < rhs.year)
    {
      return true;
    }
    if (this->year == rhs.year)
    {
      if (this->weekNumber < rhs.weekNumber)
      {
        return true;
      }
      if (this->weekNumber == rhs.weekNumber)
      {
        if (this->weekDay < rhs.weekDay)
        {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * @param rhs The ISO Week Date.
   *
   */
  bool operator<=(ISOWeekDate const& rhs) const
  {
    return !(*this > rhs);
  }

  /**
   * @param rhs The ISO Week Date.
   *
   */
  bool operator>(ISOWeekDate const& rhs) const
  {
    if (this->year > rhs.year)
    {
      return true;
    }
    if (this->year == rhs.year)
    {
      if (this->weekNumber > rhs.weekNumber)
      {
        return true;
      }
      if (this->weekNumber == rhs.weekNumber)
      {
        if (this->weekDay > rhs.weekDay)
        {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * @param rhs The ISO Week Date.
   *
   */
  bool operator>=(ISOWeekDate const& rhs) const
  {
    return !(*this < rhs);
  }

  /**
   * @param out The output stream
   * @param  rhs The time.
   * @return The ISO Week Date as an output stream.
   *
   */
  friend std::ostream& operator<<(std::ostream& out, ISOWeekDate const& rhs)
  {
    out << std::setw(4) << rhs.year << "-W" << std::setfill('0') << std::setw(2)
        << rhs.weekNumber << "-" << std::setfill('0') << std::setw(1)
        << rhs.weekDay;
    return out;
  }

public:
  /** Year */
  int year;
  /** The week number. */
  int weekNumber;
  /** The day of the week. */
  int weekDay;
};

} // End namespace DT.

#endif // ISOWEEKDATE_H_EFFD8674_5472_4AD4_911A_F1B8FCB6521C
