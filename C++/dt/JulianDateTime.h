/*=========================================================================

  Program:   Date Time Library
  File   :   JulianDateTime.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#if !defined(JULIANDATETIME_H_21123862_8711_4246_B3B6_E19821F5EB56)
#define JULIANDATETIME_H_21123862_8711_4246_B3B6_E19821F5EB56

#pragma once

#include "DateConversions.h"
#include "JD.h"
#include "TimeConversions.h"
#include "TimeInterval.h"
#include "YMDHMS.h"

#include <cmath>

namespace DT {
/**
  This class stores the date and time as a Julian Date split into an
    integral and fractional part for maximum precision.

  Operators are provided for comparisons, difference arithmetic and
    incrementing and decrementing dates.

    The classes YMD, YMDHMS, HM, HMS, TimeInterval and ISOWeekDate
    are convenience classes allowing parameters to be returned from
    various functions.

    @author Andrew J. P. Maclean

*/
class JulianDateTime : public JD, public DateConversions, public TimeConversions
{
public:
  // Default value corresponds to -4712-01-01 12:00:00 = JD0
  // JulianDateTime() = default;

  virtual ~JulianDateTime() = default;

  /**
   * @param year Year
   * @param month Month
   * @param day Day
   * @param hour Hour
   * @param minute Minute
   * @param second Second
   * @param calendar The calendar used by the date.
   *
   */
  JulianDateTime(int const& year, int const& month, int const& day,
                 int const& hour, int const& minute, double const& second,
                 CALENDAR_TYPE const& calendar)
  {
    YMDHMS ymdhms(year, month, day, hour, minute, second);
    this->SetDateTime(ymdhms, calendar);
  }

  /**
   * @param ymdhms A year, month, day, hour, minute, second structure.
   * @param calendar The calendar used by the date.
   *
   */
  JulianDateTime(YMDHMS const& ymdhms, CALENDAR_TYPE const& calendar)
  {
    this->SetDateTime(ymdhms, calendar);
  }

  /**
   * @param jdn The Julian Day Number, running from noon to noon.
   * @param fod The Fraction of the Day, running from noon to noon.
   *
   */
  JulianDateTime(double const& jdn = 0, double const& fod = 0)
  {
    this->SetJDN(static_cast<int>(std::floor(jdn)));
    this->SetFoD(jdn - static_cast<int>(std::floor(jdn)) + fod);
  }

  // Copy constructor.

  /**
   * Default copy constructor.
   *
   * @param dt A JulianDateTime object.
   */
  JulianDateTime(JulianDateTime const& dt) = default;

  /**
   *
   * This is a converting constructor.
   * A converting constructor specifies an implicit conversion from
   *  the types of its arguments (if any) to the type of its class.
   * We are allowing implicit conversion from 'JD' to 'JulianDateTime'
   *
   * @param dt A DT object.
   */
  JulianDateTime(JD const& dt)
  {
    this->SetJDN(dt.GetJDN());
    this->SetFoD(dt.GetFoD());
  }

  /**
   * Default assignment operator.
   *
   * @param dt A JulianDateTime object.
   */
  JulianDateTime& operator=(JulianDateTime const& dt) = default;

  // Assignment

  /**
   * Compare Julian Date Times for equality.
   *
   * @param jdt The Julian Date Time to be compared with this.
   * @param rel_tol Relative tolerance.
   * @param abs_tol Absolute tolerance.
   * @return True if the date times are equivalent.
   *
   */
  bool IsClose(JulianDateTime const& jdt,
               double const& rel_tol = DateTimeBase::rel_fod,
               double const& abs_tol = 0.0)
  {
    if (*this == jdt)
    {
      return true;
    }
    return this->JD::IsClose(jdt, rel_tol, abs_tol);
  }

public:
  /**
   * Get the Julian Date to its full precision.
   * The precision of the returned value will be of the order of
   *  1.0e-14 of a day or 1ns.
   *
   * @return The Julian Day Number of the date and the fraction of the
   *  Day running from noon to noon.
   */
  JD GetJDFullPrecision() const
  {
    return JD(*this);
  }

  /**
   * Get the time interval.
   * The precision of the returned value will be of the order of
   *  1.0e-14 of a day or 1ns.
   *
   * @return The time interval as deltaDay, the number of days and
   *         deltaFoD, the fraction of the day.
   */
  TimeInterval GetDeltaT()
  {
    return TimeInterval(this->GetJDN(), this->GetFoD());
  }

  /**
   * Set the time interval.
   *
   * @param dt The time interval as deltaDay, the number of days and
   *         deltaFoD, the fraction of the day.
   */
  void SetDeltaT(TimeInterval const& dt)
  {
    this->SetJDN(dt.GetDeltaDay());
    this->SetFoD(dt.GetDeltaFoD());
  }

  /**
   * Get the date and time.
   * The calendar to use is specified by the enumerated values
   *  in CALENDAR_TYPE.
   *
   * @param calendar The calendar used by the date.
   *
   * @return The date and time.
   */
  YMDHMS GetDateTime(CALENDAR_TYPE const& calendar) const;

  /**
   * Get the date.
   * The calendar to use is specified by the enumerated values
   *  in CALENDAR_TYPE.
   *
   * @param calendar The calendar used by the date.
   *
   * @return The year, month and day.
   *
   */
  YMD GetDate(CALENDAR_TYPE const& calendar) const
  {
    return GetDateTime(calendar).GetYMD();
  }

  /**
   * Get the time.
   *
   * @return The hours, minutes and seconds.
   *
   */
  HMS GetTimeHMS() const
  {
    return GetDateTime(CALENDAR_TYPE::JULIAN_GREGORIAN).GetHMS();
  }

  /**
   * Get the time.
   *
   * @return The hours and minutes.
   *
   */
  HM GetTimeHM() const
  {
    // Do not round the seconds.
    return this->h2hm(
        this->hms2h(GetDateTime(CALENDAR_TYPE::JULIAN_GREGORIAN).GetHMS()));
  }

  /**
   * Set the date and time.
   *
   * @param jn The Julian Date.
   * @param fd The fraction of the day, running from noon to noon.
   */
  void SetDateTime(double const& jn, double const& fd = 0)
  {
    this->SetJDN(static_cast<int>(jn));
    this->SetFoD(jn - static_cast<int>(jn) + fd);
  }

  /**
   * Set the date and time.
   *
   * @param ymdhms The year, month, day, hour, minute and second of the date.
   * @param calendar The calendar used by the date.
   */
  void SetDateTime(YMDHMS const& ymdhms, CALENDAR_TYPE const& calendar);

  /**
   * Set the date and time.
   *
   * @param year Year
   * @param month Month
   * @param day Day
   * @param hour Hour
   * @param minute Minute
   * @param second Second
   * @param calendar The calendar used by the date.
   *
   */
  void SetDateTime(int const& year, int const& month, int const& day,
                   int const& hour, int const& minute, double const& second,
                   CALENDAR_TYPE const& calendar)
  {
    YMDHMS ymdhms(year, month, day, hour, minute, second);
    this->SetDateTime(ymdhms, calendar);
  }

  /**
   * Set the date.
   *
   * The time is set to 00:00:00.0
   * The calendar to use is specified in ymd.
   *
   * @param ymd The year, month and day of the date.
   * @param calendar The calendar used by the date.
   */
  void SetDate(YMD const& ymd, CALENDAR_TYPE const& calendar)
  {
    SetDateTime(YMDHMS(ymd, HMS()), calendar);
  }

  /**
   * Set the time.
   *
   * The date is set to 2000-01-01 (Julian/Gregorian Calendar).
   * This is to ensure that negative Julian dates are not encountered.
   *
   * The time is normalised to the range 0 <= t < 24 so the
   *  date will change if times lie outside this range.
   *
   * @param hms The time as hours, minutes and seconds.
   *
   */
  void SetTime(HMS const& hms)
  {
    YMD ymd(2000, 01, 01);
    SetDateTime(YMDHMS(ymd, hms), CALENDAR_TYPE::JULIAN_GREGORIAN);
  }

  /**
   * Set the time.
   *
   * The date is set to 2000-01-01 (Julian/Gregorian Calendar).
   *
   * The time is normalised to the range 0 <= t < 24 so the
   *  date will change if times lie outside this range.
   *
   * @param hm The time as hours and minutes.
   *
   */
  void SetTime(HM const& hm)
  {
    SetTime(HMS(h2hms(hm2h(hm))));
  }

  /**
   * Get the day of the week in a calendar agnostic way.
   *
   * @param calendar The calendar used by the date.
   * @param firstDoW The first day of the week, either Sunday or Monday.
   * @return The day of the week (1..7).
   */
  int GetDoW(CALENDAR_TYPE const& calendar, FIRST_DOW const& firstDoW)
  {
    YMD ymd = this->GetDate(calendar);
    // The day of the week assumes the calendar type is Julian/Gregorian.
    int d = ymd.year * 10000 + ymd.month * 100 + ymd.day;
    YMD ymd1(ymd);
    if (d < static_cast<int>(DT::JG_CHANGEOVER::GREGORIAN_START_DATE))
    {
      // For dates less than 1582-10-15, use the Julian Calendar
      return this->dow(ymd1, CALENDAR_TYPE::JULIAN, firstDoW);
    }
    else
    {
      // For dates greater than or equal to 1582-10-15,
      // use the Gregorian Calendar
      return this->dow(ymd1, CALENDAR_TYPE::GREGORIAN, firstDoW);
    }
  }

  /**
   * Get the day of the year.
   *
   * @param calendar The calendar used by the date.
   * @return The day of the year.
   */
  int GetDoY(CALENDAR_TYPE const& calendar);

  /**
   * Get the week of the year.
   * See: http://en.wikipedia.org/wiki/ISO_week_date
   *  Note: This algorithm only works for Gregorian dates.
   *
   * @return The ISO week date.
   */
  ISOWeekDate GetISOWeek() const
  {
    YMD ymd = GetDate(CALENDAR_TYPE::GREGORIAN);
    return cd2woy(ymd);
  }

private:
};

} // End namespace DT.
#endif // JULIANDATETIME_H_21123862_8711_4246_B3B6_E19821F5EB56
