/*=========================================================================

Program:   Date Time Library
File   :   MONTH_NAME.h

Copyright (c) Andrew J. P. Maclean
All rights reserved.
See Copyright.txt or the documentation for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#if !defined(MONTH_NAME_H_BE134AE2_2B90_4E0F_98D1_B66B7742649B)
#define MONTH_NAME_H_BE134AE2_2B90_4E0F_98D1_B66B7742649B

#pragma once

#include <cstdint>

namespace DT {
/**
Enumerate whether to use a month name or not.

*/
enum class MONTH_NAME : std::int8_t
{
  NONE = 0,
  ABBREVIATED,
  UNABBREVIATED
};

} // End namespace DT.
#endif // MONTH_NAME_H_BE134AE2_2B90_4E0F_98D1_B66B7742649B
