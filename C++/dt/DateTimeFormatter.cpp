/*=========================================================================

Program:   Date Time Library
File   :   DateTimeFormatter.cpp

Copyright (c) Andrew J. P. Maclean
All rights reserved.
See Copyright.txt or the documentation for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "DateTimeFormatter.h"
#include "DayNames.h"
#include "MonthNames.h"

using namespace DT;

std::map<std::string, std::string>
DateTimeFormatter::GetFormattedDates(JulianDateTime& date, DateTimeFormat fmt)
{
  std::map<std::string, std::string> res;
  res["NamedDate"] = this->FormatDateWithNames(date, fmt);
  res["Date"] = this->FormatAsYMDHMS(date, fmt);
  auto dow = date.GetDoW(fmt.calendar, fmt.firstDoW);
  res["DoW"] = std::to_string(dow);
  DayNames dayNames;
  if (fmt.firstDoW == FIRST_DOW::SUNDAY)
  {
    // Sunday = 1
    res["DayName"] = dayNames.GetDayName(dow);
    res["DayNameAbbrev"] =
        dayNames.GetAbbreviatedDayName(dow, fmt.dayAbbreviationLength);
  }
  else
  {
    // Monday = 1
    // GetDayName and GetAbbreviatedDayName
    // treat Sunday as the first day of the week.
    res["DayName"] = dayNames.GetDayName((dow % 7) + 1);
    res["DayNameAbbrev"] = dayNames.GetAbbreviatedDayName(
        (dow % 7) + 1, fmt.dayAbbreviationLength);
  }
  res["DoY"] = this->FormatDOY(date, fmt);
  YMD ymd = date.GetDate(fmt.calendar);
  MonthNames mn;
  res["MonthName"] = mn.GetMonthName(ymd.month);
  res["MonthNameAbbrev"] =
      mn.GetAbbreviatedMonthName(ymd.month, fmt.monthAbbreviationLength);
  // ISO week dates are only valid for the Gregorian Calendar.
  if (ymd.year > static_cast<int>(JG_CHANGEOVER::JULIAN_GREGORIAN_YEAR) &&
      fmt.calendar != CALENDAR_TYPE::JULIAN)
  {
    res["IWD"] = this->FormatISOWeek(date);
  }
  else
  {
    res["IWD"] = "";
  }
  auto jdfp = fmt.precisionFoD;
  fmt.precisionFoD = this->maximumPrecision - 1;
  res["JD"] = this->FormatJDLong(date.GetJDFullPrecision(), fmt);
  fmt.precisionFoD = jdfp;
  res["JDShort"] = this->FormatJDShort(date.GetJDFullPrecision(), fmt);
  switch (fmt.calendar)
  {
  case CALENDAR_TYPE::JULIAN:
    res["Calendar"] = "Julian";
    break;
  case CALENDAR_TYPE::GREGORIAN:
    res["Calendar"] = "Gregorian";
    break;
  case CALENDAR_TYPE::JULIAN_GREGORIAN:
  default:
    res["Calendar"] = "Julian/Gregorian";
    break;
  }
  res["LeapYear"] =
      date.isLeapYear(date.GetDate(fmt.calendar).year, fmt.calendar) == true
      ? "Yes"
      : "No";

  return res;
}

std::string DateTimeFormatter::FormatAsYMDHMS(JulianDateTime& date,
                                              DateTimeFormat const& fmt)
{

  YMDHMS ymdhms = date.GetDateTime(fmt.calendar);

  auto prec = this->CheckPrecision(fmt.precisionSeconds);

  // Normalise for display
  auto hms = ymdhms.GetHMS();
  auto ymd = ymdhms.GetYMD();
  hms.second = this->Scale(hms.second, prec);
  if (hms.second >= 60.0)
  {
    hms.minute += 1;
    hms.second -= 60;
  }
  if (hms.minute > 59)
  {
    hms.hour += 1;
    hms.minute -= 60;
  }
  if (hms.hour > 23)
  {
    hms.hour -= 24;
    JulianDateTime jdt;
    jdt.SetDate(ymd, fmt.calendar);
    jdt += 1;
    auto d = jdt.GetDateTime(fmt.calendar).GetYMD();
    ymdhms.SetYMD(d.year, d.month, d.day);
  }
  ymdhms.SetHMS(hms.hour, hms.minute, hms.second);

  std::ostringstream os;

  if (fmt.monthName != MONTH_NAME::NONE)
  {
    MonthNames months;
    std::string mn;
    if (fmt.monthName == MONTH_NAME::UNABBREVIATED)
    {
      mn = months.GetMonthName(ymdhms.month);
    }
    else
    {
      mn = months.GetAbbreviatedMonthName(ymdhms.month,
                                          fmt.monthAbbreviationLength);
    }
    os << ymdhms.year << fmt.dateSep << mn << fmt.dateSep << std::setfill('0')
       << std::setw(2) << ymdhms.day << std::flush;
  }
  else
  {
    os << ymdhms.year << fmt.dateSep << std::setfill('0') << std::setw(2)
       << ymdhms.month << fmt.dateSep << std::setw(2) << ymdhms.day
       << std::flush;
  }

  os << fmt.dateTimeSep << std::flush;

  if (prec > 0)
  {
    os << std::setfill('0') << std::setw(2) << ymdhms.hour << fmt.timeSep
       << std::setw(2) << ymdhms.minute << fmt.timeSep
       << std::setiosflags(std::ios::fixed) << std::setw(3 + prec)
       << std::setprecision(prec) << ymdhms.second << std::flush;
  }
  else
  {
    os << std::setfill('0') << std::setw(2) << ymdhms.hour << fmt.timeSep
       << std::setw(2) << ymdhms.minute << fmt.timeSep << std::setw(2)
       << std::setprecision(prec) << static_cast<int>(ymdhms.second + 0.5)
       << std::flush;
  }

  return os.str();
}

std::string DateTimeFormatter::FormatAsYMD(JulianDateTime& date,
                                           DateTimeFormat const& fmt)
{
  auto res = FormatAsYMDHMS(date, fmt);
  return Split(res, fmt.dateTimeSep)[0];
}

std::string DateTimeFormatter::FormatAsHMS(JulianDateTime& date,
                                           DateTimeFormat const& fmt)
{
  auto res = FormatAsYMDHMS(date, fmt);
  return Split(res, fmt.dateTimeSep)[1];
}

std::string DateTimeFormatter::FormatDateWithNames(JulianDateTime& date,
                                                   DateTimeFormat const& fmt)
{

  if (fmt.dayName == DAY_NAME::NONE && fmt.monthName == MONTH_NAME::NONE)
  {
    return FormatAsYMDHMS(date, fmt);
  }

  auto prec = this->CheckPrecision(fmt.precisionSeconds);

  JulianDateTime jdt = date;
  jdt.Normalise();

  YMDHMS ymdhms(jdt.GetDateTime(fmt.calendar));
  YMD ymd(ymdhms.GetYMD());
  HMS hms(ymdhms.GetHMS());

  MonthNames mn;
  std::string month;
  if (fmt.monthName == MONTH_NAME::UNABBREVIATED)
  {
    month = mn.GetMonthName(ymd.month);
  }
  else
  {
    month = mn.GetAbbreviatedMonthName(ymd.month, fmt.monthAbbreviationLength);
  }

  DayNames dn;
  std::string day;
  if (fmt.dayName == DAY_NAME::UNABBREVIATED)
  {
    day = dn.GetDayName(date.GetDoW(fmt.calendar, FIRST_DOW::SUNDAY));
  }
  else
  {
    day = dn.GetAbbreviatedDayName(date.GetDoW(fmt.calendar, FIRST_DOW::SUNDAY),
                                   fmt.dayAbbreviationLength);
  }

  std::stringstream os;
  // dateSep and dateTimeSep are not used here, the convention is to use a
  // space.
  os << day << ", " << std::setfill('0') << std::setw(2) << ymd.day << " "
     << month << " " << std::setw(4) << ymd.year << " " << std::flush;
  if (prec > 0)
  {
    os << std::setfill('0') << std::setw(2) << hms.hour << fmt.timeSep
       << std::setw(2) << hms.minute << fmt.timeSep
       << std::setiosflags(std::ios::fixed) << std::setw(3 + prec)
       << std::setprecision(prec) << hms.second << std::flush;
  }
  else
  {
    os << std::setfill('0') << std::setw(2) << hms.hour << fmt.timeSep
       << std::setw(2) << hms.minute << fmt.timeSep << std::setw(2)
       << std::setprecision(prec) << static_cast<int>(hms.second) << std::flush;
  }

  return os.str();
}

std::string DateTimeFormatter::FormatJDLong(JD const& date,
                                            DateTimeFormat const& fmt)
{
  auto prec = this->CheckPrecision(fmt.precisionFoD);
  JD jd = date;
  jd.Normalise();

  // Normalise for display.
  double rounding = 0.5;
  if (prec == 0)
  {
    double tmp = jd.GetJDDouble();
    if (tmp >= 0)
    {
      tmp += (rounding + 0.01);
    }
    else
    {
      tmp -= (rounding + 0.01);
    }
    return std::to_string(static_cast<long long>(tmp));
  }

  int jdn = jd.GetJDN();
  double fod = jd.GetFoD();

  bool isNegative = false;
  if (jdn < 0)
  {
    isNegative = true;
    if (fod > 0.5)
    {
      ++jdn;
    }
    if (fod != 0)
    {
      fod = 1.0 - fod;
    }
  }

  if (fod != 0)
  {
    for (unsigned int i = 0; i < prec; ++i)
    {
      fod *= 10.0;
    }
    fod = std::floor(fod + rounding);
    for (unsigned int i = 0; i < prec; ++i)
    {
      fod /= 10.0;
    }
  }

  std::ostringstream os1;
  os1 << std::setiosflags(std::ios::fixed) << std::setfill('0')
      << std::setw(prec + 2) << std::setprecision(prec) << fod << std::flush;

  std::vector<std::string> res = Split(os1.str(), '.');

  std::ostringstream os;
  if (isNegative)
  {
    os << "-" << std::flush;
  }

  if (fod >= 0.5 && jdn < 0)
  {
    os << std::abs(jdn + 1) << "." << res[1] << std::flush;
  }
  else
  {
    os << std::abs(jdn) << "." << res[1] << std::flush;
  }

  return os.str();
}

std::string DateTimeFormatter::FormatTimeInterval(TimeInterval& timeInterval,
                                                  DateTimeFormat const& fmt)
{
  auto prec = this->CheckPrecision(fmt.precisionFoD);

  TimeInterval ti = timeInterval;
  ti.Normalise();

  // Normalise for display.
  double rounding = 0.5;
  if (prec == 0)
  {
    double tmp = ti.GetTimeIntervalDouble();
    if (tmp >= 0)
    {
      tmp += (rounding + 0.01);
    }
    else
    {
      tmp -= (rounding + 0.01);
    }
    return std::to_string(static_cast<long long>(tmp));
  }

  int deltaDay = ti.GetDeltaDay();
  double deltaFoD = ti.GetDeltaFoD();

  bool isNegative = false;
  if (deltaDay < 0)
  {
    isNegative = true;
  }
  else
  {
    if (deltaFoD < 0)
    {
      isNegative = true;
    }
  }
  deltaDay = std::abs(deltaDay);
  deltaFoD = std::abs(deltaFoD);

  if (deltaFoD != 0)
  {
    for (unsigned int i = 0; i < prec; ++i)
    {
      deltaFoD *= 10.0;
    }
    deltaFoD = std::floor(deltaFoD + rounding);
    for (unsigned int i = 0; i < prec; ++i)
    {
      deltaFoD /= 10.0;
    }
    while (deltaFoD >= 1)
    {
      deltaFoD -= 1;
      deltaDay += 1;
    }
  }

  std::ostringstream os1;
  os1 << std::setiosflags(std::ios::fixed) << std::setfill('0')
      << std::setw(prec + 2) << std::setprecision(prec) << deltaFoD
      << std::flush;

  std::vector<std::string> res;
  res = Split(os1.str(), '.');

  std::ostringstream os;
  if (isNegative)
  {
    os << "-" << std::flush;
  }
  os << deltaDay << "." << res[1] << std::flush;

  return os.str();
}

std::string
DateTimeFormatter::FormatTimeIntervalDHMS(TimeInterval& timeInterval,
                                          DateTimeFormat const& fmt)
{
  auto prec = this->CheckPrecision(fmt.precisionSeconds);

  TimeInterval ti = timeInterval;
  ti.Normalise();

  int deltaDay = ti.GetDeltaDay();
  double deltaFoD = ti.GetDeltaFoD();

  bool isNegative = false;
  if (deltaDay < 0)
  {
    isNegative = true;
  }
  else
  {
    if (deltaFoD < 0)
    {
      isNegative = true;
    }
  }
  deltaDay = std::abs(deltaDay);
  deltaFoD = std::abs(deltaFoD);

  TimeConversions tc;
  HMS hms = tc.h2hms(deltaFoD * 24.0);

  std::ostringstream os;
  if (isNegative)
  {
    os << "-" << std::flush;
    os << deltaDay << "d" << fmt.dayTimeSep << "-" << std::flush;
  }
  else
  {
    os << deltaDay << "d" << fmt.dayTimeSep << " " << std::flush;
  }

  if (prec > 0)
  {
    os << std::setfill('0') << std::setw(2) << hms.hour << fmt.timeSep
       << std::setw(2) << hms.minute << fmt.timeSep
       << std::setiosflags(std::ios::fixed) << std::setw(3 + prec)
       << std::setprecision(prec) << hms.second << std::flush;
  }
  else
  {
    os << std::setfill('0') << std::setw(2) << hms.hour << fmt.timeSep
       << std::setw(2) << hms.minute << fmt.timeSep << std::setw(2)
       << std::setprecision(prec) << static_cast<int>(hms.second) << std::flush;
  }

  return os.str();
}

const std::string DateTimeFormatter::FormatDOY(JulianDateTime& date,
                                               DateTimeFormat const& fmt)
{
  auto doy = date.GetDoY(fmt.calendar);
  auto fod = date.GetFoDMidnight();
  TimeInterval ti(doy, fod);

  return this->FormatTimeInterval(ti, fmt);
}

const std::string DateTimeFormatter::FormatISOWeek(JulianDateTime const& date)
{
  std::string dateSep = "-";

  ISOWeekDate iwd = date.GetISOWeek();

  std::ostringstream os;
  os << std::setfill('0') << std::setw(4) << iwd.year << dateSep << std::setw(2)
     << iwd.weekNumber << dateSep << std::setw(1) << iwd.weekDay << std::flush;

  return os.str();
}

const std::string DateTimeFormatter::FormatJDShort(JD const& date,
                                                   DateTimeFormat const& fmt)
{
  auto prec = this->CheckPrecision(fmt.precisionFoD);

  std::stringstream os;

  os << std::setiosflags(std::ios::fixed) << std::setw(2 + prec)
     << std::setprecision(prec) << date.GetJDDouble() << std::flush;

  return os.str();
}

const std::string DateTimeFormatter::FormatTZAsHM(HM const& hm)
{
  std::string sgn;
  if (hm.hour < 0 || hm.minute < 0)
  {
    sgn = "-";
  }
  else
  {
    sgn = "+";
  }

  std::ostringstream os;

  os << sgn << std::setfill('0') << std::setw(2) << std::abs(hm.hour)
     << std::setw(2) << std::abs((int)hm.minute) << std::flush;

  return os.str();
}
