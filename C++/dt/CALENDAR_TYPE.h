/*=========================================================================

Program:   Date Time Library
File   :   CALENDAR_TYPE.h

Copyright (c) Andrew J. P. Maclean
All rights reserved.
See Copyright.txt or the documentation for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#if !defined(CALENDAR_TYPE_H_3D29CE32_B46A_4D9E_8E1C_593E806A2905)
#define CALENDAR_TYPE_H_3D29CE32_B46A_4D9E_8E1C_593E806A2905

#pragma once

#include <cstdint>

namespace DT {
/**
Enumerate the calendar to use.

JULIAN: Proleptic Julian Calendar and Julian Calendar for dates on
or before 1582 Oct 4, Julian Calendar for dates on or after 1582 Oct 5

GREGORIAN: Proleptic Gregorian Calendar for dates on or before
1582 Oct 14, Gregorian Calendar for dates on or after 1582 Oct 15

JULIAN_GREGORIAN: The Proleptic Julian Calendar and the Julian Calendar
is used for dates on or before 1582 Oct 4. The Gregorian calendar is
used for dates on or after 1582 Oct 15.

*/
enum class CALENDAR_TYPE : std::int8_t
{
  JULIAN = 0,
  GREGORIAN,
  JULIAN_GREGORIAN
};

} // End namespace DT.
#endif // CALENDAR_TYPE_H_3D29CE32_B46A_4D9E_8E1C_593E806A2905
