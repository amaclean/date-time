/*=========================================================================

  Program:   Date Time Library
  File   :   MonthNames.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#if !defined(MONTHNAMES_H_62AD3492_729F_4F9C_98A4_804D940CAD47)
#define MONTHNAMES_H_62AD3492_729F_4F9C_98A4_804D940CAD47

#pragma once

#include <map>
#include <string>

namespace DT {

/*!
  This class provides a convenient way of accessing either month names
  or abbreviated month names based on the index on the month. Months are
  indexed from 1 to 12, with 1 being the first month of the year (January)
  and 12 being the last month of the year (December).

  The language used is English, however you can set the month names
   to other languages.

 @author Andrew J. P. Maclean
*/
class MonthNames
{
public:
  MonthNames() = default;

  virtual ~MonthNames() = default;

  /**
   * Default copy constructor.
   *
   * @param mn The MonthNames object.
   */
  MonthNames(MonthNames const& mn) = default;
  /**
   * Default assignment operator.
   *
   * @param mn The MonthNames object.
   */
  MonthNames& operator=(MonthNames const& mn) = default;

  /**
   * @param rhs The Month Names.
   *
   */
  bool operator==(MonthNames const& rhs) const
  {
    return this->names == rhs.names;
  }

  /**
   * @param rhs The Month Names.
   *
   */
  bool operator!=(MonthNames const& rhs) const
  {
    return !(*this == rhs);
  }

  /**
   *  Get the Month name.
   *
   *  An empty string is returned if the index of the Month
   *  lies outside the range 1...12.
   *
   * @param month The month number.
   * @return The month name.
   */
  std::string GetMonthName(int month)
  {
    auto p = names.find(month);
    if (p != names.end())
    {
      return p->second;
    }
    return "";
  }

  /**
   * Get the abbreviated Month name.
   *
   * An empty string is returned if the index of the Month
   * lies outside the range 1...12.
   *
   * @param month The month number.
   * @param size The number of letters used to form the abbreviation
   * @return The abbreviated month name.
   */
  std::string GetAbbreviatedMonthName(int month, size_t const& size)
  {
    auto p = names.find(month);
    if (p != names.end())
    {
      if (size <= p->second.size())
      {
        return p->second.substr(0, size);
      }
    }
    return "";
  }

  /**
   * Set the Month name. Useful when using a different language.
   *
   * There are 12 Months and the first Month starts at 1.
   * No updates are performed if the index of the Month
   * lies outside the range 1...12.
   *
   * @param month The month number.
   * @param monthName The month name.
   */
  void SetMonthName(int const month, std::string const& monthName)
  {
    if (month > 0 && month < 13)
    {
      names[month] = monthName;
    }
  }

private:
  /** A map for month names. */
  std::map<int, std::string> names{
      {1, "January"},   {2, "February"}, {3, "March"},     {4, "April"},
      {5, "May"},       {6, "June"},     {7, "July"},      {8, "August"},
      {9, "September"}, {10, "October"}, {11, "November"}, {12, "December"}};
};

} // End namespace DT.

#endif // MONTHNAMES_H_62AD3492_729F_4F9C_98A4_804D940CAD47
