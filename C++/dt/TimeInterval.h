/*=========================================================================

  Program:   Date Time Library
  File   :   TimeInterval.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#if !defined(TIMEINTERVAL_H_530C9482_8040_4649_871F_31450ECEA12B)
#define TIMEINTERVAL_H_530C9482_8040_4649_871F_31450ECEA12B

#pragma once

#include "DateTimeBase.h"

#include <cmath>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

namespace DT {

/**
 * This class holds a time interval as days and fractions of a day.
 *
 * @author Andrew J. P. Maclean
 */
class TimeInterval : public DateTimeBase
{
public:
  TimeInterval() : deltaDay(0), deltaFoD(0)
  {
  }

  /**
      @param deltaDay The days.
      @param deltaFoD The Fraction of the Day.

    */
  TimeInterval(int const& deltaDay, double const& deltaFoD)
  {
    this->deltaDay = deltaDay;
    this->deltaFoD = deltaFoD;
    Normalise();
  }

  /**
   * Default copy constructor.
   *
   * @param ti The TimeInterval object.
   */
  TimeInterval(TimeInterval const& ti) = default;
  /**
   * Default assignment operator.
   *
   * @param ti The TimeInterval object.
   */
  TimeInterval& operator=(TimeInterval const& ti) = default;

  virtual ~TimeInterval() = default;

  /**
   * Set the time interval.
   *
   * @param dDay The days.
   * @param dFoD The Fraction of the Day.
   *
   */
  void SetTimeInterval(int const& dDay, double const& dFoD)
  {
    this->deltaDay = dDay;
    this->deltaFoD = dFoD;
    Normalise();
  }

  /**
   * Compare time intervals for equality.
   *
   * @param ti The time interval to be compared with this.
   * @param rel_tol Relative tolerance.
   * @param abs_tol Absolute tolerance.
   * @return True if the  time intervals are equivalent.
   *
   */
  bool IsClose(TimeInterval const& ti,
               double const& rel_tol = DateTimeBase::rel_fod,
               double const& abs_tol = 0.0)
  {
    if (*this == ti)
    {
      return true;
    }
    return this->deltaDay == ti.GetDeltaDay() &&
        DateTimeBase::isclose(this->deltaFoD, ti.GetDeltaFoD(), rel_tol,
                              abs_tol);
  }

  /**
   * @param rhs The time interval.
   *
   */
  bool operator==(TimeInterval const& rhs) const
  {
    return this->deltaDay == rhs.deltaDay && this->deltaFoD == rhs.deltaFoD;
  }

  /**
   * @param rhs The time interval.
   *
   */
  bool operator!=(TimeInterval const& rhs) const
  {
    return !(*this == rhs);
  }

  /**
   * @param rhs A TimeInterval object.
   *
   */
  bool operator>(TimeInterval const& rhs) const
  {
    if (this->deltaDay > rhs.deltaDay)
    {
      return true;
    }
    return (this->deltaDay == rhs.deltaDay) && (this->deltaFoD > rhs.deltaFoD);
  }

  /**
   * @param rhs A TimeInterval object.
   *
   */
  bool operator<(TimeInterval const& rhs) const
  {
    if (this->deltaDay < rhs.deltaDay)
    {
      return true;
    }
    return (this->deltaDay == rhs.deltaDay) && (this->deltaFoD < rhs.deltaFoD);
  }

  /**
   * @param rhs A TimeInterval object.
   *
   */
  bool operator>=(TimeInterval const& rhs) const
  {
    return !(*this < rhs);
  }

  /**
   * @param rhs A TimeInterval object.
   *
   */
  bool operator<=(TimeInterval const& rhs) const
  {
    return !(*this > rhs);
  }

  // Addition subtraction.

  /**
   * @param rhs A TimeInterval object.
   *
   */
  TimeInterval operator+(TimeInterval const& rhs)
  {
    TimeInterval result(*this);
    result.deltaDay += rhs.deltaDay;
    result.deltaFoD += rhs.deltaFoD;
    result.Normalise();

    return result;
  }

  /**
   * @param days Days
   *
   */
  TimeInterval operator+(double const& days)
  {
    TimeInterval result(*this);
    result.deltaDay += static_cast<int>(days);
    result.deltaFoD += (days - static_cast<int>(days));
    result.Normalise();

    return result;
  }

  /**
   * @param rhs A TimeInterval object.
   *
   */
  void operator+=(TimeInterval const& rhs)
  {
    this->deltaDay += rhs.deltaDay;
    this->deltaFoD += rhs.deltaFoD;
    this->Normalise();
  }

  /**
   * @param days Days
   *
   */
  void operator+=(double const& days)
  {
    this->deltaDay += static_cast<int>(days);
    this->deltaFoD += (days - static_cast<int>(days));
    this->Normalise();
  }

  /**
   * @param rhs A TimeInterval object.
   *
   */
  TimeInterval operator-(TimeInterval const& rhs)
  {
    TimeInterval result(*this);
    result.deltaDay -= rhs.deltaDay;
    result.deltaFoD -= rhs.deltaFoD;
    result.Normalise();

    return result;
  }

  /**
   * @param days Days
   *
   */
  TimeInterval operator-(double const& days)
  {
    TimeInterval result(*this);
    result.deltaDay -= static_cast<int>(days);
    result.deltaFoD -= (days - static_cast<int>(days));
    result.Normalise();

    return result;
  }

  /**
   * @param rhs A TimeInterval object.
   *
   */
  void operator-=(TimeInterval const& rhs)
  {
    this->deltaDay -= rhs.deltaDay;
    this->deltaFoD -= rhs.deltaFoD;
    this->Normalise();
  }

  /**
   * @param days Days
   *
   */
  void operator-=(double const& days)
  {
    this->deltaDay -= static_cast<int>(days);
    this->deltaFoD -= (days - static_cast<int>(days));
    this->Normalise();
  }

  /**
   * Get the time difference days.
   *
   * @return The days.
   */
  int GetDeltaDay() const
  {
    return this->deltaDay;
  }

  /**
   * Get the time difference fraction of the day.
   *
   * @return The fraction of the Day
   */
  double GetDeltaFoD() const
  {
    return this->deltaFoD;
  }

  /**
   * Get the time interval as days and decimals of the day.
   *
   * The precision of the returned value will be of the order
   * of 1.0e-8 of a day or 1ms.
   *
   * @return The time interval as days and decimals of the day.
   */
  double GetTimeIntervalDouble() const
  {
    return this->deltaDay + this->deltaFoD;
  }

  /**
   * Set the time difference days.
   *
   * @param dDay The difference in days.
   */
  void SetDeltaDay(int dDay)
  {
    this->deltaDay = dDay;
    this->Normalise();
  }

  /**
   * Set the time difference fraction of the day.
   *
   * @param dFoD The difference in fractions of a day.
   */
  void SetDeltaFoD(double dFoD)
  {
    this->deltaFoD = dFoD;
    this->Normalise();
  }

  /**
   * @param out The output stream
   * @param  rhs The time.
   * @return The time interval.
   *
   */
  friend std::ostream& operator<<(std::ostream& out, TimeInterval const& rhs)
  {
    int dDay = rhs.GetDeltaDay();
    double dFoD = rhs.GetDeltaFoD();

    bool isNegative = false;
    if (dDay < 0)
    {
      isNegative = true;
    }
    else
    {
      if (dFoD < 0)
      {
        isNegative = true;
      }
    }
    if (isNegative)
    {
      out << "-";
    }
    else
    {
      out << " ";
    }
    std::ostringstream os;
    os << std::setw(19) << std::fixed << std::setprecision(15)
       << std::abs(dFoD);
    std::string s(os.str());
    size_t idx = s.find('.');
    s = s.substr(idx);
    out << int(std::abs(dDay)) << s;
    return out;
  }

  /**
   * Normalise so that the fraction of the day is > -1 and < 1.
   */
  void Normalise()
  {
    while (this->deltaFoD >= 1)
    {
      this->deltaFoD -= 1;
      this->deltaDay += 1;
    }
    while (this->deltaFoD <= -1)
    {
      this->deltaFoD += 1;
      this->deltaDay -= 1;
    }
    //  Normalise so that the sign is the same for deltaDay and deltaFoD.
    int tmp = static_cast<int>(this->deltaDay + this->deltaFoD);
    this->deltaFoD = static_cast<double>(this->deltaDay) -
        static_cast<double>(tmp) + this->deltaFoD;
    this->deltaDay = tmp;
  }

private:
  /** The days. */
  int deltaDay;
  /** The fraction of the day. */
  double deltaFoD;
};

} // End namespace DT.

#endif // TIMEINTERVAL_H_530C9482_8040_4649_871F_31450ECEA12B
