/*=========================================================================

  Program:   Date Time Library
  File   :   HM.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#if !defined(HM_H_F39FC935_3436_486F_A23E_901F5ECBB058)
#define HM_H_F39FC935_3436_486F_A23E_901F5ECBB058

#pragma once

#include "DateTimeBase.h"

#include <cmath>
#include <iomanip>
#include <iostream>

namespace DT {

/**
   This class holds a time as hours and minutes.

  The time components are public and no validation is done.
  Use the classes DateTimeParser and TimeConversions for validation.

   @author Andrew J. P. Maclean
 */
class HM : public DateTimeBase
{
public:
  HM() : DateTimeBase(), hour(0), minute(0)
  {
  }

  /**
   * @param hour Hour
   * @param minute Minute
   *
   */
  HM(int const& hour, double const& minute)
  {
    this->hour = hour;
    this->minute = minute;
  }

  /**
   * Default copy constructor.
   *
   * @param hm The time.
   */
  HM(HM const& hm) = default;
  /**
   * Default assignment operator.
   *
   * @param hm The time.
   */
  HM& operator=(HM const& hm) = default;

  virtual ~HM() = default;

  /**
   * Set the hours and minutes.
   * @param h Hour
   * @param m Minute
   *
   */
  void SetHM(int const& h, double const& m)
  {
    this->hour = h;
    this->minute = m;
  }

  /**
   * @param rhs The time.
   *
   */
  bool operator==(HM const& rhs) const
  {
    return this->hour == rhs.hour && this->minute == rhs.minute;
  }

  /**
   * @param rhs The time.
   *
   */
  bool operator!=(HM const& rhs) const
  {
    return !(*this == rhs);
  }

  /**
   * @param rhs The time.
   *
   */
  bool operator<(HM const& rhs) const
  {
    if (this->hour < rhs.hour)
    {
      return true;
    }
    if (this->hour == rhs.hour)
    {
      if (this->minute < rhs.minute)
      {
        return true;
      }
    }
    return false;
  }

  /**
   * @param rhs The time.
   *
   */
  bool operator<=(HM const& rhs) const
  {
    return !(*this > rhs);
  }

  /**
   * @param rhs The time.
   *
   */
  bool operator>(HM const& rhs) const
  {
    if (this->hour > rhs.hour)
    {
      return true;
    }
    if (this->hour == rhs.hour)
    {
      if (this->minute > rhs.minute)
      {
        return true;
      }
    }
    return false;
  }

  /**
   * @param rhs The time.
   *
   */
  bool operator>=(HM const& rhs) const
  {
    return !(*this < rhs);
  }

  /**
   * Compare hours and minutes for equality.
   *
   * @param hm The time to be compared with this.
   * @param rel_tol Relative tolerance.
   * @param abs_tol Absolute tolerance.
   * @return True if the times are equivalent.
   *
   */
  bool IsClose(HM const& hm, double const& rel_tol = DateTimeBase::rel_minutes,
               double const& abs_tol = 0.0) const
  {
    if (*this == hm)
    {
      return true;
    }
    return this->hour == hm.hour &&
        DateTimeBase::isclose(this->minute, hm.minute, rel_tol, abs_tol);
  }

  /**
   * @param out The output stream
   * @param  rhs The time.
   * @return The time as an output stream.
   *
   */
  friend std::ostream& operator<<(std::ostream& out, HM const& rhs)
  {
    bool isNegative = false;
    if (rhs.hour < 0)
    {
      isNegative = true;
    }
    else
    {
      if (rhs.minute < 0)
      {
        isNegative = true;
      }
    }
    if (isNegative)
    {
      out << "-";
    }
    else
    {
      out << " ";
    }
    out << std::setfill('0') << std::setw(2) << int(std::abs(rhs.hour)) << ":"
        << std::setfill('0') << std::setw(15) << std::fixed
        << std::setprecision(12) << std::abs(rhs.minute);
    return out;
  }

public:
  /** Hour */
  int hour;
  /** Minute */
  double minute;
};

} // End namespace DT.

#endif // HM_H_F39FC935_3436_486F_A23E_901F5ECBB058
