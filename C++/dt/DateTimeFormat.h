/*=========================================================================

Program:   Date Time Library
File   :   DateTimeFormat.h

Copyright (c) Andrew J. P. Maclean
All rights reserved.
See Copyright.txt or the documentation for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#if !defined(DATETIMEFORMAT_H_AFCE545A_17DE_4BE2_9C48_1FD758E76729)
#define DATETIMEFORMAT_H_AFCE545A_17DE_4BE2_9C48_1FD758E76729

#pragma once

#include "CALENDAR_TYPE.h"
#include "DAY_NAME.h"
#include "FIRST_DOW.h"
#include "MONTH_NAME.h"

namespace DT {
/**
 * This struct holds the parameters used for formatting
 * and parsing dates and times.
 *
 */
class DateTimeFormat
{
public:
  DateTimeFormat() = default;

  virtual ~DateTimeFormat() = default;

  /**
   * Default copy constructor.
   *
   * @param obj - the object.
   */
  DateTimeFormat(DateTimeFormat const& obj) = default;
  /**
   * Default assignment operator.
   *
   * @param obj - the object.
   */
  DateTimeFormat& operator=(DateTimeFormat const& obj) = default;

public:
  //! The calendar to use.
  CALENDAR_TYPE calendar = CALENDAR_TYPE::JULIAN_GREGORIAN;
  //! The precision of the seconds.
  unsigned int precisionSeconds = 3;
  //! The precision of the fraction of the day.
  unsigned int precisionFoD = 8;
  //! Specify whether the day name is required.
  DAY_NAME dayName = DAY_NAME::NONE;
  //! The size of the abbreviation for a day, usually 3.
  int dayAbbreviationLength = 3;
  //! The first day of the week, either Sunday or Monday.
  FIRST_DOW firstDoW = FIRST_DOW::SUNDAY;
  //! Specify whether the month name is required.
  MONTH_NAME monthName = MONTH_NAME::NONE;
  //! The size of the abbreviation for a month, usually 3.
  int monthAbbreviationLength = 3;
  //! The date separator, usually '-'.
  char dateSep = '-';
  //! The time separator, usually ':'.
  char timeSep = ':';
  //! The separator between the date and time, usually ' '.
  char dateTimeSep = ' ';
  //! The separator between the day and time in a time interval, usually ' '.
  char dayTimeSep = ' ';
};

} // End namespace DT.

#endif // DATETIMEFORMAT_H_AFCE545A_17DE_4BE2_9C48_1FD758E76729
