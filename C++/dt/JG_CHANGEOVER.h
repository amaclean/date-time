/*=========================================================================

Program:   Date Time Library
File   :   JGChangeover.h

Copyright (c) Andrew J. P. Maclean
All rights reserved.
See Copyright.txt or the documentation for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#if !defined(JG_CHANGEOVER_H_1552E184_4F73_4EED_85FC_072D3B8CF1E3)
#define JG_CHANGEOVER_H_1552E184_4F73_4EED_85FC_072D3B8CF1E3

#pragma once

#include <cstdint>

namespace DT {

/** The Julian/Gregorian calendar changeover dates.
<ul>
  <li>In the Gregorian Calendar.</li>
  <ul>
  <li>Proleptic Gregorian previous day: Jd=2299150, 1582 10 04.</li>
  <li>Gregorian first day:              Jd=2299161, 1582 10 15.</li>
  </ul>
  <li>In the Julian Calendar.</li>
  <ul>
  <li>Proleptic Gregorian previous day: Jd=2299160, 1582 10 04.</li>
  </ul>
  </ul>
*/
enum class JG_CHANGEOVER : std::int32_t
{
  GREGORIAN_PREVIOUS_DN = 2299150,
  GREGORIAN_PREVIOUS_DATE = 15821004,
  GREGORIAN_START_DN = 2299161,
  GREGORIAN_START_DATE = 15821015,
  JULIAN_GREGORIAN_YEAR = 1582,
  JULIAN_PREVIOUS_DN = 2299160
};
} // End namespace DT.
#endif // JG_CHANGEOVER_H_1552E184_4F73_4EED_85FC_072D3B8CF1E3
