/*=========================================================================

  Program:   Date Time Library
  File   :   DateConversions.cpp

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "DateConversions.h"
#include "YMD.h"

#include <cmath>

using namespace DT;

bool DateConversions::isLeapYear(int const& year, CALENDAR_TYPE const& calendar)
{
  bool res = false;
  switch (calendar)
  {
  case CALENDAR_TYPE::JULIAN:
    // If the years are greater than 1582 then this is the Julian Proleptic
    // calendar.
    res = (year % 4) == 0;
    break;
  case CALENDAR_TYPE::GREGORIAN:
    // If the years are less than 1582 then this is the Gregorian Proleptic
    // calendar.
    if ((year % 100) == 0)
    {
      res = (year % 400) == 0; // Gregorian century leap year.
    }
    else
    {
      res = (year % 4) == 0;
    }
    break;
  case CALENDAR_TYPE::JULIAN_GREGORIAN:
  default:
    if (year < static_cast<int>(
                   JG_CHANGEOVER::JULIAN_GREGORIAN_YEAR)) // Julian calendar.
    {
      res = (year % 4) == 0;
    }
    else // Gregorian calendar.
    {
      if ((year % 100) == 0)
      {
        res = (year % 400) == 0; // Gregorian century leap year.
      }
      else
      {
        res = (year % 4) == 0;
      }
    }
    break;
  }
  return res;
}

int DateConversions::cd2jdG(YMD const& ymd)
{
  int year = ymd.year;
  int month = ymd.month;
  int day = ymd.day;
  int a = (1461 * (year + 4800 + (month - 14) / 12)) / 4;
  int b = (367 * (month - 2 - 12 * ((month - 14) / 12))) / 12;
  int c = (3 * ((year + 4900 + (month - 14) / 12) / 100)) / 4;
  int d = day - 32075;
  return a + b - c + d;
}

YMD DateConversions::jd2cdG(int const& jd)
{
  YMD ymd;
  int l = jd + 68569;
  int n = (4 * l) / 146097;
  l = l - (146097 * n + 3) / 4;
  int i = (4000 * (l + 1)) / 1461001;
  l = l - (1461 * i) / 4 + 31;
  int j = (80 * l) / 2447;
  ymd.day = l - (2447 * j) / 80;
  l = j / 11;
  ymd.month = j + 2 - 12 * l;
  ymd.year = 100 * (n - 49) + i + l;
  return ymd;
}

int DateConversions::cd2jdJ(YMD const& ymd)
{
  int year = ymd.year;
  int month = ymd.month;
  int day = ymd.day;
  if (year > -4723)
  {
    int a = 367 * year;
    int b = (7 * (year + 5001 + (month - 9) / 7)) / 4;
    int c = (275 * month) / 9 + day + 1729777;
    int res = a - b + c;
    return res;
  }
  else
  {
    // Handle dates < 4712-01-01
    // We add 4712 * 3 = 14136 years to the year, do the calculations and
    // then subtract 4712 * 3 * 365.25 = 5163174 days to get the
    // correct julian day number.
    int a = 367 * (year + 14136);
    int b = (7 * ((year + 14136) + 5001 + (month - 9) / 7)) / 4;
    int c = (275 * month) / 9 + day + 1729777;
    int res = a - b + c - 5163174;
    return res;
  }
}

YMD DateConversions::jd2cdJ(int const& jd)
{
  YMD ymd;
  int j = jd + 1402;
  if (jd < 0)
  {
    // Set an origin for j so that j is positive.
    // This corresponds to -14136-01-01
    j += 5163174; // 4712 * 3 * 365.25 days
  }
  int k = (j - 1) / 1461; // 1461 = 365.25 * 4
  int l = j - 1461 * k;
  int n = (l - 1) / 365 - l / 1461;
  int i = l - 365 * n + 30;
  j = (80 * i) / 2447;
  ymd.day = i - (2447 * j) / 80;
  i = j / 11;
  ymd.month = j + 2 - 12 * i;
  ymd.year = 4 * k + n + i - 4716;
  if (jd < 0)
  {
    ymd.year -= 14136; // 4712 * 3 years
  }
  return ymd;
}

int DateConversions::doyJ(YMD const& ymd)
{
  YMD ymd0(ymd.year, 1, 1);
  return cd2jdJ(ymd) - cd2jdJ(ymd0) + 1;
}

int DateConversions::doyG(YMD const& ymd)
{
  YMD ymd0(ymd.year, 1, 1);
  return cd2jdG(ymd) - cd2jdG(ymd0) + 1;
}

int DateConversions::doyJG(YMD const& ymd) const
{
  YMD ymd0(ymd.year, 1, 1);
  return cd2jdJG(ymd) - cd2jdJG(ymd0) + 1;
}

ISOWeekDate DateConversions::cd2woy(YMD const& ymd) const
{
  // For this algorithm: 1 = Monday ... 7 = Sunday.
  CALENDAR_TYPE calendar = CALENDAR_TYPE::JULIAN_GREGORIAN;
  int doy = doyG(ymd);
  ISOWeekDate isowd;
  isowd.weekDay = this->dow(ymd, calendar, FIRST_DOW::MONDAY);
  int wd = isowd.weekDay;
  YMD ymd0(ymd.year, 1, 4);
  int doy0 = doyG(ymd0);
  YMD ymd1(ymd.year, 12, 28);
  int doy1 = doyG(ymd1);
  int y = ymd.year, m = ymd.month, d = ymd.day;
  if (doy >= doy0 && doy <= doy1)
  {
    isowd.weekNumber = (doy - wd + 10) / 7;
    isowd.year = y;
    return isowd;
  }
  // Handle cases relating to the first or last week of the year.
  YMD tmp(y - 1, 12, 31);
  wd = dow(tmp, calendar, FIRST_DOW::MONDAY);
  // If 31 December is on a Monday, Tuesday, or Wednesday,
  // it is in week 01 of the next year, otherwise in week 52 or 53.
  if (doy < doy0 && wd >= 4)
  {
    // Calculate the Monday of the first week of the year.
    d = d - isowd.weekDay + 1;
    if (d <= 0)
    {
      // December of the previous year.
      d = 31 + d;
      m = 12;
      y = y - 1;
    }
    tmp = YMD(y, m, d);
    doy = doyG(tmp);
    wd = dow(tmp, calendar, FIRST_DOW::MONDAY);
    isowd.weekNumber = (doy - wd + 10) / 7;
    isowd.year = y;
    return isowd;
  }
  // If 31 Dec is on a Monday Tuesday or Wednesday.
  tmp = YMD(y, 12, 31);
  wd = dow(tmp, calendar, FIRST_DOW::MONDAY);
  if (doy > doy1 && wd < 4)
  {
    if (isowd.weekDay > 3)
    {
      isowd.weekNumber = (doy - isowd.weekDay + 10) / 7;
      isowd.year = y;
      return isowd;
    }
    else
    {
      isowd.weekNumber = 1;
      isowd.year = y + 1;
      return isowd;
    }
  }
  isowd.weekNumber = (doy - wd + 10) / 7;
  isowd.year = y;
  return isowd;
}
