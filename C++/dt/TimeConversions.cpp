/*=========================================================================

  Program:   Date Time Library
  File   :   TimeConversions.cpp

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "TimeConversions.h"

#include <cmath>

using namespace DT;

double TimeConversions::hms2h(HMS const& hms)
{
  if (hms.hour < 0 || hms.minute < 0 || hms.second < 0)
  {
    return -((std::fabs(hms.second) / 60.0 +
              std::fabs(static_cast<double>(hms.minute))) /
                 60.0 +
             std::fabs(static_cast<double>(hms.hour)));
  }
  return (hms.second / 60.0 + hms.minute) / 60.0 + hms.hour;
}

HMS TimeConversions::h2hms(double const& hr)
{
  HMS hms;
  bool sgn = hr < 0;
  double t = fabs(hr);
  double n;
  t = modf(t, &n) * 60.0;
  hms.hour = static_cast<int>(n);
  hms.second = modf(t, &n) * 60.0;
  hms.minute = static_cast<int>(n);
  if (hms.second >= 60)
  {
    ++hms.minute;
    hms.second -= 60;
  }
  if (hms.minute >= 60)
  {
    ++hms.hour;
    hms.minute -= 60;
  }

  if (sgn)
  {
    if (hms.hour != 0)
    {
      hms.hour = -hms.hour;
      return hms;
    }
    if (hms.minute != 0)
    {
      hms.minute = -hms.minute;
      return hms;
    }
    if (hms.second != 0)
    {
      hms.second = -hms.second;
      return hms;
    }
  }
  return hms;
}

double TimeConversions::hm2h(HM const& hm)
{
  if (hm.hour < 0 || hm.minute < 0)
  {
    return -(std::fabs(hm.minute) / 60.0 +
             std::fabs(static_cast<double>(hm.hour)));
  }
  return hm.minute / 60.0 + hm.hour;
}

HM TimeConversions::h2hm(double const& hr)
{
  HM hm;
  bool sgn = hr < 0;
  double t = fabs(hr);
  double n;
  hm.minute = modf(t, &n) * 60.0;
  hm.hour = static_cast<int>(n);
  if (hm.minute >= 60)
  {
    ++hm.hour;
    hm.minute -= 60;
  }
  if (sgn)
  {
    if (hm.hour != 0)
    {
      hm.hour = -hm.hour;
      return hm;
    }
    if (hm.minute != 0)
    {
      hm.minute = -hm.minute;
      return hm;
    }
  }
  return hm;
}
