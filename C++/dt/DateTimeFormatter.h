/*=========================================================================

  Program:   Date Time Library
  File   :   DateTimeFormatter.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef DATETIMEFORMATTER_H_DDDCFA5D_3E40_46D5_8642_05B604ED5CDE
#define DATETIMEFORMATTER_H_DDDCFA5D_3E40_46D5_8642_05B604ED5CDE

#pragma once

#include "DateTimeBase.h"
#include "DateTimeFormat.h"
#include "JulianDateTime.h"

#include <map>

namespace DT {

/**
  A convenience class for formatting dates and times.

  @author Andrew J. P. Maclean
  */
class DateTimeFormatter : public DateTimeBase
{
public:
  DateTimeFormatter()
  {
  }

  /**
   * Default copy constructor.
   *
   * @param df The DateTimeFormatter object.
   */
  DateTimeFormatter(DateTimeFormatter const& df) = default;
  /**
   * Default assignment operator.
   *
   * @param df The DateTimeFormatter object.
   */
  DateTimeFormatter& operator=(DateTimeFormatter const& df) = default;

  virtual ~DateTimeFormatter() = default;

public:
  /**
   * Return a map of the various parts of a date for display.
   *
   * @param date The date and time to format.
   * @param fmt The formatting to use.
   *
   * @return A map of strings whose keys are:
   * <ul>
   * <li>NamedDate - the formatted date with month and day names.</li>
   * <li>Date - the formatted date.</li>
   * <li>DoW - the day of the week.</li>
   * <li>DayName - the day name.</li>
   * <li>DayNameAbbrev - the abbreviated day name.</li>
   * <li>MonthName - the month name.</li>
   * <li>MonthNameAbbrev - the abbreviated month name.</li>
   * <li>DoY - the day of the year.</li>
   * <li>IWD - the ISO week.</li>
   * <li>JD - The Julian Day.</li>
   * <li>JD - The short form of the Julian Day.</li>
   * <li>Calendar - The calendar used.</li>
   * <li>LeapYear - Either Yes or No.</li>
   * </ul>
   *
   * Note: ISO Week is only calculated if year &gt; 1582 in the
   * Gregorian calendar.
   */
  std::map<std::string, std::string> GetFormattedDates(JulianDateTime& date,
                                                       DateTimeFormat fmt);

  /**
   * Format the date and time.
   *
   * @param date The date and time to format.
   * @param fmt The formatting to use.
   *
   * @return YYYY-MM-DD HH:MM::SS.ddd
   */
  std::string FormatAsYMDHMS(JulianDateTime& date, DateTimeFormat const& fmt);

  /**
   * Format the date as YYYY-MM-DD.
   *
   * @param date The date and time to format.
   * @param fmt The formatting to use.
   * @return  The formatted date.
   */
  std::string FormatAsYMD(JulianDateTime& date, DateTimeFormat const& fmt);

  /**
   * Format the time as HH:MM::SS.ddd.
   *
   * @param date The date and time to format.
   * @param fmt The formatting to use.
   * @return  The formatted time.
   */
  std::string FormatAsHMS(JulianDateTime& date, DateTimeFormat const& fmt);

  /**
   * Format the date and time as: DayName, DD MonthName Year HH:MM::SS.ddd.
   *
   * If fmt.dayName == NONE and fmt.monthName == NONE then
   * YYYY-MM-DD HH:MM::SS.ddd is returned.
   *
   * @param date The date and time to format.
   * @param fmt The formatting to use.
   * @return The formatted date.
   */
  std::string FormatDateWithNames(JulianDateTime& date,
                                  DateTimeFormat const& fmt);

  /**
   * Format the long form of the Julian date.
   *
   * @param date The Julian date to format.
   * @param fmt The formatting to use.
   * @return The Julian date as a formatted string.
   */
  std::string FormatJDLong(JD const& date, DateTimeFormat const& fmt);

  /**
   * Format the short form of the Julian date.
   *
   * @param date The date and time to format.
   * @param fmt The formatting to use.
   * @return The Julian date as a formatted string.
   */
  const std::string FormatJDShort(JD const& date, DateTimeFormat const& fmt);

  /**
   * Format the time interval as days and fractions of a day.
   *
   * @param timeInterval The Time Interval to format.
   * @param fmt The formatting to use.
   * @return The Time Interval as a formatted string.
   */
  std::string FormatTimeInterval(TimeInterval& timeInterval,
                                 DateTimeFormat const& fmt);

  /**
   * Format the time interval as days and hours, minutes and seconds.
   *
   * @param timeInterval The Time Interval to format.
   * @param fmt The formatting to use.
   * @return The Time Interval as a formatted string.
   */
  std::string FormatTimeIntervalDHMS(TimeInterval& timeInterval,
                                     DateTimeFormat const& fmt);

  /**
   * Format the day of the year.
   *
   * @param date The date and time to format.
   * @param fmt The formatting to use.
   * @return  The day of the year as a formatted string.
   */
  const std::string FormatDOY(JulianDateTime& date, DateTimeFormat const& fmt);

  /**
   * Format an ISO Week Date.
   *
   * @param date The date and time to format.
   * @return The date formatted as YYYY-WW-D.
   */
  const std::string FormatISOWeek(JulianDateTime const& date);

  /**
   * Format the time zone as (+|-)HHMM.
   *
   * @param hm The timezone to format.
   * @return The timezone as a formatted string.
   */
  const std::string FormatTZAsHM(HM const& hm);

private:
};

} // End namespace DT.

#endif // DATETIMEFORMATTER_H_DDDCFA5D_3E40_46D5_8642_05B604ED5CDE
