/*=========================================================================

  Program:   Date Time Library
  File   :   DateConversions.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#if !defined(DATECONVERSIONS_H_2C485F64_B851_45D2_9265_C0E87E9DAAF1)
#define DATECONVERSIONS_H_2C485F64_B851_45D2_9265_C0E87E9DAAF1

#pragma once

#include "CALENDAR_TYPE.h"
#include "FIRST_DOW.h"
#include "ISOWeekDate.h"
#include "JG_CHANGEOVER.h"
#include "YMD.h"

namespace DT {

/**
  This class provides conversion routines between Calendrical dates and
   times and the date and time as a Julian Day Number (JDN) and fraction
   of the day (FoD).

  The Julian Day is defined as the interval of time in days and fractions of
   a day since 4713 B.C. January 1, Greenwich noon Julian proleptic calendar.

  The Julian Day Number is the integer portion of the Julian Day and JD0
   is be equivalent to 4713 B.C. January 1, Greenwich noon Julian proleptic
   calendar, a Monday or equivalently -4712-01-01 12:00:00.

  The algorithms defined here are known to work over the range:
  -13200-AUG-15 00:00  JD  -3100015.50 to  17191-MAR-15 00:00  JD   8000016.50

  For further details see:
   "Explanatory Supplement to the Astronomical Almanac",
    Ed. P. Kennith Seidelmann, University Science Books, 1992.

  Function names that start with a lower case letter indicate conversion
   methods.

  The conversion methods generally follow this pattern:

  [jd|cd]2[cd|jd[J|G|JG] where

  jd - julian date, cd - calendrical date, J - Julian Calendar,
   G - Gregorian Calendar, JG - Julian-Gregorian Calendar.

  Calendrical dates are stored as a class of type YMD or YMDHMS
   (if hours minutes and seconds are needed), Julian dates are stored as a
   class of type JD and ISO Week Dates are stored as a class of type
   ISOWeekDate.

@author Andrew J. P. Maclean
*/
class DateConversions
{
public:
  DateConversions() = default;

  /**
   * Default copy constructor.
   *
   * @param dc The DateConversions object.
   */
  DateConversions(DateConversions const& dc) = default;
  /**
   * Default assignment operator.
   *
   * @param dc The DateConversions object.
   */
  DateConversions& operator=(DateConversions const& dc) = default;

  virtual ~DateConversions() = default;

public:
  //! Test for a leap year.
  /*!
   * @param year Year
   * @param calendar The calendar to use.
   * @return true if it is a leap year.
   */
  static bool isLeapYear(int const& year, CALENDAR_TYPE const& calendar);

  /**
  * Calculate the Julian day number of the date.
  * The Proleptic Gregorian Calendar is used.
  *
  * Not valid for ymd < -4713 12 25.

  * @param ymd The year, month and day.
  *
  * @return The Julian day number of the date, an integer
  *  running from noon to noon.
  */
  static int cd2jdG(YMD const& ymd);

  /**
   * Calculate the date from the Julian day number.
   * The Proleptic Gregorian Calendar is used.
   *
   * Not valid for jd < -0.5.
   *
   * @param jd The julian day number, an integer running from noon to noon.
   *
   * @return The year, month and day.
   *
   */
  static YMD jd2cdG(int const& jd);

  /**
   * Calculate the Julian day number of the date.
   * The Proleptic Julian Calendar is used.
   *
   * @param ymd The year, month and day.
   *
   * @return The Julian day number of the date, an integer
   *  running from noon to noon.
   */
  static int cd2jdJ(YMD const& ymd);

  /**
   * Calculate the date from the Julian day number.
   * The Proleptic Julian Calendar is used.
   *
   * @param jd The julian day number, an integer running from noon to noon.
   *
   * @return The year, month and day.
   *
   */
  static YMD jd2cdJ(int const& jd);

  /**
   * Calculate the Julian day number of the date.
   * The Julian and Proleptic Julian Calendar is used for dates on or
   *  before 1582 Oct 4.
   * The Gregorian calendar is used for dates on or after 1582 Oct 15.
   *
   * @param ymd The year, month and day.
   *
   * @return The Julian day number of the date, an integer
   *  running from noon to noon.
   */
  int cd2jdJG(YMD const& ymd) const
  {
    int date = ymd.year * 10000 + ymd.month * 100 + ymd.day;
    if (date < static_cast<int>(JG_CHANGEOVER::GREGORIAN_START_DATE))
    {
      return this->cd2jdJ(ymd);
    }
    return this->cd2jdG(ymd);
  }

  /**
   * Calculate the date from the Julian day number.
   * The Julian and Proleptic Julian Calendar is used for dates on or
   *  before 1582 Oct 10 .
   * The Gregorian calendar is used for dates on or after 1582 Oct 15.
   *
   * @param jd The julian day number, an integer running from noon to noon.
   *
   * @return The year, month and day.
   *
   */
  YMD jd2cdJG(int const& jd) const
  {
    YMD ymd;
    if (jd < static_cast<int>(JG_CHANGEOVER::GREGORIAN_START_DN))
    {
      ymd = this->jd2cdJ(jd);
    }
    else
    {
      ymd = this->jd2cdG(jd);
    }
    return ymd;
  }

  //************ Day of the week or year.

  /**
   * Calculate the day of the week.
   * This function assumes that the Proleptic Julian Calendar and the
   *  Julian Calendar is used for dates on or before 1582 Oct 4 and the
   *  Gregorian calendar is used for dates on or after 1582 Oct 15.
   *
   * @param jd The Julian day number of the date, an integer running
   *  from noon to noon.
   * @param firstDoW The first day of the week, either Sunday or Monday.
   *
   * @return The day of the week.
   */
  static int dow(int const& jd, const FIRST_DOW& firstDoW)
  {
    if (firstDoW == FIRST_DOW::SUNDAY)
    {
      if (jd < 0)
      {
        return 7 + ((jd - 5) % 7);
      }
      else
      {
        return (jd + 1) % 7 + 1;
      }
    }
    else
    {
      if (jd < 0)
      {
        return 7 + ((jd - 6) % 7);
      }
      else
      {
        return jd % 7 + 1;
      }
    }
  }

  /**
   * Calculate the day of the week.
   *
   * You should use the Proleptic Julian Calendar and the Julian Calendar
   * for dates on or before 1582 Oct 4 and the Gregorian calendar
   * for dates on or after 1582 Oct 15.
   *
   * @param ymd The year, month and day.
   * @param calendar The calendar to use.
   * @param firstDoW The first day of the week, either Sunday or Monday.
   *
   * @return The day of the week.
   */
  int dow(YMD const& ymd, CALENDAR_TYPE const& calendar,
          FIRST_DOW firstDoW) const
  {
    int jd;
    switch (calendar)
    {
    case CALENDAR_TYPE::JULIAN:
      jd = this->cd2jdJ(ymd);
      break;
    case CALENDAR_TYPE::GREGORIAN:
      jd = this->cd2jdG(ymd);
      break;
    case CALENDAR_TYPE::JULIAN_GREGORIAN:
    default:
      jd = this->cd2jdJG(ymd);
      break;
    }
    return this->dow(jd, firstDoW);
  }

  /**
   * Calculate the day of the year.
   * The Proleptic Julian Calendar is used.
   *
   * @param ymd The year, month and day.
   *
   * @return The day of the year.
   */
  static int doyJ(YMD const& ymd);

  /**
   * Calculate the day of the year.
   * The Proleptic Gregorian Calendar is used.
   *
   * @param ymd The year, month and day.
   *
   * @return The day of the year.
   */
  static int doyG(YMD const& ymd);

  /**
   * Calculate the day of the year.
   * The Julian and Proleptic Julian Calendar is used for dates on or
   *  before 1582 Oct 4 .
   * The Gregorian calendar is used for dates on or after 1582 Oct 15.
   *
   * @param ymd The year, month and day.
   *
   * @return The day of the year.
   */
  int doyJG(YMD const& ymd) const;

  /**
   * Get the week of the year.
   * See: http://en.wikipedia.org/wiki/ISO_week_date
   *
   * @param ymd The year, month and day.
   *
   * @return The ISO Week Date.
   *
   *  Note: This algorithm only works for Gregorian dates.
   */
  ISOWeekDate cd2woy(YMD const& ymd) const;
};

} // End namespace DT.
#endif // DATECONVERSIONS_H_2C485F64_B851_45D2_9265_C0E87E9DAAF1
