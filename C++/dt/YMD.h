/*=========================================================================

  Program:   Date Time Library
  File   :   TimeZones.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#if !defined(YMD_H_A2498DE1_BB24_4F35_8A1C_6E9FB4CE6B71)
#define YMD_H_A2498DE1_BB24_4F35_8A1C_6E9FB4CE6B71

#pragma once

#include <iomanip>
#include <iostream>

namespace DT {

/**
   This class holds a date as year, month and day.

  The date components are public and no validation is done.
  Use the classes DateTimeParser and DateConversions for validation.

   @author Andrew J. P. Maclean
 */
class YMD
{
public:
  YMD() : year(-4712), month(1), day(1)
  {
  }

  /**
   * @param year Year
   * @param month Month
   * @param day Day
   *
   */
  YMD(int const& year, int const& month, int const& day)
  {
    this->year = year;
    this->month = month;
    this->day = day;
  }

  /**
   * Default copy constructor.
   *
   * @param  ymd The date.
   */
  YMD(YMD const& ymd) = default;
  /**
   * Default assignment operator.
   *
   * @param ymd The date.
   */
  YMD& operator=(YMD const& ymd) = default;

  virtual ~YMD() = default;

  /**
   * Set the year month and day.
   * @param y Year
   * @param m Month
   * @param d Day
   *
   */
  void SetYMD(int const& y, int const& m, int const& d)
  {
    this->year = y;
    this->month = m;
    this->day = d;
  }

  /**
   * @param rhs The date.
   *
   */
  bool operator==(YMD const& rhs) const
  {
    return this->year == rhs.year && this->month == rhs.month &&
        this->day == rhs.day;
  }

  /**
   * @param rhs The date.
   *
   */
  bool operator!=(YMD const& rhs) const
  {
    return !(*this == rhs);
  }

  /**
   * @param rhs The date.
   *
   */
  bool operator<(YMD const& rhs) const
  {
    if (this->year < rhs.year)
    {
      return true;
    }
    if (this->year == rhs.year)
    {
      if (this->month < rhs.month)
      {
        return true;
      }
      if (this->month == rhs.month)
      {
        if (this->day < rhs.day)
        {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * @param rhs The date.
   *
   */
  bool operator<=(YMD const& rhs) const
  {
    return !(*this > rhs);
  }

  /**
   * @param rhs The date.
   *
   */
  bool operator>(YMD const& rhs) const
  {
    if (this->year > rhs.year)
    {
      return true;
    }
    if (this->year == rhs.year)
    {
      if (this->month > rhs.month)
      {
        return true;
      }
      if (this->month == rhs.month)
      {
        if (this->day > rhs.day)
        {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * @param rhs The date.
   *
   */
  bool operator>=(YMD const& rhs) const
  {
    return !(*this < rhs);
  }

  /**
   * @param out The output stream
   * @param  rhs The time.
   * @return The date as an output stream.
   *
   */
  friend std::ostream& operator<<(std::ostream& out, YMD const& rhs)
  {
    out << rhs.year << "-" << std::setfill('0') << std::setw(2) << rhs.month
        << "-" << std::setfill('0') << std::setw(2) << rhs.day
        << std::setfill(' ');
    return out;
  }

public:
  /** Year */
  int year;
  /** Month */
  int month;
  /** Day */
  int day;
};

} // End namespace DT.

#endif // YMD_H_A2498DE1_BB24_4F35_8A1C_6E9FB4CE6B71
