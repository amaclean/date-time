/*=========================================================================

Program:   Date Time Library
File   :   DateTimeParser.cpp

Copyright (c) Andrew J. P. Maclean
All rights reserved.
See Copyright.txt or the documentation for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "DateTimeParser.h"
#include "MonthNames.h"
#include "TimeConversions.h"
#include "TimeInterval.h"

#include <cmath>
#include <regex>

namespace {
std::regex intNum("^(\\+|-)?\\d+$");
std::regex decNum("^((\\+|-)?\\d+)(\\.((\\d+)?))?$");
std::regex sciNum("^((\\+|-)?\\d+)(\\.((\\d+)?))?((e|E)((\\+|-)?)\\d+)?$");
std::regex trimWS("^\\s+|\\s+$"); // Trim leading and trailing whitespace.
std::regex trimLWS("^\\s+");      // Trim leading whitespace.
std::regex trimRWS("\\s+$");      // Trim trailing whitespace.
std::regex remAllWSExcept1(
    "\\s{2,}"); // Remove all whitespace except one between words.
std::regex datePattern("^(\\+|-)?\\d+\\D(\\d{1,2}|\\w{3,})\\D\\d{1,2}$");
std::regex timePattern("^(\\+|-)?(\\d{1,2}(\\D\\d{1,2})\\D\\d{1,2}(\\.((\\d+)?)"
                       ")?|\\d{1,2}(\\D\\d{1,2})?)$");

/**
 * Split a string into tokens using the separator.
 *
 * @param str The string to split.
 * @param sep The separator.
 * @return A vector of strings.
 */
std::vector<std::string> Split(std::string const& str, const char& sep)
{
  std::string nextToken;
  std::vector<std::string> result;

  for (auto p : str)
  {
    if (p == sep)
    {
      if (!nextToken.empty())
      {
        result.push_back(nextToken);
        nextToken.clear();
      }
    }
    else
    {
      nextToken += p;
    }
  }
  if (!nextToken.empty())
  {
    result.push_back(nextToken);
  }
  return result;
}

/** Split a string into tokens.
 * See: http://stackoverflow.com/questions/236129/how-to-split-a-string-in-c
 *
 *  Generally use std::vector<std::string> types as the second parameter
 * (ContainerT)...
 *  but list<> is way faster than vector<> for when direct access is not needed.
 *  Note that it passes the tokens as a reference, thus also allowing you to
 * build up
 *  tokens using multiple calls if you so wished.
 *  Lastly it allows you to specify whether to trim empty tokens from the
 *  results via a last optional parameter.
 *
 * @param str The string to tokenize.
 * @param tokens A container to hold the tokens.
 * @param delimiters The delimiters to tokenize on.
 * @param trimEmpty If true, empty tokens are trimmed from the container.
 */
template <class ContainerT>
void Tokenize(const std::string& str, ContainerT& tokens,
              const std::string& delimiters = " ", bool trimEmpty = false)
{
  std::string::size_type pos = 0;
  std::string::size_type lastPos = 0;
  while (pos < str.size())
  {
    pos = str.find_first_of(delimiters, lastPos);
    if (pos == std::string::npos)
    {
      pos = str.length();

      if (pos != lastPos || !trimEmpty)
        tokens.push_back(typename ContainerT::value_type(
            str.data() + lastPos,
            (typename ContainerT::value_type::size_type)pos - lastPos));

      break;
    }
    else
    {
      if (pos != lastPos || !trimEmpty)
        tokens.push_back(typename ContainerT::value_type(
            str.data() + lastPos,
            (typename ContainerT::value_type::size_type)pos - lastPos));
    }

    lastPos = pos + 1;
  }
}
} // End anonymous namespace.

using namespace DT;

std::pair<int, std::pair<int, double>>
DateTimeParser::ParseDecNum(std::string decStr)
{
  auto Trim = [](std::string& s) { return std::regex_replace(s, trimWS, ""); };

  auto ValidDecimal = [](std::string s) { return std::regex_match(s, decNum); };

  std::pair<int, std::pair<int, double>> res(2, std::make_pair(0, 0));
  std::string s = Trim(decStr);
  if (s.empty())
  {
    res.first = 2;
    return res;
  }
  if (!(ValidDecimal(s)))
  {
    res.first = 6;
    return res;
  }
  bool isNegative = false;
  std::vector<std::string> sv;
  Tokenize(s, sv, "-");
  if (sv.size() > 1)
  {
    isNegative = true;
    s = Trim(sv[1]);
  }

  int intPart = 0;
  double fracPart = 0;
  sv.clear();
  Tokenize(s, sv, ".");
  if (sv.size() == 1)
  {
    if (sv[0].size() != 0)
    {
      intPart = std::stoi(Trim(sv[0]));
    }
  }
  else
  {
    if (sv.size() > 1)
    {
      if (sv[0].size() != 0)
      {
        intPart = std::stoi(Trim(sv[0]));
      }
      if (sv[1].size() != 0)
      {
        fracPart = std::stod(("0." + Trim(sv[1])));
      }
    }
  }
  if (isNegative)
  {
    intPart = -intPart;
    fracPart = -fracPart;
  }
  res.first = 1;
  res.second.first = intPart;
  res.second.second = fracPart;
  return res;
}

std::pair<int, YMD> DateTimeParser::ParseDate(std::string const& date,
                                              DateTimeFormat fmt)
{
  auto Trim = [](std::string& s) { return std::regex_replace(s, trimWS, ""); };

  auto ValidDate = [](std::string s) {
    return std::regex_match(s, datePattern);
  };

  auto ValidInteger = [](std::string s) { return std::regex_match(s, intNum); };

  int year, month, day;

  std::pair<int, YMD> ret;
  std::string dateStr = date;
  dateStr = Trim(dateStr);
  if (dateStr.empty())
  {
    ret.first = 2;
    return ret;
  }
  if (dateStr.find(fmt.dateSep) == std::string::npos)
  {
    ret.first = 3;
    return ret;
  }
  if (!ValidDate(dateStr))
  {
    ret.first = 3;
    return ret;
  }

  if (dateStr[0] == '-')
  {
    dateStr[0] = '*';
  }
  std::vector<std::string> list = Split(dateStr, fmt.dateSep);
  // Check the size of the list because the date may be valid but
  // the separator is not valid.
  if (list.size() != 3)
  {
    ret.first = 3;
    return ret;
  }
  if (dateStr[0] == '*')
  {
    dateStr[0] = '-';
  }
  if (list[0][0] == '*')
  {
    list[0][0] = '-';
  }

  for (auto& p : list)
  {
    p = Trim(p);
  }

  year = std::stoi(list[0]);

  int m = 0;
  if (list[1].size() <= 2)
  {
    // We have an integer month.
    if (ValidInteger(list[1]))
    {
      m = std::stoi(list[1]);
    }
  }
  else
  {
    MonthNames months;
    for (int i = 0; i < 13; ++i)
    {
      if (months.GetMonthName(i) == list[1] ||
          months.GetAbbreviatedMonthName(i, 3) == list[1])
      {
        m = i;
        break;
      }
    }
  }
  if (m > 0 && m < 13)
  {
    month = m;
  }
  else
  {
    ret.first = 4;
    return ret;
  }
  day = std::stoi(list[2]);

  ret.second.SetYMD(year, month, day);
  ret.first = 1;

  return ret;
}

std::pair<int, HMS> DateTimeParser::ParseTime(std::string const& time,
                                              DateTimeFormat fmt)
{
  auto Trim = [](std::string& s) { return std::regex_replace(s, trimWS, ""); };

  auto ValidTime = [](std::string s) {
    return std::regex_match(s, timePattern);
  };

  auto ValidInteger = [](std::string s) { return std::regex_match(s, intNum); };

  int hour, minute;
  hour = minute = 0;
  double second = 0;

  std::pair<int, HMS> ret;
  std::string timeStr = time;
  timeStr = Trim(timeStr);
  if (timeStr.empty())
  {
    ret.first = 2;
    return ret;
  }

  if (!ValidTime(timeStr))
  {
    ret.first = 5;
    return ret;
  }

  bool isNeg = timeStr.find('-') != std::string::npos;
  std::vector<std::string> list = Split(timeStr, fmt.timeSep);

  for (auto& p : list)
  {
    p = Trim(p);
  }

  int i = 0;
  for (const auto& p : list)
  {
    if (i < 2)
    {
      if (!ValidInteger(p))
      {
        ret.first = 5;
        return ret;
      }
    }
    switch (i)
    {
    case 0:
      hour = std::abs(std::stoi(p));
      break;
    case 1:
      minute = std::abs(std::stoi(p));
      break;
    case 2:
      second = std::abs(std::stod(p));
      break;
    }
    ++i;
  }
  // Set the sign.
  if (isNeg)
  {
    if (hour != 0)
    {
      hour = -hour;
    }
    else
    {
      if (minute != 0)
      {
        minute = -minute;
      }
      else
      {
        if (second != 0)
        {
          second = -second;
        }
      }
    }
  }
  ret.second.SetHMS(hour, minute, second);
  ret.first = 1;

  return ret;
}

std::pair<int, YMDHMS> DateTimeParser::ParseDateTime(std::string const& date,
                                                     DateTimeFormat fmt)
{
  std::pair<int, YMDHMS> ret;
  std::vector<std::string> list = Split(date, fmt.dateTimeSep);
  if (list.empty())
  {
    ret.first = 2;
    return ret;
  }

  std::string dateStr, timeStr;

  if (list.size() == 1)
  {
    // We only have the date.
    dateStr = list[0];
  }
  else
  {
    dateStr = list[0];
    timeStr = list[1];
  }
  std::pair<int, YMD> ymdRes = this->ParseDate(dateStr, fmt);
  if (ymdRes.first != 1)
  {
    ret.first = ymdRes.first;
    return ret;
  }
  // An empty time is valid.
  std::pair<int, HMS> hmsRes = this->ParseTime(timeStr, fmt);
  if (hmsRes.first != 1 && hmsRes.first != 2)
  {
    ret.first = hmsRes.first;
    return ret;
  }

  ret.second.SetYMDHMS(ymdRes.second, hmsRes.second);
  ret.first = 1;

  return ret;
}

std::pair<int, TimeInterval>
DateTimeParser::ParseTimeInterval(std::string const& ti, DateTimeFormat fmt)
{

  auto Trim = [](std::string& s) { return std::regex_replace(s, trimWS, ""); };

  auto CleanTimeInterval = [](std::string const& s) {
    std::regex dD("D|d");
    std::string t = s;
    t = std::regex_replace(t, remAllWSExcept1, " ");
    t = std::regex_replace(t, dD, "");
    return std::regex_replace(t, trimWS, "");
  };

  std::pair<int, TimeInterval> ret;

  std::vector<std::string> list = Split(CleanTimeInterval(ti), fmt.dayTimeSep);
  if (list.empty())
  {
    ret.first = 2;
    return ret;
  }

  std::string dayStr, timeStr;
  if (list.size() == 1)
  {
    // We only have the days.
    dayStr = Trim(list[0]);
  }
  else
  {
    dayStr = Trim(list[0]);
    timeStr = Trim(list[1]);
  }

  std::pair<int, std::pair<int, double>> res = this->ParseDecNum(dayStr);
  if (res.first != 1)
  {
    ret.first = res.first;
    return ret;
  }
  TimeInterval timeInterval(res.second.first, res.second.second);
  std::pair<int, HMS> hmsRes = this->ParseTime(timeStr, fmt);
  // An empty time is valid.
  if (hmsRes.first != 1 && hmsRes.first != 2)
  {
    ret.first = hmsRes.first;
    return ret;
  }
  TimeConversions tc;
  double fod = tc.hms2h(hmsRes.second) / 24.0;

  ret.second.SetTimeInterval(timeInterval.GetDeltaDay(),
                             timeInterval.GetDeltaFoD() + fod);
  ret.first = 1;

  return ret;
}
