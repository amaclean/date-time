#-----------------------------------------------------------------------------
# The name of the library.
#
set(KIT dt)
set(LIB_NAME ${LIB_PREFIX}${KIT})

#-----------------------------------------------------------------------------
# The directory in which the source files are.
set(${KIT}_DIR ${CMAKE_CURRENT_SOURCE_DIR})

#-----------------------------------------------------------------------------
set(Srcs
  DateConversions.cpp
  DateTimeFormatter.cpp
  DateTimeParser.cpp
  JulianDateTime.cpp
  TimeConversions.cpp
)

set(Incs
  CALENDAR_TYPE.h
  DateConversions.h
  DateTimeBase.h
  DateTimeFormat.h
  DateTimeFormatter.h
  DateTimeParser.h
  DAY_NAME.h
  DayNames.h
  FIRST_DOW.h
  HM.h
  HMS.h
  ISOWeekDate.h
  JD.h
  JG_CHANGEOVER.h
  JulianDateTime.h
  MONTH_NAME.h
  MonthNames.h
  TimeConversions.h
  TimeInterval.h
  TimeZones.h
  YMD.h
  YMDHMS.h
)

add_library( ${LIB_NAME} ${Srcs} ${Incs} )
#-----------------------------------------------------------------------------
# Set compiler and linker flags for a single target with
#  target_compile_options and target_link_options.
if (MSVC)
  # Warning level 4, add /WX for all warnings as errors.
  target_compile_options(${LIB_NAME}  PRIVATE /W4 /std:c++latest)
else()
  # Lots of warnings, add -werror for all warnings as errors.
  target_compile_options(${LIB_NAME}  PRIVATE -Wall -Wextra -pedantic)
endif()
