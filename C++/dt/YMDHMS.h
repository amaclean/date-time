/*=========================================================================

  Program:   Date Time Library
  File   :   YMDHMS.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#if !defined(YMDHMS_H_BD12FD5B_8F5C_4B5F_AAC0_ADEA9F7B569B)
#define YMDHMS_H_BD12FD5B_8F5C_4B5F_AAC0_ADEA9F7B569B

#pragma once

#include "HM.h"
#include "HMS.h"
#include "YMD.h"

#include <iostream>

namespace DT {

/**
   This class holds the date and time as year, month, day,
    hours, minutes and seconds.

  The date and time components are public and no validation is done.
  Use the classes DateTimeParser, DateConversions and
  TimeConversions for validation.

   @author Andrew J. P. Maclean
 */
class YMDHMS : public YMD, public HMS
{
public:
  YMDHMS() = default;

  /**
   * @param ymd The date.
   * @param hms The time.
   *
   */
  YMDHMS(YMD const& ymd, HMS const& hms) : YMD(ymd), HMS(hms)
  {
  }

  /**
   * Default copy constructor.
   *
   * @param ymdhms The date and time.
   */
  YMDHMS(YMDHMS const& ymdhms) = default;
  /**
   * Default assignment operator.
   *
   * @param ymdhms The date and time.
   */
  YMDHMS& operator=(YMDHMS const& ymdhms) = default;

  virtual ~YMDHMS() = default;

  /**
   * @param year Year
   * @param month Month
   * @param day Day
   * @param hour Hour
   * @param minute Minute
   * @param second Second
   *
   */
  YMDHMS(int const& year, int const& month, int const& day, int const& hour,
         int const& minute, double const& second)
  {
    static_cast<YMD*>(this)->SetYMD(year, month, day);
    static_cast<HMS*>(this)->SetHMS(hour, minute, second);
  }

  /**
   * Set the date and time
   * @param ymd The date.
   * @param hms The time.
   *
   */
  void SetYMDHMS(YMD const& ymd, HMS const& hms)
  {
    static_cast<YMD*>(this)->SetYMD(ymd.year, ymd.month, ymd.day);
    static_cast<HMS*>(this)->SetHMS(hms.hour, hms.minute, hms.second);
  }

  /**
   * Set the date and time
   * @param y Year
   * @param mo Month
   * @param d Day
   * @param h Hour
   * @param mi Minute
   * @param s Second
   *
   */
  void SetYMDHMS(int const& y, int const& mo, int const& d, int const& h,
                 int const& mi, double const& s)
  {
    static_cast<YMD*>(this)->SetYMD(y, mo, d);
    static_cast<HMS*>(this)->SetHMS(h, mi, s);
  }

  /**
   * @param rhs The date and time.
   *
   */
  bool operator==(YMDHMS const& rhs) const
  {
    return static_cast<YMD>(*this) == static_cast<YMD>(rhs) &&
        static_cast<HMS>(*this) == static_cast<HMS>(rhs);
    ;
  }

  /**
   * @param rhs The date and time.
   *
   */
  bool operator!=(YMDHMS const& rhs) const
  {
    return !(*this == rhs);
  }

  /**
   * @param rhs The date and time.
   *
   */
  bool operator<(YMDHMS const& rhs) const
  {
    if (static_cast<YMD>(*this) < static_cast<YMD>(rhs))
    {
      return true;
    }
    return (static_cast<YMD>(*this) == static_cast<YMD>(rhs)) &&
        (static_cast<HMS>(*this) < static_cast<HMS>(rhs));
  }

  /**
   * @param rhs The date and time.
   *
   */
  bool operator<=(YMDHMS const& rhs) const
  {
    return !(*this > rhs);
  }

  /**
   * @param rhs The date and time.
   *
   */
  bool operator>(YMDHMS const& rhs) const
  {
    if (static_cast<YMD>(*this) > static_cast<YMD>(rhs))
    {
      return true;
    }
    return (static_cast<YMD>(*this) == static_cast<YMD>(rhs)) &&
        (static_cast<HMS>(*this) > static_cast<HMS>(rhs));
  }

  /**
   * @param rhs The date and time.
   *
   */
  bool operator>=(YMDHMS const& rhs) const
  {
    return !(*this < rhs);
  }

  /**
   * Compare Calendar Dates for equality.
   *
   * @param d The Calendar Date to be compared with this.
   * @param rel_tol Relative tolerance.
   * @param abs_tol AAbsolute tolerance.
   * @return True if the dates are equivalent.
   *
   */
  bool IsClose(YMDHMS const& d, double const& rel_tol = DateTimeBase::rel_fod,
               double const& abs_tol = 0.0) const
  {
    if (*this == d)
    {
      return true;
    }
    return this->GetYMD() == d.GetYMD() &&
        this->GetHMS().IsClose(d.GetHMS(), rel_tol, abs_tol);
  }

  /**
   * Get the date.
   * @return The date.
   *
   */
  YMD GetYMD() const
  {
    return YMD(this->year, this->month, this->day);
  }

  /**
   * Get the time as hours minutes and seconds.
   * @return The time as hours, minutes and seconds.
   *
   */
  HMS GetHMS() const
  {
    return HMS(this->hour, this->minute, this->second);
  }

  /**
   * Get the time as hours and minutes.
   * @return The time as hours and minutes.
   *
   */
  HM GetHM() const
  {
    return HM(this->hour, this->minute + this->second / 60.0);
  }

  /**
   * @param out The output stream
   * @param  rhs The time.
   * @return The date and time as an output stream.
   *
   */
  friend std::ostream& operator<<(std::ostream& out, YMDHMS const& rhs)
  {
    out << rhs.GetYMD() << " " << rhs.GetHMS();
    return out;
  }
};

} // End namespace DT.

#endif // YMDHMS_H_BD12FD5B_8F5C_4B5F_AAC0_ADEA9F7B569B
