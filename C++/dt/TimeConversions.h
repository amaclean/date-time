/*=========================================================================

  Program:   Date Time Library
  File   :   TimeConversions.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#if !defined(TIMECONVERSIONS_H_9CCD2601_CFA8_4281_A1AD_90225BF52506)
#define TIMECONVERSIONS_H_9CCD2601_CFA8_4281_A1AD_90225BF52506

#pragma once

#include "HM.h"
#include "HMS.h"

namespace DT {

/**
  This class provides methods to convert between hours and
   hours, minutes and seconds.

  Function names that start with a lower case letter indicate
   conversion methods.

  The conversion methods generally follow this pattern:

  [h|hm|hms]2[h|hm[hms] where

  h - hours, hm - hours and minutes, hms - hours minutes and seconds.

  Times are stored as a class of type HM (hours and minutes) or
   HMS (hours, minutes and seconds).

  @author Andrew J. P. Maclean
*/
class TimeConversions
{
public:
  TimeConversions() = default;

  virtual ~TimeConversions() = default;

  /**
   * Default copy constructor.
   *
   * @param tc The TimeConversions object.
   */
  TimeConversions(TimeConversions const& tc) = default;
  /**
   * Default assignment operator.
   *
   * @param tc The TimeConversions object.
   */
  TimeConversions& operator=(TimeConversions const& tc) = default;

public:
  /**
   * Convert hours minutes and seconds to hours.
   *
   * @param hms Hours, minutes and seconds.
   *
   * @return hours and decimals of an hour.
   */
  static double hms2h(HMS const& hms);

  /**
   * Convert hours to hours minutes and seconds.
   *
   * @param hr Hours and decimals of an hour.
   *
   * @return the hours, minutes and seconds.
   *
   */
  static HMS h2hms(double const& hr);

  /**
   * Convert hours and minutes to hours.
   *
   * @param hm Hours, minutes and seconds.
   *
   * @return hours and decimals of an hour.
   *
   */
  static double hm2h(HM const& hm);

  /**
   *  Convert hours to hours and minutes.
   *
   * @param hr Hours and decimals of an hour.
   *
   * @return hours and minutes.
   *
   */
  static HM h2hm(double const& hr);
};

} // End namespace DT.
#endif // TIMECONVERSIONS_H_9CCD2601_CFA8_4281_A1AD_90225BF52506
