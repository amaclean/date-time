/*=========================================================================

Program:   Date Time Library
File   :   DateTimeBase.h

Copyright (c) Andrew J. P. Maclean
All rights reserved.
See Copyright.txt or the documentation for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#if !defined(DATETIMEBASE_H_8A838BF6_267D_4A3A_AE86_9371F68B269D)
#define DATETIMEBASE_H_8A838BF6_267D_4A3A_AE86_9371F68B269D

#pragma once

#include <cassert>
#include <cmath>
#include <limits>
#include <stdexcept>
#include <type_traits>
#include <vector>

//! Date and time library.
/**
 * This namespace contains all the classes for the date time library.
 */
namespace DT {
/**
  A common base class for Date Time.

  @author Andrew J. P. Maclean
  */
class DateTimeBase
{
public:
  DateTimeBase() = default;

  virtual ~DateTimeBase() = default;

  /**
   * Default copy constructor.
   *
   * @param obj - the object.
   */
  DateTimeBase(DateTimeBase const& obj) = default;
  /**
   * Default assignment operator.
   *
   * @param obj - the object.
   */
  DateTimeBase& operator=(DateTimeBase const& obj) = default;

public:
  /**
   * Compare two floating point numbers for equality.
   *
   * The numbers are considered to be equal if the absolute difference
   * between them is less than a defined precision.
   *
   * See: http://en.cppreference.com/w/cpp/types/numeric_limits/epsilon
   * for a variation that considers differences using scaling.
   *
   * @param x The first number.
   * @param y The second number.
   * @param eps The precision.
   * @return True if x &asymp; y.
   */
  template <class T>
  static
      typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type
      ApproximatelyEqual(T x, T y, T eps = std::numeric_limits<T>::epsilon())
  {
    assert(0. < eps);
    assert(eps < 1.);

    // Handle infinities here.
    if (x == y)
    {
      return true;
    }

    return std::abs(x - y) < std::abs(eps);
  }

  //! Test two floats or doubles for equality.
  /**
   * Based on:
   * the function isclose in
   * [mathmodule.c](https://github.com/python/cpython/blob/master/Modules/mathmodule.c)
   *
   * Note: NaN is not close to anything, even itself. inf and -inf are only
   * close to themselves.
   *
   * @param a: The first number
   * @param b: The second number
   * @param rel_tol: relative tolerance, (= 1e-09) maximum difference for being
   *                 considered "close", relative to the magnitude of the input
   *                 values i.e abs(b - a) / min(abs(b), abs(a))
   * @param abs_tol: absolute tolerance, (= 0.0) maximum difference for being
   *                  considered "close", regardless of the magnitude of the
   *                  input values i.e. abs(b - a)
   * @return true if a, b are close, false otherwise
   */
  template <typename T,
            typename std::enable_if<std::is_floating_point<T>::value>::type* =
                nullptr>
  static bool isclose(T const& a, T const& b,
                      T const& rel_tol = static_cast<T>(1.0e-9),
                      T const& abs_tol = static_cast<T>(0.0))
  {
    if (abs_tol < 0 || rel_tol < 0)
    {
      auto s = "tolerances must be non-negative";
      throw std::runtime_error(std::string("Error: ") + s);
    }

    // Short-circuit exact equality -- needed to catch two infinities of the
    // same sign. And perhaps speeds things up a bit sometimes.
    if (a == b)
    {
      return true;
    }
    // Catches the case of two infinities of opposite sign, or one infinity and
    // one finite number. Two infinities of opposite sign would otherwise have
    // an infinite relative tolerance. Two infinities of the same sign are
    // caught by the equality check above.
    if (std::isinf(a) || std::isinf(b))
    {
      return false;
    }
    auto diff = std::abs(b - a);

    return ((diff <= std::abs(rel_tol * b)) ||
            (diff <= std::abs(rel_tol * a))) ||
        (diff <= abs_tol);
  }

  /** Scale a number by rounding it to a specified number of decimal places.
   * @param x The number to round.
   * @param precision The number of decimal places to round to.
   * @return The rounded number.
   */
  template <class T>
  typename std::enable_if<!std::numeric_limits<T>::is_integer, T>::type
  Scale(T x, unsigned int precision) const
  {
    auto prec = CheckPrecision(precision);
    T scale = 1;
    for (unsigned int i = 0; i < prec; ++i)
    {
      scale *= 10.0;
    }
    x = x * scale;
    x = std::round(x);
    return x /= scale;
  }

  /** Clamp the number of decimal places to the range 0 ... maximumPrecision.
   * @param precision The number of decimal places.
   *
   * @return A positive value clamped to the range 0 ... maximumPrecision.
   */
  unsigned int CheckPrecision(unsigned int const& precision) const
  {
    return (precision > this->maximumPrecision) ? this->maximumPrecision
                                                : precision;
  }

  /**
   * Split a string into tokens using the separator.
   *
   * @param str The string to split.
   * @param sep The separator.
   * @return A vector of strings.
   */
  std::vector<std::string> Split(std::string const& str, const char& sep)
  {
    std::string nextToken;
    std::vector<std::string> result;

    for (auto p : str)
    {
      if (p == sep)
      {
        if (!nextToken.empty())
        {
          result.push_back(nextToken);
          nextToken.clear();
        }
      }
      else
      {
        nextToken += p;
      }
    }
    if (!nextToken.empty())
    {
      result.push_back(nextToken);
    }
    return result;
  }


public:
  //! For 64-bit arithmetic, the relative tolerance of the fraction of the day.
  constexpr static double rel_fod = 1.0e-14;
  //! For 64-bit arithmetic, the relative tolerance of the minutes in a day.
  constexpr static double rel_minutes = 1.0e-11;
  //! For 64-bit arithmetic, the relative tolerance of the seconds in a day.
  constexpr static double rel_seconds = 1.0e-9;
  //! For 64-bit arithmetic, the absolute tolerance of the seconds in a day.
  constexpr static double abs_seconds = 1.0e-10;
  /**
   * The maximum allowable precision of the decimal part being displayed.
   *
   * If the precision specified in the format statements exceeds this,
   * then this value is used, it is set to 15 decimal places by default.
   *
   * The user can change this value.
   *
   * For displaying seconds a maximum precision of 9 is sufficient
   * for nanosecond precision.
   *
   * For displaying julian date times a precision of 15 will yield
   * nanosecond precision.
   */
  unsigned int maximumPrecision = 15;
};
} // End namespace DT.
#endif // DATETIMEBASE_H_8A838BF6_267D_4A3A_AE86_9371F68B269D
