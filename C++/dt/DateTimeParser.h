/*=========================================================================

  Program:   Date Time Library
  File   :   DateTimeParser.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef DATETIMEPARSER_H_32EE4A1B_DABD_4781_84C6_9D0742B415A8
#define DATETIMEPARSER_H_32EE4A1B_DABD_4781_84C6_9D0742B415A8

#pragma once

#include "DateTimeFormat.h"
#include "TimeInterval.h"
#include "YMDHMS.h"

#include <map>
#include <string>

namespace DT {

/**
 * A convenience class for parsing dates and times.
 *
 * All the parsers return a variable of type std::pair&lt; Integer,T &gt;
 * where the integer value corresponds to the states returned from
 * the parser.
 * These values are:
 *<ul>
 * <li>1 = Parsed Ok.</li>
 * <li>2 = No data.</li>
 * <li>3 = Invalid date.</li>
 * <li>4 = Bad month.</li>
 * <li>5 = Invalid time.</li>
 * <li>6 = Invalid integer or decimal number.</li>
 *</ul>
 */
class DateTimeParser
{
public:
  DateTimeParser() = default;

  virtual ~DateTimeParser() = default;

  // The copy constructor and copy assignment are deleted.
  DateTimeParser(const DateTimeParser&) = delete;
  DateTimeParser& operator=(const DateTimeParser&) = delete;

public:
  /** Parser messages.
   *
   * All the parsers return a variable of type std::pair<int,T>
   *  where the int value corresponds to the states in parserMessages.
   * Change these messages to whatever language you need
   *  by changing the values in your code.
   * Note: Do not change the keys.
   */
  std::map<int, std::string> parserMessages{
      {1, "Parsed Ok."},    {2, "No data."},
      {3, "Invalid date."}, {4, "Bad month."},
      {5, "Invalid time."}, {6, "Invalid integer or decimal number."}};

  /** Parse a string of the form dd.dddd into its integer and fractional parts.
   *
   * @param decStr The string to tokenize.
   * @return A pair consisting of an integer and the parsed number,
   *         with the integer taking the following values:
   *<ul>
   *         <li>1 = Parsed Ok.</li>
   *         <li>2 = No data.</li>
   *         <li>6 = Invalid integer or decimal number.</li>
   *</ul>
   */
  std::pair<int, std::pair<int, double>> ParseDecNum(std::string decStr);

  /**
   * Parse a date formatted as YYYY-MM-DD into a YMD
   * class.
   *
   * In addition to being an integer in the range [01 ... 12],
   * the month can be either the month name or abbreviation.
   *
   * @param date The date string to parse.
   * @param fmt The formatting to use.
   * @return A pair consisting of an integer and the parsed date,
   *         with the integer taking the following values:
   *<ul>
   *         <li>1 = Parsed Ok.</li>
   *         <li>2 = No data.</li>
   *         <li>3 = Invalid date.</li>
   *         <li>4 = Bad month.</li>
   *</ul>
   */
  std::pair<int, YMD> ParseDate(std::string const& date, DateTimeFormat fmt);

  /**
   * Parse a time formatted as HH:mm:ss.sss into an HMS
   * class.
   *
   * @param time The time string to parse.
   * @param fmt The formatting to use.
   * @return A pair consisting of an integer and the parsed date,
   *         with the integer taking the following values:
   *<ul>
   *         <li>1 = Parsed Ok.</li>
   *         <li>2 = No data.</li>
   *         <li>5 = Invalid time.</li>
   *</ul>
   */
  std::pair<int, HMS> ParseTime(std::string const& time, DateTimeFormat fmt);

  /**
   * Parse a date formatted as YYYY-MM-DD HH:mm:ss.sss into a YMDHMS
   * class.
   *
   * In addition to being an integer in the range [01 ... 12],
   * the month can be either the month name or abbreviation.
   *
   * The time string is optional. If not present then
   * the time is returned as 00:00:00.00
   *
   * @param date The date string to parse.
   * @param fmt The formatting to use.
   * @return A pair consisting of an integer and the parsed date,
   *         with the integer taking the following values:
   *<ul>
   *         <li>1 = Parsed Ok.</li>
   *         <li>2 = No data.</li>
   *         <li>3 = Invalid date.</li>
   *         <li>4 = Bad month.</li>
   *         <li>5 = Invalid time.</li>
   *</ul>
   */
  std::pair<int, YMDHMS> ParseDateTime(std::string const& date,
                                       DateTimeFormat fmt);

  /**
   * Parse a time interval formatted as d.dd HH:mm:ss.sss into a
   *  TimeInterval class.
   *
   * d.dd can be a decimal number with an optional trailing "d".
   *
   * The time string is optional. If not present then
   * the time is returned as 00:00:00.00
   *
   * Note: The day and HMS part of the string have independent signs.
   *   This means that for:
   *<ul>
   *   <li>-1d -12:00:00.0, -1d 12:00:00.0, 1d -12:00:00.0, 1d 12:00:00.0</li>
   *</ul>
   *   We get time intervals of:
   *<ul>
   *   <li>(-1, -0.5), (-1, 0.5), (1, -0.5) (1, 0.5) respectively.</li>
   *</ul>
   *
   * @param ti The time interval string to parse.
   * @param fmt The formatting to use.
   * @return A pair consisting of an integer and the parsed time interval,
   *         with the integer taking the following values:
   *<ul>
   *         <li>1 = Parsed Ok.</li>
   *         <li>2 = No data.</li>
   *         <li>5 = Invalid time.</li>
   *         <li>6 = Invalid integer or decimal number.</li>
   *</ul>
   */
  std::pair<int, TimeInterval> ParseTimeInterval(std::string const& ti,
                                                 DateTimeFormat fmt);

private:
};

} // End namespace DT.

#endif // DATETIMEPARSER_H_32EE4A1B_DABD_4781_84C6_9D0742B415A8
