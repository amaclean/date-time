/*=========================================================================

  Program:   Date Time Library
  File   :   TimeZones.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#if !defined(TIMEZONES_H_D48470C5_1D59_4A68_BE92_57158889740B)
#define TIMEZONES_H_D48470C5_1D59_4A68_BE92_57158889740B

#pragma once

#include <map>
#include <string>

namespace DT {

/*!
  This class provides a convenient way of accessing time zones.

  Two maps are provided, one of type TTimeZones, keyed by the time zone name
   and an inverse mapping of type TInvTimeZones, keyed by the difference
    in seconds from UTC 0.

  See:
  http://en.wikipedia.org/wiki/List_of_time_zones_by_UTC_offset#UTC.E2.88.9209:30.2C_V.E2.80.A0

  @author Andrew J. P. Maclean
*/
class TimeZones
{
public:
  TimeZones()
  {
    tzTimes = SwapPairs(tzNames);
  }

  virtual ~TimeZones() = default;

  /**
   * Default copy constructor.
   *
   * @param tz The TimeZones object.
   */
  TimeZones(TimeZones const& tz) = default;
  /**
   * Default assignment operator.
   *
   * @param tz The TimeZones object.
   */
  TimeZones& operator=(TimeZones const& tz) = default;

  /**
   * @param rhs The Timezones.
   *
   */
  bool operator==(TimeZones const& rhs)
  {
    return this->tzNames == rhs.tzNames && this->tzTimes == rhs.tzTimes;
  }

  /**
   * @param rhs The Timezones.
   *
   */
  bool operator!=(TimeZones const& rhs)
  {
    return !(*this == rhs);
  }

  /**
   * Get the timezones.
   *
   * @return The time zones indexed by name.
   */
  std::map<std::string, int>* GetTimeZonesByName()
  {
    return &tzNames;
  }

  /**
   * Get the timezones by time difference from UTC.
   *
   * @return The time zones indexed by time.
   */
  std::map<int, std::string>* GetTimeZonesByValue()
  {
    return &tzTimes;
  }

  /**
   * Get the name of the time zone corresponding to a given
   * offset in seconds.
   *
   * @param offset - the time zone offset in seconds.
   * @param TZName - the name of the time zone corresponding
   *                 to the offset.
   *
   * @return true if a time zone is found corresponding
   *         to the given offset, false otherwise.
   */
  bool GetTimeZoneName(int const& offset, std::string& TZName)
  {
    auto p = tzTimes.find(offset);
    if (p != tzTimes.end())
    {
      TZName = p->second;
      return true;
    }
    return false;
  }

  /**
   * Get the value of the time zone corresponding to a given
   * time zone name.
   *
   * @param TZName - the name of the time zone.
   * @param offset - the time zone offset in seconds.
   *
   * @return true if a offset is found corresponding
   *         to the given time zone name, false otherwise.
   */
  bool GetTimeZoneValue(std::string const& TZName, int& offset)
  {
    auto p = tzNames.find(TZName);
    if (p != tzNames.end())
    {
      offset = p->second;
      return true;
    }
    return false;
  }

private:
  /**
   * A map for TimeZones, keyed by the time zone name.
   *
   * <Time Zone name, difference in seconds from UT 0>
   */
  std::map<std::string, int> tzNames = {{"UT-12", -12 * 3600},
                                        {"UT-11", -11 * 3600},
                                        {"UT-10", -10 * 3600},
                                        {"UT-09:30", int(-9.5 * 3600)},
                                        {"UT-09", -9 * 3600},
                                        {"UT-08", -8 * 3600},
                                        {"UT-07", -7 * 3600},
                                        {"UT-06", -6 * 3600},
                                        {"UT-05", -5 * 3600},
                                        {"UT-04:30", int(-4.5 * 3600)},
                                        {"UT-04", -4 * 3600},
                                        {"UT-03:30", int(-3.5 * 3600)},
                                        {"UT-03", -3 * 3600},
                                        {"UT-02", -2 * 3600},
                                        {"UT-01", -1 * 3600},
                                        {"UT", 0 * 3600},
                                        {"UT+01", 1 * 3600},
                                        {"UT+02", 2 * 3600},
                                        {"UT+03", 3 * 3600},
                                        {"UT+03:30", int(3.5 * 3600)},
                                        {"UT+04", 4 * 3600},
                                        {"UT+04:30", int(4.5 * 3600)},
                                        {"UT+05", 5 * 3600},
                                        {"UT+05:30", int(5.5 * 3600)},
                                        {"UT+05:45", int(5.75 * 3600)},
                                        {"UT+06", 6 * 3600},
                                        {"UT+06:30", int(6.5 * 3600)},
                                        {"UT+07", 7 * 3600},
                                        {"UT+08", 8 * 3600},
                                        {"UT+08:45", int(8.75 * 3600)},
                                        {"UT+09", 9 * 3600},
                                        {"UT+09:30", int(9.5 * 3600)},
                                        {"UT+10", 10 * 3600},
                                        {"UT+10:30", int(10.5 * 3600)},
                                        {"UT+11", 11 * 3600},
                                        {"UT+11:30", int(11.5 * 3600)},
                                        {"UT+12", 12 * 3600},
                                        {"UT+12:45", int(12.75 * 3600)},
                                        {"UT+13", 13 * 3600},
                                        {"UT+14", 14 * 3600}};

  /**
   * A map for TimeZones keyed by the difference in seconds from UT 0.
   *
   * <difference in seconds from UT 0, Time Zone name.>
   */
  std::map<int, std::string> tzTimes;

  /**
   * Used to create the map keyed by seconds from the map keyed by
   * time zone name.
   *
   */
  template <class T1, class T2> std::map<T2, T1> SwapPairs(std::map<T1, T2> m1)
  {
    std::map<T2, T1> m2;

    for (auto&& item : m1)
    {
      m2.emplace(item.second, item.first);
    }
    return m2;
  }
};

} // End namespace DT.
#endif // TIMEZONES_H_D48470C5_1D59_4A68_BE92_57158889740B
