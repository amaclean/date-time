/*=========================================================================

Program:   Date Time Library
File   :   DAY_NAME.h

Copyright (c) Andrew J. P. Maclean
All rights reserved.
See Copyright.txt or the documentation for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#if !defined(DAY_NAME_H_B15F6862_7C63_4DAD_8DC3_E188EC15AF92)
#define DAY_NAME_H_B15F6862_7C63_4DAD_8DC3_E188EC15AF92

#pragma once

#include <cstdint>

namespace DT {
/**
Enumerate whether to use a day name or not.

*/
enum class DAY_NAME : std::int8_t
{
  NONE = 0,
  ABBREVIATED,
  UNABBREVIATED
};

} // End namespace DT.
#endif // DAY_NAME_H_B15F6862_7C63_4DAD_8DC3_E188EC15AF92
