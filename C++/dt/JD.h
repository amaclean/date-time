/*=========================================================================

  Program:   Date Time Library
  File   :   JD.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#if !defined(JD_H_DAB2EF0E_0BC0_4F97_A801_EF0B745C832F)
#define JD_H_DAB2EF0E_0BC0_4F97_A801_EF0B745C832F

#pragma once

#include "DateTimeBase.h"

#include <cmath>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

namespace DT {

/**
    This class holds a date as Julian Day Number and Fraction of the Day.

    The Julian Day Number stored in the class is the integral value of the day
    corresponding to the Julian Date (jdn) i.e int(jdn + 0.5), so it
    runs from noon to noon on the date in question.

    The fraction of the day stored runs from noon to noon.


    @author Andrew J. P. Maclean
  */
class JD : public DateTimeBase
{
public:
  // Default value corresponds to -4712-01-01 12:00:00 = JD0
  JD() : jdn(0), fod(0)
  {
  }

  /**
   * @param jdn The Julian Day Number, running from noon to noon.
   * @param fod The Fraction of the Day, running from noon to noon.
   *
   */
  JD(int const& jdn, double const& fod)
  {
    this->jdn = jdn;
    this->fod = fod;
    this->Normalise();
  }

  /**
   * Default copy constructor.
   *
   * @param jd The Julian Date.
   */
  JD(JD const& jd) = default;

  /**
   * Default assignment operator.
   *
   * @param jd The Julian Date.
   */
  JD& operator=(JD const& jd) = default;

  virtual ~JD() = default;

  // Assignment

  /**
   * Set the Julian Date.
   *
   * @param jn The Julian Day Number, running from noon to noon.
   * @param fd The Fraction of the Day, running from noon to noon.
   *
   */
  void SetJD(int const& jn, double const& fd)
  {
    this->jdn = jn;
    this->fod = fd;
    this->Normalise();
  }

  /**
   * Compare Julian Dates for equality.
   *
   * @param jd The Julian Date to be compared with this.
   * @param rel_tol Relative tolerance.
   * @param abs_tol Absolute tolerance.
   * @return True if the dates are equivalent.
   *
   */
  bool IsClose(JD const& jd, double const& rel_tol = DateTimeBase::rel_fod,
               double const& abs_tol = 0.0) const
  {
    if (*this == jd)
    {
      return true;
    }
    return this->jdn == jd.GetJDN() &&
        DateTimeBase::isclose(this->fod, jd.GetFoD(), rel_tol, abs_tol);
  }

  /**
   * @param rhs The Julian Date.
   *
   */
  bool operator==(JD const& rhs) const
  {
    return this->jdn == rhs.jdn && this->fod == rhs.fod;
  }

  /**
   * @param rhs The Julian Date.
   *
   */
  bool operator!=(JD const& rhs) const
  {
    return !(*this == rhs);
  }

  /**
   * @param rhs A JD object.
   *
   */
  bool operator>(JD const& rhs) const
  {
    if (this->jdn > rhs.jdn)
    {
      return true;
    }
    return (this->jdn == rhs.jdn) && (this->fod > rhs.fod);
  }

  /**
   * @param rhs A JD object.
   *
   */
  bool operator<(JD const& rhs) const
  {
    if (this->jdn < rhs.jdn)
    {
      return true;
    }
    return (this->jdn == rhs.jdn) && (this->fod < rhs.fod);
  }

  /**
   * @param rhs A JD object.
   *
   */
  bool operator>=(JD const& rhs) const
  {
    return !(*this < rhs);
  }

  /**
   * @param rhs A JD object.
   *
   */
  bool operator<=(JD const& rhs) const
  {
    return !(*this > rhs);
  }

  // Addition subtraction.

  /**
   * @param rhs A JD object.
   *
   */
  JD operator+(JD const& rhs)
  {
    JD result(*this);
    result.jdn += rhs.jdn;
    result.fod += rhs.fod;
    result.Normalise();

    return result;
  }

  /**
   * @param days Days
   *
   */
  JD operator+(double const& days)
  {
    JD result(*this);
    result.jdn += static_cast<int>(days);
    result.fod += (days - static_cast<int>(days));
    result.Normalise();

    return result;
  }

  /**
   * @param rhs A JD object.
   *
   */
  void operator+=(JD const& rhs)
  {
    this->jdn += rhs.jdn;
    this->fod += rhs.fod;
    this->Normalise();
  }

  /**
   * @param days Days
   *
   */
  void operator+=(double const& days)
  {
    this->jdn += static_cast<int>(days);
    this->fod += (days - static_cast<int>(days));
    this->Normalise();
  }

  /**
   * @param rhs A JD object.
   *
   */
  JD operator-(JD const& rhs)
  {
    JD result(*this);
    result.jdn -= rhs.jdn;
    result.fod -= rhs.fod;
    result.Normalise();

    return result;
  }

  /**
   * @param days Days
   *
   */
  JD operator-(double const& days)
  {
    JD result(*this);
    result.jdn -= static_cast<int>(days);
    result.fod -= (days - static_cast<int>(days));
    result.Normalise();

    return result;
  }

  /**
   * @param rhs A JD object.
   *
   */
  void operator-=(JD const& rhs)
  {
    this->jdn -= rhs.jdn;
    this->fod -= rhs.fod;
    this->Normalise();
  }

  /**
   * @param days Days
   *
   */
  void operator-=(double const& days)
  {
    this->jdn -= static_cast<int>(days);
    this->fod -= (days - static_cast<int>(days));
    this->Normalise();
  }

  /**
   * Get the Julian Day Number.
   *
   * @return Julian Day Number
   */
  int GetJDN() const
  {
    return this->jdn;
  }

  /**
   * Get the fraction of the day.
   *
   * @return Fraction of the Day
   */
  double GetFoD() const
  {
    return this->fod;
  }

  /**
   * Get the fraction of the day running from midnight to midnight.
   *
   * @return Fraction of the Day
   */
  double GetFoDMidnight()
  {
    int days;
    double hms;
    this->FoDToHMS(this->jdn, this->fod, days, hms);
    return hms;
  }

  /**
   * Get the Julian Date as days and decimals of the day.
   *
   * The precision of the returned value will be of the order
   * of 1.0e-8 of a day or 1ms.
   *
   * @return The Julian Day Number of the date as days and
   * decimals of the day.
   */
  double GetJDDouble() const
  {
    return this->jdn + this->fod;
  }

  /**
   * Set the Julian Day Number.
   *
   * @param jn The Julian Day Number.
   */
  void SetJDN(int jn)
  {
    this->jdn = jn;
    this->Normalise();
  }

  /**
   * Set the fraction of the day.
   *
   * @param fd The fraction of the day.
   */
  void SetFoD(double fd)
  {
    this->fod = fd;
    this->Normalise();
  }

  /**
   * Convert the fraction of the day running from
   * noon to noon to the fraction of the day
   * running from midnight to midnight.
   *
   * @param jn The julian day number.
   * @param fd The fraction of the day running from noon to noon (always
   * positive).
   * @param days The count of days.
   * @param hms The fraction of the day running from midnight to midnight
   * (always positive).
   *
   */
  static void FoDToHMS(int const& jn, double const& fd, int& days, double& hms)
  {
    hms = fd + 0.5;
    days = jn;
    if (hms >= 1.0) // Next day.
    {
      hms -= 1.0;
      ++days;
    }
  }

  /**
   * Convert the fraction of the day running from
   * midnight to midnight to the fraction of the day
   * running from noon to noon.
   *
   * @param days The count of days.
   * @param hms The fraction of the day running from midnight to midnight
   * (always positive).
   * @param jn The julian day number.
   * @param fd The fraction of the day running from noon to noon (always
   * positive).
   */
  static void HMSToFoD(int const& days, double const& hms, int& jn, double& fd)
  {
    fd = hms - 0.5;
    jn = days;
    if (fd < 0)
    {
      fd += 1.0; // Previous day.
      --jn;
    }
  }

  /**
   * @param out The output stream
   * @param  rhs The time.
   * @return The Julian Date as an output stream running from noon to noon.
   *
   */
  friend std::ostream& operator<<(std::ostream& out, JD const& rhs)
  {
    int jdn_l = rhs.GetJDN();
    double fod_l = rhs.GetFoD();

    bool isNegative = false;
    if (jdn_l < 0)
    {
      isNegative = true;
    }
    else
    {
      if (fod_l < 0)
      {
        isNegative = true;
      }
    }
    if (isNegative)
    {
      out << "-";
    }
    else
    {
      out << " ";
    }

    std::ostringstream os;
    os << std::setw(19) << std::fixed << std::setprecision(15)
       << std::abs(fod_l);
    std::string s(os.str());
    size_t idx = s.find('.');
    s = s.substr(idx);
    out << int(std::abs(jdn_l)) << s;
    return out;
  }

  /**
   * Normalise so that the 0 <= fod < 1.
   */
  void Normalise()
  {
    while (this->fod < 0)
    {
      fod += 1;
      --jdn;
    }
    while (this->fod >= 1.0)
    {
      fod -= 1;
      ++jdn;
    }
  }

private:
  /** The Julian Day Number, running from noon to noon. */
  int jdn;
  /** The Fraction of the Day, running from noon to noon. */
  double fod;
};

} // End namespace DT.

#endif // JD_H_DAB2EF0E_0BC0_4F97_A801_EF0B745C832F
