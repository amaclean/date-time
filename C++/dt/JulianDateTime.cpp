/*=========================================================================

  Program:   Date Time Library
  File   :   JulianDateTime.cpp

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "JulianDateTime.h"

using namespace DT;

YMDHMS JulianDateTime::GetDateTime(CALENDAR_TYPE const& calendar) const
{
  double fd = this->GetFoD();
  int jn = this->GetJDN();
  int days = 0;
  double tmp = 0.0;
  this->FoDToHMS(jn, fd, days, tmp);
  HMS hms = h2hms(tmp * 24.0);
  YMD ymd;
  switch (calendar)
  {
  case CALENDAR_TYPE::JULIAN:
    ymd = this->jd2cdJ(days);
    break;
  case CALENDAR_TYPE::GREGORIAN:
    ymd = this->jd2cdG(days);
    break;
  case CALENDAR_TYPE::JULIAN_GREGORIAN:
  default:
    ymd = this->jd2cdJG(days);
    break;
  }
  return YMDHMS(ymd, hms);
}

void JulianDateTime::SetDateTime(YMDHMS const& ymdhms,
                                 CALENDAR_TYPE const& calendar)
{
  double hms = this->hms2h(ymdhms.GetHMS()) / 24.0;
  int jn = 0;
  switch (calendar)
  {
  case CALENDAR_TYPE::JULIAN:
    jn = this->cd2jdJ(ymdhms.GetYMD());
    break;
  case CALENDAR_TYPE::GREGORIAN:
    jn = this->cd2jdG(ymdhms.GetYMD());
    break;
  case CALENDAR_TYPE::JULIAN_GREGORIAN:
  default:
    jn = this->cd2jdJG(ymdhms.GetYMD());
    break;
  }
  double fd = 0.0;
  int tmp = 0;
  this->HMSToFoD(jn, hms, tmp, fd);
  this->SetJD(tmp, fd);
}

int JulianDateTime::GetDoY(CALENDAR_TYPE const& calendar)
{
  YMD ymd = GetDate(calendar);
  switch (calendar)
  {
  case CALENDAR_TYPE::JULIAN:
    return this->doyJ(ymd);
  case CALENDAR_TYPE::GREGORIAN:
    return this->doyG(ymd);
  case CALENDAR_TYPE::JULIAN_GREGORIAN:
  default:
    return this->doyJG(ymd);
  }
}
