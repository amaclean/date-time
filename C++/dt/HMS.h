/*=========================================================================

  Program:   Date Time Library
  File   :   HMS.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#if !defined(HMS_H_2EFED856_49DD_4E73_865D_C638430D2473)
#define HMS_H_2EFED856_49DD_4E73_865D_C638430D2473

#pragma once

#include "DateTimeBase.h"

#include <cmath>
#include <iomanip>
#include <iostream>

namespace DT {

/**
  This class holds a time as hours, minutes and seconds.

  The time components are public and no validation is done.
  Use the classes DateTimeParser and TimeConversions for validation.

  @author Andrew J. P. Maclean
 */
class HMS : public DateTimeBase
{
public:
  HMS() : hour(0), minute(0), second(0)
  {
  }

  /**
   * @param hour Hour
   * @param minute Minute
   * @param second Second
   *
   */
  HMS(int const& hour, int const& minute, double const& second)
  {
    this->hour = hour;
    this->minute = minute;
    this->second = second;
  }

  /**
   * Default copy constructor.
   *
   * @param hms The time.
   */
  HMS(HMS const& hms) = default;
  /**
   * Default assignment operator.
   *
   * @param hms The time.
   */
  HMS& operator=(HMS const& hms) = default;

  virtual ~HMS() = default;

  /**
   * Set the hours, minutes and seconds.
   * @param h Hour
   * @param m Minute
   * @param s Second
   *
   */
  void SetHMS(int const& h, int const& m, double const& s)
  {
    this->hour = h;
    this->minute = m;
    this->second = s;
  }

  /**
   * @param rhs The time.
   *
   */
  bool operator==(HMS const& rhs) const
  {
    return this->hour == rhs.hour && this->minute == rhs.minute &&
        this->second == rhs.second;
  }

  /**
   * @param rhs The time.
   *
   */
  bool operator!=(HMS const& rhs) const
  {
    return !(*this == rhs);
  }

  /**
   * @param rhs The time.
   *
   */
  bool operator<(HMS const& rhs) const
  {
    if (this->hour < rhs.hour)
    {
      return true;
    }
    if (this->hour == rhs.hour)
    {
      if (this->minute < rhs.minute)
      {
        return true;
      }
      if (this->minute == rhs.minute)
      {
        if (this->second < rhs.second)
        {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * @param rhs The time.
   *
   */
  bool operator<=(HMS const& rhs) const
  {
    return !(*this > rhs);
  }

  /**
   * @param rhs The time.
   *
   */
  bool operator>(HMS const& rhs) const
  {
    if (this->hour > rhs.hour)
    {
      return true;
    }
    if (this->hour == rhs.hour)
    {
      if (this->minute > rhs.minute)
      {
        return true;
      }
      if (this->minute == rhs.minute)
      {
        if (this->second > rhs.second)
        {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * @param rhs The time.
   *
   */
  bool operator>=(HMS const& rhs) const
  {
    return !(*this < rhs);
  }

  /**
   * Compare hours minutes and seconds for equality.
   *
   * @param hms The time to be compared with this.
   * @param rel_tol Relative tolerance.
   * @param abs_tol Absolute tolerance.
   * @return True if the times are equivalent.
   *
   */
  bool IsClose(HMS const& hms,
               double const& rel_tol = DateTimeBase::rel_seconds,
               double const& abs_tol = 0.0) const
  {
    if (*this == hms)
    {
      return true;
    }
    return this->hour == hms.hour && this->minute == hms.minute &&
        DateTimeBase::isclose(this->second, hms.second, rel_tol, abs_tol);
  }

  /**
   * @param out The output stream
   * @param  rhs The time.
   * @return The time as an output stream.
   *
   */
  friend std::ostream& operator<<(std::ostream& out, HMS const& rhs)
  {
    bool isNegative = false;
    if (rhs.hour < 0)
    {
      isNegative = true;
    }
    else
    {
      if (rhs.minute < 0)
      {
        isNegative = true;
      }
      else
      {
        if (rhs.second < 0)
        {
          isNegative = true;
        }
      }
    }
    if (isNegative)
    {
      out << "-";
    }
    else
    {
      out << " ";
    }
    out << std::setfill('0') << std::setw(2) << int(std::abs(rhs.hour)) << ":"
        << std::setfill('0') << std::setw(2) << int(std::abs(rhs.minute)) << ":"
        << std::setfill('0') << std::setw(15) << std::fixed
        << std::setprecision(12) << std::abs(rhs.second) << std::setfill(' ');
    return out;
  }

public:
  /** Hour */
  int hour;
  /** Minute */
  int minute;
  /** Second */
  double second;
};

} // End namespace DT.

#endif // HMS_H_2EFED856_49DD_4E73_865D_C638430D2473
