/*=========================================================================

  Program:   Date Time Library
  File   :   DayNames.h

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#if !defined(DAYNAMES_H_5FDD1F0C_766C_43DF_AF60_C3FEE569864B)
#define DAYNAMES_H_5FDD1F0C_766C_43DF_AF60_C3FEE569864B

#pragma once

#include <map>
#include <string>

namespace DT {

/**
  This class provides a convenient way of accessing either day names
  or abbreviated day names based on the index on the day. Days are
  indexed from 1 to 7, with 1 being Sunday and day 7 being Saturday.

  The language used is English, however you can set the day names to
   other languages.

  @author Andrew J. P. Maclean
*/
class DayNames
{
public:
  DayNames() = default;

  virtual ~DayNames() = default;

  /**
   * Default copy constructor.
   *
   * @param dn The Day Names object.
   */
  DayNames(DayNames const& dn) = default;
  /**
   * Default assignment operator.
   *
   * @param dn The Day Names object.
   */
  DayNames& operator=(DayNames const& dn) = default;

  /**
   * @param rhs The Day Names.
   *
   */
  bool operator==(DayNames const& rhs) const
  {
    return this->names == rhs.names;
  }

  /**
   * @param rhs The Day Names.
   *
   */
  bool operator!=(DayNames const& rhs) const
  {
    return !(*this == rhs);
  }

  /**
   * Get the day name.
   *
   * An empty string is returned if the index of the day
   * lies outside the range 1...7.
   * @param day The day number
   * @return The name of the day.
   */
  std::string GetDayName(int day)
  {
    auto p = names.find(day);
    if (p != names.end())
    {
      return p->second;
    }
    return "";
  }

  /**
   * Get the abbreviated day name.
   *
   * An empty string is returned if the index of the day
   * lies outside the range 1...7.
   *
   * @param day The day number.
   * @param size The number of letters used to form the abbreviation.
   * @return The abbreviated day name.
   */
  std::string GetAbbreviatedDayName(int const& day, size_t const& size)
  {
    auto p = names.find(day);
    if (p != names.end())
    {
      if (size <= p->second.size())
      {
        return p->second.substr(0, size);
      }
    }
    return "";
  }

  /**
   * Set the day name. Useful when using a different language.
   *
   * There are 7 days and the first day starts at 1.
   * No updates are performed if the index of the day
   * lies outside the range 1...7.
   * @param day The day number
   * @param dayName The name of the day.
   */
  void SetDayName(int const& day, std::string const& dayName)
  {
    if (day > 0 && day < 8)
    {
      names[day] = dayName;
    }
  }

private:
  /** A map for day names. */
  std::map<int, std::string> names{
      {1, "Sunday"},   {2, "Monday"}, {3, "Tuesday"}, {4, "Wednesday"},
      {5, "Thursday"}, {6, "Friday"}, {7, "Saturday"}};
};

} // End namespace DT.

#endif // DAYNAMES_H_5FDD1F0C_766C_43DF_AF60_C3FEE569864B
