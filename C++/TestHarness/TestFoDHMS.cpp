#include "FoDHMSTests.h"

#include <cstdlib>
#include <iostream>
#include <string>

using namespace DT_Tests;

int TestFoDHMS(int argc, char* argv[])
{
  // Get rid of the argc, argv unreferenced formal parameter warnings.
  int numargs = argc;
  std::string programName;
  if (numargs != 0)
  {
    programName = argv[0];
    // Get rid of: assigned a value but never used warning.
    programName.clear();
  }

  FoDHMSTests fodhms;

  std::pair<bool, std::string> p(fodhms.TestAll());

  if (!p.first)
  {
    std::cerr << "Some tests failed." << std::endl;
    std::cerr << p.second << std::endl;
    return EXIT_FAILURE;
  }
  std::cerr << "Passed." << std::endl;

  return EXIT_SUCCESS;
}
