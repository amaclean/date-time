<!DOCTYPE html><html><head><meta charset="utf-8"><title>Date Time Library</title><style></style></head><body id="preview">
<h1><a id="Date_Time_Library_0"></a>Date Time Library</h1>
<h2><a id="Introduction_3"></a>Introduction</h2>
<p>This library provides a set of high precision date and time classes covering a large time span.</p>
<p>A consistent interface (within the limitations of the languages) is provided for C++, Java and Python. The precision of the dates and times across all languages and platforms is at least 1ns.</p>
<p>The calendars used are the Julian proleptic/Julian and Gregorian proleptic/Gregorian calendars. The changeover from the Julian Calendar to the Gregorian Calender occurred in 1582 with Thursday, 1582 October 04, being the last day of the Julian Calendar and the next day: Friday, 1582 October 15, being the first day of the Gregorian Calendar.</p>
<p>Primary references for algorithms were:</p>
<ul>
<li><a href="http://aa.usno.navy.mil/publications/docs/exp_supp.php">Explanatory Supplement to the Astronomical Almanac</a></li>
<li><a href="http://www.willbell.com/math/mc1.HTM">Astronomical Algorithms</a></li>
</ul>
<p>Testing was done using <a href="http://aa.usno.navy.mil/data/docs/JulianDate.php">Julian Date Converter</a>.</p>
<h2><a id="Requirements_19"></a>Requirements</h2>
<p>Data sizes:</p>
<ul>
<li>integer must be at least 32-bit</li>
<li>floating point must be 8-byte double precision (IEEE 754).</li>
<li>In C++: long long must be at least 64-bit.</li>
</ul>
<p>The library has been implemented in:</p>
<ul>
<li>C++: A C++20 compliant compiler.</li>
<li>Java 8 or greater,</li>
<li>Python 3.9 or greater,</li>
</ul>
<h2><a id="Languages_34"></a>Languages</h2>
<p>The implementations are coded in C++, Java and Python. For each language there is a set of tests to verify the functionality of the classes.</p>
<p>Generally it should be easy to code in any of the languages as the classes mostly have similar names and the methods in the classes are generally similar. C++ class and method names are regarded as the standard. This means that in the case of Java, method names start with a capital letter and, in the case of Python, methods have names that are are snake case and may need to reflect the data types they handle.</p>
<ul>
<li>
<p>C++ code is considered as the reference standard with the Java and Python code being based on this implementation.</p>
</li>
<li>
<p>Java:</p>
<ul>
<li>Java does not have multiple inheritance so the classes DateConversions and TimeConversions have been merged into the one class DateTimeConversions. This also means that the YMDHMS class has two fields of type YMD and HMS.</li>
<li>Java does not have operator overloading so the following method names correspond to the overloaded C++ operators:
    <ul>
        <li>assign (=), eq (==), ne (!=), lt (&lt;), le (&lt;=), gt (&gt;), ge (&gt;=),</li>
        <li>add (+), inc (+=), sub (-), dec (-=).</li>
    </ul>
</li>
</ul>
</li>
<li>
<p>Python:</p>
<ul>
<li>The python classes generally type check arguments and will automatically convert input parameters to the correct type where possible.</li>
<li>Because of the Duck Typing feature of Python, some methods explicitly check the type of their parameters.</li>
</ul>
</li>
</ul>
<h2><a id="Usage_52"></a>Usage</h2>
<p>A good approach is as follows:</p>
<ol>
<li>Create an instance of the <strong>DateTimeFormat</strong> class. This will give you reasonable default formatting parameters that you can change. Instances of this class are needed by instances of the classes <strong>DateTimeFormatter</strong> and <strong>DateTimeParser</strong>.</li>
<li>Create an instance of the <strong>DateTimeFormatter</strong> class in order to format dates, times and time intervals.</li>
<li>Create an instance of the <strong>DateTimeParser</strong> class in order to parse dates, times and time intervals represented as strings.</li>
<li>Create an instance of the <strong>JulianDateTime</strong> class. This will give you access to all the necessary methods for date and time manipulations including addition and date differences. This class stores the date and time and inherits <strong>DateConversions</strong> and <strong>TimeConversions</strong> (<strong>DateTimeConversions</strong> in the case of Java). These conversion classes do not store any data, they just provide methods for date and time conversions. This means that you can use these to implement your own specific functionality.</li>
</ol>
<p>In the <strong>JulianDateTime</strong> class, dates and times are represented as a continuous count of days and fractions of a day from -13200 August 15, Greenwich noon Julian proleptic calendar (JD-3100015.50) to 17191-Mar-15, Greenwich noon Gregorian Calendar (JD8000016.50). The underlying storage is that of an integer representing the Julian Day Number for noon on that date and a floating-point number representing the fraction of the day running from midday to midday (always positive). This should yield a precision of around 1ns covering this period.</p>
<h2><a id="Examples_63"></a>Examples</h2>
<p>Examples are provided for C++, Java and Python. In addition a QT5 and an Android example are also provided.</p>
<h2><a id="Testing_68"></a>Testing</h2>
<p>The C++ tests are the reference ones with  <em>LCOV</em> reporting 99% line coverage and 94% function coverage for <strong>dt</strong>.</p>
<p>Tests for Java and Python also provide similar coverage.</p>
<h2><a id="Documentation_75"></a>Documentation</h2>
<p>This can be generated via the scripts <code>MakeAllTheDocumentation.sh</code> for Linux and Apple, <code>MakeAllTheDocumentation.cmd</code> for Windows. In order to generate the documentation the following programs must be installed, Doxygen (for C++ and Java), Java Development Kit (for Java) and Sphinx (for Python).</p>
<h2><a id="Directory_Structure_80"></a>Directory Structure</h2>
<p>At the top level we have:</p>
<ul>
<li><code>Android</code> - containing the Android example.</li>
<li><code>C++</code> - containing the C++ code.</li>
<li><code>hooks</code> - containing various hook scripts for git.</li>
<li><code>Java</code> - containing the Java code.</li>
<li><code>Python</code> - containing the Python code.</li>
<li><code>Repository</code> - containing the master files that need to be edited and copied to various parts of the directory tree using <code>CopyDocumentation.cmd</code> or <code>CopyDocumentation.sh</code> in the top-level directory.</li>
</ul>
<p>For each language (C++, Java, Python) the main subdirectories are:</p>
<ul>
<li><code>doc</code> - containing the documentation for the Date Time Library and tests.</li>
<li><code>dt</code> - The Date Time Library</li>
<li><code>dtExamples</code> - containing examples</li>
<li><code>dtTests</code> - containing the tests along with a subdirectory <code>dtTestAll</code> that contains a driver to run all the tests.</li>
<li>MakeDocumentation - containing scripts for generating the documentation.</li>
</ul>
<h2><a id="License_100"></a>License</h2>
<p>The Date Time Library is an open-source toolkit licensed under the <a href="http://en.wikipedia.org/wiki/BSD_licenses">BSD</a> license.</p>
<p>Copyright (c) 2012-2018 Andrew J. P. Maclean
All rights reserved.</p>
<p>Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:</p>
<ul>
<li>Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.</li>
<li>Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.</li>
<li>Neither the name of Andrew J. P. Maclean nor the names of any contributors
may be used to endorse or promote products derived from this software
without specific prior written permission.</li>
</ul>
<p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ‘‘AS IS’’
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>

</body></html>
