# Change Log

## Version 3.3.0

### All languages

#### DT

- **AlmostEqual()** has been replaced with **IsClose()**

### Python

Python 3.5 is the minimum requirement since `math.isclose()` is now used.


## Version 3.2.0

Fixed some Android bugs. Removed the dt library in Android, it now uses a copy of the dt files in the Java source instead.

## Version 3.0.0

### Directory Structure

At the top level we have:

- `Android` containing the Android example.
- `C++` containing the C++ code.
- `hooks` containing various hook scripts for git.
- `Java` containing the Java code.
- `Python` containing the Python code.
- `Repository` containing the master files that need to be edited and copied to various parts of the directory tree.

For each language (C++, Java, Python) the main subdirectories are:

- `doc` containing the documentation for the Date Time Library and tests.
- `dt` The Date Time Library
- `dtExamples` containing examples
- `dtTests` containing the tests along with a subdirectory `dtTestAll` that contains a driver to run all the tests.
- MakeDocumentation containing scripts for generating the documentation.

### All languages

#### DT

- A new method **AlmostEqual()** has been introduced, this is used for comparing floating point numbers.
- All conversions now use full precision, accordingly `precision` and `maxPrecision` have been removed from the parameters in the corresponding methods.
- A new class called **DateTimeBase** has been introduced, this replaces **CommonDefintions**.
- New classes/enumerations have been introduced in order to provide better type safety and make the code clearer, they are:
  - **CALENDAR_TYPE**, specifies which calendar to use.
  - **DAY_NAME**, controls whether or not day names are displayed.
  - **FIRST_DOW**, specifies whether to treat either Sunday or Monday as the first day of the week.
  - **JG_CHANGEOVER**, this holds constants related to the Julian/Gregorian changeover.
  - **MONTH_NAME**, controls whether or not month names are displayed.

  These are scoped enumerations in C++, enums in Java and just constants within their own modules in Python.
- **DayNames**, **MonthNames**, **TimeZones** now have copy constructors and tests for equality and inequality.
- The formatting parameters for displaying and reading date and time information is now controlled by a class called **DateTimeFormat**.  It is populated with reasonable default values for the formatting parameters, however, the user can easily change these values.
- **DateFormatter** has now been moved from the examples into **dt** and renamed to **DateTimeFormatter**. This class has been introduced to simplify the formatting complexity associated with formatting dates and times. Rounding to a desired precision is now confined to the display functions in **DateTimeFormatter**. `maximumPrecision` is now a member of this class so it has been removed from the formatting statements.

#### Testing

- A new class: **CommonTests** has been introduced. This class also holds the precisions for seconds, minutes and fractions of the day that are used for testing.
- More tests have been introduced.
- Many functions have been rewritten.

#### Examples

- Added a new example **SimpleExample**.

#### Documentation

Each language specific version has its own documentation folder.

***

### C++

- Overall more code is now C++11 compliant.
- Singleton classes have been removed.
- Default parameters have been removed.

#### DT

#### Testing

- A new class called **NonCopyable** has been introduced.

#### Examples

***

### Java

#### DT

- In  **TimeZones** the methods:

  - `GetTimeZoneName(...)` returns a pair `<boolean, String>` instead of a String.
  - `GetTimeZoneValue(...)` returns a pair `<boolean, Integer>` instead of an int.

  The boolean value indicates whether the function executed successfully or not.

- In **MonthNames** and **DayNames** the methods:
  - `GetDayName`, `GetAbbreviatedDatName`, `GetMonthName`, and `GetAbbreviatedMonthName` now return an empty string instead of null if the index is out of range.

#### Testing

#### Examples

***

### Python

#### DT

- **CheckCalendar** has been removed.

#### Testing

#### Examples

***
