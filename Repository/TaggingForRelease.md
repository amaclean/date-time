# Tagging for Release

## Tags

Tags are of the form **vx.y** where:

- **x**: major version
- **y**: minor version

Once a release has been tagged, you will need to update the version numbers in several files.

## Updating Version Numbers

If this is a minor version update, the version number will be updated from **x.y** to **x.z** where **z=y+1**. For a major version update, **x.y** will change to **w.0** where **w = x+1**. In this case of a major version update, the minor version is set to **0**.

## Files to Update

These files need to be updated:

- ./Android/DateTimeCalculator/app/src/main/res/values/strings.xml

   Update: *version* in this file.

- ./C++/CMakeLists.txt

   Set *CMAKE_CXX_STANDARD* to 20 or greater.

- ./C++/CMake/Version.cmake

   Update: *SRC_MAJOR_VERSION, SRC_MINOR_VERSION, SRC_BUILD_VERSION*

- ./C++/MakeDocumentation/Doxyfile

   Update: *PROJECT_NUMBER* in this file.

- ./Java/DateTime/src/MakeDocumentation/Doxyfile

   Update: *PROJECT_NUMBER* in this file.

- ./Python/MakeDocumentation/conf.py

   Update: *version* and *release* in this file.

## Updating Introductory Information

These files may need to be updated:

- ./Repository/README.html
- ./Repository/README.md
- ./Repository/TaggingForRelease.md

## Update Python RST files:

These files may need to be updated:

- ./Python/MakeDocumentation/index.rst
- ./Python/MakeDocumentation/dt.rst
- ./Python/MakeDocumentation/Examples.rst
- ./Python/MakeDocumentation/Testing.rst

## Updating Copyright

The copyright notices may need to be updated in these files:

- ./Repository/LICENSE
- ./Repository/README.html
- ./Repository/README.md
- ./Python/MakeDocumentation/conf.py
- ./Python/MakeDocumentation/index.rst

## Update files in the Repository folder

When this is done, go to the top-level directory and run:

- **CopyDocumentation.sh** or **CopyDocumentation.cmd** to copy the various markdown and other files to the appropriate locations.
- **MakeAllTheDocumentation.sh** or **MakeAllTheDocumentation.cmd** to update the documentation in the doc folders for each language.

## Java/Android

Copy **Java/DateTime/src/dt** to **Android/DateTimeCalculator/app/src/main/java** and verify that the Android Example works.
