echo off
rem Purpose: Take the documentation in the Repository and copy it to
rem  the appropriate places in the directory tree.
rem Run this script from the top level folder.

rem Locate the documentation.
set "src_dir=%~dp0\Repository\"
rem The target folders.
set docCmdFolder="MakeDocumentation"

set root_path=%cd%

copy /B %src_dir%\CHANGELOG.md .
copy /B %src_dir%\DevelopersPleaseReadThisFirst.md .
copy /B %src_dir%\LICENSE .
copy /B %src_dir%\README.md .
copy /B %src_dir%\TaggingForRelease.md .

copy /B %src_dir%\LICENSE  "%~dp0\C++"
copy /B %src_dir%\README.md  "%~dp0\C++"

copy /B %src_dir%\LICENSE  "%~dp0\Java"
copy /B %src_dir%\README.md  "%~dp0\Java"
copy /B %src_dir%\README.html  "%~dp0\Java"

copy /B %src_dir%\LICENSE  "%~dp0\Python"
copy /B %src_dir%\README.md  "%~dp0\Python"

echo Done.
