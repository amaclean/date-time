#!/usr/bin/env python3


from datetime import datetime

import dtTests


def main():
    t0 = datetime.now()

    #  If messages length is greater than zero then the failing
    #   test names along with the failed tests is returned.
    messages = list()
    res = dtTests.DateConversionsTests().test_all()
    if len(res) > 2:
        messages.extend(res)
    res = dtTests.DayNamesTests().test_all()
    if len(res) > 2:
        messages.extend(res)
    res = dtTests.HMSTests().test_all()
    if len(res) > 2:
        messages.extend(res)
    res = dtTests.HMTests().test_all()
    if len(res) > 2:
        messages.extend(res)
    res = dtTests.ISOWeekDateTests().test_all()
    if len(res) > 2:
        messages.extend(res)
    res = dtTests.JDTests().test_all()
    if len(res) > 2:
        messages.extend(res)
    res = dtTests.MonthNamesTests().test_all()
    if len(res) > 2:
        messages.extend(res)
    res = dtTests.TimeConversionsTests().test_all()
    if len(res) > 2:
        messages.extend(res)
    res = dtTests.TimeIntervalTests().test_all()
    if len(res) > 2:
        messages.extend(res)
    res = dtTests.TimeZonesTests().test_all()
    if len(res) > 2:
        messages.extend(res)
    res = dtTests.YMDTests().test_all()
    if len(res) > 2:
        messages.extend(res)
    res = dtTests.YMDHMSTests().test_all()
    if len(res) > 2:
        messages.extend(res)

    res = dtTests.DateTimeFormatterTests().test_all()
    if len(res) > 2:
        messages.extend(res)
    res = dtTests.DateTimeParserTests().test_all()
    if len(res) > 2:
        messages.extend(res)
    res = dtTests.FoDHMSTests().test_all()
    if len(res) > 2:
        messages.extend(res)
    res = dtTests.JulianDateTimeTests().test_all()
    if len(res) > 2:
        messages.extend(res)

    # ok = True
    if messages:
        print('\n'.join(messages))
    else:
        print('All tests passed.')

    t1 = datetime.now()
    delta_t = t1 - t0
    print('Test started:  ', t0.strftime('%Y-%m-%d %H:%M:%S.%f'))
    print('Test finished: ', t1.strftime('%Y-%m-%d %H:%M:%S.%f'))
    print('Elapsed time:   {:s}'.format(str(delta_t)))


if __name__ == '__main__':
    main()
