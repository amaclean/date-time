#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   MonthNamesTests.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


import dtTests.CommonTests as CommonTests
from dt.MonthNames import MonthNames


class MonthNamesTests(CommonTests):
    """
    This class is a test harness to test the MonthNames class.

    It also provides examples of how to use the MonthNames class.

    :author: Andrew J. P. Maclean
    """

    def __init__(self):
        self.mn = MonthNames()

    def test_constructors(self):
        """
        Run the tests for the constructors.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        a = MonthNames()
        pf = self.pass_fail(a == self.mn)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} constructor {pf[1]}')
            all_passed = False

        res = ['  Testing constructors']
        if not all_passed:
            res.extend(failures)
        return res

    def test_assignment(self):
        """
        Run the tests for assignment.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        a = self.mn
        pf = self.pass_fail(a == self.mn)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} assignment {pf[1]}')
            all_passed = False

        res = ['  Testing assignment']
        if not all_passed:
            res.extend(failures)
        return res

    def test_comparison_operators(self):
        """
        Run the tests for comparison.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        a = MonthNames()
        pf = self.pass_fail(a == self.mn)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≡ {pf[1]}')
            all_passed = False
        idx += 1

        a.set_month_name(1, 'FIIK')
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≠ {pf[1]}')
            all_passed = False

        res = ['  Testing comparison operators']
        if not all_passed:
            res.extend(failures)
        return res

    def test_month_names(self):
        """
        Run the tests for testing the month names.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        abbrev_length = 3

        first = True
        for k in self.mn.names:
            name = self.mn.get_month_name(k)
            abbrev = self.mn.get_abbreviated_month_name(k)
            if name[:abbrev_length] != abbrev:
                if first:
                    failures.append(f'   Test {idx:2d} get_abbreviated_month_name(month_name)')
                    first = False
                failures.append(f'     - {name:s}: {abbrev:s}')
                all_passed = False
        idx += 1

        first = True
        for k in self.mn.names:
            name = self.mn.get_month_name(k)
            abbrev = self.mn.get_abbreviated_month_name(k, len(name))
            if name != abbrev:
                if first:
                    failures.append(f'   Test {idx:2d}  get_abbreviated_month_name(month_name, len(month_name)')
                    first = False
                failures.append(f'     - {name:s}: {abbrev:s}')
                all_passed = False
        idx += 1

        pf = self.pass_fail(self.mn.get_month_name(0) is None
                            and self.mn.get_month_name(13) is None)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} range error: get_month_name() ')
            all_passed = False
        idx += 1

        pf = self.pass_fail(self.mn.get_abbreviated_month_name(0) is None
                            and self.mn.get_abbreviated_month_name(13) is None)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} range error: get_abbreviated_month_name()')
            all_passed = False
        idx += 1

        month_name = self.mn.get_month_name(5)
        new_name = 'Month 5'
        self.mn.set_month_name(5, new_name)
        pf = self.pass_fail(self.mn.get_month_name(5) == new_name)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} check if month name can be changed: {pf[1]}')
            all_passed = False
        self.mn.set_month_name(5, month_name)

        res = ['  Testing month names and abbreviations']
        if not all_passed:
            res.extend(failures)
        return res

    def test_all(self):
        """
        Run all the tests.

        The list returned has a length of >2 if there are test failures.

        :return: A list.
        """

        messages = ['MonthNames Tests', ' All tests passed']
        res = self.test_constructors()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_assignment()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_comparison_operators()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_month_names()
        if len(res) > 1:
            messages.extend(res)

        if len(messages) > 2:
            messages[1] = ' Some tests failed:'
        return messages


def main():
    res = MonthNamesTests().test_all()
    print('\n'.join(res))


if __name__ == '__main__':
    main()
