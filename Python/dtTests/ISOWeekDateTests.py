#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   ISOWeekDateTests.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


from dt.ISOWeekDate import ISOWeekDate
from dtTests.CommonTests import CommonTests


class ISOWeekDateTests(CommonTests):
    """
    This class is a test harness to test the ISOWeekDate class.

    It also provides examples of how to use the ISOWeekDate class.

    :author: Andrew J. P. Maclean
    """

    def __init__(self):
        self.year = 2009
        self.week_number = 53
        self.week_day = 4
        self.iwd_list = [self.year, self.week_number, self.week_day]
        self.value_str = '2009-W53-4'

    def test_constructors(self):
        """
        Run the tests for the constructors.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        a = ISOWeekDate()
        pf = self.pass_fail(a.year == 0 and a.week_number == 0 and a.week_day == 0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ISOWeekDate() {pf[1]}')
            all_passed = False
        idx += 1

        b = ISOWeekDate(self.year, self.week_number, self.week_day)
        pf = self.pass_fail(b.year == self.year
                            and b.week_number == self.week_number
                            and b.week_day == self.week_day)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ISOWeekDate(year, week_number, week_day) {pf[1]}')
            all_passed = False
        idx += 1

        b = ISOWeekDate(year=self.year, week_number=self.week_number, week_day=self.week_day)
        pf = self.pass_fail(b.year == self.year
                            and b.week_number == self.week_number
                            and b.week_day == self.week_day)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ISOWeekDate(kwargs) {pf[1]}')
            all_passed = False
        idx += 1

        b = ISOWeekDate(self.iwd_list)
        pf = self.pass_fail(b.year == self.year
                            and b.week_number == self.week_number
                            and b.week_day == self.week_day)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ISOWeekDate(list) {pf[1]}')
            all_passed = False

        res = ['  Testing constructors']
        if not all_passed:
            res.extend(failures)
        return res

    def test_set(self):
        """
        Run the tests for setting members.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        a = ISOWeekDate()
        a.set_iso_week_date(self.year, self.week_number, self.week_day)
        pf = self.pass_fail(
            a.year == self.year
            and a.week_number == self.week_number and a.week_day == self.week_day)
        a.set_iso_week_date(0, 0, 0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_iso_week_date(year, week_number, week_day) {pf[1]}')
            all_passed = False
        idx += 1

        a.set_iso_week_date(year=self.year, week_number=self.week_number, week_day=self.week_day)
        pf = self.pass_fail(
            a.year == self.year
            and a.week_number == self.week_number and a.week_day == self.week_day)
        a.set_iso_week_date(0, 0, 0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_iso_week_date(kwargs) {pf[1]}')
            all_passed = False
        idx += 1

        a.set_iso_week_date(self.iwd_list)
        pf = self.pass_fail(
            a.year == self.year
            and a.week_number == self.week_number and a.week_day == self.week_day)
        a.set_iso_week_date(0, 0, 0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_iso_week_date(list) {pf[1]}')
            all_passed = False

        res = ['  Testing setting members']
        if not all_passed:
            res.extend(failures)
        return res

    def test_comparison_operators(self):
        """
        Run the tests for the comparison operators.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        h1 = ISOWeekDate(self.iwd_list)
        h2 = ISOWeekDate(self.iwd_list)
        pf = self.pass_fail(h1 == h2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≡ {pf[1]}')
            all_passed = False
        idx += 1

        h1 = ISOWeekDate(self.iwd_list)
        h2 = ISOWeekDate([self.year, self.week_number, self.week_day - 1])
        pf = self.pass_fail(h1 != h2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≠ {pf[1]}')
            all_passed = False
        idx += 1

        a = ISOWeekDate(2013, 7, 2)
        b = ISOWeekDate(2014, 7, 2)
        c = ISOWeekDate(2013, 8, 2)
        d = ISOWeekDate(2013, 7, 3)

        pf = self.pass_fail(a < b and a < c and a < d
                            and a <= b and a <= c and d >= a >= a
                            and not (a > b) and not (a >= b))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} < and ≤ {pf[1]}')
            all_passed = False
        idx += 1

        e = ISOWeekDate(2013, 7, 2)
        f = ISOWeekDate(2012, 7, 2)
        g = ISOWeekDate(2013, 6, 2)
        h = ISOWeekDate(2013, 7, 1)

        pf = self.pass_fail(e > f and e > g and e > h
                            and e >= f and e >= g and h <= e <= e
                            and not (e < f) and not (e <= f))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} > and ≥ {pf[1]}')
            all_passed = False

        res = ['  Testing comparison operators']
        if not all_passed:
            res.extend(failures)
        return res

    def test_assignment(self):
        """
        Run the tests for assignment.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        h1 = ISOWeekDate(self.iwd_list)
        h2 = h1
        pf = self.pass_fail(h1 == h2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} = {pf[1]}')
            all_passed = False

        res = ['  Testing assignment']
        if not all_passed:
            res.extend(failures)
        return res

    def test_iso_week_date(self):
        """
        Run the tests for making a ISOWeekDate object.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        a = ISOWeekDate()
        b = ISOWeekDate(a)
        pf = self.pass_fail(a == b)
        if not pf[0]:
            failures.append(f'   Test {idx:2d}  ISOWeekDate(iwd) {pf[1]}')
            all_passed = False
        idx += 1

        a = ISOWeekDate(self.year, self.week_number, self.week_day)
        b = ISOWeekDate(self.year, self.week_number, self.week_day)
        pf = self.pass_fail(a == b)
        if not pf[0]:
            failures.append(f'   Test {idx:2d}  ISOWeekDate(year, week_number, week_day) {pf[1]}')
            all_passed = False
        idx += 1

        a = ISOWeekDate(year=self.year, week_number=self.week_number, week_day=self.week_day)
        b = ISOWeekDate(year=self.year, week_number=self.week_number, week_day=self.week_day)
        pf = self.pass_fail(a == b)
        if not pf[0]:
            failures.append(f'   Test {idx:2d}  ISOWeekDate(kwargs) {pf[1]}')
            all_passed = False
        idx += 1

        a = ISOWeekDate(self.iwd_list)
        b = ISOWeekDate(self.iwd_list)
        pf = self.pass_fail(a == b)
        if not pf[0]:
            failures.append(f'   Test {idx:2d}  ISOWeekDate(list) {pf[1]}')
            all_passed = False

        res = ['  Testing assignment']
        if not all_passed:
            res.extend(failures)
        return res

    def test_all(self):
        """
        Run all the tests.

        The list returned has a length of >2 if there are test failures.

        :return: A list.
        """

        messages = ['ISOWeekDate Tests', ' All tests passed']
        res = self.test_constructors()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_set()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_comparison_operators()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_assignment()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_iso_week_date()
        if len(res) > 1:
            messages.extend(res)

        if len(messages) > 2:
            messages[1] = ' Some tests failed:'
        return messages


def main():
    res = ISOWeekDateTests().test_all()
    print('\n'.join(res))


if __name__ == '__main__':
    main()
