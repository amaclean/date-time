#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   TimeZonesTests.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


from dt.TimeZones import TimeZones
from dtTests.CommonTests import CommonTests


class TimeZonesTests(CommonTests):
    """
    This class is a test harness to test the date and time classes.

    It also provides examples of how to use the JulianDateTime class.

    :author: Andrew J. P. Maclean
    """

    def __init__(self):
        """
        """
        self.tz = TimeZones()

    def test_constructors(self):
        """
        Run the tests for the constructors.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        a = TimeZones()
        pf = self.pass_fail(a == self.tz)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} TimeZones() {pf[1]}')
            all_passed = False

        res = ['  Testing constructors']
        if not all_passed:
            res.extend(failures)
        return res

    def test_assignment(self):
        """
        Run the tests for assignment.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        a = self.tz
        pf = self.pass_fail(a == self.tz)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} = {pf[1]}')
            all_passed = False

        res = ['  Testing assignment']
        if not all_passed:
            res.extend(failures)
        return res

    def test_comparison_operators(self):
        """
        Run the tests for comparison.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        a = TimeZones()
        pf = self.pass_fail(a == self.tz)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≡ {pf[1]}')
            all_passed = False
        idx += 1

        a.tzNames[1] = 'FIIK'
        pf = self.pass_fail(a != self.tz)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≠ {pf[1]}')
            all_passed = False

        res = ['  Testing comparison operators']
        if not all_passed:
            res.extend(failures)
        return res

    def test_time_zones(self):
        """
        Run the tests for testing the time zones.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        tz_map = self.tz.get_time_zones_by_name()
        inv_tz_map = self.tz.get_time_zones_by_value()
        if not (tz_map and len(tz_map) == len(inv_tz_map)):
            failures.append(f'   Test {idx:2d} len(GetTimeZonesByName()) = {len(tz_map):d} ≠'
                            f' len(GetTimeZonesByValue()) = {len(inv_tz_map):d}')
            all_passed = False
        idx += 1

        if tz_map and len(tz_map) == len(inv_tz_map):
            ok = True
            for k in tz_map:
                if inv_tz_map[tz_map[k]] != k:
                    if ok:
                        failures.append(f'   Test {idx:2d} failed, indexes of the time zone maps do not match')
                        ok = False
                        all_passed = False
                    s = f'    - {k:s}, {inv_tz_map[tz_map[k]]:s}'
                    failures.append(s)

        res = ['  Testing time zones']
        if not all_passed:
            res.extend(failures)
        return res

    def test_all(self):
        """
        Run all the tests.

        The list returned has a length of >2 if there are test failures.

        :return: A list.
        """

        messages = ['TimeZone Tests', ' All tests passed']

        res = self.test_constructors()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_assignment()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_comparison_operators()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_time_zones()
        if len(res) > 1:
            messages.extend(res)

        if len(messages) > 2:
            messages[1] = ' Some tests failed:'
        return messages


def main():
    res = TimeZonesTests().test_all()
    print('\n'.join(res))


if __name__ == '__main__':
    main()
