#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   DateTimeParserTests.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


from math import isclose

from dt import DateTimeBase, DateTimeParser, DateTimeFormat, TimeInterval, HMS, YMDHMS
from dtTests.CommonTests import CommonTests


class DateTimeParserTests(CommonTests, DateTimeBase):
    """
    This class is a test harness to test the DateTimeParser class.

    It also provides examples of how to use the DateTimeParser class.

    :author: Andrew J. P. Maclean
    """

    def __init__(self):
        self.parser = DateTimeParser()
        self.fmt = DateTimeFormat()

        self.str_dt = {
            # Positive dates
            0: {"strDate": '2013-09-06', 'date': YMDHMS([2013, 9, 6, 0, 0, 0])},
            1: {"strDate": '2013-09-06 23', 'date': YMDHMS([2013, 9, 6, 23, 0, 0])},
            2: {"strDate": '2013-09-06 23:59', 'date': YMDHMS([2013, 9, 6, 23, 59, 0])},
            3: {"strDate": '2013-09-06 23:59:59', 'date': YMDHMS([2013, 9, 6, 23, 59, 59])},
            4: {"strDate": '2013-09-06 23:29:59.999', 'date': YMDHMS([2013, 9, 6, 23, 29, 59.999])},
            5: {"strDate": '2013-09-06 23:59:59.9995', 'date': YMDHMS([2013, 9, 6, 23, 59, 59.9995])},
            6: {"strDate": '2013-Sep-06 23:59:59.9995', 'date': YMDHMS([2013, 9, 6, 23, 59, 59.9995])},
            7: {"strDate": '2013-September-06 23:59:59.9995', 'date': YMDHMS([2013, 9, 6, 23, 59, 59.9995])},
            # Negative dates
            8: {"strDate": '-2013-09-06', 'date': YMDHMS([-2013, 9, 6, 0, 0, 0])},
            9: {"strDate": '-2013-09-06 23', 'date': YMDHMS([-2013, 9, 6, 23, 0, 0])},
            10: {"strDate": '-2013-09-06 23:59', 'date': YMDHMS([-2013, 9, 6, 23, 59, 0])},
            11: {"strDate": '-2013-09-06 23:59:59', 'date': YMDHMS([-2013, 9, 6, 23, 59, 59])},
            12: {"strDate": '-2013-09-06 23:29:59.999', 'date': YMDHMS([-2013, 9, 6, 23, 29, 59.999])},
            13: {"strDate": '-2013-09-06 23:59:59.9995', 'date': YMDHMS([-2013, 9, 6, 23, 59, 59.9995])},
        }

        self.parse_str_dt_fail = {'': 2, '2013': 3, '2013-Fik-09': 4}

        self.parse_str_ti_fail = {'': 2, '00:00:00': 6, ' 0d  00:00:.5': 5,
                                  ' d  00:00:.5': 6, ' x  00:00:00': 6, ' x': 6}

        self.decimal_string_parse_data = {
            # Should pass:
            0: {'str': '-3100015', 'intFrac': [-3100015, 0]},
            1: {'str': '-3100015.25', 'intFrac': [-3100015, -0.25]},
            2: {'str': '-3100015.50', 'intFrac': [-3100015, -0.5]},
            3: {'str': '-3100015.75', 'intFrac': [-3100015, -0.75]},
            4: {'str': '2456751', 'intFrac': [2456751, 0]},
            5: {'str': '2456751.25', 'intFrac': [2456751, 0.25]},
            6: {'str': '2456751.50', 'intFrac': [2456751, 0.5]},
            7: {'str': '2456751.75', 'intFrac': [2456751, 0.75]},
            # Should fail:
            8: {'str': '', 'intFrac': [0, 0.0]},
            9: {'str': ' ', 'intFrac': [0, 0.0]},
            10: {'str': 'ABC', 'intFrac': [0, 0.0]},
            11: {'str': '.5', 'intFrac': [0, 0.5]},
            12: {'str': '-.5', 'intFrac': [0, -0.5]},
            # Should pass:
            13: {'str': '0.5', 'intFrac': [0, 0.5]},
            14: {'str': '-0.5', 'intFrac': [0, -0.5]},
        }

        self.str_t = {
            0: {'strTime': '23:59:59.5', 'time': HMS([23, 59, 59.5])},
            1: {'strTime': '23:59', 'time': HMS([23, 59, 0])},
            2: {'strTime': '23', 'time': HMS([23, 0, 0])},
            3: {'strTime': '00:59:59.5', 'time': HMS([0, 59, 59.5])},
            4: {'strTime': '00:00:59.5', 'time': HMS([0, 0, 59.5])},
            5: {'strTime': '-23:59:59.5', 'time': HMS([-23, 59, 59.5])},
            6: {'strTime': '-23:59', 'time': HMS([-23, 59, 0])},
            7: {'strTime': '-23', 'time': HMS([-23, 0, 0])},
            8: {'strTime': '-00:59:59.5', 'time': HMS([0, -59, 59.5])},
            9: {'strTime': '-00:00:59.5', 'time': HMS([0, 0, -59.5])},
        }

        self.parse_str_t_fail = {'': 2, '12:5.7:.3': 5, '12:5:.3': 5, '12:5.7': 5,
                                 '5.7': 5, '23:Fik:09': 5, '23:59:59:5': 5
                                 }

        self.tidt = {
            0: {'ti': TimeInterval(0, 0), 'tiDHMS': ' 0d  00:00:0'},
            1: {'ti': TimeInterval(-1, -0.25), 'tiDHMS': '-1d -06:00:00.0'},
            2: {'ti': TimeInterval(-1, 0.25), 'tiDHMS': '-1d  06:00:00.0'},
            3: {'ti': TimeInterval(1, -0.25), 'tiDHMS': ' 1d -06:00:00.0'},
            4: {'ti': TimeInterval(1, 0.25), 'tiDHMS': ' 1d  06:00:00.0'},
        }

    def test_parse_dec_num(self):
        """
        Run the test for parsing a decimal string.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        for k, v in self.decimal_string_parse_data.items():
            res = self.parser.parse_dec_num(v['str'])
            if res[0] == 1:
                if res[1] != v['intFrac']:
                    failures.append(f'   Test {k:2d} parsing: {v["str"]:s}, expected: ({v["intFrac"][0]:d},'
                                    f' {v["intFrac"][1]:f}), got: ({res[1][0]:d}, {res[1][1]:f})')
                    all_passed = False
            else:
                if not v['str']:
                    continue
                if v['str'] == 'ABC':
                    continue
                if not (v['str'] == '.5' or v['str'] == '-.5' or v['str'] == ' '):
                    failures.append(f'   Test {k:2d} parsing: {v["str"]:s}, expected:'
                                    f' ({v["intFrac"][0]:d}, {v["intFrac"][1]:f})')
                    all_passed = False

        x = 3100015.75
        y = self.scale(x, 1)
        z = 3100015.8

        idx = len(self.decimal_string_parse_data)
        if not isclose(y, z, rel_tol=DateTimeBase.rel_fod):
            failures.append(f'   Test {idx:2d} testing rounding: {x:10.2f} expected: {y:10.2f} got {z:10.2f}')
            all_passed = False
        idx += 1
        x = -x
        y = self.scale(x, 1)
        z = -z
        if not isclose(y, z, rel_tol=DateTimeBase.rel_fod):
            failures.append(f'   Test {idx:2d} testing rounding: {x:10.2f} expected: {y:10.2f} got {z:10.2f}')
            all_passed = False

        res = ['  Testing parse_dec_num()']
        if not all_passed:
            res.extend(failures)
        return res

    def test_parse_date_time(self):
        """
        Run the test for parsing.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        for k, v in self.str_dt.items():
            res = self.parser.parse_date_time(v["strDate"], self.fmt)
            if res[0] == 1:
                if not v['date'].is_close(res[1], DateTimeBase.rel_seconds):
                    failures.append(f'   Test {k:2d}  parsing: {v["strDate"]:s},'
                                    f'  expected: {str(v["date"]):s} got: {str(res[1]):s}')
                    all_passed = False
            else:
                failures.append(f'   Test {k:2d}  parse failure parsing: {v["strDate"]:s},'
                                f' expected 1, got: {res[0]:d} = {self.parser.parserMessages[res[0]]:s}')
                all_passed = False

        res = ['  Testing parse_date_time()']
        if not all_passed:
            res.extend(failures)
        return res

    def test_parse_date_time_fail(self):
        """
        Run the test for parsing.

        Here we are testing for expected failures in parsing.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0
        for k, v in self.parse_str_dt_fail.items():
            res = self.parser.parse_date_time(k, self.fmt)
            if res[0] != v:
                failures.append(f'   Test {idx:2d} expected {v:d}, '
                                f'got: {res[0]:d} = {self.parser.parserMessages[res[0]]:s} '
                                f'The string being parsed is: {k:s}')
                all_passed = False

        res = ['  Testing parse_date_time() failures']
        if not all_passed:
            res.extend(failures)
        return res

    def test_parse_date_fail(self):
        """
        Run the test for parsing.

        Here we are testing for expected failures in parsing.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0
        for k, v in self.parse_str_dt_fail.items():
            res = self.parser.parse_date(k, self.fmt)
            if res[0] != v:
                failures.append(f'   Test {idx:2d} expected {v:d}, '
                                f'got: {res[0]:d} = {self.parser.parserMessages[res[0]]:s} '
                                f'The string being parsed is: {k:s}')
                all_passed = False

        res = ['  Testing parse_date() failures']
        if not all_passed:
            res.extend(failures)
        return res

    def test_parse_time(self):
        """
        Run the test for parsing.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        for k, v in self.str_t.items():
            res = self.parser.parse_time(v['strTime'], self.fmt)
            if res[0] == 1:
                if not v['time'].is_close(res[1], DateTimeBase.rel_seconds):
                    failures.append(
                        f'   Test {k:2d} parsing: {v['strTime']:s}, expected: {str(v['time']):s}, got: {str(res[1]):s}')
                    all_passed = False
            else:
                failures.append(f'   Test {k:2d}  parse failure parsing: {v["strTime"]:s},'
                                f' expected 1, got: {res[0]:d} = {self.parser.parserMessages[res[0]]:s}')
                all_passed = False

        res = ['  Testing parse_time()']
        if not all_passed:
            res.extend(failures)
        return res

    def test_parse_time_fail(self):
        """
        Run the test for parsing.

        Here we are testing for expected failures in parsing.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0
        for k, v in self.parse_str_t_fail.items():
            res = self.parser.parse_time(k, self.fmt)
            if res[0] != v:
                failures.append(f'   Test {idx:2d} expected {v:d}, '
                                f'got: {res[0]:d} = {self.parser.parserMessages[res[0]]:s} '
                                f'The string being parsed is: {k:s}')
                all_passed = False

        res = ['  Testing parse_time() failures']
        if not all_passed:
            res.extend(failures)
        return res

    def test_parse_time_interval(self):
        """
        Run the test for parsing a time interval.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        for k, v in self.tidt.items():
            # Test for independent signs for days and time.
            res = self.parser.parse_time_interval(v["tiDHMS"], self.fmt)
            if res[0] == 1:
                if not res[1].is_close(v['ti'], DateTimeBase.rel_seconds):
                    failures.append(
                        f'   Test {k:2d} parsing: {str(v['tiDHMS']):s}, expected: {str(v['ti']):s}, got: {str(res[1]):s}')
                    all_passed = False
            else:
                failures.append(f'   Test {k:2d}  parse failure parsing: {v["tiDHMS"]:s},'
                                f' expected 1, got: {res[0]:d} = {self.parser.parserMessages[res[0]]:s}')
                all_passed = False

        res = ['  Testing parse_time_interval()']
        if not all_passed:
            res.extend(failures)
        return res

    def test_parse_time_interval_fail(self):
        """
        Run the test for parsing.

        Here we are testing for expected failures in parsing.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        for k, v in self.parse_str_ti_fail.items():
            res = self.parser.parse_time_interval(k, self.fmt)
            if res[0] != v:
                failures.append(f'   Test {k:2d} expected {v:d}, '
                                f'got: {res[0]:d} = {self.parser.parserMessages[res[0]]:s} '
                                f'The string being parsed is: {k:s}')
                all_passed = False

        res = ['  Testing parse_time_interval() failures']
        if not all_passed:
            res.extend(failures)
        return res

    def test_all(self):
        """
        Run all the tests.

        The list returned has a length of >2 if there are test failures.

        :return: A list.
        """

        messages = ['DateTimeParser Tests', ' All tests passed']
        res = self.test_parse_dec_num()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_parse_date_time()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_parse_date_time_fail()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_parse_date_fail()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_parse_time()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_parse_time_fail()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_parse_time_interval()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_parse_time_interval_fail()
        if len(res) > 1:
            messages.extend(res)

        if len(messages) > 2:
            messages[1] = ' Some tests failed:'
        return messages


def main():
    res = DateTimeParserTests().test_all()
    print('\n'.join(res))


if __name__ == '__main__':
    main()
