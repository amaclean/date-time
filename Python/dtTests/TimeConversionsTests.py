#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   TimeConversionsTests.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


import dt
from dt.DateTimeBase import DateTimeBase
from dtTests.CommonTests import CommonTests


class TimeConversionsTests(CommonTests, DateTimeBase):
    """
    This class is a test harness to test the date and time classes.

    It also provides examples of how to use the JulianDateTime class.

    :author: Andrew J. P. Maclean
    """

    def __init__(self):
        """
        """
        self.tc = dt.TimeConversions()

    def test_hms_to_hr(self, hr, hms):
        """
        Test the conversion of hours minutes and seconds to hours.

        :param: hr The expected hour of the day.
        :param: hms The time.
        :return: [False|True, message]
        """
        h = self.tc.hms_to_h(hms)
        return self.compare(hr, h, 'hms_to_h')

    def test_hr_to_hms(self, hr, hms):
        """
        Test the conversion of hours to hours minutes and seconds.

        :param: hr The hour of the day.
        :param: hms The expected time.
        :return: [False|True, message]
        """
        to_hms = self.tc.h_to_hms(hr)
        return self.compare(hms, to_hms, 'h_to_hms')

    def test_hm_to_hr(self, hr, hm):
        """
        Test the conversion of hours and minutes to hours.

        :param: hr the expected hour of the day.
        :param: hm The time.
        :return: [False|True, message]
        """
        h = self.tc.hm_to_h(hm)
        return self.compare(hr, h, 'hm_to_h')

    def test_hr_to_hm(self, hr, hm):
        """
        Test the conversion of hours to hours and minutes.

        :param: hr The hour of the day.
        :param: hm The expected time.
        :return: [False|True, message]
        """
        to_hm = self.tc.h_to_hm(hr)
        return self.compare(hm, to_hm, 'h_to_hm')

    def test_time(self):
        """
        Run the tests for the week of the year conversions.

        :return: The list returned has a length of >1 if there are test failures.
        """
        test_times = {
            0: ['h=23.7625, 23:45:45', [23, 45, 45], 23.7625],
            1: ['h=-23.7625, -23:45:45', [-23, 45, 45], -23.7625],
            # Testing precision and minutes/seconds rollover.
            2: ['h=23.7625, 22:104:105.0 == 23:45:45', [22, 104, 105], 23.7625],
            3: ['h=23.7625, 23:44:105.0 == 23:45:45', [23, 44, 105], 23.7625],
            4: ['h=23.7625, 22:105:45.0 == 23:45:45', [22, 105, 45], 23.7625],
            5: ['h=-23.7625, -22:104:105.0 == -23:45:45', [-22, 104, 105], -23.7625],
            6: ['h=-23.7625, -23:44:105.0 == -23:45:45', [-23, 44, 105], -23.7625],
            7: ['h=-23.7625, -22:105:45.0 == -23:45:45', [-22, 105, 45], -23.7625],
            # Testing HM
            8: ['h=23.7625, 23:45.75', [23, 45.75], 23.7625],
            9: ['h=-23.7625, -23:45.75', [-23, 45.75], -23.7625],
            10: ['h=00.7625, 00:45.75', [0, 45.75], 0.7625],
            11: ['h=-00.7625, -00:45.75', [0, -45.75], -0.7625],
            # Testing precision and minutes rollover.
            12: ['h=1.0125, 00:60.75 == 01:00.75', [0, 60.75], 1.0125],
            13: ['h=-1.0125, -00:60.75 == -01:00.75', [0, -60.75], -1.0125],
        }
        # p = self.make_header('Testing times.', 2)

        failures = list()
        all_passed = True

        for k, v in test_times.items():
            if k < 8:
                # Convert to hours and then back to hours, minutes and seconds.
                t = self.tc.h_to_hms(self.tc.hms_to_h(dt.HMS(v[1])))
                pf = self.test_f1_f2(v[2], t, self.test_hms_to_hr, self.test_hr_to_hms)
                if not pf[0]:
                    failures.append(f'   Test {k:2d}, {v[0]} {pf[1]}')
                    all_passed = False
            else:
                # Convert to hours and then back to hours and minutes.
                t = self.tc.h_to_hm(self.tc.hm_to_h(dt.HM(v[1])))
                pf = self.test_f1_f2(v[2], t, self.test_hm_to_hr, self.test_hr_to_hm)
                if not pf[0]:
                    failures.append(f'   Test {k:2d}, {v[0]} {pf[1]}')
                    all_passed = False

        res = ['  Testing times']
        if not all_passed:
            res.extend(failures)
        return res

    def test_all(self):
        """
        Run all the tests.

        The list returned has a length of >2 if there are test failures.

        :return: A list.
        """

        messages = ['TimeConversions Tests', ' All tests passed']

        res = self.test_time()
        if len(res) > 1:
            messages.extend(res)

        if len(messages) > 2:
            messages[1] = ' Some tests failed:'
        return messages


def main():
    res = TimeConversionsTests().test_all()
    print('\n'.join(res))


if __name__ == '__main__':
    main()
