#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   DateTimeFormatterTests.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is disibuted WITHOUT ANY WARRANTY without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


from dt import DateTimeFormat, DateTimeFormatter, JulianDateTime, TimeInterval, TimeZones, HM
from dt.Constants import CalendarType, DayName, FirstDOW, MonthName
from dtTests.CommonTests import CommonTests
from dtTests.CommonTests import DictDiffer


class DateTimeFormatterTests(CommonTests):
    """
    This class is a test harness to test the DateTimeFormatter class.

    It also provides examples of how to use the DateTimeFormatter class.

    :author: Andrew J. P. Maclean
    """

    def __init__(self):
        self.formatter = DateTimeFormatter()
        self.max_precision = self.formatter.maximum_precision
        self.fmt = DateTimeFormat()

        self.test_data = (
            {'Calendar': 'Julian',
             'DayNameAbbrev': 'Mon',
             'NamedDate': 'Monday, 31 December 1900 15:12:11.123',
             'JDShort': '2415398.13346207',
             'Date': '1900-December-31 15:12:11.123',
             'DoY': '366.63346207',
             'DayName': 'Monday',
             'LeapYear': 'Yes',
             'IWD': '',
             'DoW': '2',
             'MonthNameAbbrev': 'Dec',
             'JD': '2415398.13346207175926',
             'MonthName': 'December'},

            {'Calendar': 'Gregorian',
             'DayNameAbbrev': 'Sun',
             'NamedDate': 'Sunday, 13 January 1901 15:12:11.123',
             'JDShort': '2415398.13346207',
             'Date': '1901-January-13 15:12:11.123',
             'DoY': '13.63346207',
             'DayName': 'Sunday',
             'LeapYear': 'No',
             'IWD': '1901-02-7',
             'DoW': '1',
             'MonthNameAbbrev': 'Jan',
             'JD': '2415398.13346207175926',
             'MonthName': 'January'},

            {'Calendar': 'Julian/Gregorian',
             'DayNameAbbrev': 'Sun',
             'NamedDate': 'Sunday, 13 January 1901 15:12:11.123',
             'JDShort': '2415398.13346207',
             'Date': '1901-January-13 15:12:11.123',
             'DoY': '13.63346207',
             'DayName': 'Sunday',
             'LeapYear': 'No',
             'IWD': '1901-02-7',
             'DoW': '1',
             'MonthNameAbbrev': 'Jan',
             'JD': '2415398.13346207175926',
             'MonthName': 'January'},

            {'Calendar': 'Julian/Gregorian',
             'DayNameAbbrev': 'Sun',
             'NamedDate': 'Sunday, 13 Jan 1901 15:12:11.123',
             'JDShort': '2415398.13346207',
             'Date': '1901-Jan-13 15:12:11.123',
             'DoY': '13.63346207',
             'DayName': 'Sunday',
             'LeapYear': 'No',
             'IWD': '1901-02-7',
             'DoW': '1',
             'MonthNameAbbrev': 'Jan',
             'JD': '2415398.13346207175926',
             'MonthName': 'January'},

            {'Calendar': 'Julian/Gregorian',
             'DayNameAbbrev': 'Sun',
             'NamedDate': 'Sunday, 13 Jan 1901 15:12:11.123',
             'JDShort': '2415398.13346207',
             'Date': '1901-01-13 15:12:11.123',
             'DoY': '13.63346207',
             'DayName': 'Sunday',
             'LeapYear': 'No',
             'IWD': '1901-02-7',
             'DoW': '1',
             'MonthNameAbbrev': 'Jan',
             'JD': '2415398.13346207175926',
             'MonthName': 'January'},

            {'Calendar': 'Julian/Gregorian',
             'DayNameAbbrev': 'Sun',
             'NamedDate': 'Sun, 13 Jan 1901 15:12:11.123',
             'JDShort': '2415398.13346207',
             'Date': '1901-01-13 15:12:11.123',
             'DoY': '13.63346207',
             'DayName': 'Sunday',
             'LeapYear': 'No',
             'IWD': '1901-02-7',
             'DoW': '1',
             'MonthNameAbbrev': 'Jan',
             'JD': '2415398.13346207175926',
             'MonthName': 'January'},

            {'Calendar': 'Julian/Gregorian',
             'DayNameAbbrev': 'Sun',
             'NamedDate': '1901-01-13 15:12:11.123',
             'JDShort': '2415398.13346207',
             'Date': '1901-01-13 15:12:11.123',
             'DoY': '13.63346207',
             'DayName': 'Sunday',
             'LeapYear': 'No',
             'IWD': '1901-02-7',
             'DoW': '1',
             'MonthNameAbbrev': 'Jan',
             'JD': '2415398.13346207175926',
             'MonthName': 'January'},

            {'Calendar': 'Julian/Gregorian',
             'DayNameAbbrev': 'Sun',
             'NamedDate': '1901-01-13 15:12:11.123',
             'JDShort': '2415398.13346207',
             'Date': '1901-01-13 15:12:11.123',
             'DoY': '13.63346207',
             'DayName': 'Sunday',
             'LeapYear': 'No',
             'IWD': '1901-02-7',
             'DoW': '7',
             'MonthNameAbbrev': 'Jan',
             'JD': '2415398.13346207175926',
             'MonthName': 'January'},

            {'Calendar': 'Julian/Gregorian',
             'DayNameAbbrev': 'Thu',
             'NamedDate': 'Thursday, 31 December 1500 15:12:11.123',
             'JDShort': '2269298.13346207',
             'Date': '1500-December-31 15:12:11.123',
             'DoY': '366.63346207',
             'DayName': 'Thursday',
             'LeapYear': 'Yes',
             'IWD': '',
             'DoW': '5',
             'MonthNameAbbrev': 'Dec',
             'JD': '2269298.13346207175926',
             'MonthName': 'December'},
        )

        self.dt_test_data = {
            0: {'jdt': JulianDateTime(-13200, 9, 1, 23, 59, 59.99995, CalendarType.julian_gregorian),
                'strDate': '-13200-09-02 00:00:00.0000',
                'jdLongDate': '-3099997.5000000',
                'jdLongDateAlt': '-3099997.5000000',
                'doyStr': '246.00000000'},

            1: {'jdt': JulianDateTime(-4712, 1, 1, 23, 59, 59.99995, CalendarType.julian_gregorian),
                'strDate': '-4712-01-02 00:00:00.0000',
                'jdLongDate': '0.5000000',
                'jdLongDateAlt': '0.5000000',
                'doyStr': '2.00000000'},

            2: {'jdt': JulianDateTime(2013, 9, 6, 23, 59, 59.99995, CalendarType.julian_gregorian),
                'strDate': '2013-09-07 00:00:00.0000',
                'jdLongDate': '2456542.5000000',
                'jdLongDateAlt': '2456542.5000000',
                'doyStr': '250.00000000'},

            3: {'jdt': JulianDateTime(2013, 9, 6, 23, 59, 59.999499999, CalendarType.julian_gregorian),
                'strDate': '2013-09-06 23:59:59.9995',
                'jdLongDate': '2456542.5000000',
                'jdLongDateAlt': '2456542.5000000',
                'doyStr': '249.99999999'},

            4: {'jdt': JulianDateTime(-13200, 9, 1, 23, 59, 59.99995, CalendarType.julian_gregorian),
                'strDate': '-13200-09-02 00:00:00',
                'jdLongDate': '-3099997.5000000',
                'jdLongDateAlt': '-3099998',
                'doyStr': '246'},

            5: {'jdt': JulianDateTime(-4712, 1, 1, 23, 59, 59.99995, CalendarType.julian_gregorian),
                'strDate': '-4712-01-02 00:00:00',
                'jdLongDate': '0.5000000',
                'jdLongDateAlt': '1',
                'doyStr': '2'},

            6: {'jdt': JulianDateTime(-4712, 1, 1, 23, 59, 59.99995, CalendarType.julian_gregorian),
                'strDate': '-4712-01-02 00:00:00',
                'jdLongDate': '0.5000000',
                'jdLongDateAlt': '1',
                'doyStr': '2'},

            7: {'jdt': JulianDateTime(2013, 9, 6, 23, 59, 59.99995, CalendarType.julian_gregorian),
                'strDate': '2013-09-07 00:00:00',
                'jdLongDate': '2456542.5000000',
                'jdLongDateAlt': '2456543',
                'doyStr': '250'},

            8: {'jdt': JulianDateTime(2013, 9, 6, 23, 59, 59.999499999, CalendarType.julian_gregorian),
                'strDate': '2013-09-07 00:00:00',
                'jdLongDate': '2456542.5000000',
                'jdLongDateAlt': '2456543',
                'doyStr': '250'},
        }

        # Some negative dates and times.
        self.dt_test_data_negative = {
            0: {'jdt': JulianDateTime(-13190, 8, 1, 0, 0, 0, CalendarType.julian_gregorian),
                'strDate': '-13190-08-01 00:00:00',
                'jdLongDate': '-3096377.5000000',
                'jdLongDateAlt': '-3096377.5000000',
                'doyStr': '213.00000000'},

            1: {'jdt': JulianDateTime(-13190, 8, 1, 6, 0, 0, CalendarType.julian_gregorian),
                'strDate': '-13190-08-01 06:00:00',
                'jdLongDate': '-3096377.2500000',
                'jdLongDateAlt': '-3096377.2500000',
                'doyStr': '213.25000000'},

            2: {'jdt': JulianDateTime(-13190, 8, 1, 12, 0, 0, CalendarType.julian_gregorian),
                'strDate': '-13190-08-01 12:00:00',
                'jdLongDate': '-3096377.0000000',
                'jdLongDateAlt': '-3096377.0000000',
                'doyStr': '213.50000000'},

            3: {'jdt': JulianDateTime(-13190, 8, 1, 18, 0, 0, CalendarType.julian_gregorian),
                'strDate': '-13190-08-01 18:00:00',
                'jdLongDate': '-3096376.7500000',
                'jdLongDateAlt': '-3096376.7500000',
                'doyStr': '213.75000000'},

            4: {'jdt': JulianDateTime(-4712, 1, 1, 0, 0, 0, CalendarType.julian_gregorian),
                'strDate': '-4712-01-01 00:00:00',
                'jdLongDate': '-0.5000000',
                'jdLongDateAlt': '-0.5000000',
                'doyStr': '1.00000000'},

            5: {'jdt': JulianDateTime(-4712, 1, 1, 6, 0, 0, CalendarType.julian_gregorian),
                'strDate': '-4712-01-01 06:00:00',
                'jdLongDate': '-0.2500000',
                'jdLongDateAlt': '-0.2500000',
                'doyStr': '1.25000000'},

            6: {'jdt': JulianDateTime(-4712, 1, 1, 12, 0, 0, CalendarType.julian_gregorian),
                'strDate': '-4712-01-01 12:00:00',
                'jdLongDate': '0.0000000',
                'jdLongDateAlt': '0.0000000',
                'doyStr': '1.50000000'},

            7: {'jdt': JulianDateTime(-4712, 1, 1, 18, 0, 0, CalendarType.julian_gregorian),
                'strDate': '-4712-01-01 18:00:00',
                'jdLongDate': '0.2500000',
                'jdLongDateAlt': '0.2500000',
                'doyStr': '1.75000000'},

            8: {'jdt': JulianDateTime(-4712, 1, 2, 0, 0, 0, CalendarType.julian_gregorian),
                'strDate': '-4712-01-02 00:00:00',
                'jdLongDate': '0.5000000',
                'jdLongDateAlt': '0.5000000',
                'doyStr': '2.00000000'},
        }

        self.ti = {
            0: {'ti': TimeInterval(0, 0), 'tiStr': '0.0000000', 'tiDHMS': '0d  00:00:00.0000'},
            1: {'ti': TimeInterval(-1, -0.5), 'tiStr': '-1.5000000', 'tiDHMS': '-1d -12:00:00.0000'},
            2: {'ti': TimeInterval(-1, 0.5), 'tiStr': '-0.5000000', 'tiDHMS': '-0d -12:00:00.0000'},
            3: {'ti': TimeInterval(1, -0.5), 'tiStr': '0.5000000', 'tiDHMS': '0d  12:00:00.0000'},
            4: {'ti': TimeInterval(11, -0.5), 'tiStr': '10.5000000', 'tiDHMS': '10d  12:00:00.0000'},
            5: {'ti': TimeInterval(10, 0.5), 'tiStr': '10.5000000', 'tiDHMS': '10d  12:00:00.0000'},
            6: {'ti': TimeInterval(1, -11.5), 'tiStr': '-10.5000000', 'tiDHMS': '-10d -12:00:00.0000'},
            7: {'ti': TimeInterval(-10, 0.5), 'tiStr': '-9.5000000', 'tiDHMS': '-9d -12:00:00.0000'},
            8: {'ti': TimeInterval(10, 0.5), 'tiStr': '10.5000000', 'tiDHMS': '10d  12:00:00.0000'},
            9: {'ti': TimeInterval(1, -11.5), 'tiStr': '-10.5000000', 'tiDHMS': '-10d -12:00:00.0000'},
            10: {'ti': TimeInterval(10, 0.5), 'tiStr': '11', 'tiDHMS': '10d  12:00:00'},
            11: {'ti': TimeInterval(1, -11.5), 'tiStr': '-11', 'tiDHMS': '-10d -12:00:00'}
        }

    def test_formatting(self):
        """
        Run the test for formatting.

        :return: The list returned has a length of >1 if there are test failures.
        """

        def do_check(test_n, observed):
            expected = self.test_data[test_n]
            if observed != expected:
                non_equiv = list()
                dict_diff = DictDiffer(observed, expected)
                if bool(dict_diff.added()) or bool(dict_diff.removed()):
                    non_equiv.append(' - The keys do not match or the dicts are not the same size.')
                else:
                    # Find the non-equivalent values.
                    for k in dict_diff.changed():
                        non_equiv.append(f'    - For: {k:s}, expected {expected[k]:s}, got {observed[k]:s}')
                        #                 print('Added: ', dict_diff.Added())
                        #                 print('Removed: ', dict_diff.Removed())
                        #                 print('Changed: ', dict_diff.Changed())
                        #                 print('Unchanged: ', dict_diff.Unchanged())
                return f'   Test  {test_n:2d}\n' + '\n'.join(non_equiv)

            return None

        failures = list()
        all_passed = True

        fmt = DateTimeFormat()
        fmt.day_name = DayName.full_name
        fmt.month_name = MonthName.full_name

        fmt.calendar = CalendarType.julian
        jdt = JulianDateTime(1900, 12, 31, 15, 12, 11.123, fmt.calendar)
        dates = self.formatter.get_formatted_dates(jdt, fmt)
        test_num = 0
        fail = do_check(test_num, dates)
        if fail:
            failures.append(fail)
            all_passed = False

        fmt.calendar = CalendarType.gregorian
        dates = self.formatter.get_formatted_dates(jdt, fmt)
        test_num += 1
        fail = do_check(test_num, dates)
        if fail:
            failures.append(fail)
            all_passed = False

        fmt.calendar = CalendarType.julian_gregorian
        dates = self.formatter.get_formatted_dates(jdt, fmt)
        test_num += 1
        fail = do_check(test_num, dates)
        if fail:
            failures.append(fail)
            all_passed = False

        fmt.month_name = MonthName.abbreviated
        dates = self.formatter.get_formatted_dates(jdt, fmt)
        test_num += 1
        fail = do_check(test_num, dates)
        if fail:
            failures.append(fail)
            all_passed = False

        fmt.month_name = MonthName.no_name
        dates = self.formatter.get_formatted_dates(jdt, fmt)
        test_num += 1
        fail = do_check(test_num, dates)
        if fail:
            failures.append(fail)
            all_passed = False

        fmt.month_name = MonthName.no_name
        fmt.day_name = DayName.abbreviated
        dates = self.formatter.get_formatted_dates(jdt, fmt)
        test_num += 1
        fail = do_check(test_num, dates)
        if fail:
            failures.append(fail)
            all_passed = False

        fmt.day_name = DayName.no_name
        dates = self.formatter.get_formatted_dates(jdt, fmt)
        test_num += 1
        fail = do_check(test_num, dates)
        if fail:
            failures.append(fail)
            all_passed = False

        fmt.first_dow = FirstDOW.monday
        dates = self.formatter.get_formatted_dates(jdt, fmt)
        test_num += 1
        fail = do_check(test_num, dates)
        if fail:
            failures.append(fail)
            all_passed = False

        fmt.day_name = DayName.full_name
        fmt.month_name = MonthName.full_name
        fmt.first_dow = FirstDOW.sunday
        jdt = JulianDateTime(1500, 12, 31, 15, 12, 11.123, fmt.calendar)
        dates = self.formatter.get_formatted_dates(jdt, fmt)
        test_num += 1
        fail = do_check(test_num, dates)
        if fail:
            failures.append(fail)
            all_passed = False

        res = ['  Testing parsing a decimal string']
        if not all_passed:
            res.extend(failures)
        return res

    def test_format_ymdhms(self):
        """
        Run the test for formatting as:  ymd hms.

        :return: The list returned has a length of >1 if there are test failures.
        """

        fmt = DateTimeFormat()
        fmt.calendar = CalendarType.julian_gregorian
        fmt.precision_seconds = 4

        failures = list()
        all_passed = True

        for k, v in self.dt_test_data.items():
            if k < 4:
                self.formatter.maximum_precision = 4
                s = self.formatter.format_as_ymdhms(v['jdt'], fmt)
            else:
                self.formatter.maximum_precision = 0
                s = self.formatter.format_as_ymdhms(v['jdt'], fmt)
            pf = self.compare(s, v['strDate'], '')
            if not pf[0]:
                failures.append(f'   Test {k:2d} (prec max) {pf[1]}')
                all_passed = False

        self.formatter.maximum_precision = self.max_precision
        for k, v in self.dt_test_data.items():
            if k < 4:
                fmt.precision_seconds = 4
                s = self.formatter.format_as_ymdhms(v['jdt'], fmt)
            else:
                fmt.precision_seconds = 0
                s = self.formatter.format_as_ymdhms(v['jdt'], fmt)
            pf = self.compare(s, v['strDate'], '')
            if not pf[0]:
                failures.append(f'   Test {k:2d} (prec sec) {pf[1]}')
                all_passed = False

        idx = len(self.dt_test_data)

        test_9 = '2013-Sep-07 00:00:00.0000'
        test_10 = '2013-September-07 00:00:00.0000'

        self.formatter.maximum_precision = 4
        fmt.precision_seconds = 9
        fmt.month_name = MonthName.abbreviated
        s = self.formatter.format_as_ymdhms(self.dt_test_data[2]['jdt'], fmt)
        pf = self.compare(s, test_9, '')
        if not pf[0]:
            failures.append(f'   Test {idx:2d} {pf[1]}')
            all_passed = False
        idx += 1

        fmt.month_name = MonthName.full_name
        s = self.formatter.format_as_ymdhms(self.dt_test_data[2]['jdt'], fmt)
        pf = self.compare(s, test_10, '')
        if not pf[0]:
            failures.append(f'   Test {idx:2d} {pf[1]}')
            all_passed = False
        idx += 1

        fmt.month_name = MonthName.no_name
        fmt.maximumPrecision = self.max_precision

        res = ['  Testing format_as_ymdhms()']
        if not all_passed:
            res.extend(failures)
        return res

    def test_format_jd_long(self):
        """
        Run the test for formatting as jd_long.

        :return: The list returned has a length of >1 if there are test failures.
        """

        fmt = DateTimeFormat()
        fmt.precision_fod = 12

        failures = list()
        all_passed = True

        for k, v in self.dt_test_data.items():
            if k < 4:
                self.formatter.maximum_precision = 7
                s = self.formatter.format_jd_long(v['jdt'].get_jd_full_precision(), fmt)
            else:
                self.formatter.maximum_precision = 0
                s = self.formatter.format_jd_long(v['jdt'].get_jd_full_precision(), fmt)
            pf = self.compare(s, v['jdLongDateAlt'], 'format_jd_long() (prec max)')
            if not pf[0]:
                failures.append(f'   Test {k:2d} {pf[1]}')
                all_passed = False

        self.formatter.maximum_precision = self.max_precision
        for k, v in self.dt_test_data.items():
            if k < 4:
                fmt.precision_fod = 7
                s = self.formatter.format_jd_long(v['jdt'].get_jd_full_precision(), fmt)
            else:
                fmt.precision_fod = 0
                s = self.formatter.format_jd_long(v['jdt'].get_jd_full_precision(), fmt)
            pf = self.compare(s, v['jdLongDateAlt'], 'format_jd_long() (prec fod)')
            if not pf[0]:
                failures.append(f'   Test {k:2d} {pf[1]}')
                all_passed = False

        self.formatter.maximum_precision = 7
        fmt.precision_fod = 12
        # Look at the negative dates in more detail.
        for k, v in self.dt_test_data_negative.items():
            s = self.formatter.format_jd_long(v['jdt'].get_jd_full_precision(), fmt)
            pf = self.compare(s, v['jdLongDateAlt'], 'format_jd_long() (negative)')
            if not pf[0]:
                failures.append(f'   Test {k:2d} {pf[1]}')
                all_passed = False
        fmt.maximumPrecision = self.max_precision

        res = ['  Testing format_jd_long()']
        if not all_passed:
            res.extend(failures)
        return res

    def test_format_time_interval(self):
        """
        Run the test for formatting as a time interval: d.dddd.

        :return: The list returned has a length of >1 if there are test failures.
        """

        fmt = DateTimeFormat()
        fmt.precision_fod = 12

        failures = list()
        all_passed = True

        for k, v in self.ti.items():
            if k < 10:
                self.formatter.maximum_precision = 7
                s = self.formatter.format_time_interval(v['ti'], fmt)
            else:
                self.formatter.maximum_precision = 0
                s = self.formatter.format_time_interval(v['ti'], fmt)
            pf = self.compare(s, v['tiStr'], 'format_time_interval() (max prec)')
            if not pf[0]:
                failures.append(f'   Test {k:2d} {pf[1]}')
                all_passed = False

        self.formatter.maximum_precision = 12
        for k, v in self.ti.items():
            if k < 10:
                fmt.precision_fod = 7
                s = self.formatter.format_time_interval(v['ti'], fmt)
            else:
                fmt.precision_fod = 0
                s = self.formatter.format_time_interval(v['ti'], fmt)
            pf = self.compare(s, v['tiStr'], 'format_time_interval() (prec fod)')
            if not pf[0]:
                failures.append(f'   Test {k:2d} {pf[1]}')
                all_passed = False
        fmt.maximumPrecision = self.max_precision

        res = ['  Testing format_time_interval()']
        if not all_passed:
            res.extend(failures)
        return res

    def test_format_time_interval_dhms(self):
        """
        Run the test for formatting as a time interval: d hms.dddd.

        :return: The list returned has a length of >1 if there are test failures.
        """
        fmt = DateTimeFormat()
        fmt.precision_fod = 12

        failures = list()
        all_passed = True

        fmt.precision_seconds = 12
        for k, v in self.ti.items():
            if k < 10:
                self.formatter.maximum_precision = 4
                s = self.formatter.format_time_interval_dhms(v['ti'], fmt)
            else:
                self.formatter.maximum_precision = 0
                s = self.formatter.format_time_interval_dhms(v['ti'], fmt)
            pf = self.compare(s, v['tiDHMS'], 'format_time_interval_dhms() (max prec)')
            if not pf[0]:
                failures.append(f'   Test {k:2d} {pf[1]}')
                all_passed = False

        self.formatter.maximum_precision = 12
        for k, v in self.ti.items():
            if k < 10:
                fmt.precision_seconds = 4
                s = self.formatter.format_time_interval_dhms(v['ti'], fmt)
            else:
                fmt.precision_seconds = 0
                s = self.formatter.format_time_interval_dhms(v['ti'], fmt)
            pf = self.compare(s, v['tiDHMS'], 'format_time_interval_dhms() (prec sec)')
            if not pf[0]:
                failures.append(f'   Test {k:2d} {pf[1]}')
                all_passed = False
        fmt.maximumPrecision = self.max_precision

        res = ['  Testing format_time_interval_dhms()']
        if not all_passed:
            res.extend(failures)
        return res

    def test_format_doy(self):
        """
        Run the test for formatting as day of the year.

        :return: The list returned has a length of >1 if there are test failures.
        """

        fmt = DateTimeFormat()
        fmt.calendar = CalendarType.julian_gregorian

        failures = list()
        all_passed = True

        fmt.precision_seconds = 12

        self.formatter.maximum_precision = 8
        for k, v in self.dt_test_data.items():
            if k < 4:
                fmt.precision_fod = 12
                s = self.formatter.format_doy(v['jdt'], fmt)
            else:
                fmt.precision_fod = 0
                s = self.formatter.format_doy(v['jdt'], fmt)
            pf = self.compare(s, v['doyStr'], 'format_doy() (prec fod)')
            if not pf[0]:
                failures.append(f'   Test {k:2d} {pf[1]}')
                all_passed = False

        fmt.maximumPrecision = self.max_precision
        for k, v in self.dt_test_data.items():
            if k < 4:
                fmt.precision_fod = 8
                s = self.formatter.format_doy(v['jdt'], fmt)
            else:
                fmt.precision_fod = 0
                s = self.formatter.format_doy(v['jdt'], fmt)
            pf = self.compare(s, v['doyStr'], 'format_doy() (max prec)')
            if not pf[0]:
                failures.append(f'   Test {k:2d} {pf[1]}')
                all_passed = False

        # Look at the negative dates in more detail.
        fmt.maximumPrecision = 8
        fmt.precision_fod = 12
        for k, v in self.dt_test_data_negative.items():
            s = self.formatter.format_doy(v['jdt'], fmt)
            pf = self.compare(s, v['doyStr'], 'format_doy() (negative)')
            if not pf[0]:
                failures.append(f'   Test {k:2d} {pf[1]}')
                all_passed = False
        fmt.maximumPrecision = self.max_precision

        res = ['  Testing format_doy()']
        if not all_passed:
            res.extend(failures)
        return res

    def test_format_iso_week(self):
        """
        Run the test for formatting as TimeInterval.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0
        datetime = JulianDateTime(2008, 1, 1, 0, 0, 0, CalendarType.julian_gregorian)
        expected = '2008-01-2'  # Year-week-day of the week.
        s = self.formatter.format_iso_week(datetime)
        pf = self.compare(s, expected, 'format_iso_week()')
        if not pf[0]:
            failures.append(f'   Test {idx:2d} {pf[1]}')
            all_passed = False

        res = ['  Testing format_iso_week()']
        if not all_passed:
            res.extend(failures)
        return res

    def test_other_formatting(self):
        """
        Run the test for all the other formatting options.

        :return: The list returned has a length of >1 if there are test failures.
        """

        fmt = DateTimeFormat()
        fmt.calendar = CalendarType.julian_gregorian

        failures = list()
        all_passed = True

        idx = 0
        jd = JulianDateTime(2012, 1, 3, 9, 46, 12.3215, fmt.calendar)
        self.formatter.maximum_precision = 14
        s = self.formatter.format_as_ymdhms(jd, fmt)
        expected = '2012-01-03 09:46:12.322'
        pf = self.compare(s, expected, 'format_as_ymdhms()')
        if not pf[0]:
            failures.append(f'   Test {idx:2d} {pf[1]}')
            all_passed = False
        idx += 1

        fmt.day_name = DayName.full_name
        fmt.month_name = MonthName.full_name
        fmt.precision_seconds = 3
        s = self.formatter.format_date_with_names(jd, fmt)
        expected = 'Tuesday, 03 January 2012 09:46:12.322'
        pf = self.compare(s, expected, 'format_date_with_names()')
        if not pf[0]:
            failures.append(f'   Test {idx:2d} {pf[1]}')
            all_passed = False
        idx += 1

        fmt.precision_seconds = 0
        s = self.formatter.format_date_with_names(jd, fmt)
        expected = 'Tuesday, 03 January 2012 09:46:12'
        pf = self.compare(s, expected, 'format_date_with_names()')
        if not pf[0]:
            failures.append(f'   Test {idx:2d} {pf[1]}')
            all_passed = False
        idx += 1

        fmt.day_name = DayName.abbreviated
        fmt.month_name = MonthName.abbreviated
        fmt.precision_seconds = 0
        s = self.formatter.format_date_with_names(jd, fmt)
        expected = 'Tue, 03 Jan 2012 09:46:12'
        pf = self.compare(s, expected, 'format_date_with_names()')
        if not pf[0]:
            failures.append(f'   Test {idx:2d} {pf[1]}')
            all_passed = False
        idx += 1

        fmt.day_name = DayName.no_name
        fmt.month_name = MonthName.no_name
        fmt.precision_seconds = 3

        s = self.formatter.format_as_ymd(jd, fmt)
        expected = '2012-01-03'
        pf = self.compare(s, expected, 'format_as_ymd()')
        if not pf[0]:
            failures.append(f'   Test {idx:2d} {pf[1]}')
            all_passed = False
        idx += 1

        s = self.formatter.format_as_hms(jd, fmt)
        expected = '09:46:12.322'
        pf = self.compare(s, expected, 'format_as_hms()')
        if not pf[0]:
            failures.append(f'   Test {idx:2d} {pf[1]}')
            all_passed = False
        idx += 1

        fmt.precision_fod = 9
        s = self.formatter.format_jd_short(jd, fmt)
        expected = '2455929.907087055'
        pf = self.compare(s, expected, 'format_jd_short()')
        if not pf[0]:
            failures.append(f'   Test {idx:2d} {pf[1]}')
            all_passed = False
        idx += 1

        tz = TimeZones()
        tz_diff = tz.get_time_zone_value('UT-09:30')
        tz_hm = HM(
            int(tz_diff / 3600.0),
            (tz_diff - int(tz_diff / 3600.0) * 3600.0) / 60.0)
        s = self.formatter.format_tz_as_hm(tz_hm)
        expected = '-0930'
        pf = self.compare(s, expected, 'format_tz_as_hm()')
        if not pf[0]:
            failures.append(f'   Test {idx:2d} {pf[1]}')
            all_passed = False
        idx += 1

        tz_diff = tz.get_time_zone_value('UT+12:45')
        tz_hm = HM(
            int(tz_diff / 3600.0),
            (tz_diff - int(tz_diff / 3600.0) * 3600.0) / 60.0)
        s = self.formatter.format_tz_as_hm(tz_hm)
        expected = '+1245'
        pf = self.compare(s, expected, 'format_tz_as_hm()')
        if not pf[0]:
            failures.append(f'   Test {idx:2d} {pf[1]}')
            all_passed = False

        res = ['  Testing other formatting functions']
        if not all_passed:
            res.extend(failures)
        return res

    def test_all(self):
        """
        Run all the tests.

        The list returned has a length of >2 if there are test failures.

        :return: A list.
        """

        messages = ['DateTimeFormatter Tests', ' All tests passed']
        res = self.test_formatting()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_format_ymdhms()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_format_jd_long()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_format_time_interval()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_format_time_interval_dhms()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_format_doy()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_format_iso_week()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_other_formatting()
        if len(res) > 1:
            messages.extend(res)

        if len(messages) > 2:
            messages[1] = ' Some tests failed:'
        return messages


def main():
    res = DateTimeFormatterTests().test_all()
    print('\n'.join(res))


if __name__ == '__main__':
    main()
