#!/usr/bin/env python3

"""
    Purpose: Generate variable declarations for Python, C++ and Java.

"""

from dt import DateTimeFormat, DateTimeFormatter, JulianDateTime
from dt.Constants import CalendarType, DayName, FirstDOW, MonthName


def format_dates_python(dates):
    s = '{'
    fmt2 = '"{:s}": "{:s}",\n'
    for k, v in dates.items():
        s += fmt2.format(k, v)
    s = s[:len(s) - 2]
    s += '}'
    return s


def format_python_list(lst, var_name):
    fmt = '{:s} = ('
    s = fmt.format(var_name)
    for l in lst:
        s += l + ',\n\n'
    s = s[:len(s) - 2]
    s += ")\n"
    return s


def format_dates_cpp(dates):
    s = '{'
    fmt2 = '{{"{:s}", "{:s}"}},\n'
    for k, v in dates.items():
        s += fmt2.format(k, v)
    s = s[:len(s) - 2]
    s += "}"
    return s


def format_cpp_list(lst, var_name):
    fmt = 'std::vector<std::map<std::string, std::string> > {:s} = {{\n'
    s = fmt.format(var_name)
    for l in lst:
        s += l + ',\n\n'
    s = s[:len(s) - 2]
    s += "\n};\n"
    return s


def format_dates_java(dates):
    s = 'new HashMap<String, String>() {{\n'
    fmt2 = 'put("{:s}", "{:s}");\n'
    for k, v in dates.items():
        s += fmt2.format(k, v)
    s += "}};\n"
    return s


def format_java_list(lst, var_name):
    fmt = 'private ArrayList<HashMap<String, String>> {:s} = new ArrayList<HashMap<String, String>>(Arrays.asList(\n'
    s = fmt.format(var_name)
    for l in lst:
        s += l[:len(l) - 2] + ',\n\n'
    s = s[:len(s) - 3]
    s += "\n));\n"
    return s


# def format_dates_java(dates):
#     s = dates["NamedDate"] + '\n'
#     s += dates["Date"] + '\n'
#     return s

def main():
    df = DateTimeFormatter()
    fmt = DateTimeFormat()
    fmt.day_name = DayName.full_name
    fmt.month_name = MonthName.full_name

    python_decl = list()
    cpp_decl = list()
    java_decl = list()

    fmt.calendar = CalendarType.julian
    jdt = JulianDateTime(1900, 12, 31, 15, 12, 11.123, fmt.calendar)
    dates = df.get_formatted_dates(jdt, fmt)
    python_decl.append(format_dates_python(dates))
    cpp_decl.append(format_dates_cpp(dates))
    java_decl.append(format_dates_java(dates))

    fmt.calendar = CalendarType.gregorian
    dates = df.get_formatted_dates(jdt, fmt)
    python_decl.append(format_dates_python(dates))
    cpp_decl.append(format_dates_cpp(dates))
    java_decl.append(format_dates_java(dates))

    fmt.calendar = CalendarType.julian_gregorian
    dates = df.get_formatted_dates(jdt, fmt)
    python_decl.append(format_dates_python(dates))
    cpp_decl.append(format_dates_cpp(dates))
    java_decl.append(format_dates_java(dates))

    fmt.month_name = MonthName.abbreviated
    dates = df.get_formatted_dates(jdt, fmt)
    python_decl.append(format_dates_python(dates))
    cpp_decl.append(format_dates_cpp(dates))
    java_decl.append(format_dates_java(dates))

    fmt.month_name = MonthName.no_name
    dates = df.get_formatted_dates(jdt, fmt)
    python_decl.append(format_dates_python(dates))
    cpp_decl.append(format_dates_cpp(dates))
    java_decl.append(format_dates_java(dates))

    fmt.day_name = DayName.abbreviated
    dates = df.get_formatted_dates(jdt, fmt)
    python_decl.append(format_dates_python(dates))
    cpp_decl.append(format_dates_cpp(dates))
    java_decl.append(format_dates_java(dates))

    fmt.day_name = DayName.no_name
    dates = df.get_formatted_dates(jdt, fmt)
    python_decl.append(format_dates_python(dates))
    cpp_decl.append(format_dates_cpp(dates))
    java_decl.append(format_dates_java(dates))

    fmt.first_dow = FirstDOW.monday
    dates = df.get_formatted_dates(jdt, fmt)
    python_decl.append(format_dates_python(dates))
    cpp_decl.append(format_dates_cpp(dates))
    java_decl.append(format_dates_java(dates))

    fmt.day_name = DayName.full_name
    fmt.month_name = MonthName.full_name
    fmt.first_dow = FirstDOW.sunday

    fmt.first_dow = FirstDOW.sunday
    jdt = JulianDateTime(1500, 12, 31, 15, 12, 11.123, fmt.calendar)
    dates = df.get_formatted_dates(jdt, fmt)
    python_decl.append(format_dates_python(dates))
    cpp_decl.append(format_dates_cpp(dates))
    java_decl.append(format_dates_java(dates))

    var_name = 'testData'
    print('\nPython')
    print('------\n')
    print(format_python_list(python_decl, var_name))
    print('\nC++')
    print('---\n')
    print(format_cpp_list(cpp_decl, var_name))
    print('\nJava')
    print('----\n')
    print(format_java_list(java_decl, var_name))


if __name__ == '__main__':
    main()
