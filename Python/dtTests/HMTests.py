#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   HMTests.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


from dt.HM import HM
from dtTests.CommonTests import CommonTests


class HMTests(CommonTests):
    """
    This class is a test harness to test the HM class.

    It also provides examples of how to use the HM class.

    :author: Andrew J. P. Maclean
    """

    def __init__(self):
        self.hour = 23
        self.minute = 59.999
        self.hm_list = [self.hour, self.minute]
        self.value_str = ' 23:59.999000000000'

    def test_constructors(self):
        """
        Run the tests for the constructors.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        a = HM()
        pf = self.pass_fail(a.hour == 0 and a.minute == 0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} HM() {pf[1]}')
            all_passed = False
        idx += 1

        b = HM(self.hour, self.minute)
        pf = self.pass_fail(b.hour == self.hour and b.minute == self.minute)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} HM(h, m) {pf[1]}')
            all_passed = False
        idx += 1

        b = HM(hour=self.hour, minute=self.minute)
        pf = self.pass_fail(b.hour == self.hour and b.minute == self.minute)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} HM(kwargs) {pf[1]}')
            all_passed = False
        idx += 1

        b = HM(self.hm_list)
        pf = self.pass_fail(b.hour == self.hour and b.minute == self.minute)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} HM(list) {pf[1]}')
            all_passed = False

        res = ['  Testing constructors']
        if not all_passed:
            res.extend(failures)
        return res

    def test_set(self):
        """
        Run the tests for setting members.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        a = HM()
        a.set_hm(self.hour, self.minute)
        pf = self.pass_fail(a.hour == self.hour and a.minute == self.minute)
        a.set_hm(0, 0, 0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_hm(h, m, s) {pf[1]}')
            all_passed = False
        idx += 1

        a.set_hm(hour=self.hour, minute=self.minute)
        pf = self.pass_fail(a.hour == self.hour and a.minute == self.minute)
        a.set_hm(0, 0, 0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_hm(kwargs) {pf[1]}')
            all_passed = False
        idx += 1

        a.set_hm(self.hm_list)
        pf = self.pass_fail(a.hour == self.hour and a.minute == self.minute)
        a.set_hm(0, 0, 0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_hm(list) {pf[1]}')
            all_passed = False

        res = ['  Testing setting members']
        if not all_passed:
            res.extend(failures)
        return res

    def test_comparison_operators(self):
        """
        Run the tests for the comparison operators.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        h1 = HM(self.hm_list)
        h2 = HM(self.hm_list)
        pf = self.pass_fail(h1 == h2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≡ {pf[1]}')
            all_passed = False
        idx += 1

        h2 = HM(self.hour, self.minute - 0.001)
        pf = self.pass_fail(h1.is_close(h2, 0.001))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≈ (rel_tol = 0.001) {pf[1]}')
            all_passed = False
        idx += 1

        pf = self.pass_fail(not h1.is_close(h2, 0.00001))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≉ (rel_tol = 0.00001) {pf[1]}')
            all_passed = False
        idx += 1

        h2 = HM([23, 59.99])
        pf = self.pass_fail(h1 != h2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≠ {pf[1]}')
            all_passed = False
        idx += 1

        a = HM(13, 7.2)
        b = HM(14, 7.2)
        c = HM(13, 8.2)

        pf = self.pass_fail(a < b and a < c
                            and a <= b and c >= a >= a
                            and not (a > b) and not (a >= b))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} < and ≤ {pf[1]}')
            all_passed = False
        idx += 1

        e = HM(13, 7.2)
        f = HM(12, 7.2)
        g = HM(13, 6.2)

        pf = self.pass_fail(e > f and e > g
                            and e >= f and g <= e <= e
                            and not (e < f) and not (e <= f))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} > and ≥ {pf[1]}')
            all_passed = False

        res = ['  Testing comparison operators']
        if not all_passed:
            res.extend(failures)
        return res

    def test_assignment(self):
        """
        Run the tests for assignment.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        h1 = HM(self.hm_list)
        h2 = h1
        pf = self.pass_fail(h1 == h2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} = {pf[1]}')
            all_passed = False

        res = ['  Testing assignment']
        if not all_passed:
            res.extend(failures)
        return res

    def test_all(self):
        """
        Run all the tests.

        :return: A list with a length of >2 if there are test failures.
        """
        messages = ['HM Tests', ' All tests passed']
        res = self.test_constructors()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_set()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_comparison_operators()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_assignment()
        if len(res) > 1:
            messages.extend(res)

        if len(messages) > 2:
            messages[1] = ' Some tests failed:'
        return messages


def main():
    res = HMTests().test_all()
    print('\n'.join(res))


if __name__ == '__main__':
    main()
