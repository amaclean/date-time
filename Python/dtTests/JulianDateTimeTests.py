#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   JulianDateTimeTests.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


from math import isclose, ceil

from dt import DateTimeBase, HMS, ISOWeekDate, JD, JulianDateTime, TimeZones, YMD, YMDHMS
from dt.Constants import CalendarType, FirstDOW
from dtTests.CommonTests import CommonTests


class JulianDateTimeTests(CommonTests, DateTimeBase):
    """
    This class is a test harness to test the date and time classes.

    It also provides examples of how to use the JulianDateTime class.

    :author: Andrew J. P. Maclean
    """

    def __init__(self):
        """
        """
        self.julian_date_time = JulianDateTime()

        self.year = 2012
        self.month = 1
        self.day = 1
        self.hour = 23
        self.minute = 45
        self.second = 45.0
        self.ymd_list = [self.year, self.month, self.day]
        self.hms_list = [self.hour, self.minute, self.second]
        self.ymdhms_list = self.ymd_list + self.hms_list
        self.value_str = '2012-01-01 23:45:45'

    def test_cd_to_jdj(self, jd, date):
        """
        Test the conversion of calendar date to julian day.
        The Julian Proleptic Calendar is used.

        :param: jd The julian day.
        :param: date The date.
        :return: [False|True, message]
        """
        ymd = date.get_date(CalendarType.julian)
        tjd = date.cd_to_jd_j(ymd)
        # Note: We need to round up because the calendrical calculation returns the JD at midday.
        ref = int(ceil(jd))
        return self.compare(tjd, ref, 'cd2jdJ')

    def test_jd_to_cdj(self, jd, date):
        """
        Test the conversion of julian day to calendar date .
        The Julian Proleptic Calendar is used.

        :param: jd The julian day.
        :param: date The date.
        :return: [False|True, message]
        """
        # Note: We need to round up because the calendrical calculation returns the JD at midday.
        ymd1 = date.jd_to_cd_j(ceil(jd))
        ymd2 = date.get_date(CalendarType.julian)
        return self.compare(ymd1, ymd2, 'jd2cdJ')

    def test_cd_to_jdg(self, jd, date):
        """
        Test the conversion of calendar date to julian day.
        The Gregorian Proleptic Calendar is used.

        :param: jd The julian day.
        :param: date The date.
        :return: [False|True, message]
        """
        ymd = date.get_date(CalendarType.gregorian)
        tjd = date.cd_to_jd_g(ymd)
        # Note: We need to round up because the calendrical calculation returns the JD at midday.
        ref = int(ceil(jd))
        return self.compare(tjd, ref, 'cd2jdG')

    def test_jd_to_cdg(self, jd, date):
        """
        Test the conversion of calendar date to julian day and vice versa.
        The Gregorian Proleptic Calendar is used.

        :param: jd The julian day.
        :param: date The date.
        :return: [False|True, message]
        """
        # Note: We need to round up because the calendrical calculation returns the JD at midday.
        ymd1 = date.jd_to_cd_g(ceil(jd))
        ymd2 = date.get_date(CalendarType.gregorian)
        return self.compare(ymd1, ymd2, 'jd2cdG')

    def test_get_date_jd(self, jd, date):
        """
        Test the conversion of calendar date to julian day for a given hour of the day.

        :param: jd The julian day.
        :param: date The date.
        :return: [False|True, message]
        """
        res = [True, '']
        jdpf = date.get_jd_full_precision()
        if self.is_close(jdpf.get_jd_double(), jd, DateTimeBase.rel_fod):
            res[0] = True
            res[1] = 'passed (GetJDFullPrecision())'
        else:
            res[0] = False
            res[1] = f'failed (GetJDFullPrecision()) got: {date.get_jd_double():0.6f}, expected: {jd:0.6f}'
        return res

    @staticmethod
    def test_get_date_ct(jd, date, calendar):
        """
        Test the conversion of julian day to calendar date.

        :param: jd The julian day.
        :param: date The date.
        :param: calendar The calendar to use.
        :return: [False|True, message]
        """
        res = [True, '']
        ymdhms1 = date.get_date_time(calendar)
        d = JulianDateTime()
        d.set_date_time_jd(jd)
        ymdhms2 = d.get_date_time(calendar)
        if ymdhms1.is_close(ymdhms2, DateTimeBase.rel_seconds):
            res[0] = True
            res[1] = 'passed (GetDateTime(ymdhms))'
        else:
            res[0] = False
            res[1] = f'failed (GetDateTime(ymdhms)) got: {str(ymdhms1):s}, expected: {str(ymdhms2):s}'
        return res

    def test_hms_to_hr(self, hr, time):
        """
        Test the conversion of hours minutes and seconds to hours.

        :param: hr The expected hour of the day.
        :param: time The time.
        :return: [False|True, message]
        """
        res = [True, '']
        t = JulianDateTime()
        hms = time.get_time_hms()
        h = time.hms_to_h(hms)
        if self.is_close(hr, h, DateTimeBase.rel_fod):
            res[0] = True
            res[1] = 'passed (hms_to_h)'
        else:
            res[0] = False
            res[1] = f'failed (hms_to_h) got: {h:0.12f}, expected: {hr:0.12f}'
        return res

    @staticmethod
    def test_hr_to_hms(hr, time):
        """
        Test the conversion of hours to hours minutes and seconds.

        :param: hr The hour of the day.
        :param: time The expected time.
        :return: [False|True, message]
        """
        res = [True, '']
        t = JulianDateTime()
        thms = t.h_to_hms(hr)
        hms = time.get_time_hms()
        if hms.is_close(thms, DateTimeBase.rel_seconds):
            res[0] = True
            res[1] = 'passed (h_to_hms)'
        else:
            res[0] = False
            res[1] = f'failed (h_to_hms) got: {str(hms):s}, expected: {str(thms):s}'
        return res

    def test_hm_to_hr(self, hr, time):
        """
        Test the conversion of hours and minutes to hours.

        :param: hr the expected hour of the day.
        :param: time The time.
        :return: [False|True, message]
        """
        res = [True, '']
        hm = time.get_time_hm()
        h = time.hm_to_h(hm)
        if self.is_close(hr, h, DateTimeBase.rel_fod):
            res[0] = True
            res[1] = 'passed (hm_to_h)'
        else:
            res[0] = False
            res[1] = f'failed (hm_to_h) got: {h:0.12f}, expected: {hr:0.12f}'
        return res

    @staticmethod
    def test_hr_to_hm(hr, time):
        """
        Test the conversion of hours to hours and minutes.

        :param: hr The hour of the day.
        :param: time The expected time.
        :return: [False|True, message]
        """
        res = [True, '']
        t = JulianDateTime()
        thm = t.h_to_hm(hr)
        hm = time.get_time_hm()
        if hm.is_close(thm, DateTimeBase.rel_seconds):
            res[0] = True
            res[1] = 'passed (h_to_hms)'
        else:
            res[0] = False
            res[1] = f'failed (h_to_hms) got: {str(hm):s}, expected: {str(thm):s}'
        return res

    @staticmethod
    def test_cd_to_woy(ymd, iwd):
        """
        Test the conversion of calendar date to ISO week of the year.
        The Gregorian Calendar is used.

        :param: ymd The year, month and day as a list.
        :param: iwd The ISO Week Date as a list.
        :return: [False|True, message]
        """
        res = [True, '']
        dt0 = JulianDateTime()
        dt0.set_date(ymd, CalendarType.gregorian)
        iwd1 = dt0.get_iso_week()
        if iwd1 != iwd:
            res[0] = False
            res[1] = f'failed (cd2woy) got: {str(iwd1):s}, expected: {str(iwd):s}'
        else:
            res[0] = True
            res[1] = 'passed (cd2woy)'
        return res

    def test_constructors(self):
        """
        Tests for the construction of the JulianDateTime object.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        dt0 = JulianDateTime()
        pf = self.pass_fail(dt0.get_jdn() == 0 and dt0.get_fod() == 0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JulianDateTime() {pf[1]}')
            all_passed = False
        idx += 1

        dt0 = JulianDateTime(1)
        pf = self.pass_fail(dt0.get_jdn() == 1 and dt0.get_fod() == 0.0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JulianDateTime(1) {pf[1]}')
            all_passed = False
        idx += 1

        dt0 = JulianDateTime(1, 0.5)
        pf = self.pass_fail(dt0.get_jdn() == 1 and dt0.get_fod() == 0.5)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JulianDateTime(1,0.5) {pf[1]}')
            all_passed = False
        idx += 1

        dt1 = JulianDateTime(2437934.966858796296)
        dt0 = JulianDateTime(1962, 9, 27, 11, 12, 16.6)
        pf = self.pass_fail(dt1.is_close(dt0, DateTimeBase.rel_seconds))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JulianDateTime(1962,9,27,11,12,16.6) {pf[1]}')
            all_passed = False
        idx += 1

        dt1 = JulianDateTime([2437934, 0.966858796296])
        dt0 = JulianDateTime([1962, 9, 27, 11, 12, 16.6])
        pf = self.pass_fail(dt1.is_close(dt0, DateTimeBase.rel_seconds))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JulianDateTime([1962,9,27,11,12,16.6]) {pf[1]}')
            all_passed = False
        idx += 1

        dt1 = JulianDateTime(2456275.4)
        dt0 = JulianDateTime(dt1)
        pf = self.pass_fail(dt1.is_close(dt0, DateTimeBase.rel_seconds))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JulianDateTime(JulianDateTime) {pf[1]}')
            all_passed = False
        idx += 1

        dt0 = JulianDateTime(jdn=25.2)
        pf = self.pass_fail(dt0.get_jdn() == 25 and self.is_close(dt0.get_fod(), 0.2, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JulianDateTime(jdn=25.2) {pf[1]}')
            all_passed = False
        idx += 1

        dt0 = JulianDateTime(fod=25.2)
        pf = self.pass_fail(dt0.get_jdn() == 25 and self.is_close(dt0.get_fod(), 0.2, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JulianDateTime(fod=25.2) {pf[1]}')
            all_passed = False
        idx += 1

        dt0 = JulianDateTime(jdn=25, fod=0.2)
        pf = self.pass_fail(dt0.get_jdn() == 25 and self.is_close(dt0.get_fod(), 0.2, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JulianDateTime(jdn=25,fod=0.2) {pf[1]}')
            all_passed = False
        idx += 1

        dt1 = JulianDateTime(2456275.4)
        dt0 = JulianDateTime(jdt=dt1)
        pf = self.pass_fail(dt1.get_jdn() and isclose(dt0.get_fod(), dt1.get_fod(), rel_tol=DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JulianDateTime(jdt=dt1) {pf[1]}')
            all_passed = False
        idx += 1

        calendar = CalendarType.julian_gregorian
        ymdhms = YMDHMS(2012, 1, 1, 23, 45, 45)
        dt1 = JulianDateTime()
        dt1.set_date_time(ymdhms, calendar)
        dt0 = JulianDateTime(ymdhms, calendar)
        pf = self.pass_fail(dt1.is_close(dt0, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JulianDateTime(ymdhms, calendar) {pf[1]}')
            all_passed = False
        idx += 1

        dt0 = JulianDateTime(ymdhms=ymdhms, calendar=calendar)
        pf = self.pass_fail(dt1.is_close(dt0, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JulianDateTime(ymdhms=ymdhms_object, calendar=calendar) {pf[1]}')
            all_passed = False
        idx += 1

        dt1 = JulianDateTime(2437934, 0.966858796296)
        dt0 = JulianDateTime(year=1962, month=9, day=27, hour=11, minute=12, second=16.6)
        pf = self.pass_fail(dt1.is_close(dt0, DateTimeBase.rel_seconds))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JulianDateTime(year=1962, month=9, day=27,'
                            f' hour=11, minute=12, second=16.6) {pf[1]}')
            all_passed = False
        idx += 1

        # Defaults to JulianDateTime(0, 0.0)
        dt0 = JulianDateTime(jdt=-99)
        pf = self.pass_fail(dt0.get_jdn() == 0 and dt0.get_fod() == 0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d}'
                            f' Object whose type is not JulianDateTime: JulianDateTime(jdt=-99) {pf[1]}')
            all_passed = False
        idx += 1

        # Defaults to JulianDateTime(0, 0.0)
        dt0 = JulianDateTime(xx=1)
        pf = self.pass_fail(dt0.get_jdn() == 0 and dt0.get_fod() == 0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} Unrecognised keyword: JulianDateTime(jdt=xx=1) {pf[1]}')
            all_passed = False

        res = ['  Testing constructors']
        if not all_passed:
            res.extend(failures)
        return res

    def test_set(self):
        """
        Run the tests for setting members.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        calendar = CalendarType.julian_gregorian

        idx = 0

        ymd = YMD(self.ymd_list)
        hms = HMS(self.hms_list)

        a = JulianDateTime()
        a.set_date_time(self.ymdhms_list, calendar)
        pf = self.pass_fail(
            ymd == a.get_date(calendar) and hms.is_close(a.get_time_hms(), DateTimeBase.rel_seconds))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_date_time([y, m, d, h, m, s]) {pf[1]}')
            all_passed = False
        idx += 1

        jdpf = a.get_jd_full_precision()
        b = JulianDateTime(jdpf)
        pf = self.pass_fail(a.is_close(b, DateTimeBase.rel_seconds))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JulianDateTime(a.get_jd_full_precision()) {pf[1]}')
            all_passed = False
        idx += 1

        b = JulianDateTime(jdpf.jdn, jdpf.fod)
        pf = self.pass_fail(a.is_close(b, DateTimeBase.rel_seconds))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JulianDateTime(jdn, fod) {pf[1]}')
            all_passed = False

        res = ['  Testing set']
        if not all_passed:
            res.extend(failures)
        return res

    def test_comparison_operators(self):
        """
        Tests for the comparison operators of the JulianDateTime object.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        ymdhms = [2012, 1, 1, 23, 45, 45]
        d1 = JulianDateTime()
        d1.set_date_time(ymdhms, CalendarType.julian_gregorian)
        d2 = JulianDateTime()
        d2.set_date_time(ymdhms, CalendarType.julian_gregorian)
        pf = self.pass_fail(d1 == d2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≡ {pf[1]}')
            all_passed = False
        idx += 1

        d3 = JulianDateTime()
        d3.set_date_time([2012, 1, 1, 23, 45, 45.1], CalendarType.julian_gregorian)
        pf = self.pass_fail(d1.is_close(d3, 1.0e-5))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≈ (rel_tol = 1.0e-5) {pf[1]}')
            all_passed = False
        idx += 1

        pf = self.pass_fail(not d1.is_close(d3, 1.0e-6))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≈ (rel_tol = 1.0e-6) {pf[1]}')
            all_passed = False
        idx += 1

        ymdhms[5] = 45.001
        d2.set_date_time(ymdhms, CalendarType.julian_gregorian)
        pf = self.pass_fail(d1 != d2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≠ {pf[1]}')
            all_passed = False
        idx += 1

        a = JulianDateTime(2456553, 0.53308796)
        b = JulianDateTime(2456554, 0.53308796)
        c = JulianDateTime(2456553, 0.53308797)

        pf = self.pass_fail(a < b and a < c
                            and a <= b and c >= a >= a
                            and not (a > b) and not (a >= b))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} < and ≤ {pf[1]}')
            all_passed = False
        idx += 1

        e = JulianDateTime(2456553, 0.53308796)
        f = JulianDateTime(2456552, 0.53308796)
        g = JulianDateTime(2456553, 0.53308795)

        pf = self.pass_fail(e > f and e > g
                            and e >= f and g <= e <= e
                            and not (e < f) and not (e <= f))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} > and ≥ {pf[1]}')
            all_passed = False

        res = ['  Testing comparison operators']
        if not all_passed:
            res.extend(failures)
        return res

    def test_assignment(self):
        """
        Run the tests for assignment.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        d1 = JulianDateTime(self.ymdhms_list)
        d2 = d1
        pf = self.pass_fail(d1 == d2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} = {pf[1]}')
            all_passed = False

        res = ['  Testing assignment']
        if not all_passed:
            res.extend(failures)
        return res

    def test_increment_and_addition(self):
        """
        Tests for the increment and addition operators of the JulianDateTime object.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        ymdhms = [2012, 1, 1, 23, 45, 45]
        d1 = JulianDateTime()
        d1.set_date_time(ymdhms, CalendarType.julian_gregorian)
        fod = 15 / 86400.0
        d2 = JulianDateTime()
        d2.set_delta_t([1, fod])  # One day and 15s
        d3 = JulianDateTime(d1)
        d3 += d2
        d4 = JulianDateTime(2012, 1, 2, 23, 46, 0, CalendarType.julian_gregorian)
        pf = self.pass_fail(d3.is_close(d4, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} d3 += d2 {pf[1]}')
            all_passed = False
        idx += 1

        # pf[1] += '* dt1 += days '
        days = 1.0 + fod
        d3 = JulianDateTime(d1)
        d3 += days
        pf = self.pass_fail(d3.is_close(d4, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} d3 += days {pf[1]}')
            all_passed = False
        idx += 1

        d3 = d1 + d2
        pf = self.pass_fail(d3.is_close(d4, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} d1 + d2 {pf[1]}')
            all_passed = False

        res = ['  Testing increment and addition']
        if not all_passed:
            res.extend(failures)
        return res

    def test_decrement_and_subtraction(self):
        """
        Tests for the decrement and subtraction operators of the JulianDateTime object.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        ymdhms = [2012, 1, 2, 23, 46, 0]
        d1 = JulianDateTime()
        d1.set_date_time(ymdhms, CalendarType.julian_gregorian)
        fod = 15 / 86400.0
        d2 = JulianDateTime()
        d2.set_delta_t([1, fod])  # One day and 15s
        d3 = JulianDateTime(d1)
        d3 -= d2
        d4 = JulianDateTime()
        ymdhms1 = [2012, 1, 1, 23, 45, 45]
        d4.set_date_time(ymdhms1, CalendarType.julian_gregorian)
        pf = self.pass_fail(d3.is_close(d4, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} d3 -= d2 {pf[1]}')
            all_passed = False
        idx += 1

        days = 1.0 + fod
        d3 = JulianDateTime(d1)
        d3 -= days
        pf = self.pass_fail(d3.is_close(d4, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} d3 -= days {pf[1]}')
            all_passed = False
        idx += 1

        d3 = d1 - d2
        pf = self.pass_fail(d3.is_close(d4, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} d1 - d2 {pf[1]}')
            all_passed = False

        res = ['  Testing decrement and subtraction']
        if not all_passed:
            res.extend(failures)
        return res

    def test_ephemeris_dates(self):
        """
        Run the tests for the begin/end dates of some ephemerides.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        ymdhms = [-13200, 8, 15, 0, 0, 0]
        date = JulianDateTime()
        date.set_date_time(ymdhms, CalendarType.julian)
        # Note: We need to round up because the calendrical calculation returns the JD at midday.
        pf = self.test_f1_f2(-3100015.50, date, self.test_cd_to_jdj, self.test_jd_to_cdj)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} DE413 Start Jd=-3100015.50, -13200-AUG-15 00:00:00 {pf[1]}')
            all_passed = False
        idx += 1

        ymdhms = [17191, 3, 1, 0, 0, 0]
        date.set_date_time(ymdhms, CalendarType.gregorian)
        # Note: We need to round up because the calendrical calculation returns the JD at midday.
        pf = self.test_f1_f2(8000002.5, date, self.test_cd_to_jdg, self.test_jd_to_cdg)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} DE413 End Jd=8000002.5, 17191-MAR-01 00:00:00 {pf[1]}')
            all_passed = False
        idx += 1

        ymdhms = [-3000, 2, 23, 0, 0, 0]
        date.set_date_time(ymdhms, CalendarType.julian)
        # Note: We need to round up because the calendrical calculation returns the JD at midday.
        pf = self.test_f1_f2(625360.5, date, self.test_cd_to_jdj, self.test_jd_to_cdj)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} DE406 Start Jd=625360.5, -3000 FEB 23 00:00:00 {pf[1]}')
            all_passed = False
        idx += 1

        ymdhms = [3000, 5, 6, 0, 0, 0]
        date.set_date_time(ymdhms, CalendarType.gregorian)
        pf = self.test_f1_f2(2816912.5, date, self.test_cd_to_jdg, self.test_jd_to_cdg)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} DE406 End Jd=2816912.5, 3000 MAY 06 00:00:00 {pf[1]}')
            all_passed = False
        idx += 1

        ymdhms = [3000, 7, 9, 0, 0, 0]
        date.set_date_time(ymdhms, CalendarType.gregorian)
        pf = self.test_f1_f2(2816976.5, date, self.test_cd_to_jdg, self.test_jd_to_cdg)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} DE406 End (UNIX.406). Jd=2816976.5, 3000 JUL 09 00:00:00 {pf[1]}')
            all_passed = False
        idx += 1

        ymdhms = [1599, 12, 9, 0, 0, 0]
        date.set_date_time(ymdhms, CalendarType.gregorian)
        pf = self.test_f1_f2(2305424.5, date, self.test_cd_to_jdg, self.test_jd_to_cdg)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} DE405 Start Jd=2816976.5, 1599 DEC 09 00:00:00 {pf[1]}')
            all_passed = False
        idx += 1

        ymdhms = [2201, 2, 20, 0, 0, 0]
        date.set_date_time(ymdhms, CalendarType.gregorian)
        pf = self.test_f1_f2(2525008.5, date, self.test_cd_to_jdg, self.test_jd_to_cdg)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} DE405 End Jd=2525008.5, 2201 FEB 20 00:00:00 {pf[1]}')
            all_passed = False
        idx += 1

        ymdhms = [1599, 12, 9, 0, 0, 0]
        date.set_date_time(ymdhms, CalendarType.gregorian)
        pf = self.test_f1_f2(2305424.5, date, self.test_cd_to_jdg, self.test_jd_to_cdg)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} DE200 Start Jd=2305424.5, 1599 DEC 09 00:00:00 {pf[1]}')
            all_passed = False
        idx += 1

        ymdhms = [2169, 5, 2, 0, 0, 0]
        date.set_date_time(ymdhms, CalendarType.gregorian)
        pf = self.test_f1_f2(2513392.5, date, self.test_cd_to_jdg, self.test_jd_to_cdg)
        if not pf[0]:
            failures.append(f'   Test {idx:2d}  DE200 End Jd=2513392.5, 2169 MAY 02 00:00:00 {pf[1]}')
            all_passed = False

        res = ['  Testing dates of ephemerides']
        if not all_passed:
            res.extend(failures)
        return res

    def test_double_precision(self, calendar):
        """
        Run the tests for the date and time of specified dates and a specified calendar.

        :param: calendar The calendar to use.
        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        date = JulianDateTime()
        if calendar == CalendarType.julian or calendar == CalendarType.julian_gregorian:
            ymdhms = [-3000, 2, 23, 0, 0, 0]
            date.set_date_time(ymdhms, calendar)
            pf = self.test_fg(625360.5, date, calendar, self.test_get_date_jd, self.test_get_date_ct)
            if not pf[0]:
                failures.append(f'   Test {idx:2d} DE406 Start Jd=625360.5, -3000 FEB 23 00:00:00 {pf[1]}')
                all_passed = False
            idx += 1

        if calendar == CalendarType.gregorian or calendar == CalendarType.julian_gregorian:
            ymdhms = [3000, 5, 6, 0, 0, 0]
            date.set_date_time(ymdhms, CalendarType.gregorian)
            pf = self.test_fg(2816912.5, date, calendar, self.test_get_date_jd, self.test_get_date_ct)
            if not pf[0]:
                failures.append(f'   Test {idx:2d} DE406 End. Jd=2816912.5, 3000 MAY 06 00:00:00 {pf[1]}')
                all_passed = False
            idx += 1

            ymdhms = [3000, 7, 9, 0, 0, 0]
            date.set_date_time(ymdhms, CalendarType.gregorian)
            pf = self.test_fg(2816976.5, date, calendar, self.test_get_date_jd, self.test_get_date_ct)
            if not pf[0]:
                failures.append(f'   Test {idx:2d} DE406 End (UNIX.406). Jd=2816976.5, 3000 JUL 09 00:00:00 {pf[1]}')
                all_passed = False
            idx += 1

            ymdhms = [1599, 12, 9, 0, 0, 0]
            date.set_date_time(ymdhms, CalendarType.gregorian)
            pf = self.test_fg(2305424.5, date, calendar, self.test_get_date_jd, self.test_get_date_ct)
            if not pf[0]:
                failures.append(f'   Test {idx:2d} DE405 Start Jd=2816976.5, 1599 DEC 09 00:00:00 {pf[1]}')
                all_passed = False
            idx += 1

            ymdhms = [2201, 2, 20, 0, 0, 0]
            date.set_date_time(ymdhms, CalendarType.gregorian)
            pf = self.test_fg(2525008.5, date, calendar, self.test_get_date_jd, self.test_get_date_ct)
            if not pf[0]:
                failures.append(f'   Test {idx:2d} DE405 End Jd=2525008.5, 2201 FEB 20 00:00:00 {pf[1]}')
                all_passed = False
            idx += 1

            ymdhms = [1599, 12, 9, 0, 0, 0]
            date.set_date_time(ymdhms, CalendarType.gregorian)
            pf = self.test_fg(2305424.5, date, calendar, self.test_get_date_jd, self.test_get_date_ct)
            if not pf[0]:
                failures.append(f'   Test {idx:2d} DE200 Start Jd=2305424.5, 1599 DEC 09 00:00:00 {pf[1]}')
                all_passed = False
            idx += 1

            ymdhms = [2169, 5, 2, 0, 0, 0]
            date.set_date_time(ymdhms, CalendarType.gregorian)
            pf = self.test_fg(2513392.5, date, calendar, self.test_get_date_jd, self.test_get_date_ct)
            if not pf[0]:
                failures.append(f'   Test {idx:2d} DE200 End Jd=2513392.5, 2169 MAY 02 00:00:00 {pf[1]}')
                all_passed = False
            idx += 1

        if calendar == CalendarType.julian:
            ct = ' - Julian Calendar'
        elif calendar == CalendarType.gregorian:
            ct = ' - Gregorian Calendar.'
        elif calendar == CalendarType.julian_gregorian:
            ct = ' - Julian/Gregorian Calendar.'
        else:
            ct = ' -  Unknown calendar type.'

        res = [f'  Testing dates of ephemerides using double precision{ct}']
        if not all_passed:
            res.extend(failures)
        return res

    def test_day_of_the_year(self):
        """
        Run the tests for the day of the year.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        ymd = [1900, 12, 31]
        jg = JulianDateTime(JulianDateTime().cd_to_jd_jg(ymd))
        g = JulianDateTime(JulianDateTime().cd_to_jd_jg(ymd))
        j = JulianDateTime(JulianDateTime().cd_to_jd_j(ymd))
        doy_j = j.doy_j(ymd)
        pf = self.compare(doy_j, 366, 'doy_j()')
        if not pf[0]:
            failures.append(f'   Test {0:2d}, failed {pf[1]}')
            all_passed = False

        # Not a leap year for the Gregorian and Julian/Gregorian Calendar.
        doy_g = g.doy_g(ymd)
        pf = self.compare(doy_g, 365, 'doy_g()')
        if not pf[0]:
            failures.append(f'   Test {1:2d}, failed {pf[1]}')
            all_passed = False

        doy_jg = jg.doy_jg(ymd)
        pf = self.compare(doy_jg, 365, 'doy_jg()')
        if not pf[0]:
            failures.append(f'   Test {2:2d}, failed {pf[1]}')
            all_passed = False

        res = ['  Testing Day of the Year']
        if not all_passed:
            res.extend(failures)
        return res

    def test_day_of_the_week(self):
        """
        Test the day of the week for some dates.

        :return: The list returned has a length of >1 if there are test failures.
        """

        # Note: For dow calculations we use the Julian Calendar for
        #       dates less that 1582-10-15 and the Gregorian Calendar
        #       for days on or after that date.

        test_data = {
            0: ['-4712 01 01 Monday (Julian)', [-4712, 1, 1]],
            1: ['-4713 11 24 Friday (Gregorian)', [-4713, 11, 24]],
            2: ['1582 10 04 Thursday', [1582, 10, 4]],
            3: ['1582 10 15 Friday (Gregorian)', [1582, 10, 15]],
            4: ['1582 10 17 Sunday (Gregorian)', [1582, 10, 17]],
        }

        failures = list()
        all_passed = True

        jdt = JulianDateTime()
        for k, v in test_data.items():
            if k == 0:
                jdt.set_date(v[1], CalendarType.julian)
                dow1 = jdt.get_dow(CalendarType.julian, FirstDOW.sunday)
                dow2 = jdt.get_dow(CalendarType.julian, FirstDOW.monday)
                if dow1 != 2 and dow2 != 1:
                    failures.append(f'   Test {k:2d}, got: {dow1:d} and {dow2:d}  expected: 2 and 1 (Monday).')
                    all_passed = False

            if k == 1:
                # The Julian Calendar is being used here.
                jdt.set_date(v[1], CalendarType.julian_gregorian)
                dow1 = jdt.get_dow(CalendarType.julian_gregorian, FirstDOW.sunday)
                dow2 = jdt.get_dow(CalendarType.julian_gregorian, FirstDOW.monday)
                if dow1 != 6 and dow2 != 5:
                    failures.append(f'   Test {k:2d}, got: {dow1:d} and {dow2:d}  expected: 6 and 5 (Friday).')
                    all_passed = False

            if k == 2:
                jdt.set_date(v[1], CalendarType.julian_gregorian)
                dow1 = jdt.get_dow(CalendarType.julian_gregorian, FirstDOW.sunday)
                dow2 = jdt.get_dow(CalendarType.julian_gregorian, FirstDOW.monday)
                if dow1 != 5 and dow2 != 4:
                    failures.append(f'   Test {k:2d}, got: {dow1:d} and {dow2:d}  expected: 5 and 4 (Thursday).')
                    all_passed = False

            if k == 3:
                jdt.set_date(v[1], CalendarType.gregorian)
                dow1 = jdt.get_dow(CalendarType.gregorian, FirstDOW.sunday)
                dow2 = jdt.get_dow(CalendarType.gregorian, FirstDOW.monday)
                if dow1 != 6 and dow2 != 5:
                    failures.append(f'   Test {k:2d}, got: {dow1:d} and {dow2:d}  expected: 6 and 5 (Friday).')
                    all_passed = False

            if k == 4:
                jdt.set_date(v[1], CalendarType.gregorian)
                dow1 = jdt.get_dow(CalendarType.gregorian, FirstDOW.sunday)
                dow2 = jdt.get_dow(CalendarType.gregorian, FirstDOW.monday)
                if dow1 != 1 and dow2 != 7:
                    failures.append(f'   Test {k:2d}, got: {dow1:d} and {dow2:d}  expected: 1 and 7 (Sunday).')
                    all_passed = False

        res = ['  Testing the day of the week']
        if not all_passed:
            res.extend(failures)
        return res

    def test_iso_week_date(self):
        """
        Run the tests for the week of the year conversions.

        :return: The list returned has a length of >1 if there are test failures.
        """

        # Note:
        #      2007-01-01 is 2007-W01-1 (both years 2007 start with the same day)
        #      2008-01-01 is 2008-W01-2 Gregorian year 2008 is a leap year,
        #        ISO year 2008 is 2 days shorter: 1 day longer at the start,  3 days shorter at the end.
        #      2009-12-31 is 2009-W53-4 ISO year 2009 has 53 weeks, extending the Gregorian year 2009,
        #        which starts and ends with Thursday, at both ends with three days.
        test_data = {
            0: ['2005-01-01 is 2004-W53-6', YMD([2005, 1, 1]), ISOWeekDate([2004, 53, 6])],
            1: ['2005-01-02 is 2004-W53-7', YMD([2005, 1, 2]), ISOWeekDate([2004, 53, 7])],
            2: ['2005-12-31 is 2005-W52-6', YMD([2005, 12, 31]), ISOWeekDate([2005, 52, 6])],
            3: ['2007-01-01 is 2007-W01-1', YMD([2007, 1, 1]), ISOWeekDate([2007, 1, 1])],
            4: ['2007-12-30 is 2007-W52-7', YMD([2007, 12, 30]), ISOWeekDate([2007, 52, 7])],
            5: ['2007-12-31 is 2008-W01-1', YMD([2007, 12, 31]), ISOWeekDate([2008, 1, 1])],
            6: ['2008-01-01 is 2008-W01-2', YMD([2008, 1, 1]), ISOWeekDate([2008, 1, 2])],
            7: ['2008-12-28 is 2008-W52-7', YMD([2008, 12, 28]), ISOWeekDate([2008, 52, 7])],
            8: ['2008-12-29 is 2009-W01-1', YMD([2008, 12, 29]), ISOWeekDate([2009, 1, 1])],
            9: ['2008-12-30 is 2009-W01-2', YMD([2008, 12, 30]), ISOWeekDate([2009, 1, 2])],
            10: ['2008-12-31 is 2009-W01-3', YMD([2008, 12, 31]), ISOWeekDate([2009, 1, 3])],
            11: ['2009-01-01 is 2009-W01-4', YMD([2009, 1, 1]), ISOWeekDate([2009, 1, 4])],
            12: ['2009-12-31 is 2009-W53-4', YMD([2009, 12, 31]), ISOWeekDate([2009, 53, 4])],
            13: ['2010-01-01 is 2009-W53-5', YMD([2010, 1, 1]), ISOWeekDate([2009, 53, 5])],
            14: ['2010-01-02 is 2009-W53-6', YMD([2010, 1, 2]), ISOWeekDate([2009, 53, 6])],
            15: ['2010-01-03 is 2009-W53-7', YMD([2010, 1, 3]), ISOWeekDate([2009, 53, 7])],
            16: ['2010-01-04 is 2010-W01-1', YMD([2010, 1, 4]), ISOWeekDate([2010, 1, 1])],
            17: ['2008-09-26 is 2008-W39-5', YMD([2008, 9, 26]), ISOWeekDate([2008, 39, 5])],
            18: ['2012-05-24 is 2012-W21-4', YMD([2012, 5, 24]), ISOWeekDate([2012, 21, 4])],
        }

        failures = list()
        all_passed = True

        for k, v in test_data.items():
            r = self.test_cd_to_woy(v[1], v[2])
            if not r[0]:
                failures.append(f'   Test {k:2d}, {v[0]}: {r[1]}')
                all_passed = False

        res = ['  Testing ISO week of the year']
        if not all_passed:
            res.extend(failures)
        return res

    def test_time(self):
        """
        Run the tests for the week of the year conversions.

        :return: The list returned has a length of >1 if there are test failures.
        """

        # Note:
        #      Negative times are mod 24 since the fraction of the day
        #       in the class JulianDateTime is always positive.
        #      The Julian Day Number will be adjusted in this class so
        #       that the sum of the jdn and fod will always be equivalent
        #       to the time expressed as the fraction of the day.
        test_times = {
            0: ['h=23.7625, 23:45:45', [23, 45, 45], 23.7625],
            1: ['h=0.2375, -23:45:45', [-23, 45, 45], 0.2375],
            # Testing precision and minutes/seconds rollover.
            2: ['h=23.7625, 22:104:105.0 == 23:45:45', [22, 104, 105], 23.7625],
            3: ['h=23.7625, 23:44:105.0 == 23:45:45', [23, 44, 105], 23.7625],
            4: ['h=23.7625, 22:105:45.0 == 23:45:45', [22, 105, 45], 23.7625],
            5: ['h=0.2375, -22:104:105.0 == -23:45:45', [-22, 104, 105], 0.2375],
            6: ['h=0.2375, -23:44:105.0 == -23:45:45', [-23, 44, 105], 0.2375],
            7: ['h=0.2375, -22:105:45.0 == -23:45:45', [-22, 105, 45], 0.2375],
            # Testing HM
            8: ['h=23.7625, 23:45.75', [23, 45.75], 23.7625],
            9: ['h=0.2375, -23:45.75', [-23, 45.75], 0.2375],
            10: ['h=00.7625, 00:45.75', [0, 45.75], 0.7625],
            11: ['h=-00.7625, -00:45.75', [0, -45.75], 23.2375],
            # Testing precision and minutes rollover.
            12: ['h=1.0125, 00:60.75 == 01:00.75', [0, 60.75], 1.0125],
            13: ['h=22.9875, -00:60.75 == -01:00.75', [0, -60.75], 22.9875],
        }

        failures = list()
        all_passed = True

        jdt = JulianDateTime()
        for k, v in test_times.items():
            if k < 8:
                jdt.set_time(v[1])
            else:
                jdt.set_time_hm(v[1])
            pf = self.test_f1_f2(v[2], jdt, self.test_hms_to_hr, self.test_hr_to_hms)
            if not pf[0]:
                failures.append(f'   Test {k:2d}, {v[0]} {pf[1]}')
                all_passed = False

        res = ['  Testing times']
        if not all_passed:
            res.extend(failures)
        return res

    def test_time_zone(self):
        """
        Run the tests for testing the time zone.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        time_zones = TimeZones().get_time_zones_by_name()
        tz = ['', '']
        alt_tz = ['', '']

        # pf[1] += '* UT0, UT+10\n'
        tz[0] = 'UT'
        alt_tz[0] = 'UT+10'
        tz[1] = time_zones[tz[0]] / 3600.0
        alt_tz[1] = time_zones[alt_tz[0]] / 3600.0

        # 0.1ns is the best we can expect.
        utcjd = JulianDateTime([2012, 1, 2, 23, 46, 0.0000000001], CalendarType.julian_gregorian)
        ltjd = JulianDateTime(utcjd)
        ltjd += alt_tz[1] / 24.0
        lt = ltjd.get_date_time(CalendarType.julian_gregorian)
        expected_lt = YMDHMS([2012, 1, 3, 9, 46, 0.0000000001])
        if not expected_lt.is_close(lt, DateTimeBase.rel_seconds, DateTimeBase.abs_seconds):
            failures.append(f'   Test {idx:2d} Fail UT+10:00 got {str(lt):s}, expected {str(expected_lt):s}')
            all_passed = False
        idx += 1

        tz[0] = 'UT'
        alt_tz[0] = 'UT-09:30'
        tz[1] = time_zones[tz[0]] / 3600.0
        alt_tz[1] = time_zones[alt_tz[0]] / 3600.0

        # 0.1ns is the best we can expect.
        utc = [2012, 1, 2, 23, 46, 0.0000000001]
        utcjd.set_date_time(utc, CalendarType.julian_gregorian)
        ltjd = JulianDateTime(utcjd)
        ltjd += alt_tz[1] / 24.0
        lt = ltjd.get_date_time(CalendarType.julian_gregorian)
        expected_lt = YMDHMS([2012, 1, 2, 14, 16, 0.0000000001])
        if not expected_lt.is_close(lt, DateTimeBase.rel_seconds, DateTimeBase.abs_seconds):
            failures.append(f'   Test {idx:2d} Fail UT-09:30 got {str(lt):s}, expected {str(expected_lt):s}')
            all_passed = False

        res = ['  Testing time zones']
        if not all_passed:
            res.extend(failures)
        return res

    def test_julian_dates(self):
        """
        Run the tests for Julian Dates.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        ref = JD(2400000, 0.5)
        jd = JulianDateTime(2400000, 0.5)
        jdfp = jd.get_jd_full_precision()
        jdshort = jd.get_jd_double()
        if not (jdfp == ref and jdshort == 2400000.5):
            failures.append(f'   Test {idx:2d} Failed expected 2400000.5, got get_jd_full_precision {jdfp},'
                            f' and get_jd_double {jdshort}')
            all_passed = False
        idx += 1

        ref = JD(2440000, 0.5)
        jd = JulianDateTime(2440000, 0.5)
        jdfp = jd.get_jd_full_precision()
        jdshort = jd.get_jd_double()
        if not (jdfp == ref and jdshort == 2440000.5):
            failures.append(f'   Test {idx:2d} Failed expected 2440000.5, got get_jd_full_precision {jdfp},'
                            f' and get_jd_double {jdshort}')
            all_passed = False
        idx += 1

        ref = JD([2415020, 0.0])
        jd = JulianDateTime(2415020, 0.0)
        jdfp = jd.get_jd_full_precision()
        jdshort = jd.get_jd_double()
        if not (jdfp == ref and jdshort == 2415020.0):
            failures.append(f'   Test {idx:2d} Failed expected 2415020.5, got get_jd_full_precision {jdfp},'
                            f' and get_jd_double {jdshort}')
            all_passed = False

        res = ['  Testing Julian Date Set/Get']
        if not all_passed:
            res.extend(failures)
        return res

    def test_all(self):
        """
        Run all the tests.

        The list returned has a length of >2 if there are test failures.

        :return: A list.
        """

        messages = ['JulianDateTime Tests', ' All tests passed']
        res = self.test_constructors()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_set()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_comparison_operators()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_assignment()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_increment_and_addition()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_decrement_and_subtraction()
        if len(res) > 1:
            messages.extend(res)

        res = self.test_ephemeris_dates()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_double_precision(CalendarType.julian)
        if len(res) > 1:
            messages.extend(res)
        res = self.test_double_precision(CalendarType.gregorian)
        if len(res) > 1:
            messages.extend(res)
        res = self.test_double_precision(CalendarType.julian_gregorian)
        if len(res) > 1:
            messages.extend(res)

        res = self.test_day_of_the_year()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_day_of_the_week()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_iso_week_date()
        if len(res) > 1:
            messages.extend(res)

        res = self.test_time()
        if len(res) > 1:
            messages.extend(res)

        res = self.test_time_zone()
        if len(res) > 1:
            messages.extend(res)

        res = self.test_julian_dates()
        if len(res) > 1:
            messages.extend(res)

        if len(messages) > 2:
            messages[1] = ' Some tests failed:'
        return messages


def main():
    res = JulianDateTimeTests().test_all()
    print('\n'.join(res))


if __name__ == '__main__':
    main()
