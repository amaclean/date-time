#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   DateConversionsTests.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


from dt import DateConversions, ISOWeekDate, YMD
from dt.Constants import CalendarType, FirstDOW, JGChangeover
from dtTests.CommonTests import CommonTests


class DateConversionsTests(CommonTests):
    """
    This class is a test harness to test the date and time classes.

    It also provides examples of how to use the JulianDateTime class.

    :author: Andrew J. P. Maclean
    """

    def __init__(self):
        self.dc = DateConversions()

    def test_cd_to_jdj(self, jd, ymd):
        """
        Test the conversion of calendar date to julian day.
        The Julian Proleptic Calendar is used.

        :param: jd The julian day.
        :param: ymd The date.
        :return: [False|True, message]
        """
        tjd = self.dc.cd_to_jd_j(ymd)
        return self.compare(tjd, jd, 'cd_to_jd_j()')

    def test_jd_to_cdj(self, jd, ymd):
        """
        Test the conversion of julian day to calendar date .
        The Julian Proleptic Calendar is used.

        :param: jd The julian day.
        :param: ymd The date.
        :return: [False|True, message]
        """
        ymd1 = self.dc.jd_to_cd_j(jd)
        return self.compare(ymd1, ymd, 'jd_to_cd_j()')

    def test_cd_to_jdg(self, jd, ymd):
        """
        Test the conversion of calendar date to julian day.
        The Gregorian Proleptic Calendar is used.

        :param: jd The julian day.
        :param: ymd The date.
        :return: [False|True, message]
        """
        tjd = self.dc.cd_to_jd_g(ymd)
        return self.compare(tjd, jd, 'cd_to_jd_g()')

    def test_jd_to_cdg(self, jd, ymd):
        """
        Test the conversion of calendar date to julian day and vice versa.
        The Gregorian Proleptic Calendar is used.

        :param: jd The julian day.
        :param: ymd The date.
        :return: [False|True, message]
        """
        ymd1 = self.dc.jd_to_cd_g(jd)
        return self.compare(ymd1, ymd, 'jd_to_cd_g()')

    def test_cd_to_jdjg(self, jd, ymd):
        """
        Test the conversion of calendar date to julian day.
        The Gregorian or Julian Calendar is used as appropriate.

        :param: jd The julian day.
        :param: ymd The date.
        :return: [False|True, message]
        """
        tjd = self.dc.cd_to_jd_jg(ymd)
        return self.compare(tjd, jd, 'cd_to_jd_jg()')

    def test_jd_to_cdjg(self, jd, ymd):
        """
        Test the conversion of calendar date to julian day and vice versa.
        The Gregorian or Julian Calendar is used as appropriate.

        :param: jd The julian day.
        :param: ymd The date.
        :return: [False|True, message]
        """
        ymd1 = self.dc.jd_to_cd_jg(jd)
        return self.compare(ymd1, ymd, 'jd_to_cd_jg()')

    def test_cd_to_woy(self, ymd, iwd):
        """
        Test the conversion of calendar date to ISO week of the year.
        The Gregorian Calendar is used.

        :param: ymd The year, month and day as a list.
        :param: iwd The ISO Week Date as a list.
        :return: [False|True, message]
        """
        iwd1 = self.dc.cd_to_woy(ymd)
        return self.compare(iwd1, iwd, 'cd_to_woy()')

    def test_proleptic_julian_calendar(self):
        """
        Run the Proleptic Julian calendar tests for some specified dates.

        :return: The list returned has a length of >1 if there are test failures.
        """

        test_data = {
            0: ['Jd=0, -4712 01 01', 0, YMD([-4712, 1, 1])],
            1: ['Jd=1721058, Jd=0, 0 01 01 (The first day of 1 B.C.)', 1721058, YMD([0, 1, 1])],
            2: ['Julian/Gregorian date changeover (last Julian day), Jd=2299160, 1582 10 04',
                JGChangeover.julian_previous_dn, YMD([1582, 10, 4])],
            3: ['Julian/Gregorian date changeover (first Gregorian day), Jd= 2299171, 1582 10 15', 2299171,
                YMD([1582, 10, 15])],
            4: ['Modified Julian Day 0.0. Jd= 2400013, 1858 11 17', 2400013, YMD([1858, 11, 17])],
            5: ['Jd=2451545, 1999 12 19', 2451545, YMD([1999, 12, 19])],
        }

        failures = list()
        all_passed = True
        for k, v in test_data.items():
            r = self.test_f1_f2(v[1], v[2], self.test_cd_to_jdj, self.test_jd_to_cdj)
            if not r[0]:
                failures.append(f'   Test {k:2d}, {v[0]}: {r[1]}')
                all_passed = False

        res = ['  Testing Proleptic Julian calendar']
        if not all_passed:
            res.extend(failures)
        return res

    def test_proleptic_gregorian_calendar(self):
        """
        Run the Proleptic Gregorian calendar for some specific dates.

        :return: The list returned has a length of >1 if there are test failures.
        """

        test_data = {
            0: ['Jd=0, -4713 11 24', 0, YMD([-4713, 11, 24])],
            1: ['Jd=1721060, Jd=0, 0 01 01 (The first day of 1 B.C.)', 1721060, YMD([0, 1, 1])],
            2: ['Julian/Gregorian date changeover (last Julian day), Jd=2299150, 1582 10 04',
                JGChangeover.gregorian_previous_dn, YMD([1582, 10, 4])],
            3: ['Julian/Gregorian date changeover (first Gregorian day), Jd= 2299161, 1582 10 15',
                JGChangeover.gregorian_start_dn, YMD([1582, 10, 15])],
            4: ['Modified Julian Day 0.0. Jd= 2400001, 1858 11 17', 2400001, YMD([1858, 11, 17])],
            5: ['Jd=2451545, 1999 12 19', 2451545, YMD([2000, 1, 1])],
        }

        failures = list()
        all_passed = True
        for k, v in test_data.items():
            r = self.test_f1_f2(v[1], v[2], self.test_cd_to_jdg, self.test_jd_to_cdg)
            if not r[0]:
                failures.append(f'   Test {k:2d}, {v[0]}: {r[1]}')
                all_passed = False

        res = ['  Testing Proleptic Gregorian calendar']
        if not all_passed:
            res.extend(failures)
        return res

    def test_julian_gregorian_calendar(self):
        """
        Run the Proleptic Julian, Julian, Gregorian Calendar for some specific dates.

        :return: The list returned has a length of >1 if there are test failures.
        """

        test_data = {
            0: ['Jd=0, -4712 01 01', 0, YMD([-4712, 1, 1])],
            1: ['Jd=1721058, Jd=0, 0 01 01 (The first day of 1 B.C.)', 1721058, YMD([0, 1, 1])],
            2: ['Julian/Gregorian date changeover (last Julian day), Jd=2299160, 1582 10 04',
                JGChangeover.julian_previous_dn, YMD([1582, 10, 4])],
            3: ['Julian/Gregorian date changeover (first Gregorian day), Jd= 2299161, 1582 10 15',
                JGChangeover.gregorian_start_dn, YMD([1582, 10, 15])],
            4: ['Modified Julian Day 0.0. Jd= 2400001, 1858 11 17', 2400001, YMD([1858, 11, 17])],
            5: ['Jd=2451545, 1999 12 19', 2451545, YMD([2000, 1, 1])],
        }

        failures = list()
        all_passed = True
        for k, v in test_data.items():
            r = self.test_f1_f2(v[1], v[2], self.test_cd_to_jdjg, self.test_jd_to_cdjg)
            if not r[0]:
                failures.append(f'   Test {k:2d}, {v[0]}: {r[1]}')
                all_passed = False

        res = ['  Testing Proleptic Julian, Julian, Gregorian calendar']
        if not all_passed:
            res.extend(failures)
        return res

    def test_leap_year(self):
        """
        Run the tests for leap years.

        :return: The list returned has a length of >1 if there are test failures.
        """

        test_data = {
            0: 1500,
            1: 1900,
            2: 2000,
            3: 2001,
        }

        failures = list()
        all_passed = True
        for k, v in test_data.items():
            ly_j = self.dc.is_leap_year(v, CalendarType.julian)
            ly_g = self.dc.is_leap_year(v, CalendarType.gregorian)
            ly_jg = self.dc.is_leap_year(v, CalendarType.julian_gregorian)

            if v == 1500:
                pf = ly_j == True and ly_g == False and ly_jg == True
                if not pf:
                    failures.append(f'   Test {k:2d}, {v:4d} is only a leap year for the Julian calendar.')
                    all_passed = False

            if v == 1900:
                pf = ly_j == True and ly_g == False and ly_jg == False
                if not pf:
                    failures.append(f'   Test {k:2d}, {v:4d} is only a leap year for the Julian calendar.')
                    all_passed = False

            if v == 2000:
                pf = ly_j == True and ly_g == True and ly_jg == True
                if not pf:
                    failures.append(f'   Test {k:2d}, {v:4d} is a leap year for the Julian and Gregorian calendars.')
                    all_passed = False

            if v == 2001:
                pf = ly_j == False and ly_g == False and ly_jg == False
                if not pf:
                    failures.append(f'   Test {k:2d}, {v:4d} is not a leap year.')
                    all_passed = False

        res = ['  Testing leap years']
        if not all_passed:
            res.extend(failures)
        return res

    def test_day_of_the_year(self):
        """
        Run the tests for the day of the year.

        :return: The list returned has a length of >1 if there are test failures.
        """

        ymd = YMD([1900, 12, 31])

        failures = list()
        all_passed = True

        doy_j = self.dc.doy_j(ymd)
        pf = self.compare(doy_j, 366, 'doy_j()')
        if not pf[0]:
            failures.append(f'   Test {0:2d}, {pf[1]}')
            all_passed = False

        # Not a leap year for the Gregorian Calendar.
        doy_g = self.dc.doy_g(ymd)
        p = self.compare(doy_g, 365, 'doy_g()')
        if not pf[0]:
            failures.append(f'   Test {1:2d}, {pf[1]}')
            all_passed = False

        doy_jg = self.dc.doy_jg(ymd)
        p = self.compare(doy_jg, 365, 'doy_jg()')
        if not pf[0]:
            failures.append(f'   Test {2:2d}, {pf[1]}')
            all_passed = False

        res = ['  Testing Day of the Year']
        if not all_passed:
            res.extend(failures)
        return res

    def test_day_of_the_week(self):
        """
        Test the day of the week for some dates.

        :return: The list returned has a length of >1 if there are test failures.
        """

        # Note: For dow calculations we use the Julian Calendar for
        #       dates less that 1582-10-15 and the Gregorian Calendar
        #       for days on or after that date.

        test_data = {
            0: ['-4712 01 01 Monday (Julian)', YMD([-4712, 1, 1])],
            1: ['-4713 11 24 Friday (Gregorian)', YMD([-4713, 11, 24])],
            2: ['1582 10 04 Thursday', YMD([1582, 10, 4])],
            3: ['1582 10 15 Friday (Gregorian)', YMD([1582, 10, 15])],
            4: ['1582 10 17 Sunday (Gregorian)', YMD([1582, 10, 17])],
        }

        failures = list()
        all_passed = True
        for k, v in test_data.items():
            if k == 0:
                dow1 = self.dc.dow_ymd(test_data[k][1], CalendarType.julian, FirstDOW.sunday)
                dow2 = self.dc.dow_ymd(test_data[k][1], CalendarType.julian, FirstDOW.monday)
                if dow1 != 2 and dow2 != 1:
                    failures.append(f'   Test {k:2d}, got: {dow1:d} and {dow2:d}  expected: 2 and 1 (Monday).')
                    all_passed = False

            if k == 1:
                # The Julian Calendar is being used here.
                dow1 = self.dc.dow_ymd(test_data[k][1], CalendarType.julian_gregorian, FirstDOW.sunday)
                dow2 = self.dc.dow_ymd(test_data[k][1], CalendarType.julian_gregorian, FirstDOW.monday)
                if dow1 != 6 and dow2 != 5:
                    failures.append(f'   Test {k:2d}, got: {dow1:d} and {dow2:d}  expected: 6 and 5 (Friday).')
                    all_passed = False

            if k == 2:
                dow1 = self.dc.dow_ymd(test_data[k][1], CalendarType.julian_gregorian, FirstDOW.sunday)
                dow2 = self.dc.dow_ymd(test_data[k][1], CalendarType.julian_gregorian, FirstDOW.monday)
                if dow1 != 5 and dow2 != 4:
                    failures.append(f'   Test {k:2d}, got: {dow1:d} and {dow2:d}  expected: 5 and 4 (Thursday).')
                    all_passed = False

            if k == 3:
                dow1 = self.dc.dow_ymd(test_data[k][1], CalendarType.gregorian, FirstDOW.sunday)
                dow2 = self.dc.dow_ymd(test_data[k][1], CalendarType.gregorian, FirstDOW.monday)
                if dow1 != 6 and dow2 != 5:
                    failures.append(f'   Test {k:2d}, got: {dow1:d} and {dow2:d}  expected: 6 and 5 (Friday).')
                    all_passed = False

            if k == 4:
                dow1 = self.dc.dow_ymd(test_data[k][1], CalendarType.gregorian, FirstDOW.sunday)
                dow2 = self.dc.dow_ymd(test_data[k][1], CalendarType.gregorian, FirstDOW.monday)
                if dow1 != 1 and dow2 != 7:
                    failures.append(f'   Test {k:2d}, got: {dow1:d} and {dow2:d}  expected: 1 and 7 (Sunday).')
                    all_passed = False

        res = ['  Testing the day of the week']
        if not all_passed:
            res.extend(failures)
        return res

    def test_iso_week_date(self):
        """
        Run the tests for the week of the year conversions.

        :return: The list returned has a length of >1 if there are test failures.
        """

        # Note:
        #      2007-01-01 is 2007-W01-1 (both years 2007 start with the same day)
        #      2008-01-01 is 2008-W01-2 Gregorian year 2008 is a leap year,
        #        ISO year 2008 is 2 days shorter: 1 day longer at the start,  3 days shorter at the end.
        #      2009-12-31 is 2009-W53-4 ISO year 2009 has 53 weeks, extending the Gregorian year 2009,
        #        which starts and ends with Thursday, at both ends with three days.
        test_data = {
            0: ['2005-01-01 is 2004-W53-6', YMD([2005, 1, 1]), ISOWeekDate([2004, 53, 6])],
            1: ['2005-01-02 is 2004-W53-7', YMD([2005, 1, 2]), ISOWeekDate([2004, 53, 7])],
            2: ['2005-12-31 is 2005-W52-6', YMD([2005, 12, 31]), ISOWeekDate([2005, 52, 6])],
            3: ['2007-01-01 is 2007-W01-1', YMD([2007, 1, 1]), ISOWeekDate([2007, 1, 1])],
            4: ['2007-12-30 is 2007-W52-7', YMD([2007, 12, 30]), ISOWeekDate([2007, 52, 7])],
            5: ['2007-12-31 is 2008-W01-1', YMD([2007, 12, 31]), ISOWeekDate([2008, 1, 1])],
            6: ['2008-01-01 is 2008-W01-2', YMD([2008, 1, 1]), ISOWeekDate([2008, 1, 2])],
            7: ['2008-12-28 is 2008-W52-7', YMD([2008, 12, 28]), ISOWeekDate([2008, 52, 7])],
            8: ['2008-12-29 is 2009-W01-1', YMD([2008, 12, 29]), ISOWeekDate([2009, 1, 1])],
            9: ['2008-12-30 is 2009-W01-2', YMD([2008, 12, 30]), ISOWeekDate([2009, 1, 2])],
            10: ['2008-12-31 is 2009-W01-3', YMD([2008, 12, 31]), ISOWeekDate([2009, 1, 3])],
            11: ['2009-01-01 is 2009-W01-4', YMD([2009, 1, 1]), ISOWeekDate([2009, 1, 4])],
            12: ['2009-12-31 is 2009-W53-4', YMD([2009, 12, 31]), ISOWeekDate([2009, 53, 4])],
            13: ['2010-01-01 is 2009-W53-5', YMD([2010, 1, 1]), ISOWeekDate([2009, 53, 5])],
            14: ['2010-01-02 is 2009-W53-6', YMD([2010, 1, 2]), ISOWeekDate([2009, 53, 6])],
            15: ['2010-01-03 is 2009-W53-7', YMD([2010, 1, 3]), ISOWeekDate([2009, 53, 7])],
            16: ['2010-01-04 is 2010-W01-1', YMD([2010, 1, 4]), ISOWeekDate([2010, 1, 1])],
            17: ['2008-09-26 is 2008-W39-5', YMD([2008, 9, 26]), ISOWeekDate([2008, 39, 5])],
            18: ['2012-05-24 is 2012-W21-4', YMD([2012, 5, 24]), ISOWeekDate([2012, 21, 4])],
        }

        failures = list()
        all_passed = True
        for k, v in test_data.items():
            r = self.test_cd_to_woy(v[1], v[2])
            if not r[0]:
                failures.append(f'   Test {k:2d}, {v[0]}: {r[1]}')
                all_passed = False

        res = ['  Testing ISO week of the year']
        if not all_passed:
            res.extend(failures)
        return res

    def test_all(self):
        """
        Run all the tests.

        The list returned has a length of >2 if there are test failures.

        :return: A list.
        """

        messages = ['DateConversions Tests', ' All tests passed']
        res = self.test_proleptic_julian_calendar()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_proleptic_gregorian_calendar()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_julian_gregorian_calendar()
        if len(res) > 1:
            messages.extend(res)

        res = self.test_leap_year()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_day_of_the_year()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_day_of_the_week()
        if len(res) > 1:
            messages.extend(res)

        res = self.test_iso_week_date()
        if len(res) > 1:
            messages.extend(res)

        if len(messages) > 2:
            messages[1] = ' Some tests failed:'
        return messages


def main():
    res = DateConversionsTests().test_all()
    print('\n'.join(res))


if __name__ == '__main__':
    main()
