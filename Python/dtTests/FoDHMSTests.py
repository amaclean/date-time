#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   FoDHMSTests.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================

from math import isclose

from dt import DateConversions, TimeConversions, YMDHMS
from dt.Constants import CalendarType
from dt.DateTimeBase import DateTimeBase
from dt.JulianDateTime import JulianDateTime
from dtTests.CommonTests import CommonTests


class FoDHMSTests(CommonTests, DateTimeBase):
    """
    This class is a test harness to test the JD class.

    It also provides examples of how to use the JD class.

    :author: Andrew J. P. Maclean
    """

    def __init__(self):
        self.tc = TimeConversions()
        self.dc = DateConversions()

        steps = 7
        self.testFoD = dict()
        for i in range(0, steps):
            jd_fod = i / float(steps)
            if jd_fod >= 0.5:
                hms_fod = jd_fod - 0.5
            else:
                hms_fod = jd_fod + 0.5
            self.testFoD[i] = {'jd_fod': jd_fod, 'hms_fod': hms_fod}

        # We manually fill these vectors with known values taken from DE431.
        DE431_dates = list()
        DE431_dates.append('-13200 08 15 -3100015.5')
        DE431_dates.append('-13100 01 01 -3063717.5')
        DE431_dates.append('-13100 01 02 -3063716.5')
        DE431_dates.append('-4713 12 31       -1.5')
        DE431_dates.append('-4712 01 01       -0.5')
        DE431_dates.append('-4712 01 02        0.5')
        DE431_dates.append('-4712 01 03        1.5')
        DE431_dates.append(' 8000 01 01  4642999.5')
        DE431_dates.append(' 8000 01 02  4643000.5')
        DE431_dates.append('17191 03 01  8000002.5')
        DE431_dates.append('17191 03 15  8000016.5')

        idx = 0
        self.testDates = dict()
        for s in DE431_dates:
            l = ' '.join(s.split())
            v = l.split(' ')
            ymdhms = YMDHMS(int(v[0]), int(v[1]), int(v[2]))
            jd = float(v[3])
            self.testDates[idx] = {'ymdhms': ymdhms, 'jd': jd}
            idx += 1

    def test_calendar_date_to_julian_date(self):
        """
        Testing Calendar Date -> (jdn, fod) -> Julian Date.

        :return: The list returned has a length of >1 if there are test failures.
        """
        calendar = CalendarType.julian_gregorian

        failures = list()
        all_passed = True

        jdp = JulianDateTime()
        for k, v in self.testDates.items():
            jdp.set_date_time(v['ymdhms'], calendar)
            if jdp.get_jd_double() != v['jd']:
                failures.append(f'   Test {k:2d} failed, for: {str(v["ymdhms"]):s},'
                                f' expected: {v["jd"]:10.2f}  got: {jdp.get_jd_double():10.2f},'
                                f' difference: {jdp.get_jd_double() - v["jd"]:15.12f}')
                all_passed = False

        res = ['  Testing Calendar Date -> (jdn, fod) -> Julian Date']
        if not all_passed:
            res.extend(failures)
        return res

    def test_julian_date_to_calendar_date(self):
        """
        Testing Julian Date -> (jdn, fod) -> Calendar Date.

        :return: The list returned has a length of >1 if there are test failures.
        """
        calendar = CalendarType.julian_gregorian

        failures = list()
        all_passed = True

        for k, v in self.testDates.items():
            jdp = JulianDateTime(0, v['jd'])
            ymdhms = jdp.get_date_time(calendar)
            if not ymdhms.is_close(v['ymdhms'], DateTimeBase.rel_seconds):
                failures.append(
                    f'   Test {k:2d} failed, for: {v["jd"]:10.1f},'
                    f' expected: {str(v["ymdhms"]):s} got: {str(ymdhms):s}')
                all_passed = False

        res = ['  Testing Julian Date -> (jdn, fod) -> Calendar Date']
        if not all_passed:
            res.extend(failures)
        return res

    def test_fod_hms_jd(self):
        """
        Testing jd -> jdn, hms -> calendar date -> jd.

        :return: The list returned has a length of >1 if there are test failures.
        """
        calendar = CalendarType.julian_gregorian

        failures = list()
        all_passed = True

        for k, v in self.testDates.items():
            jdp = JulianDateTime(0, v['jd'])
            for kk, vv in self.testFoD.items():
                jdp.set_fod(vv['jd_fod'])

                days_hms = jdp.jd.fod_to_hms(jdp.get_jdn(), jdp.get_fod())
                jdn_fod = jdp.jd.hms_to_fod(days_hms[0], days_hms[1])
                if jdn_fod[1] >= 0.5:
                    # The next day since we started at 12h of the current day.
                    jdn_fod[0] += 1

                ymdhms = YMDHMS(self.dc.jd_to_cd_jg(jdn_fod[0]), self.tc.h_to_hms(vv['hms_fod'] * 24.0))

                jdt1 = JulianDateTime()
                jdt1.set_date_time(ymdhms, calendar)
                if not jdp.is_close(jdt1, DateTimeBase.rel_fod):
                    failures.append(f'   Test {k:2d}, {kk:2d} failed for jd:'
                                    f' ({jdp.get_jdn():8d} {jdp.get_fod():5.8f})'
                                    f' got jd: ({jdt1.get_jdn():8d} {jdt1.get_fod():5.8f})')
                    all_passed = False

        res = ['  Testing jd -> jdn, hms -> calendar date -> jd']
        if not all_passed:
            res.extend(failures)
        return res

    def test_jd(self):
        """
        Testing jd -> jdn, hms -> calendar date -> jd.

        :return: The list returned has a length of >1 if there are test failures.
        """
        calendar = CalendarType.julian_gregorian

        failures = list()
        all_passed = True

        for k, v in self.testDates.items():
            for kk, vv in self.testFoD.items():
                ymdhms = YMDHMS(self.dc.jd_to_cd_jg(int(v['jd'])), self.tc.h_to_hms(vv['hms_fod'] * 24.0))
                jdt1 = JulianDateTime()
                jdt1.set_date_time(ymdhms, calendar)
                jdt2 = jdt1.get_jd_double()

                if not isclose(jdt2, jdt1.get_jdn() + jdt1.get_fod(), rel_tol=DateTimeBase.rel_fod):
                    failures.append(f'   Test {k:2d}, {kk:2d}  failed for'
                                    f' jd: ({jdt1.get_jdn():8d} {jdt1.get_fod():5.8f}) = '
                                    f' {jdt1.get_jdn() + jdt1.get_fod():12.8f} from'
                                    f' get_jd_double(): {jdt2:12.8f},'
                                    f' difference {jdt2 - (jdt1.get_jdn() + jdt1.get_fod()):15.12f}\n')
                    all_passed = False

                res = ['  Testing jd -> jdn, hms -> calendar date -> jd']
                if not all_passed:
                    res.extend(failures)
                return res

    def test_jd1(self):
        """
        Testing jd -> julian date time -> jd.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        for k, v in self.testDates.items():
            for kk, vv in self.testFoD.items():
                jd = v['jd'] + vv['jd_fod']
                jdt = JulianDateTime(0, jd)
                jd1 = jdt.get_jd_double()

                if not isclose(jd, jd1, rel_tol=DateTimeBase.rel_fod):
                    failures.append(f'   Test {k:2d}, {kk:2d}  failed for'
                                    f' jd: ({v["jd"]:12.8} {vv["jd_fod"]:5.8f}) ='
                                    f' {jd:12.8f} got: {jd1:12.8f}, difference: {jd1 - jd:15.12f}')
                    all_passed = False

        res = ['  Testing jd -> julian date time -> jd']
        if not all_passed:
            res.extend(failures)
        return res

    def test_jd2(self):
        """
        Testing jd -> calendar -> jd.

        :return: The list returned has a length of >1 if there are test failures.
        """
        calendar = CalendarType.julian_gregorian

        failures = list()
        all_passed = True

        for k, v in self.testDates.items():
            for kk, vv in self.testFoD.items():
                jd = v['jd'] + vv['jd_fod']
                jdt = JulianDateTime(0, jd)
                ymdhms = jdt.get_date_time(calendar)
                jdt1 = JulianDateTime()
                jdt1.set_date_time(ymdhms, calendar)
                jd1 = jdt.get_jd_double()

                if not isclose(jd, jd1, rel_tol=DateTimeBase.rel_fod):
                    failures.append(f'   Test {k:2d}, {kk:2d}  failed for'
                                    f' jd: ({v["jd"]:12.8f} {vv["jd_fod"]:5.8f}) ='
                                    f' {jd:12.8f} got: {jd1:12.8f}, difference: {jd1 - jd:15.12f}')
                    all_passed = False

        res = ['  Testing jd -> calendar -> jd']
        if not all_passed:
            res.extend(failures)
        return res

    def test_calendar(self):
        """
        Testing calendar -> jd -> calendar.

        :return: The list returned has a length of >1 if there are test failures.
        """
        calendar = CalendarType.julian_gregorian

        failures = list()
        all_passed = True

        for k, v in self.testDates.items():
            for kk, vv in self.testFoD.items():
                ymdhms = YMDHMS(v['ymdhms'])
                # Using fod_jd[i] as the hms times.
                # This makes times run from 0h to < 24h.
                hms = self.tc.h_to_hms(vv['jd_fod'] * 24.0)
                ymdhms.set_hms(hms)

                jdt1 = JulianDateTime()
                jdt1.set_date_time(ymdhms, calendar)
                ymdhms0 = jdt1.get_date_time(calendar)

                if not ymdhms.is_close(ymdhms0, DateTimeBase.rel_seconds):
                    failures.append(f'   Test {k:2d}, {kk:2d}  failed,'
                                    f' expected: {str(ymdhms):s} got: {str(ymdhms0):s}')
                    all_passed = False

        res = ['  Testing calendar -> jd -> calendar']
        if not all_passed:
            res.extend(failures)
        return res

    def test_all(self):
        """
        Run all the tests.

        The list returned has a length of >2 if there are test failures.

        :return: A list.
        """

        messages = ['FoDHMS Tests', ' All tests passed']
        res = self.test_calendar_date_to_julian_date()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_julian_date_to_calendar_date()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_fod_hms_jd()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_jd()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_jd1()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_jd2()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_calendar()
        if len(res) > 1:
            messages.extend(res)

        if len(messages) > 2:
            messages[1] = ' Some tests failed:'
        return messages


def main():
    res = FoDHMSTests().test_all()
    print('\n'.join(res))


if __name__ == '__main__':
    main()
