#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   TimeIntervalTests.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


from dt.DateTimeBase import DateTimeBase
from dt.TimeInterval import TimeInterval
from dtTests.CommonTests import CommonTests


class TimeIntervalTests(CommonTests, DateTimeBase):
    """
    This class is a test harness to test the TimeInterval class.

    It also provides examples of how to use the TimeInterval class.

    :author: Andrew J. P. Maclean
    """

    def __init__(self):
        self.delta_day = 15
        self.delta_fod = 0.459

    def test_constructors(self):
        """
        Run the tests for the constructors.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        j = TimeInterval()
        pf = self.pass_fail(j.get_delta_day() == 0 and j.get_delta_fod() == 0.0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} TimeInterval() {pf[1]}')
            all_passed = False
        idx += 1

        j1 = TimeInterval(self.delta_day, self.delta_fod)
        pf = self.pass_fail(j1.get_delta_day() == self.delta_day and j1.get_delta_fod() == self.delta_fod)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} TimeInterval(delta_day, delta_fod) {pf[1]}')
            all_passed = False
        idx += 1

        j2 = TimeInterval(delta_day=self.delta_day, delta_fod=self.delta_fod)
        pf = self.pass_fail(j2.get_delta_day() == self.delta_day and j2.get_delta_fod() == self.delta_fod)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} TimeInterval(kwargs) {pf[1]}')
            all_passed = False
        idx += 1

        j3 = TimeInterval([self.delta_day, self.delta_fod])
        pf = self.pass_fail(j3.get_delta_day() == self.delta_day and j3.get_delta_fod() == self.delta_fod)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} TimeInterval(list) {pf[1]}')
            all_passed = False

        res = ['  Testing constructors']
        if not all_passed:
            res.extend(failures)
        return res

    def test_set(self):
        """
        Run the tests for setting members.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        j = TimeInterval()
        j.set_time_interval(self.delta_day, self.delta_fod)
        pf = self.pass_fail(j.get_delta_day() == self.delta_day and j.get_delta_fod() == self.delta_fod)
        j.set_time_interval(0, 0.0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_time_interval(delta_day, delta_fod) {pf[1]}')
            all_passed = False
        idx += 1

        j.set_time_interval(delta_day=self.delta_day, delta_fod=self.delta_fod)
        pf = self.pass_fail(j.get_delta_day() == self.delta_day and j.get_delta_fod() == self.delta_fod)
        j.set_time_interval(0, 0.0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_time_interval(kwargs) {pf[1]}')
            all_passed = False
        idx += 1

        j.set_time_interval([self.delta_day, self.delta_fod])
        pf = self.pass_fail(j.get_delta_day() == self.delta_day and j.get_delta_fod() == self.delta_fod)
        j.set_time_interval(0, 0.0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_time_interval(list) {pf[1]}')
            all_passed = False

        res = ['  Testing setting members']
        if not all_passed:
            res.extend(failures)
        return res

    def test_comparison_operators(self):
        """
        Run the tests for the comparison operators.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        ti1 = TimeInterval([self.delta_day, self.delta_fod])
        ti2 = TimeInterval([self.delta_day, self.delta_fod])
        pf = self.pass_fail(ti1 == ti2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≡ {pf[1]}')
            all_passed = False
        idx += 1

        ti2.set_delta_fod(ti2.get_delta_fod() - 0.01)
        pf = self.pass_fail(ti1.is_close(ti2, 0.1))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≈ (rel_tol = 0.1) {pf[1]}')
            all_passed = False
        idx += 1

        #  Expect a fail here
        pf = self.pass_fail(not ti1.is_close(ti2, 0.01))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≈ (rel_tol = 0.01) {pf[1]}')
            all_passed = False
        idx += 1

        pf = self.pass_fail(ti1 != ti2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≠ {pf[1]}')
            all_passed = False
        idx += 1

        a = TimeInterval(2456553, 0.53308796)
        b = TimeInterval(2456554, 0.53308796)
        c = TimeInterval(2456553, 0.53308797)

        pf = self.pass_fail(a < b and a < c
                            and a <= b and c >= a >= a
                            and not (a > b) and not (a >= b))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} < and ≤ {pf[1]}')
            all_passed = False
        idx += 1

        e = TimeInterval(2456553, 0.53308796)
        f = TimeInterval(2456552, 0.53308796)
        g = TimeInterval(2456553, 0.53308795)

        pf = self.pass_fail(e > f and e > g
                            and e >= f and g <= e <= e
                            and not (e < f) and not (e <= f))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} > and ≥ {pf[1]}')
            all_passed = False

        res = ['  Testing comparison operators']
        if not all_passed:
            res.extend(failures)
        return res

    def test_assignment(self):
        """
        Run the tests for assignment.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        j1 = TimeInterval([self.delta_day, self.delta_fod])
        j2 = j1
        pf = self.pass_fail(j1 == j2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} = {pf[1]}')
            all_passed = False

        res = ['  Testing assignment']
        if not all_passed:
            res.extend(failures)
        return res

    def test_increment_and_addition(self):
        """
        Tests for the increment and addition operators of the JulianDateTime object.

        :return: The list returned has a length of >1 if there are test failures.
        """
        # p = self.make_header('Testing increment and addition.', 2)

        failures = list()
        all_passed = True

        idx = 0

        ti1 = TimeInterval(self.delta_day, self.delta_fod)
        ti2 = TimeInterval()
        delta_fod = 15 / 86400.0
        ti2.set_time_interval(1, delta_fod)  # One day and 15s
        ti3 = TimeInterval(ti1)
        ti3 += ti2
        ti4 = TimeInterval(self.delta_day + 1, self.delta_fod + delta_fod)
        pf = self.pass_fail(ti3.is_close(ti4, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ti1 += ti2 {pf[1]}')
            all_passed = False
        idx += 1

        days = 1 + delta_fod
        ti3 = TimeInterval(ti1)
        ti3 += days
        pf = self.pass_fail(ti3.is_close(ti4, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ti3 += days {pf[1]}')
            all_passed = False
        idx += 1

        ti3 = ti1 + ti2
        pf = self.pass_fail(ti3.is_close(ti4, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ti1 + ti2 {pf[1]}')
            all_passed = False

        res = ['  Testing increment and addition']
        if not all_passed:
            res.extend(failures)
        return res

    def test_decrement_and_subtraction(self):
        """
        Tests for the decrement and subtraction operators of the JulianDateTime object.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        ti1 = TimeInterval(self.delta_day, self.delta_fod)
        ti2 = TimeInterval()
        delta_fod = 15 / 86400.0
        ti2.set_time_interval(1, delta_fod)  # One day and 15s
        ti3 = TimeInterval(ti1)
        ti3 -= ti2
        ti4 = TimeInterval(self.delta_day - 1, self.delta_fod - delta_fod)
        pf = self.pass_fail(ti3.is_close(ti4, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ti1 -= ti2 {pf[1]}')
            all_passed = False
        idx += 1

        days = 1 + delta_fod
        ti3 = TimeInterval(ti1)
        ti3 -= days
        pf = self.pass_fail(ti3.is_close(ti4, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ti3 -= days {pf[1]}')
            all_passed = False
        idx += 1

        ti3 = ti1 - ti2
        pf = self.pass_fail(ti3.is_close(ti4, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ti1 - ti2 {pf[1]}')
            all_passed = False

        res = ['  Testing decrement and subtraction']
        if not all_passed:
            res.extend(failures)
        return res

    def test_time_interval(self):
        """
        Run the tests for making a TimeInterval object.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        a = TimeInterval()
        b = TimeInterval(a)
        pf = self.pass_fail(a == b)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} TimeInterval(ti) {pf[1]}')
            all_passed = False
        idx += 1

        a = TimeInterval(self.delta_day, self.delta_fod)
        b = TimeInterval(self.delta_day, self.delta_fod)
        pf = self.pass_fail(a == b)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} TimeInterval(delta_day, delta_fod) {pf[1]}')
            all_passed = False
        idx += 1

        a = TimeInterval(deltaDay=self.delta_day, deltaFoD=self.delta_fod)
        b = TimeInterval(deltaDay=self.delta_day, deltaFoD=self.delta_fod)
        pf = self.pass_fail(a == b)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} TimeInterval(kwargs) {pf[1]}')
            all_passed = False
        idx += 1

        a = TimeInterval([self.delta_day, self.delta_fod])
        b = TimeInterval([self.delta_day, self.delta_fod])
        pf = self.pass_fail(a == b)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} TimeInterval(list) {pf[1]}')
            all_passed = False

        res = ['  Testing TimeInterval()']
        if not all_passed:
            res.extend(failures)
        return res

    def test_all(self):
        """
        Run all the tests.

        The list returned has a length of >2 if there are test failures.

        :return: A list.
        """

        messages = ['TimeInterval Tests', ' All tests passed']

        res = self.test_constructors()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_set()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_comparison_operators()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_assignment()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_increment_and_addition()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_decrement_and_subtraction()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_time_interval()
        if len(res) > 1:
            messages.extend(res)

        if len(messages) > 2:
            messages[1] = ' Some tests failed:'
        return messages


def main():
    res = TimeIntervalTests().test_all()
    print('\n'.join(res))


if __name__ == '__main__':
    main()
