#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   YMDTests.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


from dt.YMD import YMD
from dtTests.CommonTests import CommonTests


class YMDTests(CommonTests):
    """
    This class is a test harness to test the YMD class.

    It also provides examples of how to use the YMD class.

    :author: Andrew J. P. Maclean
    """

    def __init__(self):
        """
        Constructor
        """
        self.year = 2013
        self.month = 1
        self.day = 1
        self.ymd = [self.year, self.month, self.day]

    def test_constructors(self):
        """
        Run the tests for the constructors.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        a = YMD()
        pf = self.pass_fail(a.year == -4712 and a.month == 1 and a.day == 1)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} YMD() {pf[1]}')
            all_passed = False
        idx += 1

        a = YMD(self.year, self.month, self.day)
        pf = self.pass_fail(a.year == self.year and a.month == self.month and a.day == self.day)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} YMD(y, m, d) {pf[1]}')
            all_passed = False
        idx += 1

        a = YMD(year=self.year, month=self.month, day=self.day)
        pf = self.pass_fail(a.year == self.year and a.month == self.month and a.day == self.day)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} YMD(kwargs) {pf[1]}')
            all_passed = False
        idx += 1

        a = YMD(self.ymd)
        pf = self.pass_fail(a.year == self.year and a.month == self.month and a.day == self.day)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} YMD(list) {pf[1]}')
            all_passed = False
        idx += 1

        res = ['  Testing constructors']
        if not all_passed:
            res.extend(failures)
        return res

    def test_set(self):
        """
        Run the tests for setting members.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        a = YMD()
        a.set_ymd(self.year, self.month, self.day)
        pf = self.pass_fail(a.year == 2013 and a.month == 1 and a.day == 1)
        a.set_ymd(0, 1, 1)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_ymd(h, m, s) {pf[1]}')
            all_passed = False
        idx += 1

        a.set_ymd(year=self.year, month=self.month, day=self.day)
        pf = self.pass_fail(a.year == self.year and a.month == self.month and a.day == self.day)
        a.set_ymd(0, 1, 1)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_ymd(kwargs) {pf[1]}')
            all_passed = False
        idx += 1

        a.set_ymd(self.ymd)
        pf = self.pass_fail(a.year == self.year and a.month == self.month and a.day == self.day)
        a.set_ymd(0, 1, 1)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_ymd(list) {pf[1]}')
            all_passed = False

        res = ['  Testing setting members']
        if not all_passed:
            res.extend(failures)
        return res

    def test_comparison_operators(self):
        """
        Run the tests for the comparison operators.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        y1 = YMD(self.ymd)
        y2 = YMD(self.ymd)
        pf = self.pass_fail(y1 == y2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≡ {pf[1]}')
            all_passed = False
        idx += 1

        y1 = YMD(self.ymd)
        y2 = YMD([2013, 12, 30])
        pf = self.pass_fail(y1 != y2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≠ {pf[1]}')
            all_passed = False
        idx += 1

        a = YMD(1992, 7, 2)
        b = YMD(1993, 7, 2)
        c = YMD(1992, 8, 2)
        d = YMD(1992, 7, 3)

        pf = self.pass_fail(a < b and a < c and a < d \
                            and a <= b and a <= c and d >= a >= a \
                            and not (a > b) and not (a >= b))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} < and ≤ {pf[1]}')
            all_passed = False
        idx += 1

        e = YMD(1992, 7, 2)
        f = YMD(1991, 7, 2)
        g = YMD(1992, 6, 2)
        h = YMD(1992, 7, 1)

        pf = self.pass_fail(e > f and e > g and e > h
                            and e >= f and e >= g and h <= e <= e
                            and not (e < f) and not (e <= f))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} > and ≥ {pf[1]}')
            all_passed = False

        res = ['  Testing comparison operators']
        if not all_passed:
            res.extend(failures)
        return res

    def test_assignment(self):
        """
        Run the tests for assignment.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        h1 = YMD([2013, 12, 31])
        h2 = h1
        pf = self.pass_fail(h1 == h2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} = {pf[1]}')
            all_passed = False

        res = ['  Testing assignment']
        if not all_passed:
            res.extend(failures)
        return res

    def test_all(self):
        """
        Run all the tests.

        The list returned has a length of >2 if there are test failures.

        :return: A list.
        """

        messages = ['YMD Tests', ' All tests passed']

        res = self.test_constructors()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_set()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_comparison_operators()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_assignment()
        if len(res) > 1:
            messages.extend(res)

        if len(messages) > 2:
            messages[1] = ' Some tests failed:'
        return messages


def main():
    res = YMDTests().test_all()
    print('\n'.join(res))


if __name__ == '__main__':
    main()
