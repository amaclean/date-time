#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   CommonTests.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


class CommonTests:
    """
    Common routines for the tests.

    :author: Andrew J. P. Maclean
    """

    @staticmethod
    def test_f1_f2(a, b, f1, f2):
        """
        Combine the results of testing two functions of the form f(p1,p2).

        :param: a The first parameter.
        :param: b The second parameter.
        :param: f1 The first function.
        :param: f2 The second function.

        :return: A pair consisting of either true or false and a message.
        """
        pf1 = f1(a, b)
        pf2 = f2(a, b)
        if pf1[0] and pf2[0]:
            return [True, f'{pf1[1]} and {pf2[1]} passed.']
        elif pf1[0] and not pf2[0]:
            return [False, f'{pf2[1]} failed.']
        elif not pf1[0] and pf2[0]:
            return [False, f'{pf1[1]} failed.']
        else:
            return [False, f'{pf1[1]} and {pf2[1]} failed.']

    @staticmethod
    def test_fg(a, b, c, f1, f2):
        """
        Combine the results of testing two functions of the form f(p1,p2)
        and g(p1,p2,p3)

        :param: a The first parameter.
        :param: b The second parameter.
        :param: c The third parameter.
        :param: f1 The first function.
        :param: f2 The second function.

        :return: A pair consisting of either true or false and a message.
        """
        pf1 = f1(a, b)
        pf2 = f2(a, b, c)
        return [pf1[0] and pf2[0], pf1[1] + pf2[1]]

    @staticmethod
    def compare(observed, expected, msg):
        """
        Compare two objects.

        The type of the objects must support the operation (operator==).

        :param: observed The expected result.
        :param: expected The observed result.
        :param: msg The first part of the pass/fail message.
        :return: A pair consisting of either true or false and a message.
        """
        ret = list()
        if observed == expected:
            ret.append(True)
            ret.append(f'{msg:s}')
        else:
            ret.append(False)
            ret.append(f'{msg:s} observed: {str(observed):s} expected: {str(expected):s}')
        return ret

    @staticmethod
    def pass_fail(result):
        """
            :param: result The result of a test, either true or false.
            :return: A Pair consisting of either true or false and a message.
        """
        if result:
            return [True, 'Passed.']
        return [False, 'Failed.']


class DictDiffer:
    """
    See: http://stackoverflow.com/questions/1165352/calculate-difference-in-keys-contained-in-two-python-dictionaries

    Calculate the difference between two dictionaries as:
    (1) Keys corresponding to items in observed but not in expected.
    (2) Keys  corresponding to items in expected but not in observed.
    (3) Keys the same in both but changed values.
    (4) Keys same in both with unchanged values
    """

    def __init__(self, observed_dict, expected_dict):
        self.observed_dict, self.expected_dict = observed_dict, expected_dict
        self.set_observed, self.set_expected = set(observed_dict.keys()), set(expected_dict.keys())
        self.intersect = self.set_observed.intersection(self.set_expected)

    def added(self):
        """
        Keys corresponding to items in observed but not in expected.
        """
        return self.set_observed - self.intersect

    def removed(self):
        """
        Keys  corresponding to items in expected but not in observed.
        """
        return self.set_expected - self.intersect

    def changed(self):
        """
        Keys the same in both but changed values.
        """
        return set(o for o in self.intersect if self.expected_dict[o] != self.observed_dict[o])

    def unchanged(self):
        """
        Keys same in both with unchanged values.
        """
        return set(o for o in self.intersect if self.expected_dict[o] == self.observed_dict[o])
