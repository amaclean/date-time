#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   JDTests.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


from dt import JD
from dt.DateTimeBase import DateTimeBase
from dtTests.CommonTests import CommonTests


class JDTests(CommonTests, DateTimeBase):
    """
    This class is a test harness to test the JD class.

    It also provides examples of how to use the JD class.

    :author: Andrew J. P. Maclean
    """

    def __init__(self):
        self.jdn = 2525008
        self.fod = 0.5

    def test_constructors(self):
        """
        Run the tests for the constructors.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        j = JD()
        pf = self.pass_fail(j.get_jdn() == 0 and j.get_fod() == 0.0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JD() {pf[1]}')
            all_passed = False
        idx += 1

        j1 = JD(self.jdn, self.fod)
        pf = self.pass_fail(j1.get_jdn() == self.jdn and j1.get_fod() == self.fod)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JD(jdn, fod) {pf[1]}')
            all_passed = False
        idx += 1

        j2 = JD(jdn=self.jdn, fod=self.fod)
        pf = self.pass_fail(j2.get_jdn() == self.jdn and j2.get_fod() == self.fod)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JD(kwargs) {pf[1]}')
            all_passed = False
        idx += 1

        j3 = JD([self.jdn, self.fod])
        pf = self.pass_fail(j3.get_jdn() == self.jdn and j3.get_fod() == self.fod)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JD(list) {pf[1]}')
            all_passed = False

        res = ['  Testing constructors']
        if not all_passed:
            res.extend(failures)
        return res

    def test_set(self):
        """
        Run the tests for setting members.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        jd = JD()
        jd.set_jd(self.jdn, self.fod)
        pf = self.pass_fail(jd.get_jdn() == self.jdn and jd.get_fod() == self.fod)
        jd.set_jd(0, 0.0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_jd(h, m, s) {pf[1]}')
            all_passed = False
        idx += 1

        jd.set_jd(jdn=self.jdn, fod=self.fod)
        pf = self.pass_fail(jd.get_jdn() == self.jdn and jd.get_fod() == self.fod)
        jd.set_jd(0, 0.0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_jd(kwargs) {pf[1]}')
            all_passed = False
        idx += 1

        jd.set_jd([self.jdn, self.fod])
        pf = self.pass_fail(jd.get_jdn() == self.jdn and jd.get_fod() == self.fod)
        jd.set_jd(0, 0.0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_jd(list) {pf[1]}')
            all_passed = False

        res = ['  Testing setting members']
        if not all_passed:
            res.extend(failures)
        return res

    def test_comparison_operators(self):
        """
        Run the tests for the comparison operators.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        jd1 = JD([self.jdn, self.fod])
        jd2 = JD([self.jdn, self.fod])
        pf = self.pass_fail(jd1 == jd2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≡ {pf[1]}')
            all_passed = False
        idx += 1

        pf = self.pass_fail(jd1.is_close(jd2))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≈ {pf[1]}')
            all_passed = False
        idx += 1

        jd2 = JD([self.jdn, self.fod - 0.001])
        pf = self.pass_fail(jd1.is_close(jd2, 0.01))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≈ (rel_tol = 0.01) {pf[1]}')
            all_passed = False
        idx += 1

        #  Expect a fail here
        pf = self.pass_fail(not jd1.is_close(jd2, .0001))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≈ (rel_tol = 0.0001) {pf[1]}')
            all_passed = False
        idx += 1

        jd2.set_fod(jd2.get_fod() - 0.2)
        pf = self.pass_fail(jd1 != jd2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≠ {pf[1]}')
            all_passed = False
        idx += 1

        a = JD(2456553, 0.53308796)
        b = JD(2456554, 0.53308796)
        c = JD(2456553, 0.53308797)

        pf = self.pass_fail(a < b and a < c
                            and a <= b and c >= a >= a
                            and not (a > b) and not (a >= b))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} < and ≤ {pf[1]}')
            all_passed = False
        idx += 1

        e = JD(2456553, 0.53308796)
        f = JD(2456552, 0.53308796)
        g = JD(2456553, 0.53308795)

        pf = self.pass_fail(e > f and e > g
                            and e >= f and g <= e <= e
                            and not (e < f) and not (e <= f))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} > and ≥ {pf[1]}')
            all_passed = False

        res = ['  Testing comparison operators']
        if not all_passed:
            res.extend(failures)
        return res

    def test_assignment(self):
        """
        Run the tests for assignment.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        jd1 = JD([self.jdn, self.fod])
        jd2 = jd1
        pf = self.pass_fail(jd1 == jd2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} = {pf[1]}')
            all_passed = False

        res = ['  Testing assignment']
        if not all_passed:
            res.extend(failures)
        return res

    def test_increment_and_addition(self):
        """
        Tests for the increment and addition operators of the JulianDateTime object.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        jd1 = JD(self.jdn, self.fod)
        jd2 = JD()
        fod = 15 / 86400.0
        jd2.set_jd(1, fod)  # One day and 15s
        jd3 = JD(jd1)
        jd3 += jd2
        jd4 = JD(self.jdn + 1, self.fod + fod)
        pf = self.pass_fail(jd3.is_close(jd4, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} jd3 += jd2 {pf[1]}')
            all_passed = False
        idx += 1

        days = 1 + fod
        jd3 = JD(jd1)
        jd3 += days
        pf = self.pass_fail(jd3.is_close(jd4, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} jd3 += days {pf[1]}')
            all_passed = False
        idx += 1

        jd3 = jd1 + jd2
        pf = self.pass_fail(jd3.is_close(jd4, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} jd1 + jd2 {pf[1]}')
            all_passed = False

        res = ['  Testing increment and addition']
        if not all_passed:
            res.extend(failures)
        return res

    def test_decrement_and_subtraction(self):
        """
        Tests for the decrement and subtraction operators of the JulianDateTime object.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        jd1 = JD(self.jdn, self.fod)
        jd2 = JD()
        fod = 15 / 86400.0
        jd2.set_jd(1, fod)  # One day and 15s
        jd3 = JD(jd1)
        jd3 -= jd2
        jd4 = JD(self.jdn - 1, self.fod - fod)
        pf = self.pass_fail(jd3.is_close(jd4, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} jd3 -= jd2 {pf[1]}')
            all_passed = False
        idx += 1

        days = 1 + fod
        jd3 = JD(jd1)
        jd3 -= days
        pf = self.pass_fail(jd3.is_close(jd4, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} jd3 -= days {pf[1]}')
            all_passed = False
        idx += 1

        jd3 = jd1 - jd2
        pf = self.pass_fail(jd3.is_close(jd4, DateTimeBase.rel_fod))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} jd1 - jd2 {pf[1]}')
            all_passed = False

        res = ['  Testing decrement and subtraction']
        if not all_passed:
            res.extend(failures)
        return res

    def test_jd(self):
        """
        Run the tests for making a JD object.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        a = JD()
        b = JD(a)
        pf = self.pass_fail(a == b)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≡ {pf[1]}')
            all_passed = False
        idx += 1

        a = JD(self.jdn, self.fod)
        b = JD(self.jdn, self.fod)
        pf = self.pass_fail(a == b)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JD(jdn, fod) {pf[1]}')
            all_passed = False
        idx += 1

        a = JD(jdn=self.jdn, fod=self.fod)
        b = JD(jdn=self.jdn, fod=self.fod)
        pf = self.pass_fail(a == b)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JD(kwargs) {pf[1]}')
            all_passed = False
        idx += 1

        a = JD([self.jdn, self.fod])
        b = JD([self.jdn, self.fod])
        pf = self.pass_fail(a == b)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JD(list) {pf[1]}')
            all_passed = False

        res = ['  Testing JD()']
        if not all_passed:
            res.extend(failures)
        return res

    def test_normalising(self):
        """
        Run the tests for normalising a JD object.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        a = JD(0, -0.5)
        pf = self.pass_fail((a.jdn == -1) and (a.fod == 0.5))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JD(0,-0.5) stored as jdn = -1, fod = 0.5 {pf[1]}')
            all_passed = False
        idx += 1

        a = JD(-1, 0.5)
        pf = self.pass_fail((a.jdn == -1) and (a.fod == 0.5))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JD(-1,0.5) stored as jdn = -1, fod = 0.5 {pf[1]}')
            all_passed = False
        idx += 1

        a = JD(0, -1.5)
        pf = self.pass_fail((a.jdn == -2) and (a.fod == 0.5))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JD(0,-1.5) stored as jdn = -2, fod = 0.5 {pf[1]}')
            all_passed = False
        idx += 1

        a = JD(-1.5)
        pf = self.pass_fail(a.jdn == -2 and a.fod == 0.5)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} JD(-1.5) stored as jdn = -2, fod = 0.5 {pf[1]}')
            all_passed = False

        res = ['  Testing normalising']
        if not all_passed:
            res.extend(failures)
        return res

    def test_all(self):
        """
        Run all the tests.

        The list returned has a length of >2 if there are test failures.

        :return: A list.
        """

        messages = ['JD Tests', ' All tests passed']
        res = self.test_constructors()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_set()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_comparison_operators()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_assignment()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_increment_and_addition()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_decrement_and_subtraction()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_jd()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_normalising()
        if len(res) > 1:
            messages.extend(res)

        if len(messages) > 2:
            messages[1] = ' Some tests failed:'
        return messages


def main():
    res = JDTests().test_all()
    print('\n'.join(res))


if __name__ == '__main__':
    main()
