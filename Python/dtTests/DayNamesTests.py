#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   DayNamesTests.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


from dt.DayNames import DayNames
from dtTests.CommonTests import CommonTests


class DayNamesTests(CommonTests):
    """
    This class is a test harness to test the DayNames class.

    It also provides examples of how to use the DayNames class.

    :author: Andrew J. P. Maclean
    """

    def __init__(self):
        self.dn = DayNames()

    def test_constructors(self):
        """
        Run the tests for the constructors.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        a = DayNames()
        pf = self.pass_fail(a == self.dn)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} constructor {pf[1]}')
            all_passed = False

        res = ['  Testing constructors']
        if not all_passed:
            res.extend(failures)
        return res

    def test_assignment(self):
        """
        Run the tests for assignment.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        a = self.dn
        pf = self.pass_fail(a == self.dn)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} assignment {pf[1]}')
            all_passed = False

        res = ['  Testing assignment']
        if not all_passed:
            res.extend(failures)
        return res

    def test_comparison_operators(self):
        """
        Run the tests for comparison.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        a = DayNames()
        pf = self.pass_fail(a == self.dn)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≡ {pf[1]}')
            all_passed = False
        idx += 1

        a.set_day_name(1, 'FIIK')
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≠ {pf[1]}')
            all_passed = False

        res = ['  Testing comparison operators']
        if not all_passed:
            res.extend(failures)
        return res

    def test_day_names(self):
        """
        Run the tests for testing the day names.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        abbrev_length = 3

        first = True
        for k in self.dn.names:
            name = self.dn.get_day_name(k)
            abbrev = self.dn.get_abbreviated_day_name(k)
            if name[:abbrev_length] != abbrev:
                if first:
                    failures.append(f'   Test {idx:2d} get_abbreviated_day_name(day_name)')
                    first = False
                failures.append(f'     - {name:s}: {abbrev:s}')
                all_passed = False
        idx += 1

        first = True
        for k in self.dn.names:
            name = self.dn.get_day_name(k)
            abbrev = self.dn.get_abbreviated_day_name(k, len(name))
            if name != abbrev:
                if first:
                    failures.append(f'   Test {idx:2d}  get_abbreviated_day_name(day_name, len(day_name)')
                    first = False
                failures.append(f'     - {name:s}: {abbrev:s}')
                all_passed = False
        idx += 1

        pf = self.pass_fail(self.dn.get_day_name(0) is None
                            and self.dn.get_day_name(8) is None)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} range error: get_day_name() ')
            all_passed = False
        idx += 1

        pf = self.pass_fail(self.dn.get_abbreviated_day_name(0) is None
                            and self.dn.get_abbreviated_day_name(8) is None)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} range error: get_abbreviated_day_name()')
            all_passed = False
        idx += 1

        day_name = self.dn.get_day_name(5)
        new_name = 'Day 5'
        self.dn.set_day_name(5, new_name)
        pf = self.pass_fail(self.dn.get_day_name(5) == new_name)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} check if day name can be changed: {pf[1]}')
            all_passed = False
        self.dn.set_day_name(5, day_name)

        res = ['  Testing day names and abbreviations']
        if not all_passed:
            res.extend(failures)
        return res

    def test_all(self):
        """
        Run all the tests.

        The list returned has a length of >2 if there are test failures.

        :return: A list.
        """

        messages = ['DayNames Tests', ' All tests passed']
        res = self.test_constructors()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_assignment()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_comparison_operators()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_day_names()
        if len(res) > 1:
            messages.extend(res)

        if len(messages) > 2:
            messages[1] = ' Some tests failed:'
        return messages


def main():
    res = DayNamesTests().test_all()
    print('\n'.join(res))


if __name__ == '__main__':
    main()
