#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   YMDHMSTests.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


from dt.YMDHMS import YMDHMS
from dtTests.CommonTests import CommonTests


class YMDHMSTests(CommonTests):
    """
    This class is a test harness to test the YMDHMS class.

    It also provides examples of how to use the YMDHMS class.

    :author: Andrew J. P. Maclean
    """

    def __init__(self):
        self.year = 2013
        self.month = 1
        self.day = 1
        self.hour = 23
        self.minute = 59
        self.second = 30
        self.ymd = [self.year, self.month, self.day]
        self.hms = [self.hour, self.minute, self.second]
        self.ymdhms = self.ymd + self.hms

    def test_constructors(self):
        """
        Run the tests for the constructors.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        a = YMDHMS()
        pf = self.pass_fail(
            a.ymd.year == -4712 and a.ymd.month == 1 and a.ymd.day == 1
            and a.hms.hour == 0 and a.hms.minute == 0 and a.hms.second == 0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} YMDHMS() {pf[1]}')
            all_passed = False
        idx += 1

        a = YMDHMS(self.year, self.month, self.day, self.hour, self.minute, self.second)
        pf = self.pass_fail(
            a.ymd.year == self.year and a.ymd.month == self.month and a.ymd.day == self.day
            and a.hms.hour == self.hour and a.hms.minute == self.minute and a.hms.second == self.second)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} YMDHMS(y, m, d) {pf[1]}')
            all_passed = False
        idx += 1

        a = YMDHMS(year=self.year, month=self.month, day=self.day, hour=self.hour, minute=self.minute,
                   second=self.second)
        pf = self.pass_fail(
            a.ymd.year == self.year and a.ymd.month == self.month and a.ymd.day == self.day
            and a.hms.hour == self.hour and a.hms.minute == self.minute and a.hms.second == self.second)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} YMDHMS(kwargs) {pf[1]}')
            all_passed = False
        idx += 1

        a = YMDHMS(self.ymdhms)
        pf = self.pass_fail(
            a.ymd.year == self.year and a.ymd.month == self.month and a.ymd.day == self.day
            and a.hms.hour == self.hour and a.hms.minute == self.minute and a.hms.second == self.second)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} YMDHMS(list) {pf[1]}')
            all_passed = False
        idx += 1

        a = YMDHMS(self.ymd, self.hms)
        pf = self.pass_fail(
            a.ymd.year == self.year and a.ymd.month == self.month and a.ymd.day == self.day
            and a.hms.hour == self.hour and a.hms.minute == self.minute and a.hms.second == self.second)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} YMDHMS(ymd, hms) {pf[1]}')
            all_passed = False
        idx += 1

        a = YMDHMS(ymd=self.ymd, hms=self.hms)
        pf = self.pass_fail(
            a.ymd.year == self.year and a.ymd.month == self.month and a.ymd.day == self.day
            and a.hms.hour == self.hour and a.hms.minute == self.minute and a.hms.second == self.second)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} YMDHMS(ymd=ymd, hms=hms) {pf[1]}')
            all_passed = False

        res = ['  Testing constructors']
        if not all_passed:
            res.extend(failures)
        return res

    def test_set(self):
        """
        Run the tests for setting members.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        a = YMDHMS()
        a.set_ymdhms(self.year, self.month, self.day, self.hour, self.minute, self.second)
        pf = self.pass_fail(
            a.ymd.year == self.year and a.ymd.month == self.month and a.ymd.day == self.day
            and a.hms.hour == self.hour and a.hms.minute == self.minute and a.hms.second == self.second)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_ymdhms(y, m, d, h, m, s) {pf[1]}')
            all_passed = False
        idx += 1

        a.set_ymdhms(year=self.year, month=self.month, day=self.day, hour=self.hour, minute=self.minute,
                     second=self.second)
        pf = self.pass_fail(
            a.ymd.year == self.year and a.ymd.month == self.month and a.ymd.day == self.day
            and a.hms.hour == self.hour and a.hms.minute == self.minute and a.hms.second == self.second)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_ymdhms(kwargs) {pf[1]}')
            all_passed = False
        idx += 1

        a.set_ymdhms(self.ymdhms)
        pf = self.pass_fail(
            a.ymd.year == self.year and a.ymd.month == self.month and a.ymd.day == self.day
            and a.hms.hour == self.hour and a.hms.minute == self.minute and a.hms.second == self.second)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_ymdhms(list) {pf[1]}')
            all_passed = False

        a.set_ymdhms(self.ymd, self.hms)
        pf = self.pass_fail(
            a.ymd.year == self.year and a.ymd.month == self.month and a.ymd.day == self.day
            and a.hms.hour == self.hour and a.hms.minute == self.minute and a.hms.second == self.second)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_ymdhms(ymd, hms) {pf[1]}')
            all_passed = False
        idx += 1

        a.set_ymdhms(ymd=self.ymd, hms=self.hms)
        pf = self.pass_fail(
            a.ymd.year == self.year and a.ymd.month == self.month and a.ymd.day == self.day
            and a.hms.hour == self.hour and a.hms.minute == self.minute and a.hms.second == self.second)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_ymdhms(ymd=ymd, hms=hms) {pf[1]}')
            all_passed = False
        idx += 1

        a.set_ymd(self.ymd)
        pf = self.pass_fail(
            a.ymd.year == self.year and a.ymd.month == self.month and a.ymd.day == self.day
            and a.hms.hour == self.hour and a.hms.minute == self.minute and a.hms.second == self.second)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_ymdhms(ymd) {pf[1]}')
            all_passed = False
        idx += 1

        a.set_hms(self.hms)
        pf = self.pass_fail(
            a.ymd.year == self.year and a.ymd.month == self.month and a.ymd.day == self.day
            and a.hms.hour == self.hour and a.hms.minute == self.minute and a.hms.second == self.second)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_ymdhms(hms) {pf[1]}')
            all_passed = False

        res = ['  Testing setting members']
        if not all_passed:
            res.extend(failures)
        return res

    def test_comparison_operators(self):
        """
        Run the tests for the comparison operators.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        y1 = YMDHMS(self.ymdhms)
        y2 = YMDHMS(self.ymdhms)
        pf = self.pass_fail(y1 == y2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≡ {pf[1]}')
            all_passed = False
        idx += 1

        y2 = YMDHMS(self.year, self.month, self.day, self.hour, self.minute, self.second + 0.01)
        pf = self.pass_fail(y1.is_close(y2, 0.001))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≈ (rel_tol = 0.001) {pf[1]}')
            all_passed = False
        idx += 1

        #  Expect a fail here
        pf = self.pass_fail(not y1.is_close(y2, 0.0001))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≉ (rel_tol = 0.0001) {pf[1]}')
            all_passed = False
        idx += 1

        y1 = YMDHMS(self.ymdhms)
        y2 = YMDHMS([2013, 12, 30, 23, 59, 59.9])
        pf = self.pass_fail(y1 != y2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≠ {pf[1]}')
            all_passed = False
        idx += 1

        a = YMDHMS(1992, 7, 2, 12, 58, 59.8)
        b = YMDHMS(1993, 7, 2, 12, 58, 59.8)
        c = YMDHMS(1992, 8, 2, 12, 58, 59.8)
        d = YMDHMS(1992, 7, 3, 12, 58, 59.8)
        e = YMDHMS(1992, 7, 2, 13, 58, 59.8)
        f = YMDHMS(1992, 7, 2, 12, 59, 59.8)
        g = YMDHMS(1992, 7, 2, 12, 58, 59.9)

        pf = self.pass_fail(
            a < b and a < c and a < d
            and a < e and a < f and a < g
            and a <= b and a <= c and a <= d
            and a <= e and a <= f and g >= a >= a
            and not (a > b) and not (a >= b))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} < and ≤ {pf[1]}')
            all_passed = False
        idx += 1

        h = YMDHMS(1992, 7, 2, 12, 59, 59.9)
        i = YMDHMS(1991, 7, 2, 12, 59, 59.9)
        j = YMDHMS(1992, 6, 2, 12, 59, 59.9)
        k = YMDHMS(1992, 7, 1, 12, 59, 59.9)
        l = YMDHMS(1992, 7, 2, 11, 59, 59.9)
        m = YMDHMS(1992, 7, 2, 12, 58, 59.9)
        n = YMDHMS(1992, 7, 2, 12, 58, 59.8)

        pf = self.pass_fail(
            h > i and h > j and h > k
            and h > l and h > m and h > n
            and h >= i and h >= j and h >= k
            and h >= l and h >= m and n <= h <= h
            and not (h < i) and not (h <= i))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} > and ≥ {pf[1]}')
            all_passed = False

        res = ['  Testing comparison operators']
        if not all_passed:
            res.extend(failures)
        return res

    def test_assignment(self):
        """
        Run the tests for assignment.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        h1 = YMDHMS([2013, 12, 31, 23, 59, 59.999])
        h2 = h1
        pf = self.pass_fail(h1 == h2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} = {pf[1]}')
            all_passed = False

        res = ['  Testing assignment']
        if not all_passed:
            res.extend(failures)
        return res

    def test_all(self):
        """
        Run all the tests.

        The list returned has a length of >2 if there are test failures.

        :return: A list.
        """

        messages = ['YMDHMS Tests', ' All tests passed']

        res = self.test_constructors()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_set()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_comparison_operators()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_assignment()
        if len(res) > 1:
            messages.extend(res)

        if len(messages) > 2:
            messages[1] = ' Some tests failed:'
        return messages


def main():
    res = YMDHMSTests().test_all()
    print('\n'.join(res))


if __name__ == '__main__':
    main()
