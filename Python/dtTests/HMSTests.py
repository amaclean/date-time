#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   HMSTests.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


from dt.HMS import HMS
from dtTests.CommonTests import CommonTests


class HMSTests(CommonTests):
    """
    This class is a test harness to test the HMS class.

    It also provides examples of how to use the HMS class.

    :author: Andrew J. P. Maclean
    """

    def __init__(self):
        self.hour = 23
        self.minute = 59
        self.second = 59.999
        self.hms_list = [self.hour, self.minute, self.second]
        self.value_str = ' 23:59:59.999000000000'

    def test_constructors(self):
        """
        Run the tests for the constructors.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        a = HMS()
        pf = self.pass_fail(a.hour == 0 and a.minute == 0 and a.second == 0.0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} HMS() {pf[1]}')
            all_passed = False
        idx += 1

        b = HMS(self.hour, self.minute, self.second)
        pf = self.pass_fail(b.hour == self.hour and b.minute == self.minute and b.second == self.second)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} HMS(h, m, s) {pf[1]}')
            all_passed = False
        idx += 1

        b = HMS(hour=self.hour, minute=self.minute, second=self.second)
        pf = self.pass_fail(b.hour == self.hour and b.minute == self.minute and b.second == self.second)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} HMS(kwargs) {pf[1]}')
            all_passed = False
        idx += 1

        b = HMS(self.hms_list)
        pf = self.pass_fail(b.hour == self.hour and b.minute == self.minute and b.second == self.second)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} HMS(list) {pf[1]}')
            all_passed = False

        res = ['  Testing constructors']
        if not all_passed:
            res.extend(failures)
        return res

    def test_set(self):
        """
        Run the tests for setting members.

        :return: The list returned has a length of >1 if there are test failures.
       """

        failures = list()
        all_passed = True

        idx = 0

        a = HMS()
        a.set_hms(self.hour, self.minute, self.second)
        pf = self.pass_fail(a.hour == self.hour and a.minute == self.minute and a.second == self.second)
        a.set_hms(0, 0, 0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_hms(h, m, s) {pf[1]}')
            all_passed = False
        idx += 1

        a.set_hms(hour=self.hour, minute=self.minute, second=self.second)
        pf = self.pass_fail(a.hour == self.hour and a.minute == self.minute and a.second == self.second)
        a.set_hms(0, 0, 0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_hms(kwargs) {pf[1]}')
            all_passed = False
        idx += 1

        a.set_hms(self.hms_list)
        pf = self.pass_fail(a.hour == self.hour and a.minute == self.minute and a.second == self.second)
        a.set_hms(0, 0, 0)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} set_hms(list) {pf[1]}')
            all_passed = False

        res = ['  Testing setting members']
        if not all_passed:
            res.extend(failures)
        return res

    def test_comparison_operators(self):
        """
        Run the tests for the comparison operators.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        h1 = HMS(self.hms_list)
        h2 = HMS(self.hms_list)
        pf = self.pass_fail(h1 == h2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≡ {pf[1]}')
            all_passed = False
        idx += 1

        h2 = HMS(self.hour, self.minute, self.second - 0.001)
        pf = self.pass_fail(h1.is_close(h2, 0.001))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≈ (rel_tol = 0.001) {pf[1]}')
            all_passed = False
        idx += 1

        pf = self.pass_fail(not h1.is_close(h2, 0.00001))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≉ (rel_tol = 0.00001) {pf[1]}')
            all_passed = False
        idx += 1

        h2 = HMS([23, 59, 59.99])
        pf = self.pass_fail(h1 != h2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} ≠ {pf[1]}')
            all_passed = False
        idx += 1

        a = HMS(13, 7, 2)
        b = HMS(14, 7, 2)
        c = HMS(13, 8, 2)
        d = HMS(13, 7, 3)

        pf = self.pass_fail(a < b and a < c and a < d \
                            and a <= b and a <= c and d >= a >= a \
                            and not (a > b) and not (a >= b))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} < and ≤ {pf[1]}')
            all_passed = False
        idx += 1

        e = HMS(13, 7, 2)
        f = HMS(12, 7, 2)
        g = HMS(13, 6, 2)
        h = HMS(13, 7, 1)

        pf = self.pass_fail(e > f and e > g and e > h
                            and e >= f and e >= g and h <= e <= e
                            and not (e < f) and not (e <= f))
        if not pf[0]:
            failures.append(f'   Test {idx:2d} > and ≥ {pf[1]}')
            all_passed = False

        res = ['  Testing comparison operators']
        if not all_passed:
            res.extend(failures)
        return res

    def test_assignment(self):
        """
        Run the tests for assignment.

        :return: The list returned has a length of >1 if there are test failures.
        """

        failures = list()
        all_passed = True

        idx = 0

        h1 = HMS(self.hms_list)
        h2 = h1
        pf = self.pass_fail(h1 == h2)
        if not pf[0]:
            failures.append(f'   Test {idx:2d} = {pf[1]}')
            all_passed = False

        res = ['  Testing assignment']
        if not all_passed:
            res.extend(failures)
        return res

    def test_all(self):
        """
        Run all the tests.

        :return: A list with a length of >2 if there are test failures.
        """
        messages = ['HMS Tests', ' All tests passed']
        res = self.test_constructors()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_set()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_comparison_operators()
        if len(res) > 1:
            messages.extend(res)
        res = self.test_assignment()
        if len(res) > 1:
            messages.extend(res)

        if len(messages) > 2:
            messages[1] = ' Some tests failed:'
        return messages


def main():
    res = HMSTests().test_all()
    print('\n'.join(res))


if __name__ == '__main__':
    main()
