from setuptools import setup

def readme():
    with open('README.md') as f:
        return f.read()

setup(name = 'dt',
      version = '3.0',
      description = 'Date Time Library',
      long_description = readme(),
      url = 'https://gitlab.com/amaclean/date-time',
      author = 'Andrew Maclean',
      author_email = 'andrew.amaclean@gmail.com',
      license = 'BSD',
      packages = ['dt', 'dtTests'],
      zip_safe = False)
