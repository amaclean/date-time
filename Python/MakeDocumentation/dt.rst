dt 
***

Date Time Base
==============

.. automodule:: DateTimeBase
   :members:

Date Conversions
================

Function names follow this pattern: xx2yyZ[Z] where **xx != yy**, **2** indicates conversion from **xx** to **yy** and:

- xx in one of **cd** (calendar date) or **jd** (julian date)
- yy is one of **jg** or **cd**
- Z[Z] is the calendrical system being used:

  * **J** (Julian/Julian Proleptic Calendar),
  * **G** (Gregorian Proleptic/Gregorian Calendar) or
  * **JG** (Julian/Gregorian Calendar)

Thus a method like :func:`DateTimeConversions.cd2jdG` converts a calendar date to julian date using the Gregorian Proleptic Calendar.

Other abbreviations are:

- **dow**: Day of the week.
- **doy**: Day of the year.
- **woy**: Week of the year.

For the parameters:

- **ymd**: Is a list [year, month, day]
- **jd**: Is the Julian Day Number of the date.

.. automodule:: DateConversions
   :members:

Date Time Format
================

.. automodule:: DateTimeFormat
   :members:

Date Time Formatter
===================

.. automodule:: DateTimeFormatter
   :members:

Date Time Parser
================

.. automodule:: DateTimeParser
   :members:

Day Names
=========

.. automodule:: DayNames
   :members:

Hours Minutes
=============

Times can be compared using the comparison operators:

+------+------+-------+
| Available Operators |
+======+======+=======+
|  ==  |   <  |   >   |
+------+------+-------+
|  !=  |   <= |   >=  |
+------+------+-------+

.. automodule:: HM
   :members:

Hours Minutes Seconds
=====================

Times can be compared using the comparison operators:

+------+------+-------+
| Available Operators |
+======+======+=======+
|  ==  |   <  |   >   |
+------+------+-------+
|  !=  |   <= |   >=  |
+------+------+-------+

.. automodule:: HMS
   :members:

ISO Week Date
=============

ISO week dates can be compared using the comparison operators:

+------+------+-------+
| Available Operators |
+======+======+=======+
|  ==  |   <  |   >   |
+------+------+-------+
|  !=  |   <= |   >=  |
+------+------+-------+

.. automodule:: ISOWeekDate
   :members:

Julian Date
===========

Julian dates can be compared using the comparison operators:

+------+------+-------+
| Available Operators |
+======+======+=======+
|  ==  |   <  |   >   |
+------+------+-------+
|  !=  |   <= |   >=  |
+------+------+-------+

The following arithmetic operators are available:

+------+---------------+
| Arithmetic Operators |
+======+===============+
|  \+  |   +=          |
+------+---------------+
|  \-  |   -=          |
+------+---------------+

.. automodule:: JD
   :members:

Julian Date Time
================

The module :mod:`JulianDateTime` depends upon the module :mod:`DateConversions` and :mod:`TimeConversions`.

This class stores the Date and Time as a Julian Day Number and the Fraction of the Day.

Dates can be compared using the comparison operators:

+------+------+-------+
| Available Operators |
+======+======+=======+
|  ==  |   <  |   >   |
+------+------+-------+
|  !=  |   <= |   >=  |
+------+------+-------+

The following arithmetic operators are available:

+------+---------------+
| Arithmetic Operators |
+======+===============+
|  \+  |   +=          |
+------+---------------+
|  \-  |   -=          |
+------+---------------+

.. automodule:: JulianDateTime
   :members:

Month Names
===========

.. automodule:: MonthNames
   :members:

Time Conversions
================

Abbreviations in function names are:

- **h**: Hours.
- **hm**: Hours and minutes.
- **hms**: Hours, minutes and seconds.

For the parameters:

- **hms**: Is a list [hours, minutes, seconds]
- **hm**: Is a list [hours, minutes]
- **hr**: Is the hour and decimals of an hour.

.. automodule:: TimeConversions
   :members:

Time Interval
=============

Time intervals can be compared using the comparison operators:

+------+------+-------+
| Available Operators |
+======+======+=======+
|  ==  |   <  |   >   |
+------+------+-------+
|  !=  |   <= |   >=  |
+------+------+-------+

The following arithmetic operators are available:

+------+---------------+
| Arithmetic Operators |
+======+===============+
|  \+  |   +=          |
+------+---------------+
|  \-  |   -=          |
+------+---------------+

.. automodule:: TimeInterval
   :members:

Time Zones
==========

.. automodule:: TimeZones
   :members:

Year Month Day
==============

Dates can be compared using the comparison operators:

+------+------+-------+
| Available Operators |
+======+======+=======+
|  ==  |   <  |   >   |
+------+------+-------+
|  !=  |   <= |   >=  |
+------+------+-------+

.. automodule:: YMD
   :members:

Year Month Day Hours Minutes Seconds
====================================

Dates can be compared using the comparison operators:

+------+------+-------+
| Available Operators |
+======+======+=======+
|  ==  |   <  |   >   |
+------+------+-------+
|  !=  |   <= |   >=  |
+------+------+-------+

.. automodule:: YMDHMS
   :members:

Constants
=========

.. automodule:: Constants
   :members:
