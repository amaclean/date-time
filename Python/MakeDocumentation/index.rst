.. Date Time Library documentation master file, created by
   sphinx-quickstart on Mon Dec 24 14:34:33 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. |br| raw:: html

   <br />

Welcome to Date Time Library's documentation!
=============================================

.. include:: ../README.md

Contents:
=========

.. toctree::
   :maxdepth: 2

   dt

.. toctree::
   :maxdepth: 2

   Examples

.. toctree::
   :maxdepth: 2

   Testing



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
