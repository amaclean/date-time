Examples
************************************

These examples demonstrate the usage of the date and time library.

They also provide examples of usage that may be useful in your own classes.

DateTime
========
Shows how to use the :mod:`JulianDateTime` and :mod:`DateTimeFormatter` classes.

.. automodule:: DateTime
   :members:

Parser
======

Shows how to use the :mod:`DateTimeParser` and :mod:`DateTimeFormat` classes.

.. automodule:: Parser
   :members:

Simple Example
==============
A simple example showing the usage of the :mod:`JulianDateTime`, :mod:`DateTimeParser` and :mod:`DateTimeFormatter` classes.

.. automodule:: SimpleExample
   :members:

Time Zone
=========

Shows how to use the :mod:`JulianDateTime`, :mod:`TimeZones` and :mod:`DateTimeFormatter` classes.

.. automodule:: TimeZone
   :members:
