Tests
*****

These tests verify the functionality of the various date time classes.

They also provide examples of usage that may be useful in your own classes.

Common Tests
============

Holds methods common to the tests.

.. automodule:: CommonTests
   :members:

Date Conversions
================

Tests the functionality of :mod:`DateConversions`.

.. automodule:: DateConversionsTests
   :members:

Date Time Formatter
===================

Tests the functionality of :mod:`DateTimeFormatter`.

.. automodule:: DateTimeFormatterTests
   :members:

Date Time Parser
================

Tests the functionality of :mod:`DateTimeParser`.

.. automodule:: DateTimeParserTests
   :members:

Day Names
=========

Tests the functionality of :mod:`DayNames`.

.. automodule:: DayNamesTests
   :members:

Hours Minutes
=============

Tests the functionality of :mod:`HM`.

.. automodule:: HMTests
   :members:

Hours Minutes Seconds
=====================

Tests the functionality of :mod:`HMS`.

.. automodule:: HMSTests
   :members:

ISO Week Date
=============

Tests the functionality of :mod:`ISOWeekDateTests`.

.. automodule:: ISOWeekDateTests
   :members:

Julian Date
===========

Tests the functionality of :mod:`JD`.

.. automodule:: JDTests
   :members:

Julian Date Time
================

Tests the functionality of :mod:`JulianDateTime`.

.. automodule:: JulianDateTimeTests
   :members:

Month Names
===========

Tests the functionality of :mod:`MonthNames`.

.. automodule:: MonthNamesTests
   :members:

Time Conversions
================

Tests the functionality of :mod:`TimeConversions`.

.. automodule:: TimeConversionsTests
   :members:

Time Interval
=============

Tests the functionality of :mod:`TimeInterval`.

.. automodule:: TimeIntervalTests
   :members:

Time Zones
==========

Tests the functionality of :mod:`TimeZoness`.

.. automodule:: TimeZonesTests
   :members:

Year Month Day
==============

Tests the functionality of :mod:`YMD`.

.. automodule:: YMDTests
   :members:

Year Month Day Hours Minutes Seconds
====================================

Tests the functionality of :mod:`YMDHMS`.

.. automodule:: YMDHMSTests
   :members:
