#!/bin/bash

# Author: Andrew Maclean
# Date: 2012-10-17
# Last Modified: 2015-02-26

# Purpose:
# Set the file permissions of files to be executable.
# If you create a file in Windows, it's permissions are set to 100644.
# If the file is a script or executable, you need to change the permissions
# to 100755.
#
# Run this in the top-level directory.
#
# Setup find correctly.
IFS=$'\n'
# Get a list of files whose permissions need to be reset.
extensions=("sh" "py")
declare -a result
declare -a t
declare -a files
# Get all the files.
files=($(git ls-files -s | awk '{print $N}' ))
#echo ${files[@]}
for ext in "${extensions[@]}"; do
    for fn in "${files[@]}"; do
        t=($(echo "$fn" | awk  '$1 == "100644" && $NF ~/.'$ext'$/ { print $N }'))
        for x in ${t[@]}; do
            y=$(echo "$x" | cut -f2)
            result=( ${result[@]} $y )
        done
    done
done
for res in  "${result[@]}"; do
    echo $res
    chmod +x "$res"
    echo "git update-index --chmod=+x" ""$res""
done
