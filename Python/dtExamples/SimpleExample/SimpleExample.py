#!/usr/bin/env python3

"""
    A simple example demonstrating the usage of the date and time library.

    We parse a date string and output everything that we know about the date.

"""

import datetime
from zoneinfo import ZoneInfo

from dt import DateTimeFormat, DateTimeFormatter, DateTimeParser, JulianDateTime
from dt.Constants import DayName, MonthName


def date_details(date_to_parse):
    """
    Display some information about a date.

    :param: date_to_parse The date to parse.
    """

    def build_date_string(d):
        """
        :parm: d A dictionary containing the formatted date.
        """
        res = [
            f'Details for:         {d['NamedDate']}',
            f'Calendar             {d['Calendar']}',
            f'JD (full precision): {d['JD']}',
            f'JD (short form):     {d['JDShort']}',
            f'Date:                {d['Date']}',
            f'Day of the year:     {d['DoY']}',
            f'Day of the week:     {d['DoW']} = {d['DayName']} or {d['DayNameAbbrev']}',
        ]
        if d['IWD']:
            res.append(f'ISO Day of the week: {d['IWD']}')
        res.append(f'Leap Year:           {d['LeapYear']}')
        return res

    # Define the formatting parameters and set the calendar.
    # Note: fmt.calendar is JULIAN_GREGORIAN by default.
    fmt = DateTimeFormat()
    fmt.day_name = DayName.full_name
    fmt.month_name = MonthName.full_name
    fmt.precision_seconds = 3
    fmt.precision_fod = 8
    formatter = DateTimeFormatter()
    parser = DateTimeParser()
    # Here we just parse the date and output everything about the date,
    # (if the parse was successful).
    parsed_date = parser.parse_date_time(date_to_parse, fmt)
    if parsed_date[0] == 1:
        jdt = JulianDateTime(parsed_date[1], fmt.calendar)
        dates = formatter.get_formatted_dates(jdt, fmt)
        # Output the date and other information about the date.
        print('\n'.join(build_date_string(dates)))
    else:
        # Why did it fail?
        print(f'Parse failed: {parser.parserMessages[parsed_date[0]]:s}\n')


def main():
    print('--------------------------------------------')
    print('Demonstrating parsing and outputting a date.')
    print('--------------------------------------------')
    # date_to_parse = '2016-12-31 23:59:59.99997'
    # date_to_parse = '2016-12-31 23:01:02.99997'
    # date_to_parse = '2016-12-31 00:01:01.99997'
    date_to_parse = '1900-December-31 15:12:11.123'

    date_details(date_to_parse)
    print('--------------------------------------------')
    tz = 'Australia/Sydney'
    print(f'Using the system time and a time zone ({tz}):')
    dt = datetime.datetime.now(ZoneInfo(tz))
    print(f'ISO Date to parse:   {dt.isoformat()}\n')
    iso_dt = dt.isoformat()[:-6].replace('T', ' ')
    date_details(iso_dt)


if __name__ == '__main__':
    main()
