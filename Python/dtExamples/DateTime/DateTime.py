#!/usr/bin/env python3

"""
    Demonstrates the usage of the date and time library.

    The initialisation of the JulianDateTime class along with its usage
    is demonstrated.

    How to nicely format the date is also demonstrated
    using a class called DateFormatter and BuildDateString functions.

    This example also demonstrates the importance of selecting
    the correct calendar to use.

"""

from datetime import datetime

from dt import DateTimeFormat, DateTimeFormatter, JulianDateTime
from dt.Constants import CalendarType, DayName, MonthName


def date_details():
    """
    Display some information about a date.
    """

    def build_date_string(d):
        """
        :parm: d A dictionary containing the formatted date.
        """
        res = [
            f'Details for:         {d['NamedDate']}',
            f'Calendar             {d['Calendar']}',
            f'JD (full precision): {d['JD']}',
            f'JD (short form):     {d['JDShort']}',
            f'Date:                {d['Date']}',
            f'Day of the year:     {d['DoY']}',
            f'Day of the week:     {d['DoW']} = {d['DayName']} or {d['DayNameAbbrev']}',
        ]
        if d['IWD']:
            res.append(f'ISO Day of the week: {d['IWD']}')
        res.append(f'Leap Year:           {d['LeapYear']}')
        return res

    fmt = DateTimeFormat()
    fmt.day_name = DayName.full_name
    fmt.month_name = MonthName.full_name
    formatter = DateTimeFormatter()
    # We instantiate a list to hold the date and time.
    ymdhms = [2012, 12, 31, 15, 12, 11.123]
    # Instantiate a JulianDateTime class.
    jdt = JulianDateTime()
    # Set the date and time
    jdt.set_date_time(ymdhms, fmt.calendar)
    dates = formatter.get_formatted_dates(jdt, fmt)

    # Output the date and other information about the date
    print('\n'.join(build_date_string(dates)))
    print()

    # Try some other dates.
    fmt.calendar = CalendarType.julian
    # Use a copy constructor.
    jdt = JulianDateTime(1900, 12, 31, 15, 12, 11.123, fmt.calendar)
    dates = formatter.get_formatted_dates(jdt, fmt)
    print('\n'.join(build_date_string(dates)))
    print()

    fmt.calendar = CalendarType.gregorian
    jdt = JulianDateTime(1900, 12, 31, 15, 12, 11.123, fmt.calendar)
    dates = formatter.get_formatted_dates(jdt, fmt)
    print('\n'.join(build_date_string(dates)))
    print()

    fmt.calendar = CalendarType.julian_gregorian
    jdt = JulianDateTime(1900, 12, 31, 15, 12, 11.123, fmt.calendar)
    dates = formatter.get_formatted_dates(jdt, fmt)
    print('\n'.join(build_date_string(dates)))
    print()


def calender_differences():
    """
    Demonstrate the differences between calendars.
    """

    '''
    Display some information about a date.
    '''

    def build_date_string(d):
        """
        :parm: d A dictionary containing the d.
        """
        res = [
            f'For JD: {d['JDShort']:s}',
            f'{d['Calendar']:<16s}: {d['NamedDate']:s}',
            f'{" ":18s}{d['Date']:s}',
        ]
        return res

    def get_dates(julian_date_time, dt_fmt):
        res = dict()
        i = 0
        formatter = DateTimeFormatter()
        for p in dt_fmt:
            dd = formatter.get_formatted_dates(julian_date_time, p)
            # Pick what we want to put in the result.
            if i == 0:
                res['Calendar'] = dd['Calendar']
                res['JDShort'] = dd['JDShort']
                res['Date'] = dd['Date']
            else:
                res['NamedDate'] = dd['NamedDate']
            i += 1
        return res

    # We instantiate a list to hold the date and time.
    ymdhms = [-4712, 1, 1, 0, 0, 1 / 3.0]
    #    ymdhms = [1582, 10, 15, 0, 0, 1 / 3.0]
    # Instantiate a JulianDateTime class.
    jdt = JulianDateTime()
    # Set the date and time
    jdt.set_date_time(ymdhms, CalendarType.julian_gregorian)

    # Define the formatting parameters.
    # We will use two different formats so fill a list
    # with the formats and modify as appropriate.
    fmt = [DateTimeFormat()] * 2
    fmt[1].day_name = DayName.full_name
    fmt[1].month_name = MonthName.full_name

    print(f'It is important to specify the calendar since the'
          f'\n date may be different for different calendars:\n')
    # Output the date in several formats for different calendars.'
    dates = get_dates(jdt, fmt)
    print('\n'.join(build_date_string(dates)))
    fmt[1].calendar = fmt[0].calendar = CalendarType.gregorian
    dates = get_dates(jdt, fmt)
    print('\n'.join(build_date_string(dates)))
    # It is easy to set a different date and time, here we just change the year.
    ymdhms[0] = 2012
    jdt.set_date_time(ymdhms, CalendarType.julian_gregorian)
    fmt[1].calendar = fmt[0].calendar = CalendarType.gregorian
    dates = get_dates(jdt, fmt)
    print('\n'.join(build_date_string(dates)))
    fmt[1].calendar = fmt[0].calendar = CalendarType.julian
    dates = get_dates(jdt, fmt)
    print('\n'.join(build_date_string(dates)))


def main():
    t0 = datetime.now()
    # print ('Started:      ', t0.strftime('%Y-%m-%d %H:%M:%S.%f'))
    print('--------------------------------------------')
    print('Demonstrating details of dates.')
    print('--------------------------------------------')
    date_details()
    print('--------------------------------------------')
    print('Demonstrating differences between calendars.')
    print('--------------------------------------------')
    calender_differences()
    t1 = datetime.now()
    print('--------------------------------------------')
    # print ('Finished:    ', t1.strftime('%Y-%m-%d %H:%M:%S.%f'))
    delta_t = t1 - t0
    print(f'Elapsed time: {str(delta_t):s}')


if __name__ == '__main__':
    main()
