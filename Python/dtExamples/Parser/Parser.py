#!/usr/bin/env python3

"""
    How to use the DateTimeParser class.

"""

from datetime import datetime

# import dt
from dt import DateTimeFormat, DateTimeParser

# Some test data.
decStrs = [
    '456.123', '123', '-456.123', '+123', '.123',
    'x', ''
]
dateStrs = [
    '-12-Jul-20', '2015-01-01', '2015-x-01', '2015-1-1',
    '', '1234', '2015 01 01'
]
timeStrs = [
    '12', '-12:12', '-12:15:23.456', '-12-15-23.456',
    '12:23.1x', ''
]
dateTimeStrs = [
    '2015-01-01 12', '2015-01-01 -12:12',
    '2015-01-01 -12:15:23.456',
    '-12-01-23.456', '-4712-01-01 -12-01-23.456',
    '2015-01-01 -12:15:23.456x', '2015-01-01', ''
]
timeIntervalStrs = [
    ' 0d  00:00:0', '-1d -06:00:00.0', '-1d  06:00:00.0', ' 1d -06:00:00.0',
    ' 1d  06:00:00.0',
    ' 12  ', '-12 2.3 ', '-12 2:3:4x ', '-12 6:0:10.3 ',
    '6x -12 6:0:10.3 ', ''
]

# This is the date time helper, it implements the parsers.
parser = DateTimeParser()
fmt = DateTimeFormat()


def parse_dec_str():
    """
    """
    print('Parsing decimals.')
    for p in decStrs:
        res = parser.parse_dec_num(p)
        if res[0] == 1:
            print(p, '=', res[1])
        else:
            print(p, ':', parser.parserMessages[res[0]])
    print()


def parse_date_str():
    """
    """
    print('Parsing dates.')
    for p in dateStrs:
        res = parser.parse_date(p, fmt)
        if res[0] == 1:
            print(p, '=', res[1])
        else:
            print(p, ':', parser.parserMessages[res[0]])
    print()


def parse_time_str():
    """
    """
    print('Parsing times.')
    for p in timeStrs:
        res = parser.parse_time(p, fmt)
        if res[0] == 1:
            print(p, '=', res[1])
        else:
            print(p, ':', parser.parserMessages[res[0]])
    print()


def parse_date_time_str():
    """
    """
    print('Parsing date and times.')
    for p in dateTimeStrs:
        res = parser.parse_date_time(p, fmt)
        if res[0] == 1:
            print(p, '=', res[1].get_ymd(), res[1].get_hms())
        else:
            print(p, ':', parser.parserMessages[res[0]])
    print()


def parse_date_time_interval_str():
    """
    """
    print('Parsing time intervals.')
    for p in timeIntervalStrs:
        res = parser.parse_time_interval(p, fmt)
        if res[0] == 1:
            print(p, '=', res[1])
        else:
            print(p, ':', parser.parserMessages[res[0]])
    print()


def main():
    # If you need to change the parser messages, e.g change the language,
    # here's how to do it:
    parser.parserMessages[2] = 'No hay datos.'
    t0 = datetime.now()
    # print('Started:       ', t0.strftime('%Y-%m-%d %H:%M:%S.%f'))
    print('--------------------------------------------')
    print('Demonstrating the parsers.')
    print('--------------------------------------------')
    parse_dec_str()
    parse_date_str()
    parse_time_str()
    parse_date_time_str()
    parse_date_time_interval_str()
    print('--------------------------------------------')
    t1 = datetime.now()
    # print('Finished:     ', t1.strftime('%Y-%m-%d %H:%M:%S.%f'))
    delta_t = t1 - t0
    print(f'Elapsed time:  {str(delta_t):s}')


if __name__ == '__main__':
    main()
