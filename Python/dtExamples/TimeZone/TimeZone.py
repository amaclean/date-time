#!/usr/bin/env python3

"""
 How to do time zone calculations.

 Demonstrates:

 1. How to convert between time zones.
 2. Date interval arithmetic.
 3. Format the date for output.
 4. Format the time zone for output.
 5. Decrementing (-=) and incrementing (+=) a date.

"""

import sys
from datetime import datetime

from dt import DateTimeFormat, DateTimeFormatter, JulianDateTime, TimeZones


def time_zone_calculation():
    """
    Display some information about a date.
    """

    formatter = DateTimeFormatter()

    time_zones = TimeZones()
    # Set up the timezone values in days.
    tz = time_zones.get_time_zone_value('UT-09:30')
    if tz is None:
        sys.exit('Unable to get a time zone offset for: UT-09:30')
    tz /= 3600.0
    tz_alt = time_zones.get_time_zone_value('UT+10')
    if tz_alt is None:
        sys.exit('Unable to get a time zone offset for: UT+10')
    tz_alt /= 3600.0

    # Define the formatting parameters.
    fmt = DateTimeFormat()

    # Note: fmt.calendar is JULIAN_GREGORIAN by default.
    # Get the time for the time zone.
    lt = [2012, 1, 3, 9, 46, 12]
    # Instantiate a JulianDateTime class.
    jd_lt = JulianDateTime(lt, fmt.calendar)

    # Instantiate a class to hold UTC.
    jd_utc = JulianDateTime(jd_lt)
    #  Subtract the time zone difference in days to get UTC.
    jd_utc -= tz / 24.0

    # Now convert to the new time zone.
    # Instantiate a class to hold the alternate time.
    jd_lt_utc = JulianDateTime(jd_utc)
    # Add the time zone difference in days to get local time.
    jd_lt_utc += tz_alt / 24.0

    # Nicely format the output.

    # We generally display the time zone at the end of the time string.
    hm = [int(tz), (tz - int(tz)) * 60.0]

    local_time = formatter.format_as_ymdhms(jd_lt, fmt) + \
                 ' ' + formatter.format_tz_as_hm(hm)
    # For UTC there is no need for the time zone (00:00).
    utc_time = formatter.format_as_ymdhms(jd_utc, fmt)
    hm[0] = int(tz_alt)
    hm[1] = (tz_alt - int(tz_alt)) * 60.0
    alternate_local_time = formatter.format_as_ymdhms(jd_lt_utc, fmt) + \
                           ' ' + formatter.format_tz_as_hm(hm)

    res = [
        f'Local Time:           {local_time:s}',
        f'UT:                   {utc_time:s}',
        f'Alternate Local Time: {alternate_local_time:s}',
        f'Note: ',
        f'1) That the alternate local time is for the following day relative to UT.',
        f'2) These three times represent the same time, so they are equivalent.',
    ]
    print('\n'.join(res))


def main():
    t0 = datetime.now()
    # print ('Started:      ', t0.strftime('%Y-%m-%d %H:%M:%S.%f'))
    print('--------------------------------------------')
    print('Demonstrating time zone calculations.')
    print('--------------------------------------------')
    time_zone_calculation()
    print('--------------------------------------------')
    t1 = datetime.now()
    # print ('Finished:    ', t1.strftime('%Y-%m-%d %H:%M:%S.%f'))
    delta_t = t1 - t0
    print('Elapsed time: {:s}'.format(str(delta_t)))


if __name__ == '__main__':
    #    print os.environ['PYTHONPATH'].split(os.pathsep)
    main()
