#!/usr/bin/env python3

import math
from datetime import datetime
from pathlib import Path

import dt


def get_program_parameters():
    import argparse
    description = 'Test the DE431 dates.'
    epilogue = '''
    '''
    parser = argparse.ArgumentParser(description=description, epilog=epilogue,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('dates', help='The file of dates to test: ../../C++/TestDE431/DE431Dates.txt.')
    parser.add_argument('result', help='The result file: result.txt.')
    args = parser.parse_args()
    return args.dates, args.result


class TestResults:
    """
    Used to store the test results.
    """

    def __init__(self):
        self.passed = 0
        self.failed = 0
        self.startDate = ''
        self.endDate = ''
        self.allPassed = False
        self.result = list()  # list of arrays [bool, string]


class DE431Dates:
    """
    This class is a test harness to test the Dates from the DE431.

    It also provides examples of how to use the DateTimeHelper class.

    :author: Andrew J. P. Maclean
    """

    def __init__(self):
        self.eps = 1.0e-14
        self.eps1 = 1.0e-08
        self.date_conversions = dt.DateConversions()

    def test_cd_jd(self, jd, ymd):
        """
        Test the conversion of calendar date to julian day.
        The Gregorian or Julian Calendar is used as appropriate.

        :param: jd the expected julian day for the given year, month and day.
        :param: ymd the date.
        :return: A list consisting of True (if the test passed) or False and a message.
        """
        res = [True, '']
        tjd = self.date_conversions.cd_to_jd_jg(ymd)
        ref = math.ceil(jd)
        if tjd == ref:
            res[0] = True
            res[1] = f'cd->jd: passed'
        else:
            res[0] = False
            res[1] = f'cd->jd got: {tjd - 0.5:0.1f} expected: {jd:0.1f}'
        return res

    def test_jd_cd(self, jd, ymd):
        """
        Test the conversion of calendar date to julian day and vice versa.
        The Gregorian or Julian Calendar is used as appropriate.

        :param: jd the expected julian day for the given year, month and day.
        :param: ymd the date.
        :return: A list consisting of True (if the test passed) or False and a message.
        """
        res = [True, '']
        ymd1 = self.date_conversions.jd_to_cd_jg(math.ceil(jd))
        if ymd1 == ymd:
            res[0] = True
            res[1] = f'jd->cd: passed'
        else:
            res[0] = False
            res[1] = f'jd->cd got: {str(ymd1):s} expected: {str(ymd):s}'
        return res

    def test_julian_gregorian(self, jd, ymd):
        """
        Test the conversion of calendar date to julian day and vice versa.
        The Gregorian Proleptic Calendar is used.

        :param: jd the expected julian day for the given year, month and day.
        :param: ymd the date.
        :return: A list consisting of True (if the test passed) or False and a message.
        """
        p1 = self.test_cd_jd(jd, ymd)
        p2 = self.test_jd_cd(jd, ymd)
        return [p1[0] and p2[0], f'{p1[1]}, {p2[1]}']

    def read_file(self, fp, min_jd, max_jd):
        res = TestResults()
        lines = fp.read_text().splitlines()
        lines_processed = 0
        jd_old = 0.0
        got_start_date = False
        ymd = dt.YMD()
        res.allPassed = True
        for line in lines:
            l = ' '.join(line.split())
            if not line:
                continue
            if l[0] == '#':
                continue
            v = l.split(' ')
            ymd.set_ymd(int(v[0]), int(v[1]), int(v[2]))
            jd = float(v[3])
            if jd > max_jd:
                break
            if jd < min_jd:
                continue
            else:
                if not got_start_date:
                    res.startDate = l
                    got_start_date = True
                    jd_old = jd
                res.endDate = l
                p1 = self.test_julian_gregorian(jd, ymd)
                s = f'{line:s} δ: {jd - jd_old:4.1f}d, {p1[1]:s}'
                p1[1] = s
                res.result.append(p1)
                res.allPassed = res.allPassed and p1[0]
                if p1[0]:
                    res.passed += 1
                else:
                    res.failed += 1
                jd_old = jd
            lines_processed += 1
            if (lines_processed % 10000) == 0:
                s = f'Processed {lines_processed:6d} dates.'
                print(s)
        return res


def main():
    test_file, result_file = get_program_parameters()
    tfp = Path(test_file)
    res_fp = Path(result_file)
    if not (tfp.exists() and tfp.is_file()):
        print(f'Nonexistent file: {tfp}')
        return
    t0 = datetime.now()
    # print('Test started:  ', t0.strftime('%Y-%m-%d %H:%M:%S.%f'))
    test = DE431Dates()
    # res = test.read_file(tfp, -3100015.50, 8000016.50)
    # res = test.read_file(tfp, -3099146.5, -3098690.5)
    # res = test.read_file(tfp, -3100015.50, 0.50)
    # res = test.read_file(tfp, -0.5, 365.5)
    res = test.read_file(tfp, -487.5, -1.5)
    stats = f'Out of {res.passed + res.failed:d} tests, {res.passed:d} passed and {res.failed:d} failed.'
    # fout = open(sys.argv[2], 'w', newline='\n')
    result = f'Tested\nFrom: {res.startDate:s}\nTo:   {res.endDate:s}\n'
    if not res.allPassed:
        result += 'These tests failed.\n'
        for p in res.result:
            if not p[0]:
                result += f'{p[1]}\n'
        result += f'{stats}\n'
        res_fp.write_text(result)
    else:
        res_fp.write_text(f'All tests passed.\n{stats}\n')

    t1 = datetime.now()
    # print('Test finished: ', t1.strftime('%Y-%m-%d %H:%M:%S.%f'))
    delta_t = t1 - t0
    print(stats)
    print('Elapsed time:  {:s}'.format(str(delta_t)))


if __name__ == '__main__':
    main()
