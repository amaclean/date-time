#!/usr/bin/python


import sys
from datetime import datetime

# Use this to handle differences in opening files between Python 3 and Python 2.
# See: http://stackoverflow.com/questions/29840849/writing-a-csv-file-in-python-that-works-for-both-python-2-7-and-python-3-3-in
if sys.version_info[0] > 2:
    access = 'wt'
    kwargs = {'newline': ''}
else:
    access = 'wb'
    kwargs = {}


def main():
    if len(sys.argv) != 3:
        print('Need an input file name and output file name.')
        return ()
    infile = sys.argv[1]  # testpo.431
    outfile = sys.argv[2]  # DE431Dates.txt

    t0 = datetime.now()
    #    fin = open('testpo.431', 'r')
    with open(outfile, access, **kwargs) as fout:
        with open(infile, 'r') as fin:
            skip = True
            lines = list()
            count = 0
            for line in fin:
                l = line.strip()
                if count < 9:
                    lines.append(' '.join(l.split()))
                if l == 'EOT' and skip:
                    skip = False
                    tmp = lines[1].split()
                    h1 = f'# {" ".join(tmp[1:]):s}\n'
                    tmp = lines[6].split()
                    h2 = f'# {" ".join(tmp[1:4]):s}  {" ".join(tmp[4:7]):s}\n'
                    fout.write(h1)
                    fout.write(h2)
                    continue
                else:
                    if skip:
                        continue
                s = l[3:25]
                d = s[:12].replace('.', ' ')
                jd = s[12:]
                fout.write(d + ' ' + jd + '\n')
        fin.close()
    fout.close()
    t1 = datetime.now()
    dT = t1 - t0
    print('Elapsed time:  {:s}'.format(str(dT)))


if __name__ == '__main__':
    main()
