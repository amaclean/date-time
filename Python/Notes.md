# To use it without setting up for development
You must add the top-level directory (the one above **dt**) to your **PYTHONPATH**.  
Once this is done you should be able to run the examples or tests in **dtExamples** or **dtTests**.

Depending how you have set up your Python environments you will need to do one of the following:
- Eclipse/PyDev
  - Create a project selecting the Python Grammar version and the Interpereter.
  - Then add the top-level directory (the one above **dt**)  to **Properties|PyDev-PYTHONPATH**.

- Using a terminal
    - Linux
        - Python 2.7  
            Just open a terminal and set **PYTHONPATH** as mentioned above.
        - Python 3.x  
            Open a terminal and set **PYTHONPATH** as mentioned above.  
            Then either:  
            `source activate py35`  
            or:  
            `workon python3`  
            depending upon how you have set up access to Python 3.x.  
            Deactivating is the same in both cases: `deactivate`

    - Windows
        - Python 2.7  
        If it is the default environment, just open a terminal and set **PYTHONPATH** as mentioned above.
        - Python 3.x  
        Open a terminal and  
        `activate py35` 
        Then set **PYTHONPATH** as mentioned above.  
        To deactivate: `deactivate`  

# For development using dt and dtTests in:
- Python 2.7
    - `python setup.py develop`
- Python 3.x
    - `source activate py35`  
      or:  
      `workon python3`  (in Linux)
    - `activate py35`  (in Windows)
    - `python setup.py develop`
If you do not want to install in the system directories, you can do:

    `python setup.py develop --user`

To return to the original environment type:
    - `source deactivate py35` (in Linux)
    - `deactivate py35`  (in Windows)
