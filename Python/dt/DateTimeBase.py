#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   DateTimeBase.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


import math


class DateTimeBase:
    """
    A common base class for Date Time.

    :author: Andrew J. P. Maclean
    """
    # For 64-bit arithmetic, the relative tolerance of the fraction of the day.
    rel_fod = 1.0e-14
    # For 64-bit arithmetic, the relative tolerance of the minutes in a day.
    rel_minutes = 1.0e-11
    # For 64-bit arithmetic, the relative tolerance of the seconds in a day.
    rel_seconds = 1.0e-9
    # For 64-bit arithmetic, the absolute tolerance of the seconds in a day.
    abs_seconds = 1.0e-10

    # The maximum precision.
    maximum_precision = 15

    def is_close(self, x, y, eps):
        """
        Compare two floating point numbers for equality.

        The numbers are considered to be equal if the absolute difference
        between them is less than a defined precision.

        :param: x The first number.
        :param: y The second number.
        :param: eps The precision.
        :return: True if the numbers are equal, False if not, None if eps is out of range.
        """

        if 0.0 < math.fabs(eps) < 1.0:

            # Handle infinities here.
            if x == y:
                return True

            return math.fabs(x - y) < math.fabs(eps)
        else:
            print(f'|{eps}| is == 0 or >= 1.0')
            return None

    def scale(self, x, precision):
        """
        Scale a number by rounding it to a specified number of decimal places.

        In Python, round(2.675, 2) gives 2.67 instead of the expected 2.68.
        It's a result of the fact that most decimal fractions cannot be
        represented exactly as a float. This function should return 2.68.

        :param: x The number to round.
        :param: precision The number of decimal places to round to.

        :return: The rounded number.
        """
        prec = self.check_precision(precision)
        scaling = 10 ** prec
        rounding = 0.049
        if x < 0:
            rounding = -0.049
        x = x * scaling + rounding
        return round(x) / scaling

    @staticmethod
    def get_exponent(f):
        """
        Get the exponent of a floating point number.

        See: [Decompose a float into mantissa and exponent in base 10 without strings](https://stackoverflow.com/questions/45332056/decompose-a-float-into-mantissa-and-exponent-in-base-10-without-strings)

        :param f: The floating point number.

        :return: The exponent.
        """

        return int(math.floor(math.log10(abs(f)))) if f != 0 else 0

    def get_mantissa(self, f):
        """
        Get the mantissa of a floating point number.

        See: [Decompose a float into mantissa and exponent in base 10 without strings](https://stackoverflow.com/questions/45332056/decompose-a-float-into-mantissa-and-exponent-in-base-10-without-strings)

        :param f: The floating point number.

        :return: The mantissa.
        """

        return f / 10 ** self.get_exponent(f)

    def check_precision(self, precision):
        """
        Clamp the number of decimal places to the range 0 ... maximum_precision.

        :param: precision The number of decimal places.

        :return: A positive value clamped to the range 0 ... maximum_precision.
        """
        if abs(precision) > abs(self.maximum_precision):
            return abs(self.maximum_precision)
        else:
            return abs(precision)
