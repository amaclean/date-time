#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   DayNames.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


def is_day_names(obj):
    """
    Verify that the object is of type DayNames.

    :param: obj The object being tested.
    """
    if not (obj.__class__.__name__ == 'DayNames' and
            hasattr(obj, 'names')):
        raise TypeError('Not a class of type DayNames.')


class DayNames:
    """
    This class holds day names.
    """

    def __init__(self):
        self.names = {1: 'Sunday', 2: 'Monday', 3: 'Tuesday',
                      4: 'Wednesday', 5: 'Thursday', 6: 'Friday',
                      7: 'Saturday'}

    def __str__(self):
        """
        :return: Return the DayNames as a string.
        """
        return self.names

    def __eq__(self, rhs):
        is_day_names(rhs)
        return self.names == rhs.names

    def __ne__(self, rhs):
        is_day_names(rhs)
        return not (self == rhs)

    def get_day_name(self, day):
        """
        Get the day name.

        None is returned if the index of the day
        lies outside the range 1...7.

        :param: day The day number.

        :return: The day name as a string or no_name if not found.
        """
        return self.names.get(day)

    def get_abbreviated_day_name(self, day, size=3):
        """
        Get the abbreviated day name.

        None is returned if the index of the day
        lies outside the range 1...7.

        :param: day The day number.
        :param: size The number of letters used to form the abbreviation.

        :return: The abbreviated day name as a string or no_name if not found.
        """
        dn = self.names.get(day)
        if dn is None:
            return dn
        if len(dn) <= size:
            return dn
        return dn[:size]

    def set_day_name(self, day, day_name):
        """
        Set the day name. Useful when using a different language.

        There are 7 days and the first day starts at 1.
        No updates are performed if the index of the day
        lies outside the range 1...7.

        :param: day The day number.
        :param: day_name The day name.
        """
        if 0 < day < 8:
            self.names[day] = day_name
