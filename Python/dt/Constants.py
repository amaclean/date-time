#!/usr/bin/env python3

from dataclasses import dataclass


# ==========================================================================
#
# Program:   Date Time Library
# File   :   Constants.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================

@dataclass(frozen=True)
class CalendarType:
    """
    Enumerate the calendar to use.

    julian: Proleptic Julian Calendar and Julian Calendar for dates on
    or before 1582 Oct 4, Julian Calendar for dates on or after 1582 Oct 5

    gregorian: Proleptic Gregorian Calendar for dates on or before
    1582 Oct 14, Gregorian Calendar for dates on or after 1582 Oct 15

    julian_gregorian: The Proleptic Julian Calendar and the Julian Calendar
    is used for dates on or before 1582 Oct 4. The Gregorian calendar is
    used for dates on or after 1582 Oct 15.

    """
    julian: int = 0
    gregorian: int = 1
    julian_gregorian: int = 2


@dataclass(frozen=True)
class DayName:
    """
    Used to decide whether to use a day name or not.
    """
    no_name: int = 0
    abbreviated: int = 1
    full_name: int = 2


@dataclass(frozen=True)
class FirstDOW:
    """
    Used to decide whether to use Sunday or Monday as the first day of the week.
    """
    sunday: int = 0
    monday: int = 1


@dataclass(frozen=True)
class JGChangeover:
    """
    The Julian/Gregorian calendar changeover dates.

    - In the Gregorian Calendar.
        - Proleptic Gregorian previous day: Jd=2299150, 1582 10 04.
        - Gregorian first day:              Jd=2299161, 1582 10 15.

    - In the Julian Calendar.
        - Proleptic Gregorian previous day: Jd=2299160, 1582 10 04.

    """
    gregorian_previous_dn: int = 2299150
    gregorian_previous_date: int = 15821004
    gregorian_start_dn: int = 2299161
    gregorian_start_date: int = 15821015
    julian_gregorian_year: int = 1582
    julian_previous_dn: int = 2299160


@dataclass(frozen=True)
class MonthName:
    """
    Used to decide whether to use a month name or not.
    """
    no_name: int = 0
    abbreviated: int = 1
    full_name: int = 2
