#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   HMS.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


from math import isclose

from dt.DateTimeBase import DateTimeBase


class HMS(DateTimeBase):
    """
    This class holds a time as hours, minutes and seconds.
    """

    def __init__(self, *args, **kwargs):
        self.hour = 0
        self.minute = 0
        self.second = 0
        if len(args) == 1:
            if type(args[0]) == type(self):
                self.hour = args[0].hour
                self.minute = args[0].minute
                self.second = args[0].second
                return
            if isinstance(args[0], list):
                if len(args[0]) == 1:
                    self.hour = args[0][0]
                elif len(args[0]) == 2:
                    self.hour = args[0][0]
                    self.minute = args[0][1]
                else:
                    self.hour = args[0][0]
                    self.minute = args[0][1]
                    self.second = args[0][2]
            else:
                self.hour = args[0]
        elif len(args) == 2:
            self.hour = args[0]
            self.minute = args[1]
        elif len(args) == 3:
            self.hour = args[0]
            self.minute = args[1]
            self.second = args[2]
        for k in kwargs:
            if k == 'hour':
                self.hour = kwargs[k]
            elif k == 'minute':
                self.minute = kwargs[k]
            elif k == 'second':
                self.second = kwargs[k]

    def set_hms(self, *args, **kwargs):
        """
        Set the time.
        """
        self.__init__(*args, **kwargs)

    def __str__(self):
        """
        :return: Return the HMS as a string.
        """
        return f'{self.hour:02d}:{self.minute:02d}:{self.second:015.12f}'

    def __eq__(self, rhs):
        self.is_hms(rhs)
        return self.hour == rhs.hour and \
            self.minute == rhs.minute and \
            self.second == rhs.second

    def __ne__(self, rhs):
        self.is_hms(rhs)
        return not (self == rhs)

    def __lt__(self, rhs):
        self.is_hms(rhs)
        if self.hour < rhs.hour:
            return True
        if self.hour == rhs.hour:
            if self.minute < rhs.minute:
                return True
            if self.minute == rhs.minute:
                if self.second < rhs.second:
                    return True
        return False

    def __le__(self, rhs):
        self.is_hms(rhs)
        return not (self > rhs)

    def __gt__(self, rhs):
        self.is_hms(rhs)
        if self.hour > rhs.hour:
            return True
        if self.hour == rhs.hour:
            if self.minute > rhs.minute:
                return True
            if self.minute == rhs.minute:
                if self.second > rhs.second:
                    return True
        return False

    def __ge__(self, rhs):
        self.is_hms(rhs)
        return not (self < rhs)

    def is_close(self, hms, rel_tol=DateTimeBase.rel_seconds, abs_tol=0.0):
        """
        Compare hours, minutes and seconds for equality.

        :param: d The Calendar Date to be compared with this.
        :param: rel_tol Relative tolerance.
        :param: abs_tol Absolute tolerance.
        :return: True if the dates are equivalent.
        """
        self.is_hms(hms)
        if self == hms:
            return True
        else:
            return self.hour == hms.hour and self.minute == hms.minute and isclose(self.second, hms.second,
                                                                                   rel_tol=rel_tol, abs_tol=abs_tol)

    @staticmethod
    def is_hms(obj):
        """
        Verify that the object is of type HMS.

        :param: obj The object being tested.
        """
        if not (obj.__class__.__name__ == 'HMS' and
                hasattr(obj, 'hour') and hasattr(obj, 'minute') and
                hasattr(obj, 'second')):
            raise TypeError('Not a class of type HMS.')

    @staticmethod
    def valid_hms(obj):
        """
        Verify that the object is of type HMS.

        :param: obj The object being tested.
        :return: true if it is a valid object, false otherwise.
        """
        return (obj.__class__.__name__ == 'HMS' and
                hasattr(obj, 'hour') and hasattr(obj, 'minute') and
                hasattr(obj, 'second'))
