#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   TimeInterval.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


import math
import numbers
from math import isclose

from dt.DateTimeBase import DateTimeBase


class TimeInterval(DateTimeBase):
    """
    This class holds a time interval as days and fractions of a day.
    """

    def __init__(self, *args, **kwargs):
        self.delta_day = 0
        self.delta_fod = 0
        if len(args) == 1:
            if type(args[0]) == type(self):
                self.delta_day = args[0].delta_day
                self.delta_fod = args[0].delta_fod
                self.normalise()
                return
            if isinstance(args[0], list):
                if len(args[0]) == 1:
                    self.delta_day = args[0][0]
                elif len(args[0]) == 2:
                    self.delta_day = args[0][0]
                    self.delta_fod = args[0][1]
            else:
                self.delta_day = args[0]
        elif len(args) == 2:
            self.delta_day = args[0]
            self.delta_fod = args[1]
        for k in kwargs:
            if k == 'delta_day':
                self.delta_day = kwargs[k]
            elif k == 'delta_fod':
                self.delta_fod = kwargs[k]
        self.normalise()

    def set_time_interval(self, *args, **kwargs):
        """
        Set the time interval.
        """
        self.__init__(*args, **kwargs)

    def __str__(self):
        """
        :return: Return the TimeInterval as a string.
        """
        delta_day = self.delta_day
        delta_fod = self.delta_fod

        is_negative = False
        if delta_day < 0:
            is_negative = True
        elif delta_fod < 0:
            is_negative = True
        if is_negative:
            sgn = '-'
        else:
            sgn = ' '

        fod = f'{math.fabs(delta_fod):19.15f}'.strip()
        fod = fod.split('.')[1]
        return f'{sgn:s}{abs(delta_day):d}.{fod:s}'

    def __eq__(self, rhs):
        self.is_time_interval(rhs)
        return self.delta_day == rhs.delta_day \
            and self.delta_fod == rhs.delta_fod

    def __ne__(self, rhs):
        self.is_time_interval(rhs)
        return not (self == rhs)

    def __lt__(self, rhs):
        self.is_time_interval(rhs)
        if self.delta_day < rhs.delta_day:
            return True
        if self.delta_day == rhs.delta_day:
            if self.delta_fod < rhs.delta_fod:
                return True
        return False

    def __le__(self, rhs):
        self.is_time_interval(rhs)
        return not (self > rhs)

    def __gt__(self, rhs):
        self.is_time_interval(rhs)
        if self.delta_day > rhs.delta_day:
            return True
        if self.delta_day == rhs.delta_day:
            if self.delta_fod > rhs.delta_fod:
                return True
        return False

    def __ge__(self, rhs):
        self.is_time_interval(rhs)
        return not (self < rhs)

    def is_close(self, ti, rel_tol=DateTimeBase.rel_fod, abs_tol=0.0):
        """
        Compare time intervals for equality.

        :param: ti The time interval to be compared with this.
        :param: eps The precision.
        :return: True if the dates are equivalent.
        """
        self.is_time_interval(ti)
        if self == ti:
            return True
        else:
            return self.delta_day == ti.delta_day and isclose(self.delta_fod, ti.delta_fod, rel_tol=rel_tol,
                                                              abs_tol=abs_tol)

    def __add__(self, rhs):
        jd = self
        if self.valid_time_interval(rhs):
            jd.delta_day += rhs.delta_day
            jd.delta_fod += rhs.delta_fod
        else:
            if isinstance(rhs, int) \
                    or isinstance(rhs, float) \
                    or isinstance(rhs, numbers.Integral):
                # We are expecting a float corresponding to the days.
                jd.delta_day += int(rhs)
                jd.delta_fod += (rhs - int(rhs))
            else:
                raise TypeError('Not a class of type TimeInterval or float.')
        jd.normalise()
        return jd

    def __iadd__(self, rhs):
        if self.valid_time_interval(rhs):
            self.delta_day += rhs.delta_day
            self.delta_fod += rhs.delta_fod
        else:
            if isinstance(rhs, int) \
                    or isinstance(rhs, float) \
                    or isinstance(rhs, numbers.Integral):
                # We are expecting a float corresponding to the days.
                self.delta_day += int(rhs)
                self.delta_fod += (rhs - int(rhs))
            else:
                raise TypeError('Not a class of type TimeInterval or float.')
        self.normalise()
        return self

    def __sub__(self, rhs):
        jd = self
        if self.valid_time_interval(rhs):
            jd.delta_day -= rhs.delta_day
            jd.delta_fod -= rhs.delta_fod
        else:
            if isinstance(rhs, int) \
                    or isinstance(rhs, float) \
                    or isinstance(rhs, numbers.Integral):
                # We are expecting a float corresponding to the days.
                jd.delta_day -= int(rhs)
                jd.delta_fod -= (rhs - int(rhs))
            else:
                raise TypeError('Not a class of type TimeInterval or float.')
        jd.normalise()
        return jd

    def __isub__(self, rhs):
        if self.valid_time_interval(rhs):
            self.delta_day -= rhs.delta_day
            self.delta_fod -= rhs.delta_fod
        else:
            if isinstance(rhs, int) \
                    or isinstance(rhs, float) \
                    or isinstance(rhs, numbers.Integral):
                # We are expecting a float corresponding to the days.
                self.delta_day -= int(rhs)
                self.delta_fod -= (rhs - int(rhs))
            else:
                raise TypeError('Not a class of type TimeInterval or float.')
        self.normalise()
        return self

    def normalise(self):
        """
        Normalise so that the fraction of the day is > -1 and < 1.
        """
        while self.delta_fod >= 1:
            self.delta_fod -= 1
            self.delta_day += 1
        while self.delta_fod <= -1:
            self.delta_fod += 1
            self.delta_day -= 1
        # Normalise so that the sign is the same for jdn and fod.
        tmp = int(self.delta_day + self.delta_fod)
        self.delta_fod = self.delta_day - tmp + self.delta_fod
        self.delta_day = tmp

    def get_delta_day(self):
        """
        Get the time difference days.

        :return:  The days.
        """
        return self.delta_day

    def get_delta_fod(self):
        """
        Get the time difference fraction of the day.

        :return: The fraction of the Day
        """
        return self.delta_fod

    def get_time_interval_double(self):
        """
        Get the Julian Date as days and decimals of the day.

        The precision of the returned value will be of the order
        of 1.0e-8 of a day or 1ms.

        :return: The Julian Day Number of the date as days and decimals of the day.
        """
        return self.delta_day + self.delta_fod

    def set_delta_day(self, delta_day):
        """
        Set the delta_day of the time interval.

        :param: delta_day The days.
        """
        self.delta_day = delta_day
        self.normalise()

    def set_delta_fod(self, delta_fod):
        """
        Set the delta_fod of the time interval.

        :param: delta_fod The days.
        """
        self.delta_fod = delta_fod
        self.normalise()

    @staticmethod
    def is_time_interval(obj):
        """
        Verify that the object is of type TimeInterval.

        :param: obj The object being tested.
        """
        if not (obj.__class__.__name__ == 'TimeInterval' and
                hasattr(obj, 'delta_day') and hasattr(obj, 'delta_fod')):
            raise TypeError('Not a class of type TimeInterval.')

    @staticmethod
    def valid_time_interval(obj):
        """
        Verify that the object is of type TimeInterval.

        :param: obj The object being tested.
        :return: true if it is a valid object, false otherwise.
        """
        return (obj.__class__.__name__ == 'TimeInterval' and
                hasattr(obj, 'delta_day') and hasattr(obj, 'delta_fod'))
