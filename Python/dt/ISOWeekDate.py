#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   ISOWeekDate.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


class ISOWeekDate:
    """
    This class holds an ISO Week date time as years, week_number and week_day.
    """

    def __init__(self, *args, **kwargs):
        self.year = 0
        self.week_number = 0
        self.week_day = 0
        if len(args) == 1:
            if type(args[0]) == type(self):
                self.year = args[0].year
                self.week_number = args[0].week_number
                self.week_day = args[0].week_day
                return
            if isinstance(args[0], list):
                if len(args[0]) == 1:
                    self.year = args[0][0]
                elif len(args[0]) == 2:
                    self.year = args[0][0]
                    self.week_number = args[0][1]
                else:
                    self.year = args[0][0]
                    self.week_number = args[0][1]
                    self.week_day = args[0][2]
            else:
                self.year = args[0]
        elif len(args) == 2:
            self.year = args[0]
            self.week_number = args[1]
        elif len(args) == 3:
            self.year = args[0]
            self.week_number = args[1]
            self.week_day = args[2]
        for k in kwargs:
            if k == 'year':
                self.year = kwargs[k]
            elif k == 'week_number':
                self.week_number = kwargs[k]
            elif k == 'week_day':
                self.week_day = kwargs[k]

    def set_iso_week_date(self, *args, **kwargs):
        """
        Set the ISO Week Date.
        """
        self.__init__(*args, **kwargs)

    def __str__(self):
        """
        :return: Return the ISOWeekDate as a string.
        """
        return f'{self.year:02d}-{self.week_number:02d}-{self.week_day:01d}'

    def __eq__(self, rhs):
        self.iso_week_date(rhs)
        return self.year == rhs.year and \
            self.week_number == rhs.week_number and \
            self.week_day == rhs.week_day

    def __ne__(self, rhs):
        self.iso_week_date(rhs)
        return not (self == rhs)

    def __lt__(self, rhs):
        self.iso_week_date(rhs)
        if self.year < rhs.year:
            return True
        if self.year == rhs.year:
            if self.week_number < rhs.week_number:
                return True
            if self.week_number == rhs.week_number:
                if self.week_day < rhs.week_day:
                    return True
        return False

    def __le__(self, rhs):
        self.iso_week_date(rhs)
        return not (self > rhs)

    def __gt__(self, rhs):
        self.iso_week_date(rhs)
        if self.year > rhs.year:
            return True
        if self.year == rhs.year:
            if self.week_number > rhs.week_number:
                return True
            if self.week_number == rhs.week_number:
                if self.week_day > rhs.week_day:
                    return True
        return False

    def __ge__(self, rhs):
        self.iso_week_date(rhs)
        return not (self < rhs)

    @staticmethod
    def iso_week_date(obj):
        """
        Verify that the object is of type ISOWeekDate.

        :param: obj The object being tested.
        """
        if not (obj.__class__.__name__ == 'ISOWeekDate' and
                hasattr(obj, 'year') and hasattr(obj, 'week_number') and
                hasattr(obj, 'week_day')):
            raise TypeError('Not a class of type ISOWeekDate.')
