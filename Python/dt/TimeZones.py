#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   TimeZones.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


def is_time_zones(obj):
    """
    Verify that the object is of type TimeZones.

    :param: obj The object being tested.
    """
    if not (obj.__class__.__name__ == 'TimeZones' and
            hasattr(obj, 'tzNames') and hasattr(obj, 'tzTimes')):
        raise TypeError('Not a class of type TimeZones.')


class TimeZones:
    """
    This class provides a convenient way of accessing time zones.

    Two maps are provided, one keyed by the time zone name and an
    inverse mapping keyed by the difference in seconds from UTC 0.

    See: http://en.wikipedia.org/wiki/List_of_time_zones_by_UTC_offset#UTC.E2.88.9209:30.2C_V.E2.80.A0

    :author: Andrew J. P. Maclean
    """

    def __init__(self):
        # The time zones indexed by name, Time Zone name: difference in seconds from UT 0.
        self.tzNames = dict()
        # The time zones indexed by time, difference in seconds from UT 0: Time Zone name.
        self.tzTimes = dict()

        self.tzNames['UT-12'] = -12 * 3600
        self.tzNames['UT-11'] = -11 * 3600
        self.tzNames['UT-10'] = -10 * 3600
        self.tzNames['UT-09:30'] = int(-9.5 * 3600)
        self.tzNames['UT-09'] = -9 * 3600
        self.tzNames['UT-08'] = -8 * 3600
        self.tzNames['UT-07'] = -7 * 3600
        self.tzNames['UT-06'] = -6 * 3600
        self.tzNames['UT-05'] = -5 * 3600
        self.tzNames['UT-04:30'] = int(-4.5 * 3600)
        self.tzNames['UT-04'] = -4 * 3600
        self.tzNames['UT-03:30'] = int(-3.5 * 3600)
        self.tzNames['UT-03'] = -3 * 3600
        self.tzNames['UT-02'] = -2 * 3600
        self.tzNames['UT-01'] = -1 * 3600
        self.tzNames['UT'] = 0 * 3600
        self.tzNames['UT+01'] = 1 * 3600
        self.tzNames['UT+02'] = 2 * 3600
        self.tzNames['UT+03'] = 3 * 3600
        self.tzNames['UT+03:30'] = int(3.5 * 3600)
        self.tzNames['UT+04'] = 4 * 3600
        self.tzNames['UT+04:30'] = int(4.5 * 3600)
        self.tzNames['UT+05'] = 5 * 3600
        self.tzNames['UT+05:30'] = int(5.5 * 3600)
        self.tzNames['UT+05:45'] = int(5.75 * 3600)
        self.tzNames['UT+06'] = 6 * 3600
        self.tzNames['UT+06:30'] = int(6.5 * 3600)
        self.tzNames['UT+07'] = 7 * 3600
        self.tzNames['UT+08'] = 8 * 3600
        self.tzNames['UT+08:45'] = int(8.75 * 3600)
        self.tzNames['UT+09'] = 9 * 3600
        self.tzNames['UT+09:30'] = int(9.5 * 3600)
        self.tzNames['UT+10'] = 10 * 3600
        self.tzNames['UT+10:30'] = int(10.5 * 3600)
        self.tzNames['UT+11'] = 11 * 3600
        self.tzNames['UT+11:30'] = int(11.5 * 3600)
        self.tzNames['UT+12'] = 12 * 3600
        self.tzNames['UT+12:45'] = int(12.75 * 3600)
        self.tzNames['UT+13'] = 13 * 3600
        self.tzNames['UT+14'] = 14 * 3600

        for key, value in list(self.tzNames.items()):
            self.tzTimes[value] = key

    def __eq__(self, rhs):
        is_time_zones(rhs)
        return ((self.tzNames == rhs.tzNames) and
                (self.tzTimes == rhs.tzTimes))

    def __ne__(self, rhs):
        is_time_zones(rhs)
        return not (self == rhs)

    def get_time_zones_by_name(self):
        """
        Get the time zones.

        :return: A dictionary of time zones indexed by Time Zone name.
        """
        return self.tzNames

    def get_time_zones_by_value(self):
        """
        Get the time zones by time difference from UTC.

        :return: A dictionary of time zones indexed by time difference
                    in seconds from UT 0.
        """
        return self.tzTimes

    def get_time_zone_name(self, offset):
        """
        Get the name of the time zone corresponding to a given
        offset in seconds.

        :param: offset - the time zone offset in seconds.

        :return: The time zone corresponding
                to the given offset, None otherwise.
        """

        return self.tzTimes.get(offset)

    def get_time_zone_value(self, tz_name):
        """
        Get the value of the time zone corresponding to a given
        time zone name.

        :param: tz_name - the name of the time zone.

        :return: The offset corresponding
                to the given time zone name, None otherwise.

        """

        return self.tzNames.get(tz_name)
