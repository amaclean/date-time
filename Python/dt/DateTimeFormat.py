# ==========================================================================
#
# Program:   Date Time Library
# File   :   DateTimeFormat.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================

from dataclasses import dataclass

from dt.Constants import CalendarType, DayName, FirstDOW, MonthName


@dataclass
class DateTimeFormat:
    """
    This class holds the parameters used for formatting dates and times.

        :param: calendar The calendar to use.
        :param: precision_seconds The precision of the seconds.
        :param: precision_fod The precision of the fraction of the day.
        :param: day_name Specify whether the day name is required.
        :param: day_abbreviation_length The size of the abbreviation for a day, usually 3.
        :param: first_dow The first day of the week, either Sunday or Monday.
        :param: month_name Specify whether the month name is required.
        :param: month_abbreviation_length The size of the abbreviation for a month, usually 3.
        :param: date_sep The date separator, usually '-'.
        :param: time_sep The time separator, usually ':'.
        :param: date_time_sep The separator between the date and time, usually ' '.
        :param: day_time_sep The separator between the day and time in a time interval, usually ' '.
        
    """

    calendar: int = CalendarType.julian_gregorian
    precision_seconds: int = 3
    precision_fod: int = 8
    day_name: int = DayName.no_name
    day_abbreviation_length: int = 3
    first_dow: int = FirstDOW.sunday
    month_name: int = MonthName.no_name
    month_abbreviation_length: int = 3
    date_sep: str = '-'
    time_sep: str = ':'
    date_time_sep: str = ' '
    day_time_sep: str = ' '
