#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   DateConversions.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


from dt.Constants import CalendarType, FirstDOW, JGChangeover
from dt.ISOWeekDate import ISOWeekDate
from dt.YMD import YMD


class DateConversions:
    """
    This class provides conversion routines between Calendrical dates and times and the date and time as a Julian Day Number (JDN) and fraction of the day (FoD).

    The Julian Day is defined as the interval of time in days and fractions of a day since 4713 B.C. January 1, Greenwich noon Julian proleptic calendar.

    The Julian Day Number is the integer portion of the Julian Day and JD 0 will be equivalent to 4713 B.C. January 1, Greenwich noon Julian proleptic calendar, a Monday or equivalently -4712-01-01 12:00:00.

    The algorithms defined here are known to work over the range:
    -13200-AUG-15 00:00  JD  -3100015.50 to  17191-MAR-15 00:00  JD   8000016.50

    For further details see: 'Explanatory Supplement to the Astronomical Almanac', Ed. P. Kennith Seidelmann, University Science Books, 1992.

    Function names that start with a lower case letter indicate conversion methods.

    The conversion methods generally follow this pattern:

     [jd|cd]_to_[cd|jd]_[j|g|jg] where

      jd - julian date, cd - calendrical date, j- Julian Calendar, g - Gregorian Calendar, jg - Julian-Gregorian Calendar.

     [h|hm|hms]_to_[h|hm[hms] where

      h - hours, hm - hours and minutes, hms - hours minutes and seconds.

    Calendrical dates are treated list ymd = [Y,M,D] or ymdhms = [Y,M,D,H,M,S] (if hours minutes and seconds are needed),
    Julian dates are treated as a variable called jd or a list [jd, fod].
    ISO Week Dates are treated as a list [week_year, week, weekday].

    :author: Andrew J. P. Maclean
    """

    @staticmethod
    def is_leap_year(year, calendar):
        """
        Test for a leap year.

        :param: year Year
        :param: calendar The calendar to use, one of:

        - julian - Julian Calendar
        - gregorian - Gregorian Calendar
        - julian_gregorian - Julian/Gregorian Calendar

        :return: true if it is a leap year.
        """
        y = int(year)
        if calendar == CalendarType.julian:
            # If the years are greater than 1582 then this is the Julian Proleptic calendar.
            res = (y % 4) == 0
        elif calendar == CalendarType.gregorian:
            # If the years are less than 1582 then this is the Gregorian Proleptic calendar.
            if (y % 100) == 0:
                res = (y % 400) == 0  # Gregorian century leap year.
            else:
                res = (y % 4) == 0
        else:  # default is JULIAN_GREGORIAN
            if year < 1582:  # Julian calendar.
                res = (y % 4) == 0
            else:
                # Gregorian calendar.
                if (y % 100) == 0:
                    res = (y % 400) == 0  # Gregorian century leap year.
                else:
                    res = (y % 4) == 0
        return res

    @staticmethod
    def cd_to_jd_g(datetime):
        """
        Calculate the Julian day number of the date.
        The Proleptic Gregorian Calendar is used.

        Not valid for ymd < -4713 12 25.

        :param: datetime  A YMD object or an object that can be converted to a YMD object.
        :return: The Julian day number of the date, n integer running from noon to noon.
        """
        ymd = YMD(datetime)
        year = ymd.year
        month = ymd.month
        day = ymd.day
        # Note: Rounding is towards zero.
        a = int(1461 * (year + 4800 + int((month - 14) / 12.0)) / 4.0)
        b = int(367 * (month - 2 - 12 * int((month - 14) / 12.0)) / 12.0)
        c = int(3 * int((year + 4900 + int((month - 14) / 12.0)) / 100) / 4.0)
        d = day - 32075
        return a + b - c + d

    @staticmethod
    def jd_to_cd_g(juliandate):
        """
        Calculate the date from the Julian day number.
        The Proleptic Gregorian Calendar is used.

        Not valid for jd < -0.5.

        :param: jd The julian day number, an integer running from noon to noon.
        :return: A YMD object.
        """
        # Note: Rounding is towards zero.
        l = int(juliandate) + 68569
        n = int((4 * l) / 146097.0)
        l -= int((146097 * n + 3) / 4.0)
        i = int((4000 * (l + 1)) / 1461001.0)
        l = l - int(((1461 * i) / 4.0)) + 31
        j = int((80 * l) / 2447.0)
        day = int(l - int((2447 * j) / 80.0))
        l = int(j / 11.0)
        month = int(j + 2 - 12 * l)
        year = int(100 * (n - 49) + i + l)
        return YMD(year, month, day)

    @staticmethod
    def cd_to_jd_j(date):
        """
        Calculate the Julian day number of the date.
        The Proleptic Julian Calendar is used.

        :param: date  A YMD object or an object that can be converted to a YMD object.
        :return: The Julian day number of the date, an integer running from noon to noon.
        """
        ymd = YMD(date)
        year = int(ymd.year)
        month = int(ymd.month)
        day = int(ymd.day)
        # Note: Rounding is towards zero.
        if year > -4723:
            a = 367 * year
            b = int(7 * (year + 5001 + int((month - 9) / 7.0)) / 4.0)
            c = int((275 * month) / 9.0) + day + 1729777
            res = a - b + c
            return res
        else:
            # Handle dates < 4712-01-01
            # We add 4712 * 3 = 14136 years to the year, do the calculations and
            #  then subtract 4712 * 3 * 365.25 = 5163174 days to get the correct julian day number.
            a = 367 * (year + 14136)
            b = int(7 * (year + 14136.0 + 5001 + int((month - 9) / 7.0)) / 4.0)
            c = int((275 * month) / 9.0) + day + 1729777
            res = a - b + c - 5163174
            return res

    @staticmethod
    def jd_to_cd_j(jd):
        """
        Calculate the date from the Julian day number.
        The Proleptic Julian Calendar is used.

        :param: jd The julian day number, an integer running from noon to noon.
        :return: A YMD object.
        """
        # Note: Rounding is towards zero.
        j = int(jd) + 1402
        if jd < 0:
            # Set an origin for j so that j is positive.
            # This corresponds to -14136-01-01
            j += 5163174  # 4712 * 3 * 365.25 days
        k = int((j - 1) / 1461.0)
        l = j - 1461 * k
        n = int((l - 1) / 365.0) - int(l / 1461.0)
        i = l - 365 * n + 30
        j = int((80 * i) / 2447.0)
        day = i - int((2447 * j) / 80.0)
        i = int(j / 11.0)
        month = j + 2 - 12 * i
        year = 4 * k + n + i - 4716
        if jd < 0:
            year -= 14136  # 4712 * 3 years
        return YMD(year, month, day)

    def cd_to_jd_jg(self, date):
        """
        Calculate the Julian day number of the date.

        The Julian and Proleptic Julian Calendar is used for dates on or before 1582 Oct 4.
        The Gregorian calendar is used for dates on or after 1582 Oct 15.

        :param: date  A YMD object or an object that can be converted to a YMD object.
        :return: The Julian day number of the date, an integer running from noon to noon.
        """
        ymd = YMD(date)
        year = int(ymd.year)
        month = int(ymd.month)
        day = int(ymd.day)
        date = year * 10000 + month * 100 + day
        if date < JGChangeover.gregorian_start_date:
            return self.cd_to_jd_j(ymd)
        return self.cd_to_jd_g(ymd)

    def jd_to_cd_jg(self, jd):
        """
        Calculate the date from the Julian day number.

        The Proleptic Julian Calendar is used.

        :param: jd The julian day number, an integer running from noon to noon.
        :return: A YMD object.
        """
        j = int(jd)
        if j < JGChangeover.gregorian_start_dn:
            return self.jd_to_cd_j(j)
        return self.jd_to_cd_g(j)

    # ************ Day of the week or year.

    @staticmethod
    def dow(jd, first_dow):
        """
         Calculate the day of the week.

         This function assumes that the Proleptic Julian Calendar and the Julian Calendar is used for dates on or before 1582 Oct 4 and the Gregorian calendar is used for dates on or after 1582 Oct 15.

         :param: jd Julian day number of the date, an integer running from noon to noon.
         :param: first_dow The first day of the week, either Sunday or Monday.
         :return: The day of the week, where 1=Sunday by default.
        """
        if first_dow == FirstDOW.sunday:
            return (jd + 1) % 7 + 1
        else:
            return jd % 7 + 1

    def dow_ymd(self, date, calendar, firstdow):
        """
        Calculate the day of the week.

        You should use the Proleptic Julian Calendar and the Julian Calendar
        for dates on or before 1582 Oct 4 and the Gregorian calendar
        for dates on or after 1582 Oct 15.

        :param: ymd A list [year, month, day].
        :param: calendar This parameter specifies the calendar to use, one of:

        - JULIAN - Julian Calendar
        - GREGORIAN - Gregorian Calendar
        - JULIAN_GREGORIAN - Julian/Gregorian Calendar

        :param: first_dow The first day of the week, either Sunday or Monday.
        :return: The day of the week, where 1=Sunday by default.
        """
        ymd = YMD(date)
        if calendar == CalendarType.julian:
            jd = self.cd_to_jd_j(ymd)
        elif calendar == CalendarType.gregorian:
            jd = self.cd_to_jd_g(ymd)
        else:  # default is julian_gregorian
            jd = self.cd_to_jd_jg(ymd)
        return self.dow(jd, firstdow)

    def doy_j(self, date):
        """
        Calculate the day of the year.

        The Proleptic Julian Calendar is used.

        :param: date  A YMD object or an object that can be converted to a YMD object.
        :return: The day of the year.
        """
        ymd = YMD(date)
        jd0 = self.cd_to_jd_j([ymd.year, 1, 1])
        jd1 = self.cd_to_jd_j(ymd)
        return int(jd1 - jd0 + 1)

    def doy_g(self, date):
        """
        Calculate the day of the year.

        The Proleptic Gregorian Calendar is used.

        :param: date  A YMD object or an object that can be converted to a YMD object.
        :return: The day of the year.

        """
        ymd = YMD(date)
        jd0 = self.cd_to_jd_g([ymd.year, 1, 1])
        jd1 = self.cd_to_jd_g(ymd)
        return int(jd1 - jd0 + 1)

    def doy_jg(self, date):
        """
        Calculate the day of the year.

        The Julian and Proleptic Julian Calendar is used for dates on or before 1582 Oct 4 .
        The Gregorian calendar is used for dates on or after 1582 Oct 15.

        :param: date  A YMD object or an object that can be converted to a YMD object.
        :return: The day of the year.
        """
        ymd = YMD(date)
        jd0 = self.cd_to_jd_jg([ymd.year, 1, 1])
        jd1 = self.cd_to_jd_jg(ymd)
        return int(jd1 - jd0 + 1)

    def cd_to_woy(self, date):
        """
        Get the week of the year.
        See: http://en.wikipedia.org/wiki/ISO_week_date

        :param: date  A YMD object or an object that can be converted to a YMD object.
        :return: An ISOWeekDate

        Note: This algorithm only works for Gregorian dates.
        """
        calendar = 'JULIAN_GREGORIAN'
        ymd = YMD(date)
        doy = self.doy_g(ymd)
        # For this algorithm: 1 = Monday ... 7 = Sunday.
        weekday = self.dow_ymd(ymd, calendar, FirstDOW.monday)
        y = int(ymd.year)
        m = int(ymd.month)
        d = int(ymd.day)
        wd = weekday
        doy0 = self.doy_g([y, 1, 4])
        doy1 = self.doy_g([y, 12, 28])
        if doy0 <= doy <= doy1:
            week = int((doy - wd + 10) / 7)
            week_year = y
            return ISOWeekDate(week_year, week, weekday)
        # Handle cases relating to the first or last week of the year.
        wd = self.dow_ymd([y - 1, 12, 31], calendar, FirstDOW.monday)
        # If 31 December is on a Monday, Tuesday, or Wednesday,
        # it is in week 01 of the next year, otherwise in week 52 or 53.
        if doy < doy0 and wd >= 4:
            # Calculate the monday of the first week of the year.
            d = d - weekday + 1
            if d <= 0:
                # December of the previous year.
                d += 31
                m = 12
                y -= 1
            doy = self.doy_g([y, m, d])
            wd = self.dow_ymd([y, m, d], calendar, FirstDOW.monday)
            week = int((doy - wd + 10) / 7)
            week_year = y
            return ISOWeekDate(week_year, week, weekday)
        # If 31 Dec is on a Monday Tuesday or Wednesday.
        wd = self.dow_ymd([y, 12, 31], calendar, FirstDOW.monday)
        if doy > doy1 and wd < 4:
            if weekday > 3:
                week = int((doy - weekday + 10) / 7)
                week_year = y
                return ISOWeekDate(week_year, week, weekday)
            else:
                week = 1
                week_year = y + 1
                return ISOWeekDate(week_year, week, weekday)
        week = int((doy - wd + 10) / 7)
        week_year = y
        return ISOWeekDate(week_year, week, weekday)
