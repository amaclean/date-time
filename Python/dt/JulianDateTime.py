#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   JulianDateTime.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


import numbers

from dt.Constants import CalendarType, JGChangeover
from dt.DateConversions import DateConversions
from dt.HMS import HMS
from dt.JD import JD
from dt.TimeConversions import TimeConversions
from dt.TimeInterval import TimeInterval
from dt.YMD import YMD
from dt.YMDHMS import YMDHMS


class JulianDateTime(DateConversions, TimeConversions):
    """
    This class stores the date and time as a Julian Date split into an integral
    and fractional part for maximum precision.

    Operators are provided for comparisons, difference arithmetic and
    incrementing and decrementing dates.

     Default value corresponds to -4712-01-01 00:00:00 = JD0

    The args parameter is one of JulianDateTime, JD or[jdn, fod] or
       [year, month, day, hour, minute, second, calendar].
       If calendar is omitted than JULIAN_GREGORIAN is assumed.
    The kwargs parameter is one of jdt=[jdn, fod] or jdn=xx, fod=yy.y or
       year=y, month=m, day=d, hour=h, minute=m, second=s

    Where:

    - jdn is the Julian day number corresponding to noon of the date.
    - fod is the fraction of the day running from noon to noon.

    **Note**:
        The constructor provides several options for constructing the
        JulianDateTime object.
        If there are no parameters or if it cannot process the parameters,
        it will attempt to construct a default JulianDateTime object for the
        date -4712-01-01 12:00:00 = JD0


    :author: Andrew J. P. Maclean
    """

    def __init__(self, *args, **kwargs):
        self.jd = JD()
        if len(args) == 1:
            if type(args[0]) == type(self):
                self.jd.set_jd(args[0].jd)
            elif isinstance(args[0], list):
                if len(args[0]) < 3:
                    self.jd = JD(args[0])
                elif len(args[0]) == 6:
                    self.set_date_time(YMDHMS(list(args[0])))
                elif len(args[0]) == 7:
                    self.set_date_time(YMDHMS(list(args[0][:6])), args[0][6])
            else:
                self.jd = JD(args[0])
        elif len(args) == 2:
            if isinstance(args[0], list) and isinstance(args[1], int):
                self.set_date_time(args[0], args[1])
            elif YMDHMS().valid_ymdhms(args[0]) and isinstance(args[1], int):
                self.set_date_time(args[0], args[1])
            else:
                self.jd = JD(list(args))
        elif len(args) == 6:
            self.set_date_time(YMDHMS(list(args)))
        elif len(args) == 7:
            self.set_date_time(YMDHMS(list(args[:6])), args[6])
        elif len(kwargs) > 0:
            jdate = [0, 0]
            date = [-4712, 1, 1, 12, 0, 0]
            calendar = CalendarType.julian_gregorian
            has_jd = False
            has_ymdhms = False
            has_calendar = False
            got_jdt = False
            for k in kwargs:
                if k == 'jdt' and type(kwargs[k]) == type(self):
                    self.jd = kwargs[k].jd
                    got_jdt = True
                    break
                elif k == 'jdn':
                    jdate[0] = kwargs[k]
                    has_jd |= True
                elif k == 'fod':
                    jdate[1] = kwargs[k]
                    has_jd |= True
                elif k == 'ymdhms':
                    jdate[0] = kwargs[k]
                    has_ymdhms |= True
                if k == 'year':
                    date[0] = kwargs[k]
                elif k == 'month':
                    date[1] = kwargs[k]
                elif k == 'day':
                    date[2] = kwargs[k]
                elif k == 'hour':
                    date[3] = kwargs[k]
                elif k == 'minute':
                    date[4] = kwargs[k]
                elif k == 'second':
                    date[5] = kwargs[k]
                elif k == 'calendar':
                    calendar = kwargs[k]
                    has_calendar = True
            if has_jd:
                self.jd.set_jd(jdate)
            elif has_ymdhms and has_calendar:
                self.set_date_time(jdate[0], calendar)
            elif not got_jdt:
                self.set_date_time(YMDHMS(date), calendar)

    def set_julian_date_time(self, *args, **kwargs):
        """
        Set the date and time.
        """
        self.__init__(*args, **kwargs)

    def __str__(self):
        """
        :return: Return the JulianDateTime as Julian Date and Fraction of the Day as a string.
        """
        return str(self.jd)

    # Note: All these comparison operators assume that the objects
    # being compared have been normalised.

    def __eq__(self, rhs):
        self.is_julian_date_time(rhs)
        return self.jd == rhs.jd

    def __ne__(self, rhs):
        self.is_julian_date_time(rhs)
        return not (self == rhs)

    def __lt__(self, rhs):
        self.is_julian_date_time(rhs)
        return self.jd < rhs.jd

    def __gt__(self, rhs):
        self.is_julian_date_time(rhs)
        return self.jd > rhs.jd

    def __ge__(self, rhs):
        self.is_julian_date_time(rhs)
        return not (self < rhs)

    def __le__(self, rhs):
        self.is_julian_date_time(rhs)
        return not (self > rhs)

    def is_close(self, jdt, eps):
        """
        Compare Julian Date Times for equality.

        :param: jdt The Julian Date Time to be compared with this.
        :param: eps The precision.
        :return: True if the dates are equivalent.
        """
        self.is_julian_date_time(jdt)
        if self == jdt:
            return True
        else:
            jd = jdt.jd
            return JD.is_close(self.jd, jd, eps)

    def __add__(self, rhs):
        jdt = self
        if self.valid_julian_date_time(rhs) or self.jd.valid_jd(rhs):
            jdt.jd += rhs.jd
        elif isinstance(rhs, int) \
                or isinstance(rhs, float) \
                or isinstance(rhs, numbers.Integral):
            # We are expecting a float corresponding to the days.
            jdt.jd += rhs
        else:
            raise TypeError('Not a class of type JulianDateTime or float.')
        return jdt

    def __iadd__(self, rhs):
        if self.valid_julian_date_time(rhs) or self.jd.valid_jd(rhs):
            self.jd += rhs.jd
        elif isinstance(rhs, int) \
                or isinstance(rhs, float) \
                or isinstance(rhs, numbers.Integral):
            # We are expecting a float corresponding to the days.
            self.jd += rhs
        else:
            raise TypeError('Not a class of type JulianDateTime or float.')
        return self

    def __sub__(self, rhs):
        jdt = self
        if self.valid_julian_date_time(rhs) or self.jd.valid_jd(rhs):
            jdt.jd -= rhs.jd
        elif isinstance(rhs, int) \
                or isinstance(rhs, float) \
                or isinstance(rhs, numbers.Integral):
            # We are expecting a float corresponding to the days.
            jdt.jd -= rhs
        else:
            raise TypeError('Not a class of type JulianDateTime or float.')
        return jdt

    def __isub__(self, rhs):
        if self.valid_julian_date_time(rhs) or self.jd.valid_jd(rhs):
            self.jd -= rhs.jd
        elif isinstance(rhs, int) \
                or isinstance(rhs, float) \
                or isinstance(rhs, numbers.Integral):
            # We are expecting a float corresponding to the days.
            self.jd -= rhs
        else:
            raise TypeError('Not a class of type JulianDateTime or float.')
        return self

    def get_jd_full_precision(self):
        """
        Get the Julian Date to its full precision.

        The precision of the returned value will be of the order of
         1.0e-14 of a day or 1ns.

        :return: An object of type JD.
        """
        return self.jd

    def get_jd_double(self):
        """
        Get the Julian Date.

        This a single double precision number corresponding to the stored Julian Date.

        The precision of the returned value will be of the order of
         1.0e-8 of a day or 1ms.

        :return: The Julian Date.
        """
        return self.jd.get_jd_double()

    def get_jdn(self):
        """
        Get the Julian Day Number.

        :return: Julian Day Number
        """
        return self.jd.get_jdn()

    def get_fod(self):
        """
        Get the fraction of the day.

        :return: Fraction of the Day
        """
        return self.jd.get_fod()

    def get_fod_midnight(self):
        """
        Get the fraction of the day running from midnight to midnight.

        :return: Fraction of the Day
        """
        return self.jd.get_fod_midnight()

    def get_delta_t(self):
        """
        Get the time interval. The precision of the returned value will be of the
        order of 1.0e-14 of a day or 1ns.

        :return: The time interval as delta_day, the number of days and delta_fod,
                 the fraction of the day.
        """
        ti = TimeInterval(self.jd.get_jdn(), self.jd.get_fod())
        return ti

    def set_delta_t(self, dt):
        """
        Set the time interval.

        :param: dt The time interval.
        """
        ti = TimeInterval(dt)
        self.jd.set_jdn(ti.get_delta_day())
        self.jd.set_fod(ti.get_delta_fod())

    def get_date_time(self, calendar=CalendarType.julian_gregorian):
        """
        Get the date and time. The calendar to use is specified by the enumerated
        values in CalendarType.

        :param: calendar This parameter specifies the calendar to use.
        :return: The date and time as a YMDHMS object.
        """

        ret = self.jd.fod_to_hms(
            self.jd.get_jdn(), self.jd.get_fod())
        ymdhms = YMDHMS()
        ymdhms.hms = self.h_to_hms(ret[1] * 24.0)
        if calendar == CalendarType.julian:
            ymdhms.ymd = self.jd_to_cd_j(ret[0])
        elif calendar == CalendarType.gregorian:
            ymdhms.ymd = self.jd_to_cd_g(ret[0])
        elif calendar == CalendarType.julian_gregorian:
            ymdhms.ymd = self.jd_to_cd_jg(ret[0])
        return ymdhms

    def get_date(self, calendar=CalendarType.julian_gregorian):
        """
        Get the date.

        :param: calendar The calendar used by the date.
        :return: The date.
        """
        return self.get_date_time(calendar).ymd

    def get_time_hms(self):
        """
        Get the time.

        :return: The hours, minutes and seconds.
        """
        return self.get_date_time(CalendarType.julian_gregorian).hms

    def get_time_hm(self):
        """
        Get the time.

        :return: The hours and minutes.
        """
        return self.h_to_hm(self.hms_to_h(
            self.get_date_time(CalendarType.julian_gregorian).hms))

    def set_date_time(self, date, calendar=CalendarType.julian_gregorian):
        """
        Set the date and time. The calendar to use is specified by the enumerated
        values in CalendarType.

        :param: ymdhms The date and time.
        :param: calendar The calendar used by the date.
        """
        ymdhms = YMDHMS(date)

        fod = self.hms_to_h(ymdhms.hms) / 24.0
        jdn = 0

        if calendar == CalendarType.julian:
            jdn = self.cd_to_jd_j(ymdhms.ymd)
        elif calendar == CalendarType.gregorian:
            jdn = self.cd_to_jd_g(ymdhms.ymd)
        elif calendar == CalendarType.julian_gregorian:
            jdn = self.cd_to_jd_jg(ymdhms.ymd)

        ret = self.jd.hms_to_fod(jdn, fod)
        self.jd.set_jd(ret[0], ret[1])

    def set_date_time_jd(self, jdn, fod=0.0):
        """
        Set the date and time.

        :param: jdn The Julian Date.
        :param: fod The fraction of the day, running from noon to noon.
        """
        self.jd.set_jdn(int(jdn))
        self.jd.set_fod(jdn - int(jdn) + fod)

    def set_date(self, date, calendar=CalendarType.julian_gregorian):
        """
        Set the date.

        The time is set to 00:00:00.0 The calendar to use is
        specified by the enumerated values in CalendarType.

        :param: ymd The year, month and day of the date.
        :param: calendar The calendar used by the date.
                """
        ymd = YMD(date)
        ymdhms = YMDHMS()
        ymdhms.set_ymd(ymd)
        self.set_date_time(ymdhms, calendar)

    def set_time(self, time):
        """
        Set the time.

        The date is set to 2000-01-01 (Julian/Gregorian Calendar).
        This is to ensure that negative Julian dates are not encountered.

        The time is normalised to the range 0 <= t < 24 so the
         date will change if times lie outside this range.

        :param: hms The time as hours, minutes and seconds.
        """
        ymdhms = YMDHMS()
        ymdhms.set_ymd([2000, 1, 1])
        hms = HMS(time)
        ymdhms.set_hms(hms)
        self.set_date_time(ymdhms, CalendarType.julian_gregorian)

    def set_time_hm(self, hm):
        """
        Set the time.

        The date is set to 2000-01-01 (Julian/Gregorian Calendar).

        The time is normalised to the range 0 <= t < 24 so the
         date will change if times lie outside this range.

        :param: hm The time as hours and minutes.
        """
        hms = HMS()
        hms.set_hms(self.h_to_hms(self.hm_to_h(hm)))
        self.set_time(hms)

    def set_jdn(self, jdn):
        """
        Set the Julian Day Number.

        :param: jdn The Julian Day Number.
        """
        return self.jd.set_jdn(jdn)

    def set_fod(self, fod):
        """
        Set the fraction of the day.

        :param: fod The fraction of the day.
        """
        return self.jd.set_fod(fod)

    def get_dow(self, calendar, first_dow):
        """
        Get the day of the week in a calendar agnostic way.

        :param: calendar The calendar used by the date.
        :param: first_dow The first day of the week, either Sunday or Monday.
        :return: The day of the week (1..7).
        """
        ymd = self.get_date(calendar)
        # The day of the week assumes the calendar type is Julian/Gregorian.
        d = ymd.year * 10000 + ymd.month * 100 + ymd.day
        ymd1 = YMD(ymd)
        if d < JGChangeover.gregorian_start_date:
            # For dates less than 1582-10-15, use the Julian Calendar
            return self.dow_ymd(ymd1, CalendarType.julian, first_dow)
        else:
            # For dates greater than or equal to 1582-10-15, use the Gregorian Calendar
            return self.dow_ymd(ymd, CalendarType.gregorian, first_dow)

    def get_doy(self, calendar):
        """
        Get the day of the year.

        :param: calendar The calendar used by the date, normally JULIAN_GREGORIAN.
        :return: The day of the year.
        """
        ymd = YMD(self.get_date(calendar))
        if calendar == CalendarType.julian:
            return self.doy_j(ymd)
        elif calendar == CalendarType.gregorian:
            return self.doy_g(ymd)
        elif calendar == CalendarType.julian_gregorian:
            return self.doy_jg(ymd)
        return None

    def get_iso_week(self):
        """
        Get the week of the year. See: http://en.wikipedia.org/wiki/ISO_week_date
        Note: This algorithm only works for Gregorian dates.

        :return: The ISO week date.
        """
        ymd = YMD(self.get_date(CalendarType.gregorian))
        return self.cd_to_woy(ymd)

    @staticmethod
    def is_julian_date_time(obj):
        """
        Verify that the object is of type JulianDateTime.

        :param: obj The object being tested.
        """
        if not (obj.__class__.__name__ == 'JulianDateTime' and hasattr(obj, 'jd')):
            raise TypeError('Not a class of type JulianDateTime.')

    @staticmethod
    def valid_julian_date_time(obj):
        """
        Verify that the object is of type JulianDateTime.

        :param: obj The object being tested.
        """
        return obj.__class__.__name__ == 'JulianDateTime' and hasattr(obj, 'jd')
