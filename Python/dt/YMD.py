#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   YMD.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


class YMD:
    """
    This class holds a date as year, month and day.
    """

    def __init__(self, *args, **kwargs):
        self.year = -4712
        self.month = 1
        self.day = 1
        if len(args) == 1:
            if type(args[0]) == type(self):
                self.year = args[0].year
                self.month = args[0].month
                self.day = args[0].day
                return
            if isinstance(args[0], list):
                if len(args[0]) == 1:
                    self.year = args[0][0]
                elif len(args[0]) == 2:
                    self.year = args[0][0]
                    self.month = args[0][1]
                else:
                    self.year = args[0][0]
                    self.month = args[0][1]
                    self.day = args[0][2]
            else:
                self.year = args[0]
        elif len(args) == 2:
            self.year = args[0]
            self.month = args[1]
        elif len(args) == 3:
            self.year = args[0]
            self.month = args[1]
            self.day = args[2]
        for k in kwargs:
            if k == 'year':
                self.year = kwargs[k]
            elif k == 'month':
                self.month = kwargs[k]
            elif k == 'day':
                self.day = kwargs[k]

    def set_ymd(self, *args, **kwargs):
        """
        Set the year, month and day.
        """
        self.__init__(*args, **kwargs)

    def __str__(self):
        """
        :return: Return the YMD as a string.
        """
        return f'{self.year:4d}-{self.month:02d}-{self.day:02d}'

    def __eq__(self, rhs):
        self.is_ymd(rhs)
        return self.year == rhs.year and \
            self.month == rhs.month and self.day == rhs.day

    def __ne__(self, rhs):
        self.is_ymd(rhs)
        return not (self == rhs)

    def __lt__(self, rhs):
        self.is_ymd(rhs)
        if self.year < rhs.year:
            return True
        if self.year == rhs.year:
            if self.month < rhs.month:
                return True
            if self.month == rhs.month:
                if self.day < rhs.day:
                    return True
        return False

    def __le__(self, rhs):
        self.is_ymd(rhs)
        return not (self > rhs)

    def __gt__(self, rhs):
        self.is_ymd(rhs)
        if self.year > rhs.year:
            return True
        if self.year == rhs.year:
            if self.month > rhs.month:
                return True
            if self.month == rhs.month:
                if self.day > rhs.day:
                    return True
        return False

    def __ge__(self, rhs):
        self.is_ymd(rhs)
        return not (self < rhs)

    @staticmethod
    def is_ymd(obj):
        """
        Verify that the object is of type YMD.

        :param: obj The object being tested.
        """
        if not (obj.__class__.__name__ == 'YMD' and
                hasattr(obj, 'year') and hasattr(obj, 'month') and hasattr(obj, 'day')):
            raise TypeError('Not a class of type YMD.')

    @staticmethod
    def valid_ymd(obj):
        """
        Verify that the object is of type YMD.

        :param: obj The object being tested.
        :return: true if it is a valid object, false otherwise.
        """
        return (obj.__class__.__name__ == 'YMD' and
                hasattr(obj, 'year') and hasattr(obj, 'month') and hasattr(obj, 'day'))
