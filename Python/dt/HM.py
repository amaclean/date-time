#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   HM.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


from math import isclose

from dt.DateTimeBase import DateTimeBase


class HM(DateTimeBase):
    """
    This class holds a time as hours and minutes.
    """

    def __init__(self, *args, **kwargs):
        self.hour = 0
        self.minute = 0
        if len(args) == 1:
            if type(args[0]) == type(self):
                self.hour = args[0].hour
                self.minute = args[0].minute
                return
            if isinstance(args[0], list):
                if len(args[0]) == 1:
                    self.hour = args[0][0]
                elif len(args[0]) == 2:
                    self.hour = args[0][0]
                    self.minute = args[0][1]
            else:
                self.hour = args[0]
        elif len(args) == 2:
            self.hour = args[0]
            self.minute = args[1]
        for k in kwargs:
            if k == 'hour':
                self.hour = kwargs[k]
            elif k == 'minute':
                self.minute = kwargs[k]

    def set_hm(self, *args, **kwargs):
        """
        Set the time.

        """
        self.__init__(*args, **kwargs)

    def __str__(self):
        """
        :return: Return the HM as a string.
        """
        return f'{self.hour:02d}:{self.minute:015.12f}'

    def __eq__(self, rhs):
        self.is_hm(rhs)
        return self.hour == rhs.hour and self.minute == rhs.minute

    def __ne__(self, rhs):
        self.is_hm(rhs)
        return not (self == rhs)

    def __lt__(self, rhs):
        self.is_hm(rhs)
        if self.hour < rhs.hour:
            return True
        if self.hour == rhs.hour:
            if self.minute < rhs.minute:
                return True
        return False

    def __le__(self, rhs):
        self.is_hm(rhs)
        return not (self > rhs)

    def __gt__(self, rhs):
        self.is_hm(rhs)
        if self.hour > rhs.hour:
            return True
        if self.hour == rhs.hour:
            if self.minute > rhs.minute:
                return True
        return False

    def __ge__(self, rhs):
        self.is_hm(rhs)
        return not (self < rhs)

    def is_close(self, hm, rel_tol=DateTimeBase.rel_minutes, abs_tol=0.0):
        """
        Compare hours and minutes for equality.

        :param: d The Calendar Date to be compared with this.
        :param: rel_tol Relative tolerance.
        :param: abs_tol Absolute tolerance.
        :return: True if the dates are equivalent.
        """
        self.is_hm(hm)
        if self == hm:
            return True
        else:
            return self.hour == hm.hour and isclose(self.minute, hm.minute, rel_tol=rel_tol, abs_tol=abs_tol)

    @staticmethod
    def is_hm(obj):
        """
        Verify that the object is of type HM.

        :param: obj The object being tested.
        """
        if not (obj.__class__.__name__ == 'HM' and
                hasattr(obj, 'hour') and hasattr(obj, 'minute')):
            raise TypeError('Not a class of type HM.')

    @staticmethod
    def valid_hm(obj):
        """
        Verify that the object is of type HM.

        :param: obj The object being tested.
        :return: true if it is a valid object, false otherwise.
        """
        return (obj.__class__.__name__ == 'HMS' and
                hasattr(obj, 'hour') and hasattr(obj, 'minute'))
