#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   JD.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


import numbers
from math import isclose

from dt.DateTimeBase import DateTimeBase


class JD(DateTimeBase):
    """
    This class holds a date as Julian Day Number and Fraction of the Day.

      The Julian Day Number stored in the class is the integral value of the day
      corresponding to the Julian Date (jdn) i.e int(jdn + 0.5), so it
      runs from noon to noon on the date in question.

      The fraction of the day stored runs from noon to noon.
    """

    def __init__(self, *args, **kwargs):
        """
        Default value corresponds to -4712-01-01 12:00:00 = JD0
        """
        self.jdn = 0
        self.fod = 0
        if len(args) == 1:
            if type(args[0]) == type(self):
                self.jdn = args[0].jdn
                self.fod = args[0].fod
                return
            if isinstance(args[0], list):
                if len(args[0]) == 1:
                    self.jdn = args[0][0]
                elif len(args[0]) == 2:
                    self.jdn = args[0][0]
                    self.fod = args[0][1]
            else:
                self.jdn = args[0]
        elif len(args) == 2:
            self.jdn = args[0]
            self.fod = args[1]
        else:
            for k in kwargs:
                if k == 'jdn':
                    self.jdn = kwargs[k]
                elif k == 'fod':
                    self.fod = kwargs[k]
        self.normalise()

    def set_jd(self, *args, **kwargs):
        """
        Set the Julian date.
        """
        self.__init__(*args, **kwargs)

    def __str__(self):
        """
        :return: Return the JD as a string.
        """
        return f'{self.jdn:d}, {self.fod:15.12f}'

    def __eq__(self, rhs):
        self.is_jd(rhs)
        return self.jdn == rhs.jdn and self.fod == rhs.fod

    def __ne__(self, rhs):
        self.is_jd(rhs)
        return not (self == rhs)

    def __lt__(self, rhs):
        self.is_jd(rhs)
        if self.jdn < rhs.jdn:
            return True
        if self.jdn == rhs.jdn:
            if self.fod < rhs.fod:
                return True
        return False

    def __le__(self, rhs):
        self.is_jd(rhs)
        return not (self > rhs)

    def __gt__(self, rhs):
        self.is_jd(rhs)
        if self.jdn > rhs.jdn:
            return True
        if self.jdn == rhs.jdn:
            if self.fod > rhs.fod:
                return True
        return False

    def __ge__(self, rhs):
        self.is_jd(rhs)
        return not (self < rhs)

    def is_close(self, jd, rel_tol=DateTimeBase.rel_fod, abs_tol=0.0):
        """
        Compare Julian Dates for equality.

        :param: jd The Julian Date to be compared with this.
        :param: rel_tol Relative tolerance.
        :param: abs_tol Absolute tolerance.
        :return: True if the dates are equivalent.
        """
        self.is_jd(jd)
        if self == jd:
            return True
        else:
            return self.jdn == jd.jdn and isclose(self.fod, jd.fod, rel_tol=rel_tol, abs_tol=abs_tol)

    def __add__(self, rhs):
        jd = self
        if self.valid_jd(rhs):
            jd.jdn += rhs.jdn
            jd.fod += rhs.fod
        else:
            if isinstance(rhs, int) \
                    or isinstance(rhs, float) \
                    or isinstance(rhs, numbers.Integral):
                # We are expecting a float corresponding to the days.
                jd.jdn += int(rhs)
                jd.fod += (rhs - int(rhs))
            else:
                raise TypeError('Not a class of type JD or float.')
        jd.normalise()
        return jd

    def __iadd__(self, rhs):
        if self.valid_jd(rhs):
            self.jdn += rhs.jdn
            self.fod += rhs.fod
        else:
            if isinstance(rhs, int) \
                    or isinstance(rhs, float) \
                    or isinstance(rhs, numbers.Integral):
                # We are expecting a float corresponding to the days.
                self.jdn += int(rhs)
                self.fod += (rhs - int(rhs))
            else:
                raise TypeError('Not a class of type JD or float.')
        self.normalise()
        return self

    def __sub__(self, rhs):
        jd = self
        if self.valid_jd(rhs):
            jd.jdn -= rhs.jdn
            jd.fod -= rhs.fod
        else:
            if isinstance(rhs, int) \
                    or isinstance(rhs, float) \
                    or isinstance(rhs, numbers.Integral):
                # We are expecting a float corresponding to the days.
                jd.jdn -= int(rhs)
                jd.fod -= (rhs - int(rhs))
            else:
                raise TypeError('Not a class of type JD or float.')
        jd.normalise()
        return jd

    def __isub__(self, rhs):
        if self.valid_jd(rhs):
            self.jdn -= rhs.jdn
            self.fod -= rhs.fod
        else:
            if isinstance(rhs, int) \
                    or isinstance(rhs, float) \
                    or isinstance(rhs, numbers.Integral):
                # We are expecting a float corresponding to the days.
                self.jdn -= int(rhs)
                self.fod -= (rhs - int(rhs))
            else:
                raise TypeError('Not a class of type JD or float.')
        self.normalise()
        return self

    def normalise(self):
        """
        Normalise so that the 0 <= fod < 1.
        """

        # make sure jdn is integer.
        jdn_int = int(self.jdn)
        fod_int = int(self.fod)
        jdn_float = self.jdn - jdn_int
        fod_float = self.fod - fod_int
        self.jdn = jdn_int + fod_int
        self.fod = jdn_float + fod_float

        while self.fod < 0:
            self.fod += 1.0
            self.jdn -= 1
        while self.fod >= 1.0:
            self.fod -= 1.0
            self.jdn += 1

    def get_jdn(self):
        """
        Get the Julian Day Number.

        :return: Julian Day Number
        """
        return self.jdn

    def get_fod(self):
        """
        Get the fraction of the day.

        :return: Fraction of the Day
        """
        return self.fod

    def get_fod_midnight(self):
        """
        Get the fraction of the day running from midnight to midnight.

        :return: Fraction of the Day
        """
        res = self.fod_to_hms(self.jdn, self.fod)
        return res[1]

    def get_jd_double(self):
        """
        Get the Julian Date as days and decimals of the day.

        The precision of the returned value will be of the order
        of 1.0e-8 of a day or 1ms.

        :return: The Julian Day Number of the date as days
                  and decimals of the day.
        """
        return self.jdn + self.fod

    def set_jdn(self, jdn):
        """
        Set the Julian Day Number.

        :param: jdn The Julian Day Number.
        """
        self.jdn = jdn
        self.normalise()

    def set_fod(self, fod):
        """
        Set the Julian Day Number.

        :param: fod The fraction of the day.
        """
        self.fod = fod
        self.normalise()

    @staticmethod
    def fod_to_hms(jdn, fod):
        """
        Convert the fraction of the day running from noon to noon to the fraction of the day running from midnight to midnight.

        :param: jdn The julian day number.
        :param: fod The fraction of the day running from noon to noon (always positive).

        :return: A pair holding the count of days and fraction of the day running from midnight to midnight (always positive).
        """
        hms = fod + 0.5
        days = jdn
        if hms >= 1.0:  # Next day.
            hms -= 1.0
            days += 1
        return [days, hms]

    @staticmethod
    def hms_to_fod(days, hms):
        """
        Convert the fraction of the day running from midnight to midnight to the fraction of the day running from noon to noon.

        :param: days The count of days.
        :param: hms The fraction of the day running from midnight to midnight (always positive).

         :return: A pair holding the  julian day number and the fraction of the day running from noon to noon (always positive).
        """
        fod = hms - 0.5
        jdn = days
        if fod < 0:
            fod += 1.0  # Previous day.
            jdn -= 1
        return [jdn, fod]

    @staticmethod
    def valid_jd(obj):
        """
        Verify that the object is of type JD.

        :param: obj The object being tested.
        :return: true if it is a valid object, false otherwise.
        """
        return (obj.__class__.__name__ == 'JD' and
                hasattr(obj, 'jdn') and hasattr(obj, 'fod'))

    @staticmethod
    def is_jd(obj):
        """
        Verify that the object is of type JD.

        :param: obj The object being tested.
        """
        if not (obj.__class__.__name__ == 'JD' and
                hasattr(obj, 'jdn') and hasattr(obj, 'fod')):
            raise TypeError('Not a class of type JD.')
