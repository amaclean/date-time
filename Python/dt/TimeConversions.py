#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   TimeConversions.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


import math

from dt.HM import HM
from dt.HMS import HMS


class TimeConversions:
    """
    This class provides methods to convert between hours and hours, minutes and seconds.

    Function names that start with a lower case letter indicate conversion methods.

    The conversion methods generally follow this pattern:

    [h|hm|hms]_to_[h|hm|hms] where

    h - hours, hm - hours and minutes, hms - hours minutes and seconds.

    Times are stored as a list [hours, minutes] or [hours, minutes and seconds].

    :author: Andrew J. P. Maclean
    """

    @staticmethod
    def hms_to_h(time):
        """
        Convert hours minutes and seconds to hours.

        :param: time An HMS object or an object that can be converted to an HMS object.
        :return: Hour and decimals of the hour.
        """
        hms = HMS(time)
        hour = float(hms.hour)
        minute = float(hms.minute)
        second = float(hms.second)
        if hour < 0 or minute < 0 or second < 0:
            return -((math.fabs(second) / 60.0 + math.fabs(minute)) / 60.0 + math.fabs(hour))
        return (second / 60.0 + minute) / 60.0 + hour

    @staticmethod
    def h_to_hms(hr):
        """
        Convert hours to hours minutes and seconds.

        :param: hr Hours and decimals of an hour.
        :return: An HMS object.
        """
        sgn = hr < 0
        t = math.fabs(hr)
        t, n = math.modf(t)
        t *= 60.0
        hour = int(n)
        s, n = math.modf(t)
        s *= 60.0
        minute = int(n)
        second = s
        if second >= 60:
            minute += 1
            second -= 60
        if minute >= 60:
            hour += 1
            minute -= 60
        if sgn:
            if hour != 0:
                hour = -hour
                return HMS(hour, minute, second)
            if minute != 0:
                minute = -minute
                return HMS(hour, minute, second)
            if second != 0:
                second = -second
                return HMS(hour, minute, second)
        return HMS(hour, minute, second)

    @staticmethod
    def hm_to_h(time):
        """
        Convert hours and minutes to hours.

        :param: time An HM object or an object that can be converted to an HM object.
        :return: Hour and decimals of the hour.
        """
        hm = HM(time)
        hour = float(hm.hour)
        minute = float(hm.minute)
        if hour < 0 or minute < 0:
            return -(math.fabs(minute) / 60.0 + math.fabs(hour))
        return minute / 60.0 + hour

    @staticmethod
    def h_to_hm(hr):
        """
        Convert hours to hours and minutes.

        :param: hr Hours and decimals of an hour.
        :return: An HM object.
        """
        sgn = hr < 0
        t = math.fabs(hr)
        m, n = math.modf(t)
        m *= 60.0
        hour = int(n)
        minute = m
        if minute >= 60:
            hour += 1
            minute -= 60
        if sgn:
            if hour != 0:
                hour = -hour
                return HM(hour, minute)
            if minute != 0:
                minute = -minute
                return HM(hour, minute)
        return HM(hour, minute)
