#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   YMDHMS.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


from dt.DateTimeBase import DateTimeBase
from dt.HMS import HMS
from dt.YMD import YMD


class YMDHMS(DateTimeBase):
    """
    This class holds a date as year, month, day, hour, minute and second.
    """

    def __init__(self, *args, **kwargs):
        """
        Default value corresponds to -4712-01-01 00:00:00
        """

        self.ymd = YMD()
        self.hms = HMS()
        if len(args) == 1:
            if type(args[0]) == type(self):
                self.ymd = args[0].ymd
                self.hms = args[0].hms
                return
            if isinstance(args[0], list):
                if len(args[0]) == 1:
                    self.ymd.year = args[0][0]
                elif len(args[0]) == 2:
                    self.ymd.year = args[0][0]
                    self.ymd.month = args[0][1]
                elif len(args[0]) == 3:
                    self.ymd.year = args[0][0]
                    self.ymd.month = args[0][1]
                    self.ymd.day = args[0][2]
                elif len(args[0]) == 4:
                    self.ymd.year = args[0][0]
                    self.ymd.month = args[0][1]
                    self.ymd.day = args[0][2]
                    self.hms.hour = args[0][3]
                elif len(args[0]) == 5:
                    self.ymd.year = args[0][0]
                    self.ymd.month = args[0][1]
                    self.ymd.day = args[0][2]
                    self.hms.hour = args[0][3]
                    self.hms.minute = args[0][4]
                else:
                    self.ymd.year = args[0][0]
                    self.ymd.month = args[0][1]
                    self.ymd.day = args[0][2]
                    self.hms.hour = args[0][3]
                    self.hms.minute = args[0][4]
                    self.hms.second = args[0][5]
            else:
                self.year = args[0]
        elif len(args) == 2:
            # Arg 1, 2 lists [ymd] [hms]
            if isinstance(args[0], list) and isinstance(args[1], list):
                self.set_ymd(args[0])
                self.set_hms(args[1])
            elif self.ymd.valid_ymd(args[0]) and self.hms.valid_hms(args[1]):
                self.set_ymd(args[0])
                self.set_hms(args[1])
            else:
                self.ymd.year = args[0]
                self.ymd.month = args[1]
        elif len(args) == 3:
            self.ymd.year = args[0]
            self.ymd.month = args[1]
            self.ymd.day = args[2]
        elif len(args) == 4:
            self.ymd.year = args[0]
            self.ymd.month = args[1]
            self.ymd.day = args[2]
            self.hms.hour = args[3]
        elif len(args) == 5:
            self.ymd.year = args[0]
            self.ymd.month = args[1]
            self.ymd.day = args[2]
            self.hms.hour = args[3]
            self.hms.minute = args[4]
        elif len(args) == 6:
            self.ymd.year = args[0]
            self.ymd.month = args[1]
            self.ymd.day = args[2]
            self.hms.hour = args[3]
            self.hms.minute = args[4]
            self.hms.second = args[5]
        for k in kwargs:
            if k == 'year':
                self.ymd.year = kwargs[k]
            elif k == 'month':
                self.ymd.month = kwargs[k]
            elif k == 'day':
                self.ymd.day = kwargs[k]
            if k == 'hour':
                self.hms.hour = kwargs[k]
            elif k == 'minute':
                self.hms.minute = kwargs[k]
            elif k == 'second':
                self.hms.second = kwargs[k]
            elif k == 'ymd':
                self.ymd = YMD(kwargs[k])
            elif k == 'hms':
                self.hms = HMS(kwargs[k])

    def set_ymdhms(self, *args, **kwargs):
        """
        Set the date and time.
        """
        self.__init__(*args, **kwargs)

    def __str__(self):
        """
        :return: Return the YMDHMS as a string.
        """
        return f'{str(self.ymd):s} {str(self.hms):s}'

    def __eq__(self, rhs):
        self.is_ymdhms(rhs)
        return self.ymd == rhs.ymd and self.hms == rhs.hms

    def __ne__(self, rhs):
        self.is_ymdhms(rhs)
        return not (self == rhs)

    def __lt__(self, rhs):
        self.is_ymdhms(rhs)
        if self.ymd < rhs.ymd:
            return True
        if self.ymd == rhs.ymd:
            if self.hms < rhs.hms:
                return True
        return False

    def __le__(self, rhs):
        self.is_ymdhms(rhs)
        return not (self > rhs)

    def __gt__(self, rhs):
        self.is_ymdhms(rhs)
        if self.ymd > rhs.ymd:
            return True
        if self.ymd == rhs.ymd:
            if self.hms > rhs.hms:
                return True
        return False

    def __ge__(self, rhs):
        self.is_ymdhms(rhs)
        return not (self < rhs)

    def is_close(self, d, rel_tol=DateTimeBase.rel_fod, abs_tol=0.0):
        """
        Compare Calendar Dates for equality.

        :param: d The Calendar Date to be compared with this.
        :param: rel_tol Relative tolerance.
        :param: abs_tol Absolute tolerance.
        :return: True if the dates are equivalent.
        """
        self.is_ymdhms(d)
        return self.get_ymd() == d.get_ymd() and self.get_hms().is_close(d.get_hms(), rel_tol, abs_tol)

    def set_ymd(self, date):
        """
        Set the date.

         :param: date The date.
        """
        ymd = YMD(date)
        self.ymd.year = ymd.year
        self.ymd.month = ymd.month
        self.ymd.day = ymd.day

    def set_hms(self, time):
        """
        Set the time.

         :param: time The time.
        """
        hms = HMS(time)
        self.hms.hour = hms.hour
        self.hms.minute = hms.minute
        self.hms.second = hms.second

    def get_ymd(self):
        """
        Get the date.

        :return: The date.
        """
        return self.ymd

    def get_hms(self):
        """
        Get the time.

        :return: The time.
        """
        return self.hms

    @staticmethod
    def is_ymdhms(obj):
        """
        Verify that the object is of type YMDHMS.

        :param: obj The object being tested.
        """
        if not (obj.__class__.__name__ == 'YMDHMS'
                and hasattr(obj, 'ymd') and hasattr(obj, 'hms')):
            raise TypeError('Not a class of type YMDHMS.')

    @staticmethod
    def valid_ymdhms(obj):
        """
        Verify that the object is of type YMD.

        :param: obj The object being tested.
        :return: true if it is a valid object, false otherwise.
        """
        return (obj.__class__.__name__ == 'YMDHMS'
                and hasattr(obj, 'ymd') and hasattr(obj, 'hms'))
