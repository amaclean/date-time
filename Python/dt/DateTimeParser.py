#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   DateTimeParser.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


import re

from dt.HMS import HMS
from dt.MonthNames import MonthNames
from dt.TimeConversions import TimeConversions
from dt.TimeInterval import TimeInterval
from dt.YMD import YMD
from dt.YMDHMS import YMDHMS


class DateTimeParser:
    """
    A convenience class for parsing dates and times.

    :param: maximum_precision The maximum allowable precision of the decimal part being displayed.

    If the precision specified in the format statements exceeds this,
     then this value is used, it is set to 15 decimal places by default.

    The user can change this value.

    For displaying seconds a maximum precision of 9 is sufficient
     for nanosecond precision.
    For displaying julian date times a precision of 15 will yield
     nanosecond precision.

    All the parsers return a variable of type [int,T]
    where the integer value corresponds to the states returned from
    the parser.
    These values are:

         - 1 = Parsed Ok.
         - 2 = No data.
         - 3 = Invalid date.
         - 4 = Bad month.
         - 5 = Invalid time.
         - 6 = Invalid integer or decimal number.

    """

    def __init__(self):
        self.intNum = re.compile("^(\\+|-)?\\d+$")
        self.decNum = re.compile("^((\\+|-)?\\d+)(\\.((\\d+)?))?$")
        self.sciNum = re.compile("^((\\+|-)?\\d+)(\\.((\\d+)?))?((e|E)((\\+|-)?)\\d+)?$")
        self.datePattern = re.compile("^(\\+|-)?\\d+\\D(\\d{1,2}|\\w{3,})\\D\\d{1,2}$")
        self.timePattern = re.compile(
            "^(\\+|-)?(\\d{1,2}(\\D\\d{1,2})\\D\\d{1,2}(\\.((\\d+)?))?|\\d{1,2}(\\D\\d{1,2})?)$")

        # All the parsers return a variable of type [int,T]
        # where the int value corresponds to the states returned from
        # the parser.
        # Change these messages to whatever language you need by changing
        # the message strings in your code.
        self.parserMessages = {
            1: 'Parsed Ok.',
            2: 'No data.',
            3: 'Invalid date.',
            4: 'Bad month.',
            5: 'Invalid time.',
            6: 'Invalid integer or decimal number.'
        }

    def parse_dec_num(self, dec_str):
        """
        Parse a string of the form dd.dddd into its integer and fractional parts.

        :param: dec_str The string to tokenize.
        :return: A pair consisting of an integer and the parsed number,
                with the integer taking the following values:

        - 1 = Parsed Ok.
        - 2 = No data.
        - 6 = Invalid integer or decimal number.
        """
        s = dec_str.strip()
        if s is None or len(s) == 0:
            return [2, [None, None]]

        if not self.decNum.match(s):
            return [6, [None, None]]

        sv = s.split('-')
        is_negative = False
        if len(sv) > 1:
            is_negative = True
            s = sv[1].strip()
        sv = s.split('.')
        int_part = 0
        frac_part = 0.0
        if len(sv) == 1:
            if len(sv[0]) != 0:
                int_part = int(sv[0].strip())
        elif len(sv) > 1:
            if len(sv[0]) != 0:
                int_part = int(sv[0].strip())
            if len(sv[1]) != 0:
                frac_part = float('0.' + sv[1].strip())
        if is_negative:
            int_part = -int_part
            frac_part = -frac_part
        return [1, [int_part, frac_part]]

    def parse_date(self, date, fmt):
        """
        Parse a date formatted as YYYY-MM-DD into a YMD class.

        In addition to being an integer in the range [01 ... 12], the month can
        be either the month name or abbreviation.

        :param: date The date string to parse.
        :param: fmt A class of type DateTimeFormat specifying the formatting to use.
        :return: A pair consisting of an integer and the parsed date, with the
                integer taking the following values:

        - 1 = Parsed Ok.
        - 2 = No data.
        - 3 = Invalid date.
        - 4 = Bad month.

        """
        ret = [1, YMD()]

        if date is None or date.strip() == '':
            ret[0] = 2
            return ret

        date_str = date.strip()
        if not self.datePattern.match(date_str):
            ret[0] = 3
            return ret

        if date_str[0] == '-':
            s = list(date_str)
            s[0] = '*'
            date_str = ''.join(s)

        date_components = date_str.split(fmt.date_sep)
        if date_components[0][0] == '*':
            s = list(date_components[0])
            s[0] = '-'
            date_components[0] = ''.join(s)

        if len(date_components) != 3:
            ret[0] = 3  # Need year, month, day
            return ret
        for idx in range(len(date_components)):
            date_components[idx] = date_components[idx].strip()

        month = day = 0
        year = date_components[0]
        if len(date_components[1]) <= 2:
            # We have an integer month.
            if self.intNum.match(date_components[1]):
                month = int(date_components[1])
        else:
            months = MonthNames()
            for i in range(1, 13):
                if months.get_month_name(i) == date_components[1] or \
                        months.get_abbreviated_month_name(i, 3) == date_components[1]:
                    month = i
                    break
        if month < 1 or month > 12:
            ret[0] = 4  # Bad month
            return ret
        if self.intNum.match(date_components[2]):
            day = date_components[2]

        ret[0] = 1
        ret[1].set_ymd(int(year), int(month), int(day))

        return ret

    def parse_time(self, time, fmt):
        """
        Parse a time formatted as HH:mm:ss.sss into an HMS class.

        :param: time The time string to parse.
        :param: fmt A class of type DateTimeFormat specifying the formatting to use.
        :return: A pair consisting of an integer and the parsed date, with the
                integer taking the following values:

        - 1 = Parsed Ok.
        - 2 = No data.
        - 5 = Invalid time.

        """
        ret = [1, HMS()]

        if time is None or time.strip() == '':
            ret[0] = 2
            return ret

        time_str = time.strip()
        if not self.timePattern.match(time_str):
            ret[0] = 5
            return ret

        is_neg = time_str.find('-') != -1
        hour = minute = second = 0
        if len(time_str) != 0:
            time_components = time_str.split(fmt.time_sep)
            for i in range(0, len(time_components)):
                if i < 2:
                    if not self.intNum.match(time_components[i]):
                        ret[0] = 5
                        return ret
                else:
                    if i == 2:
                        if not self.decNum.match(time_components[i]):
                            ret[0] = 5
                            return ret
                    else:  # Time has a maximum of three components.
                        ret[0] = 5
                        return ret
                if i == 0:
                    hour = abs(int(time_components[0]))
                elif i == 1:
                    minute = abs(int(time_components[1]))
                elif i == 2:
                    second = abs(float(time_components[2]))
            if is_neg:
                if hour != 0:
                    hour = -hour
                elif minute != 0:
                    minute = -minute
                elif second != 0:
                    second = -second

            ret[0] = 1
            ret[1].set_hms(hour, minute, second)

        return ret

    def parse_date_time(self, date, fmt):
        """
        Parse a date formatted as YYYY-MM-DD HH-mm-ss.sss into a YMDHMS
        class.

        In addition to being an integer in the range [01 ... 12], the month can
        be either the month name or abbreviation.

        The time string is optional. If not present then
        the time is returned as 00:00:00.00

        :param: date The date string to parse.
        :param: fmt A class of type DateTimeFormat specifying the formatting to use.
        :return: A pair consisting of an integer and the parsed date, with the
                integer taking the following values:

        - 1 = Parsed Ok.
        - 2 = No data.
        - 3 = Invalid date.
        - 4 = Bad month.
        - 5 = Invalid time.

        """
        ret = [1, None]

        if date is None or date.strip() == '':
            ret[0] = 2  # No data
            return ret

        datetime = date.strip().split(fmt.date_time_sep)
        # datetime will always have a length >= 1.

        time_str = ''

        if len(datetime) == 1:
            # We have just a date
            date_str = datetime[0].strip()
        else:
            date_str = datetime[0].strip()
            time_str = datetime[1].strip()

        ymd_res = self.parse_date(date_str, fmt)
        if ymd_res[0] != 1:
            ret[0] = ymd_res[0]
            return ret
        hms_res = self.parse_time(time_str, fmt)
        # An empty time string is valid.
        if hms_res[0] != 1 and hms_res[0] != 2:
            ret[0] = hms_res[0]
            return ret

        ret[0] = 1
        ret[1] = YMDHMS(ymd_res[1], hms_res[1])
        return ret

    def parse_time_interval(self, ti, fmt):
        """
        Parse a time interval formatted as d.dd HH:mm:ss.sss into a
         DT::TimeInterval class.

        d.dd can be a decimal number with an optional trailing "d".

        The time string is optional. If not present then
        the time is returned as 00:00:00.00

        Note: The day and HMS part of the string have independent signs.
        This means that for:

            -  -1d -12:00:00.0, -1d 12:00:00.0, 1d -12:00:00.0, 1d 12:00:00.0

        We get time intervals of:

            -  (-1, -0.5), (-1, 0.5), (1, -0.5) (1, 0.5) respectively.

        :param: ti The time interval string to parse.
        :param: fmt A class of type DateTimeFormat specifying the formatting to use.
        :param: A pair consisting of an integer and the parsed time interval,
                with the integer taking the following values:

        - 1 = Parsed Ok.</li>
        - 2 = No data.</li>
        - 5 = Invalid time.</li>
        - 6 =  Invalid integer or decimal number.</li>

        """
        ret = [1, None]

        def clean_time_interval(s):
            """
            Strip out spurious characters in the time interval.
            :param: s The string to clean.
            :return: The cleaned string
            """
            ws = re.compile("\\s{2,}")
            d_d = re.compile("[d|D]")
            t = s
            t = re.sub(ws, ' ', t)
            t = re.sub(d_d, '', t)
            return t.strip()

        if ti is None:
            ret[0] = 2
            return ret

        time_interval = clean_time_interval(ti)

        if len(time_interval) == 0:
            ret[0] = 2
            return ret

        day_time = time_interval.split(fmt.day_time_sep)
        # day_time will always have a length >= 1.

        time_str = ''

        if len(day_time) == 1:
            # We have just a date
            day_str = day_time[0].strip()
        else:
            day_str = day_time[0].strip()
            time_str = day_time[1].strip()

        res = self.parse_dec_num(day_str)
        if res[0] != 1:
            ret[0] = res[0]
            return ret
        time_interval = TimeInterval(res[1][0], res[1][1])
        hms_res = self.parse_time(time_str, fmt)
        # An empty time string is valid.
        if hms_res[0] != 1 and hms_res[0] != 2:
            ret[0] = hms_res[0]
            return ret
        fod = TimeConversions().hms_to_h(hms_res[1]) / 24.0

        ret[0] = 1
        ret[1] = TimeInterval(time_interval.get_delta_day(), time_interval.get_delta_fod() + fod)

        return ret
