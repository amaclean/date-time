#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   MonthNames.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


def is_month_names(obj):
    """
    Verify that the object is of type MonthNames.

    :param: obj The object being tested.
    """
    if not (obj.__class__.__name__ == 'MonthNames' and
            hasattr(obj, 'names')):
        raise TypeError('Not a class of type MonthNames.')


class MonthNames:
    """
    This class holds month names.
    """

    def __init__(self):
        self.names = {1: 'January', 2: 'February', 3: 'March',
                      4: 'April', 5: 'May', 6: 'June',
                      7: 'July', 8: 'August', 9: 'September',
                      10: 'October', 11: 'November', 12: 'December'}

    def __str__(self):
        """
        :return: Return the MonthNames as a string.
        """
        return self.names

    def __eq__(self, rhs):
        is_month_names(rhs)
        return self.names == rhs.names

    def __ne__(self, rhs):
        is_month_names(rhs)
        return not (self == rhs)

    def get_month_name(self, month):
        """
        Get the month name.

        None is returned if the index of the month
        lies outside the range 1...12.

        :param: month The month number.

        :return: The month name as a string or no_name if not found.
        """
        return self.names.get(month)

    def get_abbreviated_month_name(self, month, size=3):
        """
        Get the abbreviated month name.

        None is returned if the index of the month
        lies outside the range 1...12.

        :param: month The month number.
        :param: size The number of letters used to form the abbreviation.

        :return: The abbreviated month name as a string or no_name if not found.
        """
        dn = self.names.get(month)
        if dn is None:
            return dn
        if len(dn) <= size:
            return dn
        return dn[:size]

    def set_month_name(self, month, month_name):
        """
        Set the month name. Useful when using a different language.

        There are 12 months and the first month starts at 1.
        No updates are performed if the index of the month
        lies outside the range 1...12.

        :param: month The month number.
        :param: month_name The month name.
        """
        if 0 < month < 13:
            self.names[month] = month_name
