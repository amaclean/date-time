#!/usr/bin/env python3
# ==========================================================================
#
# Program:   Date Time Library
# File   :   DateTimeFormatter.py
#
# Copyright (c) Andrew J. P. Maclean
# All rights reserved.
# See Copyright.txt or the documentation for details.
#
#    This software is distributed WITHOUT ANY WARRANTY; without even
#    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#    PURPOSE.  See the above copyright notice for more information.
#
# ==========================================================================


import math

from dt.Constants import CalendarType, DayName, FirstDOW, JGChangeover, MonthName
from dt.DateTimeBase import DateTimeBase
from dt.DayNames import DayNames
from dt.HM import HM
from dt.JulianDateTime import JulianDateTime
from dt.MonthNames import MonthNames
from dt.TimeConversions import TimeConversions
from dt.TimeInterval import TimeInterval
from dt.YMDHMS import YMDHMS


class DateTimeFormatter(DateTimeBase):
    """
    A convenience class for formatting dates and times.

    Note: maximum_precision is set in DateTimeBase

    """

    def __init__(self):
        pass

    def get_formatted_dates(self, datetime, fmt):
        """
        Return a dictionary of the various parts of a date for display.

        :param: datetime The date and time to format as a JulianDateTime object.
        :param: fmt A class of type DateTimeFormat specifying the formatting to use.

        :return: A dictionary of strings whose keys are:

          - NamedDate - the formatted date with month and day names.
          - Date - the formatted date.
          - DoW - the day of the week.
          - DayName - the day name.
          - DayNameAbbrev - the abbreviated day name.
          - MonthName - the month name.
          - MonthNameAbbrev - the abbreviated month name.
          - DoY - the day of the year.
          - IWD - the ISO week.
          - JD - The Julian Day.
          - JD - The short form of the Julian Day.
          - Calendar - The calendar used.
          - LeapYear - Either Yes or No.

          Note: ISO Week is only calculated if year &gt; 1582 in the Gregorian calendar.
        """
        res = dict()
        res['NamedDate'] = self.format_date_with_names(datetime, fmt)
        res['Date'] = self.format_as_ymdhms(datetime, fmt)
        dow = datetime.get_dow(fmt.calendar, fmt.first_dow)
        res['DoW'] = str(dow)
        day_names = DayNames()
        if fmt.first_dow == FirstDOW.sunday:
            # Sunday = 1
            res['DayName'] = day_names.get_day_name(dow)
            res['DayNameAbbrev'] = day_names.get_abbreviated_day_name(dow, fmt.day_abbreviation_length)
        else:
            # Monday = 1
            # GetDayName and GetAbbreviatedDayName
            # treat Sunday as the first day of the week.
            res['DayName'] = day_names.get_day_name((dow % 7) + 1)
            res['DayNameAbbrev'] = day_names.get_abbreviated_day_name((dow % 7) + 1, fmt.day_abbreviation_length)
        res['DoY'] = self.format_doy(datetime, fmt)
        ymd = datetime.get_date(fmt.calendar)
        mn = MonthNames()
        res['MonthName'] = mn.get_month_name(ymd.month)
        res['MonthNameAbbrev'] = mn.get_abbreviated_month_name(ymd.month, fmt.month_abbreviation_length)
        # ISO week dates are only valid for the Gregorian Calendar.
        if ymd.year > JGChangeover.julian_gregorian_year and fmt.calendar != CalendarType.julian:
            res['IWD'] = self.format_iso_week(datetime)
        else:
            res['IWD'] = ''
        jdfp = fmt.precision_fod
        fmt.precision_fod = self.maximum_precision - 1
        res['JD'] = self.format_jd_long(datetime.get_jd_full_precision(), fmt)
        fmt.precision_fod = jdfp
        res['JDShort'] = self.format_jd_short(datetime.get_jd_full_precision(),
                                              fmt)
        if fmt.calendar == CalendarType.julian:
            res['Calendar'] = 'Julian'
        elif fmt.calendar == CalendarType.gregorian:
            res['Calendar'] = 'Gregorian'
        else:
            res['Calendar'] = 'Julian/Gregorian'
        if datetime.is_leap_year(datetime.get_date(fmt.calendar).year, fmt.calendar):
            res['LeapYear'] = "Yes"
        else:
            res['LeapYear'] = "No"
        return res

    def format_as_ymdhms(self, datetime, fmt):
        """
        Format the date and time.

        :param: datetime The date and time to format as a JulianDateTime object.
        :param: fmt A class of type DateTimeFormat specifying the formatting to use.

        :return: YYYY-MM-DD HH:MM::SS.ddd
        """

        calendar = fmt.calendar
        precision_seconds = fmt.precision_seconds
        month_name = fmt.month_name
        month_abbreviation_length = fmt.month_abbreviation_length
        date_sep = fmt.date_sep
        date_time_sep = fmt.date_time_sep
        time_sep = fmt.time_sep

        prec = self.check_precision(precision_seconds)

        ymdhms = YMDHMS(datetime.get_date_time(calendar))

        # Normalise for display
        ymd = ymdhms.get_ymd()
        hms = ymdhms.get_hms()
        hms.second = self.scale(hms.second, prec)
        if hms.second >= 60.0:
            hms.minute += 1
            hms.second -= 60
        if hms.minute > 59:
            hms.hour += 1
            hms.minute -= 60
        if hms.hour > 23:
            hms.hour -= 24
            jdt = JulianDateTime()
            jdt.set_date(ymd, calendar)
            jdt += 1
            d = jdt.get_date_time(calendar).get_ymd()
            ymd.set_ymd(d.year, d.month, d.day)
        if month_name != MonthName.no_name:
            months = MonthNames()
            if month_name == MonthName.full_name:
                mn = months.get_month_name(ymdhms.get_ymd().month)
            else:
                mn = months.get_abbreviated_month_name(
                    ymdhms.get_ymd().month, month_abbreviation_length)
            date_str = f'{ymd.year:4d}{date_sep:s}{mn:s}{date_sep:s}{ymd.day:02d}'
        else:
            date_str = f'{ymd.year:4d}{date_sep:s}{ymd.month:02d}{date_sep:s}{ymd.day:02d}'
        date_str += date_time_sep
        if prec > 0:
            width = prec + 3
            fmt_flt = f'0{width}.{prec}f'
            date_str += f'{hms.hour:02d}{time_sep:s}{hms.minute:02d}{time_sep:s}{hms.second:{fmt_flt}}'
        else:
            date_str += f'{hms.hour:02d}{time_sep:s}{hms.minute:02d}{time_sep:s}{int((hms.second + 0.5)):02d}'
        return date_str

    def format_date_with_names(self, datetime, fmt):
        """
        Format the date and time as: DayName, DD MonthName Year HH:MM::SS.ddd.

        If fmt.day_name == NONE and fmt.month_name == NONE then
        YYYY-MM-DD HH:MM::SS.ddd is returned.

        Note: The date separator and date time separator are not used here,
              the convention is to use a space.

        :param: datetime The date and time to format as a JulianDateTime object.
        :param: fmt A class of type DateTimeFormat specifying the formatting to use.

        :return: The formatted date.
        """

        calendar = fmt.calendar
        precision_seconds = fmt.precision_seconds
        day_name = fmt.day_name
        day_abbreviation_length = fmt.day_abbreviation_length
        month_name = fmt.month_name
        month_abbreviation_length = fmt.month_abbreviation_length
        time_sep = fmt.time_sep

        if day_name == DayName.no_name and month_name == MonthName.no_name:
            return self.format_as_ymdhms(datetime, fmt)

        prec = self.check_precision(precision_seconds)

        ymdhms = datetime.get_date_time(calendar)
        ymd = ymdhms.get_ymd()
        hms = ymdhms.get_hms()
        mn = MonthNames()
        if month_name == MonthName.full_name:
            month = mn.get_month_name(ymd.month)
        else:
            month = mn.get_abbreviated_month_name(ymd.month, month_abbreviation_length)
        dn = DayNames()
        if day_name == DayName.full_name:
            day = dn.get_day_name(datetime.get_dow(calendar, FirstDOW.sunday))
        else:
            day = dn.get_abbreviated_day_name(datetime.get_dow(calendar, FirstDOW.sunday), day_abbreviation_length)

        # The date separator and date time separator are not used here,
        # the convention is to use a space.
        date_str = f'{day:s}, {ymd.day:02d} {month:s} {ymd.year:4d}'

        if prec > 0:
            width = prec + 3
            fmt_flt = f'0{width}.{prec}f'
            time_str = f'{hms.hour:02d}{fmt.time_sep:s}{hms.minute:02d}{time_sep:s}{hms.second:{fmt_flt}}'
        else:
            time_str = f'{hms.hour:02d}{time_sep:s}{hms.minute:02d}{time_sep:s}{int((hms.second + 0.5)):02d}'
        return f'{date_str} {time_str}'

    def format_jd_long(self, datetime, fmt):
        """
        Format the date and time.

        :param: datetime The datetime and time to format as a JulianDateTime object.
        :param: fmt A class of type DateTimeFormat specifying the formatting to use.

        :return: The Julian date as a formatted string.
        """

        precision_fod = fmt.precision_fod

        # This will be normalised automatically.
        jd = JulianDateTime(datetime)

        rounding = 0.5

        prec = self.check_precision(precision_fod)

        if prec == 0:
            tmp = jd.get_jd_double()
            if tmp >= 0:
                tmp += (rounding + 0.01)
            else:
                tmp -= (rounding + 0.01)

            return f'{int(tmp):d}'

        jdn = jd.get_jdn()
        fod = jd.get_fod()

        is_negative = False
        if jdn < 0:
            is_negative = True
            if fod > 0.5:
                jdn += 1
            if fod != 0:
                fod = 1.0 - fod

        if fod != 0:
            fod = self.scale(fod, prec)

        if is_negative:
            s0 = '-'
        else:
            s0 = ''

        fmt = f'1.{prec:d}f'
        res = f'{abs(fod):{fmt}}'.split('.')
        if len(res) > 1:
            if fod >= 0.5 and jdn < 0:
                return f'{s0:s}{abs(jdn + 1):d}.{res[1]:s}'
            else:
                return f'{s0:s}{abs(jdn):d}.{res[1]:s}'
        else:
            return f'{fod:{fmt}}'

    def format_time_interval(self, timeinterval, fmt):
        """
        Format the time interval as days and fractions of a day.

        :param: timeInterval The Time Interval to format.
        :param: fmt A class of type DateTimeFormat specifying the formatting to use.

        :return: The Time Interval as a formatted string.
        """

        precision_fod = fmt.precision_fod

        ti = TimeInterval(timeinterval)
        ti.normalise()

        prec = self.check_precision(precision_fod)

        rounding = 0.5

        if prec == 0:
            tmp = ti.get_time_interval_double()
            if tmp >= 0:
                tmp += (rounding + 0.01)
            else:
                tmp -= (rounding + 0.01)

            return f'{int(tmp):d}'

        delta_day = ti.get_delta_day()
        delta_fod = ti.get_delta_fod()

        is_negative = False
        if delta_day < 0:
            is_negative = True
        else:
            if delta_fod < 0:
                is_negative = True

        delta_day = abs(delta_day)
        delta_fod = abs(delta_fod)

        if is_negative:
            s0 = '-'
        else:
            s0 = ''

        fmt = f'1.{str(prec)}f'
        s = f'{abs(delta_fod):{fmt}}'.split('.')
        return f'{s0}{delta_day + int(s[0]):d}.{s[1]:s}'

    def format_time_interval_dhms(self, timeinterval, fmt):
        """
        Format the time interval as days and hours minutes and seconds.

        :param: timeinterval The Time Interval to format.
        :param: fmt A class of type DateTimeFormat specifying the formatting to use.

        :return: The Time Interval as a formatted string.
        """

        precision_seconds = fmt.precision_seconds
        day_time_sep = fmt.day_time_sep
        time_sep = fmt.time_sep

        ti = TimeInterval(timeinterval)
        ti.normalise()

        prec = self.check_precision(precision_seconds)

        # Normalise
        delta_day = ti.get_delta_day()
        delta_fod = ti.get_delta_fod()

        is_negative = False
        if delta_day < 0:
            is_negative = True
        else:
            if delta_fod < 0:
                is_negative = True

        delta_day = abs(delta_day)
        delta_fod = abs(delta_fod)

        hms = TimeConversions().h_to_hms(delta_fod * 24.0)

        if is_negative:
            s0 = f'-{delta_day:d}d{day_time_sep:s}-'
        else:
            s0 = f'{delta_day:d}d{day_time_sep:s} '

        if prec > 0:
            width = prec + 3
            fmt = f'0{width:d}.{prec:d}f'
            return f'{s0:s}{hms.hour:02d}{time_sep:s}{hms.minute:02d}{time_sep:s}{hms.second:{fmt}}'
        else:
            return f'{s0:s}{hms.hour:02d}{time_sep:s}{hms.minute:02d}{time_sep:s}{int(math.ceil(hms.second)):02d}'

    def format_doy(self, datetime, fmt):
        """
        Format the day of the year.

        :param: datetime The date and time to format as a JulianDateTime object.
        :param: fmt A class of type DateTimeFormat specifying the formatting to use.

        :return: The day of the year as a formatted string.
        """

        calendar = fmt.calendar

        doy = datetime.get_doy(calendar)
        fod = datetime.get_fod_midnight()
        if fod == 1.0:
            fod = 0
        ti = TimeInterval(doy, fod)

        return self.format_time_interval(ti, fmt)

    @staticmethod
    def format_iso_week(datetime):
        """
        Format an ISO Week Date.

        :param: datetime The date and time to format as a JulianDateTime object.
        :param: fmt A class of type DateTimeFormat specifying the formatting to use.
        :return: The date formatted as YYYY-WW-D.
        """

        iwd = datetime.get_iso_week()
        ret = f'{iwd.year:4d}-{iwd.week_number:02d}-{iwd.week_day:1d}'
        return ret

    def format_as_ymd(self, datetime, fmt):
        """
        Format the day of the year.

        :param: datetime The date and time to format as a JulianDateTime object.
        :param: fmt A class of type DateTimeFormat specifying the formatting to use.

        :return: The date as a formatted string.
        """
        res = self.format_as_ymdhms(datetime, fmt).split(fmt.date_time_sep)
        return res[0]

    def format_as_hms(self, datetime, fmt):
        """
        Format the time as HH:MM::SS.ddd.

        :param: datetime The date and time to format as a JulianDateTime object.
        :param: fmt A class of type DateTimeFormat specifying the formatting to use.

        :return: The formatted time.
        """
        res = self.format_as_ymdhms(datetime, fmt).split(fmt.date_time_sep)
        return res[1]

    def format_jd_short(self, datetime, fmt):
        """
        Format the short form of the Julian date.

        :param: datetime The date and time to format as a JulianDateTime object.
        :param: fmt A class of type DateTimeFormat specifying the formatting to use.

        :return: The Julian date as a formatted string.
        """

        precision_fod = fmt.precision_fod

        prec = self.check_precision(precision_fod)

        fmt = f'02.{prec}f'
        return f'{datetime.get_jd_double():{fmt}}'

    @staticmethod
    def format_tz_as_hm(hm):
        """
        Format the time zone as (+|-)HHMM.

        :param: hm The timezone to format.

        :return: The timezone as a formatted string.
        """
        t = HM(hm)
        if t.hour < 0 or t.minute < 0:
            sgn = '-'
        else:
            sgn = '+'
        ret = f'{sgn:s}{abs(t.hour):02d}{int(abs(t.minute)):02d}'
        return ret
