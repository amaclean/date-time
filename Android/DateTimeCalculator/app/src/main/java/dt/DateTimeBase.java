/*=========================================================================

  Program:   Date Time Library
  File   :   DateTimeBase.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

/*
  The date Time Library
 */
package dt;

/**
 * @author Andrew J. P. Maclean
 */
public class DateTimeBase {

  /**
   * For 64-bit arithmetic, the relative tolerance of the fraction of the day.
   */
  public final double rel_fod = 1.0e-14;
  /**
   * For 64-bit arithmetic, the relative tolerance of the minutes in a day.
   */
  public final double rel_minutes = 1.0e-11;
  /**
   * For 64-bit arithmetic, the relative tolerance of the seconds in a day.
   */
  public final double rel_seconds = 1.0e-9;
  /**
   * For 64-bit arithmetic, the absolute tolerance of the seconds in a day.
   */
  public final double abs_seconds = 1.0e-10;

  /**
   * Test two doubles for equality.
   * <p>
   * Based on:
   * the funnction isclose in
   * [mathmodule.c](https://github.com/python/cpython/blob/master/Modules/mathmodule.c)
   * <p>
   * Note: NaN is not close to anything, even itself. inf and -inf are only
   * close to themselves.
   *
   * @param a:       The first number
   * @param b:       The second number
   * @param rel_tol: relative tolerance, (e.g 1e-09) maximum difference for being
   *                 considered "close", relative to the magnitude of the input
   *                 values i.e abs(b - a) / min(abs(b), abs(a))
   * @param abs_tol: absolute tolerance, (e.g 0.0) maximum difference for being
   *                 considered "close", regardless of the magnitude of the
   *                 input values i.e. abs(b - a)
   * @return true if a, b are close, false otherwise
   */
  public boolean isclose(double a, double b,
                         double rel_tol, double abs_tol) {
    if (abs_tol < 0 || rel_tol < 0) {
      String s = "Tolerances must be non-negative";
      throw new ArithmeticException(s);
    }

    // Short-circuit exact equality -- needed to catch two infinities of the
    // same sign. And perhaps speeds things up a bit sometimes.
    if (a == b) {
      return true;
    }
    // Catches the case of two infinities of opposite sign, or one infinity and
    // one finite number. Two infinities of opposite sign would otherwise have
    // an infinite relative tolerance. Two infinities of the same sign are
    // caught by the equality check above.
    if (Double.isInfinite(a) || Double.isInfinite(b)) {
      return false;
    }
    double diff = Math.abs(b - a);

    return ((diff <= Math.abs(rel_tol * b)) ||
        (diff <= Math.abs(rel_tol * a))) ||
        (diff <= abs_tol);
  }

  /**
   * Test two doubles for equality.
   * <p>
   * Assumes a relative tolerance of 1.0e-09 and an absolute tolerance of 0.0.
   *
   * @param a: The first number
   * @param b: The second number
   * @return true if a, b are close, false otherwise
   */
  public boolean isclose(double a, double b) {
    return isclose(a, b, 1.0e-09, 0.0);
  }

  /**
   * Test two doubles for equality.
   * <p>
   * Assumes an absolute tolerance of 0.0.
   *
   * @param a:       The first number
   * @param b:       The second number
   * @param rel_tol: relative tolerance, (e.g 1e-09) maximum difference for being
   *                 considered "close", relative to the magnitude of the input
   *                 values i.e abs(b - a) / min(abs(b), abs(a))
   * @return true if a, b are close, false otherwise
   */
  public boolean isclose(double a, double b, double rel_tol) {
    return isclose(a, b, rel_tol, 0.0);
  }
}
