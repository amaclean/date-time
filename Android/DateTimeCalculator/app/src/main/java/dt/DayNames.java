/*=========================================================================

  Program:   Date Time Library
  File   :   DayNames.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

/*

 */
package dt;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * This class provides a convenient way of accessing either day names or
 * abbreviated day names based on the index on the day. Days are indexed from 1
 * to 7, with 1 being Sunday and day 7 being Saturday.
 * <p>
 * The language used is English, however you can set the day names to other
 * languages.
 *
 * @author Andrew J. P. Maclean
 */
public class DayNames {

  private final Map<Integer, String> names = new HashMap<>();

  public DayNames() {
    names.put(1, "Sunday");
    names.put(2, "Monday");
    names.put(3, "Tuesday");
    names.put(4, "Wednesday");
    names.put(5, "Thursday");
    names.put(6, "Friday");
    names.put(7, "Saturday");
  }

  // Assignment

  /**
   * @param dn the day names.
   */
  public DayNames(DayNames dn) {
    this.names.clear();
    for (Entry<Integer, String> pair : dn.names.entrySet()) {
      this.names.put(pair.getKey(), pair.getValue());
    }
  }

  /**
   * assignment
   *
   * @param rhs The day names.
   */
  public void assign(DayNames rhs) {
    this.names.clear();
    for (Entry<Integer, String> pair : rhs.names.entrySet()) {
      this.names.put(pair.getKey(), pair.getValue());
    }
  }

  /**
   * @param rhs The time.
   * @return true if this == rhs, false otherwise
   */
  public boolean eq(DayNames rhs) {
    return this.names.equals(rhs.names);
  }

  /**
   * @param rhs The time.
   * @return true if this != rhs, false otherwise
   */
  public boolean ne(DayNames rhs) {
    return !this.eq(rhs);
  }

  /**
   * Get the day name.
   * <p>
   * An empty string is returned if the index of the day lies outside the
   * range 1...7.
   *
   * @param day The day number
   * @return The name of the day or an empty string if not found.
   */
  public String GetDayName(int day) {
    if (names.containsKey(day)) {
      return names.get(day);
    }
    return "";
  }

  /**
   * Get the abbreviated day name.
   * <p>
   * An empty string is returned if the index of the day lies outside the
   * range 1...7.
   * <p>
   * For other languages you may need to select a different number of letters
   * to ensure a unique abbreviation. For English, two letters are sufficient,
   * but three are used.
   *
   * @param day  The day number.
   * @param size The number of letters used to form the abbreviation.
   * @return The abbreviated day name or an empty string if not found.
   */
  public String GetAbbreviatedDayName(int day, int size) {
    if (names.containsKey(day)) {
      String name = names.get(day);
      if (size <= name.length()) {
        return name.substring(0, size);
      }
      return name;
    }
    return "";
  }

  /**
   * Set the day name. Useful when using a different language.
   * <p>
   * There are 7 days and the first day starts at 1. No updates are performed
   * if the index of the day lies outside the range 1...7.
   *
   * @param day     The day number
   * @param dayName The name of the day.
   */
  public void SetDayName(int day, String dayName) {
    if (day > 0 && day < 8) {
      names.put(day, dayName);
    }
  }

  /**
   * @return A string corresponding to the contents of the class.
   */
  @Override
  public String toString() {
    return String.format("(names: %s)", this.names);
  }
}
