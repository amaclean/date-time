/*=========================================================================

  Program:   Date Time Library
  File   :   MONTH_NAME.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

package dt;

/**
 * Enumerate whether to use a day name or not.
 *
 * @author Andrew J. P. Maclean
 */
public enum MONTH_NAME {
  /// @cond
  NONE,
  ABBREVIATED,
  UNABBREVIATED
  /// @endcond
}
