/*=========================================================================

  Program:   Date Time Library
  File   :   JulianDateTime.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/*

 */
package dt;

//import android.util.Pair;

/**
 * This class stores the date and time as a Julian Date split into an integral
 * and fractional part for maximum precision.
 * <p>
 * Operators are provided for comparisons, difference arithmetic and
 * incrementing and decrementing dates.
 * <p>
 * The classes JD, YMD, YMDHMS, HM, HMS, TimeInterval and ISOWeekDate are
 * convenience classes allowing parameters to be returned from various
 * functions.
 *
 * @author Andrew J. P. Maclean
 */
public class JulianDateTime extends DateTimeConversions {

  private final JD jd = new JD();

  // Default value corresponds to -4712-01-01 00:00:00 = JD0
  public JulianDateTime() {
  }

  /**
   * @param year     Year
   * @param month    Month
   * @param day      Day
   * @param hour     Hour
   * @param minute   Minute
   * @param second   Second
   * @param calendar The calendar used by the date.
   */
  public JulianDateTime(int year, int month, int day, int hour, int minute, double second, CALENDAR_TYPE calendar) {
    YMDHMS ymdhms = new YMDHMS(year, month, day, hour, minute, second);
    this.SetDateTime(ymdhms, calendar);
  }

  /**
   * @param jdn The Julian Day Number, running from noon to noon.
   * @param fod The Fraction of the Day, running from noon to noon.
   */
  public JulianDateTime(double jdn, double fod) {
    this.jd.SetJDN((int) jdn);
    this.jd.SetFoD(jdn - (int) jdn + fod);
  }

  /**
   * @param ymdhms   The date and time.
   * @param calendar The calendar used by the date.
   */
  public JulianDateTime(YMDHMS ymdhms, CALENDAR_TYPE calendar) {
    this.SetDateTime(ymdhms, calendar);
  }

  /**
   * @param dt A DT object.
   */
  public JulianDateTime(JD dt) {
    this.jd.SetJDN(dt.GetJDN());
    this.jd.SetFoD(dt.GetFoD());
  }

  // Copy constructor

  /**
   * @param dt A JulianDateTime object.
   */
  public JulianDateTime(JulianDateTime dt) {
    this.jd.SetJDN(dt.jd.GetJDN());
    this.jd.SetFoD(dt.jd.GetFoD());
  }

  /**
   * Checks the two objects for equality.
   *
   * @param o the object to which this one is to be checked for equality
   * @return true if equal
   */
  @Override
  public boolean equals(Object o) {
    // Same object
    if (this == o) {
      return true;
    }
    // Check if the other object has the correct type.
    if (!(o instanceof JulianDateTime)) {
      return false;
    }
    // Cast to the appropriate type.
    JulianDateTime p = (JulianDateTime) o;
    // Check the reference fields.
    return this.GetJDFullPrecision().equals(p.GetJDFullPrecision());
  }

  /**
   * @param rhs Julian Date Time.
   * @return true if this == rhs, false otherwise
   */
  public boolean eq(JulianDateTime rhs) {
    return this.equals(rhs);
  }

  /**
   * @param rhs Julian Date Time.
   * @return true if this != rhs, false otherwise
   */
  public boolean ne(JulianDateTime rhs) {
    return !this.equals(rhs);
  }

  /**
   * @param rhs A JulianDateTime object.
   * @return true if this &lt; rhs
   */
  public boolean lt(JulianDateTime rhs) {
    return this.jd.GetJDN() < rhs.jd.GetJDN() || (this.jd.GetJDN() == rhs.jd.GetJDN()) && (this.jd.GetFoD() < rhs.jd.GetFoD());
  }

  /**
   * @param rhs A JulianDateTime object.
   * @return true if this &le; rhs
   */
  public boolean le(JulianDateTime rhs) {
    return !(this.gt(rhs));
  }

  /**
   * @param rhs A JulianDateTime object.
   * @return true if this &gt; rhs
   */
  public boolean gt(JulianDateTime rhs) {
    return this.jd.GetJDN() > rhs.jd.GetJDN() || (this.jd.GetJDN() == rhs.jd.GetJDN()) && (this.jd.GetFoD() > rhs.jd.GetFoD());
  }

  /**
   * @param rhs A JulianDateTime object.
   * @return true if this &ge; rhs
   */
  public boolean ge(JulianDateTime rhs) {
    return !(this.lt(rhs));
  }

  /**
   * Compare Julian Date Times for equality.
   *
   * @param jdt The Julian Date Time to be compared with this.
   * @param eps The precision.
   * @return true if the date times are equivalent.
   */
  public boolean IsClose(JulianDateTime jdt, double eps) {
    // Same object
    return this == jdt || this.jd.IsClose(jdt.jd, eps);
  }

  // Addition/Subtraction

  /**
   * +
   *
   * @param rhs A JulianDateTime object.
   * @return thist + rhs
   */
  public JulianDateTime add(JulianDateTime rhs) {
    JulianDateTime result = new JulianDateTime();
    result.jd.SetJDN(this.jd.GetJDN() + rhs.jd.GetJDN());
    result.jd.SetFoD(this.jd.GetFoD() + rhs.jd.GetFoD());
    return result;
  }

  /**
   * +
   *
   * @param days Days
   * @return this + days
   */
  public JulianDateTime add(double days) {
    JulianDateTime result = new JulianDateTime();
    result.jd.SetJDN(this.jd.GetJDN() + (int) days);
    result.jd.SetFoD(this.jd.GetFoD() + (days - (int) days));
    return result;
  }

  /**
   * += Increment the date by a JulianDateTime value.
   *
   * @param rhs A JulianDateTime object.
   */
  public void inc(JulianDateTime rhs) {
    this.jd.SetJDN(this.jd.GetJDN() + rhs.jd.GetJDN());
    this.jd.SetFoD(this.jd.GetFoD() + rhs.jd.GetFoD());
  }

  /**
   * += Increment the date by the day value.
   *
   * @param days Days
   */
  public void inc(double days) {
    this.jd.SetJDN(this.jd.GetJDN() + (int) days);
    this.jd.SetFoD(this.jd.GetFoD() + (days - (int) days));
  }

  /**
   * -
   *
   * @param rhs A JulianDateTime object.
   * @return this - rhs
   */
  public JulianDateTime sub(JulianDateTime rhs) {
    JulianDateTime result = new JulianDateTime();
    result.jd.SetJDN(this.jd.GetJDN() - rhs.jd.GetJDN());
    result.jd.SetFoD(this.jd.GetFoD() - rhs.jd.GetFoD());
    return result;
  }

  /**
   * -
   *
   * @param days Days
   * @return this - days
   */
  public JulianDateTime sub(double days) {
    JulianDateTime result = new JulianDateTime();
    result.jd.SetJDN(this.jd.GetJDN() - (int) days);
    result.jd.SetFoD(this.jd.GetFoD() - (days - (int) days));
    return result;
  }

  /**
   * -= Decrement the date by a JulianDateTime value.
   *
   * @param rhs A JulianDateTime object.
   */
  public void dec(JulianDateTime rhs) {
    this.jd.SetJDN(this.jd.GetJDN() - rhs.jd.GetJDN());
    this.jd.SetFoD(this.jd.GetFoD() - rhs.jd.GetFoD());
  }

  /**
   * -= Decrement the date by the day value.
   *
   * @param days Days
   */
  public void dec(double days) {
    this.jd.SetJDN(this.jd.GetJDN() - (int) days);
    this.jd.SetFoD(this.jd.GetFoD() - (days - (int) days));
  }

  /**
   * assignment
   *
   * @param rhs The time.
   */
  public void assign(JulianDateTime rhs) {
    this.jd.SetJDN(rhs.jd.GetJDN());
    this.jd.SetFoD(rhs.jd.GetFoD());
  }

  /**
   * Get the Julian Date to its full precision. The precision of the returned
   * value will be of the order of 1.0e-14 of a day or 1ns.
   *
   * @return The Julian Day Number of the date and the fraction of the Day
   * running from noon to noon.
   */
  public JD GetJDFullPrecision() {
    return new JD(this.jd);
  }

  /**
   * Get the Julian Day Number.
   *
   * @return Julian Day Number
   */
  public int GetJDN() {
    return this.jd.GetJDN();
  }

  /**
   * Get the fraction of the day.
   *
   * @return Fraction of the Day
   */
  public double GetFoD() {
    return this.jd.GetFoD();
  }

  /**
   * Get the Julian Date as days and decimals of the day.
   * <p>
   * The precision of the returned value will be of the order of 1.0e-8 of a
   * day or 1ms.
   *
   * @return The Julian Day Number of the date as days and decimals of the
   * day.
   */
  public double GetJDDouble() {
    return this.jd.GetJDDouble();
  }

  /**
   * Get the fraction of the day running from midnight to midnight.
   *
   * @return Fraction of the Day
   */
  public double GetFoDMidnight() {
    return this.jd.GetFoDMidnight();
  }

  /**
   * Get the time interval. The precision of the returned value will be of the
   * order of 1.0e-14 of a day or 1ns.
   *
   * @return The time interval as deltaDay, the number of days and deltaFoD,
   * the fraction of the day.
   */
  public TimeInterval GetDeltaT() {
    return new TimeInterval(this.jd.GetJDN(), this.jd.GetFoD());
  }

  /**
   * Set the time interval.
   *
   * @param dt The time interval.
   */
  public void SetDeltaT(TimeInterval dt) {
    this.jd.SetJDN(dt.GetDeltaDay());
    this.jd.SetFoD(dt.GetDeltaFoD());
  }

  /**
   * Get the date and time. The calendar to use is specified by the enumerated
   * values in CalendarType.
   *
   * @param calendar This parameter specifies the calendar to use.
   * @return The date and time.
   */
  public YMDHMS GetDateTime(CALENDAR_TYPE calendar) {

    DTPair<Integer, Double> ret = this.FoDToHMS(this.jd.GetJDN(), this.jd.GetFoD());
    YMDHMS ymdhms = new YMDHMS();
    ymdhms.hms = this.h2hms(ret.second * 24.0);
    switch (calendar) {
      case JULIAN:
        ymdhms.ymd = this.jd2cdJ(ret.first);
        break;
      case GREGORIAN:
        ymdhms.ymd = this.jd2cdG(ret.first);
        break;
      case JULIAN_GREGORIAN:
      default:
        ymdhms.ymd = this.jd2cdJG(ret.first);
    }
    return ymdhms;
  }

  /**
   * Get the date.
   *
   * @param calendar The calendar used by the date.
   * @return The date.
   */
  public YMD GetDate(CALENDAR_TYPE calendar) {
    return this.GetDateTime(calendar).ymd;
  }

  /**
   * Get the time.
   *
   * @return The hours, minutes and seconds.
   */
  public HMS GetTimeHMS() {
    return this.GetDateTime(CALENDAR_TYPE.JULIAN_GREGORIAN).hms;
  }

  /**
   * Get the time.
   *
   * @return The hours and minutes.
   */
  public HM GetTimeHM() {
    return this.h2hm(this.hms2h(this.GetDateTime(CALENDAR_TYPE.JULIAN_GREGORIAN).hms.GetHMS()));
  }

  /**
   * Set the date and time. The calendar to use is specified by the enumerated
   * values in CalendarType.
   *
   * @param ymdhms   The date and time.
   * @param calendar The calendar used by the date.
   */
  public void SetDateTime(YMDHMS ymdhms, CALENDAR_TYPE calendar) {
    double hms = this.hms2h(ymdhms.hms) / 24.0;
    int jdn;

    switch (calendar) {
      case JULIAN:
        jdn = this.cd2jdJ(ymdhms.ymd);
        break;
      case GREGORIAN:
        jdn = this.cd2jdG(ymdhms.ymd);
        break;
      case JULIAN_GREGORIAN:
      default:
        jdn = this.cd2jdJG(ymdhms.ymd);
    }
    DTPair<Integer, Double> ret = this.HMSToFoD(jdn, hms);
    this.jd.SetJD(ret.first, ret.second);
  }

  /**
   * Set the date and time.
   *
   * @param jdn The Julian Date.
   * @param fod The fraction of the day, running from noon to noon.
   */
  public void SetDateTime(double jdn, double fod) {
    this.jd.SetJDN((int) jdn);
    this.jd.SetFoD(jdn - (int) jdn + fod);
  }

  /**
   * Set the date.
   * <p>
   * The time is set to 00:00:00.0 The calendar to use is specified by the
   * enumerated values in CalendarType.
   *
   * @param ymd      The year, month and day of the date.
   * @param calendar The calendar used by the date.
   */
  public void SetDate(YMD ymd, CALENDAR_TYPE calendar) {
    YMDHMS ymdhms = new YMDHMS();
    ymdhms.SetYMD(ymd);
    this.SetDateTime(ymdhms, calendar);
  }

  /**
   * Set the time.
   * <p>
   * The date is set to 2000-01-01 (Julian/Gregorian Calendar). This is to
   * ensure that negative Julian dates are not encountered.
   * <p>
   * The time is normalised to the range 0 &le; t &lt; 24 so the date will
   * change if times lie outside this range.
   *
   * @param hms The time as hours, minutes and seconds.
   */
  public void SetTime(HMS hms) {
    YMDHMS ymdhms = new YMDHMS();
    ymdhms.SetYMD(2000, 1, 1);
    ymdhms.SetHMS(hms);
    this.SetDateTime(ymdhms, CALENDAR_TYPE.JULIAN_GREGORIAN);
  }

  /**
   * Set the time.
   * <p>
   * The date is set to 2000-01-01 (Julian/Gregorian Calendar).
   * <p>
   * The time is normalised to the range 0 &le; t &lt; 24 so the date will
   * change if times lie outside this range.
   *
   * @param hm The time as hours and minutes.
   */
  public void SetTime(HM hm) {
    HMS hms = new HMS();
    hms.SetHMS(this.h2hms(this.hm2h(hm)));
    this.SetTime(hms);
  }

  /**
   * Set the Julian Day Number.
   *
   * @param jdn The Julian Day Number.
   */
  public void SetJDN(int jdn) {
    this.jd.SetJDN(jdn);
  }

  /**
   * Set the fraction of the day.
   *
   * @param fod The fraction of the day.
   */
  public void SetFoD(double fod) {
    this.jd.SetFoD(fod);
  }

  /**
   * Get the day of the week in a calendar agnostic way.
   *
   * @param calendar The calendar used by the date.
   * @param firstDoW The first day of the week, either Sunday or Monday.
   * @return The day of the week (1..7).
   */
  public int GetDoW(CALENDAR_TYPE calendar, FIRST_DOW firstDoW) {
    YMD ymd = this.GetDate(calendar);
    // The day of the week assumes the calendar type is Julian/Gregorian.
    int d = ymd.year * 10000 + ymd.month * 100 + ymd.day;
    YMD ymd1 = new YMD(ymd);
    if (d < JG_CHANGEOVER.GREGORIAN_START_DATE.getValue()) {
      // For dates less than 1582-10-15, use the Julian Calendar
      return this.dow(ymd1, CALENDAR_TYPE.JULIAN, firstDoW);
    } else {
      // For dates greater than or equal to 1582-10-15, use the Gregorian
      // Calendar
      return this.dow(ymd1, CALENDAR_TYPE.GREGORIAN, firstDoW);
    }
  }

  /**
   * Get the day of the year.
   *
   * @param calendar The calendar used by the date, normally JULIAN_GREGORIAN.
   * @return The day of the year.
   */
  public int GetDoY(CALENDAR_TYPE calendar) {
    YMD ymd = new YMD(this.GetDate(calendar));
    switch (calendar) {
      case JULIAN:
        return this.doyJ(ymd);
      case GREGORIAN:
        return this.doyG(ymd);
      case JULIAN_GREGORIAN:
      default:
        return this.doyJG(ymd);
    }
  }

  /**
   * Get the week of the year. See: http://en.wikipedia.org/wiki/ISO_week_date
   * Note: This algorithm only works for Gregorian dates.
   *
   * @return The ISO week date.
   */
  public ISOWeekDate GetISOWeek() {
    YMD ymd = new YMD(this.GetDate(CALENDAR_TYPE.GREGORIAN));
    return this.cd2woy(ymd);
  }

  /**
   * @return A string corresponding to the contents of the class.
   */
  @Override
  public String toString() {
    return String.format("%d, %19.15f", this.jd.GetJDN(), this.jd.GetFoD());
  }

  /**
   * Normalise so that 0 &le; fod &lt; 1.
   */
  protected void Normalise() {
    this.jd.Normalise();
  }

  /**
   * Convert the fraction of the day running from noon to noon to the fraction
   * of the day running from midnight to midnight.
   *
   * @param jdn The julian day number.
   * @param fod The fraction of the day running from noon to noon (always
   *            positive).
   * @return A pair holding the count of days and fraction of the day running
   * from midnight to midnight.
   */
  public DTPair<Integer, Double> FoDToHMS(int jdn, double fod) {
    DTPair<Integer, Double> ret = new DTPair<>(jdn, fod + 0.5);
    if (ret.second >= 1.0) { // Next day.
      ret.second -= 1.0;
      ++ret.first;
    }
    return ret;
  }

  /**
   * Convert the fraction of the day running from midnight to midnight to the
   * fraction of the day running from noon to noon.
   *
   * @param days The count of days.
   * @param hms  The fraction of the day running from midnight to midnight
   *             (always positive).
   * @return A pair holding the the julian day number and the fraction of the
   * day running from noon to noon (always positive).
   */
  public DTPair<Integer, Double> HMSToFoD(int days, double hms) {
    DTPair<Integer, Double> ret = new DTPair<>(days, hms - 0.5);
    if (ret.second < 0) {
      ret.second += 1.0; // Previous day.
      --ret.first;
    }
    return ret;
  }

}
