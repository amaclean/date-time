package dt;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A convenience class for parsing dates and times.
 * <p>
 * All the parsers return a variable of type DTPair &lt; Integer,T &gt; where
 * the integer value corresponds to the states returned from the parser. These
 * values are:
 * <ul>
 * <li>1 = Parsed Ok.</li>
 * <li>2 = No data.</li>
 * <li>3 = Invalid date.</li>
 * <li>4 = Bad month.</li>
 * <li>5 = Invalid time.</li>
 * <li>6 = Invalid integer or decimal number.</li>
 * </ul>
 *
 * @author Andrew J. P. Maclean
 */
public class DateTimeParser {

  /**
   * Parser messages.
   * <p>
   * All the parsers return a variable of type DTPair &lt; Integer,T &gt;
   * where the integer value corresponds to the states returned from the
   * parser.
   * <p>
   * Change these messages to whatever language you need by changing the
   * message strings in your code.
   */
  public final HashMap<Integer, String> parserMessages = new HashMap<>();
  private final Pattern intNum = Pattern.compile("^[+-]?\\d+$");
  private final Pattern decNum = Pattern.compile("^([+-]?\\d+)(\\.((\\d+)?))?$");
  // private final Pattern sciNum =
  // Pattern.compile("^([+-]?\\d+)(\\.((\\d+)?))?((e|E)([+-]?)\\d+)?$");
  private final Pattern datePattern = Pattern.compile("^[+-]?\\d+\\D(\\d{1,2}|\\w{3,})\\D\\d{1,2}$");
  private final Pattern timePattern = Pattern
      .compile("^[+-]?(\\d{1,2}(\\D\\d{1,2})\\D\\d{1,2}(\\.((\\d+)?))?|\\d{1,2}(\\D\\d{1,2})?)$");

  public DateTimeParser() {
    // Change these messages to whatever language you need
    // by changing the message strings in your code.
    parserMessages.put(1, "Parsed Ok.");
    parserMessages.put(2, "No data.");
    parserMessages.put(3, "Invalid date.");
    parserMessages.put(4, "Bad month.");
    parserMessages.put(5, "Invalid time.");
    parserMessages.put(6, "Invalid integer or decimal number.");
  }

  /**
   * Parse a string of the form dd.dddd into its integer and fractional parts.
   *
   * @param decStr The string to tokenize.
   * @return A pair consisting of an integer and the parsed number, with the
   * integer taking the following values:
   * <ul>
   * <li>1 = Parsed Ok.</li>
   * <li>2 = No data.</li>
   * <li>5 = Invalid integer or decimal number.</li>
   * </ul>
   */
  public DTPair<Integer, DTPair<Integer, Double>> ParseDecNum(String decStr) {
    DTPair<Integer, DTPair<Integer, Double>> ret = new DTPair<>(2,
        new DTPair<>(0, 0.0));

    String s = decStr.trim();
    if (s.isEmpty()) {
      ret.first = 2;
      return ret;
    }
    Matcher match = decNum.matcher(s);
    if (!match.matches()) {
      ret.first = 6;
      return ret;
    }
    boolean isNegative = false;
    String[] sv = s.split("-");
    if (sv.length > 1) {
      isNegative = true;
      s = sv[1].trim();
    }
    sv = s.split("\\.");
    int intPart = 0;
    double fracPart = 0.0;
    if (sv.length == 1) {
      if (sv[0].length() != 0) {
        intPart = Integer.parseInt(sv[0].trim());
      }
    } else {
      if (sv.length > 1) {
        if (sv[0].length() != 0) {
          intPart = Integer.parseInt(sv[0].trim());
        }
        if (sv[1].length() != 0) {
          fracPart = Double.parseDouble("0." + sv[1].trim());
        }
      }
    }
    if (isNegative) {
      if (intPart != 0) {
        intPart = -intPart;
      }
      if (fracPart != 0) {
        fracPart = -fracPart;
      }
    }
    ret.first = 1;
    ret.second.first = intPart;
    ret.second.second = fracPart;
    return ret;
  }

  /**
   * Parse a date formatted as YYYY-MM-DD into a DT::YMD class.
   * <p>
   * In addition to being an integer in the range [01 ... 12], the month can
   * be either the month name or abbreviation.
   *
   * @param date The date string to parse.
   * @param fmt  The formatting to use.
   * @return A pair consisting of an integer and the parsed date, with the
   * integer taking the following values:
   * <ul>
   * <li>1 = Parsed Ok.</li>
   * <li>2 = No data.</li>
   * <li>3 = Invalid date.</li>
   * <li>4 = Bad month.</li>
   * </ul>
   */
  public DTPair<Integer, YMD> ParseDate(String date, DateTimeFormat fmt) {
    return this.ParseDate(date, fmt.dateSep);
  }

  /**
   * Parse a time formatted as HH:mm:ss.sss into a DT::HMS class.
   *
   * @param time The time string to parse.
   * @param fmt  The formatting to use.
   * @return A pair consisting of an integer and the parsed date, with the
   * integer taking the following values:
   * <ul>
   * <li>1 = Parsed Ok.</li>
   * <li>2 = No data.</li>
   * <li>5 = Invalid time.</li>
   * </ul>
   */
  public DTPair<Integer, HMS> ParseTime(String time, DateTimeFormat fmt) {
    return this.ParseTime(time, fmt.timeSep);
  }

  /**
   * Parse a date formatted as YYYY-MM-DD HH-mm-ss.sss into a DT::YMDHMS
   * class.
   * <p>
   * In addition to being an integer in the range [01 ... 12], the month can
   * be either the month name or abbreviation.
   * <p>
   * The time string is optional. If not present then the time is returned as
   * 00:00:00.00
   *
   * @param date The date string to parse.
   * @param fmt  The formatting to use.
   * @return A pair consisting of an integer and the parsed date, with the
   * integer taking the following values:
   * <ul>
   * <li>1 = Parsed Ok.</li>
   * <li>2 = No data.</li>
   * <li>3 = Invalid date.</li>
   * <li>4 = Bad month.</li>
   * <li>5 = Invalid time.</li>
   * </ul>
   */
  public DTPair<Integer, YMDHMS> ParseDateTime(String date, DateTimeFormat fmt) {
    return this.ParseDateTime(date, fmt.dateSep, fmt.dateTimeSep, fmt.timeSep);
  }

  /**
   * Parse a time interval formatted as d.dd HH:mm:ss.sss into a
   * DT::TimeInterval class.
   * <p>
   * d.dd can be a decimal number with an optional trailing "d".
   * <p>
   * The time string is optional. If not present then the time is returned as
   * 00:00:00.00
   * <p>
   * Note: The day and HMS part of the string have independent signs. This
   * means that for:
   * <ul>
   * <li>-1d -12:00:00.0, -1d 12:00:00.0, 1d -12:00:00.0, 1d 12:00:00.0</li>
   * </ul>
   * We get time intervals of:
   * <ul>
   * <li>(-1, -0.5), (-1, 0.5), (1, -0.5) (1, 0.5) respectively.</li>
   * </ul>
   *
   * @param ti  The time interval string to parse.
   * @param fmt The formatting to use.
   * @return A pair consisting of an integer and the parsed time interval,
   * with the integer taking the following values:
   * <ul>
   * <li>1 = Parsed Ok.</li>
   * <li>2 = No data.</li>
   * <li>5 = Invalid time.</li>
   * <li>6 = Invalid integer or decimal number.</li>
   * </ul>
   */
  public DTPair<Integer, TimeInterval> ParseTimeInterval(String ti, DateTimeFormat fmt) {
    return this.ParseTimeInterval(ti, fmt.dayTimeSep, fmt.timeSep);
  }

  /**
   * Parse a date formatted as YYYY-MM-DD into a DT::YMD class.
   * <p>
   * In addition to being an integer in the range [01 ... 12], the month can
   * be either the month name or abbreviation.
   *
   * @param date    The date string to parse.
   * @param dateSep The date separator.
   * @return A pair consisting of an integer and the parsed date, with the
   * integer taking the following values:
   * <ul>
   * <li>1 = Parsed Ok.</li>
   * <li>2 = No data.</li>
   * <li>3 = Invalid date.</li>
   * <li>4 = Bad month.</li>
   * </ul>
   */
  private DTPair<Integer, YMD> ParseDate(String date, char dateSep) {
    int year, month, day;
    year = 0;
    day = 0;

    DTPair<Integer, YMD> ret = new DTPair<>(2, new YMD());

    String dateStr = date.trim();
    if (dateStr.isEmpty()) {
      ret.first = 2;
      return ret;
    }
    Matcher match = datePattern.matcher(dateStr);
    if (!match.matches()) {
      ret.first = 3;
      return ret;
    }

    if (dateStr.charAt(0) == '-') {
      dateStr = dateStr.replaceFirst("-", "*");
    }

    String[] dateComponents = dateStr.split(Character.toString(dateSep));
    if (dateComponents[0].charAt(0) == '*') {
      dateComponents[0] = dateComponents[0].replaceFirst("\\*", "-");
    }

    if (dateComponents.length != 3) {
      ret.first = 3; // Need year, month, day
      return ret;
    }

    for (int i = 0; i < dateComponents.length; ++i) {
      dateComponents[i] = dateComponents[i].trim();
    }

    match = intNum.matcher(dateComponents[0]);
    if (match.matches()) {
      year = Integer.parseInt(dateComponents[0]);
    }
    int m = 0;
    if (dateComponents[1].length() <= 2) {
      // We have in integer month.
      match = intNum.matcher(dateComponents[1]);
      if (match.matches()) {
        m = Integer.parseInt(dateComponents[1]);
      }
    } else {
      MonthNames months = new MonthNames();
      for (int i = 1; i < 13; ++i) {
        if (months.GetMonthName(i).equals(dateComponents[1])
            || months.GetAbbreviatedMonthName(i, 3).equals(dateComponents[1])) {
          m = i;
          break;
        }
      }
    }
    if (m > 0 && m < 13) {
      month = m;
    } else {
      ret.first = 4;
      return ret;
    }
    match = intNum.matcher(dateComponents[2]);
    if (match.matches()) {
      day = Integer.parseInt(dateComponents[2]);
    }
    ret.second.SetYMD(year, month, day);
    ret.first = 1;
    return ret;
  }

  /**
   * Parse a time formatted as HH:mm:ss.sss into a DT::HMS class.
   *
   * @param time    The time string to parse.
   * @param timeSep The time separator.
   * @return A pair consisting of an integer and the parsed date, with the
   * integer taking the following values:
   * <ul>
   * <li>1 = Parsed Ok.</li>
   * <li>2 = No data.</li>
   * <li>5 = Invalid time.</li>
   * </ul>
   */
  private DTPair<Integer, HMS> ParseTime(String time, char timeSep) {

    int hour, minute;
    hour = minute = 0;
    double second = 0;

    DTPair<Integer, HMS> ret = new DTPair<>(2, new HMS());

    String timeStr = time.trim();
    if (timeStr.isEmpty()) {
      ret.first = 2; // No data
      return ret;
    }
    Matcher match = timePattern.matcher(timeStr);
    if (!match.matches()) {
      ret.first = 5;
      return ret;
    }

    boolean isNeg = timeStr.indexOf('-') != -1;
    String[] timeComponents = timeStr.split(Character.toString(timeSep));
    for (int i = 0; i < timeComponents.length; ++i) {
      timeComponents[i] = timeComponents[i].trim();
    }
    for (int i = 0; i < timeComponents.length; ++i) {
      if (i < 2) {
        match = intNum.matcher(timeComponents[i]);
        if (!match.find()) {
          ret.first = 5;
          return ret;
        }
      } else {
        if (i == 2) {
          match = decNum.matcher(timeComponents[i]);
          if (!match.find()) {
            ret.first = 5;
            return ret;
          }
        } else { // Time has a maximum of three components.
          ret.first = 5;
          return ret;
        }
      }
      switch (i) {
        case 0:
          hour = Math.abs(Integer.parseInt(timeComponents[0]));
          break;
        case 1:
          minute = Math.abs(Integer.parseInt(timeComponents[1]));
          break;
        case 2:
          second = Math.abs(Double.parseDouble(timeComponents[i]));
          break;
      }
    }
    // Set the sign.
    if (isNeg) {
      if (hour != 0) {
        hour = -hour;
      } else {
        if (minute != 0) {
          minute = -minute;
        } else {
          if (second != 0) {
            second = -second;
          }
        }
      }
    }
    ret.second.SetHMS(hour, minute, second);
    ret.first = 1;
    return ret;
  }

  /**
   * Parse a date formatted as YYYY-MM-DD HH-mm-ss.sss into a DT::YMDHMS
   * class.
   * <p>
   * In addition to being an integer in the range [01 ... 12], the month can
   * be either the month name or abbreviation.
   * <p>
   * The time string is optional. If not present then the time is returned as
   * 00:00:00.00
   *
   * @param date        The date string to parse.
   * @param dateSep     The date separator.
   * @param dateTimeSep The time separator.
   * @param timeSep     The separator between the date and time.
   * @return A pair consisting of an integer and the parsed date, with the
   * integer taking the following values:
   * <ul>
   * <li>1 = Parsed Ok.</li>
   * <li>2 = No data.</li>
   * <li>3 = Invalid date.</li>
   * <li>4 = Bad month.</li>
   * <li>5 = Invalid time.</li>
   * </ul>
   */
  private DTPair<Integer, YMDHMS> ParseDateTime(String date, char dateSep, char dateTimeSep, char timeSep) {

    DTPair<Integer, YMDHMS> ret = new DTPair<>(2, new YMDHMS());

    if (!(date != null && !date.trim().isEmpty())) {
      ret.first = 2; // No data
      return ret;
    }

    String delim = "[" + dateTimeSep + "]+";
    String[] datetime = date.split(delim);
    // datetime will always have a length >= 1.

    String dateStr;
    String timeStr = "";

    if (datetime.length == 1) {
      // We have just a date
      dateStr = datetime[0].trim();
    } else {
      dateStr = datetime[0].trim();
      timeStr = datetime[1].trim();
    }
    DTPair<Integer, YMD> ymdRes = this.ParseDate(dateStr, dateSep);
    if (ymdRes.first != 1) {
      ret.first = ymdRes.first;
      return ret;
    }
    DTPair<Integer, HMS> hmsRes = this.ParseTime(timeStr, timeSep);
    // An empty time is valid.
    if (hmsRes.first != 1 && hmsRes.first != 2) {
      ret.first = hmsRes.first;
      return ret;
    }

    ret.second.SetYMDHMS(ymdRes.second, hmsRes.second);
    ret.first = 1;
    return ret;
  }

  /**
   * Parse a time interval formatted as d.dd HH:mm:ss.sss into a
   * DT::TimeInterval class.
   * <p>
   * d.dd can be a decimal number with an optional trailing "d".
   * <p>
   * The time string is optional. If not present then the time is returned as
   * 00:00:00.00
   * <p>
   * Note: The day and HMS part of the string have independent signs. This
   * means that for:
   * <ul>
   * <li>-1d -12:00:00.0, -1d 12:00:00.0, 1d -12:00:00.0, 1d 12:00:00.0</li>
   * </ul>
   * We get time intervals of:
   * <ul>
   * <li>(-1, -0.5), (-1, 0.5), (1, -0.5) (1, 0.5) respectively.</li>
   * </ul>
   *
   * @param ti         The time interval string to parse.
   * @param dayTimeSep The separator between the day and time.
   * @param timeSep    The time separator.
   * @return A pair consisting of an integer and the parsed time interval,
   * with the integer taking the following values:
   * <ul>
   * <li>1 = Parsed Ok.</li>
   * <li>2 = No data.</li>
   * <li>5 = Invalid time.</li>
   * <li>6 = Invalid integer or decimal number.</li>
   * </ul>
   */
  private DTPair<Integer, TimeInterval> ParseTimeInterval(String ti, char dayTimeSep, char timeSep) {

    DTPair<Integer, TimeInterval> ret = new DTPair<>(2, new TimeInterval());

    if (!(ti != null && !ti.trim().isEmpty())) {
      ret.first = 2;
      return ret;
    }

    // Cleanup the time interval.
    String ws = "\\s{2,}";
    String dD = "[D|d]";
    String t = ti.replaceAll(ws, " ");
    t = t.replaceAll(dD, "");
    t = t.trim();

    if (t.isEmpty()) {
      ret.first = 2;
      return ret;
    }

    String delim = "[" + dayTimeSep + "]+";
    String[] list = t.trim().split(delim);
    String dayStr;
    String timeStr = "";

    if (list.length == 1) {
      // We have just the days.
      dayStr = list[0].trim();
    } else {
      dayStr = list[0].trim();
      timeStr = list[1].trim();
    }

    DTPair<Integer, DTPair<Integer, Double>> res = ParseDecNum(dayStr);
    if (res.first != 1) {
      ret.first = res.first;
      return ret;
    }
    TimeInterval timeInterval = new TimeInterval(res.second.first, res.second.second);
    DTPair<Integer, HMS> hmsRes = this.ParseTime(timeStr, timeSep);
    // An empty time is valid.
    if (hmsRes.first != 1 && hmsRes.first != 2) {
      ret.first = hmsRes.first;
      return ret;
    }
    DateTimeConversions dtc = new DateTimeConversions();
    double fod = dtc.hms2h(hmsRes.second) / 24.0;

    ret.second.SetTimeInterval(timeInterval.GetDeltaDay(), timeInterval.GetDeltaFoD() + fod);
    ret.first = 1;

    return ret;
  }

}
