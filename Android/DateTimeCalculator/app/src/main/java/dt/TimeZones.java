/*=========================================================================

  Program:   Date Time Library
  File   :   TimeZones.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/*

 */
package dt;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * This class provides a convenient way of accessing time zones.
 * <p>
 * See: http://en.wikipedia.org/wiki/List_of_time_zones_by_UTC_offset#UTC.E2.88
 * .9209:30.2C_V.E2.80.A0
 *
 * @author Andrew J. P. Maclean
 */
public class TimeZones {

  /**
   * <Time Zone name, difference in seconds from UT 0.>
   */
  private final Map<String, Integer> tzNames = new HashMap<>();
  /**
   * <difference in seconds from UT 0, Time Zone name.>
   */
  private final Map<Integer, String> tzTimes = new HashMap<>();

  public TimeZones() {
    tzNames.put("UT-12", -12 * 3600);
    tzNames.put("UT-11", -11 * 3600);
    tzNames.put("UT-10", -10 * 3600);
    tzNames.put("UT-09:30", (int) (-9.5 * 3600));
    tzNames.put("UT-09", -9 * 3600);
    tzNames.put("UT-08", -8 * 3600);
    tzNames.put("UT-07", -7 * 3600);
    tzNames.put("UT-06", -6 * 3600);
    tzNames.put("UT-05", -5 * 3600);
    tzNames.put("UT-04:30", (int) (-4.5 * 3600));
    tzNames.put("UT-04", -4 * 3600);
    tzNames.put("UT-03:30", (int) (-3.5 * 3600));
    tzNames.put("UT-03", -3 * 3600);
    tzNames.put("UT-02", -2 * 3600);
    tzNames.put("UT-01", -1 * 3600);
    tzNames.put("UT", 0);
    tzNames.put("UT+01", 3600);
    tzNames.put("UT+02", 2 * 3600);
    tzNames.put("UT+03", 3 * 3600);
    tzNames.put("UT+03:30", (int) (3.5 * 3600));
    tzNames.put("UT+04", 4 * 3600);
    tzNames.put("UT+04:30", (int) (4.5 * 3600));
    tzNames.put("UT+05", 5 * 3600);
    tzNames.put("UT+05:30", (int) (5.5 * 3600));
    tzNames.put("UT+05:45", (int) (5.75 * 3600));
    tzNames.put("UT+06", 6 * 3600);
    tzNames.put("UT+06:30", (int) (6.5 * 3600));
    tzNames.put("UT+07", 7 * 3600);
    tzNames.put("UT+08", 8 * 3600);
    tzNames.put("UT+08:45", (int) (8.75 * 3600));
    tzNames.put("UT+09", 9 * 3600);
    tzNames.put("UT+09:30", (int) (9.5 * 3600));
    tzNames.put("UT+10", 10 * 3600);
    tzNames.put("UT+10:30", (int) (10.5 * 3600));
    tzNames.put("UT+11", 11 * 3600);
    tzNames.put("UT+11:30", (int) (11.5 * 3600));
    tzNames.put("UT+12", 12 * 3600);
    tzNames.put("UT+12:45", (int) (12.75 * 3600));
    tzNames.put("UT+13", 13 * 3600);
    tzNames.put("UT+14", 14 * 3600);

    for (Map.Entry<String, Integer> entry : tzNames.entrySet()) {
      tzTimes.put(entry.getValue(), entry.getKey());
    }
  }

  /**
   * @param tz the time zones.
   */
  public TimeZones(TimeZones tz) {
    this.assign(tz);
  }

  /**
   * assignment
   *
   * @param rhs The day names.
   */
  private void assign(TimeZones rhs) {
    this.tzNames.clear();
    for (Entry<String, Integer> pair : rhs.tzNames.entrySet()) {
      this.tzNames.put(pair.getKey(), pair.getValue());
    }
    this.tzTimes.clear();
    for (Entry<Integer, String> pair : rhs.tzTimes.entrySet()) {
      this.tzTimes.put(pair.getKey(), pair.getValue());
    }
  }

  /**
   * @param rhs The time.
   * @return true if this == rhs, false otherwise
   */
  public boolean eq(TimeZones rhs) {
    return this.tzNames.equals(rhs.tzNames) && this.tzTimes.equals(rhs.tzTimes);
  }

  /**
   * @param rhs The time.
   * @return true if this != rhs, false otherwise
   */
  public boolean ne(TimeZones rhs) {
    return !this.eq(rhs);
  }

  /**
   * @return A map of time zones indexed by name.
   */
  public Map<String, Integer> GetTimeZonesByName() {
    return tzNames;
  }

  /**
   * @return A map of time zones indexed by the difference in seconds from UT
   * 0.
   */
  public Map<Integer, String> GetTimeZonesByValue() {
    return tzTimes;
  }

  /**
   * @return A string corresponding to the contents of the class.
   */
  @Override
  public String toString() {
    return String.format("%s, %s\n", this.tzNames, this.tzTimes);
  }

  /**
   * Get the name of the time zone corresponding to a given offset in seconds.
   *
   * @param offset - the time zone offset in seconds.
   * @return A pair consisting of true and the time zone name if found, false
   * and an empty string otherwise.
   */
  public DTPair<Boolean, String> GetTimeZoneName(int offset) {
    if (this.tzTimes.containsKey(offset)) {
      return new DTPair<>(true, this.tzTimes.get(offset));
    } else {
      return new DTPair<>(false, "");
    }
  }

  /**
   * Get the value of the time zone corresponding to a given time zone name.
   *
   * @param TZName - the name of the time zone.
   * @return A pair consisting of true and the time zone offset if found,
   * false and a time zone offset of zero otherwise.
   */
  public DTPair<Boolean, Integer> GetTimeZoneValue(String TZName) {
    if (this.tzNames.containsKey(TZName)) {
      return new DTPair<>(true, this.tzNames.get(TZName));
    } else {
      return new DTPair<>(false, 0);
    }
  }
}
