/*=========================================================================

  Program:   Date Time Library
  File   :   JD.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
package dt;

/**
 * This class holds a date as Julian Day Number and Fraction of the Day.
 * <p>
 * The Julian Day Number stored in the class is the integral value of the day
 * corresponding to the Julian Date (jdn) i.e int(jdn + 0.5), so it runs from
 * noon to noon on the date in question.
 * <p>
 * The fraction of the day stored runs from noon to noon.
 *
 * @author Andrew J. P. Maclean
 */
public class JD extends DateTimeBase {

  /**
   * The Julian Day Number, running from noon to noon.
   */
  private int jdn;
  /**
   * The Fraction of the Day, running from noon to noon.
   */
  private double fod;

  // Default value corresponds to -4712-01-01 12:00:00 = JD0.0
  public JD() {
    this.jdn = 0;
    this.fod = 0;
  }

  /**
   * @param jdn The Julian Day Number, running from noon to noon.
   * @param fod The Fraction of the Day, running from noon to noon.
   */
  public JD(int jdn, double fod) {
    this.jdn = jdn;
    this.fod = fod;
    this.Normalise();
  }

  /**
   * @param jd The Julian Date
   */
  public JD(JD jd) {
    this.jdn = jd.jdn;
    this.fod = jd.fod;
    this.Normalise();
  }

  /**
   * @param jd The Julian Date
   */
  public void SetJD(JD jd) {
    this.jdn = jd.jdn;
    this.fod = jd.fod;
    this.Normalise();
  }

  /**
   * Set the Julian date.
   *
   * @param jdn The Julian Day Number, running from noon to noon.
   * @param fod The Fraction of the Day, running from noon to noon.
   */
  public void SetJD(int jdn, double fod) {
    this.jdn = jdn;
    this.fod = fod;
    this.Normalise();
  }

  /**
   * Get the Julian date.
   *
   * @return The Julian Date
   */
  public JD GetJD() {
    return this;
  }

  /**
   * Checks the two objects for equality.
   *
   * @param o the object to which this one is to be checked for equality
   * @return true if equal
   */
  @Override
  public boolean equals(Object o) {
    // Same object
    if (this == o) {
      return true;
    }
    // Check if the other object has the correct type.
    if (!(o instanceof JD)) {
      return false;
    }
    // Cast to the appropriate type.
    JD p = (JD) o;
    // Check each field.
    return this.jdn == p.jdn && this.fod == p.fod;
  }

  /**
   * @param rhs Julian Date.
   * @return true if this == rhs, false otherwise
   */
  public boolean eq(JD rhs) {
    return this.equals(rhs);
  }

  /**
   * @param rhs Julian Date.
   * @return true if this != rhs, false otherwise
   */
  public boolean ne(JD rhs) {
    return !this.equals(rhs);
  }

  /**
   * @param rhs A JD object.
   * @return true if this &lt; rhs
   */
  public boolean lt(JD rhs) {
    return this.jdn < rhs.jdn || (this.jdn == rhs.jdn) && (this.fod < rhs.fod);
  }

  /**
   * @param rhs A JD object.
   * @return true if this &le; rhs
   */
  public boolean le(JD rhs) {
    return !(this.gt(rhs));
  }

  /**
   * @param rhs A JD object.
   * @return true if this &gt; rhs
   */
  public boolean gt(JD rhs) {
    return this.jdn > rhs.jdn || (this.jdn == rhs.jdn) && (this.fod > rhs.fod);
  }

  // Addition/Subtraction

  /**
   * @param rhs A JD object.
   * @return true if this &ge; rhs
   */
  public boolean ge(JD rhs) {
    return !(this.lt(rhs));
  }

  /**
   * Compare Julian Dates for equality.
   *
   * @param jd      The Julian Date to be compared with this.
   * @param rel_tol The precision.
   * @param abs_tol The precision.
   * @return true if the dates are equivalent.
   */
  public boolean IsClose(JD jd, double rel_tol, double abs_tol) {
    // Same object
    return this == jd || this.jdn == jd.jdn && isclose(this.fod, jd.fod, rel_tol, abs_tol);
  }

  /**
   * Compare Julian Dates for equality.
   * <p>
   * Assumes an absolute tolerance of 0.0.
   *
   * @param jd      The Julian Date to be compared with this.
   * @param rel_tol The precision.
   * @return true if the dates are equivalent.
   */
  public boolean IsClose(JD jd, double rel_tol) {
    // Same object
    return this == jd || this.jdn == jd.jdn && isclose(this.fod, jd.fod, rel_tol);
  }

  /**
   * Compare Julian Dates for equality.
   * <p>
   * Assumes a relative tolerance of 1.0e-09 and an absolute tolerance of 0.0.
   *
   * @param jd The Julian Date to be compared with this.
   * @return true if the dates are equivalent.
   */
  public boolean IsClose(JD jd) {
    // Same object
    return this == jd || this.jdn == jd.jdn && isclose(this.fod, jd.fod);
  }

  /**
   * +
   *
   * @param rhs A JulianDateTime object.
   * @return this + rhs
   */
  public JD add(JD rhs) {
    JD result = new JD();
    result.jdn = this.jdn + rhs.jdn;
    result.fod = this.fod + rhs.fod;
    result.Normalise();

    return result;
  }

  /**
   * +
   *
   * @param days Days
   * @return this + days
   */
  public JD add(double days) {
    JD result = new JD();
    result.jdn = this.jdn + (int) days;
    result.fod = this.fod + (days - (int) days);
    result.Normalise();

    return result;
  }

  /**
   * += Increment the date by a JD value.
   *
   * @param rhs A JD object.
   */
  public void inc(JD rhs) {
    this.jdn += rhs.jdn;
    this.fod += rhs.fod;
    this.Normalise();
  }

  /**
   * += Increment the date by the day value.
   *
   * @param days Days
   */
  public void inc(double days) {
    this.jdn += (int) (days);
    this.fod += (days - (int) (days));
    Normalise();
  }

  /**
   * -
   *
   * @param rhs A JD object.
   * @return this - rhs
   */
  public JD sub(JD rhs) {
    JD result = new JD();
    result.jdn = this.jdn - rhs.jdn;
    result.fod = this.fod - rhs.fod;
    result.Normalise();

    return result;
  }

  /**
   * -
   *
   * @param days Days
   * @return this - days
   */
  public JD sub(double days) {
    JD result = new JD();
    result.jdn = this.jdn - (int) (days);
    result.fod = this.fod - (days - (int) (days));
    result.Normalise();

    return result;
  }

  /**
   * -= Decrement the date by a JD value.
   *
   * @param rhs A JD object.
   */
  public void dec(JD rhs) {
    this.jdn -= rhs.jdn;
    this.fod -= rhs.fod;
    Normalise();
  }

  /**
   * -= Decrement the date by the day value.
   *
   * @param days Days
   */
  public void dec(double days) {
    this.jdn -= (int) (days);
    this.fod -= (days - (int) (days));
    Normalise();
  }

  /**
   * assignment
   *
   * @param rhs The Julian Date.
   */
  public void assign(JD rhs) {
    this.jdn = rhs.jdn;
    this.fod = rhs.fod;
  }

  /**
   * Get the Julian Day Number.
   *
   * @return Julian Day Number
   */
  public int GetJDN() {
    return this.jdn;
  }

  /**
   * Get the fraction of the day.
   *
   * @return Fraction of the Day
   */
  public double GetFoD() {
    return this.fod;
  }

  /**
   * Get the Julian Date as days and decimals of the day.
   * <p>
   * The precision of the returned value will be of the order of 1.0e-8 of a
   * day or 1ms.
   *
   * @return The Julian Day Number of the date as days and decimals of the
   * day.
   */
  public double GetJDDouble() {
    return this.jdn + this.fod;
  }

  /**
   * Get the fraction of the day running from midnight to midnight.
   *
   * @return Fraction of the Day
   */
  public double GetFoDMidnight() {
    DTPair<Integer, Double> d = this.FoDToHMS(new DTPair<>(this.jdn, this.fod));
    return d.second;
  }

  /**
   * Convert the fraction of the day running from noon to noon to the fraction
   * of the day running from midnight to midnight.
   *
   * @param date A pair (jn, fd), where jn is the julian day number and fd is
   *             the fraction of the day running from noon to noon (always
   *             positive).
   * @return A pair (days, hms), where days is the count of days and hms is
   * the fraction of the day running from midnight to midnight (always
   * positive).
   */
  private DTPair<Integer, Double> FoDToHMS(DTPair<Integer, Double> date) {
    double hms = date.second + 0.5;
    int days = date.first;
    if (hms >= 1.0) // Next day.
    {
      hms -= 1.0;
      ++days;
    }
    return new DTPair<>(days, hms);
  }

  /**
   * Convert the fraction of the day running from midnight to midnight to the
   * fraction of the day running from noon to noon.
   *
   * @param date A pair (days, hms), where days is the count of days and hms is
   *             the fraction of the day running from midnight to midnight
   *             (always positive).
   * @return A pair (jn, fd), where jn is the julian day number and fd is the
   * fraction of the day running from noon to noon (always positive).
   */
  public DTPair<Integer, Double> HMSToFoD(DTPair<Integer, Double> date) {
    double fd = date.second - 0.5;
    int jn = date.first;
    if (fd < 0) {
      fd += 1.0; // Previous day.
      --jn;
    }
    return new DTPair<>(jn, fd);
  }

  /**
   * Set the Julian Day Number.
   *
   * @param jdn The Julian Day Number.
   */
  public void SetJDN(int jdn) {
    this.jdn = jdn;
    this.Normalise();
  }

  /**
   * Set the fraction of the day.
   *
   * @param fod The fraction of the day.
   */
  public void SetFoD(double fod) {
    this.fod = fod;
    this.Normalise();
  }

  /**
   * @return The Julian Date as a string.
   */
  public String GetString() {

    boolean isNegative = false;
    if (this.jdn < 0) {
      isNegative = true;
    } else {
      if (this.fod < 0) {
        isNegative = true;
      }
    }
    String s;
    if (isNegative) {
      s = "-";
    } else {
      s = " ";
    }

    String fr = String.format("%019.15f", Math.abs(this.fod));
    int idx = fr.indexOf('.');
    fr = fr.substring(idx);
    s += String.format("%d", Math.abs(this.jdn));
    s += fr;
    return s;
  }

  /**
   * @return A string corresponding to the contents of the class.
   */
  @Override
  public String toString() {
    return String.format("%d, %19.15f", this.jdn, this.fod);
  }

  /**
   * Normalise so that 0 &le; fod &lt; 1.
   */
  protected void Normalise() {
    while (this.fod >= 1) {
      this.fod -= 1;
      this.jdn += 1;
    }
    while (this.fod < 0) {
      this.fod += 1;
      this.jdn -= 1;
    }
  }

}
