/**
 * <h2>Date Time Library</h2>
 * This library provides a set of high precision date and time classes
 * covering a large time span.
 * <p>
 *
 * @author Andrew J. P. Maclean
 */
package dt;
