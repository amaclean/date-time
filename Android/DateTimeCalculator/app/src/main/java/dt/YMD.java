/*=========================================================================

  Program:   Date Time Library
  File   :   YMD.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/*

 */
package dt;

/**
 * This class holds a date as year, month and day.
 *
 * @author Andrew J. P. Maclean
 */
public class YMD {
  /**
   * Year
   */
  public int year;
  /**
   * Month
   */
  public int month;
  /**
   * Day
   */
  public int day;

  public YMD() {
    this.year = -4712;
    this.month = 1;
    this.day = 1;
  }

  /**
   * @param year  Year
   * @param month Month
   * @param day   Day
   */
  public YMD(int year, int month, int day) {
    this.year = year;
    this.month = month;
    this.day = day;
  }

  /**
   * @param ymd The date.
   */
  public YMD(YMD ymd) {
    this.year = ymd.year;
    this.month = ymd.month;
    this.day = ymd.day;
  }

  /**
   * @param ymd The date.
   */
  public void SetYMD(YMD ymd) {
    this.year = ymd.year;
    this.month = ymd.month;
    this.day = ymd.day;
  }

  /**
   * Set the date.
   *
   * @param year  Year
   * @param month Month
   * @param day   Day
   */
  public void SetYMD(int year, int month, int day) {
    this.year = year;
    this.month = month;
    this.day = day;
  }

  /**
   * Get the date.
   *
   * @return The date.
   */
  public YMD GetYMD() {
    return this;
  }

  /**
   * Checks the two objects for equality.
   *
   * @param o the object to which this one is to be checked for equality
   * @return true if equal
   */
  @Override
  public boolean equals(Object o) {
    // Same object
    if (this == o) {
      return true;
    }
    // Check if the other object has the correct type.
    if (!(o instanceof YMD)) {
      return false;
    }
    // Cast to the appropriate type.
    YMD p = (YMD) o;
    // Check each field.
    return this.year == p.year && this.month == p.month && this.day == p.day;
  }

  /**
   * @param rhs The date.
   * @return true if this == rhs, false otherwise
   */
  public boolean eq(YMD rhs) {
    return this.equals(rhs);
  }

  /**
   * @param rhs The date.
   * @return true if this == rhs, false otherwise
   */
  public boolean ne(YMD rhs) {
    return !this.equals(rhs);
  }

  /**
   * @param rhs The date.
   * @return true if this &lt; rhs
   */
  public boolean lt(YMD rhs) {
    if (this.year < rhs.year) {
      return true;
    }
    if (this.year == rhs.year) {
      if (this.month < rhs.month) {
        return true;
      }
      if (this.month == rhs.month) {
        return this.day < rhs.day;
      }
    }
    return false;
  }

  /**
   * @param rhs The date.
   * @return true if this &le; rhs
   */
  public boolean le(YMD rhs) {
    return !(this.gt(rhs));
  }

  /**
   * @param rhs The date.
   * @return true if this &gt; rhs
   */
  public boolean gt(YMD rhs) {
    if (this.year > rhs.year) {
      return true;
    }
    if (this.year == rhs.year) {
      if (this.month > rhs.month) {
        return true;
      }
      if (this.month == rhs.month) {
        return this.day > rhs.day;
      }
    }
    return false;
  }

  /**
   * @param rhs The date.
   * @return true if this &ge; rhs
   */
  public boolean ge(YMD rhs) {
    return !(this.lt(rhs));
  }

  /**
   * @return The date as a string.
   */
  public String GetString() {
    return String.format("%d-%02d-%02d", this.year, this.month, this.day);
  }

  /**
   * @return A string corresponding to the contents of the class.
   */
  @Override
  public String toString() {
    return String.format("%d-%02d-%02d", this.year, this.month, this.day);
  }

}
