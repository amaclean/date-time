/*=========================================================================

  Program:   Date Time Library
  File   :   DTFormat.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

package dt;

/**
 * This class holds the parameters used for formatting dates and times.
 */
public class DateTimeFormat {
  /**
   * The calendar to use.
   */
  public CALENDAR_TYPE calendar = CALENDAR_TYPE.JULIAN_GREGORIAN;
  /**
   * The precision of the seconds.
   */
  public int precisionSeconds = 3;
  /**
   * The precision of the fraction of the day.
   */
  public int precisionFoD = 8;
  /**
   * Specify whether the day name is required.
   */
  public DAY_NAME dayName = DAY_NAME.NONE;
  /**
   * The size of the abbreviation for a day, usually 3.
   */
  public int dayAbbreviationLength = 3;
  /**
   * The first day of the week, either Sunday or Monday.
   */
  public FIRST_DOW firstDoW = FIRST_DOW.SUNDAY;
  /**
   * Specify whether the month name is required.
   */
  public MONTH_NAME monthName = MONTH_NAME.NONE;
  /**
   * The size of the abbreviation for a month, usually 3.
   */
  public int monthAbbreviationLength = 3;
  /**
   * The date separator, usually '-'.
   */
  public char dateSep = '-';
  /**
   * The time separator, usually ':'.
   */
  public char timeSep = ':';
  /**
   * The separator between the date and time, usually ' '.
   */
  public char dateTimeSep = ' ';
  /**
   * The separator between the day and time in a time interval, usually ' '.
   */
  public char dayTimeSep = ' ';
}
