package com.datetime.amaclean.datetimecalculator;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import dt.CALENDAR_TYPE;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CalendarDialogFragment.OnCompleteListener} interface
 * to handle interaction events.
 * Use the {@link CalendarDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CalendarDialogFragment extends DialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "title";
    // private static final String ARG_PARAM2 = "param2";
    private final Common cInt = Common.Instance();
    private final CALENDAR_TYPE calendarOriginal = cInt.fmt.calendar;
    // TODO: Rename and change types of parameters
    private int mParam1;
    private CALENDAR_TYPE calendar = cInt.fmt.calendar;
    private RadioGroup calendarGroup;
    private RadioButton calendarButton;

    //    private OnFragmentInteractionListener mListener;
    private OnCompleteListener mListener;

    public CalendarDialogFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CalendarDialogFragment.
     */
    // TODO: Rename and change types and number of parameters
    // public static CalendarDialogFragment newInstance(String param1, String param2) {
    public static CalendarDialogFragment newInstance() {
        CalendarDialogFragment fragment = new CalendarDialogFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, R.string.calendar_menu_title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
        }
    }

//    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }

    // See: http://stackoverflow.com/questions/15121373/returning-string-from-dialog-fragment-back-to-activity
    // The preferred method is to use a callback to get a signal from a Fragment.
    // Also, this is the recommended method proposed by Android at:
    //	http://developer.android.com/guide/components/fragments.html#CommunicatingWithActivity

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(getString(mParam1));
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        final View v = inflater.inflate(R.layout.fragment_calendar_dialog, null);

        // Set the appropriate radio button
        calendarGroup = v.findViewById(R.id.calendarGroup);
        switch (calendarOriginal) {
            case JULIAN:
                calendarGroup.check(R.id.julianButton);
                break;
            case GREGORIAN:
                calendarGroup.check(R.id.gregorianButton);
                break;
            case JULIAN_GREGORIAN:
            default:
                calendarGroup.check(R.id.julian_gregorianButton);
                break;
        }

        calendarGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.julianButton:
                        calendar = CALENDAR_TYPE.JULIAN;
                        break;
                    case R.id.gregorianButton:
                        calendar = CALENDAR_TYPE.GREGORIAN;
                        break;
                    case R.id.julian_gregorianButton:
                        calendar = CALENDAR_TYPE.JULIAN_GREGORIAN;
                        break;
                }
            }
        });

        builder.setView(v)//inflater.inflate(R.layout.fragment_precision_alert_dialog, null))
                // Add action buttons
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        onCalendarChanged();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        calendar = calendarOriginal;
                    }
                });
        return builder.create();
    }

    // make sure the Activity implemented it
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (OnCompleteListener) activity;
        } catch (final ClassCastException e) {
            throw new ClassCastException(activity +
                    " must implement OnCompleteListener");
        }
    }

    // Send the value back to the activity via the callback.
    private void onCalendarChanged() {
        boolean changed = false;
        if (calendar != calendarOriginal) {
            cInt.fmt.calendar = calendar;
            changed = true;
        }
        if (changed) {
            String s = "CalendarChanged";
            this.mListener.onCalendarChanged(s);
        }
    }

    @Override
    public void onPause() {
        // Commit any changes that should be persisted
        // beyond the current user session
        super.onPause();
        onCalendarChanged();
    }

    public interface OnCompleteListener {
        void onCalendarChanged(String msg);
    }
}
