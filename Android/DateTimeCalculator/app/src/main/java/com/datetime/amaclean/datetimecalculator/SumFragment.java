/*=========================================================================

  Program:   Date Time Calculator
  File   :   DifferenceFragment.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

package com.datetime.amaclean.datetimecalculator;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Objects;

import androidx.fragment.app.Fragment;
import dt.CALENDAR_TYPE;
import dt.DTPair;
import dt.JD;
import dt.TimeInterval;
import dt.YMDHMS;

public class SumFragment extends Fragment implements OnClickListener {

    private final Common cInt = Common.Instance();
    private boolean viewCreated = false;

    public SumFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sum,
                container, false);
        // Let's do the variables.

        // Set title bar
        int title = SetTitle();
        ((MainActivity) Objects.requireNonNull(getActivity()))
                .setActionBarTitle(getResources().getString(title));

        // See:
        // http://www.pelagodesign.com/blog/2009/05/20/iso-8601-date-validation-that-doesnt-suck/
        //      String regex = getResources().getString(R.string.ISO8601DatePattern);
        EditText txtA = rootView.findViewById(R.id.DTAS);
        // Default to the numeric keypad.
        txtA.setRawInputType(android.text.InputType.TYPE_CLASS_DATETIME |
                android.text.InputType.TYPE_DATETIME_VARIATION_TIME);
        txtA.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {

                if (event == null) {
                    return false;
                }
                if (actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_ACTION_NEXT
                        || actionId == EditorInfo.IME_ACTION_PREVIOUS) {
                    // the user is done typing.
                    EditText txt = requireActivity().findViewById(R.id.TIDTBS);
                    txt.requestFocus();
                    return true; // consume
                }
                return false; // pass on to other listeners.
            }
        });
        txtA.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                /*
                 * When focus is lost update.
                 */
                if (!hasFocus) {
                    HideSoftInput(v);
                    GetDTAS();
                    SetDTAS();
                    SetJDAS();
                    SumDTS();
                    SumJDS();
                }
            }
        });

        EditText txtB = rootView.findViewById(R.id.TIDTBS);
        // Default to the numeric keypad.
        txtB.setRawInputType(android.text.InputType.TYPE_CLASS_DATETIME |
                android.text.InputType.TYPE_DATETIME_VARIATION_TIME);
        txtB.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {

                if (event == null) {
                    return false;
                }
                if (actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_ACTION_NEXT
                        || actionId == EditorInfo.IME_ACTION_PREVIOUS) {
                    // the user is done typing.
                    EditText txt = requireActivity().findViewById(R.id.DTAS);
                    txt.requestFocus();
                    return true; // consume
                }
                return false; // pass on to other listeners.
            }
        });
        txtB.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                /*
                 * When focus is lost update.
                 */
                if (!hasFocus) {
                    HideSoftInput(v);
                    GetTIDTBS();
                    SetTIDTBS();
                    SetTIJDBS();
                    SumDTS();
                    SumJDS();
                }
            }
        });

        EditText jdtxtA = rootView.findViewById(R.id.JDAS);
        jdtxtA.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (event == null) {
                    return false;
                }
                if (actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_ACTION_NEXT
                        || actionId == EditorInfo.IME_ACTION_PREVIOUS) {
                    // the user is done typing.
                    EditText txt = requireActivity().findViewById(R.id.TIJDBS);
                    txt.requestFocus();
                    return true; // consume
                }
                return false; // pass on to other listeners.
            }
        });
        jdtxtA.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                /*
                 * When focus is lost update.
                 */
                if (!hasFocus) {
                    HideSoftInput(v);
                    GetJDAS();
                    SetJDAS();
                    SetDTAS();
                    SumDTS();
                    SumJDS();
                }
            }
        });

        EditText jdtxtB = rootView.findViewById(R.id.TIJDBS);
        jdtxtB.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (event == null) {
                    return false;
                }
                if (actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_ACTION_NEXT
                        || actionId == EditorInfo.IME_ACTION_PREVIOUS) {
                    // the user is done typing.
                    EditText txt = requireActivity().findViewById(R.id.JDAS);
                    txt.requestFocus();
                    return true; // consume
                }
                return false; // pass on to other listeners.
            }
        });
        jdtxtB.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                /*
                 * When focus is lost update.
                 */
                if (!hasFocus) {
                    HideSoftInput(v);
                    GetTIJDBS();
                    SetTIJDBS();
                    SetTIDTBS();
                    SumDTS();
                    SumJDS();
                }
            }
        });

        // Populate the spinner
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(
                rootView.getContext(), android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        // Fill the array adapter.
        int[] a = new int[cInt.tzValues.size()];
        int i = 0;
        for (Integer key : cInt.tzValues.keySet()) {
            a[i] = key;
            ++i;
        }
        Arrays.sort(a);
        for (i = 0; i < a.length; ++i) {
            adapter.add(cInt.tzValues.get(a[i]));
        }
        // Set up the spinners
        Spinner sA = rootView.findViewById(R.id.TZAS);
        // Pass the array adapter to the spinner.
        sA.setAdapter(adapter);

        sA.setSelection(adapter.getPosition(cInt.TZS.first));

        sA.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapter, View v, int i,
                                       long lng) {
                // do something here
                cInt.TZS.first = adapter.getItemAtPosition(i).toString();
                cInt.TZS.second = cInt.tzNames.get(cInt.TZS.first);
                // Local time
                SetDTAS();
                SetJDAS();
                SumDTS();
                SumJDS();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // do something else
                HideSoftInput(arg0);
            }
        });

        // Set the times

        // Local time
        dt.JulianDateTime lt =
                new dt.JulianDateTime(cInt.utcAS);
        lt.inc(cInt.TZS.second.doubleValue() / cInt.seconds_in_day);

        // Set the local time.
        String res = cInt.formatter.FormatAsYMDHMS(lt, cInt.fmt);
        txtA.setText(res);
        jdtxtA.setText(cInt.formatter.FormatJDLong(cInt.jdAS, cInt.fmt));

        lt = new dt.JulianDateTime(cInt.utcB);
        lt.inc(cInt.TZB.second.doubleValue() / cInt.seconds_in_day);

        // Set the time intervals.
        txtB.setText(cInt.formatter.FormatTimeIntervalDHMS(cInt.tiDTBS,
                cInt.fmt));
        jdtxtB.setText(cInt.formatter.FormatTimeInterval(cInt.tiJDBS,
                cInt.fmt));

        // Sum
        dt.JulianDateTime sumlt =
                new dt.JulianDateTime(
                        lt.GetJDN() + cInt.tiDTBS.GetDeltaDay(),
                        lt.GetFoD() + cInt.tiDTBS.GetDeltaFoD());
        res = cInt.formatter.FormatAsYMDHMS(sumlt, cInt.fmt);
        EditText resDTABTxt = rootView.findViewById(R.id.resultTxtDTABS);
        resDTABTxt.setText(res);

        dt.JD sumjd = new dt.JD(
                cInt.jdAS.GetJDN() + cInt.tiDTBS.GetDeltaDay(),
                cInt.jdAS.GetFoD() + cInt.tiDTBS.GetDeltaFoD());
        res = cInt.formatter.FormatJDLong(sumjd, cInt.fmt);
        EditText resJDABTxt = rootView.findViewById(R.id.resultTxtJDABS);
        resJDABTxt.setText(res);

        // The buttons.
        Button nowBtn = rootView.findViewById(R.id.nowBtnS);
        nowBtn.setOnClickListener(this);

        this.viewCreated = true;

        return rootView;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.nowBtnS) {
            Button nowBtn = v.findViewById(R.id.nowBtnS);
            if (nowBtn.isPressed()) {
                SetCurrentTimeS();
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && viewCreated) {
            UpdateDisplay();
        }
    }

    @Override
    public void onPause() {
        // Commit any changes that should be persisted
        // beyond the current user session
        super.onPause();
        UpdateDisplay();
    }

    public void onResume() {
        super.onResume();

        // Set title bar
        int title = SetTitle();
        ((MainActivity) requireActivity())
                .setActionBarTitle(getResources().getString(title));
    }

    public void UpdateDisplay() {
        getActivity();

        SetDTAS();
        SetTIDTBS();

        SetJDAS();
        SetTIJDBS();

        SumDTS();
        SumJDS();

        // Set title bar
        int title = SetTitle();
        ((MainActivity) getActivity())
                .setActionBarTitle(getResources().getString(title));
    }

    private int SetTitle() {
        CALENDAR_TYPE calendar = cInt.fmt.calendar;
        switch (calendar) {
            case JULIAN:
                return R.string.title_section3_J;
            case GREGORIAN:
                return R.string.title_section3_G;
            case JULIAN_GREGORIAN:
            default:
                return R.string.title_section3_JG;
        }
    }

    private void SetCurrentTimeS() {
        DTPair<Integer, YMDHMS> res = cInt.parser.ParseDateTime(
                cInt.GetCurrentSystemDateTime("GMT"), cInt.fmt);
        if (res.first != 1) {
            return;
        }
        // Set the calendar date
        cInt.utcAS.SetDateTime(res.second, CALENDAR_TYPE.JULIAN_GREGORIAN);
        // Set the Julian date
        cInt.jdAS = cInt.utcAS.GetJDFullPrecision();

        SetDTAS();
        SetJDAS();

        SumDTS();
        SumJDS();
    }

    private void SetDTAS() {
        // Local time
        dt.JulianDateTime lt =
                new dt.JulianDateTime(cInt.utcAS);
        lt.inc(cInt.TZS.second.doubleValue() / cInt.seconds_in_day);

        // Set the local time.
        String dt = cInt.formatter.FormatAsYMDHMS(lt, cInt.fmt);
        EditText cdTxt = requireActivity().findViewById(R.id.DTAS);
        cdTxt.setText(dt);
    }

    private void SetTIDTBS() {
        // Set the time interval.
        String ti = cInt.formatter.FormatTimeIntervalDHMS(cInt.tiDTBS,
                cInt.fmt);
        EditText cdTiTxt = getActivity().findViewById(R.id.TIDTBS);
        cdTiTxt.setText(ti);
    }

    private void SetJDAS() {
        EditText jdTxt = requireActivity().findViewById(R.id.JDAS);
        jdTxt.setText(cInt.formatter.FormatJDLong(cInt.jdAS, cInt.fmt));

    }

    private void SetTIJDBS() {
        // Set the time interval.
        String ti = cInt.formatter.FormatTimeInterval(cInt.tiJDBS, cInt.fmt);
        EditText jdTiTxt = getActivity().findViewById(R.id.TIJDBS);
        jdTiTxt.setText(ti);

    }

    private void GetDTAS() {
        EditText cdTxt = requireActivity().findViewById(R.id.DTAS);
        DTPair<Integer, YMDHMS> res =
                cInt.parser.ParseDateTime(cdTxt.getText().toString(), cInt.fmt);
        if (res.first != 1) {
            showParserMessageDialog(res.first, cdTxt.getText().toString());
            return;
        }
        cInt.utcAS.SetDateTime(res.second, cInt.fmt.calendar);
        // Convert to UTC
        cInt.utcAS.dec(cInt.TZS.second.doubleValue() / cInt.seconds_in_day);
        cInt.jdAS = cInt.utcAS.GetJDFullPrecision();
    }

    private void GetTIDTBS() {
        EditText tiTxt = getActivity().findViewById(R.id.TIDTBS);
        DTPair<Integer, TimeInterval> res =
                cInt.parser.ParseTimeInterval(tiTxt.getText().toString(), cInt.fmt);
        if (res.first != 1) {
            showParserMessageDialog(res.first, tiTxt.getText().toString());
            return;
        }
        cInt.tiDTBS.assign(res.second);
        cInt.tiJDBS.SetTimeInterval(cInt.tiDTBS.GetDeltaDay(),
                cInt.tiDTBS.GetDeltaFoD());
    }

    private void GetJDAS() {
        EditText jdTxt = requireActivity().findViewById(R.id.JDAS);
        DTPair<Integer, DTPair<Integer, Double>> res =
                cInt.parser.ParseDecNum(jdTxt.getText().toString());
        if (res.first != 1) {
            showParserMessageDialog(res.first, jdTxt.getText().toString());
            return;
        }
        cInt.jdAS.SetJD(res.second.first, res.second.second);
        cInt.utcAS.SetDateTime(cInt.jdAS.GetJDN(), cInt.jdAS.GetFoD());
    }

    private void GetTIJDBS() {
        EditText jdTiTxt = getActivity().findViewById(R.id.TIJDBS);
        DTPair<Integer, DTPair<Integer, Double>> res =
                cInt.parser.ParseDecNum(jdTiTxt.getText().toString());
        if (res.first != 1) {
            showParserMessageDialog(res.first, jdTiTxt.getText().toString());
            return;
        }
        cInt.tiJDBS.SetTimeInterval(res.second.first, res.second.second);
        cInt.tiDTBS.SetTimeInterval(cInt.tiJDBS.GetDeltaDay(),
                cInt.tiJDBS.GetDeltaFoD());
    }

    private void SumDTS() {
        // Sum
        JD jd = cInt.utcAS.GetJDFullPrecision();
        int jdn = jd.GetJDN() + cInt.tiDTBS.GetDeltaDay();
        double fod = jd.GetFoD() + cInt.tiDTBS.GetDeltaFoD();
        jd.SetJD(jdn, fod);
        dt.JulianDateTime lt =
                new dt.JulianDateTime(jdn, fod);
        lt.inc(cInt.TZS.second.doubleValue() / cInt.seconds_in_day);
        // Calculate the difference in days.
        String dt = cInt.formatter.FormatAsYMDHMS(lt, cInt.fmt);
        EditText cdTxt = requireActivity().findViewById(
                R.id.resultTxtDTABS);
        cdTxt.setText(dt);
    }

    private void SumJDS() {
        // Calculate the sum in days.
        int jdn = cInt.jdAS.GetJDN() + cInt.tiJDBS.GetDeltaDay();
        double fod = cInt.jdAS.GetFoD() + cInt.tiJDBS.GetDeltaFoD();
        JD jd = new JD(jdn, fod);
        String res = cInt.formatter.FormatJDLong(jd, cInt.fmt);
        EditText resJDABTxt = getActivity().findViewById(
                R.id.resultTxtJDABS);
        resJDABTxt.setText(res);
    }

    private void showParserMessageDialog(int err_num, String error) {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction. We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        assert getFragmentManager() != null;
        androidx.fragment.app.FragmentTransaction ft = getFragmentManager()
                .beginTransaction();
        androidx.fragment.app.Fragment prev =
                getFragmentManager().findFragmentByTag(
                        "dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);        // Dismiss any existing dialog.
        // Create and show the dialog.
        ParserAlertDialogFragment newFragment = ParserAlertDialogFragment
                .newInstance(err_num, error);
        newFragment.show(ft, "dialog");
    }

    /**
     * Hide the soft keyboard.
     *
     * @param v the view.
     */
    private void HideSoftInput(View v) {
        InputMethodManager imm = (InputMethodManager) getActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
    }

}
