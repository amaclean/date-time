/*=========================================================================

  Program:   Date Time Calculator
  File   :   HelpFragment.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

package com.datetime.amaclean.datetimecalculator;

import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class HelpFragment extends Fragment implements OnClickListener {

    public HelpFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity())
                .setActionBarTitle(getResources().getString(R.string.help_fragment_title));
        View rootView = inflater.inflate(R.layout.fragment_help,
                container, false);
        TextView dummyTextView = rootView
                .findViewById(R.id.help_label);
        dummyTextView.setText(Html.fromHtml(getString(R.string.helpStr)));
        dummyTextView.setMovementMethod(new ScrollingMovementMethod());
        return rootView;
    }

    @Override
    public void onClick(View arg0) {
        // TODO Auto-generated method stub

    }

}
