/*=========================================================================

  Program:   Date Time Calculator
  File   :   ParserAlertDialogFragment.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

package com.datetime.amaclean.datetimecalculator;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.SparseArray;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ParserAlertDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ParserAlertDialogFragment extends DialogFragment {
    private static final String ARG_PARAM1 = "title";
    private static final String ARG_PARAM2 = "errNum";
    private static final String ARG_PARAM3 = "errTxt";
    /**
     * The parser messages array.
     * <p>
     * An alternative here would be to use the existing parserMessages in
     * dt.DateTimeHelper, then overwrite the existing messages with the
     * parser_message string array resource.
     */
    private final SparseArray<String> parserMessages = new SparseArray<>(6);
    // TODO: Rename and change types of parameters
    private int mParam1;
    private int mParam2;
    private String mParam3;


    public ParserAlertDialogFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param2 The index for the error message..
     * @return A new instance of fragment ParserAlertDialogFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ParserAlertDialogFragment newInstance(int param2, String param3) {
        ParserAlertDialogFragment fragment = new ParserAlertDialogFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, R.string.parser_dialog_title);
        args.putInt(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.mParam1 = getArguments().getInt(ARG_PARAM1);
            this.mParam2 = getArguments().getInt(ARG_PARAM2);
            this.mParam3 = getArguments().getString(ARG_PARAM3);
            String[] parserMsgStrings = getResources().getStringArray(R.array.parser_message);
            for (int i = 0; i < parserMsgStrings.length; ++i) {
                parserMessages.append(i + 1, parserMsgStrings[i]);
            }
        }
    }

//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
    // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_parser_alert_dialog, container, false);
//    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        String title = getString(this.mParam1);
        String fmt = "%s\n%s";
        String msg = String.format(fmt, parserMessages.get(this.mParam2), this.mParam3);

        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(msg);
        //null should be your on click listener
//        alertDialogBuilder.setPositiveButton("OK", null);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
//        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });


        return alertDialogBuilder.create();
    }


}
