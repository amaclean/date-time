/*=========================================================================

  Program:   Date Time Calculator
  File   :   Common.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

package com.datetime.amaclean.datetimecalculator;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.Objects;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;


public class MainActivity extends AppCompatActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks,
        PrecisionDialogFragment.OnCompleteListener, CalendarDialogFragment.OnCompleteListener {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    // Navigation menu items
    private String[] navMenuTitles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Must be called before setContentView.
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

        setContentView(R.layout.activity_main);


        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        Log.i("MyActivity", "MyClass.getView() — get item number " + position);
        Fragment fragment;
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag;
        switch (position) {
            default:
            case 0:
                fragment = new DateFragment();
                tag = navMenuTitles[0];
                break;
            case 1:
                fragment = new DifferenceFragment();
                tag = navMenuTitles[1];
                break;
            case 2:
                fragment = new SumFragment();
                tag = navMenuTitles[2];
                break;
            case 3:
                fragment = new HelpFragment();
                tag = navMenuTitles[3];
                break;
        }
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment, tag)
                .commit();
    }

    private void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setTitle(mTitle);
    }

    public void setActionBarTitle(String title) {
        mTitle = title;
        Objects.requireNonNull(getSupportActionBar()).setTitle(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
        if (id == R.id.precision) {
//            Toast.makeText(getApplicationContext(), "Example action example == precision.", Toast.LENGTH_SHORT).show();
            showPrecisionDialog();
            return true;
        }
        if (id == R.id.calendar) {
//            Toast.makeText(getApplicationContext(), "Example action example == calendar.", Toast.LENGTH_SHORT).show();
            showCalendarDialog();
            return true;
        }
        if (id == R.id.about) {
//            Toast.makeText(getApplicationContext(), "Example action example == precision.", Toast.LENGTH_SHORT).show();
            showAboutDialog();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // The about menu bar dialog.
    private void showAboutDialog() {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction. We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        android.app.FragmentTransaction ft = getFragmentManager()
                .beginTransaction();
        android.app.Fragment prev = getFragmentManager().findFragmentByTag(
                "dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        AboutDialogFragment newFragment = AboutDialogFragment
                .newInstance();
        newFragment.show(ft, "dialog");
    }

    // The precision menu bar dialog.
    private void showPrecisionDialog() {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction. We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        android.app.FragmentTransaction ft = getFragmentManager()
                .beginTransaction();
        android.app.Fragment prev = getFragmentManager().findFragmentByTag(
                "dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        //PrecisionDialogFragment newFragment = PrecisionDialogFragment
        PrecisionDialogFragment newFragment = PrecisionDialogFragment
                .newInstance();
        newFragment.show(ft, "dialog");
    }

    public void onPrecisionChanged(String result) {
        // After the dialog fragment completes, it calls this callback.
        // use the string here
        if (result.equals("PrecisionChanged")) {
            Log.i("MainActivity", "PrecisionChanged");
            //  Find the current fragment on display and update it with the changed precision values.
            for (int i = 0; i < navMenuTitles.length; ++i) {
                androidx.fragment.app.Fragment fragment =
                        getSupportFragmentManager().findFragmentByTag(navMenuTitles[i]);
                if (fragment != null && fragment.isVisible()) {
                    switch (i) {
                        case 0: { // Date
                            DateFragment f = (DateFragment) fragment;
                            f.UpdateDisplay();
                            break;
                        }
                        case 1: { // Difference
                            DifferenceFragment f = (DifferenceFragment) fragment;
                            f.UpdateDisplay();
                            break;
                        }
                        case 2: { // Sum
                            SumFragment f = (SumFragment) fragment;
                            f.UpdateDisplay();
                            break;
                        }
                        default:
                            break;
                    }
                }
            }
        }
    }

    // The calendar menu bar dialog.
    private void showCalendarDialog() {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction. We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        android.app.FragmentTransaction ft = getFragmentManager()
                .beginTransaction();
        android.app.Fragment prev = getFragmentManager().findFragmentByTag(
                "dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        //CalendarDialogFragment newFragment = CalendarDialogFragment
        CalendarDialogFragment newFragment = CalendarDialogFragment
                .newInstance();
        newFragment.show(ft, "dialog");
    }

    public void onCalendarChanged(String result) {
        // After the dialog fragment completes, it calls this callback.
        // use the string here
        if (result.equals("CalendarChanged")) {
            Log.i("MainActivity", "CalendarChanged");
//            //  Find the current fragment on display and update it with the changed calendar.
            for (int i = 0; i < navMenuTitles.length; ++i) {
                androidx.fragment.app.Fragment fragment =
                        getSupportFragmentManager().findFragmentByTag(navMenuTitles[i]);
                if (fragment != null && fragment.isVisible()) {
                    switch (i) {
                        case 0: { // Date
                            DateFragment f = (DateFragment) fragment;
                            f.UpdateDisplay();
                            break;
                        }
                        case 1: { // Difference
                            DifferenceFragment f = (DifferenceFragment) fragment;
                            f.UpdateDisplay();
                            break;
                        }
                        case 2: { // Sum
                            SumFragment f = (SumFragment) fragment;
                            f.UpdateDisplay();
                            break;
                        }
                        default:
                            break;
                    }
                }
            }
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
//        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
//            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return inflater.inflate(R.layout.fragment_main, container, false);
        }

    }


}
