/*=========================================================================

  Program:   Date Time Calculator
  File   :   DateFragment.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

package com.datetime.amaclean.datetimecalculator;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;

import androidx.fragment.app.Fragment;
import dt.CALENDAR_TYPE;
import dt.DTPair;
import dt.YMDHMS;

public class DateFragment extends Fragment implements OnClickListener {

    private final Common cInt = Common.Instance();
    private final String fmt1;
    private final String fmt2;
    private final String fmt3;
    private boolean viewCreated = false;

    public DateFragment() {
        fmt1 = " <b>%s</b>\n" +      // dates["Calendar"]
                " <br><b>%s</b>\n" +  // zone
                " <br>%s %s\n" + // dates["DayName"], dates["Date"]
                " <br><i>DoY:</i>&nbsp;%s\n" + // dates["DoY"]
                " <br><i>Leap Year:</i>&nbsp;%s\n"; // dates["LeapYear"]

        fmt2 = " <br><i>ISO Day of the week:</i>&nbsp;%s\n"; // << dates["IWD"]

        fmt3 = " <br><i>JD:</i>&nbsp;%s\n" + // dates["JD"]
                " <br><br>\n";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_date,
                container, false);
        // Let's do the variables.

        // Set title bar
        int title = SetTitle();
        ((MainActivity) requireActivity())
                .setActionBarTitle(getResources().getString(title));

        // See:
        // http://www.pelagodesign.com/blog/2009/05/20/iso-8601-date-validation-that-doesnt-suck/
        //		String regex = getResources().getString(R.string.ISO8601DatePattern);

        EditText cdtxt = rootView.findViewById(R.id.DT);
        // Default to the numeric keypad.
        cdtxt.setRawInputType(android.text.InputType.TYPE_CLASS_DATETIME |
                android.text.InputType.TYPE_DATETIME_VARIATION_TIME);

        cdtxt.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_ACTION_NEXT
                        || actionId == EditorInfo.IME_ACTION_PREVIOUS) {
                    // the user is done typing.
                    GetDT();
                    UpdateDisplay();
                    EditText jdtxt = requireActivity().findViewById(
                            R.id.JD);
                    jdtxt.requestFocus();
                    return true; // consume
                }
                return false; // pass on to other listeners.
            }
        });
        cdtxt.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                /*
                 * When focus is lost update.
                 */
                if (!hasFocus) {
                    HideSoftInput(v);
                    GetDT();
                    UpdateDisplay();
                }
            }
        });

        EditText jdtxt = rootView.findViewById(R.id.JD);
        jdtxt.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_ACTION_NEXT
                        || actionId == EditorInfo.IME_ACTION_PREVIOUS) {
                    // the user is done typing.
                    EditText txt = requireActivity().findViewById(
                            R.id.DT);
                    txt.requestFocus();
                    return true; // consume
                }
                return false; // pass on to other listeners.
            }
        });
        jdtxt.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                /*
                 * When focus is lost update.
                 */
                if (!hasFocus) {
                    HideSoftInput(v);
                    GetJD();
                    UpdateDisplay();
                }
            }
        });

        // Set the Julian date
        jdtxt.setText(cInt.formatter.FormatJDLong(cInt.jd, cInt.fmt));

        // Local time
        dt.JulianDateTime lt =
                new dt.JulianDateTime(cInt.utc);
        lt.inc(cInt.TZ.second / cInt.seconds_in_day);

        // Set the local time.
        String cd = cInt.formatter.FormatAsYMDHMS(lt, cInt.fmt);
        cdtxt.setText(cd);

        // Populate the spinner
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(
                rootView.getContext(), android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        // Fill the array adapter.
        int[] a = new int[cInt.tzValues.size()];
        int i = 0;
        for (Integer key : cInt.tzValues.keySet()) {
            a[i] = key;
            ++i;
        }
        Arrays.sort(a);
        for (i = 0; i < a.length; ++i) {
            adapter.add(cInt.tzValues.get(a[i]));
        }
        // Set up the spinners
        Spinner s1 = rootView.findViewById(R.id.TZ);
        Spinner s2 = rootView.findViewById(R.id.altTZ);
        // Pass the array adapter to the spinner.
        s1.setAdapter(adapter);
        s2.setAdapter(adapter);

        s1.setSelection(adapter.getPosition(cInt.TZ.first));
        s2.setSelection(adapter.getPosition(cInt.altTZ.first));

        s1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapter, View v, int i,
                                       long lng) {
                // do something here
                cInt.TZ.first = adapter.getItemAtPosition(i).toString();
                cInt.TZ.second = cInt.tzNames.get(cInt.TZ.first);
                GetDT();
                UpdateDisplay();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // do something else
                HideSoftInput(arg0);
            }
        });
        s2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapter, View v, int i,
                                       long lng) {
                // do something here
                cInt.altTZ.first = adapter.getItemAtPosition(i).toString();
                cInt.altTZ.second = cInt.tzNames.get(cInt.altTZ.first);
                GetDT();
                UpdateDisplay();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // do something else
            }
        });

        // The checkbox.
        CheckBox altTZChk = rootView.findViewById(R.id.altTZchk);
        altTZChk.setOnClickListener(this);
        // Set the checkbox and visibility of s2
        altTZChk.setChecked(cInt.isAltTZChecked);
        if (cInt.isAltTZChecked) {
            s2.setVisibility(View.VISIBLE);
        } else {
            s2.setVisibility(View.INVISIBLE);
        }

        // The buttons.
        Button currentDate = rootView.findViewById(R.id.nowBtn);
        currentDate.setOnClickListener(this);

        this.viewCreated = true;

        return rootView;
    }

    @Override
    public void onClick(View v) {
        HideSoftInput(v);
        if (v.getId() == R.id.altTZchk) {
            CheckBox chk = v.findViewById(R.id.altTZchk);
            if (chk.isChecked()) {
                cInt.isAltTZChecked = true;
                Spinner s2 = requireActivity()
                        .findViewById(R.id.altTZ);
                s2.setVisibility(View.VISIBLE);

            } else {
                cInt.isAltTZChecked = false;
                Spinner s2 = requireActivity()
                        .findViewById(R.id.altTZ);
                s2.setVisibility(View.INVISIBLE);
            }
            this.UpdateDisplay();
        }
        if (v.getId() == R.id.nowBtn) {
            Button currentDate = v.findViewById(R.id.nowBtn);
            if (currentDate.isPressed()) {
                this.SetCurrentTime();
            }
        }

    }

    public void onResume() {
        super.onResume();

        // Set title bar
        int title = SetTitle();
        ((MainActivity) requireActivity())
                .setActionBarTitle(getResources().getString(title));
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && viewCreated) {
            UpdateDisplay();
        }
    }

    public void UpdateDisplay() {
        SetDT();
        SetJD();
        ShowDate();
        // Set title bar
        int title = SetTitle();
        ((MainActivity) requireActivity())
                .setActionBarTitle(getResources().getString(title));
    }

    private int SetTitle() {
        CALENDAR_TYPE calendar = cInt.fmt.calendar;
        switch (calendar) {
            case JULIAN:
                return R.string.title_section1_J;
            case GREGORIAN:
                return R.string.title_section1_G;
            case JULIAN_GREGORIAN:
            default:
                return R.string.title_section1_JG;
        }
    }

    private void GetJD() {
        EditText jdTxt = requireActivity().findViewById(R.id.JD);
        DTPair<Integer, DTPair<Integer, Double>> res =
                cInt.parser.ParseDecNum(jdTxt.getText().toString());
        if (res.first != 1) {
            showParserMessageDialog(res.first, jdTxt.getText().toString());
            return;
        }
        cInt.jd.SetJD(res.second.first, res.second.second);
        cInt.utc.SetDateTime(cInt.jd.GetJDN(), cInt.jd.GetFoD());
    }

    private void GetDT() {
        EditText cdTxt = requireActivity().findViewById(R.id.DT);
        DTPair<Integer, YMDHMS> res = cInt.parser.ParseDateTime(
                cdTxt.getText().toString(), cInt.fmt);
        if (res.first != 1) {
            showParserMessageDialog(res.first, cdTxt.getText().toString());
            return;
        }

        cInt.utc.SetDateTime(res.second, cInt.fmt.calendar);
        cInt.utc.dec(cInt.TZ.second.doubleValue() / cInt.seconds_in_day);
        cInt.jd = cInt.utc.GetJDFullPrecision();
    }

    private void SetCurrentTime() {
        DTPair<Integer, YMDHMS> res = cInt.parser.ParseDateTime(cInt
                .GetCurrentSystemDateTime("GMT"), cInt.fmt);
        if (res.first != 1) {
            return;
        }
        // Set the calendar date
        cInt.utc.SetDateTime(res.second, CALENDAR_TYPE.JULIAN_GREGORIAN);
        // Set the Julian date
        cInt.jd = cInt.utc.GetJDFullPrecision();

        UpdateDisplay();
    }

    private void SetDT() {
        // Local time
        dt.JulianDateTime lt =
                new dt.JulianDateTime(cInt.utc);
        lt.inc(cInt.TZ.second.doubleValue() / cInt.seconds_in_day);

        // Set the local time.
        String cd = cInt.formatter.FormatAsYMDHMS(lt, cInt.fmt);
        EditText cdTxt = requireActivity().findViewById(R.id.DT);
        cdTxt.setText(cd);
    }

    private void SetJD() {
        // Set the Julian date
        EditText jdTxt = requireActivity().findViewById(R.id.JD);
        jdTxt.setText(cInt.formatter.FormatJDLong(cInt.utc.GetJDFullPrecision(),
                cInt.fmt));

    }

    private void ShowDate() {

        // Local time
        dt.JulianDateTime lt =
                new dt.JulianDateTime(cInt.utc);
        lt.inc(cInt.TZ.second.doubleValue() / cInt.seconds_in_day);

        // Start building the table for display.
        HashMap<String, String> dates;
        dates = cInt.formatter.GetFormattedDates(lt, cInt.fmt);
        String str = BuildDatesString(cInt.TZ.first, dates);
        CheckBox chk = requireActivity().findViewById(R.id.altTZchk);
        if (chk.isChecked()) {
            // Alternate local time
            dt.JulianDateTime altlt =
                    new dt.JulianDateTime(cInt.utc);
            altlt.inc(cInt.altTZ.second.doubleValue() / cInt.seconds_in_day);
            dates = cInt.formatter.GetFormattedDates(altlt, cInt.fmt);
            str += BuildDatesString(cInt.altTZ.first, dates);
        }
        dt.JulianDateTime utc =
                new dt.JulianDateTime(
                        cInt.jd.GetJDN(), cInt.jd.GetFoD());
        dates = cInt.formatter.GetFormattedDates(utc, cInt.fmt);
        str += BuildDatesString("UT", dates);
        EditText resTxt = requireActivity().findViewById(R.id.resultTxt);
        resTxt.setText(Html.fromHtml(str));
    }

    private String BuildDatesString(String zone, HashMap<String, String> dates) {
        String res = String.format(this.fmt1, dates.get("Calendar"), zone,
                dates.get("DayName"), dates.get("Date"), dates.get("DoY"),
                dates.get("LeapYear"));
        if (!Objects.requireNonNull(dates.get("IWD")).isEmpty()) {
            res += String.format(this.fmt2, dates.get("IWD"));
        }
        res += String.format(this.fmt3, dates.get("JD"));
        return res;
    }

    private void showParserMessageDialog(int err_num, String error) {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction. We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        assert getFragmentManager() != null;
        androidx.fragment.app.FragmentTransaction ft = getFragmentManager()
                .beginTransaction();
        androidx.fragment.app.Fragment prev =
                getFragmentManager().findFragmentByTag(
                        "dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);        // Dismiss any existing dialog.
        // Create and show the dialog.
        ParserAlertDialogFragment newFragment = ParserAlertDialogFragment
                .newInstance(err_num, error);
        newFragment.show(ft, "dialog");
    }

    /**
     * Hide the soft keyboard.
     *
     * @param v the view.
     */
    private void HideSoftInput(View v) {
        InputMethodManager imm = (InputMethodManager) requireActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
    }

}
