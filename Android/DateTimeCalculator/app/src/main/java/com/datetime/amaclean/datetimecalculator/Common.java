/*=========================================================================

  Program:   Date Time Calculator
  File   :   Common.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

package com.datetime.amaclean.datetimecalculator;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import dt.CALENDAR_TYPE;
import dt.DTPair;
import dt.DateTimeFormat;
import dt.DateTimeFormatter;
import dt.DateTimeParser;
import dt.JD;
import dt.JulianDateTime;
import dt.TimeInterval;
import dt.TimeZones;
import dt.YMDHMS;

/**
 * Contains variables common to the fragments and any initialisation needed.
 * <p>
 * It is a singleton class.
 *
 * @author amaclean
 */
class Common {

    private static Common instance = null;
    final double seconds_in_day = 86400.0;
    // Date and time formatting parameters are held here.
    final DateTimeFormat fmt = new DateTimeFormat();
    // Here we define and instantiate the formatter and parser that
    // will be used by the various fragments.
    final DateTimeFormatter formatter = new DateTimeFormatter();
    final DateTimeParser parser = new DateTimeParser();
    final int jdPrecisionMax = 15;
    final int secPrecisionMax = 9;
    // Time zones
    // A pair consisting of the time zone name and the time offset in seconds.
    // For DateFragment
    final DTPair<String, Integer> TZ = new DTPair<>("UT", 0);
    final DTPair<String, Integer> altTZ = new DTPair<>("UT", 0);
    // For difference fragment
    final DTPair<String, Integer> TZA = new DTPair<>("UT", 0);
    final DTPair<String, Integer> TZB = new DTPair<>("UT", 0);
    // For sum fragment
    final DTPair<String, Integer> TZS = new DTPair<>("UT", 0);
    // Time zones and their names and values.
    Map<String, Integer> tzNames;
    Map<Integer, String> tzValues;
    // Date and time
    JulianDateTime utc;
    JD jd;
    boolean isAltTZChecked;
    // Date difference calculations.
    JulianDateTime utcA;
    JulianDateTime utcB;
    JD jdA;
    JD jdB;
    // Date sum calculations.
    JulianDateTime utcAS;
    TimeInterval tiDTBS;
    JD jdAS;
    TimeInterval tiJDBS;

    /*
     * A private Constructor prevents any other class from instantiating.
     */
    private Common() {
        Initialise();
    }

    /* Static 'instance' method */
    static Common Instance() {
        if (instance == null) {
            instance = new Common();
        }
        return instance;
    }

    private void Initialise() {
        // We use out own time zones.
        TimeZones timeZones = new TimeZones();
        tzNames = timeZones.GetTimeZonesByName();
        tzValues = timeZones.GetTimeZonesByValue();

        DateTimeParser parser = new DateTimeParser();

        // Set the current time zone.
        Calendar cal;
        cal = Calendar.getInstance(TimeZone.getDefault());
        TimeZone tz;
        tz = cal.getTimeZone();
        // Determine the time zone offset in seconds taking into account daylight savings.
        int deltaTZ = tz.getOffset(
                cal.get(Calendar.ERA),
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH),
                cal.get(Calendar.DAY_OF_WEEK),
                cal.get(Calendar.MILLISECOND));
        deltaTZ = deltaTZ / 1000;
        TZ.first = "UT";
        TZ.second = 0;
        altTZ.first = "UT";
        altTZ.second = 0;
        isAltTZChecked = false;
        TZA.first = "UT";
        TZA.second = 0;
        TZB.first = "UT";
        TZB.second = 0;
        TZS.first = "UT";
        TZS.second = 0;
        // Set these to local time, use UTC if it fails.
        DTPair<Boolean, String> tzResult = timeZones.GetTimeZoneName(deltaTZ);
        if (tzResult.first) {
            String tzName;
            tzName = tzResult.second;
            TZ.first = tzName;
            TZ.second = tzNames.get(tzName);
            TZA.first = tzName;
            TZA.second = tzNames.get(tzName);
            TZS.first = tzName;
            TZS.second = tzNames.get(tzName);
        }

        // Set the initial dates to the current system time.
        DTPair<Integer, YMDHMS> res =
                parser.ParseDateTime(this.GetCurrentSystemDateTime("GMT"), fmt);
        // Set the calendar date
        utc = new JulianDateTime();
        utc.SetDateTime(res.second, CALENDAR_TYPE.JULIAN_GREGORIAN);
        // Set the Julian date
        jd = new JD();
        jd = utc.GetJDFullPrecision();
        utcA = new JulianDateTime(utc);
        utcB = new JulianDateTime(utc);
        jdA = new JD(jd);
        jdB = new JD(jd);
        utcAS = new JulianDateTime(utc);
        tiDTBS = new TimeInterval();
        jdAS = new JD(jd);
        tiJDBS = new TimeInterval();

    }

    /**
     * Get the current system date and time.
     *
     * @param tz - the Time Zone e.g. GMT+10:00 Australia/Sydney
     */
    String GetCurrentSystemDateTime(String tz) {
        // Initialised with the current date and time.
        Calendar cal = new GregorianCalendar();
        SimpleDateFormat date_format = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        date_format.setTimeZone(TimeZone.getTimeZone(tz));
        return date_format.format(cal.getTime());
    }

}
