/*=========================================================================

  Program:   Date Time Calculator
  File   :   PrecisionDialogFragment.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
package com.datetime.amaclean.datetimecalculator;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PrecisionDialogFragment.OnCompleteListener} interface
 * to handle interaction events.
 * Use the {@link PrecisionDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PrecisionDialogFragment extends DialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "title";
    private final Common cInt = Common.Instance();
    private final int secPrecisionOriginal = cInt.fmt.precisionSeconds;
    private final int jdPrecisionOriginal = cInt.fmt.precisionFoD;
    // TODO: Rename and change types of parameters
    private int mParam1;
    private int secPrecision = cInt.fmt.precisionSeconds;
    private int jdPrecision = cInt.fmt.precisionFoD;
    private OnCompleteListener mListener;

    public PrecisionDialogFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PrecisionAlertDialogFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PrecisionDialogFragment newInstance() {
        PrecisionDialogFragment fragment = new PrecisionDialogFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, R.string.precision_menu_title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
        }
    }


    // See: http://stackoverflow.com/questions/15121373/returning-string-from-dialog-fragment-back-to-activity
    // The preferred method is to use a callback to get a signal from a Fragment.
    // Also, this is the recommended method proposed by Android at:
    //	http://developer.android.com/guide/components/fragments.html#CommunicatingWithActivity

    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    //return inflater.inflate(R.layout.fragment_precision_alert_dialog, container, false);
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(getString(mParam1));
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View v = inflater.inflate(R.layout.fragment_precision_dialog, null);
        final NumberPicker secNP = v.findViewById(R.id.seconds_precision_np);
        secNP.setMaxValue(cInt.secPrecisionMax);
        secNP.setMinValue(0);
        String[] secPrecNums = new String[cInt.secPrecisionMax + 1];
        for (int i = 0; i < secPrecNums.length; ++i) {
            secPrecNums[i] = Integer.toString(i);
        }
        secNP.setDisplayedValues(secPrecNums);
        secNP.setValue(cInt.fmt.precisionSeconds);
        // Prevent typing in the number picker.
        secNP.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        secNP.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {

            @Override
            public void onValueChange(NumberPicker arg0, int arg1, int arg2) {
                // TODO Auto-generated method stub
                secPrecision = secNP.getValue();
            }

        });

        final NumberPicker jdNP = v.findViewById(R.id.jd_precision_np);
        jdNP.setMaxValue(cInt.jdPrecisionMax);
        jdNP.setMinValue(0);
        String[] jdPrecNums = new String[cInt.jdPrecisionMax + 1];
        for (int i = 0; i < jdPrecNums.length; ++i) {
            jdPrecNums[i] = Integer.toString(i);
        }
        jdNP.setDisplayedValues(jdPrecNums);
        jdNP.setValue(cInt.fmt.precisionFoD);
        jdNP.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        jdNP.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {

            @Override
            public void onValueChange(NumberPicker arg0, int arg1, int arg2) {
                // TODO Auto-generated method stub
                jdPrecision = jdNP.getValue();
            }

        });
        builder.setView(v)//inflater.inflate(R.layout.fragment_precision_alert_dialog, null))
                // Add action buttons
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        onPrecisionChanged();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        secPrecision = secPrecisionOriginal;
                        jdPrecision = jdPrecisionOriginal;
                    }
                });
        return builder.create();
    }

    // make sure the Activity implemented it
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (OnCompleteListener) activity;
        } catch (final ClassCastException e) {
            throw new ClassCastException(activity +
                    " must implement OnCompleteListener");
        }
    }

    // Send the value back to the activity via the callback.
    private void onPrecisionChanged() {
        boolean changed = false;
        if (cInt.fmt.precisionSeconds != secPrecision) {
            cInt.fmt.precisionSeconds = secPrecision;
            changed = true;
        }
        if (cInt.fmt.precisionFoD != jdPrecision) {
            cInt.fmt.precisionFoD = jdPrecision;
            changed = true;
        }
        if (changed) {
            String s = "PrecisionChanged";
            this.mListener.onPrecisionChanged(s);
        }
    }

    @Override
    public void onPause() {
        // Commit any changes that should be persisted
        // beyond the current user session
        super.onPause();
        onPrecisionChanged();
    }

    public interface OnCompleteListener {
        void onPrecisionChanged(String msg);
    }
}
