/*=========================================================================

  Program:   Date Time Calculator
  File   :   AboutDialogFragment.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

package com.datetime.amaclean.datetimecalculator;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;

public class AboutDialogFragment extends DialogFragment {

    private static final String ARG_PARAM1 = "title";
    private int mParam1;

    public AboutDialogFragment() {
    }

    public static AboutDialogFragment newInstance() {
        AboutDialogFragment f = new AboutDialogFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, R.string.about_menu_title);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        // Set the title.
        String title = getString(mParam1);
        alertDialogBuilder.setTitle(Html.fromHtml(title));
        // Set the message.
        String s1 = getString(R.string.app_name);
        String s2 = getString(R.string.version);
        String s3 = Common.Instance().GetCurrentSystemDateTime("UTC");
        String fmtStr = "<h2>Date Time Calculations</h2>" +
                "<em>Project Name:</em> " +
                "DT" +
                "<br>" +
                "<em>Program Name:</em> " +
                "%s" +
                "<br>" +
                "<em>Version:</em> " +
                "%s" +
                "<br>" +
                "<em>Current UTC:</em> " +
                "<br>" +
                "%s" +
                "<br>";
        String msg = String.format(fmtStr, s1, s2, s3);
        alertDialogBuilder.setMessage(Html.fromHtml(msg));
        //null should be your on click listener
//        alertDialogBuilder.setPositiveButton("OK", null);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });


        return alertDialogBuilder.create();
    }

}
