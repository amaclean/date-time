/*=========================================================================

  Program:   Date Time Calculator
  File   :   DifferenceFragment.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

package com.datetime.amaclean.datetimecalculator;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Arrays;

import androidx.fragment.app.Fragment;
import dt.CALENDAR_TYPE;
import dt.DTPair;
import dt.YMDHMS;

public class DifferenceFragment extends Fragment implements
        OnClickListener {

    private final Common cInt = Common.Instance();
    private boolean viewCreated = false;

    public DifferenceFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_difference,
                container, false);
        // Let's do the variables.

        // Set title bar
        int title = SetTitle();
        ((MainActivity) requireActivity())
                .setActionBarTitle(getResources().getString(title));

        // See:
        // http://www.pelagodesign.com/blog/2009/05/20/iso-8601-date-validation-that-doesnt-suck/
        //		String regex = getResources().getString(R.string.ISO8601DatePattern);
        EditText txtA = rootView.findViewById(R.id.DTA);
        // Default to the numeric keypad.
        txtA.setRawInputType(android.text.InputType.TYPE_CLASS_DATETIME |
                android.text.InputType.TYPE_DATETIME_VARIATION_TIME);
        txtA.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {

                if (event == null) {
                    return false;
                }
                if (actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_ACTION_NEXT
                        || actionId == EditorInfo.IME_ACTION_PREVIOUS) {
                    // the user is done typing.
                    EditText txt =
                            requireActivity().findViewById(R.id.DTB);
                    txt.requestFocus();
                    return true; // consume
                }
                return false; // pass on to other listeners.
            }
        });
        txtA.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                /*
                 * When focus is lost update.
                 */
                if (!hasFocus) {
                    HideSoftInput(v);
                    GetDTA();
                    SetDTA();
                    SetJDA();
                    DifferenceDT();
                    DifferenceJD();
                }
            }
        });

        EditText txtB = rootView.findViewById(R.id.DTB);
        // Default to the numeric keypad.
        txtB.setRawInputType(android.text.InputType.TYPE_CLASS_DATETIME |
                android.text.InputType.TYPE_DATETIME_VARIATION_TIME);
        txtB.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {

                if (event == null) {
                    return false;
                }
                if (actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_ACTION_NEXT
                        || actionId == EditorInfo.IME_ACTION_PREVIOUS) {
                    // the user is done typing.
                    EditText txt =
                            requireActivity().findViewById(R.id.DTA);
                    txt.requestFocus();
                    return true; // consume
                }
                return false; // pass on to other listeners.
            }
        });
        txtB.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                /*
                 * When focus is lost update.
                 */
                if (!hasFocus) {
                    HideSoftInput(v);
                    GetDTB();
                    SetDTB();
                    SetJDB();
                    DifferenceDT();
                    DifferenceJD();
                }
            }
        });

        EditText jdtxtA = rootView.findViewById(R.id.JDA);
        jdtxtA.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (event == null) {
                    return false;
                }
                if (actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_ACTION_NEXT
                        || actionId == EditorInfo.IME_ACTION_PREVIOUS) {
                    // the user is done typing.
                    EditText txt = requireActivity().findViewById(R.id.JDB);
                    txt.requestFocus();
                    return true; // consume
                }
                return false; // pass on to other listeners.
            }
        });
        jdtxtA.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                /*
                 * When focus is lost update.
                 */
                if (!hasFocus) {
                    HideSoftInput(v);
                    GetJDA();
                    SetJDA();
                    SetDTA();
                    DifferenceDT();
                    DifferenceJD();
                }
            }
        });

        EditText jdtxtB = rootView.findViewById(R.id.JDB);
        jdtxtB.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (event == null) {
                    return false;
                }
                if (actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_ACTION_NEXT
                        || actionId == EditorInfo.IME_ACTION_PREVIOUS) {
                    // the user is done typing.
                    EditText txt = requireActivity().findViewById(R.id.JDA);
                    txt.requestFocus();
                    return true; // consume
                }
                return false; // pass on to other listeners.
            }
        });
        jdtxtB.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                /*
                 * When focus is lost update.
                 */
                if (!hasFocus) {
                    HideSoftInput(v);
                    GetJDB();
                    SetJDB();
                    SetDTB();
                    DifferenceDT();
                    DifferenceJD();
                }
            }
        });

        // Populate the spinner
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(
                rootView.getContext(), android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        // Fill the array adapter.
        int[] a = new int[cInt.tzValues.size()];
        int i = 0;
        for (Integer key : cInt.tzValues.keySet()) {
            a[i] = key;
            ++i;
        }
        Arrays.sort(a);
        for (i = 0; i < a.length; ++i) {
            adapter.add(cInt.tzValues.get(a[i]));
        }
        // Set up the spinners
        Spinner sA = rootView.findViewById(R.id.TZA);
        Spinner sB = rootView.findViewById(R.id.TZB);
        // Pass the array adapter to the spinner.
        sA.setAdapter(adapter);
        sB.setAdapter(adapter);

        sA.setSelection(adapter.getPosition(cInt.TZA.first));
        sB.setSelection(adapter.getPosition(cInt.TZB.first));

        sA.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapter, View v, int i,
                                       long lng) {
                // do something here
                cInt.TZA.first = adapter.getItemAtPosition(i).toString();
                cInt.TZA.second = cInt.tzNames.get(cInt.TZA.first);
                // Local time
                GetDTA();
                SetDTA();
                SetJDA();
                DifferenceDT();
                DifferenceJD();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // do something else
                HideSoftInput(arg0);
            }
        });

        sB.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapter, View v, int i,
                                       long lng) {
                // do something here
                cInt.TZB.first = adapter.getItemAtPosition(i).toString();
                cInt.TZB.second = cInt.tzNames.get(cInt.TZB.first);
                GetDTB();
                SetDTB();
                SetJDB();
                DifferenceDT();
                DifferenceJD();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // do something else
                HideSoftInput(arg0);
            }
        });


        // Set the times

        // Local time
        dt.JulianDateTime lt =
                new dt.JulianDateTime(cInt.utcA);
        lt.inc(cInt.TZA.second.doubleValue() / cInt.seconds_in_day);

        // Set the local time.
        String res = cInt.formatter.FormatAsYMDHMS(lt, cInt.fmt);
        txtA.setText(res);
        jdtxtA.setText(cInt.formatter.FormatJDLong(cInt.jdA, cInt.fmt));

        lt = new dt.JulianDateTime(cInt.utcB);
        lt.inc(cInt.TZB.second.doubleValue() / cInt.seconds_in_day);

        // Set the local time.
        res = cInt.formatter.FormatAsYMDHMS(lt, cInt.fmt);
        txtB.setText(res);
        jdtxtB.setText(cInt.formatter.FormatJDLong(cInt.jdA, cInt.fmt));

        // Difference
        dt.JulianDateTime utcAB =
                new dt.JulianDateTime(
                        cInt.utcA.sub(cInt.utcB));
        // Calculate the difference in days.
        dt.TimeInterval ti = utcAB.GetDeltaT();
        res = cInt.formatter.FormatTimeIntervalDHMS(ti, cInt.fmt);
        EditText resDTABTxt =
                rootView.findViewById(R.id.resultTxtDTAB);
        resDTABTxt.setText(res);
        // Calculate the difference in days.
        ti = new dt.TimeInterval(
                cInt.jdA.GetJDN() - cInt.jdB.GetJDN(),
                cInt.jdA.GetFoD() - cInt.jdB.GetFoD());
        res = cInt.formatter.FormatTimeInterval(ti, cInt.fmt);
        EditText resJDABTxt =
                rootView.findViewById(R.id.resultTxtJDAB);
        resJDABTxt.setText(res);

        // The buttons.
        Button nowA = rootView.findViewById(R.id.nowABtn);
        nowA.setOnClickListener(this);

        Button nowB = rootView.findViewById(R.id.nowBBtn);
        nowB.setOnClickListener(this);

        this.viewCreated = true;

        return rootView;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.nowABtn) {
            Button nowA = v.findViewById(R.id.nowABtn);
            if (nowA.isPressed()) {
                SetCurrentTimeA();
            }
        }
        if (v.getId() == R.id.nowBBtn) {
            Button nowA = v.findViewById(R.id.nowBBtn);
            if (nowA.isPressed()) {
                SetCurrentTimeB();
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && viewCreated) {
            UpdateDisplay();
        }
    }

    @Override
    public void onPause() {
        // Commit any changes that should be persisted
        // beyond the current user session
        super.onPause();
        UpdateDisplay();
    }

    public void onResume() {
        super.onResume();

        // Set title bar
        int title = SetTitle();
        ((MainActivity) requireActivity())
                .setActionBarTitle(getResources().getString(title));
    }

    public void UpdateDisplay() {
        SetDTA();
        SetDTB();

        SetJDA();
        SetJDB();

        DifferenceDT();
        DifferenceJD();

        // Set title bar
        int title = SetTitle();
        ((MainActivity) requireActivity())
                .setActionBarTitle(getResources().getString(title));
    }

    private int SetTitle() {
        CALENDAR_TYPE calendar = cInt.fmt.calendar;
        switch (calendar) {
            case JULIAN:
                return R.string.title_section2_J;
            case GREGORIAN:
                return R.string.title_section2_G;
            case JULIAN_GREGORIAN:
            default:
                return R.string.title_section2_JG;
        }
    }


    private void SetCurrentTimeA() {
        DTPair<Integer, YMDHMS> res = cInt.parser.ParseDateTime(
                cInt.GetCurrentSystemDateTime("GMT"), cInt.fmt);
        if (res.first != 1) {
            return;
        }
        // Set the calendar date
        cInt.utcA.SetDateTime(res.second, CALENDAR_TYPE.JULIAN_GREGORIAN);
        // Set the Julian date
        cInt.jdA = cInt.utcA.GetJDFullPrecision();

        SetDTA();
        SetJDA();

        DifferenceDT();
        DifferenceJD();
    }

    private void SetCurrentTimeB() {
        DTPair<Integer, YMDHMS> res = cInt.parser.ParseDateTime(
                cInt.GetCurrentSystemDateTime("GMT"), cInt.fmt);
        if (res.first != 1) {
            return;
        }
        // Set the calendar date
        cInt.utcB.SetDateTime(res.second, CALENDAR_TYPE.JULIAN_GREGORIAN);
        // Set the Julian date
        cInt.jdB = cInt.utcB.GetJDFullPrecision();

        SetDTB();
        SetJDB();

        DifferenceDT();
        DifferenceJD();
    }

    private void SetDTA() {
        // Local time
        dt.JulianDateTime lt =
                new dt.JulianDateTime(cInt.utcA);
        lt.inc(cInt.TZA.second.doubleValue() / cInt.seconds_in_day);

        // Set the local time.
        String dt = cInt.formatter.FormatAsYMDHMS(lt, cInt.fmt);
        EditText cdTxt = requireActivity().findViewById(R.id.DTA);
        cdTxt.setText(dt);
    }

    private void SetDTB() {
        // Local time
        dt.JulianDateTime lt =
                new dt.JulianDateTime(cInt.utcB);
        lt.inc(cInt.TZB.second.doubleValue() / cInt.seconds_in_day);

        // Set the local time.
        String dt = cInt.formatter.FormatAsYMDHMS(lt, cInt.fmt);
        EditText cdTxt = requireActivity().findViewById(R.id.DTB);
        cdTxt.setText(dt);
    }

    private void SetJDA() {
        EditText jdTxt = requireActivity().findViewById(R.id.JDA);
        jdTxt.setText(cInt.formatter.FormatJDLong(cInt.jdA, cInt.fmt));

    }

    private void SetJDB() {
        EditText jdTxt = requireActivity().findViewById(R.id.JDB);
        jdTxt.setText(cInt.formatter.FormatJDLong(cInt.jdB, cInt.fmt));

    }

    private void GetDTA() {
        EditText cdTxt = requireActivity().findViewById(R.id.DTA);
        DTPair<Integer, YMDHMS> res =
                cInt.parser.ParseDateTime(cdTxt.getText().toString(), cInt.fmt);
        if (res.first != 1) {
            showParserMessageDialog(res.first, cdTxt.getText().toString());
            return;
        }
        cInt.utcA.SetDateTime(res.second, cInt.fmt.calendar);
        // Convert to UTC
        cInt.utcA.dec(cInt.TZA.second.doubleValue() / cInt.seconds_in_day);
        cInt.jdA = cInt.utcA.GetJDFullPrecision();
    }

    private void GetDTB() {
        EditText cdTxt = requireActivity().findViewById(R.id.DTB);
        DTPair<Integer, YMDHMS> res =
                cInt.parser.ParseDateTime(cdTxt.getText().toString(), cInt.fmt);
        if (res.first != 1) {
            showParserMessageDialog(res.first, cdTxt.getText().toString());
            return;
        }
        cInt.utcB.SetDateTime(res.second, cInt.fmt.calendar);
        // Convert to UTC
        cInt.utcB.dec(cInt.TZB.second.doubleValue() / cInt.seconds_in_day);
        cInt.jdB = cInt.utcB.GetJDFullPrecision();
    }

    private void GetJDA() {
        EditText jdTxt = requireActivity().findViewById(R.id.JDA);
        DTPair<Integer, DTPair<Integer, Double>> res =
                cInt.parser.ParseDecNum(jdTxt.getText().toString());
        if (res.first != 1) {
            showParserMessageDialog(res.first, jdTxt.getText().toString());
            return;
        }
        cInt.jdA.SetJD(res.second.first, res.second.second);
        //		cInt.jdA = cInt.formatter.ParseJDString(jdTxt.getText().toString());
        cInt.utcA.SetDateTime(cInt.jdA.GetJDN(), cInt.jdA.GetFoD());
    }

    private void GetJDB() {
        EditText jdTxt = requireActivity().findViewById(R.id.JDB);
        DTPair<Integer, DTPair<Integer, Double>> res =
                cInt.parser.ParseDecNum(jdTxt.getText().toString());
        if (res.first != 1) {
            showParserMessageDialog(res.first, jdTxt.getText().toString());
            return;
        }
        cInt.jdB.SetJD(res.second.first, res.second.second);
        //		cInt.jdB = cInt.formatter.ParseJDString(jdTxt.getText().toString());
        cInt.utcB.SetDateTime(cInt.jdB.GetJDN(), cInt.jdB.GetFoD());
    }

    private void DifferenceDT() {
        // Difference
        dt.JulianDateTime utcAB =
                new dt.JulianDateTime(
                        cInt.utcA.sub(cInt.utcB));
        // Calculate the difference in days.
        dt.TimeInterval ti = utcAB.GetDeltaT();
        String res = cInt.formatter.FormatTimeIntervalDHMS(ti, cInt.fmt);
        EditText resDTABTxt =
                requireActivity().findViewById(R.id.resultTxtDTAB);
        resDTABTxt.setText(res);
    }

    private void DifferenceJD() {
        // Calculate the difference in days.
        dt.TimeInterval ti =
                new dt.TimeInterval(
                        cInt.jdA.GetJDN() - cInt.jdB.GetJDN(),
                        cInt.jdA.GetFoD() - cInt.jdB.GetFoD());
        String res = cInt.formatter.FormatTimeInterval(ti, cInt.fmt);
        EditText resJDABTxt = requireActivity().findViewById(
                R.id.resultTxtJDAB);
        resJDABTxt.setText(res);
    }

    private void showParserMessageDialog(int err_num, String error) {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction. We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        assert getFragmentManager() != null;
        androidx.fragment.app.FragmentTransaction ft = getFragmentManager()
                .beginTransaction();
        androidx.fragment.app.Fragment prev =
                getFragmentManager().findFragmentByTag(
                        "dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);        // Dismiss any existing dialog.
        // Create and show the dialog.
        ParserAlertDialogFragment newFragment = ParserAlertDialogFragment
                .newInstance(err_num, error);
        newFragment.show(ft, "dialog");
    }

    /**
     * Hide the soft keyboard.
     *
     * @param v the view.
     */
    private void HideSoftInput(View v) {
        InputMethodManager imm = (InputMethodManager) requireActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
    }

}
