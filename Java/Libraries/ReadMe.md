# The dt.jar library

Copy this library from **../DateTime/Java/DateTime/out/artifacts/dt/dt.jar** and place the `dt.jar` library in this folder (**../DateTime/Java/Libraries**) so that any other examples you create can access it. Also copy this file to **../../Android/DateTimeCalculator/app/libs**.

Remember to update the library should anything change in **../DateTime/src/dt/**.

Currently the IntelliJ editor builds a .jar file that is not recognised by Android.
So the android application now uses a copy of the dt folder.
