package dtExamples.TimeZone;

import dt.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Purpose: Demonstrate how to do time zone calculations.
 * <p>
 * Demonstrates: 1) How to convert between time zones. 2) Date interval
 * arithmetic. 3) Format the date for output. 4) Format the time zone for
 * output. 5) Decrementing (dec) and incrementing (inc) a date.
 *
 * @author Andrew J. P. Maclean
 */
public class TimeZone {

  /**
   * @param args Not used.
   */
  public static void main(String[] args) {
    System.out.print("Started:  ");
    GetDate();
    System.out.println("--------------------------------------------");
    System.out.println("Demonstrating time zone calculations.");
    System.out.println("--------------------------------------------");
    TimeZoneCalculation();
    System.out.println("--------------------------------------------");
    System.out.print("Finished: ");
    GetDate();

    System.exit(1);

  }

  /**
   * Get the current system date and time.
   */
  private static void GetDate() {
    Calendar cal = new GregorianCalendar();
    Date date = new Date();
    cal.setTime(date);
    SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss.SSS Z");
    System.out.println(date_format.format(cal.getTime()));
  }

  private static void TimeZoneCalculation() {
    DateTimeFormatter dtf = new DateTimeFormatter();
    TimeZones timeZones = new TimeZones();

    // We are going to work with the same calendar.
    DateTimeFormat fmt = new DateTimeFormat();
    fmt.calendar = CALENDAR_TYPE.JULIAN_GREGORIAN;

    // Set up the timezone and the alternate one (in hours).
    DTPair<Boolean, Integer> tmp = timeZones.GetTimeZoneValue("UT-09:30");
    double tz = 0;
    if (tmp.first) {
      tz = tmp.second / 3600.0;
    }
    tmp = timeZones.GetTimeZoneValue("UT+10");
    double tzAlt = 0;
    if (tmp.first) {
      tzAlt = tmp.second / 3600.0;
    }

    // Get the time for the time zone.
    JulianDateTime jdlt = new JulianDateTime(2012, 1, 3, 9, 46, 12.3215, fmt.calendar);

    // Instantiate a class to hold UTC.
    JulianDateTime jdut = new JulianDateTime(jdlt);
    // Subtract the time zone difference in days to get UTC.
    jdut.dec(tz / 24.0);

    // Now convert to the new time zone.
    // Instantiate a class to hold the alternate time.
    JulianDateTime jdaltlt = new JulianDateTime(jdut);
    // Add the time zone difference in days to get local time.
    jdaltlt.inc(tzAlt / 24.0);

    // Nicely format the output.

    // We generally display the time zone at the end of the time string.
    HM hm = new HM((int) (tz), (tz - (int) (tz)) * 60.0);

    String localTime = dtf.FormatAsYMDHMS(jdlt, fmt) + " " + dtf.FormatTZAsHM(hm);
    String utcTime = dtf.FormatAsYMDHMS(jdut, fmt); // No need for the time
    // zone (00:00).
    hm.hour = (int) (tzAlt);
    hm.minute = (tzAlt - (int) (tzAlt)) * 60.0;
    String alternateLocalTime = dtf.FormatAsYMDHMS(jdaltlt, fmt) + " " + dtf.FormatTZAsHM(hm);

    System.out.printf("%s%s%n", "Local Time:           ", localTime);
    System.out.printf("%s%s%n", "UT:                   ", utcTime);
    System.out.printf("%s%s%n", "Alternate Local Time: ", alternateLocalTime);
    System.out.printf("%s%n%s%n%s%n", "Note:",
        "1) That the alternate local time is for the following day relative to UT.",
        "2) These three times represent the same time, so they are equivalent.");
  }

}
