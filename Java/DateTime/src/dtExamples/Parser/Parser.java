package dtExamples.Parser;

import dt.*;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Purpose: Demonstrate the usage of the date and time helper parsers.
 *
 * @author Andrew J. P. Maclean
 */
public class Parser {

  // This class contains the formatting and parsing functions.
  private static final DateTimeParser dtp = new DateTimeParser();

  private static final DateTimeFormat fmt = new DateTimeFormat();

  // We declare and initialise some test data for testing the parser here.

  private static final ArrayList<String> decStrs = new ArrayList<String>() {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    {
      add("456.123");
      add("123");
      add("-456.123");
      add("+123");
      add(".123");
      add("x");
      add("");
    }
  };

  private static final ArrayList<String> dateStrs = new ArrayList<String>() {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    {
      add("-12-Jul-20");
      add("2015-01-01");
      add("2015-x-01");
      add("2015-1-1");
      add("");
      add("1234");
      add("2015 01 01");
    }
  };

  private static final ArrayList<String> timeStrs = new ArrayList<String>() {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    {
      add("12");
      add("-12:12");
      add("-12:15:23.456");
      add("-12-15-23.456");
      add("12:23.1x");
      add("");
    }
  };

  private static final ArrayList<String> dateTimeStrs = new ArrayList<String>() {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    {
      add("2015-01-01 12");
      add("2015-01-01 -12:12");
      add("2015-01-01 -12:15:23.456");
      add("-12-01-23.456");
      add("-4712-01-01 -12-01-23.456");
      add("2015-01-01 -12:15:23.456x");
      add("2015-01-01");
      add("");
    }
  };

  private static final ArrayList<String> timeIntervalStrs = new ArrayList<String>() {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    {
      add(" 0d  00:00:0");
      add("-1d -06:00:00.0");
      add("-1d  06:00:00.0");
      add(" 1d -06:00:00.0");
      add(" 1d  06:00:00.0");
      add(" 12  ");
      add("-12 2.3 ");
      add("-12 2:3:4x ");
      add("-12 6:0:10.3 ");
      add("6x -12 6:0:10.3 ");
      add("");
    }
  };

  /**
   * @param args Not used.
   */
  public static void main(String[] args) {
    // If you need to change the parser messages, e.g change the language,
    // here's how to do it:
    dtp.parserMessages.put(2, "No hay datos.");

    System.out.print("Started:  ");
    System.out.println(GetCurrentSystemDateTime(TimeZone.getDefault().getID()));
    System.out.println("--------------------------------------------");
    System.out.println("Demonstrating the parsers.");
    System.out.println("--------------------------------------------");
    ParseDecStr();
    ParseDateStr();
    ParseTimeStr();
    ParseDateTimeStr();
    ParseDateTimeIntervalStr();
    System.out.println("--------------------------------------------");
    System.out.print("Finished: ");
    System.out.println(GetCurrentSystemDateTime(TimeZone.getDefault().getID()));
    System.exit(1);

  }

  /**
   * Get the current system date and time.
   *
   * @param tz - the Time Zone e.g. GMT+10:00 Australia/Sydney
   */
  private static String GetCurrentSystemDateTime(String tz) {
    // Initialised with the current date and time.
    Calendar cal = new GregorianCalendar();
    SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
    date_format.setTimeZone(TimeZone.getTimeZone(tz));
    return date_format.format(cal.getTime());
  }

  private static void ParseDecStr() {
    System.out.println("Parsing dates.");
    for (String p : decStrs) {
      DTPair<Integer, DTPair<Integer, Double>> res = dtp.ParseDecNum(p);
      if (res.first == 1) {
        String v = String.format("(%d %15.12f)", res.second.first, res.second.second);
        System.out.println(p + " = " + v);
      } else {
        System.out.println(p + ": " + dtp.parserMessages.get(res.first));
      }
    }
    System.out.println();
  }

  private static void ParseDateStr() {
    System.out.println("Parsing dates.");
    for (String p : dateStrs) {
      DTPair<Integer, YMD> res = dtp.ParseDate(p, fmt);
      if (res.first == 1) {
        System.out.println(p + " = " + res.second.GetString());
      } else {
        System.out.println(p + ": " + dtp.parserMessages.get(res.first));
      }
    }
    System.out.println();
  }

  private static void ParseTimeStr() {
    System.out.println("Parsing times.");
    for (String p : timeStrs) {
      DTPair<Integer, HMS> res = dtp.ParseTime(p, fmt);
      if (res.first == 1) {
        System.out.println(p + " = " + res.second.GetString());
      } else {
        System.out.println(p + ": " + dtp.parserMessages.get(res.first));
      }
    }
    System.out.println();
  }

  private static void ParseDateTimeStr() {
    System.out.println("Parsing date and times.");
    for (String p : dateTimeStrs) {
      DTPair<Integer, YMDHMS> res = dtp.ParseDateTime(p, fmt);
      if (res.first == 1) {
        System.out.println(p + " = " + res.second.GetString());
      } else {
        System.out.println(p + ": " + dtp.parserMessages.get(res.first));
      }
    }
    System.out.println();
  }

  private static void ParseDateTimeIntervalStr() {
    System.out.println("Parsing time intervals.");
    for (String p : timeIntervalStrs) {
      DTPair<Integer, TimeInterval> res = dtp.ParseTimeInterval(p, fmt);
      if (res.first == 1) {
        System.out.println(p + " = " + res.second.GetString());
      } else {
        System.out.println(p + ": " + dtp.parserMessages.get(res.first));
      }
    }
    System.out.println();
  }

}
