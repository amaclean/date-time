package dtExamples.DateTime;

import dt.*;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Purpose: Demonstrate the usage of the date and time library.
 * <p>
 * The initialisation of the JulianDateTime class along with its usage is
 * demonstrated. How to nicely format the date is also demonstrated. This
 * example also demonstrates the importance of selecting the correct calendar to
 * use.
 *
 * @author Andrew J. P. Maclean
 */
public class DateTime {

  private final static DateTimeFormatter df = new DateTimeFormatter();

  /**
   * @param args Not used.
   */
  public static void main(String[] args) {
    System.out.print("Started:  ");
    System.out.println(GetCurrentSystemDateTime(TimeZone.getDefault().getID()));
    System.out.println("--------------------------------------------");
    System.out.println("Demonstrating details of dates.");
    System.out.println("--------------------------------------------");
    DateDetails();
    System.out.println("--------------------------------------------");
    System.out.println("Demonstrating differences between calendars.");
    System.out.println("--------------------------------------------");
    CalenderDifferences();
    System.out.println("--------------------------------------------");
    System.out.print("Finished: ");
    System.out.println(GetCurrentSystemDateTime(TimeZone.getDefault().getID()));
    System.exit(1);

  }

  /**
   * Get the current system date and time.
   *
   * @param tz - the Time Zone e.g. GMT+10:00 Australia/Sydney
   */
  private static String GetCurrentSystemDateTime(String tz) {
    // Initialised with the current date and time.
    Calendar cal = new GregorianCalendar();
    SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
    date_format.setTimeZone(TimeZone.getTimeZone(tz));
    return date_format.format(cal.getTime());
  }

  /**
   * Display some information about a date.
   */
  private static void DateDetails() {

    // Define the formatting parameters and set the calendar.
    // Note: fmt.calendar is JULIAN_GREGORIAN by default.
    DateTimeFormat fmt = new DateTimeFormat();
    fmt.dayName = DAY_NAME.UNABBREVIATED;
    fmt.monthName = MONTH_NAME.UNABBREVIATED;

    // We instantiate a YMDHMS class to hold the date and time.
    // You can:
    // 1) Individually set the time and date elements in this class
    // as the members are public.
    // 2) Set either the date or time
    // 3) Set the date and time.
    YMDHMS ymdhms = new YMDHMS(2012, 12, 31, 15, 12, 11.123);
    // Instantiate a JulianDateTime class.
    JulianDateTime jdt = new JulianDateTime();
    // Set the date and time
    jdt.SetDateTime(ymdhms, fmt.calendar);

    HashMap<String, String> dates = df.GetFormattedDates(jdt, fmt);
    System.out.printf("%s\n\n", BuildDateString(dates));

    // Try some other dates.
    fmt.calendar = CALENDAR_TYPE.JULIAN;
    // Use a copy constructor.
    jdt = new JulianDateTime(1900, 12, 31, 15, 12, 11.123, fmt.calendar);
    dates = df.GetFormattedDates(jdt, fmt);
    System.out.printf("%s\n\n", BuildDateString(dates));
    fmt.calendar = CALENDAR_TYPE.GREGORIAN;
    jdt = new JulianDateTime(1900, 12, 31, 15, 12, 11.123, fmt.calendar);
    dates = df.GetFormattedDates(jdt, fmt);
    System.out.printf("%s\n\n", BuildDateString(dates));
    fmt.calendar = CALENDAR_TYPE.JULIAN_GREGORIAN;
    jdt = new JulianDateTime(1900, 12, 31, 15, 12, 11.123, fmt.calendar);
    dates = df.GetFormattedDates(jdt, fmt);
    System.out.printf("%s\n\n", BuildDateString(dates));
  }

  /**
   * Demonstrate the differences between calendars.
   */
  private static void CalenderDifferences() {

    // We instantiate a YMDHMS class to hold the date and time.
    // You can:
    // 1) Individually set the time and date elements in this class
    // as the members are public.
    // 2) Set either the date or time
    // 3) Set the date and time.
    YMDHMS ymdhms = new YMDHMS(-4712, 1, 1, 0, 0, 1 / 3.0);
    // Instantiate a JulianDateTime class.
    JulianDateTime jdt = new JulianDateTime();
    // Set the date and time
    jdt.SetDateTime(ymdhms, CALENDAR_TYPE.JULIAN_GREGORIAN);

    // Define the formatting parameters.
    // We will use two different formats so fill a vector
    // with the formats and modify as appropriate.
    java.util.List<DateTimeFormat> fmt = Arrays.asList(new DateTimeFormat(), new DateTimeFormat());
    fmt.get(1).dayName = DAY_NAME.UNABBREVIATED;
    fmt.get(1).monthName = MONTH_NAME.UNABBREVIATED;

    // Output the date in several formats for different calendars.
    System.out.printf("%s%n%s%n", "It is important to specify the calendar since the dates",
        " may be different for different calendars:");
    fmt.get(1).calendar = fmt.get(0).calendar = CALENDAR_TYPE.JULIAN_GREGORIAN;
    HashMap<String, String> dates = GetDates(jdt, fmt);
    System.out.printf("%s\n\n", BuildShortDateString(dates));

    // See how the date has changed here.
    fmt.get(1).calendar = fmt.get(0).calendar = CALENDAR_TYPE.GREGORIAN;
    dates = GetDates(jdt, fmt);
    System.out.printf("%s\n\n", BuildShortDateString(dates));
    // It is easy to set a different date and time, here we just change the
    // year.
    // Let's do this for the current time.
    ymdhms.ymd.year = 2012;
    jdt.SetDateTime(ymdhms, CALENDAR_TYPE.JULIAN_GREGORIAN);
    fmt.get(1).calendar = fmt.get(0).calendar = CALENDAR_TYPE.GREGORIAN;
    dates = GetDates(jdt, fmt);
    System.out.printf("%s\n\n", BuildShortDateString(dates));
    fmt.get(1).calendar = fmt.get(0).calendar = CALENDAR_TYPE.JULIAN;
    dates = GetDates(jdt, fmt);
    System.out.printf("%s\n\n", BuildShortDateString(dates));
  }

  /**
   * Assemble a string of date information.
   *
   * @param dates A map of the dates.
   * @return A string of date information.
   */
  private static String BuildDateString(HashMap<String, String> dates) {
    String fmt = "Details for:         %s" + "\nCalendar             %s" + "\nJD (full precision): %s"
        + "\nJD (short form):     %s" + "\nDate:                %s" + "\nDay of the year:     %s"
        + "\nDay of the week:     %s" + " = %s or %s";
    if (!dates.get("IWD").isEmpty()) {
      fmt += "\nISO Day of the week: %s";
    }
    fmt += "\nLeap Year:           %s";
    if (!dates.get("IWD").isEmpty()) {
      return String.format(fmt, dates.get("NamedDate"), dates.get("Calendar"), dates.get("JD"),
          dates.get("JDShort"), dates.get("Date"), dates.get("DoY"), dates.get("DoW"), dates.get("DayName"),
          dates.get("DayNameAbbrev"), dates.get("IWD"), dates.get("LeapYear"));
    } else {
      return String.format(fmt, dates.get("NamedDate"), dates.get("Calendar"), dates.get("JD"),
          dates.get("JDShort"), dates.get("Date"), dates.get("DoY"), dates.get("DoW"), dates.get("DayName"),
          dates.get("DayNameAbbrev"), dates.get("LeapYear"));
    }
  }

  /**
   * Assemble a string of date information.
   *
   * @param dates A map of the dates.
   * @return A string of date information.
   */
  private static String BuildShortDateString(HashMap<String, String> dates) {
    String fmt = "For JD: %s we have:\n%-16s: %s\n%18s%s";
    return String.format(fmt, dates.get("JDShort"), dates.get("Calendar"), dates.get("NamedDate"), " ",
        dates.get("Date"));
  }

  private static HashMap<String, String> GetDates(JulianDateTime jdt, java.util.List<DateTimeFormat> fmt) {
    HashMap<String, String> res = new HashMap<>();
    int i = 0;
    for (DateTimeFormat p : fmt) {
      HashMap<String, String> dates = df.GetFormattedDates(jdt, p);
      // Pick what we want to put in the result.
      switch (i) {
        case 0: {
          res.put("Calendar", dates.get("Calendar"));
          res.put("JDShort", dates.get("JDShort"));
          res.put("Date", dates.get("Date"));
          break;
        }
        case 1:
        default: {
          res.put("NamedDate", dates.get("NamedDate"));
        }
      }
      ++i;
    }
    return res;
  }

}
