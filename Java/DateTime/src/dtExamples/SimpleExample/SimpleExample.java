package dtExamples.SimpleExample;

import dt.*;

import java.util.HashMap;

// import dt.CALENDAR_TYPE;

/**
 * Purpose: A simple example demonstrating the usage of the date and time
 * library.
 * <p>
 * We parse a date string and output everything that we know about the date.
 *
 * @author Andrew J. P. Maclean
 */
public class SimpleExample {

  private final static DateTimeFormatter formatter = new DateTimeFormatter();
  private final static DateTimeParser parser = new DateTimeParser();

  /**
   * @param args Not used.
   */
  public static void main(String[] args) {
    System.out.println("--------------------------------------------");
    System.out.println("Demonstrating details of dates.");
    System.out.println("--------------------------------------------");
    String dateToParse = "2016-12-31 23:59:59.99997";
    DateDetails(dateToParse);
    System.out.println("--------------------------------------------");
    System.exit(1);
  }

  /**
   * Display some information about a date.
   */
  private static void DateDetails(String dateToParse) {

    // Define the formatting parameters and set the calendar.
    // Note: fmt.calendar is JULIAN_GREGORIAN by default.
    DateTimeFormat fmt = new DateTimeFormat();
    fmt.dayName = DAY_NAME.UNABBREVIATED;
    fmt.monthName = MONTH_NAME.UNABBREVIATED;
    fmt.precisionSeconds = 6;
    fmt.precisionFoD = 10;

    // Here we just parse the date and output everything about the date,
    // (if the parse was successful).
    DTPair<Integer, YMDHMS> parsedDate = parser.ParseDateTime(dateToParse, fmt);
    if (parsedDate.first == 1) {
      JulianDateTime jdt = new JulianDateTime(parsedDate.second, fmt.calendar);
      HashMap<String, String> dates = formatter.GetFormattedDates(jdt, fmt);
      // Output the date and other information about the date.
      System.out.printf("%s\n", BuildDateString(dates));
    } else {
      // Why did it fail?
      System.out.printf(String.format("Parse failed: %s", parser.parserMessages.get(parsedDate.first)));
    }
  }

  /**
   * Assemble a string of date information.
   *
   * @param dates A map of the dates.
   * @return A string of date information.
   */
  private static String BuildDateString(HashMap<String, String> dates) {
    String fmt = "Details for:         %s" + "\nCalendar             %s" + "\nJD (full precision): %s"
        + "\nJD (short form):     %s" + "\nDate:                %s" + "\nDay of the year:     %s"
        + "\nDay of the week:     %s" + " = %s or %s";
    if (!dates.get("IWD").isEmpty()) {
      fmt += "\nISO Day of the week: %s";
    }
    fmt += "\nLeap Year:           %s";
    if (!dates.get("IWD").isEmpty()) {
      return String.format(fmt, dates.get("NamedDate"), dates.get("Calendar"), dates.get("JD"),
          dates.get("JDShort"), dates.get("Date"), dates.get("DoY"), dates.get("DoW"), dates.get("DayName"),
          dates.get("DayNameAbbrev"), dates.get("IWD"), dates.get("LeapYear"));
    } else {
      return String.format(fmt, dates.get("NamedDate"), dates.get("Calendar"), dates.get("JD"),
          dates.get("JDShort"), dates.get("Date"), dates.get("DoY"), dates.get("DoW"), dates.get("DayName"),
          dates.get("DayNameAbbrev"), dates.get("LeapYear"));
    }
  }

}
