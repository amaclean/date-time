/*=========================================================================

  Program:   Date Time Library
  File   :   DateTimeConversions.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

/*

 */
package dt;

/**
 * This class provides conversion routines between Calendrical dates and times
 * and the date and time as a Julian Day Number (JDN) and fraction of the day
 * (FoD).
 * <p>
 * The Julian Day is defined as the interval of time in days and fractions of a
 * day since 4713 B.C. January 1, Greenwich noon Julian proleptic calendar.
 * <p>
 * The Julian Day Number is the integer portion of the Julian Day and JD 0 will
 * be equivalent to 4713 B.C. January 1, Greenwich noon Julian proleptic
 * calendar, a Monday or equivalently -4712-01-01 12:00:00.
 * <p>
 * The algorithms defined here are known to work over the range: -13200-AUG-15
 * 00:00 JD -3100015.50 to 17191-MAR-15 00:00 JD 8000016.50
 * <p>
 * For further details see: "Explanatory Supplement to the Astronomical Almanac"
 * , Ed. P. Kennith Seidelmann, University Science Books, 1992.
 * <p>
 * Function names that start with a lower case letter indicate conversion
 * methods.
 * <p>
 * The conversion methods generally follow this pattern:
 * <p>
 * [jd|cd]2[cd|jd[J|G|JG] where
 * <p>
 * jd - julian date, cd - calendrical date, J - Julian Calendar, G - Gregorian
 * Calendar, JG - Julian-Gregorian Calendar.
 * <p>
 * [h|hm|hms]2[h|hm[hms] where
 * <p>
 * h - hours, hm - hours and minutes, hms - hours minutes and seconds.
 * <p>
 * Calendrical dates are stored as a class of type YMD or YMDHMS (if hours
 * minutes and seconds are needed), Julian dates are stored as a class of type
 * JD and ISO Week Dates are stored as a class of type ISOWeekDate. Times are
 * stored as a class of type HM (hours and minutes) or HMS (hours, minutes and
 * seconds).
 *
 * @author Andrew J. P. Maclean
 */
public class DateTimeConversions {

  /**
   * @param year     Year
   * @param calendar The calendar to use.
   * @return true if it is a leap year.
   */
  public boolean isLeapYear(int year, CALENDAR_TYPE calendar) {
    boolean res;
    switch (calendar) {
      case JULIAN:
        // If the years are greater than 1582 then this is the julian
        // proleptic calendar.
        res = (year % 4) == 0;
        break;
      case GREGORIAN:
        // If the years are less than 1582 then this is the gregorian
        // proleptic calendar.
        if ((year % 100) == 0) {
          res = (year % 400) == 0; // Gregorian century leap year.
        } else {
          res = (year % 4) == 0;
        }
        break;
      case JULIAN_GREGORIAN:
      default:
        if (year < JG_CHANGEOVER.JULIAN_GREGORIAN_YEAR.getValue()) { // Julian
          // calendar.
          res = (year % 4) == 0;
        } else // Gregorian calendar.
          if ((year % 100) == 0) {
            res = (year % 400) == 0; // Gregorian century leap year.
          } else {
            res = (year % 4) == 0;
          }
    }
    return res;
  }

  // ************ Date Conversions.

  /**
   * Calculate the Julian day number of the date. The Proleptic Julian
   * Calendar is used.
   *
   * @param ymd The date (year, month, day).
   * @return The Julian day number of the date, an integer running from noon
   * to noon.
   */
  public int cd2jdJ(YMD ymd) {
    int year = ymd.year;
    int month = ymd.month;
    int day = ymd.day;
    if (year > -4723) {
      int a = 367 * year;
      int b = (7 * (year + 5001 + (month - 9) / 7)) / 4;
      int c = (275 * month) / 9 + day + 1729777;
      return a - b + c;
    } else {
      // Handle dates < 4712-01-01
      // We add 4712 * 3 = 14136 years to the year, do the calculations
      // and
      // then subtract 4712 * 3 * 365.25 = 5163174 days to get the correct
      // julian day number.
      int a = 367 * (year + 14136);
      int b = (7 * ((year + 14136) + 5001 + (month - 9) / 7)) / 4;
      int c = (275 * month) / 9 + day + 1729777;
      return a - b + c - 5163174;
    }
  }

  /**
   * Calculate the date from the Julian day number. The Proleptic Julian
   * Calendar is used.
   *
   * @param jd The julian day number, an integer integer running from noon to
   *           noon.
   * @return The date (year, month, day).
   */
  public YMD jd2cdJ(int jd) {
    YMD ymd = new YMD();
    int j = jd + 1402;
    if (jd < 0) {
      // Set an origin for j so that j is positive.
      // This corresponds to -14136-01-01
      j += 5163174; // 4712 * 3 * 365.25 days
    }
    int k = (j - 1) / 1461;
    int l = j - 1461 * k;
    int n = (l - 1) / 365 - l / 1461;
    int i = l - 365 * n + 30;
    j = (80 * i) / 2447;
    ymd.day = i - (2447 * j) / 80;
    i = j / 11;
    ymd.month = j + 2 - 12 * i;
    ymd.year = 4 * k + n + i - 4716;
    if (jd < 0) {
      ymd.year -= 14136; // 4712 * 3 years
    }
    return ymd;
  }

  /**
   * Calculate the Julian day number of the date. The Proleptic Gregorian
   * Calendar is used.
   * <p>
   * Not valid for ymd &lt; -4713 12 25.
   *
   * @param ymd The date (year, month, day).
   * @return The Julian day number of the date, an integer running from noon
   * to noon.
   */
  public int cd2jdG(YMD ymd) {
    int year = ymd.year;
    int month = ymd.month;
    int day = ymd.day;
    return (1461 * (year + 4800 + (month - 14) / 12)) / 4 + (367 * (month - 2 - 12 * ((month - 14) / 12))) / 12
        - (3 * ((year + 4900 + (month - 14) / 12) / 100)) / 4 + day - 32075;
  }

  /**
   * Calculate the date from the Julian day number. The Proleptic Gregorian
   * Calendar is used.
   * <p>
   * Not valid for jd &lt; -0.5.
   *
   * @param jd The julian day number, an integer running from noon to noon.
   * @return The date (year, month, day).
   */
  public YMD jd2cdG(int jd) {
    YMD ymd = new YMD();
    int l = jd + 68569;
    int n = (4 * l) / 146097;
    l = l - (146097 * n + 3) / 4;
    int i = (4000 * (l + 1)) / 1461001;
    l = l - (1461 * i) / 4 + 31;
    int j = (80 * l) / 2447;
    ymd.day = l - (2447 * j) / 80;
    l = j / 11;
    ymd.month = j + 2 - 12 * l;
    ymd.year = 100 * (n - 49) + i + l;
    return ymd;
  }

  /**
   * Calculate the Julian day number of the date. The Julian and Proleptic
   * Julian Calendar is used for dates on or before 1582 Oct 4. The Gregorian
   * calendar is used for dates on or after 1582 Oct 15.
   *
   * @param ymd The date (year, month, day).
   * @return The Julian day number of the date, an integer running from noon
   * to noon.
   */
  public int cd2jdJG(YMD ymd) {
    int date = ymd.year * 10000 + ymd.month * 100 + ymd.day;
    if (date < JG_CHANGEOVER.GREGORIAN_START_DATE.getValue()) {
      return cd2jdJ(ymd);
    }
    return cd2jdG(ymd);
  }

  /**
   * Calculate the date from the Julian day number. The Julian and Proleptic
   * Julian Calendar is used for dates on or before 1582 Oct 10. The Gregorian
   * calendar is used for dates on or after 1582 Oct 15.
   *
   * @param jd The julian day number, an integer running from noon to noon.
   * @return The date (year, month, day).
   */
  public YMD jd2cdJG(int jd) {
    if (jd < JG_CHANGEOVER.GREGORIAN_START_DN.getValue()) {
      return jd2cdJ(jd);
    } else {
      return jd2cdG(jd);
    }
  }

  // ************ Time Conversions.

  /**
   * Convert the time to hours.
   *
   * @param hms The time as hours, minutes and seconds.
   * @return The time expressed as hours.
   */
  public double hms2h(HMS hms) {
    if (hms.hour < 0 || hms.minute < 0 || hms.second < 0) {
      return -((Math.abs(hms.second) / 60.0 + Math.abs(hms.minute)) / 60.0 + Math.abs(hms.hour));
    }
    return (hms.second / 60.0 + hms.minute) / 60.0 + hms.hour;
  }

  /**
   * Convert the hours to hours, minutes, seconds.
   *
   * @param hr The time expressed as hours.
   * @return The time (hour, minute, second).
   */
  public HMS h2hms(double hr) {
    HMS hms = new HMS();
    boolean sgn = hr < 0;
    double hd = Math.abs(hr);

    hms.hour = (int) Math.floor(hd);
    hd = (hd - hms.hour) * 60.0;
    hms.minute = (int) Math.floor(hd);
    hms.second = (hd - hms.minute) * 60.0;
    if (hms.second >= 60) {
      ++hms.minute;
      hms.second -= 60;
    }
    if (hms.minute >= 60) {
      ++hms.hour;
      hms.minute -= 60;
    }
    if (sgn) {
      if (hms.hour != 0) {
        hms.hour = -hms.hour;
        return hms;
      }
      if (hms.minute != 0) {
        hms.minute = -hms.minute;
        return hms;
      }
      if (hms.second != 0) {
        hms.second = -hms.second;
        return hms;
      }
    }
    return hms;
  }

  /**
   * Convert the time expressed as hours and minutes to hours.
   *
   * @param hm The time (hour, minute).
   * @return The time expressed as hours.
   */
  public double hm2h(HM hm) {
    if (hm.hour < 0 || hm.minute < 0) {
      return -(Math.abs(hm.minute) / 60.0 + Math.abs(hm.hour));
    }
    return hm.minute / 60.0 + hm.hour;
  }

  /**
   * Convert the hours to hours and minutes.
   *
   * @param hr The time expressed as hours.
   * @return The time (hour, minute).
   */
  public HM h2hm(double hr) {
    HM hm = new HM();
    boolean sgn = hr < 0;
    double hd = Math.abs(hr);

    hm.hour = (int) Math.floor(hd);
    hm.minute = (hd - hm.hour) * 60.0;
    if (hm.minute >= 60) {
      ++hm.hour;
      hm.minute -= 60;
    }
    if (sgn) {
      if (hm.hour != 0) {
        hm.hour = -hm.hour;
        return hm;
      }
      if (hm.minute != 0) {
        hm.minute = -hm.minute;
        return hm;
      }
    }
    return hm;
  }

  // ************ Day of the week or year.

  /**
   * Calculate the day of the week. This function assumes that the Proleptic
   * Julian Calendar and the Julian Calendar is used for dates on or before
   * 1582 Oct 4 and the Gregorian calendar is used for dates on or after 1582
   * Oct 15.
   *
   * @param jd       The Julian day number of the date, an integer running from
   *                 noon to noon.
   * @param firstDoW The first day of the week, either Sunday or Monday.
   * @return The day of the week, where 1=Sunday.
   */
  private int dow(int jd, FIRST_DOW firstDoW) {
    if (firstDoW == FIRST_DOW.SUNDAY) {
      if (jd < 0) {
        return 7 + ((jd - 5) % 7);
      } else {
        return (jd + 1) % 7 + 1;
      }
    } else {
      if (jd < 0) {
        return 7 + ((jd - 6) % 7);
      } else {
        return jd % 7 + 1;
      }
    }
  }

  /**
   * Calculate the day of the week.
   * <p>
   * You should use the Proleptic Julian Calendar and the Julian Calendar for
   * dates on or before 1582 Oct 4 and the Gregorian calendar for dates on or
   * after 1582 Oct 15.
   *
   * @param ymd      The date (year, month, day).
   * @param calendar The calendar to use.
   * @param firstDoW If true, Sunday = 1, otherwise Monday = 1.
   * @return The day of the week, where 1=Sunday.
   */
  public int dow(YMD ymd, CALENDAR_TYPE calendar, FIRST_DOW firstDoW) {
    int jd;
    switch (calendar) {
      case JULIAN:
        jd = cd2jdJ(ymd);
        break;
      case GREGORIAN:
        jd = cd2jdG(ymd);
        break;
      case JULIAN_GREGORIAN:
      default:
        jd = cd2jdJG(ymd);
    }
    return dow(jd, firstDoW);
  }

  /**
   * Calculate the day of the year. The Proleptic Julian Calendar is used.
   *
   * @param ymd The date.
   * @return The day of the year.
   */
  public int doyJ(YMD ymd) {
    YMD d0 = new YMD(ymd.year, 1, 1);
    int jd0 = cd2jdJ(d0);
    int jd1 = cd2jdJ(ymd);
    return jd1 - jd0 + 1;
  }

  /**
   * Calculate the day of the year. The Proleptic Gregorian Calendar is used.
   *
   * @param ymd The date.
   * @return The day of the year.
   */
  public int doyG(YMD ymd) {
    YMD d0 = new YMD(ymd.year, 1, 1);
    int jd0 = cd2jdG(d0);
    int jd1 = cd2jdG(ymd);
    return jd1 - jd0 + 1;
  }

  /**
   * Calculate the day of the year. The Proleptic Gregorian Calendar is used.
   *
   * @param ymd The date.
   * @return The day of the year.
   */
  public int doyJG(YMD ymd) {
    YMD d0 = new YMD(ymd.year, 1, 1);
    int jd0 = cd2jdJG(d0);
    int jd1 = cd2jdJG(ymd);
    return jd1 - jd0 + 1;
  }

  /**
   * Get the week of the year. See: http://en.wikipedia.org/wiki/ISO_week_date
   *
   * @param ymd The date.
   * @return iwd - The ISO week date. Note: This algorithm only works for
   * Gregorian dates.
   */
  public ISOWeekDate cd2woy(YMD ymd) {
    // For this algorithm: 1 = Monday ... 7 = Sunday.
    CALENDAR_TYPE calendar = CALENDAR_TYPE.JULIAN_GREGORIAN;
    int doy = doyG(ymd);
    ISOWeekDate isowd = new ISOWeekDate();
    isowd.weekDay = dow(ymd, calendar, FIRST_DOW.MONDAY);
    int wd = isowd.weekDay;
    YMD ymd0 = new YMD(ymd.year, 1, 4);
    int doy0 = doyG(ymd0);
    YMD ymd1 = new YMD(ymd.year, 12, 28);
    int doy1 = doyG(ymd1);
    int y = ymd.year, m = ymd.month, d = ymd.day;
    if (doy >= doy0 && doy <= doy1) {
      isowd.weekNumber = (doy - wd + 10) / 7;
      isowd.year = y;
      return isowd;
    }
    // Handle cases relating to the first or last week of the year.
    YMD tmp = new YMD(y - 1, 12, 31);
    wd = dow(tmp, calendar, FIRST_DOW.MONDAY);
    // If 31 December is on a Monday, Tuesday, or Wednesday,
    // it is in week 01 of the next year, otherwise in week 52 or 53.
    if (doy < doy0 && wd >= 4) {
      // Calculate the Monday of the first week of the year.
      d = d - isowd.weekDay + 1;
      if (d <= 0) {
        // December of the previous year.
        d = 31 + d;
        m = 12;
        y = y - 1;
      }
      tmp.SetYMD(y, m, d);
      doy = doyG(tmp);
      wd = dow(tmp, calendar, FIRST_DOW.MONDAY);
      isowd.weekNumber = (doy - wd + 10) / 7;
      isowd.year = y;
      return isowd;
    }
    // If 31 Dec is on a Monday Tuesday or Wednesday.
    tmp.SetYMD(y, 12, 31);
    wd = dow(tmp, calendar, FIRST_DOW.MONDAY);
    if (doy > doy1 && wd < 4) {
      if (isowd.weekDay > 3) {
        isowd.weekNumber = (doy - isowd.weekDay + 10) / 7;
        isowd.year = y;
        return isowd;
      } else {
        isowd.weekNumber = 1;
        isowd.year = y + 1;
        return isowd;
      }
    }
    isowd.weekNumber = (doy - wd + 10) / 7;
    isowd.year = y;
    return isowd;
  }

}
