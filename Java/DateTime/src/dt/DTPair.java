package dt;

/*=========================================================================

 Program:   Date Time Library
 File   :   Pair.java

 Copyright (c) Andrew J. P. Maclean
 All rights reserved.
 See Copyright.txt or the documentation for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notice for more information.

 =========================================================================*/

/**
 * A container to implement the equivalent of std::pair() in C++.
 * <p>
 * An implementation of equals is also provided.
 * <p>
 * Like in C++ you can directly access the first and second elements of the
 * pair. In the class, these are not marked as final so you can assign new
 * values to them.
 * <p>
 * See:
 * http://stackoverflow.com/questions/156275/what-is-the-equivalent-of-the-c
 * -pairl-r-in-java and
 * http://stackoverflow.com/questions/5303539/didnt-java-once-have-a-pair-class
 *
 * @author Andrew J. P. Maclean
 */
public class DTPair<T1, T2> {
  /**
   * The first element of the pair.
   */
  public T1 first;
  /**
   * The second element of the pair.
   */
  public T2 second;

  /**
   * Constructor for a pair.
   *
   * @param f The first member of the pair.
   * @param s The second member of the pair.
   */
  public DTPair(T1 f, T2 s) {
    this.first = f;
    this.second = s;
  }

// --Commented out by Inspection START (2017-07-26 16:48):
//    /**
//     * Convenience method for creating an appropriately typed pair.
//     *
//     * @param a the first object in the Pair
//     * @param b the second object in the pair
//     * @return A Pair that is templated with the types of a and b
//     */
//    public static <T1, T2> DTPair<T1, T2> create(T1 a, T2 b) {
//        return new DTPair<T1, T2>(a, b);
//    }
// --Commented out by Inspection STOP (2017-07-26 16:48)

  /**
   * Checks the two objects for equality.
   *
   * @param o the object to which this one is to be checked for equality
   * @return true if equal
   */
  @Override
  public boolean equals(Object o) {
    // Same object
    if (this == o) {
      return true;
    }
    // Check if the other object has the correct type.
    if (!(o instanceof DTPair)) {
      return false;
    }
    // Cast to the appropriate type.
    DTPair<?, ?> p = (DTPair<?, ?>) o;
    // Check the reference fields.
    return this.first.equals(p.first) && this.second.equals(p.second);
  }

  /**
   * Compute a hash code using the hash codes of the underlying objects
   *
   * @return a hashcode of the Pair
   */
  @Override
  public int hashCode() {
    return (first == null ? 0 : this.first.hashCode()) ^ (this.second == null ? 0 : this.second.hashCode());
  }

  /**
   * @return A string corresponding to the contents of the class.
   */
  @Override
  public String toString() {
    return this.first + ", " + this.second;
  }
}
