/*=========================================================================

  Program:   Date Time Library
  File   :   JGChangeover.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

package dt;

/*!
 The Julian/Gregorian calendar changeover dates.
 <ul>
 <li>In the Gregorian Calendar.</li>
 <ul>
 <li>Proleptic Gregorian previous day: Jd=2299150, 1582 10 04.</li>
 <li>Gregorian first day: Jd=2299161, 1582 10 15.</li>
 </ul>
 <li>In the Julian Calendar.</li>
 <ul>
 <li>Proleptic Gregorian previous day: Jd=2299160, 1582 10 04.</li>
 </ul>
 </ul>

 @author Andrew J. P. Maclean
 */
public enum JG_CHANGEOVER {
  /// @cond
  GREGORIAN_PREVIOUS_DN(2299150),
  GREGORIAN_PREVIOUS_DATE(15821004),
  GREGORIAN_START_DN(2299161),
  GREGORIAN_START_DATE(15821015),
  JULIAN_GREGORIAN_YEAR(1582),
  JULIAN_PREVIOUS_DN(2299160);
  /// @endcond

  /**
   * The changeover date
   */
  private final int changeOverDate;

  /**
   * Constructor
   *
   * @param newChangeOverDate The new value for the change over date.
   */
  JG_CHANGEOVER(final int newChangeOverDate) {
    this.changeOverDate = newChangeOverDate;
  }

  /**
   * Returns the changeover date.
   *
   * @return The value.
   */
  public int getValue() {
    return changeOverDate;
  }

}
