/*=========================================================================

  Program:   Date Time Library
  File   :   DateFormatter.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

package dt;

import java.util.HashMap;
import java.util.Locale;

/*
  The date Time Library
 */

/**
 * A convenience class for formatting dates and times.
 *
 * @author Andrew J. P. Maclean
 */
public class DateTimeFormatter {

  /**
   * The maximum allowable precision of the decimal part being displayed.
   * <p>
   * If the precision specified in the format statements exceeds this, then
   * this value is used, it is set to 15 decimal places by default.
   * <p>
   * The user can change this value.
   * <p>
   * For displaying seconds a maximum precision of 9 is sufficient for
   * nanosecond precision.
   * <p>
   * For displaying julian date times a precision of 15 will yield nanosecond
   * precision.
   */
  public int maximumPrecision = 15;

  /**
   * Return a map of the various parts of a date for display.
   *
   * @param date The date and time to format.
   * @param fmt  The formatting to use.
   * @return A map of strings whose keys are:
   * <ul>
   * <li>NamedDate - the formatted date with month and day names.</li>
   * <li>Date - the formatted date.</li>
   * <li>DoW - the day of the week.</li>
   * <li>DayName - the day name.</li>
   * <li>DayNameAbbrev - the abbreviated day name.</li>
   * <li>MonthName - the month name.</li>
   * <li>MonthNameAbbrev - the abbreviated month name.</li>
   * <li>DoY - the day of the year.</li>
   * <li>IWD - the ISO week.</li>
   * <li>JD - The Julian Day.</li>
   * <li>JD - The short form of the Julian Day.</li>
   * <li>Calendar - The calendar used.</li>
   * <li>LeapYear - Either Yes or No.</li>
   * </ul>
   * <p>
   * Note: ISO Week is only calculated if year &gt; 1582 in the
   * Gregorian calendar.
   */
  public HashMap<String, String> GetFormattedDates(JulianDateTime date, DateTimeFormat fmt) {
    HashMap<String, String> res = new HashMap<>();
    res.put("NamedDate", this.FormatDateWithNames(date, fmt));
    res.put("Date", this.FormatAsYMDHMS(date, fmt));
    int dow = date.GetDoW(fmt.calendar, fmt.firstDoW);
    res.put("DoW", String.format("%d", dow));
    DayNames dayNames = new DayNames();
    if (fmt.firstDoW == FIRST_DOW.SUNDAY) {
      // Sunday = 1
      res.put("DayName", dayNames.GetDayName(dow));
      res.put("DayNameAbbrev", dayNames.GetAbbreviatedDayName(dow, fmt.dayAbbreviationLength));
    } else {
      // Monday = 1
      // GetDayName and GetAbbreviatedDayName
      // treat Sunday as the first day of the week.
      res.put("DayName", dayNames.GetDayName((dow % 7) + 1));
      res.put("DayNameAbbrev", dayNames.GetAbbreviatedDayName((dow % 7) + 1, fmt.dayAbbreviationLength));
    }
    res.put("DoY", this.FormatDOY(date, fmt));
    YMD ymd = date.GetDate(fmt.calendar);
    MonthNames mn = new MonthNames();
    res.put("MonthName", mn.GetMonthName(ymd.month));
    res.put("MonthNameAbbrev", mn.GetAbbreviatedMonthName(ymd.month, fmt.monthAbbreviationLength));
    // ISO week dates are only valid for the Gregorian Calendar.
    if (ymd.year > JG_CHANGEOVER.JULIAN_GREGORIAN_YEAR.getValue() && fmt.calendar != CALENDAR_TYPE.JULIAN) {
      res.put("IWD", this.FormatISOWeek(date));
    } else {
      res.put("IWD", "");
    }
    int jdfp = fmt.precisionFoD;
    fmt.precisionFoD = this.maximumPrecision - 1;
    res.put("JD", this.FormatJDLong(date.GetJDFullPrecision(), fmt));
    fmt.precisionFoD = jdfp;
    res.put("JDShort", this.FormatJDShort(date.GetJDFullPrecision(), fmt));
    switch (fmt.calendar) {
      case JULIAN:
        res.put("Calendar", "Julian");
        break;
      case GREGORIAN:
        res.put("Calendar", "Gregorian");
        break;
      case JULIAN_GREGORIAN:
        res.put("Calendar", "Julian/Gregorian");
        break;
    }
    res.put("LeapYear", date.isLeapYear(date.GetDate(fmt.calendar).year, fmt.calendar) ? "Yes" : "No");
    return res;
  }

  /**
   * Clamp the number of decimal places to the range 0 ... maximumPrecision.
   *
   * @param precision The number of decimal places.
   * @return A positive value clamped to the range 0 ... maximumPrecision.
   */
  private int CheckPrecision(int precision) {
    return (Math.abs(precision) > Math.abs(maximumPrecision)) ? Math.abs(maximumPrecision) : Math.abs(precision);
  }

  /**
   * Scale a number by rounding it to a specified number of decimal places. If
   * the number is negative, ties are rounded to negative infinity.
   *
   * @param x         The number to round.
   * @param precision The number of decimal places to round to.
   * @return The rounded number.
   */
  private double Scale(double x, int precision) {
    boolean sgn = x < 0;
    double a = Math.abs(x);
    int prec = CheckPrecision(precision);
    double scale = 1;
    for (int i = 0; i < prec; ++i) {
      scale *= 10.0;
    }
    a = Math.round(a * scale) / scale;
    return (sgn) ? -a : a;
  }

  /**
   * Scale a number by rounding it to a specified number of decimal places. If
   * the number is negative, ties are rounded to negative infinity.
   *
   * @param x         The number to round.
   * @param precision The number of decimal places to round to.
   * @return The rounded number.
   */
  public double Scale(float x, int precision) {
    boolean sgn = x < 0;
    float a = Math.abs(x);
    int prec = CheckPrecision(precision);
    float scale = 1;
    for (int i = 0; i < prec; ++i) {
      scale *= 10.0;
    }
    a = Math.round(a * scale) / scale;
    return (sgn) ? -a : a;
  }

  /**
   * Format the date and time.
   *
   * @param date The date and time to format.
   * @param fmt  The formatting to use.
   * @return YYYY-MM-DD HH:MM::SS.ddd
   */
  public String FormatAsYMDHMS(JulianDateTime date, DateTimeFormat fmt) {
    return this.FormatAsYMDHMS(date, fmt.calendar, fmt.precisionSeconds, fmt.monthName, fmt.monthAbbreviationLength,
        fmt.dateSep, fmt.dateTimeSep, fmt.timeSep);
  }

  /**
   * Format the date as YYYY-MM-DD.
   *
   * @param date The date and time to format.
   * @param fmt  The formatting to use.
   * @return The formatted date.
   */
  public String FormatAsYMD(JulianDateTime date, DateTimeFormat fmt) {
    String[] res = FormatAsYMDHMS(date, fmt).split(String.valueOf(fmt.dateTimeSep));
    return res[0];
  }

  /**
   * Format the time as HH:MM::SS.ddd.
   *
   * @param date The date and time to format.
   * @param fmt  The formatting to use.
   * @return The formatted time.
   */
  public String FormatAsHMS(JulianDateTime date, DateTimeFormat fmt) {
    String[] res = FormatAsYMDHMS(date, fmt).split(String.valueOf(fmt.dateTimeSep));
    return res[1];
  }

  /**
   * Format the date and time as: DayName, DD MonthName Year HH:MM::SS.ddd.
   * <p>
   * If fmt.dayName == NONE and fmt.monthName == NONE then
   * <p>
   * YYYY-MM-DD HH:MM::SS.ddd is returned.
   *
   * @param date The date and time to format.
   * @param fmt  The formatting to use.
   * @return The formatted date.
   */
  public String FormatDateWithNames(JulianDateTime date, DateTimeFormat fmt) {
    return this.FormatDateWithNames(date, fmt.calendar, fmt.precisionSeconds, fmt.dayName,
        fmt.dayAbbreviationLength, fmt.monthName, fmt.monthAbbreviationLength, fmt.dateSep, fmt.dateTimeSep,
        fmt.timeSep);
  }

  /**
   * Format the long form of the Julian date.
   *
   * @param date The Julian date to format.
   * @param fmt  The formatting to use.
   * @return The Julian date as a formatted string.
   */
  public String FormatJDLong(JulianDateTime date, DateTimeFormat fmt) {
    return this.FormatJDLong(date.GetJDFullPrecision(), fmt.precisionFoD);
  }

  /**
   * Format the long form of the Julian date.
   *
   * @param date The Julian date to format.
   * @param fmt  The formatting to use.
   * @return The Julian date as a formatted string.
   */
  public String FormatJDLong(JD date, DateTimeFormat fmt) {
    return this.FormatJDLong(date, fmt.precisionFoD);
  }

  /**
   * Format the short form of the Julian date.
   *
   * @param date The date and time to format.
   * @param fmt  The formatting to use.
   * @return The Julian date as a formatted string.
   */
  public String FormatJDShort(JulianDateTime date, DateTimeFormat fmt) {
    return this.FormatJDShort(date.GetJDFullPrecision(), fmt.precisionFoD);
  }

  /**
   * Format the short form of the Julian date.
   *
   * @param date The date and time to format.
   * @param fmt  The formatting to use.
   * @return The Julian date as a formatted string.
   */
  private String FormatJDShort(JD date, DateTimeFormat fmt) {
    return this.FormatJDShort(date, fmt.precisionFoD);
  }

  /**
   * Format the time interval as days and fractions of a day.
   *
   * @param timeInterval The Time Interval to format.
   * @param fmt          The formatting to use.
   * @return The Time Interval as a formatted string.
   */
  public String FormatTimeInterval(TimeInterval timeInterval, DateTimeFormat fmt) {
    return this.FormatTimeInterval(timeInterval, fmt.precisionFoD);
  }

  /**
   * Format the time interval as days and hours, minutes and seconds.
   *
   * @param timeInterval The Time Interval to format.
   * @param fmt          The formatting to use.
   * @return The Time Interval as a formatted string.
   */
  public String FormatTimeIntervalDHMS(TimeInterval timeInterval, DateTimeFormat fmt) {
    return FormatTimeIntervalDHMS(timeInterval, fmt.precisionSeconds, fmt.dayTimeSep, fmt.timeSep);
  }

  /**
   * Format the day of the year.
   *
   * @param date The date and time to format.
   * @param fmt  The formatting to use.
   * @return The day of the year as a formatted string.
   */
  public String FormatDOY(JulianDateTime date, DateTimeFormat fmt) {
    return this.FormatDOY(date, fmt.calendar, fmt.precisionFoD);
  }

  /**
   * Format an ISO Week Date.
   *
   * @param dt The date and time to format.
   * @return The date formatted as YYYY-WW-D.
   */
  public String FormatISOWeek(JulianDateTime dt) {

    ISOWeekDate iwd = dt.GetISOWeek();
    String fmt = "%4d-%02d-%1d";
    return String.format(Locale.getDefault(), fmt, iwd.year, iwd.weekNumber, iwd.weekDay);
  }

  /**
   * Format the time zone as (+|-)HHMM.
   *
   * @param hm The timezone to format.
   * @return The timezone as a formatted string.
   */
  public String FormatTZAsHM(HM hm) {
    String sgn;
    if (hm.hour < 0 || hm.minute < 0) {
      sgn = "-";
    } else {
      sgn = "+";
    }
    String fmt = "%s%02d%02d";
    return String.format(fmt, sgn, Math.abs(hm.hour), (int) (Math.abs(hm.minute)));
  }

  /**
   * Format the date and time.
   *
   * @param date                    The date and time to format.
   * @param calendar                The calendar to use.
   * @param precisionSeconds        The precision of the seconds.
   * @param monthName               Specify whether the month name is required.
   * @param monthAbbreviationLength The size of the abbreviation for a month, usually 3.
   * @param dateSep                 The date separator, usually "-".
   * @param dateTimeSep             The date time separator, usually " ".
   * @param timeSep                 The time separator, usually ":".
   * @return YYYY-MM-DD HH:MM::SS.ddd
   */
  private String FormatAsYMDHMS(JulianDateTime date, CALENDAR_TYPE calendar, int precisionSeconds,
                                MONTH_NAME monthName, int monthAbbreviationLength, char dateSep, char dateTimeSep, char timeSep) {
    YMDHMS ymdhms = new YMDHMS(date.GetDateTime(calendar));

    int prec = this.CheckPrecision(precisionSeconds);

    // Normalise for display.
    YMD ymd = new YMD(ymdhms.ymd);
    HMS hms = new HMS(ymdhms.hms);
    hms.second = this.Scale(hms.second, prec);
    if (hms.second >= 60.0) {
      hms.minute += 1;
      hms.second -= 60;
    }
    if (hms.minute > 59) {
      hms.hour += 1;
      hms.minute -= 60;
    }
    if (hms.hour > 23) {
      hms.hour -= 24;
      JulianDateTime jdt = new JulianDateTime();
      jdt.SetDate(ymd, calendar);
      jdt.inc(1);
      YMD d = jdt.GetDateTime(calendar).GetYMD();
      ymd.SetYMD(d.year, d.month, d.day);
    }

    String dateStr;
    if (monthName != MONTH_NAME.NONE) {
      MonthNames months = new MonthNames();
      String mn;
      if (monthName == MONTH_NAME.UNABBREVIATED) {
        mn = months.GetMonthName(ymdhms.GetYMD().month);
      } else {
        mn = months.GetAbbreviatedMonthName(ymdhms.GetYMD().month, monthAbbreviationLength);
      }
      dateStr = String.format("%d%c%s%c%02d", ymd.year, dateSep, mn, dateSep, ymd.day);
    } else {
      dateStr = String.format("%d%c%02d%c%02d", ymd.year, dateSep, ymd.month, dateSep, ymd.day);
    }

    dateStr += dateTimeSep;

    if (prec > 0) {
      int width = prec + 3;
      String fmt = "%02d%c%02d%c%0" + width + "." + prec + "f";
      dateStr += String.format(fmt, hms.hour, timeSep, hms.minute, timeSep, hms.second);
    } else {
      dateStr += String.format("%02d%c%02d%c%02d", hms.hour, timeSep, hms.minute, timeSep,
          (int) (hms.second + 0.5));
    }

    return dateStr;
  }

  /**
   * Format the date and time as: DayName, DD MonthName Year HH:MM::SS.ddd.
   * <p>
   * If dayName == NONE and monthName == NONE then
   * <p>
   * YYYY-MM-DD HH:MM::SS.ddd is returned.
   *
   * @param date                    The date and time to format.
   * @param calendar                The calendar to use.
   * @param precisionSeconds        The precision of the seconds.
   * @param dayName                 Specifies whether the day name is required.
   * @param dayAbbreviationLength   The size of the abbreviation for a day, usually 3.
   * @param monthName               Specifies whether the month name is required.
   * @param monthAbbreviationLength The size of the abbreviation for a month, usually 3.
   * @param dateSep                 The date separator, usually "-".
   * @param dateTimeSep             The date time separator, usually " ".
   * @param timeSep                 The time separator, usually ":".
   * @return The formatted date.
   */
  private String FormatDateWithNames(JulianDateTime date, CALENDAR_TYPE calendar, int precisionSeconds,
                                     DAY_NAME dayName, int dayAbbreviationLength, MONTH_NAME monthName, int monthAbbreviationLength,
                                     char dateSep, char dateTimeSep, char timeSep) {

    if (dayName == DAY_NAME.NONE && monthName == MONTH_NAME.NONE) {
      return FormatAsYMDHMS(date, calendar, precisionSeconds, monthName, monthAbbreviationLength, dateSep,
          dateTimeSep, timeSep);
    }

    int prec = this.CheckPrecision(precisionSeconds);

    date.Normalise();

    YMDHMS ymdhms = date.GetDateTime(calendar);
    YMD ymd = ymdhms.GetYMD();
    HMS hms = ymdhms.GetHMS();

    MonthNames mn = new MonthNames();
    String month;
    if (monthName == MONTH_NAME.UNABBREVIATED) {
      month = mn.GetMonthName(ymd.month);
    } else {
      month = mn.GetAbbreviatedMonthName(ymd.month, monthAbbreviationLength);
    }

    DayNames dn = new DayNames();
    String day;
    if (dayName == DAY_NAME.UNABBREVIATED) {
      day = dn.GetDayName(date.GetDoW(calendar, FIRST_DOW.SUNDAY));
    } else {
      day = dn.GetAbbreviatedDayName(date.GetDoW(calendar, FIRST_DOW.SUNDAY), dayAbbreviationLength);
    }
    String fmt = "%s, %02d %s %d %02d:%02d:%02." + prec + "f";
    return String.format(Locale.getDefault(), fmt, day, ymd.day, month, ymd.year, hms.hour, hms.minute,
        hms.second);
  }

  /**
   * Format the long form of the Julian date.
   *
   * @param date      The Julian date to format.
   * @param precision The precision of the fraction of the day.
   * @return The Julian date as a formatted string.
   */
  private String FormatJDLong(JD date, int precision) {

    int prec = this.CheckPrecision(precision);
    date.Normalise();

    // Normalise for display.
    double rounding = 0.5;
    if (prec == 0) {
      double tmp = date.GetJDDouble();
      if (tmp >= 0) {
        tmp += (rounding + 0.01);
      } else {
        tmp -= (rounding + 0.01);
      }
      return String.format("%d", (long) tmp);
    }

    int jdn = date.GetJDN();
    double fod = date.GetFoD();

    boolean isNegative = false;
    if (jdn < 0) {
      isNegative = true;
      if (fod > 0.5) {
        ++jdn;
      }
      if (fod != 0) {
        fod = 1.0 - fod;
      }
    }

    if (fod != 0) {
      for (int i = 0; i < prec; ++i) {
        fod *= 10.0;
      }
      fod = Math.floor(fod + rounding);
      for (int i = 0; i < prec; ++i) {
        fod /= 10.0;
      }
      while (fod >= 1) {
        fod -= 1;
        jdn += 1;
      }
    }

    String fmt = "%1." + prec + "f";
    String[] res = String.format(Locale.US, fmt, fod).split("\\.");

    String s0;
    if (isNegative) {
      s0 = "-";
    } else {
      s0 = "";
    }

    if (res.length > 1) {
      if (fod >= 0.5 && jdn < 0) {
        return String.format(Locale.getDefault(), "%s%d.%s", s0, Math.abs(jdn + 1), res[1]);
      } else {
        return String.format(Locale.getDefault(), "%s%d.%s", s0, Math.abs(jdn), res[1]);
      }
    } else {
      return String.format(Locale.US, fmt, fod);
    }
  }

  /**
   * Format the short form of the Julian date.
   *
   * @param date      The Julian date to format.
   * @param precision The precision of the julian date.
   * @return The Julian date as a formatted string.
   */
  private String FormatJDShort(JD date, int precision) {
    int prec = this.CheckPrecision(precision);

    String fmt = "%." + prec + "f";
    return String.format(Locale.getDefault(), fmt, date.GetJDDouble());
  }

  /**
   * Format the time interval as days and fractions of a day.
   *
   * @param timeInterval The Time Interval to format.
   * @param precision    The precision of the fraction of the day.
   * @return The Time Interval as a formatted string.
   */
  private String FormatTimeInterval(TimeInterval timeInterval, int precision) {

    int prec = this.CheckPrecision(precision);

    timeInterval.Normalise();

    // Normalise for display.
    double rounding = 0.5;
    if (prec == 0) {
      double tmp = timeInterval.GetTimeIntervalDouble();
      if (tmp >= 0) {
        tmp += (rounding + 0.01);
      } else {
        tmp -= (rounding + 0.01);
      }
      return String.format("%d", (long) tmp);
    }

    int deltaDay = timeInterval.GetDeltaDay();
    double deltaFoD = timeInterval.GetDeltaFoD();

    boolean isNegative = false;
    if (deltaDay < 0) {
      isNegative = true;
    } else {
      if (deltaFoD < 0) {
        isNegative = true;
      }
    }
    deltaDay = Math.abs(deltaDay);
    deltaFoD = Math.abs(deltaFoD);

    if (deltaFoD != 0) {
      for (int i = 0; i < prec; ++i) {
        deltaFoD *= 10;
      }
      deltaFoD = Math.floor(deltaFoD + rounding);
      for (int i = 0; i < prec; ++i) {
        deltaFoD /= 10;
      }
      while (deltaFoD >= 1) {
        deltaFoD -= 1;
        deltaDay += 1;
      }
    }

    String s0;
    if (isNegative) {
      s0 = "-";
    } else {
      s0 = "";
    }

    String fmt = "%1." + prec + "f";
    String[] str = String.format(Locale.US, fmt, deltaFoD).split("\\.");
    String s1 = String.format(Locale.getDefault(), "%#.0f%s", (double) (deltaDay + Integer.parseInt(str[0])),
        str[1]);
    return s0 + s1;
  }

  /**
   * Format the time interval as days and hours, minutes and seconds.
   *
   * @param timeInterval The Time Interval to format.
   * @param precision    The precision of the seconds.
   * @param dayTimeSep   The separator between the day and time.
   * @param timeSep      The time separator.
   * @return The Time Interval as a formatted string.
   */
  private String FormatTimeIntervalDHMS(TimeInterval timeInterval, int precision, char dayTimeSep, char timeSep) {

    int prec = this.CheckPrecision(precision);

    timeInterval.Normalise();

    int deltaDay = timeInterval.GetDeltaDay();
    double deltaFoD = timeInterval.GetDeltaFoD();

    boolean isNegative = false;
    if (deltaDay < 0) {
      isNegative = true;
    } else {
      if (deltaFoD < 0) {
        isNegative = true;
      }
    }
    deltaDay = Math.abs(deltaDay);
    deltaFoD = Math.abs(deltaFoD);

    DateTimeConversions dtc = new DateTimeConversions();
    HMS hms = dtc.h2hms(deltaFoD * 24.0);

    String s0;
    if (isNegative) {
      s0 = String.format("-%dd%s-", deltaDay, dayTimeSep);
    } else {
      s0 = String.format("%dd%s ", deltaDay, dayTimeSep);
    }
    String ret = s0;
    if (prec > 0) {
      int width = prec + 3;
      String fmt = "%02d%c%02d%c%0" + width + "." + prec + "f";
      ret = ret + String.format(fmt, hms.hour, timeSep, hms.minute, timeSep, hms.second);
    } else {
      ret = ret + String.format("%02d%c%02d%c%02d", hms.hour, timeSep, hms.minute, timeSep,
          (int) Math.ceil(hms.second));
    }

    return ret;
  }

  /**
   * Format the day of the year.
   *
   * @param date      The date and time to format.
   * @param calendar  The calendar to use.
   * @param precision The precision of the fraction of the day.
   * @return The day of the year as a formatted string.
   */
  private String FormatDOY(JulianDateTime date, CALENDAR_TYPE calendar, int precision) {

    int doy = date.GetDoY(calendar);
    double fod = date.GetFoDMidnight();
    TimeInterval ti = new TimeInterval(doy, fod);

    return this.FormatTimeInterval(ti, precision);
  }
}
