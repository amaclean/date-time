/*=========================================================================

  Program:   Date Time Library
  File   :   ISOWeekDate.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/*

 */
package dt;

/**
 * This class holds the weekday, week number and year corresponding to the ISO
 * week number definition.
 *
 * @author Andrew J. P. Maclean
 */
public class ISOWeekDate {

  /**
   * Year
   */
  public int year;
  /**
   * The week number.
   */
  public int weekNumber;
  /**
   * The day of the week.
   */
  public int weekDay;

  // Assignment

  public ISOWeekDate() {
    this.year = 0;
    this.weekNumber = 0;
    this.weekDay = 0;
  }

  /**
   * @param year       Year
   * @param weekNumber The week number.
   * @param weekDay    The day of the week
   */
  public ISOWeekDate(int year, int weekNumber, int weekDay) {
    this.year = year;
    this.weekNumber = weekNumber;
    this.weekDay = weekDay;
  }

  /**
   * @param iwd The ISO week date.
   */
  public ISOWeekDate(ISOWeekDate iwd) {
    this.year = iwd.year;
    this.weekNumber = iwd.weekNumber;
    this.weekDay = iwd.weekDay;
  }

  /**
   * assignment
   *
   * @param rhs A ISOWeekDate object.
   */
  public void assign(ISOWeekDate rhs) {
    this.year = rhs.year;
    this.weekNumber = rhs.weekNumber;
    this.weekDay = rhs.weekDay;
  }

  /**
   * @param iwd The ISO week date.
   */
  public void SetISOWeekDate(ISOWeekDate iwd) {
    this.year = iwd.year;
    this.weekNumber = iwd.weekNumber;
    this.weekDay = iwd.weekDay;
  }

  /**
   * Set the ISO week date.
   *
   * @param year       Year
   * @param weekNumber The week number.
   * @param weekDay    The day of the week
   */
  public void SetISOWeekDate(int year, int weekNumber, int weekDay) {
    this.year = year;
    this.weekNumber = weekNumber;
    this.weekDay = weekDay;
  }

  /**
   * Get the ISO week date.
   *
   * @return The ISO week date.
   */
  public ISOWeekDate GetISOWeekDate() {
    return this;
  }

  /**
   * Checks the two objects for equality.
   *
   * @param o the object to which this one is to be checked for equality
   * @return true if equal
   */
  @Override
  public boolean equals(Object o) {
    // Same object
    if (this == o) {
      return true;
    }
    // Check if the other object has the correct type.
    if (!(o instanceof ISOWeekDate)) {
      return false;
    }
    // Cast to the appropriate type.
    ISOWeekDate p = (ISOWeekDate) o;
    // Check each field.
    return this.year == p.year && this.weekNumber == p.weekNumber && this.weekDay == p.weekDay;
  }

  /**
   * @param rhs The ISO week date.
   * @return true if this == rhs, false otherwise
   */
  public boolean eq(ISOWeekDate rhs) {
    return this.equals(rhs);
  }

  /**
   * @param rhs The ISO week date.
   * @return true if this != rhs, false otherwise
   */
  public boolean ne(ISOWeekDate rhs) {
    return !this.eq(rhs);
  }

  /**
   * @param rhs The ISO week date.
   * @return true if this &lt; rhs
   */
  public boolean lt(ISOWeekDate rhs) {
    if (this.year < rhs.year) {
      return true;
    }
    if (this.year == rhs.year) {
      if (this.weekNumber < rhs.weekNumber) {
        return true;
      }
      if (this.weekNumber == rhs.weekNumber) {
        return this.weekDay < rhs.weekDay;
      }
    }
    return false;
  }

  /**
   * @param rhs The ISO week date.
   * @return true if this &le; rhs
   */
  public boolean le(ISOWeekDate rhs) {
    return !(this.gt(rhs));
  }

  /**
   * @param rhs The ISO week date.
   * @return true if this &gt; rhs
   */
  public boolean gt(ISOWeekDate rhs) {
    if (this.year > rhs.year) {
      return true;
    }
    if (this.year == rhs.year) {
      if (this.weekNumber > rhs.weekNumber) {
        return true;
      }
      if (this.weekNumber == rhs.weekNumber) {
        return this.weekDay > rhs.weekDay;
      }
    }
    return false;
  }

  /**
   * @param rhs The ISO week date.
   * @return true if this &ge; rhs
   */
  public boolean ge(ISOWeekDate rhs) {
    return !(this.lt(rhs));
  }

  /**
   * @return The ISO week date as a string.
   */
  public String GetString() {
    return String.format("%04d-W%02d-%01d", this.year, this.weekNumber, this.weekDay);
  }

  /**
   * @return A string corresponding to the contents of the class.
   */
  @Override
  public String toString() {
    return String.format("%02d-W%02d-%01d", this.year, this.weekNumber, this.weekDay);
  }

}
