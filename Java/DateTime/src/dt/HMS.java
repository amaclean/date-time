/*=========================================================================

  Program:   Date Time Library
  File   :   HMS.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
package dt;

/**
 * This class holds a time as hours, minutes and seconds.
 *
 * @author Andrew J. P. Maclean
 */
public class HMS extends DateTimeBase {

  /**
   * Hour
   */
  public int hour;
  /**
   * Minute
   */
  public int minute;
  /**
   * Second
   */
  public double second;

  public HMS() {
    this.hour = 0;
    this.minute = 0;
    this.second = 0;
  }

  /**
   * @param hour   Hour
   * @param minute Minute
   * @param second Second
   */
  public HMS(int hour, int minute, double second) {
    this.hour = hour;
    this.minute = minute;
    this.second = second;
  }

  /**
   * @param hms The time.
   */
  public HMS(HMS hms) {
    this.hour = hms.hour;
    this.minute = hms.minute;
    this.second = hms.second;
  }

  // Assignment

  /**
   * Set the time.
   *
   * @param hms The time.
   */
  public void SetHMS(HMS hms) {
    this.hour = hms.hour;
    this.minute = hms.minute;
    this.second = hms.second;
  }

  /**
   * Set the hours, minutes and seconds.
   *
   * @param hour   Hour
   * @param minute Minute
   * @param second Second
   */
  public void SetHMS(int hour, int minute, double second) {
    this.hour = hour;
    this.minute = minute;
    this.second = second;
  }

  /**
   * Get the time.
   *
   * @return The time.
   */
  public HMS GetHMS() {
    return this;
  }

  /**
   * Checks the two objects for equality.
   *
   * @param o the object to which this one is to be checked for equality
   * @return true if equal
   */
  @Override
  public boolean equals(Object o) {
    // Same object
    if (this == o) {
      return true;
    }
    // Check if the other object has the correct type.
    if (!(o instanceof HMS)) {
      return false;
    }
    // Cast to the appropriate type.
    HMS p = (HMS) o;
    // Check each field.
    return this.hour == p.hour && this.minute == p.minute && this.second == p.second;
  }

  /**
   * @param rhs The time.
   * @return true if this == rhs, false otherwise
   */
  public boolean eq(HMS rhs) {
    return this.equals(rhs);
  }

  /**
   * @param rhs The time.
   * @return true if this != rhs, false otherwise
   */
  public boolean ne(HMS rhs) {
    return !this.equals(rhs);
  }

  /**
   * @param rhs The time.
   * @return true if this &lt; rhs
   */
  public boolean lt(HMS rhs) {
    if (this.hour < rhs.hour) {
      return true;
    }
    if (this.hour == rhs.hour) {
      if (this.minute < rhs.minute) {
        return true;
      }
      if (this.minute == rhs.minute) {
          return this.second < rhs.second;
      }
    }
    return false;
  }

  /**
   * @param rhs The time.
   * @return true if this &le; rhs
   */
  public boolean le(HMS rhs) {
    return !(this.gt(rhs));
  }

  /**
   * @param rhs The time.
   * @return true if this &gt; rhs
   */
  public boolean gt(HMS rhs) {
    if (this.hour > rhs.hour) {
      return true;
    }
    if (this.hour == rhs.hour) {
      if (this.minute > rhs.minute) {
        return true;
      }
      if (this.minute == rhs.minute) {
          return this.second > rhs.second;
      }
    }
    return false;
  }

  /**
   * @param rhs The time.
   * @return true if this &ge; rhs
   */
  public boolean ge(HMS rhs) {
    return !(this.lt(rhs));
  }

  /**
   * Compare hours minutes and seconds for equality.
   *
   * @param hms     The time to be compared with this.
   * @param rel_tol The precision.
   * @param abs_tol The precision.
   * @return true if the times are equivalent.
   */
  public boolean IsClose(HMS hms, double rel_tol, double abs_tol) {
    // Same object
    return this == hms || this.hour == hms.hour && this.minute == hms.minute && isclose(this.second, hms.second, rel_tol, abs_tol);
  }

  /**
   * Compare hours minutes and seconds for equality.
   * <p>
   * Assumes an absolute tolerance of 0.0.
   *
   * @param hms     The time to be compared with this.
   * @param rel_tol The precision.
   * @return true if the times are equivalent.
   */
  public boolean IsClose(HMS hms, double rel_tol) {
    // Same object
    return this == hms || this.hour == hms.hour && this.minute == hms.minute && isclose(this.second, hms.second, rel_tol);
  }

  /**
   * Compare hours minutes and seconds for equality.
   * <p>
   * Assumes a relative tolerance of 1.0e-09 and an absolute tolerance of 0.0.
   *
   * @param hms The time to be compared with this.
   * @return true if the times are equivalent.
   */
  public boolean IsClose(HMS hms) {
    // Same object
    return this == hms || this.hour == hms.hour && this.minute == hms.minute && isclose(this.second, hms.second);
  }

  /**
   * @return The time as a string.
   */
  public String GetString() {
    boolean isNegative = false;
    if (this.hour < 0) {
      isNegative = true;
    } else {
      if (this.minute < 0) {
        isNegative = true;
      } else {
        if (this.second < 0) {
          isNegative = true;
        }
      }
    }
    String s;
    if (isNegative) {
      s = "-";
    } else {
      s = " ";
    }
    s += String.format("%02d:%02d:%015.12f", Math.abs(this.hour), Math.abs(this.minute), Math.abs(this.second));
    return s;
  }

  /**
   * @return A string corresponding to the contents of the class.
   */
  @Override
  public String toString() {
    return String.format("%02d:%02d:%015.12f", this.hour, this.minute, this.second);
  }
}
