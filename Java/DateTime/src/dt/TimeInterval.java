/*=========================================================================

  Program:   Date Time Library
  File   :   TimeInterval.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
package dt;

/**
 * This class holds a time interval as days and fraction of the day.
 *
 * @author Andrew J. P. Maclean
 */

public class TimeInterval extends DateTimeBase {
  /**
   * The days.
   */
  private int deltaDay;
  /**
   * The fraction of the day.
   */
  private double deltaFoD;

  public TimeInterval() {
    this.deltaDay = 0;
    this.deltaFoD = 0;
  }

  /**
   * @param dt The time interval
   */
  public TimeInterval(TimeInterval dt) {
    this.deltaDay = dt.deltaDay;
    this.deltaFoD = dt.deltaFoD;
    this.Normalise();
  }

  /**
   * @param deltaDay The days.
   * @param deltaFoD The Fraction of the Day.
   */
  public TimeInterval(int deltaDay, double deltaFoD) {
    this.deltaDay = deltaDay;
    this.deltaFoD = deltaFoD;
    this.Normalise();
  }

  /**
   * @param dt The time interval
   */
  public void SetTimeInterval(TimeInterval dt) {
    this.deltaDay = dt.deltaDay;
    this.deltaFoD = dt.deltaFoD;
    this.Normalise();
  }

  /**
   * @param deltaDay The days.
   * @param deltaFoD The Fraction of the Day.
   */
  public void SetTimeInterval(int deltaDay, double deltaFoD) {
    this.deltaDay = deltaDay;
    this.deltaFoD = deltaFoD;
    this.Normalise();
  }

  /**
   * assignment
   *
   * @param rhs The time interval.
   */
  public void assign(TimeInterval rhs) {
    this.deltaDay = rhs.deltaDay;
    this.deltaFoD = rhs.deltaFoD;
  }

  /**
   * Get the time interval.
   *
   * @return The time interval
   */
  public TimeInterval GetTimeInterval() {
    return this;
  }

  /**
   * Get the time interval as days and decimals of the day.
   * <p>
   * The precision of the returned value will be of the order of 1.0e-8 of a
   * day or 1ms.
   *
   * @return The time interval as days and decimals of the day.
   */
  public double GetTimeIntervalDouble() {
    return this.deltaDay + this.deltaFoD;
  }

  /**
   * Checks the two objects for equality.
   *
   * @param o the object to which this one is to be checked for equality
   * @return true if equal
   */
  @Override
  public boolean equals(Object o) {
    // Same object
    if (this == o) {
      return true;
    }
    // Check if the other object has the correct type.
    if (!(o instanceof TimeInterval)) {
      return false;
    }
    // Cast to the appropriate type.
    TimeInterval p = (TimeInterval) o;
    // Check each field.
    return this.deltaDay == p.deltaDay && this.deltaFoD == p.deltaFoD;
  }

  /**
   * @param rhs The date.
   * @return true if this == rhs, false otherwise
   */
  public boolean eq(TimeInterval rhs) {
    return this.equals(rhs);
  }

  /**
   * @param rhs The date.
   * @return true if this != rhs, false otherwise
   */
  public boolean ne(TimeInterval rhs) {
    return !this.equals(rhs);
  }

  /**
   * @param rhs A TimeInterval object.
   * @return true if this &lt; rhs
   */
  public boolean lt(TimeInterval rhs) {
    return this.deltaDay < rhs.deltaDay || (this.deltaDay == rhs.deltaDay) && (this.deltaFoD < rhs.deltaFoD);
  }

  /**
   * @param rhs A TimeInterval object.
   * @return true if this &le; rhs
   */
  public boolean le(TimeInterval rhs) {
    return !(this.gt(rhs));
  }

  /**
   * @param rhs A TimeInterval object.
   * @return true if this &gt; rhs
   */
  public boolean gt(TimeInterval rhs) {
    return this.deltaDay > rhs.deltaDay || (this.deltaDay == rhs.deltaDay) && (this.deltaFoD > rhs.deltaFoD);
  }

  // Addition/Subtraction

  /**
   * @param rhs A TimeInterval object.
   * @return true if this &ge; rhs
   */
  public boolean ge(TimeInterval rhs) {
    return !(this.lt(rhs));
  }

  /**
   * Compare time intervals for equality.
   *
   * @param ti      The time interval to be compared with this.
   * @param rel_tol The precision.
   * @param abs_tol The precision.
   * @return true if the time intervals are equivalent.
   */
  public boolean IsClose(TimeInterval ti, double rel_tol, double abs_tol) {
    // Same object
    if (this == ti) {
      return true;
    } else {
        return this.GetDeltaDay() == ti.GetDeltaDay()
            && isclose(this.GetDeltaFoD(), ti.GetDeltaFoD(), rel_tol, abs_tol);
    }
  }

  /**
   * Compare time intervals for equality.
   * <p>
   * Assumes an absolute tolerance of 0.0.
   *
   * @param ti      The time interval to be compared with this.
   * @param rel_tol The precision.
   * @return true if the time intervals are equivalent.
   */
  public boolean IsClose(TimeInterval ti, double rel_tol) {
    // Same object
    if (this == ti) {
      return true;
    } else {
        return this.GetDeltaDay() == ti.GetDeltaDay()
            && isclose(this.GetDeltaFoD(), ti.GetDeltaFoD(), rel_tol);
    }
  }

  /**
   * Compare time intervals for equality.
   * <p>
   * Assumes a relative tolerance of 1.0e-09 and an absolute tolerance of 0.0.
   *
   * @param ti The time interval to be compared with this.
   * @return true if the time intervals are equivalent.
   */
  public boolean IsClose(TimeInterval ti) {
    // Same object
    if (this == ti) {
      return true;
    } else {
        return this.GetDeltaDay() == ti.GetDeltaDay()
            && isclose(this.GetDeltaFoD(), ti.GetDeltaFoD());
    }
  }

  /**
   * +
   *
   * @param rhs A TimeInterval object.
   * @return this + rhs
   */
  public TimeInterval add(TimeInterval rhs) {
    TimeInterval result = new TimeInterval();
    result.deltaDay = this.deltaDay + rhs.deltaDay;
    result.deltaFoD = this.deltaFoD + rhs.deltaFoD;
    result.Normalise();

    return result;
  }

  /**
   * +
   *
   * @param days Days
   * @return this + days
   */
  public TimeInterval add(double days) {
    TimeInterval result = new TimeInterval();
    result.deltaDay = this.deltaDay + (int) days;
    result.deltaFoD = this.deltaFoD + (days - (int) days);
    result.Normalise();

    return result;
  }

  /**
   * += Increment the time interval by a TimeInterval value.
   *
   * @param rhs A TimeInterval object.
   */
  public void inc(TimeInterval rhs) {
    this.deltaDay += rhs.deltaDay;
    this.deltaFoD += rhs.deltaFoD;
    this.Normalise();
  }

  /**
   * += Increment the time interval by the day value.
   *
   * @param days Days
   */
  public void inc(double days) {
    this.deltaDay += (int) (days);
    this.deltaFoD += (days - (int) (days));
    Normalise();
  }

  /**
   * -
   *
   * @param rhs A TimeInterval object.
   * @return this - rhs
   */
  public TimeInterval sub(TimeInterval rhs) {
    TimeInterval result = new TimeInterval();
    result.deltaDay = this.deltaDay - rhs.deltaDay;
    result.deltaFoD = this.deltaFoD - rhs.deltaFoD;
    result.Normalise();

    return result;
  }

  /**
   * -
   *
   * @param days Days
   * @return this + days
   */
  public TimeInterval sub(double days) {
    TimeInterval result = new TimeInterval();
    result.deltaDay = this.deltaDay - (int) (days);
    result.deltaFoD = this.deltaFoD - (days - (int) (days));
    result.Normalise();

    return result;
  }

  /**
   * -= Decrement the time interval by a TimeInterval value.
   *
   * @param rhs A TimeInterval object.
   */
  public void dec(TimeInterval rhs) {
    this.deltaDay -= rhs.deltaDay;
    this.deltaFoD -= rhs.deltaFoD;
    Normalise();
  }

  /**
   * -= Decrement the time interval by the day value.
   *
   * @param days Days
   */
  public void dec(double days) {
    this.deltaDay -= (int) (days);
    this.deltaFoD -= (days - (int) (days));
    Normalise();
  }

  /**
   * @return The time interval as a string.
   */
  public String GetString() {

    boolean isNegative = false;
    if (this.deltaDay < 0) {
      isNegative = true;
    } else {
      if (this.deltaFoD < 0) {
        isNegative = true;
      }
    }
    String s;
    if (isNegative) {
      s = "-";
    } else {
      s = " ";
    }

    String fr = String.format("%019.15f", Math.abs(this.deltaFoD));
    int idx = fr.indexOf('.');
    fr = fr.substring(idx);
    s += String.format("%d", Math.abs(this.deltaDay));
    s += fr;
    return s;
  }

  /**
   * Get the time difference days.
   *
   * @return Fraction of the Day
   */
  public int GetDeltaDay() {
    return this.deltaDay;
  }

  /**
   * Get the time difference fraction of the day.
   *
   * @return Fraction of the Day
   */
  public double GetDeltaFoD() {
    return this.deltaFoD;
  }

  /**
   * Set the time difference days.
   *
   * @param deltaDay The difference in days.
   */
  public void SetDeltaDay(int deltaDay) {
    this.deltaDay = deltaDay;
    this.Normalise();
  }

  /**
   * Set the time difference fraction of the day.
   *
   * @param deltaFoD The difference in fractions of a day.
   */
  public void SetDeltaFoD(double deltaFoD) {
    this.deltaFoD = deltaFoD;
    this.Normalise();
  }

  /**
   * Normalise so that the fraction of the day is &gt; -1 and &lt; 1.
   */
  protected void Normalise() {
    while (this.deltaFoD >= 1) {
      this.deltaFoD -= 1;
      this.deltaDay += 1;
    }
    while (this.deltaFoD <= -1) {
      this.deltaFoD += 1;
      this.deltaDay -= 1;
    }
    // Normalise so that the sign is the same for deltaDay and deltaFoD.
    int tmp = (int) (this.deltaDay + this.deltaFoD);
    this.deltaFoD = this.deltaDay - tmp + this.deltaFoD;
    this.deltaDay = tmp;
  }

  /**
   * @return A string corresponding to the contents of the class.
   */
  @Override
  public String toString() {
    return String.format("%d, %19.15f", this.deltaDay, this.deltaFoD);
  }

}
