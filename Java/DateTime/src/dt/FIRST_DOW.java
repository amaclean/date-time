/*=========================================================================

  Program:   Date Time Library
  File   :   FIRST_DOW.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

package dt;

/**
 * Enumerate whether to use Sunday or Monday as the first day of the week.
 *
 * @author Andrew J. P. Maclean
 */
public enum FIRST_DOW {
  /// @cond
  SUNDAY, MONDAY
  /// @endcond
}
