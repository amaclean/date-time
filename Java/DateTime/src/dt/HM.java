/*=========================================================================

  Program:   Date Time Library
  File   :   HM.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/*

 */
package dt;

/**
 * This class holds a time as hours and minutes.
 *
 * @author Andrew J. P. Maclean
 */
public class HM extends DateTimeBase {

  /**
   * Hour
   */
  public int hour;
  /**
   * Minute
   */
  public double minute;

  public HM() {
    this.hour = 0;
    this.minute = 0;
  }

  /**
   * @param d      Hour
   * @param minute Minute
   */
  public HM(int d, double minute) {
    this.hour = d;
    this.minute = minute;
  }

  /**
   * @param hm The time.
   */
  public HM(HM hm) {
    this.hour = hm.hour;
    this.minute = hm.minute;
  }

  /**
   * Set the time.
   *
   * @param hm The time.
   */
  public void SetHM(HM hm) {
    this.hour = hm.hour;
    this.minute = hm.minute;
  }

  // Assignment

  /**
   * Set the hours and minutes.
   *
   * @param hour   Hour
   * @param minute Minute
   */
  public void SetHM(int hour, double minute) {
    this.hour = hour;
    this.minute = minute;
  }

  /**
   * Get the time.
   *
   * @return The time.
   */
  public HM GetHM() {
    return this;
  }

  /**
   * Checks the two objects for equality.
   *
   * @param o the object to which this one is to be checked for equality
   * @return true if equal
   */
  @Override
  public boolean equals(Object o) {
    // Same object
    if (this == o) {
      return true;
    }
    // Check if the other object has the correct type.
    if (!(o instanceof HM)) {
      return false;
    }
    // Cast to the appropriate type.
    HM p = (HM) o;
    // Check each field.
    return this.hour == p.hour && this.minute == p.minute;
  }

  /**
   * @param rhs The time.
   * @return true if this == rhs, false otherwise
   */
  public boolean eq(HM rhs) {
    return this.equals(rhs);
  }

  /**
   * @param rhs The time.
   * @return true if this != rhs, false otherwise
   */
  public boolean ne(HM rhs) {
    return !this.equals(rhs);
  }

  /**
   * @param rhs The time.
   * @return true if this &lt; rhs
   */
  public boolean lt(HM rhs) {
    if (this.hour < rhs.hour) {
      return true;
    }
    if (this.hour == rhs.hour) {
      return this.minute < rhs.minute;
    }
    return false;
  }

  /**
   * @param rhs The time.
   * @return true if this &le; rhs
   */
  public boolean le(HM rhs) {
    return !(this.gt(rhs));
  }

  /**
   * @param rhs The time.
   * @return true if this &gt; rhs
   */
  public boolean gt(HM rhs) {
    if (this.hour > rhs.hour) {
      return true;
    }
    if (this.hour == rhs.hour) {
      return this.minute > rhs.minute;
    }
    return false;
  }

  /**
   * @param rhs The time.
   * @return true if this &ge; rhs
   */
  public boolean ge(HM rhs) {
    return !(this.lt(rhs));
  }

  /**
   * Compare hours and minutes for equality.
   *
   * @param hm      The time to be compared with this.
   * @param rel_tol The precision.
   * @param abs_tol The precision.
   * @return true if the times are equivalent.
   */
  public boolean IsClose(HM hm, double rel_tol, double abs_tol) {
    // Same object
    return this == hm || this.hour == hm.hour && isclose(this.minute, hm.minute, rel_tol, abs_tol);
  }

  /**
   * Compare hours and minutes for equality.
   * <p>
   * Assumes an absolute tolerance of 0.0.
   *
   * @param hm      The time to be compared with this.
   * @param rel_tol The precision.
   * @return true if the times are equivalent.
   */
  public boolean IsClose(HM hm, double rel_tol) {
    // Same object
    return this == hm || this.hour == hm.hour && isclose(this.minute, hm.minute, rel_tol);
  }

  /**
   * Compare hours and minutes for equality.
   * <p>
   * Assumes a relative tolerance of 1.0e-09 and an absolute tolerance of 0.0.
   *
   * @param hm The time to be compared with this.
   * @return true if the times are equivalent.
   */
  public boolean IsClose(HM hm) {
    // Same object
    return this == hm || this.hour == hm.hour && isclose(this.minute, hm.minute);
  }

  /**
   * @return The time as a string.
   */
  public String GetString() {
    boolean isNegative = false;
    if (this.hour < 0) {
      isNegative = true;
    } else {
      if (this.minute < 0) {
        isNegative = true;
      }
    }
    String s;
    if (isNegative) {
      s = "-";
    } else {
      s = " ";
    }
    s += String.format("%02d:%015.12f", Math.abs(this.hour), Math.abs(this.minute));
    return s;
  }

  /**
   * @return A string corresponding to the contents of the class.
   */
  @Override
  public String toString() {
    return String.format("%02d:%015.12f", this.hour, this.minute);
  }
}
