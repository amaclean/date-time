/*=========================================================================

  Program:   Date Time Library
  File   :   YMDHMS.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
package dt;

/**
 * This class holds a date as year, month, day, hours, minutes and seconds
 *
 * @author Andrew J. P. Maclean
 */
public class YMDHMS extends DateTimeBase {

  /**
   * The date.
   */
  public YMD ymd;
  /**
   * The time as hours minutes and seconds.
   */
  public HMS hms;

  public YMDHMS() {
    this.ymd = new YMD();
    this.hms = new HMS();
  }

  /**
   * @param ymd The date.
   * @param hms The time.
   */
  public YMDHMS(YMD ymd, HMS hms) {
    this.ymd = new YMD(ymd);
    this.hms = new HMS(hms);
  }

  /**
   * @param ymdhms The date and time.
   */
  public YMDHMS(YMDHMS ymdhms) {
    this.ymd = new YMD(ymdhms.ymd);
    this.hms = new HMS(ymdhms.hms);
  }

  /**
   * @param year   Year
   * @param month  Month
   * @param day    Day
   * @param hour   Hour
   * @param minute Minute
   * @param second Second
   */
  public YMDHMS(int year, int month, int day, int hour, int minute, double second) {
    this.ymd = new YMD(year, month, day);
    this.hms = new HMS(hour, minute, second);
  }

  /**
   * @param ymdhms The date and time.
   */
  public void SetYMDHMS(YMDHMS ymdhms) {
    this.ymd.SetYMD(ymdhms.ymd);
    this.hms.SetHMS(ymdhms.hms);
  }

  /**
   * @param ymd The date.
   * @param hms The time.
   */
  public void SetYMDHMS(YMD ymd, HMS hms) {
    this.ymd.SetYMD(ymd);
    this.hms.SetHMS(hms);
  }

  /**
   * @param year   Year
   * @param month  Month
   * @param day    Day
   * @param hour   Hour
   * @param minute Minute
   * @param second Second
   */
  public void SetYMDHMS(int year, int month, int day, int hour, int minute, double second) {
    this.ymd.SetYMD(year, month, day);
    this.hms.SetHMS(hour, minute, second);
  }

  /**
   * @return The date and time.
   */
  public YMDHMS GetYMDHMS() {
    return this;
  }

  /**
   * Set the date.
   *
   * @param ymd The date.
   */
  public void SetYMD(YMD ymd) {
    this.ymd.year = ymd.year;
    this.ymd.month = ymd.month;
    this.ymd.day = ymd.day;
  }

  /**
   * Set the date.
   *
   * @param year  Year
   * @param month Month
   * @param day   Day
   */
  public void SetYMD(int year, int month, int day) {
    this.ymd.year = year;
    this.ymd.month = month;
    this.ymd.day = day;
  }

  /**
   * Get the date.
   *
   * @return The date.
   */
  public YMD GetYMD() {
    return this.ymd;
  }

  /**
   * Set the time as hours minutes and seconds.
   *
   * @param hms The time.
   */
  public void SetHMS(HMS hms) {
    this.hms.hour = hms.hour;
    this.hms.minute = hms.minute;
    this.hms.second = hms.second;
  }

  /**
   * Set the time as hours minutes and seconds.
   *
   * @param hour   Hour
   * @param minute Minute
   * @param second Second
   */
  public void SetHMS(int hour, int minute, double second) {
    this.hms.hour = hour;
    this.hms.minute = minute;
    this.hms.second = second;
  }

  /**
   * Get the time as hours minutes and seconds.
   *
   * @return hms The time.
   */
  public HMS GetHMS() {
    return this.hms;
  }

  /**
   * Get the time as hours and minutes.
   *
   * @return hm The time.
   */
  public HM GetHM() {
    return new HM(this.hms.hour, this.hms.minute + this.hms.second / 60.0);
  }

  /**
   * Checks the two objects for equality.
   *
   * @param o the object to which this one is to be checked for equality
   * @return true if equal
   */
  @Override
  public boolean equals(Object o) {
    // Same object
    if (this == o) {
      return true;
    }
    // Check if the other object has the correct type.
    if (!(o instanceof YMDHMS)) {
      return false;
    }
    // Cast to the appropriate type.
    YMDHMS p = (YMDHMS) o;
    // Check the reference fields.
    return this.GetYMD().equals(p.GetYMD()) && this.GetHMS().equals(p.GetHMS());
  }

  /**
   * @param rhs The date and time.
   * @return true if this == rhs, false otherwise
   */
  public boolean eq(YMDHMS rhs) {
    return this.equals(rhs);
  }

  /**
   * @param rhs The date and time.
   * @return true if this != rhs, false otherwise
   */
  public boolean ne(YMDHMS rhs) {
    return !this.equals(rhs);
  }

  /**
   * @param rhs The date and time.
   * @return true if this &lt; rhs
   */
  public boolean lt(YMDHMS rhs) {
    return this.ymd.lt(rhs.ymd) || (this.ymd.eq(rhs.ymd)) && (this.hms.lt(rhs.hms));
  }

  /**
   * @param rhs The date and time.
   * @return true if this &le; rhs
   */
  public boolean le(YMDHMS rhs) {
    return !(this.gt(rhs));
  }

  /**
   * @param rhs The date and time.
   * @return true if this &gt; rhs
   */
  public boolean gt(YMDHMS rhs) {
    return this.ymd.gt(rhs.ymd) || (this.ymd.eq(rhs.ymd)) && (this.hms.gt(rhs.hms));
  }

  /**
   * @param rhs The date and time.
   * @return true if this &ge; rhs
   */
  public boolean ge(YMDHMS rhs) {
    return !(this.lt(rhs));
  }

  /**
   * Compare Calendar Dates for equality.
   *
   * @param d       The Calendar Date to be compared with this.
   * @param rel_tol The precision.
   * @param abs_tol The precision.
   * @return true if the date times are equivalent.
   */
  public boolean IsClose(YMDHMS d, double rel_tol, double abs_tol) {
    // Same object
    return this == d || this.GetYMD().eq(d.GetYMD()) && this.GetHMS().IsClose(d.GetHMS(), rel_tol, abs_tol);
  }

  /**
   * Compare Calendar Dates for equality.
   * <p>
   * Assumes an absolute tolerance of 0.0.
   *
   * @param d       The Calendar Date to be compared with this.
   * @param rel_tol The precision.
   * @return true if the date times are equivalent.
   */
  public boolean IsClose(YMDHMS d, double rel_tol) {
    // Same object
    return this == d || this.GetYMD().eq(d.GetYMD()) && this.GetHMS().IsClose(d.GetHMS(), rel_tol);
  }

  /**
   * Compare Calendar Dates for equality.
   * <p>
   * Assumes a relative tolerance of 1.0e-09 and an absolute tolerance of 0.0.
   *
   * @param d The Calendar Date to be compared with this.
   * @return true if the date times are equivalent.
   */
  public boolean IsClose(YMDHMS d) {
    // Same object
    return this == d || this.GetYMD().eq(d.GetYMD()) && this.GetHMS().IsClose(d.GetHMS());
  }

  /**
   * @return The date and time as a string.
   */
  public String GetString() {
    return String.format("%s %s", this.ymd.GetString(), this.hms.GetString());
  }

  /**
   * @return A string corresponding to the contents of the class.
   */
  @Override
  public String toString() {
    return this.ymd.toString() + " " + this.hms.toString();
  }

}
