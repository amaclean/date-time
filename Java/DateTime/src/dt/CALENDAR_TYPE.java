/*=========================================================================

  Program:   Date Time Library
  File   :   CalendarType.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

/*
  The date Time Library
 */
package dt;

/**
 * Enumerate the calendar to use.
 *
 * @author Andrew J. P. Maclean
 */
public enum CALENDAR_TYPE {
  /**
   * Proleptic Julian Calendar and Julian Calendar for dates on or before 1582
   * Oct 4, Julian Calendar for dates on or after 1582 Oct 15.
   */
  JULIAN,
  /**
   * Proleptic Gregorian Calendar for dates on or before 1582 Oct 4, Gregorian
   * Calendar for dates on or after 1582 Oct 15.
   */
  GREGORIAN,
  /**
   * The Proleptic Julian Calendar and the Julian Calendar is used for dates
   * on or before 1582 Oct 4. The Gregorian calendar is used for dates on or
   * after 1582 Oct 15.
   */
  JULIAN_GREGORIAN
}
