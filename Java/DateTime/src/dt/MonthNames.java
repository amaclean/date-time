/*=========================================================================

  Program:   Date Time Library
  File   :   MonthNames.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/*

 */
package dt;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * This class provides a convenient way of accessing either month names or
 * abbreviated month names based on the index on the month. Months are indexed
 * from 1 to 12, with 1 being the first month of the year (January) and 12 being
 * the last month of the year (December).
 * <p>
 * The language used is English, however you can set the month names to other
 * languages.
 *
 * @author Andrew J. P. Maclean
 */
public class MonthNames {

  private final Map<Integer, String> names = new HashMap<>();

  public MonthNames() {
    names.put(1, "January");
    names.put(2, "February");
    names.put(3, "March");
    names.put(4, "April");
    names.put(5, "May");
    names.put(6, "June");
    names.put(7, "July");
    names.put(8, "August");
    names.put(9, "September");
    names.put(10, "October");
    names.put(11, "November");
    names.put(12, "December");
  }

  // Assignment

  /**
   * @param mn the month names.
   */
  public MonthNames(MonthNames mn) {
    this.names.clear();
    for (Entry<Integer, String> pair : mn.names.entrySet()) {
      this.names.put(pair.getKey(), pair.getValue());
    }
  }

  /**
   * assignment
   *
   * @param rhs The day names.
   */
  public void assign(MonthNames rhs) {
    this.names.clear();
    for (Entry<Integer, String> pair : rhs.names.entrySet()) {
      this.names.put(pair.getKey(), pair.getValue());
    }
  }

  /**
   * @param rhs The time.
   * @return true if this == rhs, false otherwise
   */
  public boolean eq(MonthNames rhs) {
    return this.names.equals(rhs.names);
  }

  /**
   * @param rhs The time.
   * @return true if this != rhs, false otherwise
   */
  public boolean ne(MonthNames rhs) {
    return !this.eq(rhs);
  }

  /**
   * Get the Month name.
   * <p>
   * An empty string is returned if the index of the Month lies outside the
   * range 1...12.
   *
   * @param month The month number.
   * @return The month name or an empty string if not found.
   */
  public String GetMonthName(int month) {
    if (names.containsKey(month)) {
      return names.get(month);
    }
    return "";
  }

  /**
   * Get the abbreviated Month name.
   * <p>
   * An empty string is returned if the index of the Month lies outside the
   * range 1...12.
   * <p>
   * For other languages you may need to select a different number of letters
   * to ensure a unique abbreviation. For English, two letters are sufficient,
   * but three are used.
   *
   * @param month The month number.
   * @param size  The number of letters used to form the abbreviation
   * @return The abbreviated month name or an empty string if not found.
   */
  public String GetAbbreviatedMonthName(int month, int size) {
    if (names.containsKey(month)) {
      String name = names.get(month);
      if (size <= name.length()) {
        return name.substring(0, size);
      }
      return name;
    }
    return "";
  }

  /**
   * Set the Month name. Useful when using a different language.
   * <p>
   * There are 12 Months and the first Month starts at 1. No updates are
   * performed if the index of the Month lies outside the range 1...12.
   *
   * @param month     The month number.
   * @param monthName The month name.
   */
  public void SetMonthName(int month, String monthName) {
    if (month > 0 && month < 13) {
      names.put(month, monthName);
    }
  }

  /**
   * @return A string corresponding to the contents of the class.
   */
  @Override
  public String toString() {
    return String.format("(names: %s)", this.names);
  }
}
