#!/bin/bash
# Generate javadoc documentation.

# Author: Andrew Maclean
# Date: 2015-02-05

javadoc -overview ../../../README.html -windowtitle "Date Time Library" -author -linksource -sourcetab 2 ../dt/*.java ../dtTests/*.java  -d ../../doc/javadoc

