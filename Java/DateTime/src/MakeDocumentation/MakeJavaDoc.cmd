rem Generate javadoc documentation.

rem Author: Andrew Maclean
rem Date: 2015-02-05

set javadoc="%JAVA_HOME%\bin\javadoc.exe"

%javadoc% -overview ../../../README.html -windowtitle "Date Time Library" -author -linksource -sourcetab 2 ../dt/*.java ../dtTests/*.java  -d ../../doc/javadoc
