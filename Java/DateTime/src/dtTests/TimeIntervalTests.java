/*=========================================================================

  Program:   Date Time Library
  File   :   TimeIntervalTests.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
package dtTests;

import dt.DTPair;
import dt.DateTimeBase;
import dt.TimeInterval;

import java.util.ArrayList;

/**
 * A test harness to test the TimeInterval class.
 *
 * @author Andrew J. P. Maclean
 */
public class TimeIntervalTests extends CommonTests {

  private final int deltaDay = 15;
  private final double deltaFoD = 0.459;
  private final TimeInterval ti = new TimeInterval(this.deltaDay, this.deltaFoD);
  private final DateTimeBase dtb = new DateTimeBase();

  /**
   * Run all the tests.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * string of failed tests.
   */
  public DTPair<Boolean, String> TestAll() {
    ArrayList<DTPair<Boolean, String>> messages = new ArrayList<>();

    messages.add(this.TestConstructors());
    messages.add(this.TestSetGet());
    messages.add(this.TestComparisonOperators());
    messages.add(this.TestAssignment());
    messages.add(this.TestStringOutput());

    return this.AggregateResults("TimeInterval Tests.", messages);

  }

  /**
   * Run the tests for the constructors.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestConstructors() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing constructors.", 2);

    DTPair<Boolean, String> p1;

    p.second += "* TimeInterval() ";
    TimeInterval t = new TimeInterval();
    p1 = this.PassFail(t.GetDeltaDay() == 0 && t.GetDeltaFoD() == 0.0);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* TimeInterval(deltaDay,deltaFoD) ";
    TimeInterval t1 = new TimeInterval(this.deltaDay, this.deltaFoD);
    p1 = this.PassFail(t1.GetDeltaDay() == this.deltaDay
        && this.dtb.isclose(t1.GetDeltaFoD(), this.deltaFoD, dtb.rel_fod));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* TimeInterval(ti) ";
    TimeInterval t2 = new TimeInterval(t1);
    p1 = this.PassFail(t1.GetDeltaDay() == t2.GetDeltaDay()
        && this.dtb.isclose(t1.GetDeltaFoD(), t2.GetDeltaFoD(), dtb.rel_fod));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for setting and getting the members.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestSetGet() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing setting and getting members.", 2);

    DTPair<Boolean, String> p1;

    p.second += "* SetTimeInterval(deltaDay,deltaFoD) ";
    TimeInterval t1 = new TimeInterval(this.deltaDay, this.deltaFoD);
    TimeInterval t2 = new TimeInterval();
    t2.SetTimeInterval(this.deltaDay, this.deltaFoD);
    p1 = this.PassFail(t1.GetDeltaDay() == t2.GetDeltaDay() && t1.GetDeltaFoD() == t2.GetDeltaFoD());
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* SetTimeInterval(TimeInterval) ";
    t2.SetTimeInterval(t1);
    p1 = this.PassFail(t1.GetDeltaDay() == t2.GetDeltaDay() && t1.GetDeltaFoD() == t2.GetDeltaFoD());
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* GetTimeInterval() ";
    t2 = t1.GetTimeInterval();
    p1 = this.PassFail(t1.GetDeltaDay() == t2.GetDeltaDay() && t1.GetDeltaFoD() == t2.GetDeltaFoD());
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for the comparison operators.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestComparisonOperators() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing comparison operators.", 2);

    DTPair<Boolean, String> p1;

    p.second += "* == ";
    TimeInterval t1 = new TimeInterval(this.deltaDay, this.deltaFoD);
    p1 = this.PassFail(t1.eq(this.ti));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* &asymp; ";
    t1.SetDeltaFoD(t1.GetDeltaFoD() - 0.01);
    p1 = this.PassFail(this.ti.IsClose(t1, 0.1));
    p.first &= p1.first;
    p.second += "\n<br>" + p1.second;

    p1 = this.PassFail(!this.ti.IsClose(t1, 0.01));
    p.first &= p1.first;
    p.second += "<br>" + p1.second;

    p.second += "* != ";
    p1 = this.PassFail(t1.ne(this.ti));
    p.first &= p1.first;
    p.second += p1.second;

    TimeInterval a = new TimeInterval(2456553, 0.53308796);
    TimeInterval b = new TimeInterval(2456554, 0.53308796);
    TimeInterval c = new TimeInterval(2456553, 0.53308797);

    p.second += "* &lt; and &le; ";
    p1 = this.PassFail(a.lt(b) && a.lt(c) && a.le(b) && a.le(c) && a.le(a) && !(a.gt(b)) && !(a.ge(b)));
    p.first &= p1.first;
    p.second += p1.second;

    TimeInterval e = new TimeInterval(2456553, 0.53308796);
    TimeInterval f = new TimeInterval(2456552, 0.53308796);
    TimeInterval g = new TimeInterval(2456553, 0.53308795);

    p.second += "* &gt; and &ge; ";
    p1 = this.PassFail(e.gt(f) && e.gt(g) && e.ge(f) && e.ge(g) && e.ge(e) && !(e.lt(f)) && !(e.le(f)));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for assignment.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestAssignment() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing assignment.", 2);

    DTPair<Boolean, String> p1;

    p.second += "* = ";
    TimeInterval t1 = new TimeInterval();
    t1.assign(this.ti);
    p1 = this.PassFail(t1.GetDeltaDay() == this.ti.GetDeltaDay() && t1.GetDeltaFoD() == this.ti.GetDeltaFoD());
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for the increment and addition operators.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  public DTPair<Boolean, String> TestIncrementAndAddition() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing increment and addition.", 2);

    DTPair<Boolean, String> p1;

    p.second += "* ti1 += ti2 ";
    TimeInterval ti1 = new TimeInterval(this.deltaDay, this.deltaFoD);
    TimeInterval ti2 = new TimeInterval();
    double fod = 15 / 86400.0;
    ti2.SetTimeInterval(1, fod); // One day and 15s
    TimeInterval ti3 = new TimeInterval(ti1);
    ti3.inc(ti2);
    TimeInterval ti4 = new TimeInterval(this.deltaDay + 1, this.deltaFoD + fod);
    p1 = this.PassFail(ti3.IsClose(ti4, dtb.rel_fod));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* ti1 += days ";
    double days = 1 + fod;
    ti3.assign(ti1);
    ti3.inc(days);
    p1 = this.PassFail(ti3.IsClose(ti4, dtb.rel_fod));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* ti1 + ti2 ";
    ti3 = ti1.add(ti2);
    p1 = this.PassFail(ti3.IsClose(ti4, dtb.rel_fod));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for the decrement and subtraction operators.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  public DTPair<Boolean, String> TestDecrementAndSubtraction() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing decrement and subtraction.", 2);

    DTPair<Boolean, String> p1;

    p.second += "* ti1 -= ti2 ";
    TimeInterval ti1 = new TimeInterval(this.deltaDay, this.deltaFoD);
    TimeInterval ti2 = new TimeInterval();
    double fod = 15 / 86400.0;
    ti2.SetTimeInterval(1, fod); // One day and 15s
    TimeInterval ti3 = new TimeInterval(ti1);
    ti3.dec(ti2);
    TimeInterval ti4 = new TimeInterval(this.deltaDay - 1, this.deltaFoD - fod);
    p1 = this.PassFail(ti3.IsClose(ti4, dtb.rel_fod));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* ti1 -= days ";
    double days = 1 + fod;
    ti3.assign(ti1);
    ti3.dec(days);
    p1 = this.PassFail(ti3.IsClose(ti4, dtb.rel_fod));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* ti1 - ti2 ";
    ti3 = ti1.sub(ti2);
    p1 = this.PassFail(ti3.IsClose(ti4, dtb.rel_fod));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for string output.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestStringOutput() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing GetString()", 2);

    DTPair<Boolean, String> p1;

    String s1 = this.ti.GetString();
    String valueStr = " 15.459000000000000";
    p1 = this.Compare(s1, valueStr, "* ");
    p.first &= p1.first;
    p.second += p1.second;

    TimeInterval t1 = new TimeInterval(-1, 0.5);
    String expected = "-0.500000000000000";
    s1 = t1.GetString();
    p1 = this.Compare(s1, expected, "* ");
    p.first &= p1.first;
    p.second += p1.second;

    t1.SetTimeInterval(0, -1.5);
    expected = "-1.500000000000000";
    s1 = t1.GetString();
    p1 = this.Compare(s1, expected, "* ");
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

}
