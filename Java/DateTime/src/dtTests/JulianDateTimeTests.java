/*=========================================================================

  Program:   Date Time Library
  File   :   JulianDateTimeTests.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
package dtTests;

import dt.*;

import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;

/**
 * A test class used to test the date time classes.
 *
 * @author Andrew J. P. Maclean
 */
public class JulianDateTimeTests extends CommonTests {

  private final DateTimeBase dtb = new DateTimeBase();

  /**
   * Run all the tests.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * string of failed tests.
   */
  public DTPair<Boolean, String> TestAll() {
    ArrayList<DTPair<Boolean, String>> messages = new ArrayList<>();

    messages.add(this.TestConstructors());
    messages.add(this.TestComparisonOperators());
    messages.add(this.TestAssignment());
    messages.add(this.TestIncrementAndAddition());
    messages.add(this.TestDecrementAndSubtraction());

    messages.add(this.TestEphemerisDates());
    messages.add(this.TestDoublePrecision(dt.CALENDAR_TYPE.JULIAN));
    messages.add(this.TestDoublePrecision(CALENDAR_TYPE.GREGORIAN));
    messages.add(this.TestDoublePrecision(CALENDAR_TYPE.JULIAN_GREGORIAN));

    messages.add(this.TestDayOfTheYear());
    messages.add(this.TestDayOfTheWeek());
    messages.add(this.TestISOWeekDate());

    messages.add(this.TestTime());

    messages.add(this.TestTimeZone());

    messages.add(this.TestJulianDates());

    return this.AggregateResults("JulianDateTime Tests.", messages);
  }

  /**
   * Test the conversion of calendar date to julian day. The Julian Proleptic
   * Calendar is used.
   *
   * @param jd   The expected julian day for the given year, month and day.
   * @param date The date.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestCD2JDJ(double jd, JulianDateTime date) {
    YMD ymd = new YMD(date.GetDate(CALENDAR_TYPE.JULIAN));
    int tjd = date.cd2jdJ(ymd);
    // Note: We need to round up because the calendrical calculation returns
    // the JD at midday.
    int ref = (int) Math.ceil(jd);
    return this.Compare(tjd, ref, "<br>cd2jdJ");
  }

  /**
   * Test the conversion of julian day to calendar date. The Julian Proleptic
   * Calendar is used.
   *
   * @param jd   The julian day.
   * @param date The date.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestJD2CDJ(double jd, JulianDateTime date) {
    // Note: We need to round up because the calendrical calculation returns
    // the JD at midday.
    YMD ymd1 = new YMD(date.jd2cdJ((int) Math.ceil(jd)));
    YMD ymd2 = new YMD(date.GetDate(CALENDAR_TYPE.JULIAN));
    return this.Compare(ymd1, ymd2, "<br>jd2cdJ");
  }

  /**
   * Test the conversion of calendar date to julian day and vice versa. The
   * Julian Proleptic Calendar is used.
   *
   * @param jd   The julian day.
   * @param date The date.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestJulian(double jd, JulianDateTime date) {
    DTPair<Boolean, String> p1 = TestCD2JDJ(jd, date);
    DTPair<Boolean, String> p2 = TestJD2CDJ(jd, date);
    return new DTPair<>(p1.first && p2.first, p1.second + p2.second);
  }

  /**
   * Test the conversion of calendar date to julian day. The Gregorian
   * Proleptic Calendar is used.
   *
   * @param jd   The expected julian day for the given year, month and day.
   * @param date The date.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestCD2JDG(double jd, JulianDateTime date) {
    YMD ymd = new YMD(date.GetDate(CALENDAR_TYPE.GREGORIAN));
    int tjd = date.cd2jdG(ymd);
    // Note: We need to round up because the calendrical calculation returns
    // the JD at midday.
    int ref = (int) Math.ceil(jd);
    return this.Compare(tjd, ref, "<br>cd2jdG");
  }

  /**
   * Test the conversion of julian day to calendar date. The Gregorian
   * Proleptic Calendar is used.
   *
   * @param jd   The julian day.
   * @param date The date.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestJD2CDG(double jd, JulianDateTime date) {
    // Note: We need to round up because the calendrical calculation returns
    // the JD at midday.
    YMD ymd1 = new YMD(date.jd2cdG((int) Math.ceil(jd)));
    YMD ymd2 = new YMD(date.GetDate(CALENDAR_TYPE.GREGORIAN));
    return this.Compare(ymd1, ymd2, "<br>jd2cdG");
  }

  /**
   * Test the conversion of calendar date to julian day and vice versa. The
   * Gregorian Proleptic Calendar is used.
   *
   * @param jd   The julian day.
   * @param date The date.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestGregorian(double jd, JulianDateTime date) {
    DTPair<Boolean, String> p1 = TestCD2JDG(jd, date);
    DTPair<Boolean, String> p2 = TestJD2CDG(jd, date);
    return new DTPair<>(p1.first && p2.first, p1.second + p2.second);
  }

  /**
   * Test the conversion of calendar date to julian day for a given hour of
   * the day.
   *
   * @param jd   The expected julian day for the given year, month and day.
   * @param date The date.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestGetDateJD(double jd, JulianDateTime date, CALENDAR_TYPE calendar) {
    DTPair<Boolean, String> res = new DTPair<>(null, null);
    JD jdpf = date.GetJDFullPrecision();
    if (dtb.isclose(jdpf.GetJDDouble(), jd, dtb.rel_fod)) {
      res.first = true;
      res.second = "<br>Passed (GetJDFullPrecision())\n";
    } else {
      res.first = false;
      res.second = String.format("<br>Fail (GetJDFullPrecision()) got: %.12f  expected: %.12f\n",
          date.GetJDDouble(), jd);
    }
    return res;
  }

  /**
   * Test the conversion of julian day to calendar date.
   *
   * @param jd       The julian day.
   * @param date     The date.
   * @param calendar Used to specify the calendar.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestGetDateCT(double jd, JulianDateTime date, CALENDAR_TYPE calendar) {
    DTPair<Boolean, String> res = new DTPair<>(null, null);
    YMDHMS ymdhms1 = new YMDHMS(date.GetDateTime(calendar));
    JulianDateTime d = new JulianDateTime();
    d.SetDateTime(jd, 0.0);
    YMDHMS ymdhms2 = new YMDHMS(d.GetDateTime(calendar));
    if (ymdhms1.IsClose(ymdhms2, dtb.rel_seconds)) {
      res.first = true;
      res.second = "<br>Passed (GetDateTime())\n";
    } else {
      res.first = false;
      res.second = String.format("<br>Fail (GetDateTime()) got: %s, expected: %s\n", ymdhms1, ymdhms2);
    }
    return res;
  }

  /**
   * Test the conversion of calendar date to julian day for a given hour of
   * the day and vice versa for a specified calendar system.
   *
   * @param jd       The julian day.
   * @param date     The date.
   * @param calendar Used to specify the calendar.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestDPDateTime(double jd, JulianDateTime date, CALENDAR_TYPE calendar) {
    DTPair<Boolean, String> p1 = TestGetDateJD(jd, date, calendar);
    DTPair<Boolean, String> p2 = TestGetDateCT(jd, date, calendar);
    return new DTPair<>(p1.first && p2.first, p1.second + p2.second);
  }

  /**
   * Test the conversion of hours minutes and seconds to hours.
   *
   * @param hr   The expected hour of the day.
   * @param time The time.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> Testhms2hr(double hr, JulianDateTime time) {
    DTPair<Boolean, String> res = new DTPair<>(null, null);
    HMS hms = new HMS(time.GetTimeHMS());
    double h = time.hms2h(hms);
    if (this.dtb.isclose(hr, h, dtb.rel_fod * 10)) {
      res.first = true;
      res.second = "<br>Passed (hms2h)\n";
    } else {
      res.first = false;
      res.second = String.format("<br>Fail (hms2h) got: %.12f, expected: %.12f\n", h, hr);
    }
    return res;
  }

  /**
   * Test the conversion of hours of the day to hours minutes and seconds.
   *
   * @param hr   The hour of the day.
   * @param time The expected time.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> Testhr2hms(double hr, JulianDateTime time) {
    DTPair<Boolean, String> res = new DTPair<>(null, null);
    HMS thms = new HMS(time.h2hms(hr));
    HMS hms = new HMS(time.GetTimeHMS());
    if (hms.IsClose(thms, dtb.rel_seconds)) {
      res.first = true;
      res.second = "<br>Passed (h2hms)\n";
    } else {
      res.first = false;
      res.second = String.format("<br>Fail (h2hms) got: %s,  expected: %s\n", thms, hms);
    }
    return res;
  }

  /**
   * Test the conversion of hours minutes and seconds to hours and vice versa.
   *
   * @param hr   The hour of the day.
   * @param time The time.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestHMS(double hr, JulianDateTime time) {
    DTPair<Boolean, String> p = new DTPair<>(null, null);
    DTPair<Boolean, String> p1;
    DTPair<Boolean, String> p2;
    p1 = Testhms2hr(hr, time);
    p2 = Testhr2hms(hr, time);
    p.first = p1.first & p2.first;
    p.second = p1.second + p2.second;
    return p;
  }

  /**
   * Test the conversion of hours and minutes to hours.
   *
   * @param hr   The expected hour of the day.
   * @param time The time.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> Testhm2hr(double hr, JulianDateTime time) {
    DTPair<Boolean, String> res = new DTPair<>(null, null);
    HM hm = new HM(time.GetTimeHM());
    double h = time.hm2h(hm);
    if (this.dtb.isclose(hr, h, dtb.rel_fod * 10)) {
      res.first = true;
      res.second = "<br>Passed (hm2h)\n";
      return res;
    } else {
      res.first = false;
      res.second = String.format("<br>Fail (hm2h) got: %.12f,  expected: %.12f\n", h, hr);
    }
    return res;
  }

  /**
   * Test the conversion of hours of the day to hours and minutes.
   *
   * @param hr   The hour of the day.
   * @param time The expected time.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> Testhr2hm(double hr, JulianDateTime time) {
    DTPair<Boolean, String> res = new DTPair<>(true, null);
    HM thm = new HM(time.h2hm(hr));
    HM hm = new HM(time.GetTimeHM());
    if (hm.IsClose(thm, dtb.rel_minutes)) {
      res.first = true;
      res.second = "<br>Passed (h2hm)\n";
    } else {
      res.first = false;
      res.second = String.format("<br>Fail (h2hm) got: %s, expected: %s\n", thm, hm);
    }
    return res;
  }

  /**
   * Test the conversion of hours minutes and seconds to hours and vice versa.
   *
   * @param hr   The hour of the day.
   * @param time The time.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestHM(double hr, JulianDateTime time) {
    DTPair<Boolean, String> p = new DTPair<>(null, null);
    DTPair<Boolean, String> p1;
    DTPair<Boolean, String> p2;
    p1 = Testhm2hr(hr, time);
    p2 = Testhr2hm(hr, time);
    p.first = p1.first & p2.first;
    p.second = p1.second + p2.second;
    return p;
  }

  /**
   * Test the conversion of calendar date to ISO week of the year.
   *
   * @param ymd The date.
   * @param iwd The ISO week date.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> Testcd2woy(YMD ymd, ISOWeekDate iwd) {
    DTPair<Boolean, String> res = new DTPair<>(true, null);
    JulianDateTime dt = new JulianDateTime();
    dt.SetDate(ymd, CALENDAR_TYPE.JULIAN_GREGORIAN);
    ISOWeekDate iwd1 = new ISOWeekDate(dt.GetISOWeek());
    if (iwd.eq(iwd1)) {
      res.first = true;
      res.second = "<br>Passed\n";
    } else {
      res.first = false;
      res.second = String.format("<br>Fail (cd2woy) got: %s, expected: %s\n", iwd1, iwd);
    }
    return res;
  }

  /**
   * Run the tests for the copy constructors.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestConstructors() {

    JulianDateTime d1 = new JulianDateTime();

    DTPair<Boolean, String> p = this.MakeHeader("Testing constructors.", 2);

    DTPair<Boolean, String> p1;

    p.second += "* DateTime(dt) ";
    YMDHMS ymdhms = new YMDHMS(2012, 1, 1, 23, 45, 45);
    d1.SetDateTime(ymdhms, CALENDAR_TYPE.JULIAN_GREGORIAN);
    JulianDateTime d2 = new JulianDateTime(d1);
    p1 = this.PassFail(d1.eq(d2));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* DateTime(jd, fod) ";
    JulianDateTime d3 = new JulianDateTime(d1.GetJDN(), d1.GetFoD());
    p1 = this.PassFail(d1.eq(d3));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* DateTime(jdn, 0) ";
    JulianDateTime d4 = new JulianDateTime(d1.GetJDN(), d1.GetFoD());
    p1 = this.PassFail(d1.eq(d4));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* DateTime(ymdhms, calendar) ";
    CALENDAR_TYPE calendar = CALENDAR_TYPE.JULIAN_GREGORIAN;
    JulianDateTime d5 = new JulianDateTime(ymdhms, calendar);
    p1 = this.PassFail(d1.eq(d5));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for the comparison operators.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestComparisonOperators() {

    JulianDateTime d1 = new JulianDateTime();
    JulianDateTime d2 = new JulianDateTime();

    DTPair<Boolean, String> p = this.MakeHeader("Testing comparison operators.", 2);

    CALENDAR_TYPE calendar = CALENDAR_TYPE.JULIAN_GREGORIAN;

    DTPair<Boolean, String> p1;

    p.second += "* == \n";
    YMDHMS ymdhms = new YMDHMS(2012, 1, 1, 23, 45, 45);
    d1.SetDateTime(ymdhms, calendar);
    d2.SetDateTime(ymdhms, calendar);
    p1 = this.PassFail(d1.eq(d2));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* &asymp; ";
    JulianDateTime d3 = new JulianDateTime();
    d3.SetDateTime(new YMDHMS(2012, 1, 1, 23, 45, 45.1), calendar);
    p1 = this.PassFail(d1.IsClose(d3, 1.0e-5));
    p.first &= p1.first;
    p.second += "\n<br>" + p1.second;

    p1 = this.PassFail(!d1.IsClose(d3, 1.0e-6));
    p.first &= p1.first;
    p.second += "<br>" + p1.second;

    p.second += "* != \n";
    ymdhms.hms.second = 45.001;
    d2.SetDateTime(ymdhms, calendar);
    p1 = this.PassFail(d1.ne(d2));
    p.first &= p1.first;
    p.second += p1.second;

    JulianDateTime a = new JulianDateTime(2456553, 0.53308796);
    JulianDateTime b = new JulianDateTime(2456554, 0.53308796);
    JulianDateTime c = new JulianDateTime(2456553, 0.53308797);

    p.second += "* &lt; and &le; ";
    p1 = this.PassFail(a.lt(b) && a.lt(c) && a.le(b) && a.le(c) && a.le(a) && !(a.gt(b)) && !(a.ge(b)));
    p.first &= p1.first;
    p.second += p1.second;

    JulianDateTime e = new JulianDateTime(2456553, 0.53308796);
    JulianDateTime f = new JulianDateTime(2456552, 0.53308796);
    JulianDateTime g = new JulianDateTime(2456553, 0.53308795);

    p.second += "* &gt; and &ge; ";
    p1 = this.PassFail(e.gt(f) && e.gt(g) && e.ge(f) && e.ge(g) && e.ge(e) && !(e.lt(f)) && !(e.le(f)));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for assignment.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestAssignment() {

    JulianDateTime d1 = new JulianDateTime();
    JulianDateTime d2 = new JulianDateTime();

    DTPair<Boolean, String> p = this.MakeHeader("Testing assignment.", 2);

    CALENDAR_TYPE calendar = CALENDAR_TYPE.JULIAN_GREGORIAN;
    DTPair<Boolean, String> p1 = new DTPair<>(true, null);

    p.second += "* = ";
    YMDHMS ymdhms = new YMDHMS(2012, 1, 1, 23, 45, 45);
    d1.SetDateTime(ymdhms, calendar);
    d2.assign(d1);
    p = this.PassFail(d1.eq(d2));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for the increment and addition operators.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestIncrementAndAddition() {

    JulianDateTime d1 = new JulianDateTime();

    DTPair<Boolean, String> p = this.MakeHeader("Testing increment and addition.", 2);

    CALENDAR_TYPE calendar = CALENDAR_TYPE.JULIAN_GREGORIAN;

    DTPair<Boolean, String> p1;

    p.second += "* dt1 += dt2 ";
    YMDHMS ymdhms = new YMDHMS(2012, 1, 1, 23, 45, 45);
    d1.SetDateTime(ymdhms, calendar);
    JulianDateTime d2 = new JulianDateTime();
    double fod = 15 / 86400.0;
    TimeInterval dT = new TimeInterval(1, fod); // One day and 15s
    d2.SetDeltaT(dT);
    JulianDateTime d3 = new JulianDateTime(d1);
    d3.inc(d2);
    JulianDateTime d4 = new JulianDateTime();
    YMDHMS ymdhms1 = new YMDHMS(2012, 1, 2, 23, 46, 0);
    d4.SetDateTime(ymdhms1, calendar);
    p1 = this.PassFail(d3.IsClose(d4, dtb.rel_fod));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* dt1 += days ";
    double days = 1 + fod;
    d3.assign(d1);
    d3.inc(days);
    p1 = this.PassFail(d3.IsClose(d4, dtb.rel_fod));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* dt1 + dt2 ";
    d3 = d1.add(d2);
    p1 = this.PassFail(d3.IsClose(d4, dtb.rel_fod));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for the decrement and subtraction operators.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestDecrementAndSubtraction() {

    JulianDateTime d1 = new JulianDateTime();

    DTPair<Boolean, String> p = new DTPair<>(true, null);
    p.second = "\n";
    p.second += "JulianDateTime, testing decrement and subtraction.\n";
    p.second += "\n";

    CALENDAR_TYPE calendar = CALENDAR_TYPE.JULIAN_GREGORIAN;

    DTPair<Boolean, String> p1;

    p.second += "* dt1 -= dt2 ";
    YMDHMS ymdhms = new YMDHMS(2012, 1, 2, 23, 46, 0);
    d1.SetDateTime(ymdhms, calendar);
    JulianDateTime d2 = new JulianDateTime();
    double fod = 15 / 86400.0;
    TimeInterval dT = new TimeInterval(1, fod); // One day and 15s
    d2.SetDeltaT(dT);
    JulianDateTime d3 = new JulianDateTime(d1);
    d3.dec(d2);
    JulianDateTime d4 = new JulianDateTime();
    YMDHMS ymdhms1 = new YMDHMS(2012, 1, 1, 23, 45, 45);
    d4.SetDateTime(ymdhms1, calendar);
    p1 = this.PassFail(d3.IsClose(d4, dtb.rel_fod));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* dt1 -= days ";
    double days = 1 + fod;
    d3.assign(d1);
    d3.dec(days);
    p1 = this.PassFail(d3.IsClose(d4, dtb.rel_fod));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* dt1 - dt2 ";
    d3 = d1.sub(d2);
    p1 = this.PassFail(d3.IsClose(d4, dtb.rel_fod));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for the date and time of specified dates and a specified
   * calendar.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestEphemerisDates() {
    YMDHMS ymdhms = new YMDHMS();
    JulianDateTime date = new JulianDateTime();
    CALENDAR_TYPE calendar = CALENDAR_TYPE.GREGORIAN;

    DTPair<Boolean, String> p = this.MakeHeader("Testing dates of ephemerides.", 2);

    DTPair<Boolean, String> p1;

    p.second += "* DE413 Start Jd=-3100015.50, -13200-AUG-15 00:00:00\n";
    ymdhms.SetYMDHMS(-13200, 8, 15, 0, 0, 0);
    date.SetDateTime(ymdhms, CALENDAR_TYPE.JULIAN);
    p1 = TestJulian(-3100015.50, date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* DE413 End Jd=8000002.5, 17191-MAR-01 00:00:00\n";
    ymdhms.SetYMDHMS(17191, 3, 1, 0, 0, 0);
    date.SetDateTime(ymdhms, CALENDAR_TYPE.GREGORIAN);
    p1 = TestGregorian(8000002.5, date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* DE406 Start Jd=625360.5, -3000 FEB 23 00:00:00\n";
    ymdhms.SetYMDHMS(-3000, 2, 23, 0, 0, 0);
    date.SetDateTime(ymdhms, CALENDAR_TYPE.JULIAN);
    p1 = TestJulian(625360.5, date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* DE406 end. Jd=2816912.5, 3000 MAY 06 00:00:00\n";
    ymdhms.SetYMDHMS(3000, 5, 6, 0, 0, 0);
    date.SetDateTime(ymdhms, calendar);
    p1 = TestGregorian((int) (2816912.5 + 0.5), date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* DE406 end (UNIX.406). Jd=2816976.5, 3000 JUL 09 00:00:00\n";
    ymdhms.SetYMDHMS(3000, 7, 9, 0, 0, 0);
    date.SetDateTime(ymdhms, calendar);
    p1 = TestGregorian(2816976.5, date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* DE405 Start Jd=2305424.5, 1599 DEC 09 00:00:00\n";
    ymdhms.SetYMDHMS(1599, 12, 9, 0, 0, 0);
    date.SetDateTime(ymdhms, calendar);
    p1 = TestGregorian((int) (2305424.5 + 0.5), date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* DE405 end. Jd=2525008.5, 2201 FEB 20 00:00:00\n";
    ymdhms.SetYMDHMS(2201, 2, 20, 0, 0, 0);
    date.SetDateTime(ymdhms, calendar);
    p1 = TestGregorian(2525008.5, date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* DE200 Start Jd=2305424.5, 1599 DEC 09 00:00:00\n";
    ymdhms.SetYMDHMS(1599, 12, 9, 0, 0, 0);
    date.SetDateTime(ymdhms, calendar);
    p1 = TestGregorian((int) (2305424.5 + 0.5), date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* DE200 end. Jd=2513392.5, 2169 MAY 02 00:00:00\n";
    ymdhms.SetYMDHMS(2169, 5, 2, 0, 0, 0);
    date.SetDateTime(ymdhms, calendar);
    p1 = TestGregorian(2513392.5, date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for the date and time of specified dates and a specified
   * calendar.
   *
   * @param calendar The calendar to use.
   * @return A pair consisting of true (if the test passed) or false and a
   * string of failed tests.
   */
  private DTPair<Boolean, String> TestDoublePrecision(CALENDAR_TYPE calendar) {
    JulianDateTime date = new JulianDateTime();

    DTPair<Boolean, String> p = this.MakeHeader("Testing dates of ephemerides using double precision.", 2);

    switch (calendar) {
      case JULIAN:
        p.second += this.MakeHeader("Julian Calendar.", 3).second;
        break;
      case GREGORIAN:
        p.second += this.MakeHeader("Gregorian Calendar.", 3).second;
        break;
      case JULIAN_GREGORIAN:
        p.second += this.MakeHeader("Julian/Gregorian Calendar.", 3).second;
        break;
      default:
        p.first = false;
        p.second += this.MakeHeader("Unknown calendar type.", 3).second;
    }

    DTPair<Boolean, String> p1;

    YMDHMS ymdhms = new YMDHMS();
    if (calendar == dt.CALENDAR_TYPE.JULIAN || calendar == dt.CALENDAR_TYPE.JULIAN_GREGORIAN) {
      p.second += "* DE406 Start Jd=625360.5, -3000 FEB 23 00:00:00\n";
      ymdhms.SetYMDHMS(-3000, 2, 23, 0, 0, 0);
      date.SetDateTime(ymdhms, calendar);
      p1 = TestDPDateTime(625360.5, date, calendar);
      p.first = p.first & p1.first;
      p.second = p.second + p1.second;
    }

    if (calendar == dt.CALENDAR_TYPE.GREGORIAN || calendar == dt.CALENDAR_TYPE.JULIAN_GREGORIAN) {
      p.second += "* DE406 end. Jd=2816912.5, 3000 MAY 06 00:00:00\n";
      ymdhms.SetYMDHMS(3000, 5, 6, 0, 0, 0);
      date.SetDateTime(ymdhms, calendar);
      p1 = TestDPDateTime(2816912.5, date, calendar);
      p.first = p.first & p1.first;
      p.second = p.second + p1.second;

      p.second += "* DE406 end (UNIX.406). Jd=2816976.5, 3000 JUL 09 00:00:00\n";
      ymdhms.SetYMDHMS(3000, 7, 9, 0, 0, 0);
      date.SetDateTime(ymdhms, calendar);
      p1 = TestDPDateTime(2816976.5, date, calendar);
      p.first = p.first & p1.first;
      p.second = p.second + p1.second;

      p.second += "* DE405 Start Jd=2305424.5, 1599 DEC 09 00:00:00\n";
      ymdhms.SetYMDHMS(1599, 12, 9, 0, 0, 0);
      date.SetDateTime(ymdhms, calendar);
      p1 = TestDPDateTime(2305424.5, date, calendar);
      p.first = p.first & p1.first;
      p.second = p.second + p1.second;

      p.second += "* DE405 end. Jd=2525008.5, 2201 FEB 20 00:00:00\n";
      ymdhms.SetYMDHMS(2201, 2, 20, 0, 0, 0);
      date.SetDateTime(ymdhms, calendar);
      p1 = TestDPDateTime(2525008.5, date, calendar);
      p.first = p.first & p1.first;
      p.second = p.second + p1.second;

      p.second += "* DE200 Start Jd=2305424.5, 1599 DEC 09 00:00:00\n";
      ymdhms.SetYMDHMS(1599, 12, 9, 0, 0, 0);
      date.SetDateTime(ymdhms, calendar);
      p1 = TestDPDateTime(2305424.5, date, calendar);
      p.first = p.first & p1.first;
      p.second = p.second + p1.second;

      p.second += "* DE200 end. Jd=2513392.5, 2169 MAY 02 00:00:00\n";
      ymdhms.SetYMDHMS(2169, 5, 2, 0, 0, 0);
      date.SetDateTime(ymdhms, calendar);
      p1 = TestDPDateTime(2513392.5, date, calendar);
      p.first = p.first & p1.first;
      p.second = p.second + p1.second;
    }

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for testing the day of the year.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestDayOfTheYear() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing Day of the Year.", 2);

    DTPair<Boolean, String> p1 = new DTPair<>(true, null);

    p.second += "* 1900.\n";

    YMD ymd = new YMD(1900, 12, 31);
    JulianDateTime jd = new JulianDateTime();
    jd.SetDateTime(jd.cd2jdJG(ymd), 0);
    int doyJ;// = jd.doyJ(ymd);
    int doyG = jd.doyG(ymd);
    int doyJG = jd.doyJG(ymd);

    // If we are using the Julian Calendar
    // Then this Julian Day corresponds to
    // this day of the year.
    doyJ = 353;
    if (doyJ == jd.GetDoY(CALENDAR_TYPE.JULIAN) && doyG == jd.GetDoY(CALENDAR_TYPE.GREGORIAN)
        && doyJG == jd.GetDoY(CALENDAR_TYPE.JULIAN_GREGORIAN)) {
      p1.first = true;
      p1.second = "<br>Passed.\n";
    } else {
      p1.first = false;
      p1.second = "<br>Failed.\n";
    }
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Test the day of the week for some dates.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestDayOfTheWeek() {

    JulianDateTime date = new JulianDateTime();

    DTPair<Boolean, String> p = this.MakeHeader("Testing the day of the week.", 2);

    DayNames dn = new DayNames();
    int idx;
    String name;

    p.second += "* -4712 01 01 Monday (Julian)\n";
    YMD d = new YMD(); // year = -4712, month = 1, day = 1 by default.
    date.SetDate(d, CALENDAR_TYPE.JULIAN);
    idx = date.GetDoW(CALENDAR_TYPE.JULIAN, FIRST_DOW.SUNDAY);
    name = dn.GetDayName(idx);
    if (idx != 2 || !Objects.equals(name, "Monday")) {
      p.first &= false;
      p.second += String.format("<br>Fail got: %d = %s, expected: 2 = Monday.\n", idx, name);
    } else {
      p.second += "<br>Passed.\n";
    }

    p.second += "* -4713 11 24 Friday (Gregorian)\n";
    d.SetYMD(-4713, 11, 24);
    date.SetDate(d, CALENDAR_TYPE.GREGORIAN);
    idx = date.GetDoW(CALENDAR_TYPE.GREGORIAN, FIRST_DOW.SUNDAY);
    name = dn.GetDayName(idx);
    if (idx != 6 || !Objects.equals(name, "Friday")) {
      p.first &= false;
      p.second += String.format("<br>Fail got: %d = %s, expected: 6 = Friday.\n", idx, name);
    } else {
      p.second += "<br>Passed.\n";
    }

    p.second += "* 1582 10 04 Thursday (Julian)\n";
    d.SetYMD(1582, 10, 4);
    date.SetDate(d, CALENDAR_TYPE.JULIAN_GREGORIAN);
    idx = date.GetDoW(CALENDAR_TYPE.JULIAN_GREGORIAN, FIRST_DOW.SUNDAY);
    name = dn.GetDayName(idx);
    if (idx != 5 || !Objects.equals(name, "Thursday")) {
      p.first &= false;
      p.second += String.format("<br>Fail got: %d = %s, expected: 5 = Thursday.\n", idx, name);
    } else {
      p.second += "<br>Passed.\n";
    }

    p.second += "* 1582 10 15 Friday (Gregorian)\n";
    d.SetYMD(1582, 10, 15);
    date.SetDate(d, CALENDAR_TYPE.GREGORIAN);
    idx = date.GetDoW(CALENDAR_TYPE.GREGORIAN, FIRST_DOW.SUNDAY);
    name = dn.GetDayName(idx);
    if (idx != 6 || !Objects.equals(name, "Friday")) {
      p.first &= false;
      p.second += String.format("<br>Fail got: %d = %s, expected: 6 = Friday.\n", idx, name);
    } else {
      p.second += "<br>Passed.\n";
    }

    p.second += "* 1582 10 15 Friday (Julian)\n";
    d.SetYMD(1582, 10, 15);
    date.SetDate(d, CALENDAR_TYPE.JULIAN);
    idx = date.GetDoW(CALENDAR_TYPE.JULIAN, FIRST_DOW.SUNDAY);
    name = dn.GetDayName(idx);
    if (idx != 6 || !Objects.equals(name, "Friday")) {
      p.first &= false;
      p.second += String.format("<br>Fail got: %d = %s, expected: 6 = Friday.\n", idx, name);
    } else {
      p.second += "<br>Passed.\n";
    }

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for the week of the year conversions.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestISOWeekDate() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing week of the year.", 2);

    DTPair<Boolean, String> p1;

    YMD ymd = new YMD();
    ISOWeekDate iwd = new ISOWeekDate();

    p.second += "* 2005-01-01 is 2004-W53-6\n";
    ymd.SetYMD(2005, 1, 1);
    iwd.SetISOWeekDate(2004, 53, 6);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2005-01-02 is 2004-W53-7\n";
    ymd.SetYMD(2005, 1, 2);
    iwd.SetISOWeekDate(2004, 53, 7);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2005-12-31 is 2005-W52-6\n";
    ymd.SetYMD(2005, 12, 31);
    iwd.SetISOWeekDate(2005, 52, 6);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2007-01-01 is 2007-W01-1 (both years 2007 start with the same day)\n";
    ymd.SetYMD(2007, 1, 1);
    iwd.SetISOWeekDate(2007, 1, 1);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2007-12-30 is 2007-W52-7\n";
    ymd.SetYMD(2007, 12, 30);
    iwd.SetISOWeekDate(2007, 52, 7);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2007-12-31 is 2008-W01-1\n";
    ymd.SetYMD(2007, 12, 31);
    iwd.SetISOWeekDate(2008, 1, 1);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2008-01-01 is 2008-W01-2 (Gregorian year 2008 is a leap year,\n ISO year 2008 is 2 days shorter: 1 day longer at the start,\n 3 days shorter at the end)\n";
    ymd.SetYMD(2008, 1, 1);
    iwd.SetISOWeekDate(2008, 1, 2);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2008-12-28 is 2008-W52-7\n";
    ymd.SetYMD(2008, 12, 28);
    iwd.SetISOWeekDate(2008, 52, 7);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2008-12-29 is 2009-W01-1\n";
    ymd.SetYMD(2008, 12, 29);
    iwd.SetISOWeekDate(2009, 1, 1);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2008-12-30 is 2009-W01-2\n";
    ymd.SetYMD(2008, 12, 30);
    iwd.SetISOWeekDate(2009, 1, 2);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2008-12-31 is 2009-W01-3\n";
    ymd.SetYMD(2008, 12, 31);
    iwd.SetISOWeekDate(2009, 1, 3);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2009-01-01 is 2009-W01-4\n";
    ymd.SetYMD(2009, 1, 1);
    iwd.SetISOWeekDate(2009, 1, 4);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2009-12-31 is 2009-W53-4 (ISO year 2009 has 53 weeks,\n extending the Gregorian year 2009,\n which starts and ends with Thursday,\n at both ends with three days)\n";
    ymd.SetYMD(2009, 12, 31);
    iwd.SetISOWeekDate(2009, 53, 4);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2010-01-01 is 2009-W53-5\n";
    ymd.SetYMD(2010, 1, 1);
    iwd.SetISOWeekDate(2009, 53, 5);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2010-01-02 is 2009-W53-6\n";
    ymd.SetYMD(2010, 1, 2);
    iwd.SetISOWeekDate(2009, 53, 6);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2010-01-03 is 2009-W53-7\n";
    ymd.SetYMD(2010, 1, 3);
    iwd.SetISOWeekDate(2009, 53, 7);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2010-01-04 is 2010-W01-1\n";
    ymd.SetYMD(2010, 1, 4);
    iwd.SetISOWeekDate(2010, 1, 1);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2008-09-26 is 2009-W39-5\n";
    ymd.SetYMD(2008, 9, 26);
    iwd.SetISOWeekDate(2008, 39, 5);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2012-05-24 is 2012-W21-4\n";
    ymd.SetYMD(2012, 5, 24);
    iwd.SetISOWeekDate(2012, 21, 4);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for the time conversions.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestTime() {
    // Negative times are mod 24 since the fraction of the day
    // in the class JulianDateTime is always positive. The
    // Julian Day Number will be adjusted in this class so
    // that the sum of the jdn and fod will always be equivalent
    // to the time expressed as the fraction of the day.

    DTPair<Boolean, String> p = this.MakeHeader("Testing times.", 2);

    DTPair<Boolean, String> p1;

    p.second += "* h=23.7625, 23:45:45\n";
    HMS hms = new HMS(23, 45, 45);
    JulianDateTime time = new JulianDateTime();
    time.SetTime(hms);
    p1 = TestHMS(23.7625, time);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* h=-23.7625, -23:45:45\n";
    hms.hour = -hms.hour;
    time.SetTime(hms);
    p1 = TestHMS(24 - 23.7625, time);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* h=23.7625, 23:45.75\n";
    HM hm = new HM(23, 45.75);
    JulianDateTime time_hm = new JulianDateTime();
    time_hm.SetTime(hm);
    p1 = TestHM(23.7625, time_hm);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* h=-23.7625, -23:45.75\n";
    hm.hour = -hm.hour;
    time_hm.SetTime(hm);
    p1 = TestHM(24 - 23.7625, time_hm);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* h=00.7625, 00:45.75\n";
    hm.hour = 0;
    hm.minute = 45.75;
    time_hm.SetTime(hm);
    p1 = TestHM(0.7625, time_hm);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* h=-00.7625, -00:45.75\n";
    hm.minute = -hm.minute;
    time_hm.SetTime(hm);
    p1 = TestHM(24.0 - 0.7625, time_hm);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for testing the time zone.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestTimeZone() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing time zones.", 2);

    DTPair<Boolean, String> p1 = new DTPair<>(true, null);

    // Set up the timezone and the alternate one.
    TimeZones timeZones = new TimeZones();
    Map<String, Integer> tzNames = timeZones.GetTimeZonesByName();

    p.second += "* UT0, UT+10\n";
    // Time zone name, difference from UT 0 in seconds.
    DTPair<String, Double> TZ = new DTPair<>(null, null);
    TZ.first = "UT";
    // Alternate time zone name, difference from UT 0 in seconds.
    DTPair<String, Double> altTZ = new DTPair<>(null, null);
    altTZ.first = "UT+10";
    TZ.second = tzNames.get(TZ.first) / 3600.0;
    altTZ.second = tzNames.get(altTZ.first) / 3600.0;
    YMDHMS utc = new YMDHMS();
    YMDHMS expected_lt = new YMDHMS();
    utc.SetYMDHMS(2012, 1, 2, 23, 46, 0.0000000001);
    JulianDateTime utcjd = new JulianDateTime();
    utcjd.SetDateTime(utc, CALENDAR_TYPE.JULIAN_GREGORIAN);
    JulianDateTime localjd = new JulianDateTime(utcjd);
    localjd.inc(altTZ.second / 24);
    YMDHMS lt = localjd.GetDateTime(CALENDAR_TYPE.JULIAN_GREGORIAN);
    expected_lt.SetYMDHMS(2012, 1, 3, 9, 46, 0.0000000001);
    // 0.1ns is the best we can expect.
    if (expected_lt.IsClose(lt, lt.rel_seconds, lt.abs_seconds)) {
      p1.first = true;
      p1.second = "<br>Passed.\n";
    } else {
      p1.first = false;
      p1.second = String.format("<br>Fail got: %s,  expected %s.\n", lt, expected_lt);
    }
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* UT0, UT-09:30\n";
    TZ.first = "UT";
    altTZ.first = "UT-09:30";
    TZ.second = tzNames.get(TZ.first) / 3600.0;
    altTZ.second = tzNames.get(altTZ.first) / 3600.0;
    utc.SetYMDHMS(2012, 1, 2, 23, 46, 0.0000000001);
    utcjd.SetDateTime(utc, CALENDAR_TYPE.JULIAN_GREGORIAN);
    localjd = utcjd;
    localjd.inc(altTZ.second / 24);
    lt = localjd.GetDateTime(CALENDAR_TYPE.JULIAN_GREGORIAN);
    expected_lt.SetYMDHMS(2012, 1, 2, 14, 16, 0.0000000001);
    if (expected_lt.IsClose(lt, lt.rel_seconds, lt.abs_seconds)) {
      p1.first = true;
      p1.second = "<br>Passed.\n";
    } else {
      p1.first = false;
      p1.second = String.format("<br>Fail got: %s,  expected %s.\n", lt, expected_lt);
    }
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for Julian date setting and getting.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestJulianDates() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing Julian Date Set/Get.", 2);

    DTPair<Boolean, String> p1 = new DTPair<>(true, null);

    p.second += "* Modified JD (2400000.5).\n";
    JD ref = new JD(2400000, 0.5);
    JulianDateTime jd = new JulianDateTime(2400000, 0.5);
    JD jdfp = jd.GetJDFullPrecision();
    double jdshort = jd.GetJDDouble();
    if (jdfp.eq(ref) && jdshort == 2400000.5) {
      p1.first = true;
      p1.second = "<br>Passed.\n";
    } else {
      p1.first = false;
      p1.second = String.format("<br>Failed expected 2400000.5, got GetJDFullPrecision() (%s) and GetJD() %.8f\n",
          jdfp, jdshort);
    }
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "Truncated JD (2440000.5).\n";
    ref = new JD(2440000, 0.5);
    jd = new JulianDateTime(2440000, 0.5);
    jdfp = jd.GetJDFullPrecision();
    jdshort = jd.GetJDDouble();
    if (jdfp.eq(ref) && jdshort == 2440000.5) {
      p1.first = true;
      p1.second = "<br>Passed.\n";
    } else {
      p1.first = false;
      p1.second = String.format("<br>Failed Expected 2440000.5, got GetJDFullPrecision() (%s) and GetJD() %.8f\n",
          jdfp, jdshort);
    }
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* Dublin JD (2415020).\n";
    ref = new JD(2415020, 0.0);
    jd = new JulianDateTime(2415020, 0.0);
    jdfp = jd.GetJDFullPrecision();
    jdshort = jd.GetJDDouble();
    if (jdfp.eq(ref) && jdshort == 2415020) {
      p1.first = true;
      p1.second = "<br>Passed.\n";
    } else {
      p1.first = false;
      p1.second = String.format("<br>Failed Expected 2415020, got GetJDFullPrecision() (%s) and GetJD() %.8f\n",
          jdfp, jdshort);
    }
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

}
