/*=========================================================================

  Program:   Date Time Library
  File   :   YMDHMSTests.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
package dtTests;

import dt.*;

import java.util.ArrayList;

/**
 * A test harness to test the YMDHMS class.
 *
 * @author Andrew J. P. Maclean
 */
public class YMDHMSTests extends CommonTests {

  private final int year = 2013;
  private final int month = 1;
  private final int day = 1;
  private final int hour = 23;
  private final int minute = 59;
  private final double second = 30;
  private final YMDHMS ymdhms = new YMDHMS(this.year, this.month, this.day, this.hour, this.minute, this.second);

  /**
   * Run all the tests.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * string of failed tests.
   */
  public DTPair<Boolean, String> TestAll() {
    ArrayList<DTPair<Boolean, String>> messages = new ArrayList<>();

    messages.add(this.TestConstructors());
    messages.add(this.TestSetGet());
    messages.add(this.TestComparisonOperators());
    messages.add(this.TestAssignment());
    messages.add(this.TestStringOutput());

    return this.AggregateResults("YMDHMS Tests.", messages);

  }

  /**
   * Run the tests for the constructors.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestConstructors() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing constructors.", 2);

    p.second += "* YMDHMS() ";

    DTPair<Boolean, String> p1;

    YMDHMS y1 = new YMDHMS();
    p1 = this.PassFail(y1.ymd.year == -4712 && y1.ymd.month == 1 && y1.ymd.day == 1 && y1.hms.hour == 0
        && y1.hms.minute == 0 && y1.hms.second == 0);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* YMDHMS(y, mon, d, h, min, s) ";
    y1 = new YMDHMS(this.year, this.month, this.day, this.hour, this.minute, this.second);
    p1 = this.PassFail(y1.ymd.year == this.year && y1.ymd.month == this.month && y1.ymd.day == this.day
        && y1.hms.hour == this.hour && y1.hms.minute == this.minute && y1.hms.second == this.second);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* YMDHMS(ymd, hms) ";
    YMD ymd = new YMD(this.year, this.month, this.day);
    HMS hms = new HMS(this.hour, this.minute, this.second);
    YMDHMS y2 = new YMDHMS(ymd, hms);
    p1 = this.PassFail(y2.ymd.year == this.year && y2.ymd.month == this.month && y2.ymd.day == this.day
        && y2.hms.hour == this.hour && y2.hms.minute == this.minute && y2.hms.second == this.second);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* YMDHMS(ymdhms) ";
    YMDHMS y3 = new YMDHMS(y1);
    p1 = this.PassFail(y3.ymd.year == this.year && y3.ymd.month == this.month && y3.ymd.day == this.day
        && y3.hms.hour == this.hour && y3.hms.minute == this.minute && y3.hms.second == this.second);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for setting and getting the members.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestSetGet() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing setting and getting members.", 2);

    p.second += "* SetYMDHMS(y ,m, d, h, min, s) ";

    DTPair<Boolean, String> p1;

    YMDHMS y1 = new YMDHMS(this.year, this.month, this.day, this.hour, this.minute, this.second);
    YMDHMS y2 = new YMDHMS();
    y2.SetYMDHMS(this.year, this.month, this.day, this.hour, this.minute, this.second);
    p1 = this.PassFail(y1.ymd.year == y2.ymd.year && y1.ymd.month == y2.ymd.month && y1.ymd.day == y2.ymd.day
        && y1.hms.hour == y2.hms.hour && y1.hms.minute == y2.hms.minute && y1.hms.second == y2.hms.second);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* SetYMDHMS(ymd, hms) ";
    YMD ymd = new YMD(this.year, this.month, this.day);
    HMS hms = new HMS(this.hour, this.minute, this.day);
    y2.SetYMDHMS(ymd, hms);
    p1 = this.PassFail(y2.ymd.year == ymd.year && y2.ymd.month == ymd.month && y2.ymd.day == ymd.day
        && y2.hms.hour == hms.hour && y2.hms.minute == hms.minute && y2.hms.second == hms.second);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* SetYMDHMS(ymdhms) ";
    y2.SetYMDHMS(y1);
    p1 = this.PassFail(y1.ymd.year == y2.ymd.year && y1.ymd.month == y2.ymd.month && y1.ymd.day == y2.ymd.day
        && y1.hms.hour == y2.hms.hour && y1.hms.minute == y2.hms.minute && y1.hms.second == y2.hms.second);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* GetYMDHMS() ";
    y2 = y1.GetYMDHMS();
    p1 = this.PassFail(y1.ymd.year == y2.ymd.year && y1.ymd.month == y2.ymd.month && y1.ymd.day == y2.ymd.day
        && y1.hms.hour == y2.hms.hour && y1.hms.minute == y2.hms.minute && y1.hms.second == y2.hms.second);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* SetYMD(ymd) ";
    y2.SetYMD(ymd);
    p1 = this.PassFail(y2.ymd.year == ymd.year && y2.ymd.month == ymd.month && y2.ymd.day == ymd.day);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* GetYMD() ";
    ymd = y1.GetYMD();
    p1 = this.PassFail(y1.ymd.year == ymd.year && y1.ymd.month == ymd.month && y1.ymd.day == ymd.day);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* SetHMS(hms) ";
    y2.SetHMS(hms);
    p1 = this.PassFail(y1.hms.hour == hms.hour && y1.hms.minute == hms.minute && y1.hms.second == hms.second);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* GetHMS() ";
    hms = y1.GetHMS();
    p1 = this.PassFail(y1.hms.hour == hms.hour && y1.hms.minute == hms.minute && y1.hms.second == hms.second);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* GetHM() ";
    HM hm = y1.GetHM();
    p1 = this.PassFail(hm.hour == hms.hour && hm.minute == hms.minute + hms.second / 60.0);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for the comparison operators.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestComparisonOperators() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing comparison operators.", 2);

    DTPair<Boolean, String> p1;

    p.second += "* == ";
    YMDHMS y1 = new YMDHMS(this.year, this.month, this.day, this.hour, this.minute, this.second);
    p1 = this.PassFail(y1.eq(this.ymdhms));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* &asymp; ";
    YMDHMS y2 = new YMDHMS(this.year, this.month, this.day, this.hour, this.minute, this.second + 0.01);
    p1 = this.PassFail(this.ymdhms.IsClose(y2, 0.001));
    p.first &= p1.first;
    p.second += "\n<br>" + p1.second;

    p1 = this.PassFail(!this.ymdhms.IsClose(y2, 0.0001));
    p.first &= p1.first;
    p.second += "<br>" + p1.second;

    p.second += "* != ";
    y1.ymd.day -= 1;
    p1 = this.PassFail(y1.ne(this.ymdhms));
    p.first &= p1.first;
    p.second += p1.second;

    YMDHMS a = new YMDHMS(1992, 7, 2, 12, 58, 59.8);
    YMDHMS b = new YMDHMS(1993, 7, 2, 12, 58, 59.8);
    YMDHMS c = new YMDHMS(1992, 8, 2, 12, 58, 59.8);
    YMDHMS d = new YMDHMS(1992, 7, 3, 12, 58, 59.8);
    YMDHMS e = new YMDHMS(1992, 7, 2, 13, 58, 59.8);
    YMDHMS f = new YMDHMS(1992, 7, 2, 12, 59, 59.8);
    YMDHMS g = new YMDHMS(1992, 7, 2, 12, 58, 59.9);

    p.second += "* &lt; and &le; ";
    p1 = this.PassFail(a.lt(b) && a.lt(c) && a.lt(d) && a.lt(e) && a.lt(f) && a.lt(g) && a.le(b) && a.le(c)
        && a.le(d) && a.le(e) && a.le(f) && a.le(g) && a.le(a) && !(a.gt(b)) && !(a.ge(b)));
    p.first &= p1.first;
    p.second += p1.second;

    YMDHMS h = new YMDHMS(1992, 7, 2, 12, 59, 59.9);
    YMDHMS i = new YMDHMS(1991, 7, 2, 12, 59, 59.9);
    YMDHMS j = new YMDHMS(1992, 6, 2, 12, 59, 59.9);
    YMDHMS k = new YMDHMS(1992, 7, 1, 12, 59, 59.9);
    YMDHMS l = new YMDHMS(1992, 7, 2, 11, 59, 59.9);
    YMDHMS m = new YMDHMS(1992, 7, 2, 12, 58, 59.9);
    YMDHMS n = new YMDHMS(1992, 7, 2, 12, 58, 59.8);

    p.second += "* &gt; and &ge; ";
    p1 = this.PassFail(h.gt(i) && h.gt(j) && h.gt(k) && h.gt(l) && h.gt(m) && h.gt(n) && h.ge(i) && h.ge(j)
        && h.ge(k) && h.ge(l) && h.ge(m) && h.ge(n) && h.ge(h) && !(h.lt(i)) && !(h.le(i)));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for assignment.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestAssignment() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing assignment.", 2);

    p.second += "* = ";
    YMDHMS y1 = this.ymdhms;
    DTPair<Boolean, String> p1;
    p1 = this.PassFail(y1.ymd.year == this.ymdhms.ymd.year && y1.ymd.month == this.ymdhms.ymd.month
        && y1.ymd.day == this.ymdhms.ymd.day && y1.hms.hour == this.ymdhms.hms.hour
        && y1.hms.minute == this.ymdhms.hms.minute && y1.hms.second == this.ymdhms.hms.second);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for string output.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestStringOutput() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing GetString()", 2);

    String s1 = this.ymdhms.GetString();
    DTPair<Boolean, String> p1;
    String valueStr = "2013-01-01  23:59:30.000000000000";
    p1 = this.Compare(s1, valueStr, "* ");
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

}
