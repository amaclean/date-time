/*=========================================================================

  Program:   Date Time Library
  File   :   TimeZonesTests.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
package dtTests;

import dt.DTPair;
import dt.TimeZones;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * A test harness to test the TimeZones class.
 *
 * @author Andrew J. P. Maclean
 */
public class TimeZonesTests extends CommonTests {

  private final TimeZones tz = new TimeZones();

  /**
   * Run all the tests.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * string of failed tests.
   */
  public DTPair<Boolean, String> TestAll() {
    ArrayList<DTPair<Boolean, String>> messages = new ArrayList<>();

    messages.add(this.TestConstructors());
    messages.add(this.TestAssignment());
    messages.add(this.TestGet());
    messages.add(this.TestComparisonOperators());
    messages.add(this.TestTimeZones());

    return this.AggregateResults("TimeZones Tests.", messages);
  }

  /**
   * Run the tests for testing constructors.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestConstructors() {
    DTPair<Boolean, String> p = this.MakeHeader("Testing constructors.", 2);

    p.second += "* pTimeZones() ";
    TimeZones tz1 = new TimeZones(this.tz);
    DTPair<Boolean, String> p1 = this.PassFail(tz1.eq(this.tz));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";
    return p;
  }

  /**
   * Run the tests for testing assignemt.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestAssignment() {
    DTPair<Boolean, String> p = this.MakeHeader("Testing assignment.", 2);

    p.second += "* = ";
    TimeZones tz1 = new TimeZones(this.tz);
    DTPair<Boolean, String> p1 = this.PassFail(tz1.eq(this.tz));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";
    return p;
  }

  /**
   * Run the tests for testing getting members.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestGet() {
    DTPair<Boolean, String> p = this.MakeHeader("Testing getting members.", 2);

    p.second += "* GetTimeZoneName() ";
    String name = "UT";
    DTPair<Boolean, String> nameRes = this.tz.GetTimeZoneName(0);
    DTPair<Boolean, String> p1 = this.PassFail(Objects.equals(name, nameRes.second));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* GetTimeZoneName() ";
    // Expecting a fail.
    nameRes = this.tz.GetTimeZoneName(15 * 3600);
    p1 = this.PassFail(!nameRes.first);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* GetTimeZoneValue() ";
    int value = 0;
    DTPair<Boolean, Integer> valueRes = this.tz.GetTimeZoneValue(name);
    p1 = this.PassFail(value == valueRes.second);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* GetTimeZoneValue() ";
    // Expecting a fail.
    valueRes = this.tz.GetTimeZoneValue("FIIK");
    p1 = this.PassFail(!valueRes.first);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";
    return p;
  }

  /**
   * Run the tests for testing comparison operators.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestComparisonOperators() {
    DTPair<Boolean, String> p = this.MakeHeader("Testing comparison operators.", 2);

    p.second += "* == ";
    TimeZones tz1 = new TimeZones(this.tz);
    DTPair<Boolean, String> p1 = this.PassFail(tz1.eq(this.tz));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* != ";
    p1 = this.PassFail(!(tz1.ne(this.tz)));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";
    return p;
  }

  /**
   * Run the tests for testing the time zones.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestTimeZones() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing time zones.", 2);

    DTPair<Boolean, String> p1 = new DTPair<>(true, "");

    p.second += "* Size of the time zone maps.\n";
    Map<String, Integer> TZMap = new HashMap<>(tz.GetTimeZonesByName());
    Map<Integer, String> invTZMap = new HashMap<>(tz.GetTimeZonesByValue());
    if (!TZMap.isEmpty() && (TZMap.size() == invTZMap.size())) {
      p1.first = true;
      p1.second = "<br>Passed.\n";
    } else {
      p1.first = false;
      p1.second = String.format("<br>Failed: %s%s%s%d and %d respectively.\n",
          "The maps returned by GetTimeZonesByName() and ",
          "GetTimeZonesByValue() should be the same size and > 0.", "\n<br>The sizes are: ", TZMap.size(),
          invTZMap.size());
    }
    p.first &= p1.first;
    p.second += p1.second;

    StringBuilder fail = new StringBuilder();
    boolean ok = true;
    if (TZMap.size() == invTZMap.size() && !TZMap.isEmpty()) {
      p.second += "* Indexing of the time zone maps.\n";
      for (Map.Entry<String, Integer> entry : TZMap.entrySet()) {
        String key = entry.getKey();
        int value = entry.getValue();
        if (!Objects.equals(invTZMap.get(value), key)) {
          ok = false;
          fail.append(String.format("  - (%s,  %s)\n", key, invTZMap.get(value)));
        }
      }
      if (ok) {
        p1.first = true;
        p1.second = "<br>Passed.\n";
      } else {
        p1.first = false;
        p1.second = String.format("<br>Failed: here is a list of (Expected Index, Actual Index):\n%s", fail.toString());
      }
      p.first &= p1.first;
      p.second += p1.second;

    }

    p.second += "\n";

    return p;
  }

}
