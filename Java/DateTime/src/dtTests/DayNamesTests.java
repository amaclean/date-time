/*=========================================================================

  Program:   Date Time Library
  File   :   DayNamesTests.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
package dtTests;

import dt.DTPair;
import dt.DayNames;

import java.util.ArrayList;

/**
 * A test harness to test the DayNames class.
 *
 * @author Andrew J. P. Maclean
 */
public class DayNamesTests extends CommonTests {

  private final DayNames dn = new DayNames();

  /**
   * Run all the tests.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * string of failed tests.
   */
  public DTPair<Boolean, String> TestAll() {
    ArrayList<DTPair<Boolean, String>> messages = new ArrayList<>();

    messages.add(this.TestConstructors());
    messages.add(this.TestAssignment());
    messages.add(this.TestComparisonOperators());
    messages.add(this.TestDayNames());

    return this.AggregateResults("DayNames Tests.", messages);

  }

  /**
   * Run the tests for the constructors.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestConstructors() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing constructors.", 2);

    p.second += "* DayNames() ";
    DayNames m1 = new DayNames(this.dn);
    DTPair<Boolean, String> p1 = this.PassFail(m1.eq(dn));
    p.first &= p1.first;
    p.second += "<br>" + p1.second;

    p.second += "\n";
    return p;
  }

  /**
   * Run the tests for the assignment.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestAssignment() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing assignment.", 2);

    p.second += "* = ";
    DayNames m1 = new DayNames();
    m1.SetDayName(1, "99th day!");
    m1.assign(this.dn);
    DTPair<Boolean, String> p1 = this.PassFail(m1.eq(dn));
    p.first &= p1.first;
    p.second += "<br>" + p1.second;

    p.second += "\n";
    return p;
  }

  /**
   * Run the tests for the comparison operators.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestComparisonOperators() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing comparison operators.", 2);

    p.second += "* == ";
    DayNames m1 = new DayNames(this.dn);
    DTPair<Boolean, String> p1 = this.PassFail(m1.eq(dn));
    p.first &= p1.first;
    p.second += "<br>" + p1.second;

    p.second += "* != ";
    m1.SetDayName(1, "FIIK");
    p1 = this.PassFail(m1.ne(dn));
    p.first &= p1.first;
    p.second += "<br>" + p1.second;

    p.second += "\n";
    return p;
  }

  /**
   * Run the tests for day names.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestDayNames() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing day names.", 2);

    p.second += "* Day names and abbreviations.\n";
    p.first = true;
    StringBuilder fail = new StringBuilder();
    boolean ok = true;
    DTPair<Boolean, String> p1 = new DTPair<>(true, null);

    int abbrevLength = 3;
    for (int i = 1; i < 8; ++i) {
      String name = this.dn.GetDayName(i);
      String abbrev = this.dn.GetAbbreviatedDayName(i, abbrevLength);
      if (!name.substring(0, abbrevLength).equals(abbrev)) {
        ok &= false;
        fail.append("  - (").append(name).append(", ").append(abbrev).append(")\n");
      }
    }
    if (ok) {
      p1.first = true;
      p1.second = "<br>Passed.\n";
    } else {
      p1.first = false;
      p1.second = String.format("<br>Failed day names:\n%s\n", fail.toString());
    }
    p.first &= p1.first;
    p.second += p1.second;

    ok = true;
    fail = new StringBuilder();
    p.second += "* Day names and unabbreviated names.\n";
    for (int i = 1; i < 8; ++i) {
      String name = this.dn.GetDayName(i);
      String abbrev = this.dn.GetAbbreviatedDayName(i, name.length());
      if (!name.equals(abbrev)) {
        ok &= false;
        fail.append("  - (").append(name).append(", ").append(abbrev).append(")\n");
      }
    }
    if (ok) {
      p1.first = true;
      p1.second = "<br>Passed.\n";
    } else {
      p1.first = false;
      p1.second = String.format("<br>Failed day names:\n%s\n", fail.toString());
    }
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* Day Name range error.\n";
    p1 = this.PassFail(this.dn.GetDayName(0).isEmpty() || this.dn.GetDayName(8).isEmpty());
    p.first &= p1.first;
    p.second += "<br>" + p1.second;

    p.second += "* Abbreviated Day Name range error.\n";
    p1 = this.PassFail(this.dn.GetAbbreviatedDayName(0, abbrevLength).isEmpty()
        || this.dn.GetAbbreviatedDayName(8, abbrevLength).isEmpty());
    p.first &= p1.first;
    p.second += "<br>" + p1.second;

    p.second += "* Set Day Name.\n";
    String d = this.dn.GetDayName(5);
    this.dn.SetDayName(5, "Day 5");
    p1 = this.PassFail(this.dn.GetDayName(5).equals("Day 5"));
    this.dn.SetDayName(5, d);
    p.first &= p1.first;
    p.second += "<br>" + p1.second;

    p.second += "\n";
    return p;
  }

}
