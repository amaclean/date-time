/*=========================================================================

  Program:   Date Time Library
  File   :   DateTimeConversionsTests.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
package dtTests;

import dt.*;

import java.util.ArrayList;

/**
 * A test harness to test the DateTimeConversions class.
 *
 * @author Andrew J. P. Maclean
 */
public class DateTimeConversionsTests extends CommonTests {

  private final DateTimeConversions dtc = new DateTimeConversions();
  private final DateTimeBase dtb = new DateTimeBase();

  /**
   * Run all the tests.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * string of failed tests.
   */
  public DTPair<Boolean, String> TestAll() {
    ArrayList<DTPair<Boolean, String>> messages = new ArrayList<>();

    messages.add(this.TestProlepticJulianCalendar());
    messages.add(this.TestProlepticGregorianCalendar());
    messages.add(this.TestJulianGregorianCalendar());

    messages.add(this.TestLeapYear());
    messages.add(this.TestDayOfTheYear());
    messages.add(this.TestDayOfTheWeek());

    messages.add(this.TestISOWeekDate());

    messages.add(this.TestTime());

    return this.AggregateResults("DateTimeConversions Tests.", messages);
  }

  /**
   * Run the proleptic Julian calendar tests for some specified dates.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestProlepticJulianCalendar() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing Proleptic Julian, Julian calendar.", 2);

    YMD date = new YMD();
    DTPair<Boolean, String> p1;

    p.second += "* Jd=0, -4712 01 01\n";
    date.SetYMD(-4712, 1, 1);
    p1 = TestJulian(0, date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* Jd=1721058. Jd=0, 0 01 01 (The first day of 1 B.C.)\n";
    date.SetYMD(0, 1, 1);
    p1 = TestJulian(1721058, date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* Julian/Gregorian date changeover (last Julian day). Jd=2299160, 1582 10 04.\n";
    date.SetYMD(1582, 10, 4);
    p1 = TestJulian(JG_CHANGEOVER.JULIAN_PREVIOUS_DN.getValue(), date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* Julian/Gregorian date changeover (first Gregorian day). Jd= 2299171, 1582 10 15.\n";
    date.SetYMD(1582, 10, 15);
    p1 = TestJulian(2299171, date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* Modified Julian Day 0.0. Jd= 2400013, 1858 11 17.\n";
    date.SetYMD(1858, 11, 17);
    p1 = TestJulian(2400013, date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* Jd=2451545, 1999 12 19.\n";
    date.SetYMD(1999, 12, 19);
    p1 = TestJulian(2451545, date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the proleptic Gregorian calendar tests for some specified dates.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestProlepticGregorianCalendar() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing Proleptic Gregorian, Gregorian calendar.", 2);

    YMD date = new YMD();
    DTPair<Boolean, String> p1;

    p.second += "* Jd=0, -4713 11 24\n";
    date.SetYMD(-4713, 11, 24);
    p1 = TestGregorian(0, date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* Jd=1721060. Jd=0, 0 01 01 (The first day of 1 B.C.)\n";
    date.SetYMD(0, 1, 1);
    p1 = TestGregorian(1721060, date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* Julian/Gregorian date changeover (last Julian day). Jd=2299150, 1582 10 04.\n";
    date.SetYMD(1582, 10, 4);
    p1 = TestGregorian(JG_CHANGEOVER.GREGORIAN_PREVIOUS_DN.getValue(), date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* Julian/Gregorian date changeover (first Gregorian day). Jd=2299161, 1582 10 15.\n";
    date.SetYMD(1582, 10, 15);
    p1 = TestGregorian(JG_CHANGEOVER.GREGORIAN_START_DN.getValue(), date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* Modified Julian Day 0.0. Jd= 2400001, 1858 11 17.\n";
    date.SetYMD(1858, 11, 17);
    p1 = TestGregorian(2400001, date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* Jd=2451545, 2000 01 01.\n";
    date.SetYMD(2000, 1, 1);
    p1 = TestGregorian(2451545, date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the Julian/Gregorian tests for some specified dates.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestJulianGregorianCalendar() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing Proleptic Julian, Julian, Gregorian calendar.", 2);

    YMD date = new YMD();
    DTPair<Boolean, String> p1;

    p.second += "* Jd=0, -4712 01 01\n";
    date.SetYMD(-4712, 1, 1);
    p1 = TestJulianGregorian(0, date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* Jd=1721058. Jd=0, 0 01 01 (The first day of 1 B.C.)\n";
    date.SetYMD(0, 1, 1);
    p1 = TestJulianGregorian(1721058, date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* Julian/Gregorian date changeover (last Julian day). Jd=2299160, 1582 10 04.\n";
    date.SetYMD(1582, 10, 4);
    p1 = TestJulianGregorian(JG_CHANGEOVER.JULIAN_PREVIOUS_DN.getValue(), date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* Julian/Gregorian date changeover (first Gregorian day). Jd=2299161, 1582 10 15.\n";
    date.SetYMD(1582, 10, 15);
    p1 = TestJulianGregorian(JG_CHANGEOVER.GREGORIAN_START_DN.getValue(), date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* Modified Julian Day 0.0. Jd= 2400001, 1858 11 17.\n";
    date.SetYMD(1858, 11, 17);
    p1 = TestJulianGregorian(2400001, date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* Jd=2451545, 2000 01 01.\n";
    date.SetYMD(2000, 1, 1);
    p1 = TestJulianGregorian(2451545, date);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Test the conversion of calendar date to julian day. The Julian Proleptic
   * Calendar is used.
   *
   * @param jd  The expected julian day for the given year, month and day.
   * @param ymd The date.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestCD2JDJ(int jd, YMD ymd) {
    int tjd = this.dtc.cd2jdJ(ymd);
    return this.Compare(tjd, jd, "<br>cd2jdJ");
  }

  /**
   * Test the conversion of julian day to calendar date. The Julian Proleptic
   * Calendar is used.
   *
   * @param jd  The julian day.
   * @param ymd The date.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestJD2CDJ(int jd, YMD ymd) {
    YMD ymd1 = new YMD(this.dtc.jd2cdJ(jd));
    return this.Compare(ymd1, ymd, "<br>jd2cdJ");
  }

  /**
   * Test the conversion of calendar date to julian day and vice versa. The
   * Julian Proleptic Calendar is used.
   *
   * @param jd  The julian day.
   * @param ymd The date.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestJulian(int jd, YMD ymd) {
    DTPair<Boolean, String> p1 = TestCD2JDJ(jd, ymd);
    DTPair<Boolean, String> p2 = TestJD2CDJ(jd, ymd);
    return new DTPair<>(p1.first && p2.first, p1.second + p2.second);
  }

  /**
   * Test the conversion of calendar date to julian day. The Gregorian
   * Proleptic Calendar is used.
   *
   * @param jd  The expected julian day for the given year, month and day.
   * @param ymd The date.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestCD2JDG(int jd, YMD ymd) {
    int tjd = this.dtc.cd2jdG(ymd);
    return this.Compare(tjd, jd, "<br>cd2jdG");
  }

  /**
   * Test the conversion of julian day to calendar date. The Gregorian
   * Proleptic Calendar is used.
   *
   * @param jd  The julian day.
   * @param ymd The date.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestJD2CDG(int jd, YMD ymd) {
    YMD ymd1 = new YMD(this.dtc.jd2cdG(jd));
    return this.Compare(ymd1, ymd, "<br>jd2cdG");
  }

  /**
   * Test the conversion of calendar date to julian day and vice versa. The
   * Gregorian Proleptic Calendar is used.
   *
   * @param jd  The julian day.
   * @param ymd The date.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestGregorian(int jd, YMD ymd) {
    DTPair<Boolean, String> p1 = TestCD2JDG(jd, ymd);
    DTPair<Boolean, String> p2 = TestJD2CDG(jd, ymd);
    return new DTPair<>(p1.first && p2.first, p1.second + p2.second);
  }

  /**
   * Test the conversion of calendar date to julian day. The Gregorian or
   * Julian Calendar is used as appropriate.
   *
   * @param jd  The expected julian day for the given year, month and day.
   * @param ymd The date.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestCD2JDJG(int jd, YMD ymd) {
    int tjd = this.dtc.cd2jdJG(ymd);
    return this.Compare(tjd, jd, "<br>cd2jdJG");
  }

  /**
   * Test the conversion of julian day to calendar date. The Gregorian or
   * Julian Calendar is used as appropriate.
   *
   * @param jd  The julian day.
   * @param ymd The date.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestJD2CDJG(int jd, YMD ymd) {
    YMD ymd1 = new YMD(this.dtc.jd2cdJG(jd));
    return this.Compare(ymd1, ymd, "<br>jd2cdJG");
  }

  /**
   * Test the conversion of calendar date to julian day and vice versa. The
   * Gregorian or Julian Calendar is used as appropriate.
   *
   * @param jd  The julian day for the given year, month and day.
   * @param ymd The date.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestJulianGregorian(int jd, YMD ymd) {
    DTPair<Boolean, String> p1 = TestCD2JDJG(jd, ymd);
    DTPair<Boolean, String> p2 = TestJD2CDJG(jd, ymd);
    return new DTPair<>(p1.first && p2.first, p1.second + p2.second);
  }

  /**
   * Run the tests for leap years.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestLeapYear() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing leap year.", 2);

    DTPair<Boolean, String> p1 = new DTPair<>(null, null);

    p.second += "* 1500\n";
    int year = 1500;
    if (this.dtc.isLeapYear(year, CALENDAR_TYPE.JULIAN)
        && !this.dtc.isLeapYear(year, CALENDAR_TYPE.GREGORIAN)
        && this.dtc.isLeapYear(year, CALENDAR_TYPE.JULIAN_GREGORIAN)) {
      p1.first = true;
      p1.second = "<br>Passed.\n";
    } else {
      p1.first = false;
      p1.second = String.format("<br>Fail %d is a only a leap year for the Julian calendar.\n", year);
    }
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* 1900\n";
    year = 1900;
    if (this.dtc.isLeapYear(year, CALENDAR_TYPE.JULIAN)
        && !this.dtc.isLeapYear(year, CALENDAR_TYPE.GREGORIAN)
        && !this.dtc.isLeapYear(year, CALENDAR_TYPE.JULIAN_GREGORIAN)) {
      p1.first = true;
      p1.second = "<br>Passed.\n";
    } else {
      p1.first = false;
      p1.second = String.format("<br>Fail %d is a only a leap year for the Julian calendar.\n", year);
    }
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* 2000\n";
    year = 2000;
    if (this.dtc.isLeapYear(year, CALENDAR_TYPE.JULIAN)
        && this.dtc.isLeapYear(year, CALENDAR_TYPE.GREGORIAN)
        && this.dtc.isLeapYear(year, CALENDAR_TYPE.JULIAN_GREGORIAN)) {
      p1.first = true;
      p1.second = "<br>Passed.\n";
    } else {
      p1.first = false;
      p1.second = String.format("<br>Fail %d is a leap year for the Julian and Gregorian calendars.\n", year);
    }
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* 2001\n";
    year = 2001;
    if (!this.dtc.isLeapYear(year, CALENDAR_TYPE.JULIAN)
        && !this.dtc.isLeapYear(year, CALENDAR_TYPE.GREGORIAN)
        && !this.dtc.isLeapYear(year, CALENDAR_TYPE.JULIAN_GREGORIAN)) {
      p1.first = true;
      p1.second = "<br>Passed.\n";
    } else {
      p1.first = false;
      p1.second = String.format("<br>Fail %d is not a leap year.\n", year);
    }
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for testing the day of the year.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestDayOfTheYear() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing Day of the Year.", 2);

    p.second += "* 1900.\n";
    YMD ymd = new YMD(1900, 12, 31);
    int doyJ = this.dtc.doyJ(ymd);
    DTPair<Boolean, String> p1 = this.Compare(doyJ, 366, "<br>doyJ()");
    p.first &= p1.first;
    p.second += p1.second;

    // Not a leap year for the Gregorian Calendar.
    int doyG = this.dtc.doyG(ymd);
    p1 = this.Compare(doyG, 365, "<br>doyG()");
    p.first &= p1.first;
    p.second += p1.second;

    int doyJG = this.dtc.doyJG(ymd);
    p1 = this.Compare(doyJG, 365, "<br>doyJG()");
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Test the day of the week for some dates.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestDayOfTheWeek() {
    // Note: For dow calculations we use the Julian Calendar for
    // dates less that 1582-10-15 and the Gregorian Calendar
    // for days on or after that date.

    DTPair<Boolean, String> p = this.MakeHeader("Testing the day of the week.", 2);

    int dow1, dow2;
    p.second += "* -4712 01 01 Monday (Julian)\n";
    YMD ymd = new YMD(); // year = -4712, month = 1, day = 1 by default.
    dow1 = this.dtc.dow(ymd, CALENDAR_TYPE.JULIAN, FIRST_DOW.SUNDAY);
    dow2 = this.dtc.dow(ymd, CALENDAR_TYPE.JULIAN, FIRST_DOW.MONDAY);
    if (dow1 != 2 && dow2 != 1) {
      p.first &= false;
      p.second += String.format("<br>Fail got: %d and %d expected: 2 and 1 (Monday).\n", dow1, dow2);
    } else {
      p.second += "<br>Passed.\n";
    }

    p.second += "* -4713 11 24 Friday (Gregorian)\n";
    ymd.SetYMD(-4713, 11, 24);
    dow1 = this.dtc.dow(ymd, CALENDAR_TYPE.JULIAN, FIRST_DOW.SUNDAY);
    dow2 = this.dtc.dow(ymd, CALENDAR_TYPE.JULIAN, FIRST_DOW.MONDAY);
    if (dow1 != 6 && dow2 != 5) {
      p.first &= false;
      p.second += String.format("<br>Fail got: %d and %d expected: 6 and 5 (Friday).\n", dow1, dow2);
    } else {
      p.second += "<br>Passed.\n";
    }

    p.second += "* 1582 10 04 Thursday (Julian)\n";
    ymd.SetYMD(1582, 10, 4);
    dow1 = this.dtc.dow(ymd, CALENDAR_TYPE.JULIAN_GREGORIAN, FIRST_DOW.SUNDAY);
    dow2 = this.dtc.dow(ymd, CALENDAR_TYPE.JULIAN_GREGORIAN, FIRST_DOW.MONDAY);
    if (dow1 != 5 && dow2 != 4) {
      p.first &= false;
      p.second += String.format("<br>Fail got: %d and %d expected: 5 and 4 (Thursday).\n", dow1, dow2);
    } else {
      p.second += "<br>Passed.\n";
    }

    p.second += "* 1582 10 15 Friday (Gregorian)\n";
    ymd.SetYMD(1582, 10, 15);
    dow1 = this.dtc.dow(ymd, CALENDAR_TYPE.JULIAN_GREGORIAN, FIRST_DOW.SUNDAY);
    dow2 = this.dtc.dow(ymd, CALENDAR_TYPE.JULIAN_GREGORIAN, FIRST_DOW.MONDAY);
    if (dow1 != 6 && dow2 != 5) {
      p.first &= false;
      p.second += String.format("<br>Fail got: %d and %d expected: 6 and 5 (Friday).\n", dow1, dow2);
    } else {
      p.second += "<br>Passed.\n";
    }

    p.second += "* 1582 10 17 Sunday (Gregorian)\n";
    ymd.SetYMD(1582, 10, 17);
    dow1 = this.dtc.dow(ymd, CALENDAR_TYPE.JULIAN_GREGORIAN, FIRST_DOW.SUNDAY);
    dow2 = this.dtc.dow(ymd, CALENDAR_TYPE.JULIAN_GREGORIAN, FIRST_DOW.MONDAY);
    if (dow1 != 1 && dow2 != 7) {
      p.first &= false;
      p.second += String.format("<br>Fail got: %d and %d expected: 1 and 7 (Sunday).\n", dow1, dow2);
    } else {
      p.second += "<br>Passed.\n";
    }

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for the week of the year conversions.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestISOWeekDate() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing week of the year.", 2);

    YMD ymd = new YMD();
    ISOWeekDate iwd = new ISOWeekDate();
    DTPair<Boolean, String> p1;

    p.second += "* 2005-01-01 is 2004-W53-6\n";
    ymd.SetYMD(2005, 1, 1);
    iwd.SetISOWeekDate(2004, 53, 6);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2005-01-02 is 2004-W53-7\n";
    ymd.SetYMD(2005, 1, 2);
    iwd.SetISOWeekDate(2004, 53, 7);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2005-12-31 is 2005-W52-6\n";
    ymd.SetYMD(2005, 12, 31);
    iwd.SetISOWeekDate(2005, 52, 6);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2007-01-01 is 2007-W01-1 (both years 2007 start with the same day)\n";
    ymd.SetYMD(2007, 1, 1);
    iwd.SetISOWeekDate(2007, 1, 1);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2007-12-30 is 2007-W52-7\n";
    ymd.SetYMD(2007, 12, 30);
    iwd.SetISOWeekDate(2007, 52, 7);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2007-12-31 is 2008-W01-1\n";
    ymd.SetYMD(2007, 12, 31);
    iwd.SetISOWeekDate(2008, 1, 1);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2008-01-01 is 2008-W01-2 (Gregorian year 2008 is a leap year, ISO year 2008 is 2 days shorter: 1 day longer at the start, 3 days shorter at the end)\n";
    ymd.SetYMD(2008, 1, 1);
    iwd.SetISOWeekDate(2008, 1, 2);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2008-12-28 is 2008-W52-7\n";
    ymd.SetYMD(2008, 12, 28);
    iwd.SetISOWeekDate(2008, 52, 7);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2008-12-29 is 2009-W01-1\n";
    ymd.SetYMD(2008, 12, 29);
    iwd.SetISOWeekDate(2009, 1, 1);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2008-12-30 is 2009-W01-2\n";
    ymd.SetYMD(2008, 12, 30);
    iwd.SetISOWeekDate(2009, 1, 2);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2008-12-31 is 2009-W01-3\n";
    ymd.SetYMD(2008, 12, 31);
    iwd.SetISOWeekDate(2009, 1, 3);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2009-01-01 is 2009-W01-4\n";
    ymd.SetYMD(2009, 1, 1);
    iwd.SetISOWeekDate(2009, 1, 4);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2009-12-31 is 2009-W53-4 (ISO year 2009 has 53 weeks, extending the Gregorian year 2009, which starts and ends with Thursday, at both ends with three days)\n";
    ymd.SetYMD(2009, 12, 31);
    iwd.SetISOWeekDate(2009, 53, 4);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2010-01-01 is 2009-W53-5\n";
    ymd.SetYMD(2010, 1, 1);
    iwd.SetISOWeekDate(2009, 53, 5);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2010-01-02 is 2009-W53-6\n";
    ymd.SetYMD(2010, 1, 2);
    iwd.SetISOWeekDate(2009, 53, 6);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2010-01-03 is 2009-W53-7\n";
    ymd.SetYMD(2010, 1, 3);
    iwd.SetISOWeekDate(2009, 53, 7);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2010-01-04 is 2010-W01-1\n";
    ymd.SetYMD(2010, 1, 4);
    iwd.SetISOWeekDate(2010, 1, 1);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2008-09-26 is 2009-W39-5\n";
    ymd.SetYMD(2008, 9, 26);
    iwd.SetISOWeekDate(2008, 39, 5);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* 2012-05-24 is 2012-W21-4\n";
    ymd.SetYMD(2012, 5, 24);
    iwd.SetISOWeekDate(2012, 21, 4);
    p1 = Testcd2woy(ymd, iwd);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Test the conversion of calendar date to ISO week of the year.
   *
   * @param ymd The date.
   * @param iwd The ISO week date.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> Testcd2woy(YMD ymd, ISOWeekDate iwd) {
    ISOWeekDate iwd1 = this.dtc.cd2woy(ymd);
    return this.Compare(iwd1, iwd, "<br>cd2woy()");
  }

  /**
   * Test the conversion of hours minutes and seconds to hours.
   *
   * @param hr  The expected hour of the day.
   * @param hms The time.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> Testhms2hr(double hr, HMS hms) {
    DTPair<Boolean, String> res = new DTPair<>(null, null);
    double h = this.dtc.hms2h(hms);
    if (this.dtb.isclose(hr, h, dtb.rel_fod)) {
      res.first = true;
      res.second = "<br>Passed (hms2h)\n";
    } else {
      res.first = false;
      res.second = String.format("<br>Fail, (hms2h) got: %.12f,  expected: %.12f\n", h, hr);
    }
    return res;
  }

  /**
   * Test the conversion of hours of the day to hours minutes and seconds.
   *
   * @param hr  The hour of the day.
   * @param hms The expected time.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> Testhr2hms(double hr, HMS hms) {
    DTPair<Boolean, String> res = new DTPair<>(null, null);
    HMS thms = new HMS(this.dtc.h2hms(hr));
    if (thms.IsClose(hms, dtb.rel_seconds)) {
      res.first = true;
      res.second = "<br>Passed (h2hms)\n";
    } else {
      res.first = false;
      res.second = String.format("<br>Fail, (h2hms) got: %s,  expected: %s\n", thms, hms);
    }
    return res;
  }

  /**
   * Test the conversion of hours minutes and seconds to hours and vice versa.
   *
   * @param hr  The hour of the day.
   * @param hms The time.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestHMS(double hr, HMS hms) {
    DTPair<Boolean, String> p1;
    DTPair<Boolean, String> p2;
    p1 = Testhms2hr(hr, hms);
    p2 = Testhr2hms(hr, hms);
    return new DTPair<>(p1.first & p2.first, p1.second + p2.second);
  }

  /**
   * Test the conversion of hours and minutes to hours.
   *
   * @param hr The expected hour of the day.
   * @param hm The time.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> Testhm2hr(double hr, HM hm) {
    DTPair<Boolean, String> res = new DTPair<>(null, null);
    double h = this.dtc.hm2h(hm);
    if (this.dtb.isclose(hr, h, dtb.rel_fod)) {
      res.first = true;
      res.second = "<br>Passed (hm2h)\n";
    } else {
      res.first = false;
      res.second = String.format("<br>Fail, (hm2h) got: %.12f,  expected: %.12f\n", h, hr);
    }
    return res;
  }

  /**
   * Test the conversion of hours of the day to hours and minutes.
   *
   * @param hr The hour of the day.
   * @param hm The expected time.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> Testhr2hm(double hr, HM hm) {
    DTPair<Boolean, String> res = new DTPair<>(null, null);
    HM thm = new HM(this.dtc.h2hm(hr));
    if (thm.IsClose(hm, dtb.rel_minutes)) {
      res.first = true;
      res.second = "<br>Passed (h2hm)\n";
    } else {
      res.first = false;
      res.second = String.format("<br>Fail, (h2hm), got: %s  expected: %s\n", thm, hm);
    }
    return res;
  }

  /**
   * Test the conversion of hours minutes and seconds to hours and vice versa.
   *
   * @param hr The hour of the day.
   * @param hm The time.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestHM(double hr, HM hm) {
    DTPair<Boolean, String> p1;
    DTPair<Boolean, String> p2;
    p1 = Testhm2hr(hr, hm);
    p2 = Testhr2hm(hr, hm);
    return new DTPair<>(p1.first & p2.first, p1.second + p2.second);
  }

  /**
   * Run the tests for the time conversions.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestTime() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing times.", 2);

    DTPair<Boolean, String> p1;

    p.second += "* h=23.7625, 23:45:45\n";
    HMS hms = new HMS(23, 45, 45);
    p1 = TestHMS(23.7625, hms);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* h=-23.7625, -23:45:45\n";
    hms.hour = -hms.hour;
    p1 = TestHMS(-23.7625, hms);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    // * precisions and minutes rollover.
    p.second += "* h=23.7625, 22:104:105.0 == 23:45:45\n";
    hms.SetHMS(22, 104, 105);
    hms = this.dtc.h2hms(this.dtc.hms2h(hms));
    p1 = TestHMS(23.7625, hms);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* h=23.7625, 23:44:105.0 == 23:45:45\n";
    hms.SetHMS(23, 44, 105);
    hms = this.dtc.h2hms(this.dtc.hms2h(hms));
    p1 = TestHMS(23.7625, hms);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* h=23.7625, 22:105:45.0 == 23:45:45\n";
    hms.SetHMS(22, 105, 45);
    hms = this.dtc.h2hms(this.dtc.hms2h(hms));
    p1 = TestHMS(23.7625, hms);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* h=-23.7625, -22:104:105.0 == -23:45:45\n";
    hms.SetHMS(-22, 104, 105);
    hms = this.dtc.h2hms(this.dtc.hms2h(hms));
    p1 = TestHMS(-23.7625, hms);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* h=-23.7625, -23:44:105.0 == -23:45:45\n";
    hms.SetHMS(-23, 44, 105);
    hms = this.dtc.h2hms(this.dtc.hms2h(hms));
    p1 = TestHMS(-23.7625, hms);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* h=-23.7625, -22:105:45.0 == -23:45:45\n";
    hms.SetHMS(-22, 105, 45);
    hms = this.dtc.h2hms(this.dtc.hms2h(hms));
    p1 = TestHMS(-23.7625, hms);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    // * HM
    p.second += "* h=23.7625, 23:45.75\n";
    HM hm = new HM(23, 45.75);
    p1 = TestHM(23.7625, hm);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* h=-23.7625, -23:45.75\n";
    hm.hour = -hm.hour;
    p1 = TestHM(-23.7625, hm);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* h=00.7625, 00:45.75\n";
    hm.hour = 0;
    hm.minute = 45.75;
    p1 = TestHM(0.7625, hm);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* h=-00.7625, -00:45.75\n";
    hm.minute = -hm.minute;
    p1 = TestHM(-0.7625, hm);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    // * precision and minutes rollover.
    p.second += "* h=1.0125, 00:60.75 == 01:00.75\n";
    hm.SetHM(0, 60.75);
    hm = this.dtc.h2hm(this.dtc.hm2h(hm));
    p1 = TestHM(1.0125, hm);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* h=1.016665, 00:60.9999 == 01:00.9999\n";
    hm.SetHM(0, 60.9999);
    hm = this.dtc.h2hm(this.dtc.hm2h(hm));
    p1 = TestHM(1.016665, hm);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "* h=-1.0125, -00:60.75 == -01:00.75\n";
    hm.SetHM(0, -60.75);
    hm = this.dtc.h2hm(this.dtc.hm2h(hm));
    p1 = TestHM(-1.0125, hm);
    p.first = p.first & p1.first;
    p.second = p.second + p1.second;

    p.second += "\n";

    return p;
  }

}
