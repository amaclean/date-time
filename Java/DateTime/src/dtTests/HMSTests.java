/*=========================================================================

  Program:   Date Time Library
  File   :   HMSTests.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
package dtTests;

import dt.DTPair;
import dt.HMS;

import java.util.ArrayList;

/**
 * A test harness to test the HMS class.
 *
 * @author Andrew J. P. Maclean
 */
public class HMSTests extends CommonTests {

  private final int hour = 23;
  private final int minute = 59;
  private final double second = 59.999;
  private final HMS hms = new HMS(this.hour, this.minute, this.second);

  /**
   * Run all the tests.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * string of failed tests.
   */
  public DTPair<Boolean, String> TestAll() {
    ArrayList<DTPair<Boolean, String>> messages = new ArrayList<>();

    messages.add(this.TestConstructors());
    messages.add(this.TestSetGet());
    messages.add(this.TestComparisonOperators());
    messages.add(this.TestAssignment());
    messages.add(this.TestStringOutput());

    return this.AggregateResults("HMS Tests.", messages);
  }

  /**
   * Run the tests for the constructors.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestConstructors() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing constructors.", 2);

    p.second += "* HMS() ";
    HMS h = new HMS();
    DTPair<Boolean, String> p1;
    p1 = this.PassFail(h.hour == 0 && h.minute == 0 && h.second == 0.0);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* HMS(h,m,s) ";
    HMS h1 = new HMS(this.hour, this.minute, this.second);
    p1 = this.PassFail(h1.hour == this.hour && h1.minute == this.minute && h1.second == this.second);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* HMS(hms) ";
    HMS h2 = new HMS(h1);
    p1 = this.PassFail(h1.hour == h2.hour && h1.minute == h2.minute && h1.second == h2.second);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for setting and getting the members.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestSetGet() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing setting/getting members.", 2);

    p.second += "* SetHMS(h,m,s) ";
    HMS h1 = new HMS(this.hour, this.minute, this.second);
    HMS h2 = new HMS();
    h2.SetHMS(this.hour, this.minute, this.second);
    DTPair<Boolean, String> p1;
    p1 = this.PassFail(h1.hour == h2.hour && h1.minute == h2.minute && h1.second == h2.second);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* SetHMS(HMS) ";
    h2.SetHMS(h1);
    p1 = this.PassFail(h1.hour == h2.hour && h1.minute == h2.minute && h1.second == h2.second);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* GetHMS() ";
    h2 = h1.GetHMS();
    p1 = this.PassFail(h1.hour == h2.hour && h1.minute == h2.minute && h1.second == h2.second);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for the comparison operators.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestComparisonOperators() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing comparison operators.", 2);

    p.second += "* == ";
    HMS h1 = new HMS(this.hour, this.minute, this.second);
    DTPair<Boolean, String> p1;
    p1 = this.PassFail(h1.eq(this.hms));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* &asymp; ";
    HMS h2 = new HMS(this.hour, this.minute, this.second - 0.001);
    p1 = this.PassFail(this.hms.IsClose(h2, 0.0001));
    p.first &= p1.first;
    p.second += "\n<br>" + p1.second;

    p1 = this.PassFail(!this.hms.IsClose(h2, 0.00001));
    p.first &= p1.first;
    p.second += "<br>" + p1.second;

    p.second += "* != ";
    h1.second -= 1;
    p1 = this.PassFail(h1.ne(this.hms));
    p.first &= p1.first;
    p.second += p1.second;

    HMS a = new HMS(13, 7, 2);
    HMS b = new HMS(14, 7, 2);
    HMS c = new HMS(13, 8, 2);
    HMS d = new HMS(13, 7, 3);

    p.second += "* &lt; and &le; ";
    p1 = this.PassFail(
        a.lt(b) && a.lt(c) && a.lt(d) && a.le(b) && a.le(c) && a.le(d) && a.le(a) && !(a.gt(b)) && !(a.ge(b)));
    p.first &= p1.first;
    p.second += p1.second;

    HMS e = new HMS(13, 7, 2);
    HMS f = new HMS(12, 7, 2);
    HMS g = new HMS(13, 6, 2);
    HMS h = new HMS(13, 7, 1);

    p.second += "* &gt; and &ge; ";
    p1 = this.PassFail(
        e.gt(f) && e.gt(g) && e.gt(h) && e.ge(f) && e.ge(g) && e.ge(h) && e.ge(e) && !(e.lt(f)) && !(e.le(f)));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for assignment.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestAssignment() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing assignment.", 2);

    p.second += "* = ";
    HMS h1;
    h1 = this.hms;
    DTPair<Boolean, String> p1;
    p1 = this.PassFail(h1.hour == this.hms.hour && h1.minute == this.hms.minute && h1.second == this.hms.second);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for string output.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestStringOutput() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing GetString()", 2);

    String s1 = this.hms.GetString();
    String valueStr = " 23:59:59.999000000000";
    DTPair<Boolean, String> p1 = this.Compare(s1, valueStr, "* ");
    p.first &= p1.first;
    p.second += p1.second;

    HMS h1 = new HMS(-23, 59, 59.01);
    String expected = "-23:59:59.010000000000";
    s1 = h1.GetString();
    p1 = this.Compare(s1, expected, "* ");
    p.first &= p1.first;
    p.second += p1.second;

    h1.SetHMS(0, -59, 59.01);
    expected = "-00:59:59.010000000000";
    s1 = h1.GetString();
    p1 = this.Compare(s1, expected, "* ");
    p.first &= p1.first;
    p.second += p1.second;

    h1.SetHMS(0, 0, -59.01);
    expected = "-00:00:59.010000000000";
    s1 = h1.GetString();
    p1 = this.Compare(s1, expected, "* ");
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

}
