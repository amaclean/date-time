/*=========================================================================

  Program:   Date Time Library
  File   :   HMTests.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
package dtTests;

import dt.DTPair;
import dt.HM;

import java.util.ArrayList;

/**
 * A test harness to test the HM class.
 *
 * @author Andrew J. P. Maclean
 */
public class HMTests extends CommonTests {

  private final int hour = 23;
  private final double minute = 59.999;
  private final HM hm = new HM(this.hour, this.minute);
  private final String valueStr = " 23:59.999000000000";

  /**
   * Run all the tests.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * string of failed tests.
   */
  public DTPair<Boolean, String> TestAll() {
    ArrayList<DTPair<Boolean, String>> messages = new ArrayList<>();

    messages.add(this.TestConstructors());
    messages.add(this.TestSetGet());
    messages.add(this.TestComparisonOperators());
    messages.add(this.TestAssignment());
    messages.add(this.TestStringOutput());

    return this.AggregateResults("HM Tests.", messages);
  }

  /**
   * Run the tests for the constructors.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestConstructors() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing constructors.", 2);

    p.second += "* HM() ";
    HM h = new HM();
    DTPair<Boolean, String> p1;
    p1 = this.PassFail(h.hour == 0 && h.minute == 0.0);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* HM(h,m) ";
    HM h1 = new HM(this.hour, this.minute);
    p1 = this.PassFail(h1.hour == this.hour && h1.minute == this.minute);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* HM(hm) ";
    HM h2 = new HM(h1);
    p1 = this.PassFail(h1.hour == h2.hour && h1.minute == h2.minute);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for setting and getting the members.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestSetGet() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing setting/getting members.", 2);

    p.second += "* SetHM(h,m) ";
    HM h1 = new HM(this.hour, this.minute);
    HM h2 = new HM();
    h2.SetHM(this.hour, this.minute);
    DTPair<Boolean, String> p1;
    p1 = this.PassFail(h1.hour == h2.hour && h1.minute == h2.minute);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* SetHM(hm) ";
    h2.SetHM(h1);
    p1 = this.PassFail(h1.hour == h2.hour && h1.minute == h2.minute);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* GetHM() ";
    h2 = h1.GetHM();
    p1 = this.PassFail(h1.hour == h2.hour && h1.minute == h2.minute);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for the comparison operators.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestComparisonOperators() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing comparison operators.", 2);

    p.second += "* == ";
    HM h1 = new HM(this.hour, this.minute);
    DTPair<Boolean, String> p1;
    p1 = this.PassFail(h1.eq(this.hm));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* &asymp; ";
    HM h2 = new HM(this.hour, this.minute - 0.001);
    p1 = this.PassFail(this.hm.IsClose(h2, 00.001));
    p.first &= p1.first;
    p.second += "\n<br>" + p1.second;

    p1 = this.PassFail(!this.hm.IsClose(h2, 0.00001));
    p.first &= p1.first;
    p.second += "<br>" + p1.second;

    p.second += "* != ";
    h1.minute -= 1;
    p1 = this.PassFail(h1.ne(this.hm));
    p.first &= p1.first;
    p.second += p1.second;

    HM a = new HM(13, 7.2);
    HM b = new HM(14, 7.2);
    HM c = new HM(13, 8.2);

    p.second += "* &lt; and &le; ";
    p1 = this.PassFail(a.lt(b) && a.lt(c) && a.le(b) && a.le(c) && a.le(a) && !(a.gt(b)) && !(a.ge(b)));
    p.first &= p1.first;
    p.second += p1.second;

    HM e = new HM(13, 7.2);
    HM f = new HM(12, 7.2);
    HM g = new HM(13, 6.2);

    p.second += "* &gt; and &ge; ";
    p1 = this.PassFail(e.gt(f) && e.gt(g) && e.ge(f) && e.ge(g) && e.ge(e) && !(e.lt(f)) && !(e.le(f)));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for assignment.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestAssignment() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing assignment.", 2);

    p.second += "* = ";
    HM h1;
    h1 = this.hm;
    DTPair<Boolean, String> p1;
    p1 = this.PassFail(h1.hour == this.hm.hour && h1.minute == this.hm.minute);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for string output.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestStringOutput() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing GetString()", 2);

    String s1 = this.hm.GetString();
    DTPair<Boolean, String> p1 = this.Compare(s1, this.valueStr, "* ");
    p.first &= p1.first;
    p.second += p1.second;

    HM h1 = new HM(-23, 59.01);
    String expected = "-23:59.010000000000";
    s1 = h1.GetString();
    p1 = this.Compare(s1, expected, "* ");
    p.first &= p1.first;
    p.second += p1.second;

    h1.SetHM(0, -59.01);
    expected = "-00:59.010000000000";
    s1 = h1.GetString();
    p1 = this.Compare(s1, expected, "* ");
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

}
