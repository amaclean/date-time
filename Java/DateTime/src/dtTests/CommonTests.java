/*=========================================================================

  Program:   Date Time Library
  File   :   CommonTests.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
package dtTests;

import dt.DTPair;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Common routines for the tests.
 *
 * @author Andrew J. P. Maclean
 */
public class CommonTests {

  /**
   * Make a markdown compatible header.
   *
   * @param header The header.
   * @param level  The level of the header (1 ... 6).
   * @return A pair consisting of true and the header string.
   */
  public DTPair<Boolean, String> MakeHeader(String header, int level) {
    // Clamp level to the range 1 .. 6 inclusive.
    final char[] array = new char[Math.max(1, Math.min(level, 6))];
    char ch = '#';
    Arrays.fill(array, ch);
    return new DTPair<>(true, String.format("%s %s\n",
        new String(array), header));
  }

  /**
   * Concatenate the results into a markdown compatible format.
   *
   * @param header   The header.
   * @param messages The messages to be aggregated.
   * @return A pair consisting of either true or false and the concatenated
   * message.
   */
  public DTPair<Boolean, String> AggregateResults(String header,
                                                  ArrayList<DTPair<Boolean, String>> messages) {
    DTPair<Boolean, String> p = this.MakeHeader(header, 1);
    for (DTPair<Boolean, String> bs : messages) {
      if (!bs.first) {
        p.first &= bs.first;
        p.second += bs.second;
      }
    }
    return p;
  }

  /**
   * Compare two objects. The type T must support the operation (operator==).
   *
   * @param observed The expected result.
   * @param expected The observed result.
   * @param msg      The first part of the pass/fail message.
   * @return A pair consisting of either true or false and a message.
   */
  public <T> DTPair<Boolean, String> Compare(T observed, T expected,
                                             String msg) {
    String s = msg;
    if (observed.equals(expected)) {
      s += " passed.\n";
      return new DTPair<>(true, s);
    } else {
      s += " failed, observed: " + observed.toString() + " expected: " +
          expected.toString() + "\n";
      return new DTPair<>(false, s);
    }
  }

  /**
   * Generate a pass/fail message.
   *
   * @param result The result of a test, either true or false.
   * @return A Pair consisting of either true or false and a message.
   */
  public <T> DTPair<Boolean, String> PassFail(boolean result) {
    if (result) {
      return new DTPair<>(true, "Passed.\n");
    }
    return new DTPair<>(false, "Failed.\n");
  }
}
