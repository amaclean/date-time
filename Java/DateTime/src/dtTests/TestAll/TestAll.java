package dtTests.TestAll;

import dt.DTPair;
import dtTests.*;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class TestAll {

  /**
   * @param args Not used.
   */
  public static void main(String[] args) {
    // System.out.print("Test started: ");
    // System.out.println(GetCurrentSystemDateTime(TimeZone.getDefault().getID()));
    RunTests();
    // System.out.print("Test finished: ");
    // System.out.println(GetCurrentSystemDateTime(TimeZone.getDefault().getID()));
  }

  /**
   * Run all the tests for all classes.
   */
  private static void RunTests() {
    long t0 = System.nanoTime();
    DateTimeConversionsTests dtc = new DateTimeConversionsTests();
    HMTests hm = new HMTests();
    HMSTests hms = new HMSTests();
    DayNamesTests dn = new DayNamesTests();
    FoDHMSTests fodhms = new FoDHMSTests();
    ISOWeekDateTests iwd = new ISOWeekDateTests();
    JDTests jd = new JDTests();
    MonthNamesTests mn = new MonthNamesTests();
    TimeIntervalTests ti = new TimeIntervalTests();
    TimeZonesTests tz = new TimeZonesTests();
    YMDTests ymd = new YMDTests();
    YMDHMSTests ymdhms = new YMDHMSTests();

    JulianDateTimeTests jdt = new JulianDateTimeTests();
    DateTimeParserTests dth = new DateTimeParserTests();
    DateTimeFormatterTests dtf = new DateTimeFormatterTests();

    ArrayList<DTPair<Boolean, String>> messages = new ArrayList<>();

    messages.add(dtc.TestAll());
    messages.add(hm.TestAll());
    messages.add(hms.TestAll());
    messages.add(dn.TestAll());
    messages.add(fodhms.TestAll());
    messages.add(iwd.TestAll());
    messages.add(jd.TestAll());
    messages.add(mn.TestAll());
    messages.add(ti.TestAll());
    messages.add(tz.TestAll());
    messages.add(ymd.TestAll());
    messages.add(ymdhms.TestAll());

    messages.add(jdt.TestAll());
    messages.add(dth.TestAll());
    messages.add(dtf.TestAll());

    Boolean ok = true;
    for (DTPair<Boolean, String> bs : messages) {
      ok &= bs.first;
    }

    if (!ok) {
      System.out.printf("# Some tests failed.\n");
      for (DTPair<Boolean, String> bs : messages) {
        if (!bs.first) {
          System.out.printf(bs.second);
        }
      }
    } else {
      System.out.printf("# All tests passed.\n");
    }
    long t1 = System.nanoTime();
    long dT = t1 - t0;
    System.out.printf(String.format("<br>Elapsed time:   %s\n", FormatElapsedTime(dT)));
  }

// --Commented out by Inspection START (2017-07-27 15:06):
//    /**
//     * Get the current system date and time.
//     *
//     * @param tz
//     *            - the Time Zone e.g. GMT+10:00 Australia/Sydney
//     */
//    public static String GetCurrentSystemDateTime(String tz) {
//        // Initialised with the current date and time.
//        Calendar cal = new GregorianCalendar();
//        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
//        date_format.setTimeZone(TimeZone.getTimeZone(tz));
//        return date_format.format(cal.getTime());
//    }
// --Commented out by Inspection STOP (2017-07-27 15:06)

  /**
   * Format the elapsed time.
   *
   * @param deltaT - the elapsed time in nanoseconds.
   */
  private static String FormatElapsedTime(long deltaT) {
    double seconds = ((deltaT / 1.0e9) % 60.0);
    long minutes = TimeUnit.MINUTES.convert(deltaT, TimeUnit.NANOSECONDS) % 60;
    long hours = TimeUnit.HOURS.convert(deltaT, TimeUnit.NANOSECONDS) % 24;
    long days = TimeUnit.DAYS.convert(deltaT, TimeUnit.NANOSECONDS);

    return String.format("%02dd %02d:%02d:%012.9f", days, hours, minutes, seconds);
  }

}
