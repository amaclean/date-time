/*=========================================================================

  Program:   Date Time Library
  File   :   JDTests.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
package dtTests;

import dt.DTPair;
import dt.JD;

import java.util.ArrayList;

/**
 * A test harness to test the JD class.
 *
 * @author Andrew J. P. Maclean
 */
public class JDTests extends CommonTests {

  private final int jdn = 2525008;
  private final double fod = 0.5;
  private final JD jd = new JD(this.jdn, this.fod);

  /**
   * Run all the tests.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * string of failed tests.
   */
  public DTPair<Boolean, String> TestAll() {
    ArrayList<DTPair<Boolean, String>> messages = new ArrayList<>();

    messages.add(this.TestConstructors());
    messages.add(this.TestSetGet());
    messages.add(this.TestComparisonOperators());
    messages.add(this.TestAssignment());
    messages.add(this.TestIncrementAndAddition());
    messages.add(this.TestDecrementAndSubtraction());
    messages.add(this.TestNormalisation());
    messages.add(this.TestStringOutput());

    return this.AggregateResults("JD Tests.", messages);
  }

  /**
   * Run the tests for the constructors.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestConstructors() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing constructors.", 2);

    p.second += "* JD() ";
    JD jd1 = new JD();
    DTPair<Boolean, String> p1;
    p1 = this.PassFail(jd1.GetJDN() == 0 && jd1.GetFoD() == 0.0);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* JD(jdn,fod) ";
    jd1 = new JD(this.jdn, this.fod);
    p1 = this.PassFail(jd1.GetJDN() == this.jdn && jd1.GetFoD() == this.fod);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* JD(jd) ";
    JD jd2 = new JD(jd1);
    p1 = this.PassFail(jd1.GetJDN() == jd2.GetJDN() && jd1.GetFoD() == jd2.GetFoD());
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for setting and getting the members.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestSetGet() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing setting/getting members.", 2);

    p.second += "* SetJD(h,m,s) ";
    JD jd1 = new JD(this.jdn, this.fod);
    JD jd2 = new JD();
    jd2.SetJD(this.jdn, this.fod);
    DTPair<Boolean, String> p1;
    p1 = this.PassFail(jd1.eq(jd2));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* SetJD(JD) ";
    jd2.SetJD(jd1);
    p1 = this.PassFail(jd1.eq(jd2));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* GetJD() ";
    jd2 = jd1.GetJD();
    p1 = this.PassFail(jd1.eq(jd2));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for the comparison operators.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestComparisonOperators() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing comparison operators.", 2);

    p.second += "* == ";
    JD jd1 = new JD(this.jdn, this.fod);
    DTPair<Boolean, String> p1;
    p1 = this.PassFail(jd1.eq(this.jd));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* &asymp; ";
    p1 = this.PassFail(this.jd.IsClose(jd1));
    p.first &= p1.first;
    p.second += "\n<br>" + p1.second;

    JD jd2 = new JD(this.jdn, this.fod - 0.001);
    p1 = this.PassFail(this.jd.IsClose(jd2, 0.01));
    p.first &= p1.first;
    p.second += "<br>" + p1.second;

    p1 = this.PassFail(!this.jd.IsClose(jd2, 0.0001));
    p.first &= p1.first;
    p.second += "<br>" + p1.second;

    p.second += "* != ";
    jd1.SetFoD(jd1.GetFoD() - 0.2);
    p1 = this.PassFail(jd1.ne(this.jd));
    p.first &= p1.first;
    p.second += p1.second;

    JD a = new JD(2456553, 0.53308796);
    JD b = new JD(2456554, 0.53308796);
    JD c = new JD(2456553, 0.53308797);

    p.second += "* &lt; and &le; ";
    p1 = this.PassFail(a.lt(b) && a.lt(c) && a.le(b) && a.le(c) && a.le(a) && !(a.gt(b)) && !(a.ge(b)));
    p.first &= p1.first;
    p.second += p1.second;

    JD e = new JD(2456553, 0.53308796);
    JD f = new JD(2456552, 0.53308796);
    JD g = new JD(2456553, 0.53308795);

    p.second += "* &gt; and &ge; ";
    p1 = this.PassFail(e.gt(f) && e.gt(g) && e.ge(f) && e.ge(g) && e.ge(e) && !(e.lt(f)) && !(e.le(f)));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for assignment.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestAssignment() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing assignment.", 2);

    p.second += "* assign (=) ";
    JD jd1 = new JD();
    jd1.assign(this.jd);
    DTPair<Boolean, String> p1;
    p1 = this.PassFail(jd1.eq(this.jd));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for the increment and addition operators.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestIncrementAndAddition() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing increment and addition.", 2);

    p.second += "* jd1 += jd2 ";
    DTPair<Boolean, String> p1;
    JD jd1 = new JD(this.jdn, this.fod);
    JD jd2 = new JD();
    double fod = 15 / 86400.0;
    jd2.SetJD(1, fod); // One day and 15s
    JD jd3 = new JD(jd1);
    jd3.inc(jd2);
    JD jd4 = new JD(this.jdn + 1, this.fod + fod);
    p1 = this.PassFail(jd3.IsClose(jd4, jd3.rel_fod));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* jd1 += days ";
    double days = 1 + fod;
    jd3.assign(jd1);
    jd3.inc(days);
    p1 = this.PassFail(jd3.IsClose(jd4, jd4.rel_fod));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* jd1 + jd2 ";
    jd3 = jd1.add(jd2);
    p1 = this.PassFail(jd3.IsClose(jd4, jd4.rel_fod));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for the decrement and subtraction operators.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestDecrementAndSubtraction() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing decrement and subtraction.", 2);

    p.second += "* jd1 -= jd2 ";
    DTPair<Boolean, String> p1;
    JD jd1 = new JD(this.jdn, this.fod);
    JD jd2 = new JD();
    double fod = 15 / 86400.0;
    jd2.SetJD(1, fod); // One day and 15s
    JD jd3 = new JD(jd1);
    jd3.dec(jd2);
    JD jd4 = new JD(this.jdn - 1, this.fod - fod);
    p1 = this.PassFail(jd3.IsClose(jd4, jd4.rel_fod));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* jd1 -= days ";
    double days = 1 + fod;
    jd3.assign(jd1);
    jd3.dec(days);
    p1 = this.PassFail(jd3.IsClose(jd4, jd4.rel_fod));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* jd1 - jd2 ";
    jd3 = jd1.sub(jd2);
    p1 = this.PassFail(jd3.IsClose(jd4, jd4.rel_fod));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for string output.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestNormalisation() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing normalising.", 2);

    p.second += "* JD(0,-0.5) stored as jdn = -1, fod = 0.5 ";
    JD jd1 = new JD(0, -0.5);
    DTPair<Boolean, String> p1 = this.PassFail(jd1.GetJDN() == -1 && jd1.GetFoD() == 0.5);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* JD(-1,0.5) stored as jdn = -1, fod = 0.5 ";
    jd1 = new JD(-1, 0.5);
    p1 = this.PassFail(jd1.GetJDN() == -1 && jd1.GetFoD() == 0.5);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* JD(0,-1.5) stored as jdn = -2, fod = 0.5 ";
    jd1 = new JD(0, -1.5);
    p1 = this.PassFail(jd1.GetJDN() == -2 && jd1.GetFoD() == 0.5);
    p.first &= p1.first;
    p.second += p1.second;
    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for string output.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestStringOutput() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing GetString()", 2);

    String s1 = this.jd.GetString();
    DTPair<Boolean, String> p1;
    String valueStr = " 2525008.500000000000000";
    p1 = this.Compare(s1, valueStr, "* ");
    p.first &= p1.first;
    p.second += p1.second;

    JD j1 = new JD(-1, 0.5);
    String expected = "-1.500000000000000";
    s1 = j1.GetString();
    p1 = this.Compare(s1, expected, "* ");
    p.first &= p1.first;
    p.second += p1.second;

    j1.SetJD(0, -1.5);
    expected = "-2.500000000000000";
    s1 = j1.GetString();
    p1 = this.Compare(s1, expected, "* ");
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

}
