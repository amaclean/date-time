/*=========================================================================

  Program:   Date Time Library
  File   :   DateTimeParserTests.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
package dtTests;

import dt.*;

import java.util.ArrayList;
import java.util.Objects;

/**
 * A test harness to test the DateTimeParser class.
 *
 * @author Andrew J. P. Maclean
 */
public class DateTimeParserTests extends CommonTests {

  private final DateTimeParser dth = new DateTimeParser();
  private final DateTimeBase dtb = new DateTimeBase();
  private final DateTimeFormat fmt = new DateTimeFormat();

  private final ArrayList<DTPair<String, YMDHMS>> strDT = new ArrayList<>();
  private final ArrayList<DTPair<String, Integer>> parseStrDTFail = new ArrayList<>();
  private final ArrayList<DTPair<String, Integer>> parseStrDFail = new ArrayList<>();
  private final ArrayList<DTPair<String, HMS>> strT = new ArrayList<>();
  private final ArrayList<DTPair<String, Integer>> parseStrTFail = new ArrayList<>();
  private final ArrayList<DTPair<String, DTPair<Integer, Double>>> decimalStringParseData = new ArrayList<>();
  private final ArrayList<DTPair<TimeInterval, String>> tidt = new ArrayList<>();
  private final ArrayList<DTPair<String, Integer>> parseStrTIFail = new ArrayList<>();

  public DateTimeParserTests() {
    this.Init();
  }

  private void Init() {
    this.strDT.add(new DTPair<>("2013-09-06", new YMDHMS(2013, 9, 6, 0, 0, 0)));
    this.strDT.add(new DTPair<>("2013-09-06 23", new YMDHMS(2013, 9, 6, 23, 0, 0)));
    this.strDT.add(new DTPair<>("2013-09-06 23:59", new YMDHMS(2013, 9, 6, 23, 59, 0)));
    this.strDT.add(new DTPair<>("2013-09-06 23:59:59", new YMDHMS(2013, 9, 6, 23, 59, 59)));
    this.strDT.add(new DTPair<>("2013-09-06 23:59:59.999", new YMDHMS(2013, 9, 6, 23, 59, 59.999)));
    this.strDT
        .add(new DTPair<>("2013-Sep-06 23:59:59.9995", new YMDHMS(2013, 9, 6, 23, 59, 59.9995)));
    this.strDT.add(
        new DTPair<>("2013-September-06 23:59:59.9995", new YMDHMS(2013, 9, 6, 23, 59, 59.9995)));
    // Negative dates.
    this.strDT.add(new DTPair<>("-2013-09-06", new YMDHMS(-2013, 9, 6, 0, 0, 0)));
    this.strDT.add(new DTPair<>("-2013-09-06 23", new YMDHMS(-2013, 9, 6, 23, 0, 0)));
    this.strDT.add(new DTPair<>("-2013-09-06 23:59", new YMDHMS(-2013, 9, 6, 23, 59, 0)));
    this.strDT.add(new DTPair<>("-2013-09-06 23:59:59", new YMDHMS(-2013, 9, 6, 23, 59, 59)));
    this.strDT.add(new DTPair<>("-2013-09-06 23:59:59.999", new YMDHMS(-2013, 9, 6, 23, 59, 59.999)));
    this.strDT
        .add(new DTPair<>("-2013-09-06 23:59:59.9995", new YMDHMS(-2013, 9, 6, 23, 59, 59.9995)));

    this.parseStrDTFail.add(new DTPair<>("", 2));
    this.parseStrDTFail.add(new DTPair<>("2013", 3));
    this.parseStrDTFail.add(new DTPair<>("2013-09-06 12:51:05:05", 5));
    this.parseStrDTFail.add(new DTPair<>("2013-09-06 12.2:51:05.05", 5));
    this.parseStrDTFail.add(new DTPair<>("2013-Fik-09", 4));

    this.parseStrDFail.add(new DTPair<>("", 2));
    this.parseStrDFail.add(new DTPair<>("2013", 3));
    this.parseStrDFail.add(new DTPair<>("2013-09-06 12:51:05:05", 3));
    this.parseStrDFail.add(new DTPair<>("2013-09-06 12.2:51:05.05", 3));
    this.parseStrDFail.add(new DTPair<>("2013-Fik-09", 4));

    this.strT.add(new DTPair<>("23:59:59.5", new HMS(23, 59, 59.5)));
    this.strT.add(new DTPair<>("23:59", new HMS(23, 59, 0)));
    this.strT.add(new DTPair<>("23", new HMS(23, 0, 0)));
    this.strT.add(new DTPair<>("00:59:59.5", new HMS(0, 59, 59.5)));
    this.strT.add(new DTPair<>("00:00:59.5", new HMS(0, 0, 59.5)));
    this.strT.add(new DTPair<>("-23:59:59.5", new HMS(-23, 59, 59.5)));
    this.strT.add(new DTPair<>("-23:59", new HMS(-23, 59, 0)));
    this.strT.add(new DTPair<>("-23", new HMS(-23, 0, 0)));
    this.strT.add(new DTPair<>("-00:59:59.5", new HMS(0, -59, 59.5)));
    this.strT.add(new DTPair<>("-00:00:59.5", new HMS(0, 0, -59.5)));

    this.parseStrTFail.add(new DTPair<>("", 2));
    this.parseStrTFail.add(new DTPair<>("12:5.7:.3", 5));
    this.parseStrTFail.add(new DTPair<>("12:5:.3", 5));
    this.parseStrTFail.add(new DTPair<>("12:5.7", 5));
    this.parseStrTFail.add(new DTPair<>("5.7", 5));
    this.parseStrTFail.add(new DTPair<>("23:Fik:09", 5));
    this.parseStrTFail.add(new DTPair<>("23:59:59:5", 5));

    this.parseStrTIFail.add(new DTPair<>("", 2));
    this.parseStrTIFail.add(new DTPair<>("00:00:00", 6));
    this.parseStrTIFail.add(new DTPair<>(" 0d  00:00:.5", 5));
    this.parseStrTIFail.add(new DTPair<>(" d  00:00:.5", 6));
    this.parseStrTIFail.add(new DTPair<>(" x  00:00:00", 6));
    this.parseStrTIFail.add(new DTPair<>(" x", 6));

    this.tidt.add(new DTPair<>(new TimeInterval(0, 0), " 0d  00:00:0"));
    this.tidt.add(new DTPair<>(new TimeInterval(-1, -0.25), "-1d -06:00:00.0"));
    this.tidt.add(new DTPair<>(new TimeInterval(-1, 0.25), "-1d  06:00:00.0"));
    this.tidt.add(new DTPair<>(new TimeInterval(1, -0.25), " 1d -06:00:00.0"));
    this.tidt.add(new DTPair<>(new TimeInterval(1, 0.25), " 1d  06:00:00.0"));

    this.decimalStringParseData.add(
        new DTPair<>("-3100015", new DTPair<>(-3100015, 0.0)));
    this.decimalStringParseData.add(new DTPair<>("-3100015.25",
        new DTPair<>(-3100015, -0.25)));
    this.decimalStringParseData.add(new DTPair<>("-3100015.50",
        new DTPair<>(-3100015, -0.5)));
    this.decimalStringParseData.add(new DTPair<>("-3100015.75",
        new DTPair<>(-3100015, -0.75)));

    this.decimalStringParseData
        .add(new DTPair<>("2456751", new DTPair<>(2456751, 0.0)));
    this.decimalStringParseData.add(
        new DTPair<>("2456751.25", new DTPair<>(2456751, 0.25)));
    this.decimalStringParseData.add(
        new DTPair<>("2456751.50", new DTPair<>(2456751, 0.5)));
    this.decimalStringParseData.add(
        new DTPair<>("2456751.75", new DTPair<>(2456751, 0.75)));
    // Should fail.
    this.decimalStringParseData
        .add(new DTPair<>("", new DTPair<>(0, 0.0)));
    this.decimalStringParseData
        .add(new DTPair<>(" ", new DTPair<>(0, 0.0)));
    this.decimalStringParseData
        .add(new DTPair<>("ABC", new DTPair<>(0, 0.0)));
    this.decimalStringParseData
        .add(new DTPair<>(".5", new DTPair<>(0, 0.5)));
    this.decimalStringParseData
        .add(new DTPair<>("-.5", new DTPair<>(0, -0.5)));
    // Should pass.
    this.decimalStringParseData
        .add(new DTPair<>("0.5", new DTPair<>(0, 0.5)));
    this.decimalStringParseData
        .add(new DTPair<>("-0.5", new DTPair<>(0, -0.5)));

  }

  /**
   * Run all the tests.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * string of failed tests.
   */
  public DTPair<Boolean, String> TestAll() {

    // Test Scale (we expect 2.68).
    // System.out.printf("%5.3f %4.2f", 2.675, this.pDTH.Scale(2.675, 2));

    ArrayList<DTPair<Boolean, String>> messages = new ArrayList<>();

    messages.add(this.TestParseDecNumStr());
    messages.add(this.TestParseDateTime());
    messages.add(this.TestParseDateTimeFail());
    messages.add(this.TestParseTime());
    messages.add(this.TestParseTimeFail());
    messages.add(this.TestParseTimeInterval());
    messages.add(this.TestParseTimeIntervalFail());

    return this.AggregateResults("DateTimeParser Tests.", messages);
  }

  /**
   * Run the test for parsing a decimal string.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestParseDecNumStr() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing parsing a decimal string.", 2);

    String[] fmt = {"* Passed testing rounding.\n",
        "* Failed, testing rounding: %11.2f  expected: %10.1f got: %10.1f\n",
        "* Failed, parsing: %s, expected (%s) got: (%s)\n", "* Passed: (%s)\n",
        "* Fail is expected for parsing: %s\n", "* Fail is expected for parsing empty string.\n",
        "* Fail is expected for parsing non-decimal string.\n"};

    double x = 3100015.75;
    double y = Scale(x, 1);
    double z = 3100015.8;
    if (this.dtb.isclose(y, z, dtb.rel_fod)) {
      p.first &= true;
      p.second += fmt[0];
    } else {
      p.first &= false;
      p.second += String.format(fmt[1], x, z, y);
    }
    x = -x;
    y = Scale(x, 1);
    z = -z;
    if (this.dtb.isclose(y, z, dtb.rel_fod)) {
      p.first &= true;
      p.second += fmt[0];
    } else {
      p.first &= false;
      p.second += String.format(fmt[1], x, z, y);
    }

    for (DTPair<String, DTPair<Integer, Double>> q : this.decimalStringParseData) {
      DTPair<Integer, DTPair<Integer, Double>> res = dth.ParseDecNum(q.first);
      if (res.first == 1) {
        if (!res.second.equals(q.second)) {
          p.first &= false;
          p.second += String.format(fmt[2], q.first, q.second, res.second);
        } else {
          p.first &= true;
          p.second += String.format(fmt[3], q.first);
        }
      } else {
        if (q.first.isEmpty()) {
          p.first &= true;
          p.second += fmt[5];
          continue;
        }
        if (Objects.equals(q.first, "ABC")) {
          p.first &= true;
          p.second += fmt[6];
          continue;
        }
        if (q.first.equals(".5") || q.first.equals("-.5") || Objects.equals(q.first, " ")) {
          p.first &= true;
          p.second += String.format(fmt[4], q.first);
        } else {
          p.first &= false;
          p.second += String.format(fmt[2], q.first, q.second, res.second);
        }
      }
    }

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for parsing.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestParseDateTime() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing expected ParseDateTime().", 2);

    DTPair<Boolean, String> p1 = new DTPair<>(true, "");

    for (DTPair<String, YMDHMS> q : this.strDT) {
      DTPair<Integer, YMDHMS> res = this.dth.ParseDateTime(q.first, fmt);
      if (res.first == 1) {
        if (q.second.IsClose(res.second, dtb.rel_seconds)) {
          p1.first = true;
          p1.second = "* Passed.\n";
        } else {
          p1.first = false;
          p1.second = String.format("* Failed, parsing: %s expected: %s got: %s\n", q.first,
              q.second.GetString(), res.second.GetString());
        }
      } else {
        p1.first = false;
        p1.second = String.format("* Failed, parse failure parsing: %s, expected 1, got: %d = %s\n", q.first,
            res.first, this.dth.parserMessages.get(res.first));
      }
      p.first &= p1.first;
      p.second += p1.second;
    }

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for parsing.
   * <p>
   * Here we are testing for expected failures in parsing.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestParseDateTimeFail() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing expected ParseDateTime() failures.", 2);

    DTPair<Boolean, String> p1 = new DTPair<>(true, "");

    for (DTPair<String, Integer> q : this.parseStrDTFail) {
      DTPair<Integer, YMDHMS> res = this.dth.ParseDateTime(q.first, fmt);
      if (!Objects.equals(res.first, q.second)) {
        p1.first = false;
        p1.second = String.format("* Failed, expected %d, got: %d The string being parsed is: %s\n", q.second,
            res.first, q.first);
      } else {
        p1.first = true;
        p1.second = String.format("* Expected to fail, the string being parsed is: %s\n", q.first);
      }
      p.first &= p1.first;
      p.second += p1.second;
    }

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for parsing a time.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestParseTime() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing expected ParseTime().", 2);

    DTPair<Boolean, String> p1 = new DTPair<>(true, "");

    for (DTPair<String, HMS> q : this.strT) {
      DTPair<Integer, HMS> res = dth.ParseTime(q.first, fmt);
      if (res.first == 1) {
        if (q.second.IsClose(res.second, dtb.rel_seconds)) {
          p1.first = true;
          p1.second += "* Passed.\n";
        } else {
          p1.first = false;
          p1.second = String.format("* Failed, parsing: %s expected: %s got: %s\n", q.first,
              q.second.GetString(), res.second.GetString());
        }
      } else {
        p1.first = false;
        p1.second = String.format("* Failed, parse failure parsing: %s, expected 1, got: %d = %s\n", q.first,
            res.first, this.dth.parserMessages.get(res.first));
      }
      p.first &= p1.first;
      p.second += p1.second;
    }

    p.second += "\n";
    return p;
  }

  /**
   * Run the test for parsing a time.
   * <p>
   * Here we are testing for expected failures in parsing.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestParseTimeFail() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing expected ParseTime() failures.", 2);

    DTPair<Boolean, String> p1 = new DTPair<>(true, "");

    for (DTPair<String, Integer> q : this.parseStrTFail) {
      DTPair<Integer, HMS> res = this.dth.ParseTime(q.first, fmt);
      if (!Objects.equals(res.first, q.second)) {
        p1.first = false;
        p1.second = String.format("* Failed, expected %d, got: %d The string being parsed is: %s\n", q.second,
            res.first, q.first);
      } else {
        p1.first = true;
        p1.second = String.format("* Expected to fail, the string being parsed is: %s\n", q.first);
      }
      p.first &= p1.first;
      p.second += p1.second;
    }

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for parsing a time interval.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestParseTimeInterval() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing ParseTimeInterval().", 2);

    DTPair<Boolean, String> p1 = new DTPair<>(true, "");

    for (DTPair<TimeInterval, String> q : this.tidt) {
      DTPair<Integer, TimeInterval> res = this.dth.ParseTimeInterval(q.second, fmt);
      if (res.first == 1) {
        if (res.second.IsClose(q.first, dtb.rel_fod)) {
          p1.first = true;
          p1.second = "* Passed.\n";
        } else {
          p1.first = false;
          p1.second = String.format("* Failed, parsing: %s  expected: %s got: %s\n", q.second,
              q.first.GetString(), res.second.GetString());
        }
      } else {
        p1.first = false;
        p1.second = String.format("* Failed, parse failure expected 1, got: %d - %s\n", res.first,
            dth.parserMessages.get(res.first));
      }
      p.first &= p1.first;
      p.second += p1.second;
    }

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for parsing a time interval.
   * <p>
   * Here we are testing for expected failures in parsing.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestParseTimeIntervalFail() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing expected ParseTimeInterval() failures.", 2);

    DTPair<Boolean, String> p1 = new DTPair<>(true, "");

    for (DTPair<String, Integer> q : this.parseStrTIFail) {
      DTPair<Integer, TimeInterval> res = this.dth.ParseTimeInterval(q.first, fmt);
      if (!Objects.equals(res.first, q.second)) {
        p1.first = false;
        p1.second = String.format("* Failed, expected %d, got: %d The string being parsed is: %s\n", q.second,
            res.first, q.first);
      } else {
        p1.first = true;
        p1.second = String.format("* Expected to fail, the string being parsed is: %s\n", q.first);
      }
      p.first &= p1.first;
      p.second += p1.second;
    }

    p.second += "\n";

    return p;
  }

  /**
   * Scale a number by rounding it to a specified number of decimal places. If
   * the number is negative, ties are rounded to negative infinity.
   *
   * @param x         The number to round.
   * @param precision The number of decimal places to round to.
   * @return The rounded number.
   */
  private double Scale(double x, int precision) {
    boolean sgn = x < 0;
    double a = Math.abs(x);
    int maximumPrecision = 15;
    int prec = (Math.abs(precision) > Math.abs(maximumPrecision)) ? Math.abs(maximumPrecision)
        : Math.abs(precision);
    double scale = 1;
    for (int i = 0; i < prec; ++i) {
      scale *= 10.0;
    }
    a = Math.round(a * scale) / scale;
    return (sgn) ? -a : a;
  }

}
