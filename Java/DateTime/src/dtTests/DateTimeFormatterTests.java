/*=========================================================================

  Program:   Date Time Library
  File   :   DateTimeFormatterTests.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
package dtTests;

import dt.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 * A test harness to test the DateTimeFormatter class.
 *
 * @author Andrew J. P. Maclean
 */
public class DateTimeFormatterTests extends CommonTests {

  private final ArrayList<HashMap<String, String>> testData = new ArrayList<>(
      Arrays.asList(new HashMap<String, String>() {
                      /**
                       *
                       */
                      private static final long serialVersionUID = 1L;

                      {
                        put("MonthNameAbbrev", "Dec");
                        put("NamedDate", "Monday, 31 December 1900 15:12:11.123");
                        put("JD", "2415398.13346207175926");
                        put("Calendar", "Julian");
                        put("DayNameAbbrev", "Mon");
                        put("JDShort", "2415398.13346207");
                        put("DoW", "2");
                        put("DoY", "366.63346207");
                        put("IWD", "");
                        put("Date", "1900-December-31 15:12:11.123");
                        put("MonthName", "December");
                        put("DayName", "Monday");
                        put("LeapYear", "Yes");
                      }
                    },

          new HashMap<String, String>() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            {
              put("MonthNameAbbrev", "Jan");
              put("NamedDate", "Sunday, 13 January 1901 15:12:11.123");
              put("JD", "2415398.13346207175926");
              put("Calendar", "Gregorian");
              put("DayNameAbbrev", "Sun");
              put("JDShort", "2415398.13346207");
              put("DoW", "1");
              put("DoY", "13.63346207");
              put("IWD", "1901-02-7");
              put("Date", "1901-January-13 15:12:11.123");
              put("MonthName", "January");
              put("DayName", "Sunday");
              put("LeapYear", "No");
            }
          },

          new HashMap<String, String>() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            {
              put("MonthNameAbbrev", "Jan");
              put("NamedDate", "Sunday, 13 January 1901 15:12:11.123");
              put("JD", "2415398.13346207175926");
              put("Calendar", "Julian/Gregorian");
              put("DayNameAbbrev", "Sun");
              put("JDShort", "2415398.13346207");
              put("DoW", "1");
              put("DoY", "13.63346207");
              put("IWD", "1901-02-7");
              put("Date", "1901-January-13 15:12:11.123");
              put("MonthName", "January");
              put("DayName", "Sunday");
              put("LeapYear", "No");
            }
          },

          new HashMap<String, String>() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            {
              put("MonthNameAbbrev", "Jan");
              put("NamedDate", "Sunday, 13 Jan 1901 15:12:11.123");
              put("JD", "2415398.13346207175926");
              put("Calendar", "Julian/Gregorian");
              put("DayNameAbbrev", "Sun");
              put("JDShort", "2415398.13346207");
              put("DoW", "1");
              put("DoY", "13.63346207");
              put("IWD", "1901-02-7");
              put("Date", "1901-Jan-13 15:12:11.123");
              put("MonthName", "January");
              put("DayName", "Sunday");
              put("LeapYear", "No");
            }
          },

          new HashMap<String, String>() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            {
              put("MonthNameAbbrev", "Jan");
              put("NamedDate", "Sunday, 13 Jan 1901 15:12:11.123");
              put("JD", "2415398.13346207175926");
              put("Calendar", "Julian/Gregorian");
              put("DayNameAbbrev", "Sun");
              put("JDShort", "2415398.13346207");
              put("DoW", "1");
              put("DoY", "13.63346207");
              put("IWD", "1901-02-7");
              put("Date", "1901-01-13 15:12:11.123");
              put("MonthName", "January");
              put("DayName", "Sunday");
              put("LeapYear", "No");
            }
          },

          new HashMap<String, String>() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            {
              put("MonthNameAbbrev", "Jan");
              put("NamedDate", "Sun, 13 Jan 1901 15:12:11.123");
              put("JD", "2415398.13346207175926");
              put("Calendar", "Julian/Gregorian");
              put("DayNameAbbrev", "Sun");
              put("JDShort", "2415398.13346207");
              put("DoW", "1");
              put("DoY", "13.63346207");
              put("IWD", "1901-02-7");
              put("Date", "1901-01-13 15:12:11.123");
              put("MonthName", "January");
              put("DayName", "Sunday");
              put("LeapYear", "No");
            }
          },

          new HashMap<String, String>() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            {
              put("MonthNameAbbrev", "Jan");
              put("NamedDate", "1901-01-13 15:12:11.123");
              put("JD", "2415398.13346207175926");
              put("Calendar", "Julian/Gregorian");
              put("DayNameAbbrev", "Sun");
              put("JDShort", "2415398.13346207");
              put("DoW", "1");
              put("DoY", "13.63346207");
              put("IWD", "1901-02-7");
              put("Date", "1901-01-13 15:12:11.123");
              put("MonthName", "January");
              put("DayName", "Sunday");
              put("LeapYear", "No");
            }
          },

          new HashMap<String, String>() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            {
              put("MonthNameAbbrev", "Jan");
              put("NamedDate", "1901-01-13 15:12:11.123");
              put("JD", "2415398.13346207175926");
              put("Calendar", "Julian/Gregorian");
              put("DayNameAbbrev", "Sun");
              put("JDShort", "2415398.13346207");
              put("DoW", "7");
              put("DoY", "13.63346207");
              put("IWD", "1901-02-7");
              put("Date", "1901-01-13 15:12:11.123");
              put("MonthName", "January");
              put("DayName", "Sunday");
              put("LeapYear", "No");
            }
          },

          new HashMap<String, String>() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            {
              put("MonthNameAbbrev", "Dec");
              put("NamedDate", "Thursday, 31 December 1500 15:12:11.123");
              put("JD", "2269298.13346207175926");
              put("Calendar", "Julian/Gregorian");
              put("DayNameAbbrev", "Thu");
              put("JDShort", "2269298.13346207");
              put("DoW", "5");
              put("DoY", "366.63346207");
              put("IWD", "");
              put("Date", "1500-December-31 15:12:11.123");
              put("MonthName", "December");
              put("DayName", "Thursday");
              put("LeapYear", "Yes");
            }
          }));
  private final ArrayList<DT_TEST_DATA> dtTestData = new ArrayList<>();
  private final ArrayList<DT_TEST_DATA> dtTestDataNegative = new ArrayList<>();
  private final ArrayList<TI_TEST_DATA> ti = new ArrayList<>();
  private final DateTimeFormatter dtf = new DateTimeFormatter();
  private final int maxPrec = 15;

  public DateTimeFormatterTests() {
    this.Init();
  }

  private void Init() {

    DT_TEST_DATA tmp = new DT_TEST_DATA();

    tmp.jdt.SetDateTime(new YMDHMS(-13200, 9, 1, 23, 59, 59.99996), CALENDAR_TYPE.JULIAN_GREGORIAN);
    tmp.strDate = "-13200-09-02 00:00:00.0000";
    tmp.jdLongDate = "-3099997.5000000";
    tmp.jdLongDateAlt = "-3099997.5000000";
    tmp.doyStr = "246.00000000";
    dtTestData.add(new DT_TEST_DATA(tmp));

    tmp.jdt.SetDateTime(new YMDHMS(-4712, 1, 1, 23, 59, 59.99996), CALENDAR_TYPE.JULIAN_GREGORIAN);
    tmp.strDate = "-4712-01-02 00:00:00.0000";
    tmp.jdLongDate = "0.5000000";
    tmp.jdLongDateAlt = "0.5000000";
    tmp.doyStr = "2.00000000";
    dtTestData.add(new DT_TEST_DATA(tmp));

    tmp.jdt.SetDateTime(new YMDHMS(2013, 9, 6, 23, 59, 59.99996), CALENDAR_TYPE.JULIAN_GREGORIAN);
    tmp.strDate = "2013-09-07 00:00:00.0000";
    tmp.jdLongDate = "2456542.5000000";
    tmp.jdLongDateAlt = "2456542.5000000";
    tmp.doyStr = "250.00000000";
    dtTestData.add(new DT_TEST_DATA(tmp));

    tmp.jdt.SetDateTime(new YMDHMS(2013, 9, 6, 23, 59, 59.999499999), CALENDAR_TYPE.JULIAN_GREGORIAN);
    tmp.strDate = "2013-09-06 23:59:59.9995";
    tmp.jdLongDate = "2456542.5000000";
    tmp.jdLongDateAlt = "2456542.5000000";
    tmp.doyStr = "249.99999999";
    dtTestData.add(new DT_TEST_DATA(tmp));

    tmp.jdt.SetDateTime(new YMDHMS(-13200, 9, 1, 23, 59, 59.99995), CALENDAR_TYPE.JULIAN_GREGORIAN);
    tmp.strDate = "-13200-09-02 00:00:00";
    tmp.jdLongDate = "-3099997.5000000";
    tmp.jdLongDateAlt = "-3099998";
    tmp.doyStr = "246";
    dtTestData.add(new DT_TEST_DATA(tmp));

    tmp.jdt.SetDateTime(new YMDHMS(-4712, 1, 1, 23, 59, 59.99995), CALENDAR_TYPE.JULIAN_GREGORIAN);
    tmp.strDate = "-4712-01-02 00:00:00";
    tmp.jdLongDate = "0.5000000";
    tmp.jdLongDateAlt = "1";
    tmp.doyStr = "2";
    dtTestData.add(new DT_TEST_DATA(tmp));

    tmp.jdt.SetDateTime(new YMDHMS(2013, 9, 6, 23, 59, 59.99995), CALENDAR_TYPE.JULIAN_GREGORIAN);
    tmp.strDate = "2013-09-07 00:00:00";
    tmp.jdLongDate = "2456542.5000000";
    tmp.jdLongDateAlt = "2456543";
    tmp.doyStr = "250";
    dtTestData.add(new DT_TEST_DATA(tmp));

    tmp.jdt.SetDateTime(new YMDHMS(2013, 9, 6, 23, 59, 59.999499999), CALENDAR_TYPE.JULIAN_GREGORIAN);
    tmp.strDate = "2013-09-07 00:00:00";
    tmp.jdLongDate = "2456542.5000000";
    tmp.jdLongDateAlt = "2456543";
    tmp.doyStr = "250";
    dtTestData.add(new DT_TEST_DATA(tmp));

    tmp.jdt.SetDateTime(new YMDHMS(0, 1, 1, 23, 59, 59.999499999), CALENDAR_TYPE.JULIAN_GREGORIAN);
    tmp.strDate = "0-01-02 00:00:00";
    tmp.jdLongDate = "1721058.5000000";
    tmp.jdLongDateAlt = "1721059";
    tmp.doyStr = "2";
    dtTestData.add(new DT_TEST_DATA(tmp));

    tmp.jdt.SetDateTime(new YMDHMS(-1, 1, 1, 23, 59, 59.999499999), CALENDAR_TYPE.JULIAN_GREGORIAN);
    tmp.strDate = "-1-01-02 00:00:00";
    tmp.jdLongDate = "1720693.5000000";
    tmp.jdLongDateAlt = "1720694";
    tmp.doyStr = "2";
    dtTestData.add(new DT_TEST_DATA(tmp));

    tmp.jdt.SetDateTime(new YMDHMS(-1, 1, 1, 12, 0, 0), CALENDAR_TYPE.JULIAN_GREGORIAN);
    tmp.strDate = "-1-01-01 12:00:00";
    tmp.jdLongDate = "1720693.0000000";
    tmp.jdLongDateAlt = "1720693";
    tmp.doyStr = "2";
    dtTestData.add(new DT_TEST_DATA(tmp));

    // Some negative dates and times.
    tmp.jdt.SetDateTime(new YMDHMS(-13190, 8, 1, 0, 0, 0), CALENDAR_TYPE.JULIAN_GREGORIAN);
    tmp.strDate = "-13190-08-01 00:00:00";
    tmp.jdLongDate = "-3096377.5000000";
    tmp.jdLongDateAlt = "-3096377.5000000";
    tmp.doyStr = "213.00000000";
    dtTestDataNegative.add(new DT_TEST_DATA(tmp));

    tmp.jdt.SetDateTime(new YMDHMS(-13190, 8, 1, 6, 0, 0), CALENDAR_TYPE.JULIAN_GREGORIAN);
    tmp.strDate = "-13190-08-01 06:00:00";
    tmp.jdLongDate = "-3096377.2500000";
    tmp.jdLongDateAlt = "-3096377.2500000";
    tmp.doyStr = "213.25000000";
    dtTestDataNegative.add(new DT_TEST_DATA(tmp));

    tmp.jdt.SetDateTime(new YMDHMS(-13190, 8, 1, 12, 0, 0), CALENDAR_TYPE.JULIAN_GREGORIAN);
    tmp.strDate = "-13190-08-01 12:00:00";
    tmp.jdLongDate = "-3096377.0000000";
    tmp.jdLongDateAlt = "-3096377.0000000";
    tmp.doyStr = "213.50000000";
    dtTestDataNegative.add(new DT_TEST_DATA(tmp));

    tmp.jdt.SetDateTime(new YMDHMS(-13190, 8, 1, 18, 0, 0), CALENDAR_TYPE.JULIAN_GREGORIAN);
    tmp.strDate = "-13190-08-01 18:00:00";
    tmp.jdLongDate = "-3096376.7500000";
    tmp.jdLongDateAlt = "-3096376.7500000";
    tmp.doyStr = "213.75000000";
    dtTestDataNegative.add(new DT_TEST_DATA(tmp));

    tmp.jdt.SetDateTime(new YMDHMS(-4712, 1, 1, 0, 0, 0), CALENDAR_TYPE.JULIAN_GREGORIAN);
    tmp.strDate = "-4712-01-01 00:00:00";
    tmp.jdLongDate = "-0.5000000";
    tmp.jdLongDateAlt = "-0.5000000";
    tmp.doyStr = "1.00000000";
    dtTestDataNegative.add(new DT_TEST_DATA(tmp));

    tmp.jdt.SetDateTime(new YMDHMS(-4712, 1, 1, 6, 0, 0), CALENDAR_TYPE.JULIAN_GREGORIAN);
    tmp.strDate = "-4712-01-01 06:00:00";
    tmp.jdLongDate = "-0.2500000";
    tmp.jdLongDateAlt = "-0.2500000";
    tmp.doyStr = "1.25000000";
    dtTestDataNegative.add(new DT_TEST_DATA(tmp));

    tmp.jdt.SetDateTime(new YMDHMS(-4712, 1, 1, 12, 0, 0), CALENDAR_TYPE.JULIAN_GREGORIAN);
    tmp.strDate = "-4712-01-01 12:00:00";
    tmp.jdLongDate = "0.0000000";
    tmp.jdLongDateAlt = "0.0000000";
    tmp.doyStr = "1.50000000";
    dtTestDataNegative.add(new DT_TEST_DATA(tmp));

    tmp.jdt.SetDateTime(new YMDHMS(-4712, 1, 1, 18, 0, 0), CALENDAR_TYPE.JULIAN_GREGORIAN);
    tmp.strDate = "-4712-01-01 18:00:00";
    tmp.jdLongDate = "0.2500000";
    tmp.jdLongDateAlt = "0.2500000";
    tmp.doyStr = "1.75000000";
    dtTestDataNegative.add(new DT_TEST_DATA(tmp));

    tmp.jdt.SetDateTime(new YMDHMS(-4712, 1, 2, 0, 0, 0), CALENDAR_TYPE.JULIAN_GREGORIAN);
    tmp.strDate = "-4712-01-02 00:00:00";
    tmp.jdLongDate = "0.5000000";
    tmp.jdLongDateAlt = "0.5000000";
    tmp.doyStr = "2.00000000";
    dtTestDataNegative.add(new DT_TEST_DATA(tmp));

    TI_TEST_DATA tmpTi = new TI_TEST_DATA();

    tmpTi.ti.SetTimeInterval(new TimeInterval(0, 0));
    tmpTi.tidStr = "0.0000000";
    tmpTi.tiDHMS = "0d  00:00:00.0000";
    ti.add(new TI_TEST_DATA(tmpTi));

    tmpTi.ti.SetTimeInterval(new TimeInterval(-1, -0.5));
    tmpTi.tidStr = "-1.5000000";
    tmpTi.tiDHMS = "-1d -12:00:00.0000";
    ti.add(new TI_TEST_DATA(tmpTi));

    tmpTi.ti.SetTimeInterval(new TimeInterval(-1, 0.5));
    tmpTi.tidStr = "-0.5000000";
    tmpTi.tiDHMS = "-0d -12:00:00.0000";
    ti.add(new TI_TEST_DATA(tmpTi));

    tmpTi.ti.SetTimeInterval(new TimeInterval(1, -0.5));
    tmpTi.tidStr = "0.5000000";
    tmpTi.tiDHMS = "0d  12:00:00.0000";
    ti.add(new TI_TEST_DATA(tmpTi));

    tmpTi.ti.SetTimeInterval(new TimeInterval(11, -0.5));
    tmpTi.tidStr = "10.5000000";
    tmpTi.tiDHMS = "10d  12:00:00.0000";
    ti.add(new TI_TEST_DATA(tmpTi));

    tmpTi.ti.SetTimeInterval(new TimeInterval(10, 0.5));
    tmpTi.tidStr = "10.5000000";
    tmpTi.tiDHMS = "10d  12:00:00.0000";
    ti.add(new TI_TEST_DATA(tmpTi));

    tmpTi.ti.SetTimeInterval(new TimeInterval(1, -11.5));
    tmpTi.tidStr = "-10.5000000";
    tmpTi.tiDHMS = "-10d -12:00:00.0000";
    ti.add(new TI_TEST_DATA(tmpTi));

    tmpTi.ti.SetTimeInterval(new TimeInterval(-10, 0.5));
    tmpTi.tidStr = "-9.5000000";
    tmpTi.tiDHMS = "-9d -12:00:00.0000";
    ti.add(new TI_TEST_DATA(tmpTi));

    tmpTi.ti.SetTimeInterval(new TimeInterval(10, 0.5));
    tmpTi.tidStr = "10.5000000";
    tmpTi.tiDHMS = "10d  12:00:00.0000";
    ti.add(new TI_TEST_DATA(tmpTi));

    tmpTi.ti.SetTimeInterval(new TimeInterval(1, -11.5));
    tmpTi.tidStr = "-10.5000000";
    tmpTi.tiDHMS = "-10d -12:00:00.0000";
    ti.add(new TI_TEST_DATA(tmpTi));

    tmpTi.ti.SetTimeInterval(new TimeInterval(10, 0.5));
    tmpTi.tidStr = "11";
    tmpTi.tiDHMS = "10d  12:00:00";
    ti.add(new TI_TEST_DATA(tmpTi));

    tmpTi.ti.SetTimeInterval(new TimeInterval(1, -11.5));
    tmpTi.tidStr = "-11";
    tmpTi.tiDHMS = "-10d -12:00:00";
    ti.add(new TI_TEST_DATA(tmpTi));

  }

  /**
   * Run all the tests.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * string of failed tests.
   */
  public DTPair<Boolean, String> TestAll() {

    ArrayList<DTPair<Boolean, String>> messages = new ArrayList<>();

    messages.add(this.TestFormatting());
    messages.add(this.TestFormatYMDHMS());
    messages.add(this.TestFormatJDLong());
    messages.add(this.TestFormatTimeInterval());
    messages.add(this.TestFormatTimeIntervalDHMS());
    messages.add(this.TestFormatDOY());
    messages.add(this.TestFormatISOWeek());
    messages.add(this.TestOtherFormatting());

    return this.AggregateResults("DateTimeFormatter Tests.", messages);
  }

  /**
   * Run the test for parsing a decimal string.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestFormatting() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing GetFormattedDates.", 2);

    DateTimeFormat fmt = new DateTimeFormat();
    fmt.dayName = DAY_NAME.UNABBREVIATED;
    fmt.monthName = MONTH_NAME.UNABBREVIATED;

    fmt.calendar = CALENDAR_TYPE.JULIAN;
    JulianDateTime jdt = new JulianDateTime(1900, 12, 31, 15, 12, 11.123, fmt.calendar);
    HashMap<String, String> dates = this.dtf.GetFormattedDates(jdt, fmt);
    int testNum = 0;
    this.DoCheck(testNum, dates, p);

    fmt.calendar = CALENDAR_TYPE.GREGORIAN;
    dates = this.dtf.GetFormattedDates(jdt, fmt);
    testNum += 1;
    DoCheck(testNum, dates, p);

    fmt.calendar = CALENDAR_TYPE.JULIAN_GREGORIAN;
    dates = this.dtf.GetFormattedDates(jdt, fmt);
    testNum += 1;
    DoCheck(testNum, dates, p);

    fmt.monthName = MONTH_NAME.ABBREVIATED;
    dates = this.dtf.GetFormattedDates(jdt, fmt);
    testNum += 1;
    DoCheck(testNum, dates, p);

    fmt.monthName = MONTH_NAME.NONE;
    dates = this.dtf.GetFormattedDates(jdt, fmt);
    testNum += 1;
    DoCheck(testNum, dates, p);

    fmt.dayName = DAY_NAME.ABBREVIATED;
    dates = this.dtf.GetFormattedDates(jdt, fmt);
    testNum += 1;
    DoCheck(testNum, dates, p);

    fmt.dayName = DAY_NAME.NONE;
    dates = this.dtf.GetFormattedDates(jdt, fmt);
    testNum += 1;
    DoCheck(testNum, dates, p);

    fmt.firstDoW = FIRST_DOW.MONDAY;
    dates = this.dtf.GetFormattedDates(jdt, fmt);
    testNum += 1;
    DoCheck(testNum, dates, p);

    fmt.dayName = DAY_NAME.UNABBREVIATED;
    fmt.monthName = MONTH_NAME.UNABBREVIATED;
    fmt.firstDoW = FIRST_DOW.SUNDAY;

    jdt = new JulianDateTime(1500, 12, 31, 15, 12, 11.123, fmt.calendar);
    dates = this.dtf.GetFormattedDates(jdt, fmt);
    testNum += 1;
    DoCheck(testNum, dates, p);
    p.second += "\n";

    return p;
  }

  /**
   * Test if two hash maps are equivalent.
   *
   * @param testNum The test number.
   * @param a       The map to be tested.
   * @param p       The result to be updated.
   * @return The updated result
   */
  private DTPair<Boolean, String> DoCheck(int testNum, HashMap<String, String> a, DTPair<Boolean, String> p) {

    p.second += "* Test " + testNum + " ";

    DTPair<Boolean, String> p1 = new DTPair<>(true, "");

    if (a.equals(testData.get(testNum))) {
      p1.first = true;
      p1.second = "passed.\n";
    } else {
      p1.first = false;
      p1.second = "failed:\n";
      p1.second += this.WhereFailed(a, testData.get(testNum));
    }
    p.first &= p1.first;
    p.second += p1.second;
    return p;
  }

  /**
   * Report differences between two hash maps.
   *
   * @param a The observed map.
   * @param b The expected map.
   * @return The differences.
   */
  private String WhereFailed(HashMap<String, String> a, HashMap<String, String> b) {
    StringBuilder s = new StringBuilder();
    if (!this.MapKeyCompare(a, b)) {
      s.append(" - The keys do not match or the maps are not the same size.\n");
    } else {
      // Find the non-equivalent values.
      for (Entry<String, String> entry : a.entrySet()) {
        String key = entry.getKey();
        if (!(entry.getValue().equals(b.get(key)))) {
          s.append("    - For: ").append(key).append(", expected ").append(b.get(key)).append(", got ").append(entry.getValue()).append("\n");
        }
      }
    }
    return s.toString();
  }

  /**
   * Compare the keys in two maps.
   *
   * @param a The observed map.
   * @param b The expected map.
   * @return true if the maps are the same size and the keys are the same.
   */
  private boolean MapKeyCompare(HashMap<String, String> a, HashMap<String, String> b) {
    if (a.size() == b.size()) {
      for (Entry<String, String> entry : a.entrySet()) {
        if (!b.containsKey(entry.getKey())) {
          return false;
        }
      }
      return true;
    }
    return false;

  }

  /**
   * Run the test for formatting as YMDHMS.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestFormatYMDHMS() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing  FormatYMDHMS().", 2);

    DTPair<Boolean, String> p1;

    int idx = 0;
    DateTimeFormat fmt = new DateTimeFormat();
    fmt.calendar = CALENDAR_TYPE.JULIAN_GREGORIAN;
    fmt.precisionSeconds = 9;

    for (DT_TEST_DATA q : this.dtTestData) {
      String str;
      if (idx < 4) {
        dtf.maximumPrecision = 4;
        str = dtf.FormatAsYMDHMS(q.jdt, fmt);
      } else {
        dtf.maximumPrecision = 0;
        str = dtf.FormatAsYMDHMS(q.jdt, fmt);
      }
      p1 = this.Compare(str, q.strDate, "* ");
      p.first &= p1.first;
      p.second += p1.second;
      ++idx;
    }

    idx = 0;
    dtf.maximumPrecision = this.maxPrec;
    for (DT_TEST_DATA q : this.dtTestData) {
      String str;
      if (idx < 4) {
        fmt.precisionSeconds = 4;
        str = dtf.FormatAsYMDHMS(q.jdt, fmt);
      } else {
        fmt.precisionSeconds = 0;
        str = dtf.FormatAsYMDHMS(q.jdt, fmt);
      }
      p1 = this.Compare(str, q.strDate, "* ");
      p.first &= p1.first;
      p.second += p1.second;
      ++idx;
    }

    String res1 = "2013-Sep-07 00:00:00.0000";
    String res2 = "2013-September-07 00:00:00.0000";

    dtf.maximumPrecision = 4;
    fmt.precisionSeconds = 9;
    fmt.monthName = MONTH_NAME.ABBREVIATED;
    String str = dtf.FormatAsYMDHMS(this.dtTestData.get(2).jdt, fmt);
    p1 = this.Compare(str, res1, "* ");
    p.first &= p1.first;
    p.second += p1.second;

    fmt.monthName = MONTH_NAME.UNABBREVIATED;
    str = dtf.FormatAsYMDHMS(this.dtTestData.get(2).jdt, fmt);
    p1 = this.Compare(str, res2, "* ");
    p.first &= p1.first;
    p.second += p1.second;

    fmt.monthName = MONTH_NAME.NONE;
    dtf.maximumPrecision = this.maxPrec;

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for formatting as JDLong.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestFormatJDLong() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing FormatJDLong().", 2);

    DTPair<Boolean, String> p1;

    int idx = 0;
    DateTimeFormat fmt = new DateTimeFormat();
    fmt.precisionFoD = 12;

    for (DT_TEST_DATA q : this.dtTestData) {
      String str;
      if (idx < 4) {
        dtf.maximumPrecision = 7;
        str = dtf.FormatJDLong(q.jdt.GetJDFullPrecision(), fmt);
      } else {
        dtf.maximumPrecision = 0;
        str = dtf.FormatJDLong(q.jdt.GetJDFullPrecision(), fmt);
      }
      p1 = this.Compare(str, q.jdLongDateAlt, "* ");
      p.first &= p1.first;
      p.second += p1.second;
      ++idx;
    }

    dtf.maximumPrecision = this.maxPrec;
    idx = 0;
    for (DT_TEST_DATA q : this.dtTestData) {
      String str;
      if (idx < 4) {
        dtf.maximumPrecision = 7;
        str = dtf.FormatJDLong(q.jdt.GetJDFullPrecision(), fmt);
      } else {
        dtf.maximumPrecision = 0;
        str = dtf.FormatJDLong(q.jdt.GetJDFullPrecision(), fmt);
      }
      p1 = this.Compare(str, q.jdLongDateAlt, "* ");
      p.first &= p1.first;
      p.second += p1.second;
      ++idx;
    }

    dtf.maximumPrecision = 7;
    fmt.precisionFoD = 12;
    // Look at negative dates in more detail.
    for (DT_TEST_DATA q : this.dtTestDataNegative) {
      String str = dtf.FormatJDLong(q.jdt.GetJDFullPrecision(), fmt);
      p1 = this.Compare(str, q.jdLongDateAlt, "* ");
      p.first &= p1.first;
      p.second += p1.second;
      ++idx;
    }

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for formatting as TimeInterval.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestFormatTimeInterval() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing FormatTimeInterval().", 2);

    DTPair<Boolean, String> p1;

    DateTimeFormat fmt = new DateTimeFormat();

    int idx = 0;
    dtf.maximumPrecision = 7;
    for (TI_TEST_DATA q : ti) {
      String str;
      if (idx < 10) {
        fmt.precisionFoD = 12;
        str = dtf.FormatTimeInterval(q.ti, fmt);
      } else {
        fmt.precisionFoD = 0;
        str = dtf.FormatTimeInterval(q.ti, fmt);
      }
      p1 = this.Compare(str, q.tidStr, "* ");
      p.first &= p1.first;
      p.second += p1.second;
      ++idx;
    }

    idx = 0;
    dtf.maximumPrecision = this.maxPrec;
    for (TI_TEST_DATA q : ti) {
      String str;
      if (idx < 10) {
        fmt.precisionFoD = 7;
        str = dtf.FormatTimeInterval(q.ti, fmt);
      } else {
        fmt.precisionFoD = 0;
        str = dtf.FormatTimeInterval(q.ti, fmt);
      }
      p1 = this.Compare(str, q.tidStr, "* ");
      p.first &= p1.first;
      p.second += p1.second;
      ++idx;
    }

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for formatting as TimeIntervalDHMS.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestFormatTimeIntervalDHMS() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing FormatTimeIntervalDHMS().", 2);

    DTPair<Boolean, String> p1;

    DateTimeFormat fmt = new DateTimeFormat();

    int idx = 0;
    for (TI_TEST_DATA q : this.ti) {
      String str;
      if (idx < 10) {
        dtf.maximumPrecision = 4;
        fmt.precisionSeconds = 12;
        str = dtf.FormatTimeIntervalDHMS(q.ti, fmt);
      } else {
        dtf.maximumPrecision = 12;
        fmt.precisionSeconds = 0;
        str = dtf.FormatTimeIntervalDHMS(q.ti, fmt);
      }
      p1 = this.Compare(str, q.tiDHMS, "* ");
      p.first &= p1.first;
      p.second += p1.second;
      ++idx;
    }

    idx = 0;
    dtf.maximumPrecision = this.maxPrec;
    for (TI_TEST_DATA q : this.ti) {
      String str;
      if (idx < 10) {
        fmt.precisionSeconds = 4;
        str = dtf.FormatTimeIntervalDHMS(q.ti, fmt);
      } else {
        fmt.precisionSeconds = 0;
        str = dtf.FormatTimeIntervalDHMS(q.ti, fmt);
      }
      p1 = this.Compare(str, q.tiDHMS, "* ");
      p.first &= p1.first;
      p.second += p1.second;
      ++idx;
    }

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for formatting as DOY.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestFormatDOY() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing FormatDOY().", 2);

    DTPair<Boolean, String> p1;

    DateTimeFormat fmt = new DateTimeFormat();
    fmt.calendar = CALENDAR_TYPE.JULIAN_GREGORIAN;

    int idx = 0;
    dtf.maximumPrecision = 8;
    for (DT_TEST_DATA q : this.dtTestData) {
      String str;
      if (idx < 4) {
        fmt.precisionFoD = 12;
        str = dtf.FormatDOY(q.jdt, fmt);
      } else {
        fmt.precisionFoD = 0;
        str = dtf.FormatDOY(q.jdt, fmt);
      }
      p1 = this.Compare(str, q.doyStr, "* ");
      p.first &= p1.first;
      p.second += p1.second;
      ++idx;
    }

    idx = 0;
    dtf.maximumPrecision = this.maxPrec;
    for (DT_TEST_DATA q : this.dtTestData) {
      String str;
      if (idx < 4) {
        fmt.precisionFoD = 8;
        str = dtf.FormatDOY(q.jdt, fmt);
      } else {
        fmt.precisionFoD = 0;
        str = dtf.FormatDOY(q.jdt, fmt);
      }
      p1 = this.Compare(str, q.doyStr, "* ");
      p.first &= p1.first;
      p.second += p1.second;
      ++idx;
    }

    dtf.maximumPrecision = 8;
    fmt.precisionFoD = 12;
    // Look at negative dates in more detail.
    for (DT_TEST_DATA q : this.dtTestDataNegative) {
      String str = dtf.FormatDOY(q.jdt, fmt);
      p1 = this.Compare(str, q.doyStr, "* ");
      p.first &= p1.first;
      p.second += p1.second;
      ++idx;
    }
    dtf.maximumPrecision = this.maxPrec;

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for formatting as ISOWeek.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestFormatISOWeek() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing FormatISOWeek().", 2);

    JulianDateTime dt = new JulianDateTime(2008, 1, 1, 0, 0, 0, CALENDAR_TYPE.JULIAN_GREGORIAN);
    String expected = "2008-01-2"; // Year-week-day of the week.
    String str = dtf.FormatISOWeek(dt);
    DTPair<Boolean, String> p1;
    p1 = this.Compare(str, expected, "* ");
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for all the other formatting options.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestOtherFormatting() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing other formatting functions.", 2);

    DateTimeFormat fmt = new DateTimeFormat();
    fmt.calendar = CALENDAR_TYPE.JULIAN_GREGORIAN;

    JulianDateTime jd = new JulianDateTime(2012, 1, 3, 9, 46, 12.3215, fmt.calendar);
    String str = dtf.FormatAsYMDHMS(jd, fmt);
    String expected = "2012-01-03 09:46:12.322";
    DTPair<Boolean, String> p1;
    p1 = this.Compare(str, expected, "* ");
    p.first &= p1.first;
    p.second += "FormatAsYMDHMS() " + p1.second;

    fmt.dayName = DAY_NAME.UNABBREVIATED;
    fmt.monthName = MONTH_NAME.UNABBREVIATED;
    str = dtf.FormatDateWithNames(jd, fmt);
    expected = "Tuesday, 03 January 2012 09:46:12.322";
    p1 = this.Compare(str, expected, "* ");
    p.first &= p1.first;
    p.second += "FormatDateWithNames()" + p1.second;

    fmt.precisionSeconds = 0;
    str = dtf.FormatDateWithNames(jd, fmt);
    expected = "Tuesday, 03 January 2012 09:46:12";
    p1 = this.Compare(str, expected, "* ");
    p.first &= p1.first;
    p.second += "FormatDateWithNames()" + p1.second;

    fmt.dayName = DAY_NAME.ABBREVIATED;
    fmt.monthName = MONTH_NAME.ABBREVIATED;
    fmt.precisionSeconds = 0;
    str = dtf.FormatDateWithNames(jd, fmt);
    expected = "Tue, 03 Jan 2012 09:46:12";
    p1 = this.Compare(str, expected, "* ");
    p.first &= p1.first;
    p.second += "FormatDateWithNames()" + p1.second;

    fmt.dayName = DAY_NAME.NONE;
    fmt.monthName = MONTH_NAME.NONE;
    fmt.precisionSeconds = 3;

    str = dtf.FormatAsYMD(jd, fmt);
    expected = "2012-01-03";
    p1 = this.Compare(str, expected, "* ");
    p.first &= p1.first;
    p.second += "FormatAsYMDHMS() " + p1.second;

    str = dtf.FormatAsHMS(jd, fmt);
    expected = "09:46:12.322";
    p1 = this.Compare(str, expected, "* ");
    p.first &= p1.first;
    p.second += "FormatAsHMS() " + p1.second;

    str = dtf.FormatJDShort(jd, fmt);
    expected = "2455929.90708705";
    p1 = this.Compare(str, expected, "* ");
    p.first &= p1.first;
    p.second += "FormatJDShort() " + p1.second;

    TimeZones tz = new TimeZones();
    double tzDiff = tz.GetTimeZoneValue("UT-09:30").second;
    HM tzHM = new HM((int) (tzDiff / 3600.0), (tzDiff - (int) (tzDiff / 3600.0) * 3600.0) / 60.0);
    str = dtf.FormatTZAsHM(tzHM);
    expected = "-0930";
    p1 = this.Compare(str, expected, "* ");
    p.first &= p1.first;
    p.second += "FormatTZAsHM() " + p1.second;

    tzDiff = tz.GetTimeZoneValue("UT+12:45").second;
    tzHM = new HM((int) (tzDiff / 3600.0), (tzDiff - (int) (tzDiff / 3600.0) * 3600.0) / 60.0);
    str = dtf.FormatTZAsHM(tzHM);
    expected = "+1245";
    p1 = this.Compare(str, expected, "* ");
    p.first &= p1.first;
    p.second += "FormatTZAsHM() " + p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * A convenience class holding date data used by the DateTimeFormatterTests.
   * <p>
   * Emulates a struct in C++
   *
   * @author Andrew J. P. Maclean
   */
  public class DT_TEST_DATA {
    /**
     * The julian date time.
     */
    public JulianDateTime jdt = new JulianDateTime();
    /**
     * The corresponding date as a string.
     */
    public String strDate;
    /**
     * The corresponding date as a julian date expressed as a string.
     */
    public String jdLongDate;
    /**
     * The corresponding date as a julian date expressed as a string.
     */
    public String jdLongDateAlt;
    /**
     * The day of the year expressed as a string.
     */
    public String doyStr;

    public DT_TEST_DATA() {
    }

    /**
     * @param td the test date.
     */
    public DT_TEST_DATA(DT_TEST_DATA td) {
      this.jdt = new JulianDateTime(td.jdt);
      this.strDate = td.strDate;
      this.jdLongDate = td.jdLongDate;
      this.jdLongDateAlt = td.jdLongDateAlt;
      this.doyStr = td.doyStr;
    }
  }

  /**
   * A convenience class holding time interval data used by the
   * DateTimeFormatterTests.
   * <p>
   * Emulates a struct in C++
   *
   * @author Andrew J. P. Maclean
   */
  public class TI_TEST_DATA {
    /**
     * The time interval.
     */
    public TimeInterval ti = new TimeInterval();
    /**
     * The corresponding time interval as a decimal string.
     */
    public String tidStr;
    /**
     * The corresponding time interval as string of days hours minutes and
     * seconds.
     */
    public String tiDHMS;

    public TI_TEST_DATA() {
    }

    /**
     * @param ti the test time interval.
     */
    public TI_TEST_DATA(TI_TEST_DATA ti) {
      this.ti = new TimeInterval(ti.ti);
      this.tidStr = ti.tidStr;
      this.tiDHMS = ti.tiDHMS;
    }
  }

}
