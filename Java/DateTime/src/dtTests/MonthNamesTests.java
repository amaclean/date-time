/*=========================================================================

  Program:   Date Time Library
  File   :   MonthNamesTests.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
package dtTests;

import dt.DTPair;
import dt.MonthNames;

import java.util.ArrayList;

/**
 * A test harness to test the MonthNames class.
 *
 * @author Andrew J. P. Maclean
 */
public class MonthNamesTests extends CommonTests {

  private final MonthNames mn = new MonthNames();

  /**
   * Run all the tests.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * string of failed tests.
   */
  public DTPair<Boolean, String> TestAll() {
    ArrayList<DTPair<Boolean, String>> messages = new ArrayList<>();

    messages.add(this.TestConstructors());
    messages.add(this.TestAssignment());
    messages.add(this.TestComparisonOperators());
    messages.add(this.TestMonthNames());

    return this.AggregateResults("MonthNames Tests.", messages);

  }

  /**
   * Run the tests for the constructors.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestConstructors() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing constructors.", 2);

    p.second += "* MonthNames() ";
    MonthNames m1 = new MonthNames(this.mn);
    DTPair<Boolean, String> p1 = this.PassFail(m1.eq(mn));
    p.first &= p1.first;
    p.second += "<br>" + p1.second;

    p.second += "\n";
    return p;
  }

  /**
   * Run the tests for the assignment.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestAssignment() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing assignment.", 2);

    p.second += "* = ";
    MonthNames m1 = new MonthNames();
    m1.SetMonthName(1, "99th month!");
    m1.assign(this.mn);
    DTPair<Boolean, String> p1 = this.PassFail(m1.eq(mn));
    p.first &= p1.first;
    p.second += "<br>" + p1.second;

    p.second += "\n";
    return p;
  }

  /**
   * Run the tests for the comparison operators.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestComparisonOperators() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing comparison operators.", 2);

    p.second += "* == ";
    MonthNames m1 = new MonthNames(this.mn);
    DTPair<Boolean, String> p1 = this.PassFail(m1.eq(mn));
    p.first &= p1.first;
    p.second += "<br>" + p1.second;

    p.second += "* != ";
    m1.SetMonthName(1, "FIIK");
    p1 = this.PassFail(m1.ne(mn));
    p.first &= p1.first;
    p.second += "<br>" + p1.second;

    p.second += "\n";
    return p;
  }

  /**
   * Run the tests for month names.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestMonthNames() {

    DTPair<Boolean, String> p = new DTPair<>(true, null);
    p.second = "-----------------------------------------------------------------------------\n";
    p.second += "MonthNames, testing month names.\n";
    p.second += "-----------------------------------------------------------------------------\n";

    DTPair<Boolean, String> p1 = new DTPair<>(true, null);

    p.second += "* Month names and abbreviations.\n";
    p.first = true;
    StringBuilder fail = new StringBuilder();
    boolean ok = true;
    int abbrevLength = 3;
    for (int i = 1; i < 13; ++i) {
      String name = this.mn.GetMonthName(i);
      String abbrev = this.mn.GetAbbreviatedMonthName(i, abbrevLength);
      if (!name.substring(0, abbrevLength).equals(abbrev)) {
        ok &= false;
        fail.append("  - (").append(name).append(", ").append(abbrev).append(")\n");
      }
    }
    if (ok) {
      p1.first = true;
      p1.second = "<br>Passed.\n";
    } else {
      p1.first = false;
      p1.second = String.format("<br>Failed month names:\n%s\n", fail.toString());
    }
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* Month names and unabbreviated names.\n";
    p.first = true;
    fail = new StringBuilder();
    ok = true;
    for (int i = 1; i < 13; ++i) {
      String name = this.mn.GetMonthName(i);
      String abbrev = this.mn.GetAbbreviatedMonthName(i, name.length());
      if (!name.equals(abbrev)) {
        ok &= false;
        fail.append("  - (").append(name).append(", ").append(abbrev).append(")\n");
      }
    }
    if (ok) {
      p1.first = true;
      p1.second = "<br>Passed.\n";
    } else {
      p1.first = false;
      p1.second = String.format("<br>Failed month names:\n%s\n", fail.toString());
    }
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* Month Name range error.\n";
    p1 = this.PassFail(this.mn.GetMonthName(0).isEmpty() || this.mn.GetMonthName(13).isEmpty());
    p.first &= p1.first;
    p.second += "<br>" + p1.second;

    p.second += "*  Abbreviated Month Name range error.\n";
    p1 = this.PassFail(this.mn.GetAbbreviatedMonthName(0, abbrevLength).isEmpty()
        || this.mn.GetAbbreviatedMonthName(13, abbrevLength).isEmpty());
    p.first &= p1.first;
    p.second += "<br>" + p1.second;

    p.second += "* Set Month Name.\n";
    String m = this.mn.GetMonthName(9);
    this.mn.SetMonthName(9, "Month 9");
    p1 = this.PassFail(this.mn.GetMonthName(9).equals("Month 9"));
    this.mn.SetMonthName(9, m);
    p.first &= p1.first;
    p.second += "<br>" + p1.second;

    p.second += "\n";

    return p;
  }

}
