/*=========================================================================

  Program:   Date Time Library
  File   :   FoDHMSTests.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
package dtTests;

import dt.*;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * A test class used to test fraction of the day to HMS conversions.
 *
 * @author Andrew J. P. Maclean
 */
public class FoDHMSTests extends CommonTests {

  private final DateTimeConversions dtc = new DateTimeConversions();
  private final DateTimeBase dtb = new DateTimeBase();
  // This vector is filled with test times consisting of pairs of
  // Julian date fractions of the day (they run from 12 Noon)
  // and hours minutes and seconds expressed as a fraction
  // of the day (they run from midnight).
  private final ArrayList<DTPair<Double, Double>> testFoD = new ArrayList<>();
  // This vector is filled with test dates consisting of pairs of
  // ymdhms and Julian Date.
  private final ArrayList<DTPair<YMDHMS, Double>> testDates = new ArrayList<>();

  public FoDHMSTests() {
    Init();
  }

  /**
   * Initialise the class.
   */
  private void Init() {
    int steps = 7; // Number of jdFoD and hmsFoD elements.
    for (int i = 0; i < steps; ++i) {
      double jdFoD = i / (double) steps;
      double hmsFoD = (jdFoD >= 0.5) ? jdFoD - 0.5 : jdFoD + 0.5;
      testFoD.add(new DTPair<>(jdFoD, hmsFoD));
    }

    // We manually fill these vectors with known values taken from DE431.
    ArrayList<String> DE431Dates = new ArrayList<>();
    DE431Dates.add("-13200 08 15 -3100015.5");
    DE431Dates.add("-13100 01 01 -3063717.5");
    DE431Dates.add("-13100 01 02 -3063716.5");
    DE431Dates.add("-4713 12 31       -1.5");
    DE431Dates.add("-4712 01 01       -0.5");
    DE431Dates.add("-4712 01 02        0.5");
    DE431Dates.add("-4712 01 03        1.5");
    DE431Dates.add(" 8000 01 01  4642999.5");
    DE431Dates.add(" 8000 01 02  4643000.5");
    DE431Dates.add("17191 03 01  8000002.5");
    DE431Dates.add("17191 03 15  8000016.5");

    double jd;
    for (String q : DE431Dates) {
      YMDHMS ymdhms = new YMDHMS();
      Scanner a = new Scanner(q);
      ymdhms.ymd.year = a.nextInt();
      ymdhms.ymd.month = a.nextInt();
      ymdhms.ymd.day = a.nextInt();
      jd = a.nextDouble();
      a.close();
      testDates.add(new DTPair<>(ymdhms, jd));
    }

  }

  /**
   * Run all the tests.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * string of failed tests.
   */
  public DTPair<Boolean, String> TestAll() {
    ArrayList<DTPair<Boolean, String>> messages = new ArrayList<>();

    messages.add(this.TestCalendarDateToJulianDate());
    messages.add(this.TestJulianDateToCalendarDate());
    messages.add(this.TestFodHMSJD());
    messages.add(this.TestJD());
    messages.add(this.TestJD1());
    messages.add(this.TestJD2());
    messages.add(this.TestCalendar());

    return this.AggregateResults("FoDHMS Tests.", messages);

  }

  /**
   * Testing Calendar Date -> (jdn, fod) -> Julian Date.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestCalendarDateToJulianDate() {
    CALENDAR_TYPE calendar = CALENDAR_TYPE.JULIAN_GREGORIAN;

    DTPair<Boolean, String> p = this.MakeHeader("Testing Calendar Date -> (jdn, fod) -> Julian Date.", 2);

    JulianDateTime jdp = new JulianDateTime();
    boolean pass = true;
    for (DTPair<YMDHMS, Double> q : testDates) {
      jdp.SetDateTime(q.first, calendar);
      if (jdp.GetJDDouble() != q.second) {
        pass = false;
        String fmt = "* Failed, for: %s, expected: %10.2f got: %10.2f, difference: %15.12f\n";
        String s = String.format(fmt, q.first.GetString(), q.second, jdp.GetJDDouble(),
            jdp.GetJDDouble() - q.second);
        p.first &= false;
        p.second += s;
      }
    }
    if (pass) {
      p.first = true;
      p.second = "* Passed.\n";
    }

    p.second += "\n";

    return p;
  }

  /**
   * Testing Julian Date -> (jdn, fod) -> Calendar Date.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestJulianDateToCalendarDate() {
    CALENDAR_TYPE calendar = CALENDAR_TYPE.JULIAN_GREGORIAN;

    DTPair<Boolean, String> p = this.MakeHeader("Testing Julian Date -> (jdn, fod) -> Calendar Date.", 2);

    boolean pass = true;
    for (DTPair<YMDHMS, Double> q : testDates) {
      JulianDateTime jdp = new JulianDateTime(0, q.second);
      YMDHMS ymdhms = jdp.GetDateTime(calendar);
      if (!ymdhms.IsClose(q.first, dtb.rel_seconds)) {
        pass = false;
        String fmt = "* Failed, for: %10.2f, expected: %s got: %s\n";
        String s = String.format(fmt, q.second, q.first.GetString(), ymdhms.GetString());
        p.first &= false;
        p.second += s;
      }
    }
    if (pass) {
      p.first = true;
      p.second = "* Passed.\n";
    }

    p.second += "\n";

    return p;
  }

  /**
   * Testing jd -> jdn, hms -> calendar date -> jd.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestFodHMSJD() {
    CALENDAR_TYPE calendar = CALENDAR_TYPE.JULIAN_GREGORIAN;

    DTPair<Boolean, String> p = this.MakeHeader("Testing (jdn, fod) -> (jdn, hms) -> calendar date -> (jdn, fod).",
        2);

    boolean pass = true;
    JulianDateTime jdp = new JulianDateTime();
    for (DTPair<YMDHMS, Double> q : testDates) {
      jdp.SetDateTime(0, q.second);
      for (DTPair<Double, Double> r : testFoD) {
        jdp.SetFoD(r.first);

        DTPair<Integer, Double> days_hms = jdp.FoDToHMS(jdp.GetJDN(), jdp.GetFoD());
        DTPair<Integer, Double> jd_fod = jdp.HMSToFoD(days_hms.first, days_hms.second);
        if (jd_fod.second >= 0.5) {
          // The next day since we started at 12h of the current day.
          jd_fod.first += 1;
        }
        YMDHMS ymdhms = new YMDHMS(dtc.jd2cdJG(jd_fod.first), dtc.h2hms(r.second * 24.0));
        JulianDateTime jdt1 = new JulianDateTime();
        jdt1.SetDateTime(ymdhms, calendar);

        if (!jdp.IsClose(jdt1, dtb.rel_fod)) {
          pass = false;
          String fmt = "* Failed, jd: (%8d %5.2f) hms: %5.2f days (as hms): (%8d %5.2f) %s jd: (%8d %5.2f)\n";
          String s = String.format(fmt, jdp.GetJDN(), jdp.GetFoD(), r.second, days_hms.first, days_hms.second,
              ymdhms.GetString(), jdt1.GetJDN(), jdt1.GetFoD());
          p.first &= false;
          p.second += s;
        }
      }
    }
    if (pass) {
      p.first = true;
      p.second = "* Passed.\n";
    }

    p.second += "\n";

    return p;
  }

  /**
   * Testing jd -> jdn, hms -> calendar date -> jd.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestJD() {
    CALENDAR_TYPE calendar = CALENDAR_TYPE.JULIAN_GREGORIAN;

    DTPair<Boolean, String> p = this.MakeHeader("Testing jd -> jdn, hms -> calendar date -> jd.", 2);

    String fmt = "* Failed, jd: (%8d %5.2f) = %12.2f from GetJDDouble(): %12.2f, difference %15.12f\n";
    boolean pass = true;
    for (DTPair<YMDHMS, Double> q : testDates) {
      for (DTPair<Double, Double> r : testFoD) {
        YMDHMS ymdhms = new YMDHMS(dtc.jd2cdJG(q.second.intValue()), dtc.h2hms(r.second * 24.0));
        JulianDateTime jdt1 = new JulianDateTime();
        jdt1.SetDateTime(ymdhms, calendar);
        double jdt2 = jdt1.GetJDDouble();

        if (!this.dtb.isclose(jdt2, jdt1.GetJDN() + jdt1.GetFoD(), dtb.rel_fod)) {
          pass = false;
          String s = String.format(fmt, jdt1.GetJDN(), jdt1.GetFoD(), jdt1.GetJDN() + jdt1.GetFoD(), jdt2,
              jdt2 - (jdt1.GetJDN() + jdt1.GetFoD()));
          p.first &= false;
          p.second += s;
        }
      }
    }
    if (pass) {
      p.first = true;
      p.second = "* Passed.\n";
    }

    p.second += "\n";

    return p;
  }

  /**
   * Testing jd -> julian date time -> jd.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestJD1() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing jd -> julian date time -> jd.", 2);

    boolean pass = true;
    for (DTPair<YMDHMS, Double> q : testDates) {
      for (DTPair<Double, Double> r : testFoD) {
        double jd = q.second + r.first;
        JulianDateTime jdt = new JulianDateTime(0, jd);
        double jd1 = jdt.GetJDDouble();

        if (!this.dtb.isclose(jd, jd1, dtb.rel_fod)) {
          pass = false;
          String fmt = "* Failed, jd: (%12.2f %5.2f) = %12.2f got: %12.2f, difference %15.12f\n";
          String s = String.format(fmt, q.second, r.first, jd, jd1, jd1 - jd);
          p.first &= false;
          p.second += s;
        }
      }
    }
    if (pass) {
      p.first = true;
      p.second = "* Passed.\n";
    }

    p.second += "\n";

    return p;
  }

  /**
   * Testing jd -> calendar -> jd.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestJD2() {
    CALENDAR_TYPE calendar = CALENDAR_TYPE.JULIAN_GREGORIAN;

    DTPair<Boolean, String> p = this.MakeHeader("Testing jd -> calendar -> jd.", 2);

    boolean pass = true;
    String fmt = "* Failed, jd: (%12.2f %5.2f) = %12.2f got: %12.2f, difference %15.12f\n";
    for (DTPair<YMDHMS, Double> q : testDates) {
      for (DTPair<Double, Double> r : testFoD) {
        double jd = q.second + r.first;
        JulianDateTime jdt = new JulianDateTime(0, jd);
        YMDHMS ymdhms = jdt.GetDateTime(calendar);
        JulianDateTime jdt1 = new JulianDateTime(0, jd);
        jdt1.SetDateTime(ymdhms, calendar);
        double jd1 = jdt.GetJDDouble();

        if (!this.dtb.isclose(jd, jd1, dtb.rel_fod)) {
          pass = false;
          String s = String.format(fmt, q.second, r.first, jd, jd1, jd1 - jd);
          p.first &= false;
          p.second += s;
        }
      }
    }

    if (pass) {
      p.first = true;
      p.second = "* Passed.\n";
    }

    p.second += "\n";

    return p;
  }

  /**
   * Testing calendar -> jd -> calendar.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestCalendar() {
    CALENDAR_TYPE calendar = CALENDAR_TYPE.JULIAN_GREGORIAN;

    DTPair<Boolean, String> p = this.MakeHeader("Testing calendar -> jd -> calendar.", 2);

    boolean pass = true;
    for (DTPair<YMDHMS, Double> q : testDates) {
      for (DTPair<Double, Double> r : testFoD) {
        YMDHMS ymdhms = new YMDHMS(q.first);
        // Using fod_jd[i] as the hms times.
        // This makes times run from 0h to < 24h.
        HMS hms = dtc.h2hms(r.first * 24.0);
        ymdhms.SetHMS(hms);

        JulianDateTime jdt1 = new JulianDateTime();
        jdt1.SetDateTime(ymdhms, calendar);
        YMDHMS ymdhms0 = jdt1.GetDateTime(calendar);

        if (!ymdhms.IsClose(ymdhms0, dtb.rel_seconds)) {
          pass = false;
          String fmt = "* Failed, expected: %s got: %s\n";
          String s = String.format(fmt, ymdhms.GetString(), ymdhms0.GetString());
          p.first &= false;
          p.second += s;
        }
      }
    }
    if (pass) {
      p.first = true;
      p.second = "* Passed.\n";
    }

    p.second += "\n";

    return p;
  }

}
