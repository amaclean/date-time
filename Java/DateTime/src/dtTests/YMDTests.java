/*=========================================================================

  Program:   Date Time Library
  File   :   YMDTests.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
package dtTests;

import dt.DTPair;
import dt.YMD;

import java.util.ArrayList;

/**
 * A test harness to test the YMD class.
 *
 * @author Andrew J. P. Maclean
 */
public class YMDTests extends CommonTests {

  private final int year = 2013;
  private final int month = 1;
  private final int day = 1;
  private final YMD ymd = new YMD(this.year, this.month, this.day);

  /**
   * Run all the tests.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * string of failed tests.
   */
  public DTPair<Boolean, String> TestAll() {
    ArrayList<DTPair<Boolean, String>> messages = new ArrayList<>();

    messages.add(this.TestConstructors());
    messages.add(this.TestSetGet());
    messages.add(this.TestComparisonOperators());
    messages.add(this.TestAssignment());
    messages.add(this.TestStringOutput());

    return this.AggregateResults("YMD Tests.", messages);
  }

  /**
   * Run the tests for the constructors.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestConstructors() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing constructors.", 2);

    p.second += "* YMD() ";
    YMD y = new YMD();
    DTPair<Boolean, String> p1;
    p1 = this.PassFail(y.year == -4712 && y.month == 1 && y.day == 1);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* YMD(y, m, d) ";
    YMD y1 = new YMD(this.year, this.month, this.day);
    p1 = this.PassFail(y1.year == this.year && y1.month == this.month && y1.day == this.day);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* YMD(ymd) ";
    YMD y2 = new YMD(y1);
    p1 = this.PassFail(y1.year == y2.year && y1.month == y2.month && y1.day == y2.day);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for setting and getting the members.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestSetGet() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing setting/getting members.", 2);

    p.second += "* SetYMD(h,m,s) ";
    YMD y1 = new YMD(this.year, this.month, this.day);
    YMD y2 = new YMD();
    y2.SetYMD(this.year, this.month, this.day);
    DTPair<Boolean, String> p1;
    p1 = this.PassFail(y1.year == y2.year && y1.month == y2.month && y1.day == y2.day);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* SetYMD(YMD) ";
    y2.SetYMD(y1);
    p1 = this.PassFail(y1.year == y2.year && y1.month == y2.month && y1.day == y2.day);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* GetYMD() ";
    y2 = y1.GetYMD();
    p1 = this.PassFail(y1.year == y2.year && y1.month == y2.month && y1.day == y2.day);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for the comparison operators.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestComparisonOperators() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing comparison operators.", 2);

    p.second += "* == ";
    YMD y1 = new YMD(this.year, this.month, this.day);
    DTPair<Boolean, String> p1;
    p1 = this.PassFail(y1.eq(this.ymd));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* != ";
    y1.day -= 1;
    p1 = this.PassFail(y1.ne(this.ymd));
    p.first &= p1.first;
    p.second += p1.second;

    YMD a = new YMD(1992, 7, 2);
    YMD b = new YMD(1993, 7, 2);
    YMD c = new YMD(1992, 8, 2);
    YMD d = new YMD(1992, 7, 3);

    p.second += "* &lt; and &le; ";
    p1 = this.PassFail(
        a.lt(b) && a.lt(c) && a.lt(d) && a.le(b) && a.le(c) && a.le(d) && a.le(a) && !(a.gt(b)) && !(a.ge(b)));
    p.first &= p1.first;
    p.second += p1.second;

    YMD e = new YMD(1992, 7, 2);
    YMD f = new YMD(1991, 7, 2);
    YMD g = new YMD(1992, 6, 2);
    YMD h = new YMD(1992, 7, 1);

    p.second += "* &gt; and &ge; ";
    p1 = this.PassFail(
        e.gt(f) && e.gt(g) && e.gt(h) && e.ge(f) && e.ge(g) && e.ge(h) && e.ge(e) && !(e.lt(g)) && !(e.le(f)));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for assignment.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestAssignment() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing assignment.", 2);

    p.second += "* = ";
    YMD y1 = this.ymd;
    DTPair<Boolean, String> p1;
    p1 = this.PassFail(y1.year == this.ymd.year && y1.month == this.ymd.month && y1.day == this.ymd.day);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for string output.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestStringOutput() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing GetString()", 2);
    String s1 = this.ymd.GetString();
    DTPair<Boolean, String> p1;
    String valueStr = "2013-01-01";
    p1 = this.Compare(s1, valueStr, "* ");
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

}
