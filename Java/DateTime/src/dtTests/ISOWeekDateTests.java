/*=========================================================================

  Program:   Date Time Library
  File   :   ISOWeekDateTests.java

  Copyright (c) Andrew J. P. Maclean
  All rights reserved.
  See Copyright.txt or the documentation for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
package dtTests;

import dt.DTPair;
import dt.ISOWeekDate;

import java.util.ArrayList;

/**
 * A test harness to test the ISOWeekDate class.
 *
 * @author Andrew J. P. Maclean
 */
public class ISOWeekDateTests extends CommonTests {

  private final int year = 2009;
  private final int weekNumber = 53;
  private final int weekDay = 4;
  private final ISOWeekDate iwd = new ISOWeekDate(this.year, this.weekNumber, this.weekDay);

  /**
   * Run all the tests.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * string of failed tests.
   */
  public DTPair<Boolean, String> TestAll() {
    ArrayList<DTPair<Boolean, String>> messages = new ArrayList<>();

    messages.add(this.TestConstructors());
    messages.add(this.TestSetGet());
    messages.add(this.TestComparisonOperators());
    messages.add(this.TestAssignment());
    messages.add(this.TestStringOutput());

    return this.AggregateResults("ISOWeekDate Tests.", messages);
  }

  /**
   * Run the tests for the constructors.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestConstructors() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing constructors.", 2);

    p.second += "* ISOWeekDate() ";
    ISOWeekDate i = new ISOWeekDate();
    DTPair<Boolean, String> p1;
    p1 = this.PassFail(i.year == 0 && i.weekNumber == 0 && i.weekDay == 0.0);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* ISOWeekDate(y,wn,yd) ";
    ISOWeekDate iwd1 = new ISOWeekDate(this.year, this.weekNumber, this.weekDay);
    p1 = this
        .PassFail(iwd1.year == this.year && iwd1.weekNumber == this.weekNumber && iwd1.weekDay == this.weekDay);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* ISOWeekDate(iwd ) ";
    ISOWeekDate iwd2 = new ISOWeekDate(iwd1);
    p1 = this
        .PassFail(iwd1.year == iwd2.year && iwd1.weekNumber == iwd2.weekNumber && iwd1.weekDay == iwd2.weekDay);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for setting and getting the members.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestSetGet() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing setting/getting members.", 2);

    p.second += "* SetISOWeekDate(h,m,s) ";
    ISOWeekDate iwd1 = new ISOWeekDate(this.year, this.weekNumber, this.weekDay);
    ISOWeekDate iwd2 = new ISOWeekDate();
    iwd2.SetISOWeekDate(this.year, this.weekNumber, this.weekDay);
    DTPair<Boolean, String> p1;
    p1 = this
        .PassFail(iwd1.year == iwd2.year && iwd1.weekNumber == iwd2.weekNumber && iwd1.weekDay == iwd2.weekDay);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* SetISOWeekDate(iwd) ";
    iwd2.SetISOWeekDate(iwd1);
    p1 = this
        .PassFail(iwd1.year == iwd2.year && iwd1.weekNumber == iwd2.weekNumber && iwd1.weekDay == iwd2.weekDay);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* GetISOWeekDate() ";
    iwd2 = iwd1.GetISOWeekDate();
    p1 = this
        .PassFail(iwd1.year == iwd2.year && iwd1.weekNumber == iwd2.weekNumber && iwd1.weekDay == iwd2.weekDay);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the tests for the comparison operators.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestComparisonOperators() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing comparison operators.", 2);

    p.second += "* == ";
    ISOWeekDate iwd1 = new ISOWeekDate(this.year, this.weekNumber, this.weekDay);
    DTPair<Boolean, String> p1;
    p1 = this.PassFail(iwd1.eq(this.iwd));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "* != ";
    iwd1.weekDay -= 1;
    p1 = this.PassFail(iwd1.ne(this.iwd));
    p.first &= p1.first;
    p.second += p1.second;

    ISOWeekDate a = new ISOWeekDate(2013, 7, 2);
    ISOWeekDate b = new ISOWeekDate(2014, 7, 2);
    ISOWeekDate c = new ISOWeekDate(2013, 8, 2);
    ISOWeekDate d = new ISOWeekDate(2013, 7, 3);

    p.second += "* &lt; and &le; ";
    p1 = this.PassFail(
        a.lt(b) && a.lt(c) && a.lt(d) && a.le(b) && a.le(c) && a.le(d) && a.le(a) && !(a.gt(b)) && !(a.ge(b)));
    p.first &= p1.first;
    p.second += p1.second;

    ISOWeekDate e = new ISOWeekDate(2013, 7, 2);
    ISOWeekDate f = new ISOWeekDate(2012, 7, 2);
    ISOWeekDate g = new ISOWeekDate(2013, 6, 2);
    ISOWeekDate h = new ISOWeekDate(2013, 7, 1);

    p.second += "* &gt; and &ge; ";
    p1 = this.PassFail(
        e.gt(f) && e.gt(g) && e.gt(h) && e.ge(f) && e.ge(g) && e.ge(h) && e.ge(e) && !(e.lt(f)) && !(e.le(f)));
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for assignment.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestAssignment() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing assignment.", 2);

    p.second += "* = ";
    ISOWeekDate iwd1 = new ISOWeekDate();
    iwd1.assign(this.iwd);
    DTPair<Boolean, String> p1;
    p1 = this.PassFail(iwd1.year == this.iwd.year && iwd1.weekNumber == this.iwd.weekNumber
        && iwd1.weekDay == this.iwd.weekDay);
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

  /**
   * Run the test for string output.
   *
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestStringOutput() {

    DTPair<Boolean, String> p = this.MakeHeader("Testing GetString()", 2);

    String s1 = this.iwd.GetString();
    DTPair<Boolean, String> p1;
    String valueStr = "2009-W53-4";
    p1 = this.Compare(s1, valueStr, "* ");
    p.first &= p1.first;
    p.second += p1.second;

    p.second += "\n";

    return p;
  }

}
