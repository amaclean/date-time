package DE431;

import dt.DTPair;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

public class TestDE431Dates {

  /**
   * @param args - The input and output file paths.
   */
  public static void main(String[] args) {
    if (args.length != 2) {
      System.out.println("Need an input file name and output file name.");
      System.exit(0);
    }
    System.out.print("Test started:  ");
    System.out.println(GetCurrentSystemDateTime(TimeZone.getDefault()
        .getID()));
    DE431Dates test = new DE431Dates();
    DE431Dates.TestResults res = test.ReadFile(args[0], -3100015.50,
        8000016.50);
    //      DE431Dates.TestResults res = test.ReadFile(args[0], -3099146.5, -3098690.5);
    //      DE431Dates.TestResults res = test.ReadFile(args[0], -3100015.50, 0.50);
    //      DE431Dates.TestResults res = test.ReadFile(args[0], -0.5, 365.5);
    //      DE431Dates.TestResults res = test.ReadFile(argv[1], -487.5, -1.5);
    String stats = String.format("%d total, %d passed, %d failed",
        res.passed + res.failed, res.passed, res.failed);
    try {
      PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(
          args[1])));
      String result = String.format("Tested\nFrom: %s\nTo:   %s\n",
          res.startDate, res.endDate);
      pw.write(result);
      pw.flush();
      if (!res.allPassed) {
        pw.write("These tests failed.\n");
        for (DTPair<Boolean, String> p : res.result) {
          if (!p.first) {
            pw.write(p.second);
          }
        }
      }
      pw.write(stats);
      pw.write('\n');
      pw.flush();
      pw.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    System.out.println(stats);
    System.out.print("Test finished: ");
    System.out.println(GetCurrentSystemDateTime(TimeZone.getDefault()
        .getID()));

  }


  /**
   * Get the current system date and time.
   *
   * @param tz - the Time Zone e.g. GMT+10:00 Australia/Sydney
   */
  private static String GetCurrentSystemDateTime(String tz) {
    // Initialised with the current date and time.
    Calendar cal = new GregorianCalendar();
    SimpleDateFormat date_format = new SimpleDateFormat(
        "yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
    date_format.setTimeZone(TimeZone.getTimeZone(tz));
    return date_format.format(cal.getTime());
  }

}
