package DE431;

import dt.DTPair;
import dt.DateTimeConversions;
import dt.YMD;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class DE431Dates {
  private final DateTimeConversions dtc = new DateTimeConversions();

  public DE431Dates() {
  }

  public TestResults ReadFile(String fileName, double minJD,
                              double maxJD) {
    TestResults res = new TestResults();
    res.passed = 0;
    res.failed = 0;
    res.allPassed = false;

    try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
      int linesProcessed = 0;
      double jd;
      double jdOld = 0;
      YMD ymd = new YMD();
      boolean gotStartDate = false;
      res.allPassed = true;
      DTPair<Boolean, String> p1;
      for (String line; (line = br.readLine()) != null; ) {
        if (line.charAt(0) == '#') {
          continue;
        }

        Scanner a = new Scanner(line);
        ymd.year = a.nextInt();
        ymd.month = a.nextInt();
        ymd.day = a.nextInt();
        jd = a.nextDouble();
        a.close();
        // Assume the dates in the file are ordered.
        if (jd > maxJD) {
          break;
        }
        if (jd < minJD) {
          continue;
        } else {
          if (!gotStartDate) {
            res.startDate = line;
            gotStartDate = true;
            jdOld = jd;
          }
          res.endDate = line;
          p1 = TestJulianGregorian(jd, ymd);
          p1.second = String.format("%s %s %4.1f\n%s", line, "deltaDays:",
              jd - jdOld, p1.second);
          res.result.add(p1);
          res.allPassed = res.allPassed && p1.first;
          if (p1.first) {
            ++res.passed;
          } else {
            ++res.failed;
          }
          jdOld = jd;
        }
        ++linesProcessed;
        if (linesProcessed % 10000 == 0) {
          System.out.format("Processed: %d dates.\n", linesProcessed);
        }
      }
    } catch (FileNotFoundException e) {
      System.out.format("Failed to open: %s for reading.\n", fileName);
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return res;

  }

  /**
   * Test the conversion of calendar date to julian day. The Gregorian
   *
   * @param jd  The expected julian day for the given year, month and day.
   * @param ymd The date.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestCD2JDJG(double jd, YMD ymd) {
    DTPair<Boolean, String> res = new DTPair<>(null, null);
    int tjd = this.dtc.cd2jdJG(ymd);
    int ref = (int) Math.ceil(jd);
    if (tjd != ref) {
      res.first = false;
      res.second =
          String.format("Fail (cd2jdJG) got: %12.1f  expected: %12.1f\n",
              tjd - 0.5, ref - 0.5);
      return res;
    }
    res.first = true;
    res.second = "Passed (cd2jdG)\n";
    return res;
  }

  /**
   * Test the conversion of julian day to calendar date.
   * The Gregorian or Julian Calendar is used as appropriate.
   *
   * @param jd  The julian day.
   * @param ymd The date.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestJD2CDJG(double jd, YMD ymd) {
    DTPair<Boolean, String> res = new DTPair<>(null, null);
    YMD ymd1 = new YMD(this.dtc.jd2cdJG((int) Math.ceil(jd)));
    if (ymd.ne(ymd1)) {
      res.first = false;
      res.second = String
          .format("Fail (jd2cdJG) got: %02d-%02d-%02d  expected: %02d-%02d-%02d\n",
              ymd1.year, ymd1.month, ymd1.day, ymd.year, ymd.month,
              ymd.day);
      return res;
    }
    res.first = true;
    res.second = "Passed (cd2jdG)\n";
    return res;
  }

  /**
   * Test the conversion of calendar date to julian day and vice versa. The
   * The Gregorian or Julian Calendar is used as appropriate.
   *
   * @param jd  The julian day.
   * @param ymd The date.
   * @return A pair consisting of true (if the test passed) or false and a
   * message.
   */
  private DTPair<Boolean, String> TestJulianGregorian(double jd,
                                                      YMD ymd) {
    DTPair<Boolean, String> p1 = TestCD2JDJG(jd, ymd);
    DTPair<Boolean, String> p2 = TestJD2CDJG(jd, ymd);
    return new DTPair<>(
        p1.first && p2.first, p1.second + p2.second);
  }

  /**
   * A class for holding the test results.
   *
   * @author Andrew J. P. Maclean
   */
  public class TestResults {
    final ArrayList<DTPair<Boolean, String>> result =
        new ArrayList<>();
    public int passed;
    public int failed;
    public String startDate;
    public String endDate;
    public boolean allPassed;
  }
}
