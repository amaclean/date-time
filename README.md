Date Time Library
=================

Introduction
------------

This library provides a set of high precision date and time classes covering a large time span.

A consistent interface (within the limitations of the languages) is provided for C++, Java and Python. The precision of the dates and times across all languages and platforms is at least 1ns.

The calendars used are the Julian proleptic/Julian and Gregorian proleptic/Gregorian calendars. The changeover from the Julian Calendar to the Gregorian Calender occurred in 1582 with Thursday, 1582 October 04, being the last day of the Julian Calendar and the next day: Friday, 1582 October 15, being the first day of the Gregorian Calendar.

Primary references for algorithms were:

- [Explanatory Supplement to the Astronomical Almanac](http://aa.usno.navy.mil/publications/docs/exp_supp.php)
- [Astronomical Algorithms](http://www.willbell.com/math/mc1.HTM)

Testing was done using [Julian Date Converter](http://aa.usno.navy.mil/data/docs/JulianDate.php).

Requirements
------------

Data sizes:

- integer must be at least 32-bit
- floating point must be 8-byte double precision (IEEE 754).
- In C++: long long must be at least 64-bit.

The library has been implemented in:

- C++: A C++20 compliant compiler.
- Java 8 or greater,
- Python 3.9 or greater,

Languages
---------

The implementations are coded in C++, Java and Python. For each language there is a set of tests to verify the functionality of the classes.

Generally it should be easy to code in any of the languages as the classes mostly have similar names and the methods in the classes are generally similar. C++ class and method names are regarded as the standard. This means that in the case of Java, method names start with a capital letter and, in the case of Python, methods have names that are are snake case and may need to reflect the data types they handle.

- C++ code is considered as the reference standard with the Java and Python code being based on this implementation.

- Java:

  - Java does not have multiple inheritance so the classes DateConversions and TimeConversions have been merged into the one class DateTimeConversions. This also means that the YMDHMS class has two fields of type YMD and HMS.
  - Java does not have operator overloading so the following method names correspond to the overloaded C++ operators:

    - assign (=), eq (==), ne (!=), lt (<), le (<=), gt (>), ge (>=),

    - add (+), inc (+=), sub (-), dec (-=).

- Python:

  - The python classes generally type check arguments and will automatically convert input parameters to the correct type where possible.
  - Because of the Duck Typing feature of Python, some methods explicitly check the type of their parameters.

Usage
-----

A good approach is as follows:

1. Create an instance of the **DateTimeFormat** class. This will give you reasonable default formatting parameters that you can change. Instances of this class are needed by instances of the classes **DateTimeFormatter** and **DateTimeParser**.
2. Create an instance of the **DateTimeFormatter** class in order to format dates, times and time intervals.
3. Create an instance of the **DateTimeParser** class in order to parse dates, times and time intervals represented as strings.
4. Create an instance of the **JulianDateTime** class. This will give you access to all the necessary methods for date and time manipulations including addition and date differences. This class stores the date and time and inherits **DateConversions** and **TimeConversions** (**DateTimeConversions** in the case of Java). These conversion classes do not store any data, they just provide methods for date and time conversions. This means that you can use these to implement your own specific functionality.

In the **JulianDateTime** class, dates and times are represented as a continuous count of days and fractions of a day from -13200 August 15, Greenwich noon Julian proleptic calendar (JD-3100015.50) to 17191-Mar-15, Greenwich noon Gregorian Calendar (JD8000016.50). The underlying storage is that of an integer representing the Julian Day Number for noon on that date and a floating-point number representing the fraction of the day running from midday to midday (always positive). This should yield a precision of around 1ns covering this period.

Examples
--------

Examples are provided for C++, Java and Python. In addition a QT6 and an Android example are also provided.

Testing
-------

The C++ tests are the reference ones with  *LCOV* reporting 99% line coverage and 94% function coverage for **dt**.

Tests for Java and Python also provide similar coverage.

Documentation
-------------

This can be generated via the scripts `MakeAllTheDocumentation.sh` for Linux and Apple, `MakeAllTheDocumentation.cmd` for Windows. In order to generate the documentation the following programs must be installed, Doxygen (for C++, Java and Python), Java Development Kit (for Java) and Sphinx (for Python).

Directory Structure
-------------------

At the top level we have:

- `Android` - containing the Android example.
- `C++` - containing the C++ code.
- `hooks` - containing various hook scripts for git.
- `Java` - containing the Java code.
- `Python` - containing the Python code.
- `Repository` - containing the master files that need to be edited and copied to various parts of the directory tree using `CopyDocumentation.cmd` or `CopyDocumentation.sh` in the top-level directory.

For each language (C++, Java, Python) the main subdirectories are:

- `doc` - containing the documentation for the Date Time Library and tests.
- `dt` - The Date Time Library
- `dtExamples` - containing examples
- `dtTests` - containing the tests along with a subdirectory `dtTestAll` that contains a driver to run all the tests.
- MakeDocumentation - containing scripts for generating the documentation.

License
-------

The Date Time Library is an open-source toolkit licensed under the [BSD](http://en.wikipedia.org/wiki/BSD_licenses) license.

Copyright (c) 2012-2026 Andrew J. P. Maclean
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

- Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.
- Neither the name of Andrew J. P. Maclean nor the names of any contributors
  may be used to endorse or promote products derived from this software
  without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ''AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
